﻿//using System;
//using System.Collections.Generic;
//using System.Text;
//using Microsoft.EntityFrameworkCore;
//using Microsoft.EntityFrameworkCore.Design;
//using Microsoft.Extensions.DependencyInjection;

//namespace TDL.ContentPlatform.Server.Database
//{
//    public class ContentDbConnectionMockContextFactory : IDesignTimeDbContextFactory<ContentDbContext>
//    {
//        // Used during command line regeneration of Migrations


//        public ContentDbContext CreateDbContext(string[] args)
//        {
//            var optionsBuilder = new DbContextOptionsBuilder<ContentDbContext>();
//            optionsBuilder.UseSqlServer("Server=SQL2.localdev.tedd.no;Database=TDL_Content;persist security info=True;user id=TDL_dev;password=yoghurt;MultipleActiveResultSets=True;",
//                    x =>
//                    {
//                        x.MigrationsHistoryTable(tableName: "__EFMigrationsHistory_ContentPlatform");
//                        x.EnableRetryOnFailure(maxRetryCount: 5, maxRetryDelay: TimeSpan.FromSeconds(10), errorNumbersToAdd: new int[0]);
//                    })
//                .UseQueryTrackingBehavior(QueryTrackingBehavior.TrackAll);


//            return new ContentDbContext(optionsBuilder.Options);
//        }
//    }
//}
