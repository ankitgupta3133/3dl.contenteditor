﻿using System;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using TDL.ContentPlatform.Server.Database.Tables;

namespace TDL.ContentPlatform.Server.Database
{
    public class ContentDbContext : IdentityDbContext<TdlUser, TdlRole, Guid>
    {
        public ContentDbContext() : base()
        {
            //if (_firstRun)
            //{
            //    _firstRun = false;
            //    Database.Migrate();
            //}
        }
        //private static bool _firstRun = true;
        public ContentDbContext(DbContextOptions<ContentDbContext> options) : base(options)
        {
            //if (_firstRun)
            //{
            //    _firstRun = false;
            //    Database.Migrate();
            //}
        }

        //public virtual DbSet<AspNetRoleClaims> AspNetRoleClaims { get; set; }
        //public virtual DbSet<TdlRole> AspNetRoles { get; set; }
        //public virtual DbSet<AspNetUserClaims> AspNetUserClaims { get; set; }
        //public virtual DbSet<AspNetUserLogins> AspNetUserLogins { get; set; }
        //public virtual DbSet<AspNetUserRoles> AspNetUserRoles { get; set; }
        //public virtual DbSet<TdlUser> AspNetUsers { get; set; }
        //public virtual DbSet<AspNetUserTokens> AspNetUserTokens { get; set; }
        public virtual DbSet<Asset> Asset { get; set; }
        public virtual DbSet<AssetContent> AssetContent { get; set; }
        public virtual DbSet<AssetContentDetail> AssetContentDetail { get; set; }
        public virtual DbSet<AssetContentMetadata> AssetContentMetadata { get; set; }
        public virtual DbSet<AssetDetails> AssetDetails { get; set; }
        public virtual DbSet<AssetGrades> AssetGrades { get; set; }
        public virtual DbSet<AssetHistory> AssetHistory { get; set; }
        public virtual DbSet<AssetLanguages> AssetLanguages { get; set; }
        public virtual DbSet<AssetNames> AssetNames { get; set; }
        public virtual DbSet<AssetStreams> AssetStreams { get; set; }
        public virtual DbSet<AssetSubjects> AssetSubjects { get; set; }
        public virtual DbSet<AssetSubTopics> AssetSubTopics { get; set; }
        public virtual DbSet<AssetSyllabus> AssetSyllabus { get; set; }
        public virtual DbSet<AssetTopics> AssetTopics { get; set; }
        public virtual DbSet<ClientGeneralConfig> ClientGeneralConfig { get; set; }
        public virtual DbSet<ClientMetricLog> ClientMetricLog { get; set; }
        public virtual DbSet<ContentPackage> ContentPackage { get; set; }
        public virtual DbSet<ContentPackageFileTable> ContentPackageFileTable { get; set; }
        public virtual DbSet<ContentPackageTable> ContentPackageTable { get; set; }
        public virtual DbSet<CsAccount> CsAccount { get; set; }
        public virtual DbSet<CsAttributeMetadata> CsAttributeMetadata { get; set; }
        public virtual DbSet<CsDeleteLog> CsDeleteLog { get; set; }
        public virtual DbSet<CsGlobalOptionSetMetadata> CsGlobalOptionSetMetadata { get; set; }
        public virtual DbSet<CsNewGrade> CsNewGrade { get; set; }
        public virtual DbSet<CsNewGradeStream> CsNewGradeStream { get; set; }
        public virtual DbSet<CsNewGradeSubject> CsNewGradeSubject { get; set; }
        public virtual DbSet<CsNewGradeSyllabus> CsNewGradeSyllabus { get; set; }
        public virtual DbSet<CsNewLanguage> CsNewLanguage { get; set; }
        public virtual DbSet<CsNewMasterlogin> CsNewMasterlogin { get; set; }
        public virtual DbSet<CsNewOpportunitySubscriptionMapping> CsNewOpportunitySubscriptionMapping { get; set; }
        public virtual DbSet<CsNewOpportunitySubscriptionMasterLogin> CsNewOpportunitySubscriptionMasterLogin { get; set; }
        public virtual DbSet<CsNewPackage> CsNewPackage { get; set; }
        public virtual DbSet<CsNewPackageGrade> CsNewPackageGrade { get; set; }
        public virtual DbSet<CsNewPackageLanguage> CsNewPackageLanguage { get; set; }
        public virtual DbSet<CsNewPlatform> CsNewPlatform { get; set; }
        public virtual DbSet<CsNewStream> CsNewStream { get; set; }
        public virtual DbSet<CsNewSubject> CsNewSubject { get; set; }
        public virtual DbSet<CsNewSubjectTopic> CsNewSubjectTopic { get; set; }
        public virtual DbSet<CsNewSubscription> CsNewSubscription { get; set; }
        public virtual DbSet<CsNewSubscriptionPackage> CsNewSubscriptionPackage { get; set; }
        public virtual DbSet<CsNewSubtopic> CsNewSubtopic { get; set; }
        public virtual DbSet<CsNewSyllabus> CsNewSyllabus { get; set; }
        public virtual DbSet<CsNewSyllabusStream> CsNewSyllabusStream { get; set; }
        public virtual DbSet<CsNewSyllabusSubject> CsNewSyllabusSubject { get; set; }
        public virtual DbSet<CsNewTopic> CsNewTopic { get; set; }
        public virtual DbSet<CsNewTopicSubtopic> CsNewTopicSubtopic { get; set; }
        public virtual DbSet<CsOpportunity> CsOpportunity { get; set; }
        public virtual DbSet<CsOptionSetMetadata> CsOptionSetMetadata { get; set; }
        public virtual DbSet<CsStateMetadata> CsStateMetadata { get; set; }
        public virtual DbSet<CsStatusMetadata> CsStatusMetadata { get; set; }
        public virtual DbSet<CsTargetMetadata> CsTargetMetadata { get; set; }
        public virtual DbSet<EfmigrationsHistoryContentPlatform> EfmigrationsHistoryContentPlatform { get; set; }
        public virtual DbSet<EntityThumbnail> EntityThumbnail { get; set; }
        public virtual DbSet<FavouriteAssets> FavouriteAssets { get; set; }
        public virtual DbSet<MstActivity> MstActivity { get; set; }
        public virtual DbSet<MstFileType> MstFileType { get; set; }
        public virtual DbSet<MstLanguage> MstLanguage { get; set; }
        public virtual DbSet<MstLevel> MstLevel { get; set; }
        public virtual DbSet<MstStatus> MstStatus { get; set; }
        public virtual DbSet<MstStream> MstStream { get; set; }
        public virtual DbSet<MstSubject> MstSubject { get; set; }
        public virtual DbSet<MstSubtopic> MstSubtopic { get; set; }
        public virtual DbSet<MstSyllabus> MstSyllabus { get; set; }
        public virtual DbSet<MstTag> MstTag { get; set; }
        public virtual DbSet<MstTopic> MstTopic { get; set; }
        public virtual DbSet<Nlog> Nlog { get; set; }
        public virtual DbSet<UserAssets> UserAssets { get; set; }
        public virtual DbSet<UserLogs> UserLogs { get; set; }
        public virtual DbSet<UserProfile> UserProfile { get; set; }
        public virtual DbSet<UserDetails> UserDetails { get; set; }
        public virtual DbSet<XmlcontentLabel> XmlcontentLabel { get; set; }
        public virtual DbSet<XmlcontentModel> XmlcontentModel { get; set; }
        public virtual DbSet<Activities> Activities { get; set; }
        public virtual DbSet<ActivityNames> ActivityNames { get; set; }
        public virtual DbSet<ActivityPlatforms> ActivityPlatforms { get; set; }
        public virtual DbSet<ActivityDetails> ActivityDetails { get; set; }
        public virtual DbSet<QuizDetails> QuizDetails { get; set; }
        public virtual DbSet<PuzzleDetails> PuzzleDetails { get; set; }
        public virtual DbSet<QuizLabelDetails> QuizLabelDetails { get; set; }
        public virtual DbSet<AssetRelations> AssetRelations { get; set; }
        public virtual DbSet<MstMaterial> MstMaterial { get; set; }
        public virtual DbSet<MstElement> MstElement { get; set; }
        public virtual DbSet<MstCompound> MstCompound { get; set; }
        public virtual DbSet<CompoundElement> CompoundElement { get; set; }
        public virtual DbSet<AppMaster> AppMaster { get; set; }
        public virtual DbSet<ApkDetail> ApkDetail { get; set; }
        public virtual DbSet<AssetContentFeedback> AssetContentFeedback { get; set; }
        public virtual DbSet<MstProperty> MstProperty { get; set; }
        public virtual DbSet<XmlContentLabelNames> XmlContentLabelNames { get; set; }
        public virtual DbSet<ClassificationDetails> ClassificationDetails { get; set; }
        public virtual DbSet<ClassificationAssets> ClassificationAssets { get; set; }
        public virtual DbSet<MstTagNames> MstTagNames { get; set; }
        public virtual DbSet<AssetTags> AssetTags { get; set; }
        public virtual DbSet<MstPropertyNames> MstPropertyNames { get; set; }
        public virtual DbSet<AssetProperties> AssetProperties { get; set; }
        public virtual DbSet<XmlcontentModelNames> XmlcontentModelNames { get; set; }
        public virtual DbSet<UserDetailPlatform> UserDetailPlatform { get; set; }
        public virtual DbSet<ApplicationMessage> ApplicationMessage { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            //modelBuilder.Entity<AspNetRoleClaims>(entity =>
            //{
            //    entity.HasIndex(e => e.RoleId);
            //});

            //modelBuilder.Entity<TdlRole>(entity =>
            //{
            //    entity.HasIndex(e => e.NormalizedName)
            //        .HasName("RoleNameIndex")
            //        .IsUnique()
            //        .HasFilter("([NormalizedName] IS NOT NULL)");

            //    entity.Property(e => e.Id).ValueGeneratedNever();
            //});

            //modelBuilder.Entity<AspNetUserClaims>(entity =>
            //{
            //    entity.HasIndex(e => e.UserId);
            //});

            //modelBuilder.Entity<AspNetUserLogins>(entity =>
            //{
            //    entity.HasKey(e => new { e.LoginProvider, e.ProviderKey });

            //    entity.HasIndex(e => e.UserId);
            //});

            //modelBuilder.Entity<AspNetUserRoles>(entity =>
            //{
            //    entity.HasKey(e => new { e.UserId, e.RoleId });

            //    entity.HasIndex(e => e.RoleId);
            //});

            //modelBuilder.Entity<TdlUser>(entity =>
            //{
            //    entity.HasIndex(e => e.NormalizedEmail)
            //        .HasName("EmailIndex");

            //    entity.HasIndex(e => e.NormalizedUserName)
            //        .HasName("UserNameIndex")
            //        .IsUnique()
            //        .HasFilter("([NormalizedUserName] IS NOT NULL)");

            //    entity.Property(e => e.Id).ValueGeneratedNever();
            //});

            //modelBuilder.Entity<AspNetUserTokens>(entity =>
            //{
            //    entity.HasKey(e => new { e.UserId, e.LoginProvider, e.Name });
            //});

            modelBuilder.Entity<Asset>(entity =>
            {
                entity.Property(e => e.AssetName).IsUnicode(false);

                entity.Property(e => e.FileName).IsUnicode(false);

                entity.Property(e => e.FilePath).IsUnicode(false);

                entity.Property(e => e.FileSize).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");

                entity.Property(e => e.IsDirectory).HasDefaultValueSql("((0))");

                entity.Property(e => e.Language).IsUnicode(false);

                entity.Property(e => e.Level).IsUnicode(false);

                entity.Property(e => e.ModifiedByName).IsUnicode(false);

                entity.Property(e => e.Status).IsUnicode(false);

                entity.Property(e => e.Stream).IsUnicode(false);

                entity.Property(e => e.Subject).IsUnicode(false);

                entity.Property(e => e.Subtopic).IsUnicode(false);

                entity.Property(e => e.SubtopicDesc).IsUnicode(false);

                entity.Property(e => e.Syllabus).IsUnicode(false);

                entity.Property(e => e.Tags).IsUnicode(false);

                entity.Property(e => e.Topic).IsUnicode(false);

                entity.Property(e => e.Type).IsUnicode(false);

                entity.HasOne(d => d.ContentPackage)
                    .WithMany(p => p.Asset)
                    .HasForeignKey(d => d.ContentPackageId)
                    .HasConstraintName("FK__Asset__ContentPa__1332DBDC");
            });

            modelBuilder.Entity<AssetContent>(entity =>
            {
                entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");

                entity.Property(e => e.IsDirectory).HasDefaultValueSql("((0))");

                entity.Property(e => e.IsEnabled).HasDefaultValueSql("((0))");

                entity.HasOne(d => d.Asset)
                    .WithMany(p => p.AssetContent)
                    .HasForeignKey(d => d.AssetId)
                    .HasConstraintName("FK__AssetCont__Asset__0F624AF8");

                entity.HasIndex(p => new { p.AssetId, p.IsEnabled })
                .IsUnique();
            });
                

            modelBuilder.Entity<AssetContentDetail>(entity =>
            {
                entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");
                entity.Property(e => e.IsEnabled).HasDefaultValueSql("((0))");
                entity.Property(e => e.LanguageShortName).IsUnicode(false);

                entity.HasIndex(acd => new { acd.AssetContentId, acd.IsDeleted });

                entity.HasOne(d => d.AssetContent)
                    .WithMany(p => p.AssetContentDetail)
                    .HasForeignKey(d => d.AssetContentId)
                    .HasConstraintName("FK__AssetCont__Asset__208CD6FA");
            });

            modelBuilder.Entity<AssetContentMetadata>(entity =>
            {
                entity.HasQueryFilter(q => q.IsDeleted == false);

                entity.HasIndex(acd => new { acd.AssetContentId, acd.IsDeleted });

                entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");
                entity.Property(e => e.IsEnabled).HasDefaultValueSql("((0))");
                entity.Property(e => e.Json).IsUnicode(true);

                entity.HasOne(d => d.AssetContent)
                    .WithMany(p => p.Metadata)
                    .HasForeignKey(d => d.AssetContentId)
                    .HasConstraintName("FK__AssetCont__Asset__208CD6FF");
            });

            modelBuilder.Entity<AssetDetails>(entity =>
            {
                entity.HasIndex(e => e.AssetId);
            });

            modelBuilder.Entity<AssetGrades>(entity =>
            {
                entity.HasIndex(e => e.AssetId);
            });

            modelBuilder.Entity<AssetHistory>(entity =>
            {
                entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");

                entity.HasOne(d => d.AssetContent)
                    .WithMany(p => p.AssetHistory)
                    .HasForeignKey(d => d.AssetContentId)
                    .HasConstraintName("FK__AssetHist__Asset__0FEC5ADD");

                entity.HasOne(d => d.Asset)
                    .WithMany(p => p.AssetHistory)
                    .HasForeignKey(d => d.AssetId)
                    .HasConstraintName("FK__AssetHist__Asset__0EF836A4");
            });

            modelBuilder.Entity<AssetLanguages>(entity =>
            {
                entity.HasIndex(e => e.AssetId);
            });

            modelBuilder.Entity<AssetNames>(entity =>
            {
                entity.HasIndex(e => e.AssetId);
            });

            modelBuilder.Entity<AssetStreams>(entity =>
            {
                entity.HasIndex(e => e.AssetId);
            });

            modelBuilder.Entity<AssetSubjects>(entity =>
            {
                entity.HasIndex(e => e.AssetId);
            });

            modelBuilder.Entity<AssetSubTopics>(entity =>
            {
                entity.HasIndex(e => e.AssetId);
            });

            modelBuilder.Entity<AssetSyllabus>(entity =>
            {
                entity.HasIndex(e => e.AssetId);
            });

            modelBuilder.Entity<AssetTopics>(entity =>
            {
                entity.HasIndex(e => e.AssetId);
            });

            modelBuilder.Entity<ClientGeneralConfig>(entity =>
            {
                entity.HasIndex(e => e.IsDeleted);
            });

            modelBuilder.Entity<ContentPackage>(entity =>
            {
                entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");

                entity.Property(e => e.IsEnabled).HasDefaultValueSql("((1))");
            });

            modelBuilder.Entity<ContentPackageFileTable>(entity =>
            {
                entity.HasIndex(e => e.ContentPackageId);

                entity.HasIndex(e => e.IsDeleted);

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<ContentPackageTable>(entity =>
            {
                entity.HasIndex(e => e.IsDeleted);

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<CsAccount>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<CsDeleteLog>(entity =>
            {
                entity.HasIndex(e => new { e.EntityName, e.RecordId })
                    .HasName("IDX_DeleteLog_EntityNameRecordId")
                    .IsUnique();
            });

            modelBuilder.Entity<CsGlobalOptionSetMetadata>(entity =>
            {
                entity.HasKey(e => new { e.OptionSetName, e.Option, e.IsUserLocalizedLabel, e.LocalizedLabelLanguageCode });
            });

            modelBuilder.Entity<CsNewGrade>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<CsNewGradeStream>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<CsNewGradeSubject>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<CsNewGradeSyllabus>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<CsNewLanguage>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<CsNewMasterlogin>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<CsNewOpportunitySubscriptionMapping>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<CsNewOpportunitySubscriptionMasterLogin>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<CsNewPackage>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<CsNewPackageGrade>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<CsNewPackageLanguage>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<CsNewPlatform>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<CsNewStream>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<CsNewSubject>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<CsNewSubjectTopic>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<CsNewSubscription>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<CsNewSubscriptionPackage>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<CsNewSubtopic>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<CsNewSyllabus>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<CsNewSyllabusStream>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<CsNewSyllabusSubject>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<CsNewTopic>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<CsNewTopicSubtopic>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<CsOpportunity>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<CsOptionSetMetadata>(entity =>
            {
                entity.HasKey(e => new { e.EntityName, e.OptionSetName, e.Option, e.IsUserLocalizedLabel, e.LocalizedLabelLanguageCode });
            });

            modelBuilder.Entity<CsStateMetadata>(entity =>
            {
                entity.HasKey(e => new { e.EntityName, e.State, e.IsUserLocalizedLabel, e.LocalizedLabelLanguageCode });
            });

            modelBuilder.Entity<CsStatusMetadata>(entity =>
            {
                entity.HasKey(e => new { e.EntityName, e.State, e.Status, e.IsUserLocalizedLabel, e.LocalizedLabelLanguageCode });
            });

            modelBuilder.Entity<CsTargetMetadata>(entity =>
            {
                entity.HasKey(e => new { e.EntityName, e.AttributeName, e.ReferencedEntity });
            });

            modelBuilder.Entity<EfmigrationsHistoryContentPlatform>(entity =>
            {
                entity.Property(e => e.MigrationId).ValueGeneratedNever();
            });

            modelBuilder.Entity<FavouriteAssets>(entity =>
            {
                entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");

                entity.HasOne(d => d.AssetContent)
                    .WithMany(p => p.FavouriteAssets)
                    .HasForeignKey(d => d.AssetContentId)
                    .HasConstraintName("FK__Favourite__Asset__14B10FFA");

                entity.HasOne(d => d.Asset)
                    .WithMany(p => p.FavouriteAssets)
                    .HasForeignKey(d => d.AssetId)
                    .HasConstraintName("FK__Favourite__Asset__13BCEBC1");
            });

            modelBuilder.Entity<MstFileType>(entity =>
            {
                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");

                entity.Property(e => e.TypeName).IsUnicode(false);
            });

            modelBuilder.Entity<MstLanguage>(entity =>
            {
                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");

                entity.Property(e => e.LanguageName).IsUnicode(false);

                entity.Property(e => e.LanguageShortName).IsUnicode(false);
            });

            modelBuilder.Entity<MstLevel>(entity =>
            {
                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");

                entity.Property(e => e.LevelName).IsUnicode(false);
            });

            modelBuilder.Entity<MstStatus>(entity =>
            {
                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");

                entity.Property(e => e.StatusName).IsUnicode(false);
            });

            modelBuilder.Entity<MstStream>(entity =>
            {
                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");

                entity.Property(e => e.StreamName).IsUnicode(false);
            });

            modelBuilder.Entity<MstSubject>(entity =>
            {
                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");

                entity.Property(e => e.SubjectName).IsUnicode(false);
            });

            modelBuilder.Entity<MstSubtopic>(entity =>
            {
                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");

                entity.Property(e => e.PsubtopicName).IsUnicode(false);

                entity.Property(e => e.SubtopicName).IsUnicode(false);

                entity.HasOne(d => d.Topic)
                    .WithMany(p => p.MstSubtopic)
                    .HasForeignKey(d => d.TopicId)
                    .HasConstraintName("FK__MstSubtop__Topic__6B24EA82");
            });

            modelBuilder.Entity<MstSyllabus>(entity =>
            {
                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");

                entity.Property(e => e.SyllabusName).IsUnicode(false);
            });

            modelBuilder.Entity<MstTag>(entity =>
            {
                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");

                entity.Property(e => e.TagName).IsUnicode(false);
            });

            modelBuilder.Entity<MstActivity>(entity =>
            {
                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");

                entity.Property(e => e.ActivityName).IsUnicode(false);
            });

            modelBuilder.Entity<MstTopic>(entity =>
            {
                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");

                entity.Property(e => e.TopicName).IsUnicode(false);

                entity.HasOne(d => d.Subject)
                    .WithMany(p => p.MstTopic)
                    .HasForeignKey(d => d.SubjectId)
                    .HasConstraintName("FK__MstTopic__Subjec__66603565");
            });

            modelBuilder.Entity<UserAssets>(entity =>
            {
                entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");

                entity.Property(e => e.IsDownloaded).HasDefaultValueSql("((0))");

                entity.HasOne(d => d.AssetContent)
                    .WithMany(p => p.UserAssets)
                    .HasForeignKey(d => d.AssetContentId)
                    .HasConstraintName("FK__UserAsset__Asset__1975C517");

                entity.HasOne(d => d.Asset)
                    .WithMany(p => p.UserAssets)
                    .HasForeignKey(d => d.AssetId)
                    .HasConstraintName("FK__UserAsset__Asset__1881A0DE");
            });

            modelBuilder.Entity<UserLogs>(entity =>
            {
                entity.Property(e => e.Action).IsUnicode(false);

                entity.Property(e => e.AddedOn).HasDefaultValueSql("(getutcdate())");

                entity.Property(e => e.Description).IsUnicode(false);

                entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");

                entity.Property(e => e.UserName).IsUnicode(false);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserLogs)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK__UserLogs__UserID__52593CB8");
            });

            modelBuilder.Entity<UserProfile>(entity =>
            {
                entity.Property(e => e.UserId).ValueGeneratedNever();

                entity.Property(e => e.Address1).IsUnicode(false);

                entity.Property(e => e.Adrress2).IsUnicode(false);

                entity.Property(e => e.City).IsUnicode(false);

                entity.Property(e => e.Country).IsUnicode(false);

                entity.Property(e => e.CreatedOn).HasDefaultValueSql("(getutcdate())");

                entity.Property(e => e.Fax).IsUnicode(false);

                entity.Property(e => e.FirstName).IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");

                entity.Property(e => e.LastName).IsUnicode(false);

                entity.Property(e => e.Password).IsUnicode(false);

                entity.Property(e => e.Phone).IsUnicode(false);

                entity.Property(e => e.State).IsUnicode(false);

                entity.Property(e => e.UserName).IsUnicode(false);

                entity.Property(e => e.WebsiteUrl).IsUnicode(false);

                entity.Property(e => e.Zip).IsUnicode(false);
            });

            modelBuilder.Entity<XmlcontentLabel>(entity =>
            {
                entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");

                entity.Property(e => e.IsEnabled).HasDefaultValueSql("((0))");

                entity.HasOne(d => d.Model)
                    .WithMany(p => p.XmlcontentLabel)
                    .HasForeignKey(d => d.ModelId)
                    .HasConstraintName("FK__XMLConten__Model__1AD3FDA4");

                entity.HasIndex(p => new { p.LabelId, p.IsEnabled })
                .IsUnique();
            });

            modelBuilder.Entity<XmlcontentModel>(entity =>
            {
                entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");

                entity.Property(e => e.IsEnabled).HasDefaultValueSql("((0))");

                entity.HasOne(d => d.AssetContent)
                    .WithMany(p => p.XmlcontentModel)
                    .HasForeignKey(d => d.AssetContentId)
                    .HasConstraintName("FK__XMLConten__Asset__160F4887");

                entity.HasIndex(p => p.AssetContentId)
                .IsUnique();
            });

            //modelBuilder.Entity<UserDetails>(entity =>
            //{
            //    entity.HasIndex(p => new { p.UserId, p.IsActive })
            //    .IsUnique();
            //});
        }
    }
}
