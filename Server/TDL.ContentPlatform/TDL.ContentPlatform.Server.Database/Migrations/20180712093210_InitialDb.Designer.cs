﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using TDL.ContentPlatform.Server.Database;

namespace TDL.ContentPlatform.Server.Database.Migrations
{
    [DbContext(typeof(ContentDbContext))]
    [Migration("20180712093210_InitialDb")]
    partial class InitialDb
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
        }
    }
}
