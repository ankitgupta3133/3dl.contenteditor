﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TDL.ContentPlatform.Server.Database.Migrations
{
    public partial class UserFileds2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AssetHistory_Users_UserID",
                table: "AssetHistory");

            migrationBuilder.DropForeignKey(
                name: "FK_FavouriteAssets_Users_UserID",
                table: "FavouriteAssets");

            migrationBuilder.DropForeignKey(
                name: "FK_GradeDetails_MstLevel_GradeID",
                table: "GradeDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_GradeDetails_MstStream_StreamID",
                table: "GradeDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_GradeDetails_MstSubject_SubjectID",
                table: "GradeDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_GradeDetails_MstSyllabus_SyllabusID",
                table: "GradeDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_Opportunity_Account_AccountID",
                table: "Opportunity");

            migrationBuilder.DropForeignKey(
                name: "FK_OpportunitySubscription_Opportunity_OpportunityID",
                table: "OpportunitySubscription");

            migrationBuilder.DropForeignKey(
                name: "FK_OpportunitySubscription_Subscription_SubscriptionID",
                table: "OpportunitySubscription");

            migrationBuilder.DropForeignKey(
                name: "FK_PackageDetails_MstLevel_GradeID",
                table: "PackageDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_PackageDetails_MstLanguage_LanguageID",
                table: "PackageDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_PackageDetails_ContentPackage_PackageID",
                table: "PackageDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_SubscriptionPackage_ContentPackage_PackageID",
                table: "SubscriptionPackage");

            migrationBuilder.DropForeignKey(
                name: "FK_SubscriptionPackage_Subscription_SubscriptionID",
                table: "SubscriptionPackage");

            migrationBuilder.DropForeignKey(
                name: "FK_UserAccount_Account_AccountID",
                table: "UserAccount");

            migrationBuilder.DropForeignKey(
                name: "FK_UserAccount_Users_UserID",
                table: "UserAccount");

            migrationBuilder.DropForeignKey(
                name: "FK_UserAssets_Users_UserID",
                table: "UserAssets");

            migrationBuilder.DropForeignKey(
                name: "FK_UserDetails_MstLevel_GradeID",
                table: "UserDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_UserDetails_MstLanguage_LanguageID",
                table: "UserDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_UserDetails_MstStream_StreamID",
                table: "UserDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_UserDetails_MstSubject_SubjectID",
                table: "UserDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_UserDetails_MstSyllabus_SyllabusID",
                table: "UserDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_UserDetails_Users_UserID",
                table: "UserDetails");

            migrationBuilder.AlterColumn<bool>(
                name: "IsDeleted",
                table: "UserDetails",
                nullable: true,
                defaultValueSql: "((0))",
                oldClrType: typeof(bool),
                oldNullable: true);

            migrationBuilder.AlterColumn<bool>(
                name: "IsDeleted",
                table: "UserAccount",
                nullable: true,
                defaultValueSql: "((0))",
                oldClrType: typeof(bool),
                oldNullable: true);

            migrationBuilder.AlterColumn<bool>(
                name: "IsActive",
                table: "UserAccount",
                nullable: true,
                defaultValueSql: "((1))",
                oldClrType: typeof(bool),
                oldNullable: true);

            migrationBuilder.AlterColumn<bool>(
                name: "IsDeleted",
                table: "SubscriptionPackage",
                nullable: true,
                defaultValueSql: "((0))",
                oldClrType: typeof(bool),
                oldNullable: true);

            migrationBuilder.AlterColumn<bool>(
                name: "IsDeleted",
                table: "Subscription",
                nullable: true,
                defaultValueSql: "((0))",
                oldClrType: typeof(bool),
                oldNullable: true);

            migrationBuilder.AlterColumn<bool>(
                name: "IsActive",
                table: "Subscription",
                nullable: true,
                defaultValueSql: "((1))",
                oldClrType: typeof(bool),
                oldNullable: true);

            migrationBuilder.AlterColumn<bool>(
                name: "IsDeleted",
                table: "PackageDetails",
                nullable: true,
                defaultValueSql: "((0))",
                oldClrType: typeof(bool),
                oldNullable: true);

            migrationBuilder.AlterColumn<bool>(
                name: "IsDeleted",
                table: "OpportunitySubscription",
                nullable: true,
                defaultValueSql: "((0))",
                oldClrType: typeof(bool),
                oldNullable: true);

            migrationBuilder.AlterColumn<bool>(
                name: "IsActive",
                table: "OpportunitySubscription",
                nullable: true,
                defaultValueSql: "((1))",
                oldClrType: typeof(bool),
                oldNullable: true);

            migrationBuilder.AlterColumn<bool>(
                name: "IsDeleted",
                table: "Opportunity",
                nullable: true,
                defaultValueSql: "((0))",
                oldClrType: typeof(bool),
                oldNullable: true);

            migrationBuilder.AlterColumn<bool>(
                name: "IsActive",
                table: "Opportunity",
                nullable: true,
                defaultValueSql: "((1))",
                oldClrType: typeof(bool),
                oldNullable: true);

            migrationBuilder.AlterColumn<bool>(
                name: "IsDeleted",
                table: "GradeDetails",
                nullable: true,
                defaultValueSql: "((0))",
                oldClrType: typeof(bool),
                oldNullable: true);

            migrationBuilder.AlterColumn<bool>(
                name: "IsDeleted",
                table: "Account",
                nullable: true,
                defaultValueSql: "((0))",
                oldClrType: typeof(bool),
                oldNullable: true);

            migrationBuilder.AlterColumn<bool>(
                name: "IsActive",
                table: "Account",
                nullable: true,
                defaultValueSql: "((1))",
                oldClrType: typeof(bool),
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "LogMetaData",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IsDeleted = table.Column<bool>(nullable: true, defaultValueSql: "((0))"),
                    RequestBy = table.Column<Guid>(nullable: true),
                    RequestContentType = table.Column<string>(maxLength: 100, nullable: true),
                    RequestData = table.Column<string>(nullable: true),
                    RequestIPAddress = table.Column<string>(maxLength: 100, nullable: true),
                    RequestMethod = table.Column<string>(maxLength: 100, nullable: true),
                    RequestTimestamp = table.Column<DateTime>(type: "datetime", nullable: true),
                    RequestUri = table.Column<string>(maxLength: 200, nullable: true),
                    ResponseContentType = table.Column<string>(maxLength: 100, nullable: true),
                    ResponseData = table.Column<string>(nullable: true),
                    ResponseStatusCode = table.Column<string>(maxLength: 100, nullable: true),
                    ResponseTimestamp = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LogMetaData", x => x.ID);
                });

            migrationBuilder.AddForeignKey(
                name: "FK__AssetHist__UserI__55BFB948",
                table: "AssetHistory",
                column: "UserID",
                principalTable: "Users",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK__Favourite__UserI__5006DFF2",
                table: "FavouriteAssets",
                column: "UserID",
                principalTable: "Users",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK__GradeDeta__Grade__2F9A1060",
                table: "GradeDetails",
                column: "GradeID",
                principalTable: "MstLevel",
                principalColumn: "LevelID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK__GradeDeta__Strea__32767D0B",
                table: "GradeDetails",
                column: "StreamID",
                principalTable: "MstStream",
                principalColumn: "StreamID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK__GradeDeta__Subje__318258D2",
                table: "GradeDetails",
                column: "SubjectID",
                principalTable: "MstSubject",
                principalColumn: "SubjectID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK__GradeDeta__Sylla__308E3499",
                table: "GradeDetails",
                column: "SyllabusID",
                principalTable: "MstSyllabus",
                principalColumn: "SyllabusID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK__Opportuni__Accou__16CE6296",
                table: "Opportunity",
                column: "AccountID",
                principalTable: "Account",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK__Opportuni__Oppor__1F63A897",
                table: "OpportunitySubscription",
                column: "OpportunityID",
                principalTable: "Opportunity",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK__Opportuni__Subsc__2057CCD0",
                table: "OpportunitySubscription",
                column: "SubscriptionID",
                principalTable: "Subscription",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK__PackageDe__Grade__2BC97F7C",
                table: "PackageDetails",
                column: "GradeID",
                principalTable: "MstLevel",
                principalColumn: "LevelID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK__PackageDe__Langu__2AD55B43",
                table: "PackageDetails",
                column: "LanguageID",
                principalTable: "MstLanguage",
                principalColumn: "LanguageID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK__PackageDe__Packa__29E1370A",
                table: "PackageDetails",
                column: "PackageID",
                principalTable: "ContentPackage",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK__Subscript__Packa__2610A626",
                table: "SubscriptionPackage",
                column: "PackageID",
                principalTable: "ContentPackage",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK__Subscript__Subsc__251C81ED",
                table: "SubscriptionPackage",
                column: "SubscriptionID",
                principalTable: "Subscription",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK__UserAccou__Accou__3BFFE745",
                table: "UserAccount",
                column: "AccountID",
                principalTable: "Account",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK__UserAccou__UserI__3B0BC30C",
                table: "UserAccount",
                column: "UserID",
                principalTable: "Users",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK__UserAsset__UserI__4959E263",
                table: "UserAssets",
                column: "UserID",
                principalTable: "Users",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK__UserDetai__Grade__42ACE4D4",
                table: "UserDetails",
                column: "GradeID",
                principalTable: "MstLevel",
                principalColumn: "LevelID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK__UserDetai__Langu__41B8C09B",
                table: "UserDetails",
                column: "LanguageID",
                principalTable: "MstLanguage",
                principalColumn: "LanguageID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK__UserDetai__Strea__4589517F",
                table: "UserDetails",
                column: "StreamID",
                principalTable: "MstStream",
                principalColumn: "StreamID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK__UserDetai__Subje__44952D46",
                table: "UserDetails",
                column: "SubjectID",
                principalTable: "MstSubject",
                principalColumn: "SubjectID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK__UserDetai__Sylla__43A1090D",
                table: "UserDetails",
                column: "SyllabusID",
                principalTable: "MstSyllabus",
                principalColumn: "SyllabusID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK__UserDetai__UserI__40C49C62",
                table: "UserDetails",
                column: "UserID",
                principalTable: "Users",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
