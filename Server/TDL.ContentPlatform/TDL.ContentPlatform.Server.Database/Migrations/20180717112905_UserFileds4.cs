﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TDL.ContentPlatform.Server.Database.Migrations
{
    public partial class UserFileds4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "NumberOfAttempts",
                table: "Asset",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<decimal>(
                name: "PercentageOfLabels",
                table: "Asset",
                nullable: false,
                defaultValue: 0m);

            var sp = @"CREATE PROCEDURE sp_GetXMLDetail
                        (
	                        @ModelID INT
                        )
                        AS
                        BEGIN
	                        SELECT M.EN AS '@EN', M.[NO] AS '@NO', M.AR AS '@AR', M.hidingposX AS '@hidingposX', M.hidingposY AS '@hidingposY', M.hidingposZ AS '@hidingposZ',
                            (
                                SELECT L.Face AS '@face', L.Number AS '@number',
                                    L.EN AS 'name/@EN', L.[NO] AS 'name/@NO', L.AR AS 'name/@AR',
                                    L.highlight AS 'color/@highlight', L.original AS 'color/@original',
                                    L.xPosition AS 'popup/position/@x', L.yPosition AS 'popup/position/@y', L.zPosition AS 'popup/position/@z',
                                    L.xRotation AS 'popup/rotation/@x', L.yRotation AS 'popup/rotation/@y', L.zRotation AS 'popup/rotation/@z',
                                    L.xScale AS 'popup/scale/@x', L.yScale AS 'popup/scale/@y', L.zScale AS 'popup/scale/@z'
                                FROM XMLContentLabel AS L where ModelID = M.ModelID
                                FOR XML PATH('label'), type
                            )
                            FROM XMLContentModel AS M
                            WHERE M.ModelID = @ModelID
                            FOR XML PATH('model'), ROOT('xml');
                        END";

            migrationBuilder.Sql(sp);

            var sp1 = @"CREATE PROCEDURE sp_GetSubtopicDetails
                        (
	                        @SubtopicName VARCHAR(100)
                        )
                        AS
                        BEGIN
	                        declare @str varchar(1000)

	                        ;WITH cteST (SubtopicID, SubtopicName, PSubtopicID)
	                        AS
	                          (
		                        SELECT SubtopicID, SubtopicName, PSubtopicID
		                        FROM MstSubtopic
		                        WHERE SubtopicName = @SubtopicName

		                        UNION ALL

		                        SELECT e.SubtopicID, e.SubtopicName, e.PSubtopicID
		                        FROM MstSubtopic e INNER JOIN cteST r
		                        ON e.SubtopicID = r.PSubtopicID
	                          )

	                        --select SubtopicName from cteST

	                        SELECT @str=coalesce(@str + ', ', '') + a.SubtopicName 
	                        FROM (SELECT SubtopicName from cteST) a
	
	                        --print @str 
	                        SELECT @str 
                        END";

            migrationBuilder.Sql(sp1);

            var sp2 = @"CREATE PROCEDURE sp_CreateLabels
                        (
	                        @AssetID INT,
	                        @ContentID INT
                        )
                        AS
                        BEGIN
	                        DECLARE @ID INT
	                        DECLARE @PrevID INT

	                        SELECT @ID = COALESCE(MAX(ID),0) FROM AssetContent a
	                        INNER JOIN XMLContentModel m ON a.ID = m.AssetContentID 
	                        WHERE AssetID = @AssetID AND ID < @ContentID

	                        --PRINT @ID
	                        IF (@ID != 0)
	                        BEGIN
		                        INSERT INTO XMLContentModel (AssetContentID,ModelName,EN,[NO],AR,hidingposX,hidingposY,hidingposZ,CreatedBy,CreatedOn,IsEnabled,IsDeleted)
		                        SELECT @ContentID,ModelName,EN,[NO],AR,hidingposX,hidingposY,hidingposZ,CreatedBy,GETDATE(),IsEnabled,IsDeleted 
		                        FROM XMLContentModel WHERE AssetContentID = @ID

		                        SELECT @PrevID = ModelID FROM XMLContentModel WHERE AssetContentID = @ID
		                        SELECT @ID = ModelID FROM XMLContentModel WHERE AssetContentID = @ContentID 
	
		                        --PRINT @ID

		                        INSERT INTO XMLContentLabel (ModelID,LabelName,Face,Number,EN,[NO],AR,highlight,original,xPosition,yPosition,zPosition,xRotation,
		                        yRotation,zRotation,xScale,yScale,zScale,CreatedBy,CreatedOn,IsEnabled,IsDeleted)
		                        SELECT @ID,LabelName,Face,Number,EN,[NO],AR,highlight,original,xPosition,yPosition,zPosition,xRotation,
		                        yRotation,zRotation,xScale,yScale,zScale,CreatedBy,GETDATE(),IsEnabled,IsDeleted
		                        FROM XMLContentLabel WHERE ModelID = @PrevID
	                        END
                        END";

            migrationBuilder.Sql(sp2);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "NumberOfAttempts",
                table: "Asset");

            migrationBuilder.DropColumn(
                name: "PercentageOfLabels",
                table: "Asset");
        }
    }
}
