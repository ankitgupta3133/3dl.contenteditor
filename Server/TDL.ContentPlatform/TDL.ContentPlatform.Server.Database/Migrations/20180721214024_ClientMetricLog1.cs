﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TDL.ContentPlatform.Server.Database.Migrations
{
    public partial class ClientMetricLog1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ClientMetricLog",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    MetricId = table.Column<Guid>(nullable: false),
                    ComputerName = table.Column<string>(nullable: true),
                    SessionId = table.Column<Guid>(nullable: false),
                    TimeStamp = table.Column<DateTime>(nullable: false),
                    MetricsType = table.Column<string>(nullable: true),
                    ClassName = table.Column<string>(nullable: true),
                    CallerMemberName = table.Column<string>(nullable: true),
                    CallerFilePath = table.Column<string>(nullable: true),
                    CallerLineNumber = table.Column<int>(nullable: false),
                    Action = table.Column<string>(nullable: true),
                    JsonParameters = table.Column<string>(nullable: true),
                    IsAuthenticated = table.Column<bool>(nullable: false),
                    Username = table.Column<string>(nullable: true),
                    UserId = table.Column<Guid>(nullable: false),
                    TransferredDttmUtc = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClientMetricLog", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ClientMetricLog");
        }
    }
}
