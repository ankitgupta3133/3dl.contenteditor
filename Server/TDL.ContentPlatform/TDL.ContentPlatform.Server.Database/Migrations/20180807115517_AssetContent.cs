﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TDL.ContentPlatform.Server.Database.Migrations
{
    public partial class AssetContent : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Platform",
                table: "AssetContent",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PublishFile",
                table: "AssetContent",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Platform",
                table: "AssetContent");

            migrationBuilder.DropColumn(
                name: "PublishFile",
                table: "AssetContent");
        }
    }
}
