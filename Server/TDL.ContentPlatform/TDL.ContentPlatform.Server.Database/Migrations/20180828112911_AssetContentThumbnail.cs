﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TDL.ContentPlatform.Server.Database.Migrations
{
    public partial class AssetContentThumbnail : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "PublishThumbnail",
                table: "AssetContent",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsQuizEnabled",
                table: "Asset",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PublishThumbnail",
                table: "AssetContent");

            migrationBuilder.DropColumn(
                name: "IsQuizEnabled",
                table: "Asset");
        }
    }
}
