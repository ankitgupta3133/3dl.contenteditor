﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TDL.ContentPlatform.Server.Database.Migrations
{
    public partial class dropActivitiesEntities : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PuzzleDetails");

            migrationBuilder.DropTable(
                name: "QuizDetails");

            migrationBuilder.DropTable(
                name: "QuizLabelDetails");

            migrationBuilder.DropTable(
                name: "Activity");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Activity",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ActivityType = table.Column<int>(nullable: false),
                    CreatedBy = table.Column<Guid>(nullable: true),
                    CreatedOn = table.Column<DateTime>(type: "datetime", nullable: true),
                    IsDeleted = table.Column<bool>(nullable: true),
                    IsEnabled = table.Column<bool>(nullable: true),
                    IsModelSpecific = table.Column<bool>(nullable: true),
                    LanguageId = table.Column<Guid>(nullable: true),
                    ModifiedBy = table.Column<Guid>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(type: "datetime", nullable: true),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Activity", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Activity_MstActivity_ActivityType",
                        column: x => x.ActivityType,
                        principalTable: "MstActivity",
                        principalColumn: "ActivityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PuzzleDetails",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AssetId = table.Column<int>(nullable: false),
                    CreatedBy = table.Column<Guid>(nullable: true),
                    CreatedOn = table.Column<DateTime>(type: "datetime", nullable: true),
                    IsDeleted = table.Column<bool>(nullable: true),
                    IsEnabled = table.Column<bool>(nullable: true),
                    Labels = table.Column<string>(nullable: true),
                    ModelId = table.Column<int>(nullable: true),
                    ModifiedBy = table.Column<Guid>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(type: "datetime", nullable: true),
                    NumberOfParts = table.Column<int>(nullable: false),
                    PuzzleId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PuzzleDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PuzzleDetails_Asset_AssetId",
                        column: x => x.AssetId,
                        principalTable: "Asset",
                        principalColumn: "AssetID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PuzzleDetails_Activity_PuzzleId",
                        column: x => x.PuzzleId,
                        principalTable: "Activity",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "QuizDetails",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AssetId = table.Column<int>(nullable: false),
                    CreatedBy = table.Column<Guid>(nullable: true),
                    CreatedOn = table.Column<DateTime>(type: "datetime", nullable: true),
                    IsDeleted = table.Column<bool>(nullable: true),
                    IsEnabled = table.Column<bool>(nullable: true),
                    Labels = table.Column<string>(nullable: true),
                    ModelId = table.Column<int>(nullable: true),
                    ModifiedBy = table.Column<Guid>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(type: "datetime", nullable: true),
                    NumberOfAttempts = table.Column<int>(nullable: false),
                    NumberOfLabels = table.Column<int>(nullable: false),
                    QuizId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QuizDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_QuizDetails_Asset_AssetId",
                        column: x => x.AssetId,
                        principalTable: "Asset",
                        principalColumn: "AssetID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_QuizDetails_Activity_QuizId",
                        column: x => x.QuizId,
                        principalTable: "Activity",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "QuizLabelDetails",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<Guid>(nullable: true),
                    CreatedOn = table.Column<DateTime>(type: "datetime", nullable: true),
                    IsDeleted = table.Column<bool>(nullable: true),
                    IsEnabled = table.Column<bool>(nullable: true),
                    LabelId = table.Column<int>(nullable: false),
                    ModifiedBy = table.Column<Guid>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(type: "datetime", nullable: true),
                    QuizId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QuizLabelDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_QuizLabelDetails_XMLContentLabel_LabelId",
                        column: x => x.LabelId,
                        principalTable: "XMLContentLabel",
                        principalColumn: "LabelID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_QuizLabelDetails_Activity_QuizId",
                        column: x => x.QuizId,
                        principalTable: "Activity",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Activity_ActivityType",
                table: "Activity",
                column: "ActivityType");

            migrationBuilder.CreateIndex(
                name: "IX_PuzzleDetails_AssetId",
                table: "PuzzleDetails",
                column: "AssetId");

            migrationBuilder.CreateIndex(
                name: "IX_PuzzleDetails_PuzzleId",
                table: "PuzzleDetails",
                column: "PuzzleId");

            migrationBuilder.CreateIndex(
                name: "IX_QuizDetails_AssetId",
                table: "QuizDetails",
                column: "AssetId");

            migrationBuilder.CreateIndex(
                name: "IX_QuizDetails_QuizId",
                table: "QuizDetails",
                column: "QuizId");

            migrationBuilder.CreateIndex(
                name: "IX_QuizLabelDetails_LabelId",
                table: "QuizLabelDetails",
                column: "LabelId");

            migrationBuilder.CreateIndex(
                name: "IX_QuizLabelDetails_QuizId",
                table: "QuizLabelDetails",
                column: "QuizId");
        }
    }
}
