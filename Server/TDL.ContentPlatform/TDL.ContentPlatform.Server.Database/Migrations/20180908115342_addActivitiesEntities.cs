﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TDL.ContentPlatform.Server.Database.Migrations
{
    public partial class addActivitiesEntities : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Activities",
                columns: table => new
                {
                    ActivityId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ActivityName = table.Column<string>(nullable: true),
                    ActivityType = table.Column<int>(nullable: false),
                    LanguageId = table.Column<Guid>(nullable: true),
                    IsModelSpecific = table.Column<bool>(nullable: true),
                    CreatedBy = table.Column<Guid>(nullable: true),
                    CreatedOn = table.Column<DateTime>(type: "datetime", nullable: true),
                    ModifiedBy = table.Column<Guid>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(type: "datetime", nullable: true),
                    IsEnabled = table.Column<bool>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Activities", x => x.ActivityId);
                    table.ForeignKey(
                        name: "FK_Activities_MstActivity_ActivityType",
                        column: x => x.ActivityType,
                        principalTable: "MstActivity",
                        principalColumn: "ActivityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PuzzleDetails",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    PuzzleId = table.Column<int>(nullable: false),
                    AssetId = table.Column<int>(nullable: false),
                    AssetContentId = table.Column<int>(nullable: false),
                    NumberOfParts = table.Column<int>(nullable: false),
                    CreatedBy = table.Column<Guid>(nullable: true),
                    CreatedOn = table.Column<DateTime>(type: "datetime", nullable: true),
                    ModifiedBy = table.Column<Guid>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(type: "datetime", nullable: true),
                    IsEnabled = table.Column<bool>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: true),
                    ModelId = table.Column<int>(nullable: false),
                    Labels = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PuzzleDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PuzzleDetails_AssetContent_AssetContentId",
                        column: x => x.AssetContentId,
                        principalTable: "AssetContent",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PuzzleDetails_Asset_AssetId",
                        column: x => x.AssetId,
                        principalTable: "Asset",
                        principalColumn: "AssetID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PuzzleDetails_XMLContentModel_ModelId",
                        column: x => x.ModelId,
                        principalTable: "XMLContentModel",
                        principalColumn: "ModelID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PuzzleDetails_Activities_PuzzleId",
                        column: x => x.PuzzleId,
                        principalTable: "Activities",
                        principalColumn: "ActivityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "QuizDetails",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    QuizId = table.Column<int>(nullable: false),
                    AssetId = table.Column<int>(nullable: false),
                    AssetContentId = table.Column<int>(nullable: false),
                    NumberOfAttempts = table.Column<int>(nullable: false),
                    NumberOfLabels = table.Column<int>(nullable: false),
                    CreatedBy = table.Column<Guid>(nullable: true),
                    CreatedOn = table.Column<DateTime>(type: "datetime", nullable: true),
                    ModifiedBy = table.Column<Guid>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(type: "datetime", nullable: true),
                    IsEnabled = table.Column<bool>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: true),
                    ModelId = table.Column<int>(nullable: false),
                    Labels = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QuizDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_QuizDetails_AssetContent_AssetContentId",
                        column: x => x.AssetContentId,
                        principalTable: "AssetContent",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_QuizDetails_Asset_AssetId",
                        column: x => x.AssetId,
                        principalTable: "Asset",
                        principalColumn: "AssetID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_QuizDetails_XMLContentModel_ModelId",
                        column: x => x.ModelId,
                        principalTable: "XMLContentModel",
                        principalColumn: "ModelID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_QuizDetails_Activities_QuizId",
                        column: x => x.QuizId,
                        principalTable: "Activities",
                        principalColumn: "ActivityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "QuizLabelDetails",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    QuizDetailId = table.Column<int>(nullable: false),
                    LabelId = table.Column<int>(nullable: false),
                    CreatedBy = table.Column<Guid>(nullable: true),
                    CreatedOn = table.Column<DateTime>(type: "datetime", nullable: true),
                    ModifiedBy = table.Column<Guid>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(type: "datetime", nullable: true),
                    IsEnabled = table.Column<bool>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QuizLabelDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_QuizLabelDetails_XMLContentLabel_LabelId",
                        column: x => x.LabelId,
                        principalTable: "XMLContentLabel",
                        principalColumn: "LabelID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_QuizLabelDetails_QuizDetails_QuizDetailId",
                        column: x => x.QuizDetailId,
                        principalTable: "QuizDetails",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Activities_ActivityType",
                table: "Activities",
                column: "ActivityType");

            migrationBuilder.CreateIndex(
                name: "IX_PuzzleDetails_AssetContentId",
                table: "PuzzleDetails",
                column: "AssetContentId");

            migrationBuilder.CreateIndex(
                name: "IX_PuzzleDetails_AssetId",
                table: "PuzzleDetails",
                column: "AssetId");

            migrationBuilder.CreateIndex(
                name: "IX_PuzzleDetails_ModelId",
                table: "PuzzleDetails",
                column: "ModelId");

            migrationBuilder.CreateIndex(
                name: "IX_PuzzleDetails_PuzzleId",
                table: "PuzzleDetails",
                column: "PuzzleId");

            migrationBuilder.CreateIndex(
                name: "IX_QuizDetails_AssetContentId",
                table: "QuizDetails",
                column: "AssetContentId");

            migrationBuilder.CreateIndex(
                name: "IX_QuizDetails_AssetId",
                table: "QuizDetails",
                column: "AssetId");

            migrationBuilder.CreateIndex(
                name: "IX_QuizDetails_ModelId",
                table: "QuizDetails",
                column: "ModelId");

            migrationBuilder.CreateIndex(
                name: "IX_QuizDetails_QuizId",
                table: "QuizDetails",
                column: "QuizId");

            migrationBuilder.CreateIndex(
                name: "IX_QuizLabelDetails_LabelId",
                table: "QuizLabelDetails",
                column: "LabelId");

            migrationBuilder.CreateIndex(
                name: "IX_QuizLabelDetails_QuizDetailId",
                table: "QuizLabelDetails",
                column: "QuizDetailId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PuzzleDetails");

            migrationBuilder.DropTable(
                name: "QuizLabelDetails");

            migrationBuilder.DropTable(
                name: "QuizDetails");

            migrationBuilder.DropTable(
                name: "Activities");
        }
    }
}
