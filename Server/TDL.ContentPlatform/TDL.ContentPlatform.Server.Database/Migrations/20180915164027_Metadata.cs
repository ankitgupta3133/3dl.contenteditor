﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TDL.ContentPlatform.Server.Database.Migrations
{
    public partial class Metadata : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
         

            migrationBuilder.CreateTable(
                name: "AssetContentMetadata",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AssetContentID = table.Column<int>(nullable: true),
                    IsEnabled = table.Column<bool>(nullable: true, defaultValueSql: "((0))"),
                    IsDeleted = table.Column<bool>(nullable: true, defaultValueSql: "((0))"),
                    CreatedBy = table.Column<Guid>(nullable: true),
                    CreatedOn = table.Column<DateTime>(type: "datetime", nullable: true),
                    ModifiedBy = table.Column<Guid>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(type: "datetime", nullable: true),
                    Type = table.Column<string>(maxLength: 50, nullable: true),
                    Json = table.Column<string>(type: "varchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AssetContentMetadata", x => x.ID);
                    table.ForeignKey(
                        name: "FK__AssetCont__Asset__208CD6FF",
                        column: x => x.AssetContentID,
                        principalTable: "AssetContent",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AssetContentDetail_AssetContentID_IsDeleted",
                table: "AssetContentDetail",
                columns: new[] { "AssetContentID", "IsDeleted" });

            migrationBuilder.CreateIndex(
                name: "IX_AssetContentMetadata_AssetContentID_IsDeleted",
                table: "AssetContentMetadata",
                columns: new[] { "AssetContentID", "IsDeleted" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AssetContentMetadata");

            migrationBuilder.DropIndex(
                name: "IX_AssetContentDetail_AssetContentID_IsDeleted",
                table: "AssetContentDetail");

            migrationBuilder.CreateIndex(
                name: "IX_AssetContentDetail_AssetContentID",
                table: "AssetContentDetail",
                column: "AssetContentID");
        }
    }
}
