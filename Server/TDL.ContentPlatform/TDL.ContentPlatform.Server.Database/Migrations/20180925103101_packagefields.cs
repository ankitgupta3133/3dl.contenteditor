﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TDL.ContentPlatform.Server.Database.Migrations
{
    public partial class packagefields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            var sp = @"CREATE PROCEDURE sp_GetPackageDetail
                        AS
                        BEGIN
	                        select p.new_name as Package, ISNULL(l.new_name,'') as [Language], ISNULL(g.new_name,'') as Grade, ISNULL(s.new_name,'') as Syllabus, 
	                        ISNULL(st.new_name,'') as Stream, ISNULL(su.new_name,'') as [Subject], ISNULL(t.new_name,'') as Topic, ISNULL(sub.new_name,'') as [Subtopic]
	                        FROM [cs__new_package] p left outer join [cs__new_package_language] pl on p.Id = pl.new_packageid
	                        left outer join [cs__new_language] l on pl.new_languageid = l.Id
	                        left outer join [cs__new_package_grade] pg on p.Id = pg.new_packageid
	                        left outer join [cs__new_grade] g on pg.new_gradeid = g.Id
	                        left outer join [cs__new_grade_syllabus] gs on g.Id = gs.new_gradeid
	                        left outer join [cs__new_syllabus] s on gs.new_syllabusid = s.Id
	                        left outer join [cs__new_grade_stream] gst on g.Id = gst.new_gradeid
	                        left outer join [cs__new_stream] st on gst.new_streamid = st.Id
	                        left outer join [cs__new_grade_subject] gsu on g.Id = gsu.new_gradeid
	                        left outer join [cs__new_subject] su on gsu.new_subjectid = su.Id
	                        left outer join [cs__new_subject_topic] sut on su.Id = sut.new_subjectid
	                        left outer join [cs__new_topic] t on sut.new_topicid = t.Id
	                        left outer join [cs__new_topic_subtopic] tsub on t.Id = tsub.new_topicid
	                        left outer join [cs__new_subtopic] sub on tsub.new_subtopicid = sub.Id
                        END";

            migrationBuilder.Sql(sp);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
