﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TDL.ContentPlatform.Server.Database.Migrations
{
    public partial class assetfields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            var sp = @"CREATE PROCEDURE sp_GetAssetDetails
                        AS
                        BEGIN
	                        Select a.AssetID, a.AssetName, ISNULL(p.new_name, '') as Package, ISNULL(l.new_name, '') as [Language], ISNULL(g.new_name, '') as Grade, ISNULL(s.new_name, '') as Syllabus, 
	                        ISNULL(st.new_name, '') as Stream, ISNULL(su.new_name, '') as [Subject], ISNULL(t.new_name, '') as Topic, ISNULL(sb.new_name, '') as [Subtopic], a.Status, a.[Type],
	                        pl.new_name as [Platform], IIF(ac.IsEnabled = 1, 'Published', 'Unpublished') as [PublishStatus]
	                        from Asset a inner join AssetDetails ad on a.AssetId = ad.AssetID
	                        left outer join [cs__new_package] p on ad.PackageId = p.Id
	                        left outer join [cs__new_language] l on ad.languageId=l.Id
	                        left outer join [cs__new_grade] g on ad.GradeId=g.Id
	                        left outer join [cs__new_syllabus] s on ad.SyllabusId=s.Id
	                        left outer join [cs__new_stream] st on ad.StreamId=st.Id
	                        left outer join [cs__new_subject] su on ad.SubjectId=su.Id
	                        left outer join [cs__new_topic] t on ad.TopicId=t.Id
	                        left outer join [cs__new_subtopic] sb on ad.SubtopicId=sb.Id
	                        inner join AssetContent ac on ad.AssetID = ac.AssetID
	                        inner join [cs__new_platform] pl on ac.Platform = pl.Id
	                        where a.IsDeleted=0 and ad.IsDeleted=0 and ac.IsDeleted=0 
	                        and ac.Id not in (Select ac2.Id from assetcontent ac1 inner join assetcontent ac2 on ac1.assetid = ac2.assetid and ac1.platform = ac2.platform where ac1.isenabled=1 and ac2.isenabled=0)
	                        order by AssetName
                        END";

            migrationBuilder.Sql(sp);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
