﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TDL.ContentPlatform.Server.Database.Migrations
{
    public partial class ElementFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "MstCompound",
                columns: table => new
                {
                    CompoundId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CompundName = table.Column<string>(maxLength: 100, nullable: true),
                    CompoundFormulae = table.Column<string>(maxLength: 100, nullable: true),
                    Discription = table.Column<string>(maxLength: 100, nullable: true),
                    IsDeleted = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MstCompound", x => x.CompoundId);
                });

            migrationBuilder.CreateTable(
                name: "MstElement",
                columns: table => new
                {
                    ElementId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ElementName = table.Column<string>(maxLength: 100, nullable: true),
                    ElementShortName = table.Column<string>(maxLength: 10, nullable: true),
                    AtomicNumber = table.Column<int>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MstElement", x => x.ElementId);
                });

            migrationBuilder.CreateTable(
                name: "CompoundElement",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CompoundId = table.Column<int>(nullable: false),
                    ElementId = table.Column<int>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CompoundElement", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CompoundElement_MstCompound_CompoundId",
                        column: x => x.CompoundId,
                        principalTable: "MstCompound",
                        principalColumn: "CompoundId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CompoundElement_MstElement_ElementId",
                        column: x => x.ElementId,
                        principalTable: "MstElement",
                        principalColumn: "ElementId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CompoundElement_CompoundId",
                table: "CompoundElement",
                column: "CompoundId");

            migrationBuilder.CreateIndex(
                name: "IX_CompoundElement_ElementId",
                table: "CompoundElement",
                column: "ElementId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CompoundElement");

            migrationBuilder.DropTable(
                name: "MstCompound");

            migrationBuilder.DropTable(
                name: "MstElement");
        }
    }
}
