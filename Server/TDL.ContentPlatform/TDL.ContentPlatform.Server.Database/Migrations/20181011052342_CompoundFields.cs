﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TDL.ContentPlatform.Server.Database.Migrations
{
    public partial class CompoundFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Discription",
                table: "MstCompound",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 100,
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BondedAssetId",
                table: "MstCompound",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Color",
                table: "MstCompound",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "CreatedBy",
                table: "MstCompound",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedOn",
                table: "MstCompound",
                type: "datetime",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsActive",
                table: "MstCompound",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "MaterialId",
                table: "MstCompound",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "ModifiedBy",
                table: "MstCompound",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ModifiedOn",
                table: "MstCompound",
                type: "datetime",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SphericalAssetId",
                table: "MstCompound",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "WikiLink",
                table: "MstCompound",
                maxLength: 100,
                nullable: true);

            migrationBuilder.CreateTable(
                name: "MstMaterial",
                columns: table => new
                {
                    MaterialId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    MaterialName = table.Column<string>(maxLength: 100, nullable: true),
                    CreatedBy = table.Column<Guid>(nullable: true),
                    CreatedOn = table.Column<DateTime>(type: "datetime", nullable: true),
                    ModifiedBy = table.Column<Guid>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(type: "datetime", nullable: true),
                    IsDeleted = table.Column<bool>(nullable: true),
                    IsActive = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MstMaterial", x => x.MaterialId);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MstCompound_BondedAssetId",
                table: "MstCompound",
                column: "BondedAssetId");

            migrationBuilder.CreateIndex(
                name: "IX_MstCompound_MaterialId",
                table: "MstCompound",
                column: "MaterialId");

            migrationBuilder.CreateIndex(
                name: "IX_MstCompound_SphericalAssetId",
                table: "MstCompound",
                column: "SphericalAssetId");

            migrationBuilder.AddForeignKey(
                name: "FK_MstCompound_Asset_BondedAssetId",
                table: "MstCompound",
                column: "BondedAssetId",
                principalTable: "Asset",
                principalColumn: "AssetID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_MstCompound_MstMaterial_MaterialId",
                table: "MstCompound",
                column: "MaterialId",
                principalTable: "MstMaterial",
                principalColumn: "MaterialId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_MstCompound_Asset_SphericalAssetId",
                table: "MstCompound",
                column: "SphericalAssetId",
                principalTable: "Asset",
                principalColumn: "AssetID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MstCompound_Asset_BondedAssetId",
                table: "MstCompound");

            migrationBuilder.DropForeignKey(
                name: "FK_MstCompound_MstMaterial_MaterialId",
                table: "MstCompound");

            migrationBuilder.DropForeignKey(
                name: "FK_MstCompound_Asset_SphericalAssetId",
                table: "MstCompound");

            migrationBuilder.DropTable(
                name: "MstMaterial");

            migrationBuilder.DropIndex(
                name: "IX_MstCompound_BondedAssetId",
                table: "MstCompound");

            migrationBuilder.DropIndex(
                name: "IX_MstCompound_MaterialId",
                table: "MstCompound");

            migrationBuilder.DropIndex(
                name: "IX_MstCompound_SphericalAssetId",
                table: "MstCompound");

            migrationBuilder.DropColumn(
                name: "BondedAssetId",
                table: "MstCompound");

            migrationBuilder.DropColumn(
                name: "Color",
                table: "MstCompound");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "MstCompound");

            migrationBuilder.DropColumn(
                name: "CreatedOn",
                table: "MstCompound");

            migrationBuilder.DropColumn(
                name: "IsActive",
                table: "MstCompound");

            migrationBuilder.DropColumn(
                name: "MaterialId",
                table: "MstCompound");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "MstCompound");

            migrationBuilder.DropColumn(
                name: "ModifiedOn",
                table: "MstCompound");

            migrationBuilder.DropColumn(
                name: "SphericalAssetId",
                table: "MstCompound");

            migrationBuilder.DropColumn(
                name: "WikiLink",
                table: "MstCompound");

            migrationBuilder.AlterColumn<string>(
                name: "Discription",
                table: "MstCompound",
                maxLength: 100,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
