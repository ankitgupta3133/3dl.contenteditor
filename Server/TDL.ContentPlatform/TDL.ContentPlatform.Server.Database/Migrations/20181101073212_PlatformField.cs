﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TDL.ContentPlatform.Server.Database.Migrations
{
    public partial class PlatformField : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ActivityPlatforms",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ActivityId = table.Column<int>(nullable: false),
                    PlatformId = table.Column<Guid>(nullable: false),
                    IsSelected = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ActivityPlatforms", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ActivityPlatforms_Activities_ActivityId",
                        column: x => x.ActivityId,
                        principalTable: "Activities",
                        principalColumn: "ActivityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ActivityNames_LanguageId",
                table: "ActivityNames",
                column: "LanguageId");

            migrationBuilder.CreateIndex(
                name: "IX_ActivityPlatforms_ActivityId",
                table: "ActivityPlatforms",
                column: "ActivityId");

            migrationBuilder.AddForeignKey(
                name: "FK_ActivityNames_cs__new_language_LanguageId",
                table: "ActivityNames",
                column: "LanguageId",
                principalTable: "cs__new_language",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ActivityNames_cs__new_language_LanguageId",
                table: "ActivityNames");

            migrationBuilder.DropTable(
                name: "ActivityPlatforms");

            migrationBuilder.DropIndex(
                name: "IX_ActivityNames_LanguageId",
                table: "ActivityNames");
        }
    }
}
