﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TDL.ContentPlatform.Server.Database.Migrations
{
    public partial class UserDetailsFields1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropIndex(
            //    name: "IX_XMLContentModel_AssetContentID",
            //    table: "XMLContentModel");

            //migrationBuilder.DropIndex(
            //    name: "IX_AssetContent_AssetID",
            //    table: "AssetContent");

            migrationBuilder.CreateTable(
                name: "UserDetails",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UserId = table.Column<Guid>(nullable: false),
                    SubscriptionId = table.Column<Guid>(nullable: false),
                    PackageId = table.Column<Guid>(nullable: false),
                    GradeId = table.Column<Guid>(nullable: false),
                    LanguageId = table.Column<Guid>(nullable: false),
                    SyllabusId = table.Column<Guid>(nullable: false),
                    StreamId = table.Column<Guid>(nullable: false),
                    SubjectId = table.Column<Guid>(nullable: false),
                    CreatedBy = table.Column<Guid>(nullable: true),
                    CreatedOn = table.Column<DateTime>(type: "datetime", nullable: true),
                    ModifiedBy = table.Column<Guid>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(type: "datetime", nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserDetails", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_XMLContentModel_AssetContentID",
                table: "XMLContentModel",
                column: "AssetContentID",
                unique: true,
                filter: "[AssetContentID] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_XMLContentLabel_LabelID_IsEnabled",
                table: "XMLContentLabel",
                columns: new[] { "LabelID", "IsEnabled" },
                unique: true,
                filter: "[IsEnabled] IS NOT NULL");

            //migrationBuilder.CreateIndex(
            //    name: "IX_AssetContent_AssetID_IsEnabled",
            //    table: "AssetContent",
            //    columns: new[] { "AssetID", "IsEnabled" },
            //    unique: true,
            //    filter: "[AssetID] IS NOT NULL AND [IsEnabled] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_UserDetails_UserId_IsActive",
                table: "UserDetails",
                columns: new[] { "UserId", "IsActive" },
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "UserDetails");

            migrationBuilder.DropIndex(
                name: "IX_XMLContentModel_AssetContentID",
                table: "XMLContentModel");

            migrationBuilder.DropIndex(
                name: "IX_XMLContentLabel_LabelID_IsEnabled",
                table: "XMLContentLabel");

            migrationBuilder.DropIndex(
                name: "IX_AssetContent_AssetID_IsEnabled",
                table: "AssetContent");

            migrationBuilder.CreateIndex(
                name: "IX_XMLContentModel_AssetContentID",
                table: "XMLContentModel",
                column: "AssetContentID");

            migrationBuilder.CreateIndex(
                name: "IX_AssetContent_AssetID",
                table: "AssetContent",
                column: "AssetID");
        }
    }
}
