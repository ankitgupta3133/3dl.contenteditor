﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TDL.ContentPlatform.Server.Database.Migrations
{
    public partial class UserDetailsFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            var sp = @"CREATE PROCEDURE sp_GetUserDetails
                    AS
                    BEGIN
	                    Select u.Id, u.Firstname as FirstName, ISNULL(u.Lastname, '') as LastName, u.UserName, u.Email, r.Name, ISNULL(a.name, '') as Account, 
	                    ISNULL(o.name, '') as Opportunity, s.new_name as Subscription, 1 as IsActive, 0 as IsDeleted
	                    from AspNetUsers u inner join AspNetUserRoles ur on u.Id = ur.UserId
	                    inner join AspNetRoles r on r.Id = ur.RoleId
	                    inner join [cs__account] a on a.accountId=u.Account
	                    inner join [cs__opportunity] o on o.parentaccountid = a.accountId
	                    inner join [cs__new_opportunity_subscription_mapping] os on os.new_opportunityid = o.opportunityid
	                    inner join [cs__new_subscription] s on s.Id = os.new_subscriptionid
	                    order by FirstName
                    END";

            migrationBuilder.Sql(sp);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
