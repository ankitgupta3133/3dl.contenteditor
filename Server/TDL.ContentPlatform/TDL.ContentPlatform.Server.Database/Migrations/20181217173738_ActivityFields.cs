﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TDL.ContentPlatform.Server.Database.Migrations
{
    public partial class ActivityFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Duration",
                table: "Activities",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "ActivityDetails",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ActivityId = table.Column<int>(nullable: false),
                    PackageId = table.Column<Guid>(nullable: false),
                    LanguageId = table.Column<Guid>(nullable: false),
                    GradeId = table.Column<Guid>(nullable: false),
                    SyllabusId = table.Column<Guid>(nullable: false),
                    StreamId = table.Column<Guid>(nullable: false),
                    SubjectId = table.Column<Guid>(nullable: false),
                    TopicId = table.Column<Guid>(nullable: false),
                    SubtopicId = table.Column<Guid>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ActivityDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ActivityDetails_Activities_ActivityId",
                        column: x => x.ActivityId,
                        principalTable: "Activities",
                        principalColumn: "ActivityId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ActivityDetails_cs__new_grade_GradeId",
                        column: x => x.GradeId,
                        principalTable: "cs__new_grade",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ActivityDetails_cs__new_language_LanguageId",
                        column: x => x.LanguageId,
                        principalTable: "cs__new_language",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ActivityDetails_cs__new_package_PackageId",
                        column: x => x.PackageId,
                        principalTable: "cs__new_package",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ActivityDetails_cs__new_stream_StreamId",
                        column: x => x.StreamId,
                        principalTable: "cs__new_stream",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ActivityDetails_cs__new_subject_SubjectId",
                        column: x => x.SubjectId,
                        principalTable: "cs__new_subject",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ActivityDetails_cs__new_subtopic_SubtopicId",
                        column: x => x.SubtopicId,
                        principalTable: "cs__new_subtopic",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ActivityDetails_cs__new_syllabus_SyllabusId",
                        column: x => x.SyllabusId,
                        principalTable: "cs__new_syllabus",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ActivityDetails_cs__new_topic_TopicId",
                        column: x => x.TopicId,
                        principalTable: "cs__new_topic",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ActivityDetails_ActivityId",
                table: "ActivityDetails",
                column: "ActivityId");

            migrationBuilder.CreateIndex(
                name: "IX_ActivityDetails_GradeId",
                table: "ActivityDetails",
                column: "GradeId");

            migrationBuilder.CreateIndex(
                name: "IX_ActivityDetails_LanguageId",
                table: "ActivityDetails",
                column: "LanguageId");

            migrationBuilder.CreateIndex(
                name: "IX_ActivityDetails_PackageId",
                table: "ActivityDetails",
                column: "PackageId");

            migrationBuilder.CreateIndex(
                name: "IX_ActivityDetails_StreamId",
                table: "ActivityDetails",
                column: "StreamId");

            migrationBuilder.CreateIndex(
                name: "IX_ActivityDetails_SubjectId",
                table: "ActivityDetails",
                column: "SubjectId");

            migrationBuilder.CreateIndex(
                name: "IX_ActivityDetails_SubtopicId",
                table: "ActivityDetails",
                column: "SubtopicId");

            migrationBuilder.CreateIndex(
                name: "IX_ActivityDetails_SyllabusId",
                table: "ActivityDetails",
                column: "SyllabusId");

            migrationBuilder.CreateIndex(
                name: "IX_ActivityDetails_TopicId",
                table: "ActivityDetails",
                column: "TopicId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ActivityDetails");

            migrationBuilder.DropColumn(
                name: "Duration",
                table: "Activities");
        }
    }
}
