﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TDL.ContentPlatform.Server.Database.Migrations
{
    public partial class ActivityFields1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ActivityDetails_cs__new_grade_GradeId",
                table: "ActivityDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_ActivityDetails_cs__new_language_LanguageId",
                table: "ActivityDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_ActivityDetails_cs__new_package_PackageId",
                table: "ActivityDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_ActivityDetails_cs__new_stream_StreamId",
                table: "ActivityDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_ActivityDetails_cs__new_subject_SubjectId",
                table: "ActivityDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_ActivityDetails_cs__new_subtopic_SubtopicId",
                table: "ActivityDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_ActivityDetails_cs__new_syllabus_SyllabusId",
                table: "ActivityDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_ActivityDetails_cs__new_topic_TopicId",
                table: "ActivityDetails");

            migrationBuilder.DropIndex(
                name: "IX_ActivityDetails_GradeId",
                table: "ActivityDetails");

            migrationBuilder.DropIndex(
                name: "IX_ActivityDetails_LanguageId",
                table: "ActivityDetails");

            migrationBuilder.DropIndex(
                name: "IX_ActivityDetails_PackageId",
                table: "ActivityDetails");

            migrationBuilder.DropIndex(
                name: "IX_ActivityDetails_StreamId",
                table: "ActivityDetails");

            migrationBuilder.DropIndex(
                name: "IX_ActivityDetails_SubjectId",
                table: "ActivityDetails");

            migrationBuilder.DropIndex(
                name: "IX_ActivityDetails_SubtopicId",
                table: "ActivityDetails");

            migrationBuilder.DropIndex(
                name: "IX_ActivityDetails_SyllabusId",
                table: "ActivityDetails");

            migrationBuilder.DropIndex(
                name: "IX_ActivityDetails_TopicId",
                table: "ActivityDetails");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_ActivityDetails_GradeId",
                table: "ActivityDetails",
                column: "GradeId");

            migrationBuilder.CreateIndex(
                name: "IX_ActivityDetails_LanguageId",
                table: "ActivityDetails",
                column: "LanguageId");

            migrationBuilder.CreateIndex(
                name: "IX_ActivityDetails_PackageId",
                table: "ActivityDetails",
                column: "PackageId");

            migrationBuilder.CreateIndex(
                name: "IX_ActivityDetails_StreamId",
                table: "ActivityDetails",
                column: "StreamId");

            migrationBuilder.CreateIndex(
                name: "IX_ActivityDetails_SubjectId",
                table: "ActivityDetails",
                column: "SubjectId");

            migrationBuilder.CreateIndex(
                name: "IX_ActivityDetails_SubtopicId",
                table: "ActivityDetails",
                column: "SubtopicId");

            migrationBuilder.CreateIndex(
                name: "IX_ActivityDetails_SyllabusId",
                table: "ActivityDetails",
                column: "SyllabusId");

            migrationBuilder.CreateIndex(
                name: "IX_ActivityDetails_TopicId",
                table: "ActivityDetails",
                column: "TopicId");

            migrationBuilder.AddForeignKey(
                name: "FK_ActivityDetails_cs__new_grade_GradeId",
                table: "ActivityDetails",
                column: "GradeId",
                principalTable: "cs__new_grade",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ActivityDetails_cs__new_language_LanguageId",
                table: "ActivityDetails",
                column: "LanguageId",
                principalTable: "cs__new_language",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ActivityDetails_cs__new_package_PackageId",
                table: "ActivityDetails",
                column: "PackageId",
                principalTable: "cs__new_package",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ActivityDetails_cs__new_stream_StreamId",
                table: "ActivityDetails",
                column: "StreamId",
                principalTable: "cs__new_stream",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ActivityDetails_cs__new_subject_SubjectId",
                table: "ActivityDetails",
                column: "SubjectId",
                principalTable: "cs__new_subject",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ActivityDetails_cs__new_subtopic_SubtopicId",
                table: "ActivityDetails",
                column: "SubtopicId",
                principalTable: "cs__new_subtopic",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ActivityDetails_cs__new_syllabus_SyllabusId",
                table: "ActivityDetails",
                column: "SyllabusId",
                principalTable: "cs__new_syllabus",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ActivityDetails_cs__new_topic_TopicId",
                table: "ActivityDetails",
                column: "TopicId",
                principalTable: "cs__new_topic",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
