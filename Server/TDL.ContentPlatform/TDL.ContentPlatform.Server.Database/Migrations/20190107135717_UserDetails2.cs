﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TDL.ContentPlatform.Server.Database.Migrations
{
    public partial class UserDetails2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsSubscriptionExpired",
                table: "UserDetails",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "dtSubscriptionEndDate",
                table: "UserDetails",
                type: "datetime",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "dtSubscriptionStartDate",
                table: "UserDetails",
                type: "datetime",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsSubscriptionExpired",
                table: "UserDetails");

            migrationBuilder.DropColumn(
                name: "dtSubscriptionEndDate",
                table: "UserDetails");

            migrationBuilder.DropColumn(
                name: "dtSubscriptionStartDate",
                table: "UserDetails");
        }
    }
}
