﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TDL.ContentPlatform.Server.Database.Migrations
{
    public partial class AppMasterFields1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsActive",
                table: "AppMaster",
                nullable: true);

            var sp = @"CREATE PROCEDURE sp_GetAppList
                AS
                BEGIN
                     Select DISTINCT a.Id, a.AppId, a.AppName, IIF(u.LastName is null, u.Firstname, u.FirstName + ' ' + u.LastName) as UserName, a.UserId, a.IsActive as IsActive, 
	                 a.IsDeleted as IsDeleted
                     from AppMaster a inner
                     join AspNetUsers u on a.UserId = u.Id
                     order by AppName
                END";

            migrationBuilder.Sql(sp);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsActive",
                table: "AppMaster");
        }
    }
}
