﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TDL.ContentPlatform.Server.Database.Migrations
{
    public partial class LanguageFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "DE",
                table: "XMLContentLabel",
                maxLength: 100,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DK",
                table: "XMLContentLabel",
                maxLength: 100,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ES",
                table: "XMLContentLabel",
                maxLength: 100,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FR",
                table: "XMLContentLabel",
                maxLength: 100,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SE",
                table: "XMLContentLabel",
                maxLength: 100,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DE",
                table: "XMLContentLabel");

            migrationBuilder.DropColumn(
                name: "DK",
                table: "XMLContentLabel");

            migrationBuilder.DropColumn(
                name: "ES",
                table: "XMLContentLabel");

            migrationBuilder.DropColumn(
                name: "FR",
                table: "XMLContentLabel");

            migrationBuilder.DropColumn(
                name: "SE",
                table: "XMLContentLabel");
        }
    }
}
