﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TDL.ContentPlatform.Server.Database.Migrations
{
    public partial class FeedbackFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AssetContentFeedback",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AssetContentId = table.Column<int>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false),
                    Feedback = table.Column<string>(nullable: true),
                    SubscriptionId = table.Column<Guid>(nullable: false),
                    PackageId = table.Column<Guid>(nullable: false),
                    LanguageId = table.Column<Guid>(nullable: false),
                    GradeId = table.Column<Guid>(nullable: false),
                    SyllabusId = table.Column<Guid>(nullable: false),
                    StreamId = table.Column<Guid>(nullable: false),
                    SubjectId = table.Column<Guid>(nullable: false),
                    TopicId = table.Column<Guid>(nullable: false),
                    SubtopicId = table.Column<Guid>(nullable: false),
                    PublishAttachment = table.Column<string>(nullable: true),
                    CreatedBy = table.Column<Guid>(nullable: true),
                    CreatedOn = table.Column<DateTime>(type: "datetime", nullable: true),
                    CreatedByName = table.Column<string>(nullable: true),
                    ModifiedBy = table.Column<Guid>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(type: "datetime", nullable: true),
                    ModifiedByName = table.Column<string>(nullable: true),
                    IsEnabled = table.Column<bool>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AssetContentFeedback", x => x.ID);
                    table.ForeignKey(
                        name: "FK_AssetContentFeedback_AssetContent_AssetContentId",
                        column: x => x.AssetContentId,
                        principalTable: "AssetContent",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AssetContentFeedback_cs__new_grade_GradeId",
                        column: x => x.GradeId,
                        principalTable: "cs__new_grade",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AssetContentFeedback_cs__new_language_LanguageId",
                        column: x => x.LanguageId,
                        principalTable: "cs__new_language",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AssetContentFeedback_cs__new_package_PackageId",
                        column: x => x.PackageId,
                        principalTable: "cs__new_package",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AssetContentFeedback_cs__new_stream_StreamId",
                        column: x => x.StreamId,
                        principalTable: "cs__new_stream",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AssetContentFeedback_cs__new_subject_SubjectId",
                        column: x => x.SubjectId,
                        principalTable: "cs__new_subject",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AssetContentFeedback_cs__new_subscription_SubscriptionId",
                        column: x => x.SubscriptionId,
                        principalTable: "cs__new_subscription",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AssetContentFeedback_cs__new_subtopic_SubtopicId",
                        column: x => x.SubtopicId,
                        principalTable: "cs__new_subtopic",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AssetContentFeedback_cs__new_syllabus_SyllabusId",
                        column: x => x.SyllabusId,
                        principalTable: "cs__new_syllabus",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AssetContentFeedback_cs__new_topic_TopicId",
                        column: x => x.TopicId,
                        principalTable: "cs__new_topic",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AssetContentFeedback_AssetContentId",
                table: "AssetContentFeedback",
                column: "AssetContentId");

            migrationBuilder.CreateIndex(
                name: "IX_AssetContentFeedback_GradeId",
                table: "AssetContentFeedback",
                column: "GradeId");

            migrationBuilder.CreateIndex(
                name: "IX_AssetContentFeedback_LanguageId",
                table: "AssetContentFeedback",
                column: "LanguageId");

            migrationBuilder.CreateIndex(
                name: "IX_AssetContentFeedback_PackageId",
                table: "AssetContentFeedback",
                column: "PackageId");

            migrationBuilder.CreateIndex(
                name: "IX_AssetContentFeedback_StreamId",
                table: "AssetContentFeedback",
                column: "StreamId");

            migrationBuilder.CreateIndex(
                name: "IX_AssetContentFeedback_SubjectId",
                table: "AssetContentFeedback",
                column: "SubjectId");

            migrationBuilder.CreateIndex(
                name: "IX_AssetContentFeedback_SubscriptionId",
                table: "AssetContentFeedback",
                column: "SubscriptionId");

            migrationBuilder.CreateIndex(
                name: "IX_AssetContentFeedback_SubtopicId",
                table: "AssetContentFeedback",
                column: "SubtopicId");

            migrationBuilder.CreateIndex(
                name: "IX_AssetContentFeedback_SyllabusId",
                table: "AssetContentFeedback",
                column: "SyllabusId");

            migrationBuilder.CreateIndex(
                name: "IX_AssetContentFeedback_TopicId",
                table: "AssetContentFeedback",
                column: "TopicId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AssetContentFeedback");
        }
    }
}
