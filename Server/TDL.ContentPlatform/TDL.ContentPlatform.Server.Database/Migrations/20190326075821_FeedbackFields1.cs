﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TDL.ContentPlatform.Server.Database.Migrations
{
    public partial class FeedbackFields1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AssetContentFeedback_cs__new_grade_GradeId",
                table: "AssetContentFeedback");

            migrationBuilder.DropForeignKey(
                name: "FK_AssetContentFeedback_cs__new_language_LanguageId",
                table: "AssetContentFeedback");

            migrationBuilder.DropForeignKey(
                name: "FK_AssetContentFeedback_cs__new_package_PackageId",
                table: "AssetContentFeedback");

            migrationBuilder.DropForeignKey(
                name: "FK_AssetContentFeedback_cs__new_stream_StreamId",
                table: "AssetContentFeedback");

            migrationBuilder.DropForeignKey(
                name: "FK_AssetContentFeedback_cs__new_subject_SubjectId",
                table: "AssetContentFeedback");

            migrationBuilder.DropForeignKey(
                name: "FK_AssetContentFeedback_cs__new_subscription_SubscriptionId",
                table: "AssetContentFeedback");

            migrationBuilder.DropForeignKey(
                name: "FK_AssetContentFeedback_cs__new_subtopic_SubtopicId",
                table: "AssetContentFeedback");

            migrationBuilder.DropForeignKey(
                name: "FK_AssetContentFeedback_cs__new_syllabus_SyllabusId",
                table: "AssetContentFeedback");

            migrationBuilder.DropForeignKey(
                name: "FK_AssetContentFeedback_cs__new_topic_TopicId",
                table: "AssetContentFeedback");

            migrationBuilder.AlterColumn<Guid>(
                name: "TopicId",
                table: "AssetContentFeedback",
                nullable: true,
                oldClrType: typeof(Guid));

            migrationBuilder.AlterColumn<Guid>(
                name: "SyllabusId",
                table: "AssetContentFeedback",
                nullable: true,
                oldClrType: typeof(Guid));

            migrationBuilder.AlterColumn<Guid>(
                name: "SubtopicId",
                table: "AssetContentFeedback",
                nullable: true,
                oldClrType: typeof(Guid));

            migrationBuilder.AlterColumn<Guid>(
                name: "SubscriptionId",
                table: "AssetContentFeedback",
                nullable: true,
                oldClrType: typeof(Guid));

            migrationBuilder.AlterColumn<Guid>(
                name: "SubjectId",
                table: "AssetContentFeedback",
                nullable: true,
                oldClrType: typeof(Guid));

            migrationBuilder.AlterColumn<Guid>(
                name: "StreamId",
                table: "AssetContentFeedback",
                nullable: true,
                oldClrType: typeof(Guid));

            migrationBuilder.AlterColumn<Guid>(
                name: "PackageId",
                table: "AssetContentFeedback",
                nullable: true,
                oldClrType: typeof(Guid));

            migrationBuilder.AlterColumn<Guid>(
                name: "LanguageId",
                table: "AssetContentFeedback",
                nullable: true,
                oldClrType: typeof(Guid));

            migrationBuilder.AlterColumn<Guid>(
                name: "GradeId",
                table: "AssetContentFeedback",
                nullable: true,
                oldClrType: typeof(Guid));

            migrationBuilder.AddForeignKey(
                name: "FK_AssetContentFeedback_cs__new_grade_GradeId",
                table: "AssetContentFeedback",
                column: "GradeId",
                principalTable: "cs__new_grade",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AssetContentFeedback_cs__new_language_LanguageId",
                table: "AssetContentFeedback",
                column: "LanguageId",
                principalTable: "cs__new_language",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AssetContentFeedback_cs__new_package_PackageId",
                table: "AssetContentFeedback",
                column: "PackageId",
                principalTable: "cs__new_package",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AssetContentFeedback_cs__new_stream_StreamId",
                table: "AssetContentFeedback",
                column: "StreamId",
                principalTable: "cs__new_stream",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AssetContentFeedback_cs__new_subject_SubjectId",
                table: "AssetContentFeedback",
                column: "SubjectId",
                principalTable: "cs__new_subject",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AssetContentFeedback_cs__new_subscription_SubscriptionId",
                table: "AssetContentFeedback",
                column: "SubscriptionId",
                principalTable: "cs__new_subscription",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AssetContentFeedback_cs__new_subtopic_SubtopicId",
                table: "AssetContentFeedback",
                column: "SubtopicId",
                principalTable: "cs__new_subtopic",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AssetContentFeedback_cs__new_syllabus_SyllabusId",
                table: "AssetContentFeedback",
                column: "SyllabusId",
                principalTable: "cs__new_syllabus",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AssetContentFeedback_cs__new_topic_TopicId",
                table: "AssetContentFeedback",
                column: "TopicId",
                principalTable: "cs__new_topic",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AssetContentFeedback_cs__new_grade_GradeId",
                table: "AssetContentFeedback");

            migrationBuilder.DropForeignKey(
                name: "FK_AssetContentFeedback_cs__new_language_LanguageId",
                table: "AssetContentFeedback");

            migrationBuilder.DropForeignKey(
                name: "FK_AssetContentFeedback_cs__new_package_PackageId",
                table: "AssetContentFeedback");

            migrationBuilder.DropForeignKey(
                name: "FK_AssetContentFeedback_cs__new_stream_StreamId",
                table: "AssetContentFeedback");

            migrationBuilder.DropForeignKey(
                name: "FK_AssetContentFeedback_cs__new_subject_SubjectId",
                table: "AssetContentFeedback");

            migrationBuilder.DropForeignKey(
                name: "FK_AssetContentFeedback_cs__new_subscription_SubscriptionId",
                table: "AssetContentFeedback");

            migrationBuilder.DropForeignKey(
                name: "FK_AssetContentFeedback_cs__new_subtopic_SubtopicId",
                table: "AssetContentFeedback");

            migrationBuilder.DropForeignKey(
                name: "FK_AssetContentFeedback_cs__new_syllabus_SyllabusId",
                table: "AssetContentFeedback");

            migrationBuilder.DropForeignKey(
                name: "FK_AssetContentFeedback_cs__new_topic_TopicId",
                table: "AssetContentFeedback");

            migrationBuilder.AlterColumn<Guid>(
                name: "TopicId",
                table: "AssetContentFeedback",
                nullable: false,
                oldClrType: typeof(Guid),
                oldNullable: true);

            migrationBuilder.AlterColumn<Guid>(
                name: "SyllabusId",
                table: "AssetContentFeedback",
                nullable: false,
                oldClrType: typeof(Guid),
                oldNullable: true);

            migrationBuilder.AlterColumn<Guid>(
                name: "SubtopicId",
                table: "AssetContentFeedback",
                nullable: false,
                oldClrType: typeof(Guid),
                oldNullable: true);

            migrationBuilder.AlterColumn<Guid>(
                name: "SubscriptionId",
                table: "AssetContentFeedback",
                nullable: false,
                oldClrType: typeof(Guid),
                oldNullable: true);

            migrationBuilder.AlterColumn<Guid>(
                name: "SubjectId",
                table: "AssetContentFeedback",
                nullable: false,
                oldClrType: typeof(Guid),
                oldNullable: true);

            migrationBuilder.AlterColumn<Guid>(
                name: "StreamId",
                table: "AssetContentFeedback",
                nullable: false,
                oldClrType: typeof(Guid),
                oldNullable: true);

            migrationBuilder.AlterColumn<Guid>(
                name: "PackageId",
                table: "AssetContentFeedback",
                nullable: false,
                oldClrType: typeof(Guid),
                oldNullable: true);

            migrationBuilder.AlterColumn<Guid>(
                name: "LanguageId",
                table: "AssetContentFeedback",
                nullable: false,
                oldClrType: typeof(Guid),
                oldNullable: true);

            migrationBuilder.AlterColumn<Guid>(
                name: "GradeId",
                table: "AssetContentFeedback",
                nullable: false,
                oldClrType: typeof(Guid),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_AssetContentFeedback_cs__new_grade_GradeId",
                table: "AssetContentFeedback",
                column: "GradeId",
                principalTable: "cs__new_grade",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_AssetContentFeedback_cs__new_language_LanguageId",
                table: "AssetContentFeedback",
                column: "LanguageId",
                principalTable: "cs__new_language",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_AssetContentFeedback_cs__new_package_PackageId",
                table: "AssetContentFeedback",
                column: "PackageId",
                principalTable: "cs__new_package",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_AssetContentFeedback_cs__new_stream_StreamId",
                table: "AssetContentFeedback",
                column: "StreamId",
                principalTable: "cs__new_stream",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_AssetContentFeedback_cs__new_subject_SubjectId",
                table: "AssetContentFeedback",
                column: "SubjectId",
                principalTable: "cs__new_subject",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_AssetContentFeedback_cs__new_subscription_SubscriptionId",
                table: "AssetContentFeedback",
                column: "SubscriptionId",
                principalTable: "cs__new_subscription",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_AssetContentFeedback_cs__new_subtopic_SubtopicId",
                table: "AssetContentFeedback",
                column: "SubtopicId",
                principalTable: "cs__new_subtopic",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_AssetContentFeedback_cs__new_syllabus_SyllabusId",
                table: "AssetContentFeedback",
                column: "SyllabusId",
                principalTable: "cs__new_syllabus",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_AssetContentFeedback_cs__new_topic_TopicId",
                table: "AssetContentFeedback",
                column: "TopicId",
                principalTable: "cs__new_topic",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
