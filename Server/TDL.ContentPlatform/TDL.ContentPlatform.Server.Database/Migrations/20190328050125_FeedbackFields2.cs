﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TDL.ContentPlatform.Server.Database.Migrations
{
    public partial class FeedbackFields2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "StatusId",
                table: "AssetContentFeedback",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_AssetContentFeedback_StatusId",
                table: "AssetContentFeedback",
                column: "StatusId");

            migrationBuilder.AddForeignKey(
                name: "FK_AssetContentFeedback_MstStatus_StatusId",
                table: "AssetContentFeedback",
                column: "StatusId",
                principalTable: "MstStatus",
                principalColumn: "StatusID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AssetContentFeedback_MstStatus_StatusId",
                table: "AssetContentFeedback");

            migrationBuilder.DropIndex(
                name: "IX_AssetContentFeedback_StatusId",
                table: "AssetContentFeedback");

            migrationBuilder.DropColumn(
                name: "StatusId",
                table: "AssetContentFeedback");
        }
    }
}
