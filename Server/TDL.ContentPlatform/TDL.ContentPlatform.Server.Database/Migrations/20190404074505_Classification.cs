﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TDL.ContentPlatform.Server.Database.Migrations
{
    public partial class Classification : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ClassificationDetails",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ClassificationId = table.Column<int>(nullable: false),
                    PropertyId = table.Column<int>(nullable: false),
                    CreatedBy = table.Column<Guid>(nullable: true),
                    CreatedOn = table.Column<DateTime>(type: "datetime", nullable: true),
                    ModifiedBy = table.Column<Guid>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(type: "datetime", nullable: true),
                    IsEnabled = table.Column<bool>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClassificationDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ClassificationDetails_Activities_ClassificationId",
                        column: x => x.ClassificationId,
                        principalTable: "Activities",
                        principalColumn: "ActivityId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ClassificationDetails_MstProperty_PropertyId",
                        column: x => x.PropertyId,
                        principalTable: "MstProperty",
                        principalColumn: "PropertyID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ClassificationAssets",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ClassificationDetailId = table.Column<int>(nullable: false),
                    AssetId = table.Column<int>(nullable: false),
                    AssetContentId = table.Column<int>(nullable: false),
                    CreatedBy = table.Column<Guid>(nullable: true),
                    CreatedOn = table.Column<DateTime>(type: "datetime", nullable: true),
                    ModifiedBy = table.Column<Guid>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(type: "datetime", nullable: true),
                    IsEnabled = table.Column<bool>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClassificationAssets", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ClassificationAssets_AssetContent_AssetContentId",
                        column: x => x.AssetContentId,
                        principalTable: "AssetContent",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ClassificationAssets_Asset_AssetId",
                        column: x => x.AssetId,
                        principalTable: "Asset",
                        principalColumn: "AssetID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ClassificationAssets_ClassificationDetails_ClassificationDetailId",
                        column: x => x.ClassificationDetailId,
                        principalTable: "ClassificationDetails",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ClassificationAssets_AssetContentId",
                table: "ClassificationAssets",
                column: "AssetContentId");

            migrationBuilder.CreateIndex(
                name: "IX_ClassificationAssets_AssetId",
                table: "ClassificationAssets",
                column: "AssetId");

            migrationBuilder.CreateIndex(
                name: "IX_ClassificationAssets_ClassificationDetailId",
                table: "ClassificationAssets",
                column: "ClassificationDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_ClassificationDetails_ClassificationId",
                table: "ClassificationDetails",
                column: "ClassificationId");

            migrationBuilder.CreateIndex(
                name: "IX_ClassificationDetails_PropertyId",
                table: "ClassificationDetails",
                column: "PropertyId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ClassificationAssets");

            migrationBuilder.DropTable(
                name: "ClassificationDetails");
        }
    }
}
