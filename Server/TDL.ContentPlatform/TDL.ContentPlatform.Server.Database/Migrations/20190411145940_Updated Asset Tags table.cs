﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TDL.ContentPlatform.Server.Database.Migrations
{
    public partial class UpdatedAssetTagstable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AssetTags_MstTagNames_MstTagNameId",
                table: "AssetTags");

            migrationBuilder.RenameColumn(
                name: "MstTagNameId",
                table: "AssetTags",
                newName: "MstTagId");

            migrationBuilder.RenameIndex(
                name: "IX_AssetTags_MstTagNameId",
                table: "AssetTags",
                newName: "IX_AssetTags_MstTagId");

            migrationBuilder.AddForeignKey(
                name: "FK_AssetTags_MstTag_MstTagId",
                table: "AssetTags",
                column: "MstTagId",
                principalTable: "MstTag",
                principalColumn: "TagID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AssetTags_MstTag_MstTagId",
                table: "AssetTags");

            migrationBuilder.RenameColumn(
                name: "MstTagId",
                table: "AssetTags",
                newName: "MstTagNameId");

            migrationBuilder.RenameIndex(
                name: "IX_AssetTags_MstTagId",
                table: "AssetTags",
                newName: "IX_AssetTags_MstTagNameId");

            migrationBuilder.AddForeignKey(
                name: "FK_AssetTags_MstTagNames_MstTagNameId",
                table: "AssetTags",
                column: "MstTagNameId",
                principalTable: "MstTagNames",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
