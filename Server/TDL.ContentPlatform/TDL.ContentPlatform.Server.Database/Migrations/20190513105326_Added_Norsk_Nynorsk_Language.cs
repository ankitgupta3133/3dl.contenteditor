﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TDL.ContentPlatform.Server.Database.Migrations
{
    public partial class Added_Norsk_Nynorsk_Language : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "NN",
                table: "XMLContentModel",
                maxLength: 100,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NN",
                table: "XMLContentLabel",
                maxLength: 100,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "NN",
                table: "XMLContentModel");

            migrationBuilder.DropColumn(
                name: "NN",
                table: "XMLContentLabel");
        }
    }
}
