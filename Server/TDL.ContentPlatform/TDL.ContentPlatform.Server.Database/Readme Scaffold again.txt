﻿To scaffold database again:
scaffold-dbcontext -OutputDir Tables -Connection "Server=3dl-p-sql01.database.windows.net;Database=3dl-p-Sql-Content1;persist security info=True;user id=dbadmin;password=5tKX)7g?aK__x;MultipleActiveResultSets=True;" -Provider "Microsoft.EntityFrameworkCore.SqlServer" -DataAnnotations

It is recommended to do this in a separate project called TDL.ContentPlatform.Server.Database and copy files over

All models can be safely overwritten. They are by default partial classes, so all extensions (relations) are added by their _Relations counterpart.
For example Asset.cs has Asset_Relations.cs.
