﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    public partial class Account
    {
        public Account()
        {
            Opportunity = new HashSet<Opportunity>();
            UserAccount = new HashSet<UserAccount>();
        }

        [Column("ID")]
        public Guid Id { get; set; }
        [StringLength(100)]
        public string Name { get; set; }
        [Column("ParentAccountID")]
        public Guid? ParentAccountId { get; set; }
        public Guid? CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedOn { get; set; }
        public Guid? ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedOn { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDeleted { get; set; }

        [InverseProperty("Account")]
        public ICollection<Opportunity> Opportunity { get; set; }
        [InverseProperty("Account")]
        public ICollection<UserAccount> UserAccount { get; set; }
    }
}
