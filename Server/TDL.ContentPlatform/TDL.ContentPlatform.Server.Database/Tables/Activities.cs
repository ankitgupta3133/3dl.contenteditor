﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    public partial class Activities
    {
        [Key]
        public int ActivityId { get; set; }
        public string ActivityName { get; set; }
        public int ActivityType { get; set; }
        public int Duration { get; set; }
        public Guid? LanguageId { get; set; }
        public bool? IsModelSpecific { get; set; }
        public Guid? CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedOn { get; set; }
        public Guid? ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedOn { get; set; }
        public bool? IsEnabled { get; set; }
        public bool? IsDeleted { get; set; }
        [ForeignKey("ActivityType")]
        [InverseProperty("Activity")]
        public MstActivity MstActivity { get; set; }
        [InverseProperty("Activity")]
        public ICollection<ActivityNames> Names { get; set; }
        [InverseProperty("Activity")]
        public ICollection<ActivityPlatforms> Platforms { get; set; }
        [InverseProperty("Activity")]
        public ICollection<ActivityDetails> Details { get; set; }
        [InverseProperty("Activity")]
        public ICollection<QuizDetails> QuizDetails { get; set; }
        [InverseProperty("Activity")]
        public ICollection<PuzzleDetails> PuzzleDetails { get; set; }
        [InverseProperty("Activity")]
        public ICollection<ClassificationDetails> ClassificationDetails { get; set; }
        public Activities()
        {
            QuizDetails = new HashSet<QuizDetails>();
            PuzzleDetails = new HashSet<PuzzleDetails>();
            ClassificationDetails = new HashSet<ClassificationDetails>();
            Names = new HashSet<ActivityNames>();
            Platforms = new HashSet<ActivityPlatforms>();
            Details = new HashSet<ActivityDetails>();
        }
    }
}
