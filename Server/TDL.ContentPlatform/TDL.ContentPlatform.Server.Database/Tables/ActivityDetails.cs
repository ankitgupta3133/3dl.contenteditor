﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    public partial class ActivityDetails
    {
        public int Id { get; set; }
        public int ActivityId { get; set; }
        public Guid PackageId { get; set; }
        public Guid LanguageId { get; set; }
        public Guid GradeId { get; set; }
        public Guid SyllabusId { get; set; }
        public Guid StreamId { get; set; }
        public Guid SubjectId { get; set; }
        public Guid TopicId { get; set; }
        public Guid SubtopicId { get; set; }
        public bool IsDeleted { get; set; }

        [ForeignKey("ActivityId")]
        [InverseProperty("Details")]
        public Activities Activity { get; set; }
    }
}
