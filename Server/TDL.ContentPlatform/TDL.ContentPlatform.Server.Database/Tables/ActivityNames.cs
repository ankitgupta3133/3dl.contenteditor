﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    public partial class ActivityNames
    {
        public int Id { get; set; }
        public int ActivityId { get; set; }
        public string Name { get; set; }
        public Guid LanguageId { get; set; }
        public bool IsDeleted { get; set; }

        [ForeignKey("ActivityId")]
        [InverseProperty("Names")]
        public Activities Activity { get; set; }
    }
    public partial class ActivityNames
    {
        public CsNewLanguage Language { get; set; }
    }
}
