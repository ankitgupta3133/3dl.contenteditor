﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    public partial class ActivityPlatforms
    {
        public int Id { get; set; }
        public int ActivityId { get; set; }
        public Guid PlatformId { get; set; }
        public bool IsSelected { get; set; }
        [ForeignKey("ActivityId")]
        [InverseProperty("Platforms")]
        public Activities Activity { get; set; }
    }
}
