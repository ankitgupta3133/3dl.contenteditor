﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    public class ApkDetail
    {
        [Key]
        public int ApkId { get; set; }
        public string ApkName { get; set; }        
        public string ApkDescription { get; set; }
        public int ApkVersion { get; set; }
        public string PublishApk { get; set; }
        public bool IsMandatory { get; set; }
        public Guid? CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedOn { get; set; }
        public Guid? ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedOn { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDeleted { get; set; }
    }
}
