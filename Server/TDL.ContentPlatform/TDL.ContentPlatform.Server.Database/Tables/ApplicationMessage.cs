﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    public partial class ApplicationMessage
    {
        public int Id { get; set; }
        [StringLength(100)]
        public string MessageCode { get; set; }
        public byte MessageType { get; set; }
        public Guid LanguageId { get; set; }
        [StringLength(2000)]
        public string Message { get; set; }
        public bool IsDeleted { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedOn { get; set; }        
    }
}
