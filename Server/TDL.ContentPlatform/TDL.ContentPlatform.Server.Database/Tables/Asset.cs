﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
   public partial class Asset
    {
        public Asset()
        {
            AssetContent = new HashSet<AssetContent>();
            AssetDetails = new HashSet<AssetDetails>();
            AssetGrades = new HashSet<AssetGrades>();
            AssetHistory = new HashSet<AssetHistory>();
            AssetLanguages = new HashSet<AssetLanguages>();
            AssetNames = new HashSet<AssetNames>();
            AssetStreams = new HashSet<AssetStreams>();
            AssetSubTopics = new HashSet<AssetSubTopics>();
            AssetSubjects = new HashSet<AssetSubjects>();
            AssetSyllabus = new HashSet<AssetSyllabus>();
            AssetTopics = new HashSet<AssetTopics>();
            FavouriteAssets = new HashSet<FavouriteAssets>();
            UserAssets = new HashSet<UserAssets>();
            QuizDetails = new HashSet<QuizDetails>();
            PuzzleDetails = new HashSet<PuzzleDetails>();
            AssetRelations = new HashSet<AssetRelations>();
            BondedCompounds = new HashSet<MstCompound>();
            SphericalCompounds = new HashSet<MstCompound>();
            ClassificationAssets = new HashSet<ClassificationAssets>();
            AssetTags = new HashSet<AssetTags>();
            AssetProperties = new HashSet<AssetProperties>();
        }

        [Column("AssetID")]
        public int AssetId { get; set; }
        [StringLength(100)]
        public string AssetName { get; set; }
        [StringLength(100)]
        public string FileName { get; set; }
        [StringLength(100)]
        public string Type { get; set; }
        [StringLength(100)]
        public string Language { get; set; }
        [StringLength(100)]
        public string Status { get; set; }
        [StringLength(100)]
        public string Subject { get; set; }
        [StringLength(100)]
        public string Topic { get; set; }
        [StringLength(100)]
        public string Subtopic { get; set; }
        [StringLength(100)]
        public string Tags { get; set; }
        [StringLength(200)]
        public string VimeoUrl { get; set; }
        public Guid? CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedOn { get; set; }
        public Guid? ModifiedBy { get; set; }
        [StringLength(100)]
        public string ModifiedByName { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedOn { get; set; }
        public bool? IsDeleted { get; set; }
        public bool? IsActive { get; set; }
        [StringLength(100)]
        public string Level { get; set; }
        [StringLength(200)]
        public string FilePath { get; set; }
        [StringLength(50)]
        public string FileSize { get; set; }
        public bool? IsDirectory { get; set; }
        [StringLength(200)]
        public string SubtopicDesc { get; set; }
        [StringLength(100)]
        public string Syllabus { get; set; }
        [StringLength(100)]
        public string Stream { get; set; }
        [Column("ReplaceAssetID")]
        public int? ReplaceAssetId { get; set; }
        [Column("ContentPackageID")]
        public int? ContentPackageId { get; set; }
        [Column("PackageID")]
        public Guid PackageId { get; set; }
        public bool? IsQuizEnabled { get; set; }
        public int NumberOfAttempts { get; set; }
        public decimal PercentageOfLabels { get; set; }

        [ForeignKey("ContentPackageId")]
        [InverseProperty("Asset")]
        public ContentPackage ContentPackage { get; set; }
        [InverseProperty("Asset")]
        public ICollection<AssetContent> AssetContent { get; set; }
        [InverseProperty("Asset")]
        public ICollection<AssetDetails> AssetDetails { get; set; }
        [InverseProperty("Asset")]
        public ICollection<AssetGrades> AssetGrades { get; set; }
        [InverseProperty("Asset")]
        public ICollection<AssetHistory> AssetHistory { get; set; }
        [InverseProperty("Asset")]
        public ICollection<AssetLanguages> AssetLanguages { get; set; }
        [InverseProperty("Asset")]
        public ICollection<AssetNames> AssetNames { get; set; }
        [InverseProperty("Asset")]
        public ICollection<AssetStreams> AssetStreams { get; set; }
        [InverseProperty("Asset")]
        public ICollection<AssetSubTopics> AssetSubTopics { get; set; }
        [InverseProperty("Asset")]
        public ICollection<AssetSubjects> AssetSubjects { get; set; }
        [InverseProperty("Asset")]
        public ICollection<AssetSyllabus> AssetSyllabus { get; set; }
        [InverseProperty("Asset")]
        public ICollection<AssetTopics> AssetTopics { get; set; }
        [InverseProperty("Asset")]
        public ICollection<FavouriteAssets> FavouriteAssets { get; set; }
        [InverseProperty("Asset")]
        public ICollection<UserAssets> UserAssets { get; set; }
        [InverseProperty("Asset")]
        public ICollection<QuizDetails> QuizDetails { get; set; }
        [InverseProperty("Asset")]
        public ICollection<PuzzleDetails> PuzzleDetails { get; set; }
        [InverseProperty("Asset")]
        public ICollection<AssetRelations> AssetRelations { get; set; }
        [InverseProperty("BondedAssets")]
        public virtual ICollection<MstCompound> BondedCompounds { get; set; }
        [InverseProperty("SphericalAssets")]
        public virtual ICollection<MstCompound> SphericalCompounds { get; set; }
        [InverseProperty("Asset")]
        public virtual ICollection<ClassificationAssets> ClassificationAssets { get; set; }
        [InverseProperty("Asset")]
        public ICollection<AssetTags> AssetTags { get; set; }
        [InverseProperty("Asset")]
        public ICollection<AssetProperties> AssetProperties { get; set; }
    }
}
