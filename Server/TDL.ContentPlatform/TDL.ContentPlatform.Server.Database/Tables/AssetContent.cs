﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    public partial class AssetContent
    {

        public AssetContent()
        {
            AssetContentDetail = new HashSet<AssetContentDetail>();
            AssetHistory = new HashSet<AssetHistory>();
            FavouriteAssets = new HashSet<FavouriteAssets>();
            UserAssets = new HashSet<UserAssets>();
            XmlcontentModel = new HashSet<XmlcontentModel>();
            QuizDetails = new HashSet<QuizDetails>();
            PuzzleDetails = new HashSet<PuzzleDetails>();
            AssetRelations = new HashSet<AssetRelations>();
            AssetContentFeedback = new HashSet<AssetContentFeedback>();
            ClassificationAssets = new HashSet<ClassificationAssets>();
        }

        [Column("ID")]
        public int Id { get; set; }
        [Column("AssetID")]
        public int? AssetId { get; set; }
        [StringLength(200)]
        public string FileName { get; set; }
        [StringLength(1000)]
        public string FilePath { get; set; }
        [StringLength(100)]
        public string FileSize { get; set; }
        public bool? IsDirectory { get; set; }
        public int VersionNumber { get; set; }
        public Guid? CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedOn { get; set; }
        public Guid? ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedOn { get; set; }
        public bool? IsEnabled { get; set; }
        public bool? IsDeleted { get; set; }
        [StringLength(200)]
        public string Thumbnail { get; set; }
        [Column("LabelXML")]
        [StringLength(200)]
        public string LabelXml { get; set; }
        public string Platform { get; set; }
        public string PublishFile { get; set; }
        public string PublishThumbnail { get; set; }
        [StringLength(200)]
        public string Background { get; set; }
        [ForeignKey("AssetId")]
        [InverseProperty("AssetContent")]
        public Asset Asset { get; set; }
        [InverseProperty("AssetContent")]
        public ICollection<AssetContentMetadata> Metadata { get; set; }
        [InverseProperty("AssetContent")]
        public ICollection<AssetContentDetail> AssetContentDetail { get; set; }
        [InverseProperty("AssetContent")]
        public ICollection<AssetHistory> AssetHistory { get; set; }
        [InverseProperty("AssetContent")]
        public ICollection<FavouriteAssets> FavouriteAssets { get; set; }
        [InverseProperty("AssetContent")]
        public ICollection<UserAssets> UserAssets { get; set; }
        [InverseProperty("AssetContent")]
        public ICollection<XmlcontentModel> XmlcontentModel { get; set; }
        [InverseProperty("AssetContent")]
        public ICollection<QuizDetails> QuizDetails { get; set; }
        [InverseProperty("AssetContent")]
        public ICollection<PuzzleDetails> PuzzleDetails { get; set; }
        [InverseProperty("AssetContent")]
        public ICollection<AssetRelations> AssetRelations { get; set; }
        [InverseProperty("AssetContent")]
        public ICollection<AssetContentFeedback> AssetContentFeedback { get; set; }
        [InverseProperty("AssetContent")]
        public virtual ICollection<ClassificationAssets> ClassificationAssets { get; set; }
    }
}
