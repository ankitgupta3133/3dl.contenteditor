﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    public partial class AssetContentDetail
    {
        [Column("ID")]
        public int Id { get; set; }
        [Column("AssetContentID")]
        public int? AssetContentId { get; set; }
        public string Description { get; set; }
        public string WhatIsNew { get; set; }
        [StringLength(10)]
        public string LanguageShortName { get; set; }
        public Guid? CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedOn { get; set; }
        public Guid? ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedOn { get; set; }
        public bool? IsEnabled { get; set; }
        public bool? IsDeleted { get; set; }
        public Guid LanguageId { get; set; }

        [ForeignKey("AssetContentId")]
        [InverseProperty("AssetContentDetail")]
        public AssetContent AssetContent { get; set; }
    }
}
