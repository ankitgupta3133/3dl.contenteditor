﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    public partial class AssetContentFeedback
    {
        [Column("ID")]
        public int Id { get; set; }
        public int AssetContentId { get; set; }
        public Guid UserId { get; set; }
        public string Feedback { get; set; }
        public Guid? SubscriptionId { get; set; }
        public Guid? PackageId { get; set; }
        public Guid? LanguageId { get; set; }
        public Guid? GradeId { get; set; }
        public Guid? SyllabusId { get; set; }
        public Guid? StreamId { get; set; }
        public Guid? SubjectId { get; set; }
        public Guid? TopicId { get; set; }
        public Guid? SubtopicId { get; set; }
        public string PublishAttachment { get; set; }
        public int? StatusId { get; set; }
        public Guid? CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedOn { get; set; }
        public string CreatedByName { get; set; }
        public Guid? ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedOn { get; set; }
        public string ModifiedByName { get; set; }
        public bool? IsEnabled { get; set; }
        public bool? IsDeleted { get; set; }

        [ForeignKey("AssetContentId")]
        [InverseProperty("AssetContentFeedback")]
        public AssetContent AssetContent { get; set; }

        [ForeignKey("StatusId")]
        [InverseProperty("AssetContentFeedback")]
        public MstStatus Status { get; set; }
    }
}
