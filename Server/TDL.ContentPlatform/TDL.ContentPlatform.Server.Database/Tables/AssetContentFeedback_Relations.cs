﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    public partial class AssetContentFeedback
    {
        public CsNewSubscription Subscription { get; set; }
        public CsNewPackage Package { get; set; }
        public CsNewLanguage Language { get; set; }
        public CsNewGrade Grade { get; set; }
        public CsNewSyllabus Syllabus { get; set; }
        public CsNewStream Stream { get; set; }
        public CsNewSubject Subject { get; set; }
        public CsNewTopic Topic { get; set; }
        public CsNewSubtopic Subtopic { get; set; }
    }
}
