﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    public class AssetContentMetadata
    {
        [Column("ID")]
        public int Id { get; set; }
        [Column("AssetContentID")]
        public int? AssetContentId { get; set; }
        
        [ForeignKey("AssetContentId")]
        [InverseProperty("Metadata")]
        public AssetContent AssetContent { get; set; }

        public bool? IsEnabled { get; set; }
        public bool? IsDeleted { get; set; }

        public Guid? CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedOn { get; set; }
        public Guid? ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedOn { get; set; }

        [MaxLength(50)]
        public string Type { get;set; }
        [Column(TypeName = "varchar(max)")]
        public string Json { get;set; }

    }
}