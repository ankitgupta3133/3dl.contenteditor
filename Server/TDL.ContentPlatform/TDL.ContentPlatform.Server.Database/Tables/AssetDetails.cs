﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    public partial class AssetDetails
    {
        public int Id { get; set; }
        public int AssetId { get; set; }
        public Guid PackageId { get; set; }
        public Guid LanguageId { get; set; }
        public Guid GradeId { get; set; }
        public Guid SyllabusId { get; set; }
        public Guid StreamId { get; set; }
        public Guid SubjectId { get; set; }
        public Guid TopicId { get; set; }
        public Guid SubtopicId { get; set; }
        public bool IsDeleted { get; set; }

        [ForeignKey("AssetId")]
        [InverseProperty("AssetDetails")]
        public Asset Asset { get; set; }
        //[ForeignKey("PackageId")]
        //[InverseProperty("AssetDetails")]
        //public CsNewPackage CsNewPackage { get; set; }
        //[ForeignKey("LanguageId")]
        //[InverseProperty("AssetDetails")]
        //public CsNewLanguage CsNewLanguage { get; set; }
        //[ForeignKey("GradeId")]
        //[InverseProperty("AssetDetails")]
        //public CsNewGrade CsNewGrade { get; set; }
        //[ForeignKey("SyllabusId")]
        //[InverseProperty("AssetDetails")]
        //public CsNewSyllabus CsNewSyllabus { get; set; }
        //[ForeignKey("StreamId")]
        //[InverseProperty("AssetDetails")]
        //public CsNewStream csNewStream { get; set; }
        //[ForeignKey("SubjectId")]
        //[InverseProperty("AssetDetails")]
        //public CsNewSubject CsNewSubject { get; set; }
        //[ForeignKey("TopicId")]
        //[InverseProperty("AssetDetails")]
        //public CsNewTopic CsNewTopic { get; set; }
        //[ForeignKey("SubtopicId")]
        //[InverseProperty("AssetDetails")]
        //public CsNewSubtopic csNewSubtopic { get; set; }
    }
}
