﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    public partial class AssetGrades
    {
        public int Id { get; set; }
        public int AssetId { get; set; }
        public Guid GradeId { get; set; }

        [ForeignKey("AssetId")]
        [InverseProperty("AssetGrades")]
        public Asset Asset { get; set; }
    }
}
