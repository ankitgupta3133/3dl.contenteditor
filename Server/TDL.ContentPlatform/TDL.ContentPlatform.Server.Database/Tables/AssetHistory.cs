﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    public partial class AssetHistory
    {
        [Column("ID")]
        public int Id { get; set; }
        [Column("UserID")]
        public Guid? UserId { get; set; }
        [Column("AssetID")]
        public int? AssetId { get; set; }
        [Column("AssetContentID")]
        public int? AssetContentId { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedOn { get; set; }
        public bool? IsDeleted { get; set; }

        [ForeignKey("AssetId")]
        [InverseProperty("AssetHistory")]
        public Asset Asset { get; set; }
        [ForeignKey("AssetContentId")]
        [InverseProperty("AssetHistory")]
        public AssetContent AssetContent { get; set; }
    }
}
