﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    public partial class AssetLanguages
    {
        public int Id { get; set; }
        public int AssetId { get; set; }
        public Guid LanguageId { get; set; }

        [ForeignKey("AssetId")]
        [InverseProperty("AssetLanguages")]
        public Asset Asset { get; set; }
    }
}
