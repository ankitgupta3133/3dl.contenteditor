﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    public partial class AssetNames
    {
        public int Id { get; set; }
        public int AssetId { get; set; }
        public string Name { get; set; }
        public Guid LanguageId { get; set; }
        public bool IsDeleted { get; set; }

        [ForeignKey("AssetId")]
        [InverseProperty("AssetNames")]
        public Asset Asset { get; set; }
    }
}
