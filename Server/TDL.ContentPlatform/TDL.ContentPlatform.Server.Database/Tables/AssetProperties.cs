﻿using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    public partial class AssetProperties
    {
        public int Id { get; set; }
        public int AssetId { get; set; }
        public int MstPropertyId { get; set; }

        [ForeignKey("AssetId")]
        [InverseProperty("AssetProperties")]
        public Asset Asset { get; set; }

        [ForeignKey("MstPropertyId")]
        [InverseProperty("AssetProperties")]
        public MstProperty MstProperty { get; set; }
    }
}
