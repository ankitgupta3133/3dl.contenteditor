﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    public partial class AssetRelations
    {
        [Key]
        public int RelationId { get; set; }
        public int AssetId { get; set; }
        public int LinkedAssetId { get; set; }
        public int LinkedContentId { get; set; }
        public Guid? CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedOn { get; set; }
        public Guid? ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedOn { get; set; }
        public bool? IsSelected { get; set; }
        public bool? IsDeleted { get; set; }
        public string Description { get; set; }
        [ForeignKey("LinkedAssetId")]
        [InverseProperty("AssetRelations")]
        public Asset Asset { get; set; }
        [ForeignKey("LinkedContentId")]
        [InverseProperty("AssetRelations")]
        public AssetContent AssetContent { get; set; }
    }
}
