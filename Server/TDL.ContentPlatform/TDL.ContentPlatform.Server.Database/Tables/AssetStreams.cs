﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    public partial class AssetStreams
    {
        public int Id { get; set; }
        public int AssetId { get; set; }
        public Guid StreamId { get; set; }

        [ForeignKey("AssetId")]
        [InverseProperty("AssetStreams")]
        public Asset Asset { get; set; }
    }
}
