﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    public partial class AssetSubTopics
    {
        public int Id { get; set; }
        public int AssetId { get; set; }
        public Guid SubTopicId { get; set; }

        [ForeignKey("AssetId")]
        [InverseProperty("AssetSubTopics")]
        public Asset Asset { get; set; }
    }
}
