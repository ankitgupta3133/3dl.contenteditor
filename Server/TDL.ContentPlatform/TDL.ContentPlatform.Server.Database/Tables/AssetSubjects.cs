﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    public partial class AssetSubjects
    {
        public int Id { get; set; }
        public int AssetId { get; set; }
        public Guid SubjectId { get; set; }

        [ForeignKey("AssetId")]
        [InverseProperty("AssetSubjects")]
        public Asset Asset { get; set; }
    }
}
