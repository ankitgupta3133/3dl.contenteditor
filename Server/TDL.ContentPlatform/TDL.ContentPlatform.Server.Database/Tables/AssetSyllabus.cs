﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    public partial class AssetSyllabus
    {
        public int Id { get; set; }
        public int AssetId { get; set; }
        public Guid SyllabusId { get; set; }

        [ForeignKey("AssetId")]
        [InverseProperty("AssetSyllabus")]
        public Asset Asset { get; set; }
    }
}
