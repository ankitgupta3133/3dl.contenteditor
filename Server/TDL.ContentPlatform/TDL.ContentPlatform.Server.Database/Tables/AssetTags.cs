﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    public partial class AssetTags
    {
        public int Id { get; set; }
        public int AssetId { get; set; }
        public int MstTagId { get; set; }

        [ForeignKey("AssetId")]
        [InverseProperty("AssetTags")]
        public Asset Asset { get; set; }

        [ForeignKey("MstTagId")]
        [InverseProperty("AssetTags")]
        public MstTag MstTags { get; set; }
    }
}
