﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    public partial class AssetTopics
    {
        public int Id { get; set; }
        public int AssetId { get; set; }
        public Guid TopicId { get; set; }

        [ForeignKey("AssetId")]
        [InverseProperty("AssetTopics")]
        public Asset Asset { get; set; }
    }
}
