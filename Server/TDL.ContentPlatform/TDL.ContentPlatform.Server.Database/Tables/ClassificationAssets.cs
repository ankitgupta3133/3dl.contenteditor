﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    public partial class ClassificationAssets
    {
        [Key]
        public int Id { get; set; }
        public int ClassificationDetailId { get; set; }
        public int AssetId { get; set; }
        public int AssetContentId { get; set; }
        public Guid? CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedOn { get; set; }
        public Guid? ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedOn { get; set; }
        public bool? IsEnabled { get; set; }
        public bool? IsDeleted { get; set; }
        [ForeignKey("AssetId")]
        [InverseProperty("ClassificationAssets")]
        public Asset Asset { get; set; }
        [ForeignKey("AssetContentId")]
        [InverseProperty("ClassificationAssets")]
        public AssetContent AssetContent { get; set; }
        [ForeignKey("ClassificationDetailId")]
        [InverseProperty("ClassificationAssets")]
        public ClassificationDetails ClassificationDetails { get; set; }
    }
}
