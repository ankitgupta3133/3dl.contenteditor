﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    public partial class ClassificationDetails
    {
        [Key]
        public int Id { get; set; }
        public int ClassificationId { get; set; }
        public int PropertyId { get; set; }
        public Guid? CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedOn { get; set; }
        public Guid? ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedOn { get; set; }
        public bool? IsEnabled { get; set; }
        public bool? IsDeleted { get; set; }
        [ForeignKey("ClassificationId")]
        [InverseProperty("ClassificationDetails")]
        public Activities Activity { get; set; }
        [ForeignKey("PropertyId")]
        [InverseProperty("ClassificationDetails")]
        public MstProperty MstProperty { get; set; }
        [InverseProperty("ClassificationDetails")]
        public ICollection<ClassificationAssets> ClassificationAssets { get; set; }
        public ClassificationDetails()
        {
            ClassificationAssets = new HashSet<ClassificationAssets>();
        }
    }
}
