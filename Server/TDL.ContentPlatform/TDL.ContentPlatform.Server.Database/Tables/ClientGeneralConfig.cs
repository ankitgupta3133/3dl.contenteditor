﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    public partial class ClientGeneralConfig
    {
        public long Id { get; set; }
        public bool IsDeleted { get; set; }
        public string ClientType { get; set; }
        public string Key { get; set; }
        public int DataType { get; set; }
        public string Value { get; set; }
    }
}
