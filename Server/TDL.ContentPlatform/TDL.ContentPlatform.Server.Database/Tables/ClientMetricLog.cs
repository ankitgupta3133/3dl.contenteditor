﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    public partial class ClientMetricLog
    {
        public int Id { get; set; }
        public Guid MetricId { get; set; }
        public string ComputerName { get; set; }
        public Guid SessionId { get; set; }
        public DateTime TimeStamp { get; set; }
        public string MetricsType { get; set; }
        public string ClassName { get; set; }
        public string CallerMemberName { get; set; }
        public string CallerFilePath { get; set; }
        public int CallerLineNumber { get; set; }
        public string Action { get; set; }
        public string JsonParameters { get; set; }
        public bool IsAuthenticated { get; set; }
        public string Username { get; set; }
        public Guid UserId { get; set; }
        public DateTime TransferredDttmUtc { get; set; }
    }
}
