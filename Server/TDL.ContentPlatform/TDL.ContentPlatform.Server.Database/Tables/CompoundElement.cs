﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    public partial class CompoundElement
    {
        [Key]
        public int Id { get; set; }
        public int CompoundId { get; set; }
        public int ElementId { get; set; }
        public bool? IsDeleted { get; set; }
        [ForeignKey("CompoundId")]
        [InverseProperty("CompoundElement")]
        public MstCompound MstCompound { get; set; }
        [ForeignKey("ElementId")]
        [InverseProperty("CompoundElement")]
        public MstElement MstElement { get; set; }
    }
}
