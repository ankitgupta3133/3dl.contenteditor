﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    public partial class ContentPackage
    {
        public ContentPackage()
        {
            Asset = new HashSet<Asset>();
        }

        [Column("ID")]
        public int Id { get; set; }
        [Column("ContentPackageID")]
        public Guid? ContentPackageId { get; set; }
        [Column("ReplacesPackageContentID")]
        public Guid? ReplacesPackageContentId { get; set; }
        [StringLength(200)]
        public string Name { get; set; }
        public Guid? CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedOn { get; set; }
        public Guid? ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedOn { get; set; }
        public bool? IsEnabled { get; set; }
        public bool? IsDeleted { get; set; }

        [InverseProperty("ContentPackage")]
        public ICollection<Asset> Asset { get; set; }
    }
}
