﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    public partial class ContentPackageFileTable
    {
        public Guid Id { get; set; }
        [Required]
        [StringLength(255)]
        public string Name { get; set; }
        public long Size { get; set; }
        public string Url { get; set; }
        public bool IsDeleted { get; set; }
        public Guid ContentPackageId { get; set; }

        [ForeignKey("ContentPackageId")]
        [InverseProperty("ContentPackageFileTable")]
        public ContentPackageTable ContentPackage { get; set; }
    }
}
