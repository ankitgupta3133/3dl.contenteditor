﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    public partial class ContentPackageTable
    {
        public ContentPackageTable()
        {
            ContentPackageFileTable = new HashSet<ContentPackageFileTable>();
        }

        public Guid Id { get; set; }
        [Required]
        [StringLength(255)]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }
        public long Size { get; set; }
        public bool IsDeleted { get; set; }

        [InverseProperty("ContentPackage")]
        public ICollection<ContentPackageFileTable> ContentPackageFileTable { get; set; }
    }
}
