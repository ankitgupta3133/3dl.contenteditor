﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    [Table("cs__account")]
    public partial class CsAccount
    {
        public Guid Id { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? SinkCreatedOn { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? SinkModifiedOn { get; set; }
        [Column("statecode")]
        public int? Statecode { get; set; }
        [Column("statuscode")]
        public int? Statuscode { get; set; }
        [Column("address1_addresstypecode")]
        public int? Address1Addresstypecode { get; set; }
        [Column("address1_shippingmethodcode")]
        public int? Address1Shippingmethodcode { get; set; }
        [Column("address1_freighttermscode")]
        public int? Address1Freighttermscode { get; set; }
        [Column("accountratingcode")]
        public int? Accountratingcode { get; set; }
        [Column("preferredappointmenttimecode")]
        public int? Preferredappointmenttimecode { get; set; }
        [Column("accountclassificationcode")]
        public int? Accountclassificationcode { get; set; }
        [Column("customertypecode")]
        public int? Customertypecode { get; set; }
        [Column("preferredcontactmethodcode")]
        public int? Preferredcontactmethodcode { get; set; }
        [Column("ownershipcode")]
        public int? Ownershipcode { get; set; }
        [Column("address2_addresstypecode")]
        public int? Address2Addresstypecode { get; set; }
        [Column("businesstypecode")]
        public int? Businesstypecode { get; set; }
        [Column("shippingmethodcode")]
        public int? Shippingmethodcode { get; set; }
        [Column("paymenttermscode")]
        public int? Paymenttermscode { get; set; }
        [Column("customersizecode")]
        public int? Customersizecode { get; set; }
        [Column("accountcategorycode")]
        public int? Accountcategorycode { get; set; }
        [Column("industrycode")]
        public int? Industrycode { get; set; }
        [Column("preferredappointmentdaycode")]
        public int? Preferredappointmentdaycode { get; set; }
        [Column("address2_shippingmethodcode")]
        public int? Address2Shippingmethodcode { get; set; }
        [Column("address2_freighttermscode")]
        public int? Address2Freighttermscode { get; set; }
        [Column("territorycode")]
        public int? Territorycode { get; set; }
        [Column("donotpostalmail")]
        public bool? Donotpostalmail { get; set; }
        [Column("creditonhold")]
        public bool? Creditonhold { get; set; }
        [Column("donotbulkpostalmail")]
        public bool? Donotbulkpostalmail { get; set; }
        [Column("donotbulkemail")]
        public bool? Donotbulkemail { get; set; }
        [Column("donotfax")]
        public bool? Donotfax { get; set; }
        [Column("donotemail")]
        public bool? Donotemail { get; set; }
        [Column("marketingonly")]
        public bool? Marketingonly { get; set; }
        [Column("followemail")]
        public bool? Followemail { get; set; }
        [Column("merged")]
        public bool? Merged { get; set; }
        [Column("donotphone")]
        public bool? Donotphone { get; set; }
        [Column("donotsendmm")]
        public bool? Donotsendmm { get; set; }
        [Column("participatesinworkflow")]
        public bool? Participatesinworkflow { get; set; }
        [Column("isprivate")]
        public bool? Isprivate { get; set; }
        [Column("slainvokedid")]
        public Guid? Slainvokedid { get; set; }
        [Column("slainvokedid_entitytype")]
        [StringLength(128)]
        public string SlainvokedidEntitytype { get; set; }
        [Column("originatingleadid")]
        public Guid? Originatingleadid { get; set; }
        [Column("originatingleadid_entitytype")]
        [StringLength(128)]
        public string OriginatingleadidEntitytype { get; set; }
        [Column("owningbusinessunit")]
        public Guid? Owningbusinessunit { get; set; }
        [Column("owningbusinessunit_entitytype")]
        [StringLength(128)]
        public string OwningbusinessunitEntitytype { get; set; }
        [Column("territoryid")]
        public Guid? Territoryid { get; set; }
        [Column("territoryid_entitytype")]
        [StringLength(128)]
        public string TerritoryidEntitytype { get; set; }
        [Column("slaid")]
        public Guid? Slaid { get; set; }
        [Column("slaid_entitytype")]
        [StringLength(128)]
        public string SlaidEntitytype { get; set; }
        [Column("primarycontactid")]
        public Guid? Primarycontactid { get; set; }
        [Column("primarycontactid_entitytype")]
        [StringLength(128)]
        public string PrimarycontactidEntitytype { get; set; }
        [Column("parentaccountid")]
        public Guid? Parentaccountid { get; set; }
        [Column("parentaccountid_entitytype")]
        [StringLength(128)]
        public string ParentaccountidEntitytype { get; set; }
        [Column("createdonbehalfby")]
        public Guid? Createdonbehalfby { get; set; }
        [Column("createdonbehalfby_entitytype")]
        [StringLength(128)]
        public string CreatedonbehalfbyEntitytype { get; set; }
        [Column("modifiedby")]
        public Guid? Modifiedby { get; set; }
        [Column("modifiedby_entitytype")]
        [StringLength(128)]
        public string ModifiedbyEntitytype { get; set; }
        [Column("modifiedonbehalfby")]
        public Guid? Modifiedonbehalfby { get; set; }
        [Column("modifiedonbehalfby_entitytype")]
        [StringLength(128)]
        public string ModifiedonbehalfbyEntitytype { get; set; }
        [Column("owninguser")]
        public Guid? Owninguser { get; set; }
        [Column("owninguser_entitytype")]
        [StringLength(128)]
        public string OwninguserEntitytype { get; set; }
        [Column("owningteam")]
        public Guid? Owningteam { get; set; }
        [Column("owningteam_entitytype")]
        [StringLength(128)]
        public string OwningteamEntitytype { get; set; }
        [Column("transactioncurrencyid")]
        public Guid? Transactioncurrencyid { get; set; }
        [Column("transactioncurrencyid_entitytype")]
        [StringLength(128)]
        public string TransactioncurrencyidEntitytype { get; set; }
        [Column("masterid")]
        public Guid? Masterid { get; set; }
        [Column("masterid_entitytype")]
        [StringLength(128)]
        public string MasteridEntitytype { get; set; }
        [Column("modifiedbyexternalparty")]
        public Guid? Modifiedbyexternalparty { get; set; }
        [Column("modifiedbyexternalparty_entitytype")]
        [StringLength(128)]
        public string ModifiedbyexternalpartyEntitytype { get; set; }
        [Column("defaultpricelevelid")]
        public Guid? Defaultpricelevelid { get; set; }
        [Column("defaultpricelevelid_entitytype")]
        [StringLength(128)]
        public string DefaultpricelevelidEntitytype { get; set; }
        [Column("preferredequipmentid")]
        public Guid? Preferredequipmentid { get; set; }
        [Column("preferredequipmentid_entitytype")]
        [StringLength(128)]
        public string PreferredequipmentidEntitytype { get; set; }
        [Column("createdby")]
        public Guid? Createdby { get; set; }
        [Column("createdby_entitytype")]
        [StringLength(128)]
        public string CreatedbyEntitytype { get; set; }
        [Column("createdbyexternalparty")]
        public Guid? Createdbyexternalparty { get; set; }
        [Column("createdbyexternalparty_entitytype")]
        [StringLength(128)]
        public string CreatedbyexternalpartyEntitytype { get; set; }
        [Column("preferredserviceid")]
        public Guid? Preferredserviceid { get; set; }
        [Column("preferredserviceid_entitytype")]
        [StringLength(128)]
        public string PreferredserviceidEntitytype { get; set; }
        [Column("preferredsystemuserid")]
        public Guid? Preferredsystemuserid { get; set; }
        [Column("preferredsystemuserid_entitytype")]
        [StringLength(128)]
        public string PreferredsystemuseridEntitytype { get; set; }
        [Column("ownerid")]
        public Guid? Ownerid { get; set; }
        [Column("ownerid_entitytype")]
        [StringLength(128)]
        public string OwneridEntitytype { get; set; }
        [Column("aging30", TypeName = "decimal(38, 2)")]
        public decimal? Aging30 { get; set; }
        [Column("aging60_base", TypeName = "decimal(38, 4)")]
        public decimal? Aging60Base { get; set; }
        [Column("marketcap", TypeName = "decimal(38, 2)")]
        public decimal? Marketcap { get; set; }
        [Column("creditlimit", TypeName = "decimal(38, 2)")]
        public decimal? Creditlimit { get; set; }
        [Column("creditlimit_base", TypeName = "decimal(38, 4)")]
        public decimal? CreditlimitBase { get; set; }
        [Column("aging90_base", TypeName = "decimal(38, 4)")]
        public decimal? Aging90Base { get; set; }
        [Column("aging60", TypeName = "decimal(38, 2)")]
        public decimal? Aging60 { get; set; }
        [Column("aging30_base", TypeName = "decimal(38, 4)")]
        public decimal? Aging30Base { get; set; }
        [Column("revenue", TypeName = "decimal(38, 2)")]
        public decimal? Revenue { get; set; }
        [Column("openrevenue", TypeName = "decimal(38, 2)")]
        public decimal? Openrevenue { get; set; }
        [Column("revenue_base", TypeName = "decimal(38, 4)")]
        public decimal? RevenueBase { get; set; }
        [Column("openrevenue_base", TypeName = "decimal(38, 2)")]
        public decimal? OpenrevenueBase { get; set; }
        [Column("marketcap_base", TypeName = "decimal(38, 4)")]
        public decimal? MarketcapBase { get; set; }
        [Column("aging90", TypeName = "decimal(38, 2)")]
        public decimal? Aging90 { get; set; }
        [Column("emailaddress3")]
        [StringLength(100)]
        public string Emailaddress3 { get; set; }
        [Column("emailaddress2")]
        [StringLength(100)]
        public string Emailaddress2 { get; set; }
        [Column("emailaddress1")]
        [StringLength(100)]
        public string Emailaddress1 { get; set; }
        [Column("masteraccountidyominame")]
        [StringLength(100)]
        public string Masteraccountidyominame { get; set; }
        [Column("address1_city")]
        [StringLength(80)]
        public string Address1City { get; set; }
        [Column("address1_line1")]
        [StringLength(250)]
        public string Address1Line1 { get; set; }
        [Column("modifiedon", TypeName = "datetime")]
        public DateTime? Modifiedon { get; set; }
        [Column("address1_longitude", TypeName = "decimal(38, 5)")]
        public decimal? Address1Longitude { get; set; }
        [Column("websiteurl")]
        [StringLength(200)]
        public string Websiteurl { get; set; }
        [Column("createdbyexternalpartyname")]
        [StringLength(100)]
        public string Createdbyexternalpartyname { get; set; }
        [Column("entityimage_timestamp")]
        public long? EntityimageTimestamp { get; set; }
        [Column("modifiedbyyominame")]
        [StringLength(100)]
        public string Modifiedbyyominame { get; set; }
        [Column("sharesoutstanding")]
        public int? Sharesoutstanding { get; set; }
        [Column("primarycontactidname")]
        [StringLength(100)]
        public string Primarycontactidname { get; set; }
        [Column("address2_city")]
        [StringLength(80)]
        public string Address2City { get; set; }
        [Column("preferredsystemuseridyominame")]
        [StringLength(100)]
        public string Preferredsystemuseridyominame { get; set; }
        [Column("opendeals_date", TypeName = "datetime")]
        public DateTime? OpendealsDate { get; set; }
        [Column("modifiedbyexternalpartyyominame")]
        [StringLength(100)]
        public string Modifiedbyexternalpartyyominame { get; set; }
        [Column("masteraccountidname")]
        [StringLength(100)]
        public string Masteraccountidname { get; set; }
        [Column("preferredsystemuseridname")]
        [StringLength(100)]
        public string Preferredsystemuseridname { get; set; }
        [Column("preferredserviceidname")]
        [StringLength(100)]
        public string Preferredserviceidname { get; set; }
        [Column("address2_stateorprovince")]
        [StringLength(50)]
        public string Address2Stateorprovince { get; set; }
        [Column("timespentbymeonemailandmeetings")]
        [StringLength(1250)]
        public string Timespentbymeonemailandmeetings { get; set; }
        [Column("address2_country")]
        [StringLength(80)]
        public string Address2Country { get; set; }
        [Column("address2_line2")]
        [StringLength(250)]
        public string Address2Line2 { get; set; }
        [Column("accountid")]
        public Guid? Accountid { get; set; }
        [Column("onholdtime")]
        public int? Onholdtime { get; set; }
        [Column("originatingleadidname")]
        [StringLength(100)]
        public string Originatingleadidname { get; set; }
        [Column("parentaccountidname")]
        [StringLength(100)]
        public string Parentaccountidname { get; set; }
        [Column("address1_utcoffset")]
        public int? Address1Utcoffset { get; set; }
        [Column("numberofemployees")]
        public int? Numberofemployees { get; set; }
        [Column("modifiedbyexternalpartyname")]
        [StringLength(100)]
        public string Modifiedbyexternalpartyname { get; set; }
        [Column("exchangerate", TypeName = "decimal(38, 10)")]
        public decimal? Exchangerate { get; set; }
        [Column("address2_county")]
        [StringLength(50)]
        public string Address2County { get; set; }
        [Column("address2_line1")]
        [StringLength(250)]
        public string Address2Line1 { get; set; }
        [Column("telephone3")]
        [StringLength(50)]
        public string Telephone3 { get; set; }
        [Column("address2_latitude", TypeName = "decimal(38, 5)")]
        public decimal? Address2Latitude { get; set; }
        [Column("createdon", TypeName = "datetime")]
        public DateTime? Createdon { get; set; }
        [Column("slaname")]
        [StringLength(100)]
        public string Slaname { get; set; }
        [Column("slainvokedidname")]
        [StringLength(100)]
        public string Slainvokedidname { get; set; }
        [Column("address1_composite")]
        public string Address1Composite { get; set; }
        [Column("opendeals_state")]
        public int? OpendealsState { get; set; }
        [Column("address2_postalcode")]
        [StringLength(20)]
        public string Address2Postalcode { get; set; }
        [Column("lastusedincampaign", TypeName = "datetime")]
        public DateTime? Lastusedincampaign { get; set; }
        [Column("utcconversiontimezonecode")]
        public int? Utcconversiontimezonecode { get; set; }
        [Column("owneridyominame")]
        [StringLength(100)]
        public string Owneridyominame { get; set; }
        [Column("entityimage_url")]
        [StringLength(200)]
        public string EntityimageUrl { get; set; }
        [Column("address2_line3")]
        [StringLength(250)]
        public string Address2Line3 { get; set; }
        [Column("description")]
        public string Description { get; set; }
        [Column("timezoneruleversionnumber")]
        public int? Timezoneruleversionnumber { get; set; }
        [Column("address1_county")]
        [StringLength(50)]
        public string Address1County { get; set; }
        [Column("createdbyexternalpartyyominame")]
        [StringLength(100)]
        public string Createdbyexternalpartyyominame { get; set; }
        [Column("entityimageid")]
        public Guid? Entityimageid { get; set; }
        [Column("address2_postofficebox")]
        [StringLength(20)]
        public string Address2Postofficebox { get; set; }
        [Column("address2_telephone1")]
        [StringLength(50)]
        public string Address2Telephone1 { get; set; }
        [Column("address2_telephone2")]
        [StringLength(50)]
        public string Address2Telephone2 { get; set; }
        [Column("address2_telephone3")]
        [StringLength(50)]
        public string Address2Telephone3 { get; set; }
        [Column("originatingleadidyominame")]
        [StringLength(100)]
        public string Originatingleadidyominame { get; set; }
        [Column("preferredequipmentidname")]
        [StringLength(100)]
        public string Preferredequipmentidname { get; set; }
        [Column("address1_addressid")]
        public Guid? Address1Addressid { get; set; }
        [Column("traversedpath")]
        [StringLength(1250)]
        public string Traversedpath { get; set; }
        [Column("territoryidname")]
        [StringLength(100)]
        public string Territoryidname { get; set; }
        [Column("yominame")]
        [StringLength(160)]
        public string Yominame { get; set; }
        [Column("createdonbehalfbyname")]
        [StringLength(100)]
        public string Createdonbehalfbyname { get; set; }
        [Column("address2_name")]
        [StringLength(200)]
        public string Address2Name { get; set; }
        [Column("openrevenue_state")]
        public int? OpenrevenueState { get; set; }
        [Column("primarysatoriid")]
        [StringLength(200)]
        public string Primarysatoriid { get; set; }
        [Column("name")]
        [StringLength(160)]
        public string Name { get; set; }
        [Column("primarytwitterid")]
        [StringLength(128)]
        public string Primarytwitterid { get; set; }
        [Column("owneridname")]
        [StringLength(100)]
        public string Owneridname { get; set; }
        [Column("modifiedonbehalfbyname")]
        [StringLength(100)]
        public string Modifiedonbehalfbyname { get; set; }
        [Column("overriddencreatedon", TypeName = "datetime")]
        public DateTime? Overriddencreatedon { get; set; }
        [Column("address2_composite")]
        public string Address2Composite { get; set; }
        [Column("address1_country")]
        [StringLength(80)]
        public string Address1Country { get; set; }
        [Column("transactioncurrencyidname")]
        [StringLength(100)]
        public string Transactioncurrencyidname { get; set; }
        [Column("address1_stateorprovince")]
        [StringLength(50)]
        public string Address1Stateorprovince { get; set; }
        [Column("address1_line3")]
        [StringLength(250)]
        public string Address1Line3 { get; set; }
        [Column("processid")]
        public Guid? Processid { get; set; }
        [Column("address1_telephone1")]
        [StringLength(50)]
        public string Address1Telephone1 { get; set; }
        [Column("address1_telephone2")]
        [StringLength(50)]
        public string Address1Telephone2 { get; set; }
        [Column("address1_telephone3")]
        [StringLength(50)]
        public string Address1Telephone3 { get; set; }
        [Column("address1_postofficebox")]
        [StringLength(20)]
        public string Address1Postofficebox { get; set; }
        [Column("createdonbehalfbyyominame")]
        [StringLength(100)]
        public string Createdonbehalfbyyominame { get; set; }
        [Column("address1_name")]
        [StringLength(200)]
        public string Address1Name { get; set; }
        [Column("fax")]
        [StringLength(50)]
        public string Fax { get; set; }
        [Column("sic")]
        [StringLength(20)]
        public string Sic { get; set; }
        [Column("address2_utcoffset")]
        public int? Address2Utcoffset { get; set; }
        [Column("stageid")]
        public Guid? Stageid { get; set; }
        [Column("accountnumber")]
        [StringLength(20)]
        public string Accountnumber { get; set; }
        [Column("address2_fax")]
        [StringLength(50)]
        public string Address2Fax { get; set; }
        [Column("owneridtype")]
        [StringLength(4000)]
        public string Owneridtype { get; set; }
        [Column("address2_longitude", TypeName = "decimal(38, 5)")]
        public decimal? Address2Longitude { get; set; }
        [Column("ftpsiteurl")]
        [StringLength(200)]
        public string Ftpsiteurl { get; set; }
        [Column("createdbyname")]
        [StringLength(100)]
        public string Createdbyname { get; set; }
        [Column("address1_primarycontactname")]
        [StringLength(100)]
        public string Address1Primarycontactname { get; set; }
        [Column("lastonholdtime", TypeName = "datetime")]
        public DateTime? Lastonholdtime { get; set; }
        [Column("address1_line2")]
        [StringLength(250)]
        public string Address1Line2 { get; set; }
        [Column("openrevenue_date", TypeName = "datetime")]
        public DateTime? OpenrevenueDate { get; set; }
        [Column("address1_postalcode")]
        [StringLength(20)]
        public string Address1Postalcode { get; set; }
        [Column("tickersymbol")]
        [StringLength(10)]
        public string Tickersymbol { get; set; }
        [Column("address2_upszone")]
        [StringLength(4)]
        public string Address2Upszone { get; set; }
        [Column("defaultpricelevelidname")]
        [StringLength(100)]
        public string Defaultpricelevelidname { get; set; }
        [Column("stockexchange")]
        [StringLength(20)]
        public string Stockexchange { get; set; }
        [Column("importsequencenumber")]
        public int? Importsequencenumber { get; set; }
        [Column("telephone2")]
        [StringLength(50)]
        public string Telephone2 { get; set; }
        [Column("versionnumber")]
        public long? Versionnumber { get; set; }
        [Column("telephone1")]
        [StringLength(50)]
        public string Telephone1 { get; set; }
        [Column("address2_addressid")]
        public Guid? Address2Addressid { get; set; }
        [Column("address1_fax")]
        [StringLength(50)]
        public string Address1Fax { get; set; }
        [Column("address1_latitude", TypeName = "decimal(38, 5)")]
        public decimal? Address1Latitude { get; set; }
        [Column("primarycontactidyominame")]
        [StringLength(100)]
        public string Primarycontactidyominame { get; set; }
        [Column("modifiedbyname")]
        [StringLength(100)]
        public string Modifiedbyname { get; set; }
        [Column("createdbyyominame")]
        [StringLength(100)]
        public string Createdbyyominame { get; set; }
        [Column("address1_upszone")]
        [StringLength(4)]
        public string Address1Upszone { get; set; }
        [Column("modifiedonbehalfbyyominame")]
        [StringLength(100)]
        public string Modifiedonbehalfbyyominame { get; set; }
        [Column("parentaccountidyominame")]
        [StringLength(100)]
        public string Parentaccountidyominame { get; set; }
        [Column("address2_primarycontactname")]
        [StringLength(100)]
        public string Address2Primarycontactname { get; set; }
        [Column("opendeals")]
        public int? Opendeals { get; set; }
    }
}
