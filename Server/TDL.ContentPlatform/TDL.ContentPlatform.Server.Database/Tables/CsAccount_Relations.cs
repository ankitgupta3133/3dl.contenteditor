﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    public partial class CsAccount
    {
        [InverseProperty("Parentaccount")]
        public List<CsOpportunity> CsOpportunities { get;set; }
    }
}