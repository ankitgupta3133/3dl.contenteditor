﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    [Table("cs__AttributeMetadata")]
    public partial class CsAttributeMetadata
    {
        public long Id { get; set; }
        [Required]
        [StringLength(64)]
        public string EntityName { get; set; }
        [Required]
        [StringLength(64)]
        public string AttributeName { get; set; }
        [Required]
        [StringLength(64)]
        public string AttributeType { get; set; }
        public int AttributeTypeCode { get; set; }
        public long? Version { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? Timestamp { get; set; }
        [StringLength(64)]
        public string MetadataId { get; set; }
        public int? Precision { get; set; }
    }
}
