﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    [Table("cs__DeleteLog")]
    public partial class CsDeleteLog
    {
        public long Id { get; set; }
        [Required]
        [StringLength(64)]
        public string EntityName { get; set; }
        [Required]
        [StringLength(64)]
        public string RecordId { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime SinkDeleteTime { get; set; }
        public long VersionNumber { get; set; }
    }
}
