﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    [Table("cs__new_grade_stream")]
    public partial class CsNewGradeStream
    {
        public Guid Id { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? SinkCreatedOn { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? SinkModifiedOn { get; set; }
        [Column("new_grade_streamid")]
        public Guid? NewGradeStreamid { get; set; }
        [Column("versionnumber")]
        public long? Versionnumber { get; set; }
        [Column("new_gradeid")]
        public Guid? NewGradeid { get; set; }
        [Column("new_streamid")]
        public Guid? NewStreamid { get; set; }
    }
}
