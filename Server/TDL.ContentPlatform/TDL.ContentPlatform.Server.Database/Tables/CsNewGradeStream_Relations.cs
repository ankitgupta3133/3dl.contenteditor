﻿using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    public partial class CsNewGradeStream
    {
        public CsNewGrade NewGrade { get; set; }
        public CsNewStream NewStream { get; set; }
    }
}