﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    [Table("cs__new_grade_subject")]
    public partial class CsNewGradeSubject
    {
        public Guid Id { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? SinkCreatedOn { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? SinkModifiedOn { get; set; }
        [Column("new_gradeid")]
        public Guid? NewGradeid { get; set; }
        [Column("versionnumber")]
        public long? Versionnumber { get; set; }
        [Column("new_subjectid")]
        public Guid? NewSubjectid { get; set; }
        [Column("new_grade_subjectid")]
        public Guid? NewGradeSubjectid { get; set; }
    }
}
