﻿namespace TDL.ContentPlatform.Server.Database.Tables
{
    public partial class CsNewGradeSubject
    {
        public CsNewGrade NewGrade { get; set; }
        public CsNewSubject NewSubject { get; set; }
    }
}