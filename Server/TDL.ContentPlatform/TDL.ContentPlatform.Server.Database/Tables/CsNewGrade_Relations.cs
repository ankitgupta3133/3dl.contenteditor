﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    public partial class CsNewGrade
    {
        [InverseProperty("NewGrade")]
        public List<CsNewGradeStream> CsNewGradeStreams { get; set; }
        [InverseProperty("NewGrade")]
        public List<CsNewGradeSubject> CsNewGradeSubjects { get; set; }
    }
}