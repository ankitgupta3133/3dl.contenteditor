﻿namespace TDL.ContentPlatform.Server.Database.Tables
{
    public partial class CsNewOpportunitySubscriptionMapping
    {
        public CsOpportunity NewOpportunity { get; set; }
        public CsNewSubscription NewSubscription { get; set; }
    }
}