﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    [Table("cs__new_opportunity_subscription_master_login")]
    public partial class CsNewOpportunitySubscriptionMasterLogin
    {
        public Guid Id { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? SinkCreatedOn { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? SinkModifiedOn { get; set; }
        [Column("new_masterloginid")]
        public Guid? NewMasterloginid { get; set; }
        [Column("versionnumber")]
        public long? Versionnumber { get; set; }
        [Column("new_opportunity_subscription_master_loginid")]
        public Guid? NewOpportunitySubscriptionMasterLoginid { get; set; }
        [Column("new_opportunity_subscription_mappingid")]
        public Guid? NewOpportunitySubscriptionMappingid { get; set; }
    }
}
