﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    [Table("cs__new_package")]
    public partial class CsNewPackage
    {
        public Guid Id { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? SinkCreatedOn { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? SinkModifiedOn { get; set; }
        [Column("statecode")]
        public int? Statecode { get; set; }
        [Column("statuscode")]
        public int? Statuscode { get; set; }
        [Column("owninguser")]
        public Guid? Owninguser { get; set; }
        [Column("owninguser_entitytype")]
        [StringLength(128)]
        public string OwninguserEntitytype { get; set; }
        [Column("createdonbehalfby")]
        public Guid? Createdonbehalfby { get; set; }
        [Column("createdonbehalfby_entitytype")]
        [StringLength(128)]
        public string CreatedonbehalfbyEntitytype { get; set; }
        [Column("owningbusinessunit")]
        public Guid? Owningbusinessunit { get; set; }
        [Column("owningbusinessunit_entitytype")]
        [StringLength(128)]
        public string OwningbusinessunitEntitytype { get; set; }
        [Column("owningteam")]
        public Guid? Owningteam { get; set; }
        [Column("owningteam_entitytype")]
        [StringLength(128)]
        public string OwningteamEntitytype { get; set; }
        [Column("modifiedby")]
        public Guid? Modifiedby { get; set; }
        [Column("modifiedby_entitytype")]
        [StringLength(128)]
        public string ModifiedbyEntitytype { get; set; }
        [Column("createdby")]
        public Guid? Createdby { get; set; }
        [Column("createdby_entitytype")]
        [StringLength(128)]
        public string CreatedbyEntitytype { get; set; }
        [Column("modifiedonbehalfby")]
        public Guid? Modifiedonbehalfby { get; set; }
        [Column("modifiedonbehalfby_entitytype")]
        [StringLength(128)]
        public string ModifiedonbehalfbyEntitytype { get; set; }
        [Column("ownerid")]
        public Guid? Ownerid { get; set; }
        [Column("ownerid_entitytype")]
        [StringLength(128)]
        public string OwneridEntitytype { get; set; }
        [Column("createdonbehalfbyyominame")]
        [StringLength(100)]
        public string Createdonbehalfbyyominame { get; set; }
        [Column("owneridname")]
        [StringLength(100)]
        public string Owneridname { get; set; }
        [Column("new_packageid")]
        public Guid? NewPackageid { get; set; }
        [Column("importsequencenumber")]
        public int? Importsequencenumber { get; set; }
        [Column("new_name")]
        [StringLength(100)]
        public string NewName { get; set; }
        [Column("modifiedbyyominame")]
        [StringLength(100)]
        public string Modifiedbyyominame { get; set; }
        [Column("utcconversiontimezonecode")]
        public int? Utcconversiontimezonecode { get; set; }
        [Column("createdbyyominame")]
        [StringLength(100)]
        public string Createdbyyominame { get; set; }
        [Column("modifiedbyname")]
        [StringLength(100)]
        public string Modifiedbyname { get; set; }
        [Column("timezoneruleversionnumber")]
        public int? Timezoneruleversionnumber { get; set; }
        [Column("owneridtype")]
        [StringLength(4000)]
        public string Owneridtype { get; set; }
        [Column("new_description")]
        public string NewDescription { get; set; }
        [Column("owneridyominame")]
        [StringLength(100)]
        public string Owneridyominame { get; set; }
        [Column("modifiedon", TypeName = "datetime")]
        public DateTime? Modifiedon { get; set; }
        [Column("modifiedonbehalfbyyominame")]
        [StringLength(100)]
        public string Modifiedonbehalfbyyominame { get; set; }
        [Column("createdbyname")]
        [StringLength(100)]
        public string Createdbyname { get; set; }
        [Column("createdon", TypeName = "datetime")]
        public DateTime? Createdon { get; set; }
        [Column("createdonbehalfbyname")]
        [StringLength(100)]
        public string Createdonbehalfbyname { get; set; }
        [Column("modifiedonbehalfbyname")]
        [StringLength(100)]
        public string Modifiedonbehalfbyname { get; set; }
        [Column("versionnumber")]
        public long? Versionnumber { get; set; }
        [Column("overriddencreatedon", TypeName = "datetime")]
        public DateTime? Overriddencreatedon { get; set; }
    }
}
