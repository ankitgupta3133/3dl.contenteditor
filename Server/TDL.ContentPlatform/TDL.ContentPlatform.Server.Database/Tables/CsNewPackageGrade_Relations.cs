﻿namespace TDL.ContentPlatform.Server.Database.Tables
{
    public partial class CsNewPackageGrade
    {
        public CsNewGrade NewGrade { get; set; }
        public CsNewPackage NewPackage { get; set; }
    }
}