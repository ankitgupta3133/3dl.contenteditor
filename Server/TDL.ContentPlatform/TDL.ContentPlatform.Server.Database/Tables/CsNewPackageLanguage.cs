﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    [Table("cs__new_package_language")]
    public partial class CsNewPackageLanguage
    {
        public Guid Id { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? SinkCreatedOn { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? SinkModifiedOn { get; set; }
        [Column("versionnumber")]
        public long? Versionnumber { get; set; }
        [Column("new_languageid")]
        public Guid? NewLanguageid { get; set; }
        [Column("new_package_languageid")]
        public Guid? NewPackageLanguageid { get; set; }
        [Column("new_packageid")]
        public Guid? NewPackageid { get; set; }
    }
}
