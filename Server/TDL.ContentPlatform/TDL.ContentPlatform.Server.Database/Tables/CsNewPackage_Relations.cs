﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    public partial class CsNewPackage
    {
        [InverseProperty("NewPackage")]
        public List<CsNewPackageGrade> CsNewPackageGrades { get; set; }
    }
}