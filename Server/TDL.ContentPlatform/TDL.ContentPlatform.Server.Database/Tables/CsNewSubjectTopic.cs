﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    [Table("cs__new_subject_topic")]
    public partial class CsNewSubjectTopic
    {
        public Guid Id { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? SinkCreatedOn { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? SinkModifiedOn { get; set; }
        [Column("new_subject_topicid")]
        public Guid? NewSubjectTopicid { get; set; }
        [Column("versionnumber")]
        public long? Versionnumber { get; set; }
        [Column("new_topicid")]
        public Guid? NewTopicid { get; set; }
        [Column("new_subjectid")]
        public Guid? NewSubjectid { get; set; }
    }
}
