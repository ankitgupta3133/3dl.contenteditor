﻿namespace TDL.ContentPlatform.Server.Database.Tables
{
    public partial class CsNewSubjectTopic
    {
        public CsNewTopic NewTopic { get; set; }
        public CsNewSubject NewSubject { get; set; }
    }
}