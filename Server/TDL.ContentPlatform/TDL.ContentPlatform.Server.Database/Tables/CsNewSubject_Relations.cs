﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    public partial class CsNewSubject
    {
        [InverseProperty("NewSubject")]
        public List<CsNewSubjectTopic> CsNewSubjectTopics { get; set; }

    }
}