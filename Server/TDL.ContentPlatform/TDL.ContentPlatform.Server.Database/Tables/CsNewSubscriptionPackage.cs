﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    [Table("cs__new_subscription_package")]
    public partial class CsNewSubscriptionPackage
    {
        public Guid Id { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? SinkCreatedOn { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? SinkModifiedOn { get; set; }
        [Column("versionnumber")]
        public long? Versionnumber { get; set; }
        [Column("new_subscription_packageid")]
        public Guid? NewSubscriptionPackageid { get; set; }
        [Column("new_subscriptionid")]
        public Guid? NewSubscriptionid { get; set; }
        [Column("new_packageid")]
        public Guid? NewPackageid { get; set; }
    }
}
