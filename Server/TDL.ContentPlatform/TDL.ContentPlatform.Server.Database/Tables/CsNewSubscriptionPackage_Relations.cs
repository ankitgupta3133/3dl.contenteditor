﻿namespace TDL.ContentPlatform.Server.Database.Tables
{
    public partial class CsNewSubscriptionPackage       
    {
        public CsNewSubscription NewSubscription { get; set; }
        public CsNewPackage NewPackage { get; set; }
    }
}