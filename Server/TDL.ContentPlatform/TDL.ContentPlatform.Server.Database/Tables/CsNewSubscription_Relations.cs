﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    public partial class CsNewSubscription
    {
        [InverseProperty("NewSubscription")]
        public List<CsNewSubscriptionPackage> CsNewSubscriptionPackages { get; set; }
    }
}