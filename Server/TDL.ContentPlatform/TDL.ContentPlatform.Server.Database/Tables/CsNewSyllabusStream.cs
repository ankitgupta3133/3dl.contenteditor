﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    [Table("cs__new_syllabus_stream")]
    public partial class CsNewSyllabusStream
    {
        public Guid Id { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? SinkCreatedOn { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? SinkModifiedOn { get; set; }
        [Column("versionnumber")]
        public long? Versionnumber { get; set; }
        [Column("new_streamid")]
        public Guid? NewStreamid { get; set; }
        [Column("new_syllabus_streamid")]
        public Guid? NewSyllabusStreamid { get; set; }
        [Column("new_syllabusid")]
        public Guid? NewSyllabusid { get; set; }
    }
}
