﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    [Table("cs__new_syllabus_subject")]
    public partial class CsNewSyllabusSubject
    {
        public Guid Id { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? SinkCreatedOn { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? SinkModifiedOn { get; set; }
        [Column("versionnumber")]
        public long? Versionnumber { get; set; }
        [Column("new_subjectid")]
        public Guid? NewSubjectid { get; set; }
        [Column("new_syllabusid")]
        public Guid? NewSyllabusid { get; set; }
        [Column("new_syllabus_subjectid")]
        public Guid? NewSyllabusSubjectid { get; set; }
    }
}
