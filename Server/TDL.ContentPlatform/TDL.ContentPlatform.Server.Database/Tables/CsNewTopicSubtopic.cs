﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    [Table("cs__new_topic_subtopic")]
    public partial class CsNewTopicSubtopic
    {
        public Guid Id { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? SinkCreatedOn { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? SinkModifiedOn { get; set; }
        [Column("versionnumber")]
        public long? Versionnumber { get; set; }
        [Column("new_subtopicid")]
        public Guid? NewSubtopicid { get; set; }
        [Column("new_topic_subtopicid")]
        public Guid? NewTopicSubtopicid { get; set; }
        [Column("new_topicid")]
        public Guid? NewTopicid { get; set; }
    }
}
