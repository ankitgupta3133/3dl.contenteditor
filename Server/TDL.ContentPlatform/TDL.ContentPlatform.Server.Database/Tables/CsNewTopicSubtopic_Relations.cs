﻿namespace TDL.ContentPlatform.Server.Database.Tables
{
    public partial class CsNewTopicSubtopic
    {
        public CsNewTopic NewTopic { get; set; }
        public CsNewSubtopic NewSubtopic { get; set; }
    }
}