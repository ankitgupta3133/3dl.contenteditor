﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    public partial class CsNewTopic
    {
        [InverseProperty("NewTopic")]
        public List<CsNewTopicSubtopic> CsNewTopicSubtopics { get; set; }
    }
}