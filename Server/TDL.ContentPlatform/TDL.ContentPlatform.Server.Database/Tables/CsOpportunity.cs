﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    [Table("cs__opportunity")]
    public partial class CsOpportunity
    {
        public Guid Id { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? SinkCreatedOn { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? SinkModifiedOn { get; set; }
        [Column("statecode")]
        public int? Statecode { get; set; }
        [Column("statuscode")]
        public int? Statuscode { get; set; }
        [Column("budgetstatus")]
        public int? Budgetstatus { get; set; }
        [Column("initialcommunication")]
        public int? Initialcommunication { get; set; }
        [Column("opportunityratingcode")]
        public int? Opportunityratingcode { get; set; }
        [Column("salesstage")]
        public int? Salesstage { get; set; }
        [Column("timeline")]
        public int? Timeline { get; set; }
        [Column("purchasetimeframe")]
        public int? Purchasetimeframe { get; set; }
        [Column("salesstagecode")]
        public int? Salesstagecode { get; set; }
        [Column("pricingerrorcode")]
        public int? Pricingerrorcode { get; set; }
        [Column("purchaseprocess")]
        public int? Purchaseprocess { get; set; }
        [Column("prioritycode")]
        public int? Prioritycode { get; set; }
        [Column("need")]
        public int? Need { get; set; }
        [Column("presentfinalproposal")]
        public bool? Presentfinalproposal { get; set; }
        [Column("developproposal")]
        public bool? Developproposal { get; set; }
        [Column("isrevenuesystemcalculated")]
        public bool? Isrevenuesystemcalculated { get; set; }
        [Column("confirminterest")]
        public bool? Confirminterest { get; set; }
        [Column("presentproposal")]
        public bool? Presentproposal { get; set; }
        [Column("completefinalproposal")]
        public bool? Completefinalproposal { get; set; }
        [Column("identifypursuitteam")]
        public bool? Identifypursuitteam { get; set; }
        [Column("pursuitdecision")]
        public bool? Pursuitdecision { get; set; }
        [Column("filedebrief")]
        public bool? Filedebrief { get; set; }
        [Column("sendthankyounote")]
        public bool? Sendthankyounote { get; set; }
        [Column("evaluatefit")]
        public bool? Evaluatefit { get; set; }
        [Column("identifycompetitors")]
        public bool? Identifycompetitors { get; set; }
        [Column("completeinternalreview")]
        public bool? Completeinternalreview { get; set; }
        [Column("captureproposalfeedback")]
        public bool? Captureproposalfeedback { get; set; }
        [Column("participatesinworkflow")]
        public bool? Participatesinworkflow { get; set; }
        [Column("decisionmaker")]
        public bool? Decisionmaker { get; set; }
        [Column("isprivate")]
        public bool? Isprivate { get; set; }
        [Column("resolvefeedback")]
        public bool? Resolvefeedback { get; set; }
        [Column("identifycustomercontacts")]
        public bool? Identifycustomercontacts { get; set; }
        [Column("slaid")]
        public Guid? Slaid { get; set; }
        [Column("slaid_entitytype")]
        [StringLength(128)]
        public string SlaidEntitytype { get; set; }
        [Column("slainvokedid")]
        public Guid? Slainvokedid { get; set; }
        [Column("slainvokedid_entitytype")]
        [StringLength(128)]
        public string SlainvokedidEntitytype { get; set; }
        [Column("owningbusinessunit")]
        public Guid? Owningbusinessunit { get; set; }
        [Column("owningbusinessunit_entitytype")]
        [StringLength(128)]
        public string OwningbusinessunitEntitytype { get; set; }
        [Column("accountid")]
        public Guid? Accountid { get; set; }
        [Column("accountid_entitytype")]
        [StringLength(128)]
        public string AccountidEntitytype { get; set; }
        [Column("parentcontactid")]
        public Guid? Parentcontactid { get; set; }
        [Column("parentcontactid_entitytype")]
        [StringLength(128)]
        public string ParentcontactidEntitytype { get; set; }
        [Column("contactid")]
        public Guid? Contactid { get; set; }
        [Column("contactid_entitytype")]
        [StringLength(128)]
        public string ContactidEntitytype { get; set; }
        [Column("parentaccountid")]
        public Guid? Parentaccountid { get; set; }
        [Column("parentaccountid_entitytype")]
        [StringLength(128)]
        public string ParentaccountidEntitytype { get; set; }
        [Column("pricelevelid")]
        public Guid? Pricelevelid { get; set; }
        [Column("pricelevelid_entitytype")]
        [StringLength(128)]
        public string PricelevelidEntitytype { get; set; }
        [Column("modifiedby")]
        public Guid? Modifiedby { get; set; }
        [Column("modifiedby_entitytype")]
        [StringLength(128)]
        public string ModifiedbyEntitytype { get; set; }
        [Column("modifiedonbehalfby")]
        public Guid? Modifiedonbehalfby { get; set; }
        [Column("modifiedonbehalfby_entitytype")]
        [StringLength(128)]
        public string ModifiedonbehalfbyEntitytype { get; set; }
        [Column("owningteam")]
        public Guid? Owningteam { get; set; }
        [Column("owningteam_entitytype")]
        [StringLength(128)]
        public string OwningteamEntitytype { get; set; }
        [Column("owninguser")]
        public Guid? Owninguser { get; set; }
        [Column("owninguser_entitytype")]
        [StringLength(128)]
        public string OwninguserEntitytype { get; set; }
        [Column("createdonbehalfby")]
        public Guid? Createdonbehalfby { get; set; }
        [Column("createdonbehalfby_entitytype")]
        [StringLength(128)]
        public string CreatedonbehalfbyEntitytype { get; set; }
        [Column("transactioncurrencyid")]
        public Guid? Transactioncurrencyid { get; set; }
        [Column("transactioncurrencyid_entitytype")]
        [StringLength(128)]
        public string TransactioncurrencyidEntitytype { get; set; }
        [Column("campaignid")]
        public Guid? Campaignid { get; set; }
        [Column("campaignid_entitytype")]
        [StringLength(128)]
        public string CampaignidEntitytype { get; set; }
        [Column("originatingleadid")]
        public Guid? Originatingleadid { get; set; }
        [Column("originatingleadid_entitytype")]
        [StringLength(128)]
        public string OriginatingleadidEntitytype { get; set; }
        [Column("createdby")]
        public Guid? Createdby { get; set; }
        [Column("createdby_entitytype")]
        [StringLength(128)]
        public string CreatedbyEntitytype { get; set; }
        [Column("ownerid")]
        public Guid? Ownerid { get; set; }
        [Column("ownerid_entitytype")]
        [StringLength(128)]
        public string OwneridEntitytype { get; set; }
        [Column("customerid")]
        public Guid? Customerid { get; set; }
        [Column("customerid_entitytype")]
        [StringLength(128)]
        public string CustomeridEntitytype { get; set; }
        [Column("totallineitemdiscountamount", TypeName = "decimal(38, 2)")]
        public decimal? Totallineitemdiscountamount { get; set; }
        [Column("budgetamount_base", TypeName = "decimal(38, 4)")]
        public decimal? BudgetamountBase { get; set; }
        [Column("freightamount", TypeName = "decimal(38, 2)")]
        public decimal? Freightamount { get; set; }
        [Column("totallineitemamount_base", TypeName = "decimal(38, 4)")]
        public decimal? TotallineitemamountBase { get; set; }
        [Column("totallineitemdiscountamount_base", TypeName = "decimal(38, 4)")]
        public decimal? TotallineitemdiscountamountBase { get; set; }
        [Column("totaldiscountamount", TypeName = "decimal(38, 2)")]
        public decimal? Totaldiscountamount { get; set; }
        [Column("discountamount_base", TypeName = "decimal(38, 4)")]
        public decimal? DiscountamountBase { get; set; }
        [Column("freightamount_base", TypeName = "decimal(38, 4)")]
        public decimal? FreightamountBase { get; set; }
        [Column("estimatedvalue", TypeName = "decimal(38, 2)")]
        public decimal? Estimatedvalue { get; set; }
        [Column("actualvalue_base", TypeName = "decimal(38, 4)")]
        public decimal? ActualvalueBase { get; set; }
        [Column("totalamountlessfreight_base", TypeName = "decimal(38, 4)")]
        public decimal? TotalamountlessfreightBase { get; set; }
        [Column("totalamountlessfreight", TypeName = "decimal(38, 2)")]
        public decimal? Totalamountlessfreight { get; set; }
        [Column("totallineitemamount", TypeName = "decimal(38, 2)")]
        public decimal? Totallineitemamount { get; set; }
        [Column("actualvalue", TypeName = "decimal(38, 2)")]
        public decimal? Actualvalue { get; set; }
        [Column("budgetamount", TypeName = "decimal(38, 2)")]
        public decimal? Budgetamount { get; set; }
        [Column("estimatedvalue_base", TypeName = "decimal(38, 4)")]
        public decimal? EstimatedvalueBase { get; set; }
        [Column("totaldiscountamount_base", TypeName = "decimal(38, 4)")]
        public decimal? TotaldiscountamountBase { get; set; }
        [Column("totalamount", TypeName = "decimal(38, 2)")]
        public decimal? Totalamount { get; set; }
        [Column("totalamount_base", TypeName = "decimal(38, 4)")]
        public decimal? TotalamountBase { get; set; }
        [Column("totaltax", TypeName = "decimal(38, 2)")]
        public decimal? Totaltax { get; set; }
        [Column("discountamount", TypeName = "decimal(38, 2)")]
        public decimal? Discountamount { get; set; }
        [Column("totaltax_base", TypeName = "decimal(38, 4)")]
        public decimal? TotaltaxBase { get; set; }
        [Column("finaldecisiondate", TypeName = "datetime")]
        public DateTime? Finaldecisiondate { get; set; }
        [Column("modifiedon", TypeName = "datetime")]
        public DateTime? Modifiedon { get; set; }
        [Column("overriddencreatedon", TypeName = "datetime")]
        public DateTime? Overriddencreatedon { get; set; }
        [Column("contactidname")]
        [StringLength(100)]
        public string Contactidname { get; set; }
        [Column("transactioncurrencyidname")]
        [StringLength(100)]
        public string Transactioncurrencyidname { get; set; }
        [Column("estimatedclosedate", TypeName = "datetime")]
        public DateTime? Estimatedclosedate { get; set; }
        [Column("parentaccountidname")]
        [StringLength(100)]
        public string Parentaccountidname { get; set; }
        [Column("originatingleadidname")]
        [StringLength(100)]
        public string Originatingleadidname { get; set; }
        [Column("contactidyominame")]
        [StringLength(100)]
        public string Contactidyominame { get; set; }
        [Column("name")]
        [StringLength(300)]
        public string Name { get; set; }
        [Column("parentaccountidyominame")]
        [StringLength(100)]
        public string Parentaccountidyominame { get; set; }
        [Column("exchangerate", TypeName = "decimal(38, 10)")]
        public decimal? Exchangerate { get; set; }
        [Column("createdon", TypeName = "datetime")]
        public DateTime? Createdon { get; set; }
        [Column("slaname")]
        [StringLength(100)]
        public string Slaname { get; set; }
        [Column("customeridyominame")]
        [StringLength(450)]
        public string Customeridyominame { get; set; }
        [Column("actualclosedate", TypeName = "datetime")]
        public DateTime? Actualclosedate { get; set; }
        [Column("stepid")]
        public Guid? Stepid { get; set; }
        [Column("customeridtype")]
        [StringLength(4000)]
        public string Customeridtype { get; set; }
        [Column("onholdtime")]
        public int? Onholdtime { get; set; }
        [Column("owneridyominame")]
        [StringLength(100)]
        public string Owneridyominame { get; set; }
        [Column("description")]
        public string Description { get; set; }
        [Column("stepname")]
        [StringLength(200)]
        public string Stepname { get; set; }
        [Column("accountidname")]
        [StringLength(100)]
        public string Accountidname { get; set; }
        [Column("originatingleadidyominame")]
        [StringLength(100)]
        public string Originatingleadidyominame { get; set; }
        [Column("traversedpath")]
        [StringLength(1250)]
        public string Traversedpath { get; set; }
        [Column("createdonbehalfbyname")]
        [StringLength(100)]
        public string Createdonbehalfbyname { get; set; }
        [Column("schedulefollowup_qualify", TypeName = "datetime")]
        public DateTime? SchedulefollowupQualify { get; set; }
        [Column("owneridtype")]
        [StringLength(4000)]
        public string Owneridtype { get; set; }
        [Column("timespentbymeonemailandmeetings")]
        [StringLength(1250)]
        public string Timespentbymeonemailandmeetings { get; set; }
        [Column("modifiedonbehalfbyname")]
        [StringLength(100)]
        public string Modifiedonbehalfbyname { get; set; }
        [Column("createdonbehalfbyyominame")]
        [StringLength(100)]
        public string Createdonbehalfbyyominame { get; set; }
        [Column("qualificationcomments")]
        public string Qualificationcomments { get; set; }
        [Column("opportunityid")]
        public Guid? Opportunityid { get; set; }
        [Column("processid")]
        public Guid? Processid { get; set; }
        [Column("emailaddress")]
        [StringLength(100)]
        public string Emailaddress { get; set; }
        [Column("customerneed")]
        public string Customerneed { get; set; }
        [Column("parentcontactidyominame")]
        [StringLength(100)]
        public string Parentcontactidyominame { get; set; }
        [Column("slainvokedidname")]
        [StringLength(100)]
        public string Slainvokedidname { get; set; }
        [Column("discountpercentage", TypeName = "decimal(38, 2)")]
        public decimal? Discountpercentage { get; set; }
        [Column("scheduleproposalmeeting", TypeName = "datetime")]
        public DateTime? Scheduleproposalmeeting { get; set; }
        [Column("timezoneruleversionnumber")]
        public int? Timezoneruleversionnumber { get; set; }
        [Column("createdbyname")]
        [StringLength(100)]
        public string Createdbyname { get; set; }
        [Column("modifiedbyyominame")]
        [StringLength(100)]
        public string Modifiedbyyominame { get; set; }
        [Column("lastonholdtime", TypeName = "datetime")]
        public DateTime? Lastonholdtime { get; set; }
        [Column("schedulefollowup_prospect", TypeName = "datetime")]
        public DateTime? SchedulefollowupProspect { get; set; }
        [Column("modifiedonbehalfbyyominame")]
        [StringLength(100)]
        public string Modifiedonbehalfbyyominame { get; set; }
        [Column("stageid")]
        public Guid? Stageid { get; set; }
        [Column("utcconversiontimezonecode")]
        public int? Utcconversiontimezonecode { get; set; }
        [Column("customeridname")]
        [StringLength(160)]
        public string Customeridname { get; set; }
        [Column("importsequencenumber")]
        public int? Importsequencenumber { get; set; }
        [Column("customerpainpoints")]
        public string Customerpainpoints { get; set; }
        [Column("versionnumber")]
        public long? Versionnumber { get; set; }
        [Column("pricelevelidname")]
        [StringLength(100)]
        public string Pricelevelidname { get; set; }
        [Column("campaignidname")]
        [StringLength(100)]
        public string Campaignidname { get; set; }
        [Column("quotecomments")]
        public string Quotecomments { get; set; }
        [Column("parentcontactidname")]
        [StringLength(100)]
        public string Parentcontactidname { get; set; }
        [Column("modifiedbyname")]
        [StringLength(100)]
        public string Modifiedbyname { get; set; }
        [Column("createdbyyominame")]
        [StringLength(100)]
        public string Createdbyyominame { get; set; }
        [Column("owneridname")]
        [StringLength(100)]
        public string Owneridname { get; set; }
        [Column("proposedsolution")]
        public string Proposedsolution { get; set; }
        [Column("accountidyominame")]
        [StringLength(100)]
        public string Accountidyominame { get; set; }
        [Column("closeprobability")]
        public int? Closeprobability { get; set; }
        [Column("currentsituation")]
        public string Currentsituation { get; set; }
    }
}
