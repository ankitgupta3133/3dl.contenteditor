﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    public partial class CsOpportunity
    {
        //[ForeignKey("Parentaccountid")]
        public CsAccount Parentaccount { get; set; }

        [InverseProperty("NewOpportunity")]
        public List<CsNewOpportunitySubscriptionMapping> CsNewOpportunitySubscriptionMappings { get; set; }
    }
}