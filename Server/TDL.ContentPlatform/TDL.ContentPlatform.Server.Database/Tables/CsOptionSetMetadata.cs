﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    [Table("cs__OptionSetMetadata")]
    public partial class CsOptionSetMetadata
    {
        [StringLength(64)]
        public string EntityName { get; set; }
        [StringLength(64)]
        public string OptionSetName { get; set; }
        public int Option { get; set; }
        public bool IsUserLocalizedLabel { get; set; }
        public int LocalizedLabelLanguageCode { get; set; }
        [StringLength(350)]
        public string LocalizedLabel { get; set; }
    }
}
