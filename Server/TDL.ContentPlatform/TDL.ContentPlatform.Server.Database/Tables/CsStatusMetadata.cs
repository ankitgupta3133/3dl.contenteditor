﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    [Table("cs__StatusMetadata")]
    public partial class CsStatusMetadata
    {
        [StringLength(64)]
        public string EntityName { get; set; }
        public int State { get; set; }
        public int Status { get; set; }
        public bool IsUserLocalizedLabel { get; set; }
        public int LocalizedLabelLanguageCode { get; set; }
        [StringLength(350)]
        public string LocalizedLabel { get; set; }
    }
}
