﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    [Table("cs__TargetMetadata")]
    public partial class CsTargetMetadata
    {
        [StringLength(64)]
        public string EntityName { get; set; }
        [StringLength(64)]
        public string AttributeName { get; set; }
        [StringLength(64)]
        public string ReferencedEntity { get; set; }
        [StringLength(64)]
        public string ReferencedAttribute { get; set; }
    }
}
