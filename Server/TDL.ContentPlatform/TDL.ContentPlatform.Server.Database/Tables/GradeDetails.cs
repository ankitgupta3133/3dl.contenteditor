﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    public partial class GradeDetails
    {
        [Column("ID")]
        public int Id { get; set; }
        [Column("GradeID")]
        public int? GradeId { get; set; }
        [Column("SyllabusID")]
        public int? SyllabusId { get; set; }
        [Column("SubjectID")]
        public int? SubjectId { get; set; }
        [Column("StreamID")]
        public int? StreamId { get; set; }
        public bool? IsDeleted { get; set; }

        [ForeignKey("GradeId")]
        [InverseProperty("GradeDetails")]
        public MstLevel Grade { get; set; }
        [ForeignKey("StreamId")]
        [InverseProperty("GradeDetails")]
        public MstStream Stream { get; set; }
        [ForeignKey("SubjectId")]
        [InverseProperty("GradeDetails")]
        public MstSubject Subject { get; set; }
        [ForeignKey("SyllabusId")]
        [InverseProperty("GradeDetails")]
        public MstSyllabus Syllabus { get; set; }
    }
}
