﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    public partial class LogMetaData
    {
        [Column("ID")]
        public int Id { get; set; }
        [StringLength(200)]
        public string RequestUri { get; set; }
        [StringLength(100)]
        public string RequestMethod { get; set; }
        public string RequestData { get; set; }
        [StringLength(100)]
        public string RequestContentType { get; set; }
        [Column("RequestIPAddress")]
        [StringLength(100)]
        public string RequestIpaddress { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? RequestTimestamp { get; set; }
        [StringLength(100)]
        public string ResponseContentType { get; set; }
        [StringLength(100)]
        public string ResponseStatusCode { get; set; }
        public string ResponseData { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ResponseTimestamp { get; set; }
        public Guid? RequestBy { get; set; }
        public bool? IsDeleted { get; set; }
    }
}
