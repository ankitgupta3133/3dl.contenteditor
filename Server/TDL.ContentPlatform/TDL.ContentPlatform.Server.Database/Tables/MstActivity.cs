﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    public partial class MstActivity
    {
        [Key]
        public int ActivityId { get; set; }
        [StringLength(100)]
        public string ActivityName { get; set; }
        public Guid? CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedOn { get; set; }
        public Guid? ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedOn { get; set; }
        public bool? IsDeleted { get; set; }
        public bool? IsActive { get; set; }
        [InverseProperty("MstActivity")]
        public ICollection<Activities> Activity { get; set; }
        public MstActivity()
        {
            Activity = new HashSet<Activities>();
        }
    }
}
