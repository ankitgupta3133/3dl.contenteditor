﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    public partial class MstCompound
    {
        [Key]
        public int CompoundId { get; set; }
        [StringLength(100)]
        public string CompundName { get; set; }
        [StringLength(100)]
        public string CommonName { get; set; }
        [StringLength(1000)]
        public string CompoundFormulae { get; set; }
        public int? SphericalAssetId { get; set; }
        public int? BondedAssetId { get; set; }
        public int? MaterialId { get; set; }
        [StringLength(50)]
        public string Color { get; set; }
        [StringLength(100)]
        public string WikiLink { get; set; }
        [MaxLength]
        public string Discription { get; set; }
        public Guid? CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedOn { get; set; }
        public Guid? ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedOn { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDeleted { get; set; }
        [ForeignKey("BondedAssetId")]
        public virtual Asset BondedAssets { get; set; }
        [ForeignKey("SphericalAssetId")]
        public virtual Asset SphericalAssets { get; set; }
        [ForeignKey("MaterialId")]
        public virtual MstMaterial MstMaterial { get; set; }
        [InverseProperty("MstCompound")]
        public ICollection<CompoundElement> CompoundElement { get; set; }
        public MstCompound()
        {
            CompoundElement = new HashSet<CompoundElement>();
        }
    }
}
