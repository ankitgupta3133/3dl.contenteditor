﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    public partial class MstElement
    {
        [Key]
        public int ElementId { get; set; }
        [StringLength(100)]
        public string ElementName { get; set; }
        [StringLength(10)]
        public string ElementShortName { get; set; }
        public int AtomicNumber { get; set; }
        public bool? IsDeleted { get; set; }
        [InverseProperty("MstElement")]
        public ICollection<CompoundElement> CompoundElement { get; set; }
        public MstElement()
        {
            CompoundElement = new HashSet<CompoundElement>();
        }
    }
}
