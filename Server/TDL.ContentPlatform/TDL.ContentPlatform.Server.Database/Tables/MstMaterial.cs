﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    public partial class MstMaterial
    {
        [Key]
        public int MaterialId { get; set; }
        [StringLength(100)]
        public string MaterialName { get; set; }
        public Guid? CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedOn { get; set; }
        public Guid? ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedOn { get; set; }
        public bool? IsDeleted { get; set; }
        public bool? IsActive { get; set; }
        [InverseProperty("MstMaterial")]
        public ICollection<MstCompound> Compounds { get; set; }
        public MstMaterial()
        {
            Compounds = new HashSet<MstCompound>();
        }
    }
}
