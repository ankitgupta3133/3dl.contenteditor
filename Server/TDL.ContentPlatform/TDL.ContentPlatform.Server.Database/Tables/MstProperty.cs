﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    public partial class MstProperty
    {
        [Key]
        [Column("PropertyID")]
        public int PropertyId { get; set; }
        [StringLength(100)]
        public string PropertyName { get; set; }
        public Guid? LanguageId { get; set; }
        public Guid? CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedOn { get; set; }
        public Guid? ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedOn { get; set; }
        public bool? IsDeleted { get; set; }
        public bool? IsActive { get; set; }
        [InverseProperty("MstProperty")]
        public virtual ICollection<ClassificationDetails> ClassificationDetails { get; set; }

        [InverseProperty("MstProperty")]
        public virtual ICollection<MstPropertyNames> PropertyNames { get; set; }

        [InverseProperty("MstProperty")]
        public virtual ICollection<AssetProperties> AssetProperties { get; set; }

        public MstProperty()
        {
            PropertyNames = new HashSet<MstPropertyNames>();
            AssetProperties = new HashSet<AssetProperties>();
            ClassificationDetails = new HashSet<ClassificationDetails>();
        }
    }
}
