﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    public partial class MstPropertyNames
    {
        [Key]
        public int Id { get; set; }
        public int PropertyId { get; set; }
        [StringLength(100)]
        public string Name { get; set; }

        public Guid LanguageId { get; set; }
        public bool IsDeleted { get; set; }

        [ForeignKey("PropertyId")]
        public MstProperty MstProperty { get; set; }
    }
}