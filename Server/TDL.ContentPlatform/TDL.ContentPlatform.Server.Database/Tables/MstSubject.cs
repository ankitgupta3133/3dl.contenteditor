﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    public partial class MstSubject
    {
        public MstSubject()
        {
            MstTopic = new HashSet<MstTopic>();
        }

        [Key]
        [Column("SubjectID")]
        public int SubjectId { get; set; }
        [StringLength(100)]
        public string SubjectName { get; set; }
        public Guid? CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedOn { get; set; }
        public Guid? ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedOn { get; set; }
        public bool? IsDeleted { get; set; }
        public bool? IsActive { get; set; }

        [InverseProperty("Subject")]
        public ICollection<MstTopic> MstTopic { get; set; }
    }
}
