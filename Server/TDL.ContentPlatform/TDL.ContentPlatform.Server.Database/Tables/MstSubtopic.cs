﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    public partial class MstSubtopic
    {
        [Key]
        [Column("SubtopicID")]
        public int SubtopicId { get; set; }
        [StringLength(100)]
        public string SubtopicName { get; set; }
        public int? TopicId { get; set; }
        public Guid? CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedOn { get; set; }
        public Guid? ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedOn { get; set; }
        public bool? IsDeleted { get; set; }
        public bool? IsActive { get; set; }
        [Column("PSubtopicID")]
        public int? PsubtopicId { get; set; }
        [Column("PSubtopicName")]
        [StringLength(100)]
        public string PsubtopicName { get; set; }

        [ForeignKey("TopicId")]
        [InverseProperty("MstSubtopic")]
        public MstTopic Topic { get; set; }
    }
}
