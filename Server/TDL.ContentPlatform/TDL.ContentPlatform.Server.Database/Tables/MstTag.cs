﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    public partial class MstTag
    {
        [Key]
        [Column("TagID")]
        public int TagId { get; set; }
        [StringLength(100)]
        public string TagName { get; set; }
        public Guid? CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedOn { get; set; }
        public Guid? ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedOn { get; set; }
        public bool? IsDeleted { get; set; }
        public bool? IsActive { get; set; }
        public ICollection<MstTagNames> TagNames { get; set; }

        [InverseProperty("MstTags")]
        public ICollection<AssetTags> AssetTags { get; set; }

        public MstTag()
        {
            TagNames = new HashSet<MstTagNames>();
            AssetTags = new HashSet<AssetTags>();
        }
    }
}
