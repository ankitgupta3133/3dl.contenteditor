﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    public partial class MstTagNames
    {
        [Key]
        public int Id { get; set; }
        public int TagId { get; set; }
        [StringLength(100)]
        public string Name { get; set; }

        public Guid LanguageId { get; set; }
        public bool IsDeleted { get; set; }

        [ForeignKey("TagId")]
        public MstTag MstTag { get; set; }
    }
}