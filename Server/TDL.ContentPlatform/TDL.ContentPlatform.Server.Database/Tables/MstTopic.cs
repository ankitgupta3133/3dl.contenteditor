﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    public partial class MstTopic
    {
        public MstTopic()
        {
            MstSubtopic = new HashSet<MstSubtopic>();
        }

        [Key]
        [Column("TopicID")]
        public int TopicId { get; set; }
        [StringLength(100)]
        public string TopicName { get; set; }
        public int? SubjectId { get; set; }
        public Guid? CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedOn { get; set; }
        public Guid? ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedOn { get; set; }
        public bool? IsDeleted { get; set; }
        public bool? IsActive { get; set; }

        [ForeignKey("SubjectId")]
        [InverseProperty("MstTopic")]
        public MstSubject Subject { get; set; }
        [InverseProperty("Topic")]
        public ICollection<MstSubtopic> MstSubtopic { get; set; }
    }
}
