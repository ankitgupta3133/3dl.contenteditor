﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    [Table("NLog")]
    public partial class Nlog
    {
        public int Id { get; set; }
        [StringLength(200)]
        public string MachineName { get; set; }
        [Required]
        [StringLength(200)]
        public string SiteName { get; set; }
        public string Properties { get; set; }
        public DateTime Logged { get; set; }
        [Required]
        [StringLength(5)]
        public string Level { get; set; }
        [Required]
        public string Message { get; set; }
        [StringLength(200)]
        public string UserName { get; set; }
        [StringLength(200)]
        public string ServerName { get; set; }
        [StringLength(100)]
        public string Port { get; set; }
        [StringLength(2000)]
        public string Url { get; set; }
        public bool? Https { get; set; }
        [StringLength(100)]
        public string ServerAddress { get; set; }
        [StringLength(100)]
        public string RemoteAddress { get; set; }
        [StringLength(300)]
        public string Logger { get; set; }
        [StringLength(300)]
        public string Callsite { get; set; }
        public string Exception { get; set; }
    }
}
