﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    public partial class Opportunity
    {
        public Opportunity()
        {
            OpportunitySubscription = new HashSet<OpportunitySubscription>();
        }

        [Column("ID")]
        public Guid Id { get; set; }
        [StringLength(100)]
        public string Name { get; set; }
        public string Description { get; set; }
        [Column("AccountID")]
        public Guid? AccountId { get; set; }
        public Guid? CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedOn { get; set; }
        public Guid? ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedOn { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDeleted { get; set; }

        [ForeignKey("AccountId")]
        [InverseProperty("Opportunity")]
        public Account Account { get; set; }
        [InverseProperty("Opportunity")]
        public ICollection<OpportunitySubscription> OpportunitySubscription { get; set; }
    }
}
