﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    public partial class OpportunitySubscription
    {
        [Column("ID")]
        public int Id { get; set; }
        [StringLength(100)]
        public string Name { get; set; }
        public string Description { get; set; }
        [Column("OpportunityID")]
        public Guid? OpportunityId { get; set; }
        [Column("SubscriptionID")]
        public Guid? SubscriptionId { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? StartDate { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? EndDate { get; set; }
        public Guid? CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedOn { get; set; }
        public Guid? ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedOn { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDeleted { get; set; }

        [ForeignKey("OpportunityId")]
        [InverseProperty("OpportunitySubscription")]
        public Opportunity Opportunity { get; set; }
        [ForeignKey("SubscriptionId")]
        [InverseProperty("OpportunitySubscription")]
        public Subscription Subscription { get; set; }
    }
}
