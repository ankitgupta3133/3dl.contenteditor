﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    public partial class PackageDetails
    {
        [Column("ID")]
        public int Id { get; set; }
        [Column("PackageID")]
        public int? PackageId { get; set; }
        [Column("LanguageID")]
        public int? LanguageId { get; set; }
        [Column("GradeID")]
        public int? GradeId { get; set; }
        public bool? IsDeleted { get; set; }

        [ForeignKey("GradeId")]
        [InverseProperty("PackageDetails")]
        public MstLevel Grade { get; set; }
        [ForeignKey("LanguageId")]
        [InverseProperty("PackageDetails")]
        public MstLanguage Language { get; set; }
        [ForeignKey("PackageId")]
        [InverseProperty("PackageDetails")]
        public ContentPackage Package { get; set; }
    }
}
