﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    public partial class PuzzleDetails
    {
        [Key]
        public int Id { get; set; }
        public int PuzzleId { get; set; }
        public int AssetId { get; set; }
        public int AssetContentId { get; set; }
        public int NumberOfParts { get; set; }
        public Guid? CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedOn { get; set; }
        public Guid? ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedOn { get; set; }
        public bool? IsEnabled { get; set; }
        public bool? IsDeleted { get; set; }
        public int ModelId { get; set; }
        public string Labels { get; set; }
        [ForeignKey("AssetId")]
        [InverseProperty("PuzzleDetails")]
        public Asset Asset { get; set; }
        [ForeignKey("AssetContentId")]
        [InverseProperty("PuzzleDetails")]
        public AssetContent AssetContent { get; set; }
        [ForeignKey("PuzzleId")]
        [InverseProperty("PuzzleDetails")]
        public Activities Activity { get; set; }
        [ForeignKey("ModelId")]
        [InverseProperty("PuzzleDetails")]
        public XmlcontentModel XmlcontentModel { get; set; }
    }
}
