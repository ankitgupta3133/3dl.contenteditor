﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    public partial class QuizDetails
    {
        [Key]
        public int Id { get; set; }
        public int QuizId { get; set; }
        public int AssetId { get; set; }
        public int AssetContentId { get; set; }
        public int NumberOfAttempts { get; set; }
        public int NumberOfLabels { get; set; }
        public Guid? CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedOn { get; set; }
        public Guid? ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedOn { get; set; }
        public bool? IsEnabled { get; set; }
        public bool? IsDeleted { get; set; }
        public int ModelId { get; set; }
        public string Labels { get; set; }
        [ForeignKey("AssetId")]
        [InverseProperty("QuizDetails")]
        public Asset Asset { get; set; }
        [ForeignKey("AssetContentId")]
        [InverseProperty("QuizDetails")]
        public AssetContent AssetContent { get; set; }
        [ForeignKey("QuizId")]
        [InverseProperty("QuizDetails")]
        public Activities Activity { get; set; }
        [ForeignKey("ModelId")]
        [InverseProperty("QuizDetails")]
        public XmlcontentModel XmlcontentModel { get; set; }
        [InverseProperty("QuizDetails")]
        public ICollection<QuizLabelDetails> QuizLabelDetails { get; set; }
        public QuizDetails()
        {
            QuizLabelDetails = new HashSet<QuizLabelDetails>();
        }
    }
}
