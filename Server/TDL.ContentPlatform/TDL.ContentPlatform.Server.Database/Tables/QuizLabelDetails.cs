﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    public partial class QuizLabelDetails
    {
        [Key]
        public int Id { get; set; }
        public int QuizDetailId { get; set; }
        public int LabelId { get; set; }
        public string LabelName { get; set; }
        public Guid? CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedOn { get; set; }
        public Guid? ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedOn { get; set; }
        public bool? IsEnabled { get; set; }
        public bool? IsDeleted { get; set; }
        [ForeignKey("LabelId")]
        [InverseProperty("QuizLabelDetails")]
        public XmlcontentLabel XmlcontentLabel { get; set; }
        [ForeignKey("QuizDetailId")]
        [InverseProperty("QuizLabelDetails")]
        public QuizDetails QuizDetails { get; set; }
    }
}
