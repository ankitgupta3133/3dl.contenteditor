﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    public partial class Subscription
    {
        public Subscription()
        {
            OpportunitySubscription = new HashSet<OpportunitySubscription>();
            SubscriptionPackage = new HashSet<SubscriptionPackage>();
        }

        [Column("ID")]
        public Guid Id { get; set; }
        [StringLength(100)]
        public string Name { get; set; }
        public string Description { get; set; }
        public int? Duration { get; set; }
        public Guid? CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedOn { get; set; }
        public Guid? ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedOn { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDeleted { get; set; }

        [InverseProperty("Subscription")]
        public ICollection<OpportunitySubscription> OpportunitySubscription { get; set; }
        [InverseProperty("Subscription")]
        public ICollection<SubscriptionPackage> SubscriptionPackage { get; set; }
    }
}
