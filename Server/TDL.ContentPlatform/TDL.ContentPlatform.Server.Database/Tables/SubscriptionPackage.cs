﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    public partial class SubscriptionPackage
    {
        [Column("ID")]
        public int Id { get; set; }
        [Column("SubscriptionID")]
        public Guid? SubscriptionId { get; set; }
        [Column("PackageID")]
        public int? PackageId { get; set; }
        public bool? IsDeleted { get; set; }

        [ForeignKey("PackageId")]
        [InverseProperty("SubscriptionPackage")]
        public ContentPackage Package { get; set; }
        [ForeignKey("SubscriptionId")]
        [InverseProperty("SubscriptionPackage")]
        public Subscription Subscription { get; set; }
    }
}
