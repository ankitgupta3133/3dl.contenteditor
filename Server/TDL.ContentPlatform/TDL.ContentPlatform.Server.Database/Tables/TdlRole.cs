﻿using System;
using Microsoft.AspNetCore.Identity;
using RT.Comb;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    public class TdlRole : IdentityRole<Guid>
    {
        public TdlRole()
        {
            Id = Provider.Sql.Create();

        }

        public TdlRole(string name)
        {
            Id = Provider.Sql.Create();
            Name = name;
        }
    }
}