﻿using System;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using RT.Comb;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    public class TdlUser : IdentityUser<Guid>
    {
        public Guid SessionKey { get; set; }
        public Guid Account { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public Boolean IsActive { get; set; }

        public TdlUser()
        {
            Id = Provider.Sql.Create();
            IsActive = true;
        }

        public TdlUser(string username)
        {
            Id = Provider.Sql.Create();
            UserName = username;
        }
    }
}