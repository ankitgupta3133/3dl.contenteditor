﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    public partial class UserAccount
    {
        [Column("ID")]
        public int Id { get; set; }
        [Column("UserID")]
        public Guid? UserId { get; set; }
        [Column("AccountID")]
        public Guid? AccountId { get; set; }
        public Guid? CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedOn { get; set; }
        public Guid? ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedOn { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDeleted { get; set; }

        [ForeignKey("AccountId")]
        [InverseProperty("UserAccount")]
        public Account Account { get; set; }
        [ForeignKey("UserId")]
        [InverseProperty("UserAccount")]
        public Users User { get; set; }
    }
}
