﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    public partial class UserAssets
    {
        [Column("ID")]
        public int Id { get; set; }
        [Column("UserID")]
        public Guid? UserId { get; set; }
        [Column("AssetID")]
        public int? AssetId { get; set; }
        [Column("AssetContentID")]
        public int? AssetContentId { get; set; }
        [StringLength(100)]
        public string DownloadStatus { get; set; }
        public bool? IsDownloaded { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? DownloadedOn { get; set; }
        public bool? IsDeleted { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? DeletedOn { get; set; }

        [ForeignKey("AssetId")]
        [InverseProperty("UserAssets")]
        public Asset Asset { get; set; }
        [ForeignKey("AssetContentId")]
        [InverseProperty("UserAssets")]
        public AssetContent AssetContent { get; set; }
    }
}
