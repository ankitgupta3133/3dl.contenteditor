﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    public partial class UserDetailPlatform
    {
        public int Id { get; set; }
        public int UserDetailId { get; set; }
        public Guid PlatformId { get; set; }
        [ForeignKey("PlatformId")]
        public CsNewPlatform Platform { get; set; }
        public Guid? CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedOn { get; set; }
    }
}
