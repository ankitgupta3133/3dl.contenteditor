﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    public partial class UserDetails
    {
        public UserDetails()
        {
        }

        public int Id { get; set; }
        public Guid UserId { get; set; }
        public Guid SubscriptionId { get; set; }
        public Guid PackageId { get; set; }
        public Guid GradeId { get; set; }
        public Guid LanguageId { get; set; }
        public Guid SyllabusId { get; set; }
        public Guid StreamId { get; set; }
        public Guid SubjectId { get; set; }
        public Guid TopicId { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? dtSubscriptionStartDate { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? dtSubscriptionEndDate { get; set; }
        public bool IsSubscriptionExpired { get; set; }
        public Guid? CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedOn { get; set; }
        public Guid? ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedOn { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }        
    }
}
