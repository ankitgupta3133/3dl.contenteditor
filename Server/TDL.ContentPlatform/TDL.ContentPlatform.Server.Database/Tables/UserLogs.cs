﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    public partial class UserLogs
    {
        [Key]
        [Column("UserLogID")]
        public int UserLogId { get; set; }
        [Column("UserID")]
        public Guid? UserId { get; set; }
        [StringLength(100)]
        public string UserName { get; set; }
        [Column("AssetID")]
        public int? AssetId { get; set; }
        [StringLength(100)]
        public string Action { get; set; }
        public string Description { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? AddedOn { get; set; }
        public bool? IsDeleted { get; set; }

        [ForeignKey("UserId")]
        [InverseProperty("UserLogs")]
        public UserProfile User { get; set; }
    }
}
