﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    public partial class UserProfile
    {
        public UserProfile()
        {
            UserLogs = new HashSet<UserLogs>();
        }

        [Key]
        public Guid UserId { get; set; }
        [Required]
        [StringLength(50)]
        public string UserName { get; set; }
        [Required]
        [StringLength(50)]
        public string Password { get; set; }
        [StringLength(50)]
        public string FirstName { get; set; }
        [StringLength(50)]
        public string LastName { get; set; }
        [StringLength(500)]
        public string Address1 { get; set; }
        [StringLength(500)]
        public string Adrress2 { get; set; }
        [StringLength(100)]
        public string City { get; set; }
        [StringLength(100)]
        public string State { get; set; }
        [StringLength(100)]
        public string Country { get; set; }
        [StringLength(10)]
        public string Zip { get; set; }
        [StringLength(15)]
        public string Phone { get; set; }
        [StringLength(15)]
        public string Fax { get; set; }
        [Column("WebsiteURL")]
        [StringLength(200)]
        public string WebsiteUrl { get; set; }
        public Guid? CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedOn { get; set; }
        public Guid? ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedOn { get; set; }
        public bool? IsDeleted { get; set; }
        public bool? IsActive { get; set; }

        [InverseProperty("User")]
        public ICollection<UserLogs> UserLogs { get; set; }
    }
}
