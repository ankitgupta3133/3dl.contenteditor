﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    public partial class Users
    {
        public Users()
        {
            AssetHistory = new HashSet<AssetHistory>();
            FavouriteAssets = new HashSet<FavouriteAssets>();
            UserAccount = new HashSet<UserAccount>();
            UserAssets = new HashSet<UserAssets>();
            UserDetails = new HashSet<UserDetails>();
        }

        [Column("ID")]
        public Guid Id { get; set; }
        [Column("UserID")]
        public Guid? UserId { get; set; }
        [StringLength(200)]
        public string Name { get; set; }
        [StringLength(100)]
        public string FirstName { get; set; }
        [StringLength(100)]
        public string MiddleName { get; set; }
        [StringLength(100)]
        public string LastName { get; set; }
        [StringLength(20)]
        public string ShortName { get; set; }
        [StringLength(500)]
        public string Address1 { get; set; }
        [StringLength(500)]
        public string Address2 { get; set; }
        [StringLength(100)]
        public string City { get; set; }
        [StringLength(100)]
        public string State { get; set; }
        [StringLength(100)]
        public string Country { get; set; }
        [StringLength(20)]
        public string Zip { get; set; }
        [StringLength(50)]
        public string Fax { get; set; }
        [Column("WebsiteURL")]
        [StringLength(200)]
        public string WebsiteUrl { get; set; }
        [StringLength(200)]
        public string ProfilePic { get; set; }
        [StringLength(100)]
        public string PrimaryLanguage { get; set; }
        public Guid? CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedOn { get; set; }
        public Guid? ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedOn { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDeleted { get; set; }

        //[ForeignKey("UserId")]
        //[InverseProperty("Users")]
        //public AspNetUsers User { get; set; }
        [InverseProperty("User")]
        public ICollection<AssetHistory> AssetHistory { get; set; }
        [InverseProperty("User")]
        public ICollection<FavouriteAssets> FavouriteAssets { get; set; }
        [InverseProperty("User")]
        public ICollection<UserAccount> UserAccount { get; set; }
        [InverseProperty("User")]
        public ICollection<UserAssets> UserAssets { get; set; }
        [InverseProperty("User")]
        public ICollection<UserDetails> UserDetails { get; set; }
    }
}
