﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    [Table("XMLContentLabelNames")]
    public partial class XmlContentLabelNames
    {
        [Key]
        [Column("LabelNameID")]
        public int LabelNameId { get; set; }

        [Required]
        public int LabelId { get; set; }

        [Required]
        public Guid LanguageId { get; set; }
        public bool IsDeleted { get; set; }

        [Required]
        [StringLength(200)]
        public string LabelName { get; set; }

        public string Description { get; set; }

        [ForeignKey("LabelId")]
        public XmlcontentLabel XmlContentLabel { get; set; }
    }
}
