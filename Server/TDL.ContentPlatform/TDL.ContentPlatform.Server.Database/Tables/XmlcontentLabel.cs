﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    [Table("XMLContentLabel")]
    public partial class XmlcontentLabel
    {
        [Key]
        [Column("LabelID")]
        public int LabelId { get; set; }
        [Column("ModelID")]
        public int? ModelId { get; set; }
        [StringLength(200)]
        public string LabelName { get; set; }
        public int? Face { get; set; }
        public int? Number { get; set; }
        [Column("EN")]
        [StringLength(100)]
        public string En { get; set; }
        [Column("NO")]
        [StringLength(100)]
        public string No { get; set; }
        [Column("AR")]
        [StringLength(100)]
        public string Ar { get; set; }
        [Column("NN")]
        [StringLength(100)]
        public string Nn { get; set; }
        [Column("DE")]
        [StringLength(100)]
        public string De { get; set; }
        [Column("DK")]
        [StringLength(100)]
        public string Dk { get; set; }
        [Column("ES")]
        [StringLength(100)]
        public string Es { get; set; }
        [Column("FR")]
        [StringLength(100)]
        public string Fr { get; set; }
        [Column("SE")]
        [StringLength(100)]
        public string Se { get; set; }
        [Column("highlight")]
        [StringLength(100)]
        public string Highlight { get; set; }
        [Column("original")]
        [StringLength(100)]
        public string Original { get; set; }
        [Column("xPosition")]
        [StringLength(100)]
        public string XPosition { get; set; }
        [Column("yPosition")]
        [StringLength(100)]
        public string YPosition { get; set; }
        [Column("zPosition")]
        [StringLength(100)]
        public string ZPosition { get; set; }
        [Column("xRotation")]
        [StringLength(100)]
        public string XRotation { get; set; }
        [Column("yRotation")]
        [StringLength(100)]
        public string YRotation { get; set; }
        [Column("zRotation")]
        [StringLength(100)]
        public string ZRotation { get; set; }
        [Column("xScale")]
        [StringLength(100)]
        public string XScale { get; set; }
        [Column("yScale")]
        [StringLength(100)]
        public string YScale { get; set; }
        [Column("zScale")]
        [StringLength(100)]
        public string ZScale { get; set; }
        public Guid? CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedOn { get; set; }
        public Guid? ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedOn { get; set; }
        public bool? IsEnabled { get; set; }
        public bool? IsDeleted { get; set; }

        [ForeignKey("ModelId")]
        [InverseProperty("XmlcontentLabel")]
        public XmlcontentModel Model { get; set; }
        [InverseProperty("XmlcontentLabel")]
        public ICollection<QuizLabelDetails> QuizLabelDetails { get; set; }

        public ICollection<XmlContentLabelNames> XmlContentLabelNames { get; set; }

        public XmlcontentLabel()
        {
            XmlContentLabelNames = new HashSet<XmlContentLabelNames>();            
        }
    }
}
