﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    [Table("XMLContentModel")]
    public partial class XmlcontentModel
    {
        public XmlcontentModel()
        {
            XmlcontentLabel = new HashSet<XmlcontentLabel>();
            QuizDetails = new HashSet<QuizDetails>();
            PuzzleDetails = new HashSet<PuzzleDetails>();
            XmlcontentModelNames = new HashSet<XmlcontentModelNames>();
        }

        [Key]
        [Column("ModelID")]
        public int ModelId { get; set; }
        [Column("AssetContentID")]
        public int? AssetContentId { get; set; }
        [StringLength(200)]
        public string ModelName { get; set; }
        [Column("EN")]
        [StringLength(100)]
        public string En { get; set; }
        [Column("NO")]
        [StringLength(100)]
        public string No { get; set; }
        [Column("AR")]
        [StringLength(100)]
        public string Ar { get; set; }
        [Column("NN")]
        [StringLength(100)]
        public string Nn { get; set; }
        [Column("hidingposX", TypeName = "decimal(18, 10)")]
        public decimal? HidingposX { get; set; }
        [Column("hidingposY", TypeName = "decimal(18, 10)")]
        public decimal? HidingposY { get; set; }
        [Column("hidingposZ", TypeName = "decimal(18, 10)")]
        public decimal? HidingposZ { get; set; }
        public Guid? CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedOn { get; set; }
        public Guid? ModifiedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedOn { get; set; }
        public bool? IsEnabled { get; set; }
        public bool? IsDeleted { get; set; }

        [ForeignKey("AssetContentId")]
        [InverseProperty("XmlcontentModel")]
        public AssetContent AssetContent { get; set; }
        [InverseProperty("Model")]
        public ICollection<XmlcontentLabel> XmlcontentLabel { get; set; }
        [InverseProperty("XmlcontentModel")]
        public ICollection<QuizDetails> QuizDetails { get; set; }
        [InverseProperty("XmlcontentModel")]
        public ICollection<PuzzleDetails> PuzzleDetails { get; set; }
        public ICollection<XmlcontentModelNames> XmlcontentModelNames { get; set; }
    }
}
