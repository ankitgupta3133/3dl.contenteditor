﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TDL.ContentPlatform.Server.Database.Tables
{
    [Table("XMLContentModelNames")]
    public partial class XmlcontentModelNames
    {
        public XmlcontentModelNames()
        {

        }

        [Key]
        [Column("Id")]
        public int Id { get; set; }

        [Required]
        public int ModelId { get; set; }

        [Required]
        public Guid LanguageId { get; set; }
        public bool IsDeleted { get; set; }

        [Required]
        [StringLength(200)]
        public string ModelName { get; set; }

        [ForeignKey("ModelId")]
        public XmlcontentModel XmlContentModel { get; set; }
    }
}
