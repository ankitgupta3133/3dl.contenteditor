﻿using System;

namespace TDL.ContentPlatform.Server.WebApi.ConfigModels
{
    public class AccountConfig
    {
        public Guid AccountId { get; set; }
    }
}
