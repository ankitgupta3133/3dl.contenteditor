﻿using System.Collections.Generic;

namespace TDL.ContentPlatform.Server.WebApi.ConfigModels
{
    public class AdminConfig
    {
        public List<TdlUserConfig> DefaultSystemUsers { get; set; }
    }
}