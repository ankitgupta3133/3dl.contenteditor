﻿namespace TDL.ContentPlatform.Server.WebApi.ConfigModels
{
    public class AssetsConfig
    {
        /// <summary>
        /// Deprecated
        /// </summary>
        /// <deprecated></deprecated>
        public string BaseUrl { get;set; }
        public string ApkBaseUrl { get; set; }
        public string FileBaseUrl { get;set; }
        public string ThumbnailBaseUrl { get;set; }
        public string BackgroundBaseUrl { get; set; }
    }
}