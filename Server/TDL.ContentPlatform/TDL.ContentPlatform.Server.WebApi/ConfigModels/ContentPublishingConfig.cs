﻿namespace TDL.ContentPlatform.Server.WebApi.ConfigModels
{
    public class ContentPublishingConfig
    {
        public string Password { get; set; }
        public string SourceDir { get; set; }
        public string DestinationDir { get; set; }
        public string DestinationThumbnailDir { get; set; }
    }
}