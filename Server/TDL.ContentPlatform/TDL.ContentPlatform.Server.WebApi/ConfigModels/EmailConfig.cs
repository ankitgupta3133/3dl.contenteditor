﻿namespace TDL.ContentPlatform.Server.WebApi.ConfigModels
{
    public class EmailConfig
    {
        public string ServerName { get; set; }
        public string Address { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public int PortNumber { get; set; }
        public string EnableSsl { get; set; }
        public string Url { get; set; }
        public string BaseUrl { get; set; }
    }
}