﻿namespace TDL.ContentPlatform.Server.WebApi.ConfigModels
{
    public class JwtConfiguration
    {
        public string Issuer { get; set; }
        public string Secret { get; set; }
        public double ExpireTimeMinutes { get; set; } = 8 * 60;
        public string DefaultAudience { get; set; }
    }
}