﻿using System;
using System.Linq;
using System.Threading.Tasks;

namespace TDL.ContentPlatform.Server.WebApi.ConfigModels
{
    public class ManagedConfig
    {
        public JwtConfiguration JwtConfiguration { get; set; }
        public AdminConfig Admin { get; set; } = new AdminConfig();
        public EmailConfig Email { get; set; } = new EmailConfig();
        public AccountConfig Account { get; set; } = new AccountConfig();
        public AssetsConfig Assets { get; set; } = new AssetsConfig();
        public ContentPublishingConfig ContentPublishing { get; set; } = new ContentPublishingConfig();
        public SecurityConfig Security { get; set; }
    }
}
