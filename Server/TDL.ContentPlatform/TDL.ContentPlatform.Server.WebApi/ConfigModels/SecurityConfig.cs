﻿using Microsoft.AspNetCore.Identity;

namespace TDL.ContentPlatform.Server.WebApi.ConfigModels
{
    public class SecurityConfig
    {
        public PasswordOptions PasswordPolicy { get; set; }
    }
}