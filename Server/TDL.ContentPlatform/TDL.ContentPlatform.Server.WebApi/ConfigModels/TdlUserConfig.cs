﻿using System.Collections.Generic;
using TDL.ContentPlatform.Server.Database.Tables;

namespace TDL.ContentPlatform.Server.WebApi.ConfigModels
{
    public class TdlUserConfig : TdlUser
    {
        public string Password { get;set; }
        public List<string> Roles { get;set; }=new List<string>();
    }
}