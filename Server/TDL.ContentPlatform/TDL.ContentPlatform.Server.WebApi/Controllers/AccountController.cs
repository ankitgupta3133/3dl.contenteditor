﻿using System;
using System.Web;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using RT.Comb;
using TDL.ContentPlatform.Server.Common;
using TDL.ContentPlatform.Server.Common.Enums;
using TDL.ContentPlatform.Server.Common.Models;
using TDL.ContentPlatform.Server.Common.Models.Communication;
using TDL.ContentPlatform.Server.Common.Models.Content;
using TDL.ContentPlatform.Server.Database;
using TDL.ContentPlatform.Server.Database.Tables;
using TDL.ContentPlatform.Server.WebApi.ConfigModels;
using TDL.ContentPlatform.Server.WebApi.Models;
using TDL.ContentPlatform.Server.WebApi.Serviceproviders;
using Microsoft.AspNetCore.Hosting;
using TDL.ContentPlatform.Server.Common.Models.v2;
using System.Text.RegularExpressions;
using TDL.ContentPlatform.Server.Common.Utils;
using TDL.ContentPlatform.Server.WebApi.Utils;

namespace TDL.ContentPlatform.Server.WebApi.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]/[action]")]
    public class AccountController : Controller
    {
        private readonly ILogger<ConfigController> _logger;
        private readonly ContentDbContext _contentDbContext;
        private readonly IMapper _mapper;
        private readonly ManagedConfig _managedConfig;
        private readonly UserManager<TdlUser> _userManager;
        private readonly RoleManager<TdlRole> _roleManager;
        private readonly SignInManager<TdlUser> _signInManager;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly EmailServiceprovider _emailServiceprovider;
        private IHostingEnvironment _env;
        private static Regex _platformArchitectureRewriteRegex = new Regex("(X(86|64)|ARM)$", RegexOptions.Compiled | RegexOptions.IgnoreCase);

        public AccountController(ILogger<ConfigController> logger, ContentDbContext contentDbContext, IMapper mapper, IOptions<ManagedConfig> managedConfig, UserManager<TdlUser> userManager, RoleManager<TdlRole> roleManager, SignInManager<TdlUser> signInManager, IHttpContextAccessor httpContextAccessor,
            EmailServiceprovider emailServiceprovider, IHostingEnvironment env)
        {
            _logger = logger;
            _contentDbContext = contentDbContext;
            _mapper = mapper;
            _managedConfig = managedConfig.Value;
            _userManager = userManager;
            _roleManager = roleManager;
            _signInManager = signInManager;
            _httpContextAccessor = httpContextAccessor;
            _emailServiceprovider = emailServiceprovider;
            _env = env;
        }


        /// <summary>
        /// Authenticate user.
        /// </summary>
        /// <param name="request">Username and password</param>
        /// <returns>Jwt Bearer token that has to be returned as Authorize header in every request to protected resources.</returns>
        /// <remarks>One token is valid for up to 10 hours. Failing to pass token or using unvalid/expired token will lead to "404" error from API.</remarks>
        [HttpGet]
        public async Task<AccountLoginResponse> Login(AccountLoginRequest request)
        {
            //
            // We do a tiny bit extra verbose logging in this function just to be able to debug users from logs, and to easily filter out attacks from log-files.
            //

            // Trim username/password in case we are getting space before/after. These often come from users who make these mistakes.
            request.Username = request.Username?.Trim();
            request.Password = request.Password?.Trim();

            var ret = new AccountLoginResponse();
            var ip = _httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();

            var username = request.Username ?? "<null>";

            try
            {
                _logger.LogInformation(LogMessageType.Login, $"Login\t{ip}\t{username}");

                // If username contains @ we assume its an e-mail and resolve it to username.
                TdlUser user = null;
                if (username.Contains("@"))
                {
                    user = await _userManager.FindByEmailAsync(username);
                    _logger.LogInformation(LogMessageType.Login, $"Login\t{ip}\t{username}\tresolved to\tId: {user?.Id.ToString() ?? "<null>"}, UserName: {user?.UserName ?? "<null>"}");
                    if (user != null)
                        username = user.UserName;
                }
                else
                {
                    user = await _userManager.FindByNameAsync(username);
                }


                var result = await _signInManager.CheckPasswordSignInAsync(user, request.Password, false);

                // Failure
                if (!result.Succeeded)
                {
                    _logger.LogInformation(LogMessageType.LoginDenied, $"Login denied\t{ip}\t{username}\tSucess: {result.Succeeded}, IsLockedOut: {result.IsLockedOut}, IsNotAllowed: {result.IsNotAllowed}, RequiresToFactor: {result.RequiresTwoFactor}");
                    ret.Success = false;
                    if (result.IsLockedOut)
                    {
                        ret.ErrorMessage = "User locked out.";
                        ret.LocalizedError = CommonMethods.GetErrorLocal(ErrorCode.UserLocked, _contentDbContext);
                    }
                    else
                    {
                        ret.ErrorMessage = "Login denied.";
                        ret.LocalizedError = CommonMethods.GetErrorLocal(ErrorCode.LoginDenied, _contentDbContext);
                    }
                    return ret;
                }

                if (!user.IsActive)
                {
                    ret.Success = false;
                    ret.ErrorMessage = "Account deactivated.";
                    ret.LocalizedError = CommonMethods.GetErrorLocal(ErrorCode.AccountDeactivated, _contentDbContext);
                    return ret;
                }
                user.SessionKey = Provider.Sql.Create();
                await _userManager.UpdateAsync(user);

                //Validate user platform is supplied
                if (!string.IsNullOrWhiteSpace(request.Platform))
                {
                    request.Platform = request.Platform = RewriteRequestPlatform(request.Platform);
                    List<CsNewPlatform> lstPlatforms = _contentDbContext.CsNewPlatform.ToList();
                    Dictionary<string, CsNewPlatform> lstAllPlatforms = new Dictionary<string, CsNewPlatform>(StringComparer.OrdinalIgnoreCase);
                    foreach (CsNewPlatform objPlatform in lstPlatforms)
                    {

                        if (!lstAllPlatforms.ContainsKey(objPlatform.NewName))
                            lstAllPlatforms.Add(objPlatform.NewName, objPlatform);
                    }

                    lstAllPlatforms.TryGetValue(request.Platform, out var objOutputPlatform);

                    if (objOutputPlatform == null)
                    {
                        ret.Success = false;
                        ret.ErrorMessage = "Invalid platform";
                        ret.LocalizedError = CommonMethods.GetErrorLocal(ErrorCode.InvalidPlatform, _contentDbContext);
                        return ret;
                    }

                    var userDetails = _contentDbContext.UserDetails.Where(X => X.UserId == user.Id);
                    if (userDetails != null)
                    {
                        var userPlatform = _contentDbContext.UserDetailPlatform.Where(X => userDetails.Select(Y => Y.Id).Contains(X.UserDetailId) && X.PlatformId == objOutputPlatform.Id);
                        if (userPlatform.Count() == 0)
                        {
                            ret.Success = false;
                            ret.ErrorMessage = "Invalid user platform";
                            ret.LocalizedError = CommonMethods.GetErrorLocal(ErrorCode.InvalidUserPlatform, _contentDbContext);
                            return ret;
                        }
                    }
                    else
                    {
                        ret.Success = false;
                        ret.ErrorMessage = "Invalid user platform";
                        ret.LocalizedError = CommonMethods.GetErrorLocal(ErrorCode.InvalidUserPlatform, _contentDbContext);
                        return ret;
                    }
                }

                // Success
                _logger.LogInformation(LogMessageType.LoginSuccess, $"Login success\t{ip}\t{username}");
                ret.User = _mapper.Map<ClientUser>(user);
                ret.Success = true;

                // Generate claims and return JwtBearer token
                var claims = new List<Claim>()
                {
                    new Claim(ClaimTypes.Name, request.Username),
                    new Claim(JwtClaimsEnum.UserSessionKey.ToString(), user.SessionKey.ToString())
                };

                var roles = await _userManager.GetRolesAsync(user);
                foreach (var role in roles)
                    claims.Add(new Claim(ClaimTypes.Role, role));

                ret.User.Role = roles.FirstOrDefault();

                var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_managedConfig.JwtConfiguration.Secret));
                var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

                var token = new JwtSecurityToken(
                    issuer: _managedConfig.JwtConfiguration.Issuer,
                    audience: _managedConfig.JwtConfiguration.DefaultAudience,
                    claims: claims,
                    expires: DateTime.Now.AddMinutes(_managedConfig.JwtConfiguration.ExpireTimeMinutes),
                    signingCredentials: creds);

                ret.AuthorizationToken = "Bearer " + (new JwtSecurityTokenHandler().WriteToken(token));
            }
            catch (Exception exception)
            {
                _logger.LogError(LogMessageType.LoginError, exception, $"Login error\t{ip}\t{username}");
                ret.Success = false;
                ret.ErrorMessage = "Internal error during login procedure.";
                ret.LocalizedError = CommonMethods.GetErrorLocal(ErrorCode.InternalErrorLogin, _contentDbContext);
            }

            return ret;
        }

        private string RewriteRequestPlatform(string requestPlatform)
        {
            // Remove "Player" and "Editor" from string. Ref https://docs.unity3d.com/ScriptReference/RuntimePlatform.html
            //requestPlatform = _platformRewriteRegex.Replace(requestPlatform, "");
            requestPlatform = _platformArchitectureRewriteRegex.Replace(requestPlatform, "");
            requestPlatform = requestPlatform.Replace("Player", "");
            requestPlatform = requestPlatform.Replace("Editor", "");
            requestPlatform = requestPlatform.Replace("Metro", "WSA");
            return requestPlatform;
        }

        [HttpPost]
        [Authorize(Policy = "Administrator")]
        public async Task<AccountRegisterResponse> Register([FromBody] AccountRegisterRequest request)
        {
            //
            // We do a tiny bit extra verbose logging in this function just to be able to debug users from logs, and to easily filter out attacks from log-files.
            //

            // Trim username/password in case we are getting space beopre/after. These often come from users who make these mistakes.
            request.Username = request.Username?.Trim();
            request.Password = request.Password?.Trim();

            var ret = new AccountRegisterResponse();
            var ip = _httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();

            var username = request.Username ?? "<null>";
            try
            {
                _logger.LogInformation(LogMessageType.Register, $"Register\t{ip}\t{username}");

                var user = new TdlUser() { UserName = request.Username, Email = request.Email, Firstname = request.Firstname, Lastname = request.Lastname, Account = request.Account };

                var createdResult = await _userManager.CreateAsync(user, request.Password);

                // Failure to add user
                if (!createdResult.Succeeded)
                {
                    var ec = 0;
                    _logger.LogInformation(LogMessageType.Register, $"Register denied\t{ip}\t{user}\tAdding user\tErrors follow.");
                    ret.Errors = createdResult.Errors.Select(e => new ErrorCodeDescription(e.Code, e.Description)).ToList();
                    foreach (var error in createdResult.Errors)
                    {
                        ec++;
                        _logger.LogInformation(LogMessageType.Register, $"Register denied\t{ip}\t{user}\tAdding user\tError {ec}\t{error.Code ?? "<null>"}\t{error.Description ?? "<null>"}");
                    }

                    ret.Success = false;
                    ret.ErrorMessage = "Register denied.";
                    ret.LocalizedError = CommonMethods.GetErrorLocal(ErrorCode.RegisterDenied, _contentDbContext);
                    return ret;
                }

                // Success, we need to set roles
                var roleResult = await _userManager.AddToRolesAsync(user, request.Roles);

                // Failure to set roles
                if (!roleResult.Succeeded)
                {
                    var ec = 0;
                    _logger.LogInformation(LogMessageType.Register, $"Register denied\t{ip}\t{user}\tAdding user to roles\tErrors follow.");
                    ret.Errors = roleResult.Errors.Select(e => new ErrorCodeDescription(e.Code, e.Description)).ToList();
                    foreach (var error in roleResult.Errors)
                    {
                        ec++;
                        _logger.LogInformation(LogMessageType.Register, $"Register denied\t{ip}\t{user}\ttAdding user to roles\tError {ec}\t{error.Code ?? "<null>"}\t{error.Description ?? "<null>"}");
                    }

                    ret.Success = false;
                    ret.ErrorMessage = "Register denied.";
                    ret.LocalizedError = CommonMethods.GetErrorLocal(ErrorCode.RegisterDenied, _contentDbContext);
                    return ret;
                }


                // Success creating user
                _logger.LogInformation(LogMessageType.Register, $"Register success\t{ip}\t{user}");
                ret.UserId = user.Id;
                ret.Success = true;

                ////string filepath = new Microsoft.AspNetCore.Http.PathString("/EmailTemplates/NewUserCreation.html");
                //var webRoot = _env.WebRootPath;

                //if (request.IsDemoAccount)
                //{
                //    var demofilepath = System.IO.Path.Combine(webRoot, "EmailTemplates/NewDemoUserRegistration.html");
                //    var expiryDate = DateTime.Today.AddDays(request.Duration).ToShortDateString();
                //    var demomailcontent = System.IO.File.ReadAllText(demofilepath);
                //    demomailcontent = demomailcontent.Replace("{Name}", user.Firstname.Trim()).Replace("{URL}", _managedConfig.Email.Url.ToString()).Replace("{Email}", user.Email).Replace("{Password}", request.Password).Replace("{EndDate}", expiryDate);

                //    //Sharing account details with the user
                //    _emailServiceprovider.SendMail(request.Email, "Demo account creation", demomailcontent);
                //}
                //else
                //{
                //    var filepath = System.IO.Path.Combine(webRoot, "EmailTemplates/NewUserRegistration.html");

                //    var mailcontent = System.IO.File.ReadAllText(filepath);
                //    mailcontent = mailcontent.Replace("{Name}", user.Firstname.Trim()).Replace("{URL}", _managedConfig.Email.Url.ToString()).Replace("{Email}", user.Email).Replace("{Password}", request.Password);

                //    //Sharing account details with the user
                //    _emailServiceprovider.SendMail(request.Email, "New account creation", mailcontent);
                //}         
            }
            catch (Exception exception)
            {
                _logger.LogError(LogMessageType.Register, exception, $"Register error\t{ip}\t{username}");
                ret.Success = false;
                ret.ErrorMessage = "Internal error during register procedure.";
                ret.LocalizedError = CommonMethods.GetErrorLocal(ErrorCode.InternalErrorRegister, _contentDbContext);
            }

            return ret;
        }

        [HttpPost]
        public async Task<AccountResetPasswordLinkResponse> ResetPasswordLink([FromBody] AccountResetPasswordLinkRequest request)
        {
            request.Username = request.Username?.Trim();
            request.NewPassword = request.NewPassword?.Trim();
            var username = request.Username ?? "<null>";
            var ip = _httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();

            var ret = new AccountResetPasswordLinkResponse();
            try
            {
                TdlUser user = null;
                // If username contains @ we assume its an e-mail and resolve it to username.
                if (username.Contains("@"))
                {
                    user = await _userManager.FindByEmailAsync(username);
                    _logger.LogInformation(LogMessageType.ResetPassword, $"ResetPassword\t{ip}\t{username}\tresolved to\tId: {user?.Id.ToString() ?? "<null>"}, UserName: {user?.UserName ?? "<null>"}");
                    if (user != null)
                        username = user.UserName;
                }
                else
                {
                    user = await _userManager.FindByNameAsync(username);
                }

                if (user == null)
                {
                    ret.Success = false;
                    ret.ErrorMessage = "User not found";
                    ret.LocalizedError = CommonMethods.GetErrorLocal(ErrorCode.UserNotFound, _contentDbContext);
                    return ret;
                }

                var result = await _userManager.ResetPasswordAsync(user, request.ResetPasswordToken, request.NewPassword);

                if (!result.Succeeded)
                {
                    var ec = 0;
                    _logger.LogInformation(LogMessageType.ResetPassword, $"Reset Password denied\t{ip}\t{user}\tResetting Password\tErrors follow.");
                    ret.Errors = result.Errors.Select(e => new ErrorCodeDescription(e.Code, e.Description)).ToList();
                    foreach (var error in result.Errors)
                    {
                        ec++;
                        _logger.LogInformation(LogMessageType.ResetPassword, $"Reset Password denied\t{ip}\t{user}\tResetting Password\tError {ec}\t{error.Code ?? "<null>"}\t{error.Description ?? "<null>"}");
                    }

                    ret.Success = false;
                    ret.ErrorMessage = "Reset Password denied.";
                    ret.LocalizedError = CommonMethods.GetErrorLocal(ErrorCode.ResetPasswordDenied, _contentDbContext);
                    return ret;
                }

                //Getting language
                string language = string.Empty;
                var detail = _contentDbContext.UserDetails.Where(x => x.UserId == user.Id && x.IsDeleted == false).FirstOrDefault();
                if (detail != null)
                {
                    if (detail.LanguageId != Guid.Empty)
                    {
                        var la = _contentDbContext.CsNewLanguage.Where(x => x.Id == detail.LanguageId).FirstOrDefault();
                        language = la != null ? la.NewName : string.Empty;
                    }
                    else if (detail.PackageId != Guid.Empty)
                    {
                        language = GetLanguageFromPackage(detail.PackageId);
                    }
                    else
                    {
                        language = GetLanguageFromSubscription(detail.SubscriptionId);
                    }
                }

                if (string.IsNullOrEmpty(language))
                {
                    language = GetLanguageFromAccount(user.Account);
                }

                //Sending mail to enduser
                var webRoot = _env.WebRootPath;
                string filename = string.Concat("ResetPasswordConfirmation_", language, ".html");
                var filepath = System.IO.Path.Combine(webRoot, "EmailTemplates", filename);
                var mailcontent = System.IO.File.ReadAllText(filepath);

                //Sharing account details with the user
                await Task.Run(() => _emailServiceprovider.SendMail(request.Username, "Reset Password Confirmation", mailcontent));

                ret.Success = true;
            }
            catch (Exception exception)
            {
                _logger.LogError(LogMessageType.ResetPassword, exception, $"Reset Password error\t{ip}\t{username}");
                ret.Success = false;
                ret.ErrorMessage = "Internal error during reset password procedure.";
                ret.LocalizedError = CommonMethods.GetErrorLocal(ErrorCode.InternalErrorPassword, _contentDbContext);
            }
            return ret;
        }

        [HttpPost]
        public async Task<AccountResetPasswordResponse> ResetPassword([FromBody] AccountResetPasswordRequest request)
        {
            // Trim username/password in case we are getting space before/after. These often come from users who make these mistakes.
            request.Username = request.Username?.Trim();
            var username = request.Username ?? "<null>";
            var ip = _httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();

            var ret = new AccountResetPasswordResponse();

            try
            {
                TdlUser user = null;
                // If username contains @ we assume its an e-mail and resolve it to username.
                if (username.Contains("@"))
                {
                    user = await _userManager.FindByEmailAsync(username);
                    _logger.LogInformation(LogMessageType.ResetPassword, $"ResetPassword\t{ip}\t{username}\tresolved to\tId: {user?.Id.ToString() ?? "<null>"}, UserName: {user?.UserName ?? "<null>"}");
                    if (user != null)
                        username = user.UserName;
                }
                else
                {
                    user = await _userManager.FindByNameAsync(username);
                }

                if (user == null)
                {
                    ret.Success = false;
                    ret.ErrorMessage = "User not found";
                    ret.LocalizedError = CommonMethods.GetErrorLocal(ErrorCode.UserNotFound, _contentDbContext);
                    return ret;
                }

                var token = await _userManager.GeneratePasswordResetTokenAsync(user);

                //Getting language
                string language = string.Empty;
                var detail = _contentDbContext.UserDetails.Where(x => x.UserId == user.Id && x.IsDeleted == false).FirstOrDefault();
                if (detail != null)
                {
                    if (detail.LanguageId != Guid.Empty)
                    {
                        var la = _contentDbContext.CsNewLanguage.Where(x => x.Id == detail.LanguageId).FirstOrDefault();
                        language = la != null ? la.NewName : string.Empty;
                    }
                    else if (detail.PackageId != Guid.Empty)
                    {
                        language = GetLanguageFromPackage(detail.PackageId);
                    }
                    else
                    {
                        language = GetLanguageFromSubscription(detail.SubscriptionId);
                    }
                }

                if (string.IsNullOrEmpty(language))
                {
                    language = GetLanguageFromAccount(user.Account);
                }

                // TODO: Send e-mail
                var webRoot = _env.WebRootPath;
                string filename = string.Concat("ForgotPasswordTemplate_", language, ".html");
                var filepath = System.IO.Path.Combine(webRoot, "EmailTemplates", filename);
                string ResetURL = _managedConfig.Email.BaseUrl.ToString() + "/UserManagement/ResetPassword?code=" + HttpUtility.UrlEncode(token);
                var mailcontent = System.IO.File.ReadAllText(filepath);
                mailcontent = mailcontent.Replace("{ResetURL}", ResetURL);

                //Sharing account details with the user
                await Task.Run(() => _emailServiceprovider.SendMail(request.Username, "Reset Password Assistance", mailcontent));

                ret.Success = true;
            }
            catch (Exception exception)
            {
                _logger.LogError(LogMessageType.ResetPassword, exception, $"Reset Password Link error\t{ip}\t{username}");
                ret.Success = false;
                ret.ErrorMessage = "Internal error during reset password link procedure.";
                ret.LocalizedError = CommonMethods.GetErrorLocal(ErrorCode.InternalErrorResetPasswordLink, _contentDbContext);
            }
            return ret;
        }

        [HttpPost]
        public async Task<AccountChangePasswordResponse> ChangePassword([FromBody] AccountChangePasswordRequest request)
        {
            //
            // We do a tiny bit extra verbose logging in this function just to be able to debug users from logs, and to easily filter out attacks from log-files.
            //

            // Trim username/password in case we are getting space before/after. These often come from users who make these mistakes.
            request.Username = request.Username?.Trim();
            request.OldPassword = request.OldPassword?.Trim();
            request.NewPassword = request.NewPassword?.Trim();

            var ret = new AccountChangePasswordResponse();
            var ip = _httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();

            var username = request.Username ?? "<null>";

            try
            {
                _logger.LogInformation(LogMessageType.ChangePassword, $"Change Password\t{ip}\t{username}");

                // If username contains @ we assume its an e-mail and resolve it to username.
                TdlUser user = null;
                if (username.Contains("@"))
                {
                    user = await _userManager.FindByEmailAsync(username);
                    _logger.LogInformation(LogMessageType.ChangePassword, $"Change Password\t{ip}\t{username}\tresolved to\tId: {user?.Id.ToString() ?? "<null>"}, UserName: {user?.UserName ?? "<null>"}");
                    if (user != null)
                        username = user.UserName;
                }
                else
                {
                    user = await _userManager.FindByNameAsync(username);
                }


                var result = await _userManager.ChangePasswordAsync(user, request.OldPassword, request.NewPassword);

                // Failure to add user
                if (!result.Succeeded)
                {
                    var ec = 0;
                    _logger.LogInformation(LogMessageType.ChangePassword, $"Change Password denied\t{ip}\t{user}\tChanging Password\tErrors follow.");
                    ret.Errors = result.Errors.Select(e => new ErrorCodeDescription(e.Code, e.Description)).ToList();
                    foreach (var error in result.Errors)
                    {
                        ec++;
                        _logger.LogInformation(LogMessageType.ChangePassword, $"Change Password denied\t{ip}\t{user}\tChanging Password\tError {ec}\t{error.Code ?? "<null>"}\t{error.Description ?? "<null>"}");
                    }

                    ret.Success = false;
                    ret.ErrorMessage = "Change Password denied.";
                    ret.LocalizedError = CommonMethods.GetErrorLocal(ErrorCode.ChangePasswordDenied, _contentDbContext);
                    return ret;
                }

                //Getting language
                string language = string.Empty;
                var detail = _contentDbContext.UserDetails.Where(x => x.UserId == user.Id && x.IsDeleted == false).FirstOrDefault();
                if (detail != null)
                {
                    if (detail.LanguageId != Guid.Empty)
                    {
                        var la = _contentDbContext.CsNewLanguage.Where(x => x.Id == detail.LanguageId).FirstOrDefault();
                        language = la != null ? la.NewName : string.Empty;
                    }
                    else if (detail.PackageId != Guid.Empty)
                    {
                        language = GetLanguageFromPackage(detail.PackageId);
                    }
                    else
                    {
                        language = GetLanguageFromSubscription(detail.SubscriptionId);
                    }
                }

                if (string.IsNullOrEmpty(language))
                {
                    language = GetLanguageFromAccount(user.Account);
                }

                //Sending mail to enduser
                var webRoot = _env.WebRootPath;
                string filename = string.Concat("ChangePassword_", language, ".html");
                var filepath = System.IO.Path.Combine(webRoot, "EmailTemplates", filename);
                var mailcontent = System.IO.File.ReadAllText(filepath);

                //Sharing account details with the user
                await Task.Run(() => _emailServiceprovider.SendMail(request.Username, "Change Password Confirmation", mailcontent));

                ret.Success = true;
            }
            catch (Exception exception)
            {
                _logger.LogError(LogMessageType.ChangePassword, exception, $"Change Password error\t{ip}\t{username}");
                ret.Success = false;
                ret.ErrorMessage = "Internal error during change password procedure.";
                ret.LocalizedError = CommonMethods.GetErrorLocal(ErrorCode.InternalErrorChangePassword, _contentDbContext);
            }

            return ret;
        }

        [HttpPost]
        [Authorize(Policy = "Administrator")]
        public async Task<AccountDeleteUserResponse> DeleteUser([FromBody] AccountDeleteUserRequest request)
        {
            var ret = new AccountDeleteUserResponse();
            TdlUser user = null;
            user = await _userManager.FindByIdAsync(request.Id);
            var rolesForUser = await _userManager.GetRolesAsync(user);

            if (rolesForUser.Count() > 0)
            {
                foreach (var item in rolesForUser.ToList())
                {
                    // item should be the name of the role
                    var result = await _userManager.RemoveFromRoleAsync(user, item);
                    if (!result.Succeeded)
                    {
                        ret.Success = false;
                        ret.ErrorMessage = "Request denied.";
                        ret.LocalizedError = CommonMethods.GetErrorLocal(ErrorCode.RequestDenied, _contentDbContext);
                        return ret;
                    }
                }
            }

            var status = await _userManager.DeleteAsync(user);

            if (!status.Succeeded)
            {
                ret.Success = false;
                ret.ErrorMessage = "Request denied.";
                ret.LocalizedError = CommonMethods.GetErrorLocal(ErrorCode.RequestDenied, _contentDbContext);
                return ret;
            }

            ret.Success = true;
            ret.ErrorMessage = "User deleted.";
            ret.LocalizedError = CommonMethods.GetErrorLocal(ErrorCode.UserDeleted, _contentDbContext);
            return ret;
        }

        #region AppMaster
        [HttpPost]
        public async Task<AppMasterResponse> AppMaster(AppMasterRequest request)
        {
            var ret = new AppMasterResponse();
            var result = _contentDbContext.AppMaster.Where(x => x.AppId == request.AppId).FirstOrDefault();
            if (result != null)
            {
                var user = await _userManager.FindByIdAsync(result.UserId.ToString());
                if (user != null)
                {
                    ret.Username = user.UserName;
                }
            }
            else
            {
                ret.Success = false;
                ret.ErrorMessage = "User deleted.";
                ret.LocalizedError = CommonMethods.GetErrorLocal(ErrorCode.UserDeleted, _contentDbContext);
            }

            ret.Success = true;
            ret.ErrorMessage = "";
            return ret;
        }
        #endregion

        #region Language List
        [HttpGet]
        public LanguageListResponse LanguageList(LanguageListRequest request)
        {
            var ret = new LanguageListResponse();
            ret.Languages = _mapper.Map<List<Common.Models.v2.Language>>(_contentDbContext.CsNewLanguage.Where(l => l.NewLanguageshortname.Equals("en-US") || l.NewLanguageshortname.Equals("nb-NO")).ToList());
            //ret.Languages = _mapper.Map<List<Common.Models.v2.Language>>(_contentDbContext.CsNewLanguage.Where(l => l.NewLanguageshortname.Equals("en-US") || l.NewLanguageshortname.Equals("nb-NO") || l.NewLanguageshortname.Equals("nn-NO")).ToList());
            //ret.Languages = _mapper.Map<List<Common.Models.v2.Language>>(_contentDbContext.CsNewLanguage.ToList());
            ret.Success = true;
            ret.ErrorMessage = "";
            return ret;
        }
        #endregion

        #region Application Message List
        [HttpGet]
        public MessageListResponse MessageList()
        {
            var ret = new MessageListResponse();




            var appMessages = (from E in _contentDbContext.ApplicationMessage
                               join L in _contentDbContext.CsNewLanguage on E.LanguageId equals L.NewLanguageid
                               where E.IsDeleted == false && E.MessageType == (byte)MessageType.AppLabel
                               select new { Code = E.MessageCode, Message = E.Message, LangCode = L.NewLanguageshortname });

            ret.Messages = (from M in appMessages
                            group M by M.Code into G
                            select new AppMessage { messageCode = G.Key, messageLocal = G.Select(X => new Message() { culture = X.LangCode, name = X.Message }).ToList()}).ToList();


            ret.Success = true;
            ret.ErrorMessage = "";
            return ret;
        }
        #endregion

        #region Apk List
        [HttpGet]
        [Authorize(Roles = "Student,Teacher,Administrator")]
        public ApkDetailResponse ApkDetail(ApkDetailRequest request)
        {
            var ret = new ApkDetailResponse();

            var apk = _contentDbContext.ApkDetail.OrderByDescending(x => x.ApkId).FirstOrDefault();
            if (apk != null)
            {
                ret.Apk = _mapper.Map<Common.Models.v2.Apk>(apk);
                ret.Apk.ApkCdnUrls = new List<CdnUrl>() { new CdnUrl() { Url = _managedConfig.Assets.ApkBaseUrl.Replace("{filename}", Uri.EscapeUriString(apk.ApkName)) } };
            }
            else
            {
                ret.Success = false;
                ret.ErrorMessage = "No apk found";
                ret.LocalizedError = CommonMethods.GetErrorLocal(ErrorCode.NoApkFound, _contentDbContext);
                return ret;
            }

            ret.Success = true;
            ret.ErrorMessage = "";
            return ret;
        }
        #endregion

        private string GetLanguageFromPackage(Guid PackageId)
        {
            var pack = (from pl in _contentDbContext.CsNewPackageLanguage
                        join l in _contentDbContext.CsNewLanguage on pl.NewLanguageid equals l.NewLanguageid
                        where pl.NewPackageid == PackageId
                        select l).Distinct().FirstOrDefault();

            if (pack != null)
            {
                return pack.NewName;
            }

            return string.Empty;
        }

        private string GetLanguageFromSubscription(Guid SubscriptionId)
        {
            var sub = (from sp in _contentDbContext.CsNewSubscriptionPackage
                       join pl in _contentDbContext.CsNewPackageLanguage on sp.NewPackageid equals pl.NewPackageid
                       join l in _contentDbContext.CsNewLanguage on pl.NewLanguageid equals l.NewLanguageid
                       where sp.NewSubscriptionid == SubscriptionId
                       select l).Distinct().FirstOrDefault();

            if (sub != null)
            {
                return sub.NewName;
            }

            return string.Empty;
        }

        private string GetLanguageFromAccount(Guid AccountId)
        {
            var newSub = (from o in _contentDbContext.CsOpportunity
                          join os in _contentDbContext.CsNewOpportunitySubscriptionMapping on o.Id equals os.NewOpportunityid
                          join s in _contentDbContext.CsNewSubscription on os.NewSubscriptionid equals s.Id
                          where o.Parentaccountid == AccountId
                          select s).Distinct().FirstOrDefault();

            if (newSub != null)
            {
                var newLang = (from sp in _contentDbContext.CsNewSubscriptionPackage
                               join pl in _contentDbContext.CsNewPackageLanguage on sp.NewPackageid equals pl.NewPackageid
                               join l in _contentDbContext.CsNewLanguage on pl.NewLanguageid equals l.NewLanguageid
                               where sp.NewSubscriptionid == newSub.Id
                               select l).Distinct().FirstOrDefault();

                if (newLang != null)
                {
                    return newLang.NewName;
                }
            }

            return "English";
        }

        [HttpPost]
        public async Task<AccountResetPasswordLinkResponse> ResendPassword([FromBody] AccountResetPasswordLinkRequest request)
        {
            request.Username = request.Username?.Trim();
            request.NewPassword = request.NewPassword?.Trim();
            var username = request.Username ?? "<null>";
            var ip = _httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();

            var ret = new AccountResetPasswordLinkResponse();
            try
            {
                TdlUser user = null;

                // If username contains @ we assume its an e-mail and resolve it to username.
                if (username.Contains("@"))
                {
                    user = await _userManager.FindByEmailAsync(username);
                    _logger.LogInformation(LogMessageType.ResetPassword, $"ResendPassword\t{ip}\t{username}\tresolved to\tId: {user?.Id.ToString() ?? "<null>"}, UserName: {user?.UserName ?? "<null>"}");
                    if (user != null)
                        username = user.UserName;
                }
                else
                {
                    user = await _userManager.FindByNameAsync(username);
                }

                if (user == null)
                {
                    ret.Success = false;
                    ret.ErrorMessage = "User not found";
                    ret.LocalizedError = CommonMethods.GetErrorLocal(ErrorCode.UserNotFound, _contentDbContext);
                    return ret;
                }

                var token = await _userManager.GeneratePasswordResetTokenAsync(user);
                var result = await _userManager.ResetPasswordAsync(user, token, request.NewPassword);

                if (!result.Succeeded)
                {
                    var ec = 0;
                    _logger.LogInformation(LogMessageType.ResetPassword, $"Resend Password denied\t{ip}\t{user}\tResetting Password\tErrors follow.");
                    ret.Errors = result.Errors.Select(e => new ErrorCodeDescription(e.Code, e.Description)).ToList();
                    foreach (var error in result.Errors)
                    {
                        ec++;
                        _logger.LogInformation(LogMessageType.ResetPassword, $"Resend Password denied\t{ip}\t{user}\tResetting Password\tError {ec}\t{error.Code ?? "<null>"}\t{error.Description ?? "<null>"}");
                    }

                    ret.Success = false;
                    ret.ErrorMessage = "Resend Password denied.";
                    ret.LocalizedError = CommonMethods.GetErrorLocal(ErrorCode.ResetPasswordDenied, _contentDbContext);
                    return ret;
                }

                //Getting language
                string language = string.Empty;
                var detail = _contentDbContext.UserDetails.Where(x => x.UserId == user.Id && x.IsDeleted == false).FirstOrDefault();
                if (detail != null)
                {
                    if (detail.LanguageId != Guid.Empty)
                    {
                        var la = _contentDbContext.CsNewLanguage.Where(x => x.Id == detail.LanguageId).FirstOrDefault();
                        language = la != null ? la.NewName : string.Empty;
                    }
                    else if (detail.PackageId != Guid.Empty)
                    {
                        language = GetLanguageFromPackage(detail.PackageId);
                    }
                    else
                    {
                        language = GetLanguageFromSubscription(detail.SubscriptionId);
                    }
                }

                if (string.IsNullOrEmpty(language))
                {
                    language = GetLanguageFromAccount(user.Account);
                }

                //Sending mail to enduser
                var webRoot = _env.WebRootPath;
                string filename = string.Concat("ResendPasswordConfirmation_", language, ".html");
                var filepath = System.IO.Path.Combine(webRoot, "EmailTemplates", filename);
                var mailcontent = System.IO.File.ReadAllText(filepath);
                mailcontent = mailcontent.Replace("{Password}", request.NewPassword);

                //Sharing account details with the user
                await Task.Run(() => _emailServiceprovider.SendMail(request.Username, "Resend Password Confirmation", mailcontent));

                ret.Success = true;
            }
            catch (Exception exception)
            {
                _logger.LogError(LogMessageType.ResetPassword, exception, $"Resend Password error\t{ip}\t{username}");
                ret.Success = false;
                ret.ErrorMessage = "Internal error during resend password procedure.";
                ret.LocalizedError = CommonMethods.GetErrorLocal(ErrorCode.InternalErrorResendPassword, _contentDbContext);
            }
            return ret;
        }

        [HttpPost]
        public async Task<AccountResetPasswordLinkResponse> ResendInvitation([FromBody] AccountResetPasswordLinkRequest request)
        {
            request.Username = request.Username?.Trim();
            request.NewPassword = request.NewPassword?.Trim();
            var username = request.Username ?? "<null>";
            var ip = _httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();

            var ret = new AccountResetPasswordLinkResponse();
            try
            {
                TdlUser user = null;

                // If username contains @ we assume its an e-mail and resolve it to username.
                if (username.Contains("@"))
                {
                    user = await _userManager.FindByEmailAsync(username);
                    _logger.LogInformation(LogMessageType.ResetPassword, $"ResendInvitation\t{ip}\t{username}\tresolved to\tId: {user?.Id.ToString() ?? "<null>"}, UserName: {user?.UserName ?? "<null>"}");
                    if (user != null)
                        username = user.UserName;
                }
                else
                {
                    user = await _userManager.FindByNameAsync(username);
                }

                if (user == null)
                {
                    ret.Success = false;
                    ret.ErrorMessage = "User not found";
                    ret.LocalizedError = CommonMethods.GetErrorLocal(ErrorCode.UserNotFound, _contentDbContext);
                    return ret;
                }

                var token = await _userManager.GeneratePasswordResetTokenAsync(user);
                var result = await _userManager.ResetPasswordAsync(user, token, request.NewPassword);

                if (!result.Succeeded)
                {
                    var ec = 0;
                    _logger.LogInformation(LogMessageType.ResetPassword, $"Resend Invitation denied\t{ip}\t{user}\tResetting Password\tErrors follow.");
                    ret.Errors = result.Errors.Select(e => new ErrorCodeDescription(e.Code, e.Description)).ToList();
                    foreach (var error in result.Errors)
                    {
                        ec++;
                        _logger.LogInformation(LogMessageType.ResetPassword, $"Resend Invitation denied\t{ip}\t{user}\tResetting Password\tError {ec}\t{error.Code ?? "<null>"}\t{error.Description ?? "<null>"}");
                    }

                    ret.Success = false;
                    ret.ErrorMessage = "Resend Invitation denied.";
                    ret.LocalizedError = CommonMethods.GetErrorLocal(ErrorCode.ResendInvitationDenied, _contentDbContext);
                    return ret;
                }

                ret.Success = true;
            }
            catch (Exception exception)
            {
                _logger.LogError(LogMessageType.ResetPassword, exception, $"Resend invitation error\t{ip}\t{username}");
                ret.Success = false;
                ret.ErrorMessage = "Internal error during resend invitation procedure.";
                ret.LocalizedError = CommonMethods.GetErrorLocal(ErrorCode.InternalErrorResendInvitationProcedure, _contentDbContext);
            }
            return ret;
        }
    }
}