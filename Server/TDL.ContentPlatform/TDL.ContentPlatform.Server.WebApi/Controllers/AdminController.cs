﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using TDL.ContentPlatform.Server.Common.Enums;
using TDL.ContentPlatform.Server.Common.Models.Communication;
using TDL.ContentPlatform.Server.Database;
using TDL.ContentPlatform.Server.Database.Tables;
using TDL.ContentPlatform.Server.WebApi.ConfigModels;

namespace TDL.ContentPlatform.Server.WebApi.Controllers
{
    [ApiVersion("1.0")]
    [Authorize(Policy =  "Administrator")]
    [Route("api/v{version:apiVersion}/[controller]/[action]")]
    public class AdminController : Controller
    {
        private readonly ILogger<ConfigController> _logger;

        private readonly ContentDbContext _contentDbContext;

        private readonly IMapper _mapper;
        private readonly ManagedConfig _managedConfig;
        private readonly UserManager<TdlUser> _userManager;
        private readonly RoleManager<TdlRole> _roleManager;
        private readonly SignInManager<TdlUser> _signInManager;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public AdminController(ILogger<ConfigController> logger, ContentDbContext contentDbContext, IMapper mapper, IOptions<ManagedConfig> managedConfig, UserManager<TdlUser> userManager, RoleManager<TdlRole> roleManager, SignInManager<TdlUser> signInManager, IHttpContextAccessor httpContextAccessor)
        {
            _logger = logger;
            _contentDbContext = contentDbContext;
            _mapper = mapper;
            _managedConfig = managedConfig.Value;
            _userManager = userManager;
            _roleManager = roleManager;
            _signInManager = signInManager;
            _httpContextAccessor = httpContextAccessor;
        }

        #region SetupDb
        [HttpGet]
        public async Task<AdminSyncDbResponse> SetupDb(AdminSyncDbRequest request)
        {
            var ret = new AdminSyncDbResponse();

            // Migrate database
            try
            {
                _contentDbContext.Database.Migrate();
            }
            catch (Exception exception)
            {
                _logger.LogError(exception, "Database migration");
                ret.Success = false;
                ret.ErrorMessage = exception.Message;
                return ret;
            }


            // Make sure these roles exist
            foreach (var roleName in Enum.GetNames(typeof(KnownUserRolesEnum)))
            {
                var role = await _roleManager.FindByNameAsync(roleName);
                ret.Success = true;
                try
                {
                    if (role == null)
                    {
                        ret.Log.Add($"Adding non-existant role: {roleName}");
                        var result = await _roleManager.CreateAsync(new TdlRole(roleName));
                        if (!result.Succeeded)
                            foreach (var error in result.Errors)
                                ret.Log.Add($"ErrorMessage adding non-existant role {roleName}: {error.Code} {error.Description}");
                    }
                }
                catch (Exception exception)
                {
                    _logger.LogError(exception, "Creating roles");
                    ret.Success = false;
                    ret.ErrorMessage = exception.Message;
                    return ret;
                }
            }

            foreach (var userPass in _managedConfig.Admin.DefaultSystemUsers)
            {
                try
                {
                    var user = await _userManager.FindByNameAsync(userPass.UserName);
                    if (user == null)
                    {
                        ret.Log.Add($"Adding non-existant user: {userPass.UserName}");
                        user = userPass;
                        var hashedPwd = _userManager.PasswordHasher.HashPassword(user, userPass.Password);
                        user.PasswordHash = hashedPwd;
                        var result = await _userManager.CreateAsync(user);
                        if (!result.Succeeded)
                            foreach (var error in result.Errors)
                                ret.Log.Add($"ErrorMessage adding non-existant user {userPass.UserName}: {error.Code} {error.Description}");
                    }

                    var currentRoles = await _userManager.GetRolesAsync(user);
                    foreach (var roleName in userPass.Roles)
                    {
                        if (currentRoles != null && currentRoles.Contains(roleName))
                            continue;
                        var result=await _userManager.AddToRoleAsync(user, roleName);
                        if (!result.Succeeded)
                            foreach (var error in result.Errors)
                                ret.Log.Add($"ErrorMessage adding new user to role {roleName}: {error.Code} {error.Description}");
                    }
                }
                catch (Exception exception)
                {
                    _logger.LogError(exception, "Creating users");
                    ret.Success = false;
                    ret.ErrorMessage = exception.Message;
                    return ret;
                }

            }

            return ret;
        }
        #endregion
        
    }
}