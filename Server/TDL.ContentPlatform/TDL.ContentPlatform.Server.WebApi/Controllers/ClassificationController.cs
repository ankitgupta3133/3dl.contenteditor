﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using TDL.ContentPlatform.Server.Common.Models.Communication;
using TDL.ContentPlatform.Server.Common.Models.v2;
using TDL.ContentPlatform.Server.Database;
using TDL.ContentPlatform.Server.Database.Tables;

namespace TDL.ContentPlatform.Server.WebApi.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]/[action]")]
    public class ClassificationController : Controller
    {
        private readonly ILogger<ConfigController> _logger;
        private readonly ContentDbContext _contentDbContext;
        private readonly IMapper _mapper;

        public ClassificationController(ILogger<ConfigController> logger, ContentDbContext contentDbContext,
            IMapper mapper)
        {
            _logger = logger;
            _contentDbContext = contentDbContext;
            _mapper = mapper;
        }

        [HttpGet]
        [Authorize(Roles = "Student,Teacher,Administrator")]
        public ClassificationGetDetailsResponse GetDetails(ClassificationGetDetailsRequest request)
        {
            var ret = new ClassificationGetDetailsResponse();

            try
            {
                var db = _contentDbContext;

                //To get asset
                var item = db.Activities
                            .Include(x => x.ClassificationDetails)
                            .Include(x => x.Names).ThenInclude(x => x.Language)
                            .Include(x => x.Platforms)
                            .Where(x => x.ActivityId == request.ClassificationId && x.IsDeleted == false && x.Platforms.Count > 0)
                            .FirstOrDefault();

                if (item == null)
                {
                    ret.Success = false;
                    ret.ErrorMessage = "No classification found";
                    return ret;
                }
                else
                {
                    ret.Classification = _mapper.Map<Common.Models.v2.ClassificationItem>(item);

                    //Get classification details
                    var activityDetails = db.ActivityDetails.Where(x => x.ActivityId == item.ActivityId && x.IsDeleted == false).ToList();
                    ret.Classification.ClassificationDetails = _mapper.Map<List<Common.Models.v2.ActivityDetail>>(activityDetails);

                    //Get all properties
                    var properties = db.ClassificationDetails.Include(c => c.MstProperty).Where(x => x.ClassificationId == item.ActivityId && x.IsDeleted == false && x.IsEnabled == true).ToList();
                    ret.Classification.Properties = _mapper.Map<List<Common.Models.v2.PropertyItem>>(properties);

                    foreach (var property in ret.Classification.Properties)
                    {
                        //Get all assets
                        var models = db.ClassificationAssets.Include(c => c.Asset).Where(x => x.ClassificationDetailId == property.ItemId && x.IsDeleted == false && x.IsEnabled == true).ToList();
                        property.ModelDetails = _mapper.Map<List<Common.Models.v2.ModelItem>>(models);


                        //Dictionary<int, MstPropertyNames[]> lstPropertyNames = db.MstPropertyNames.Where(a => a.PropertyId == property.PropertyId).GroupBy(a => a.PropertyId).ToDictionary(k => k.Key, v => v.ToArray());
                        property.LocalizedName = new Dictionary<string, string>();

                        var lstPropertyNames = (from p in db.MstPropertyNames
                                                join l in db.CsNewLanguage on p.LanguageId equals l.Id
                                                where p.PropertyId == property.PropertyId && p.IsDeleted == false
                                                select new Language
                                                {
                                                    CultureCode = l.NewLanguageshortname,
                                                    Name = p.Name
                                                }).ToList();

                        foreach (var name in lstPropertyNames)
                        {
                            property.LocalizedName.Add(name.CultureCode, name.Name);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                _logger.LogError(exception, "Processing Classification GetDetails request.");
                ret.Success = false;
                ret.ErrorMessage = exception.Message;
                return ret;
            }
            ret.Success = true;
            return ret;
        }
    }
}