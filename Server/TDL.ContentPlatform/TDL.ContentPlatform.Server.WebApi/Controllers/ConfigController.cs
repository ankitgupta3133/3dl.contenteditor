﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using TDL.ContentPlatform.Server.Common.Models.Communication;
using TDL.ContentPlatform.Server.Database;
using TDL.ContentPlatform.Server.Database.Tables;

namespace TDL.ContentPlatform.Server.WebApi.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]/[action]")]
    public class ConfigController : Controller
    {
        private readonly ILogger<ConfigController> _logger;

        private readonly ContentDbContext _contentDbContext;

        private readonly IMapper _mapper;
        //[HttpGet, MapToApiVersion("3.0")]
        //public string GetV3() => "Hello world v3!";

        public ConfigController(ILogger<ConfigController> logger, ContentDbContext contentDbContext, IMapper mapper)
        {
            _logger = logger;
            _contentDbContext = contentDbContext;
            _mapper = mapper;
        }

        [Authorize]
        [HttpGet]
        public ConfigConfigurationResponse ClientConfig()
        {
            var ret = new ConfigConfigurationResponse();

            var configs = _contentDbContext.ClientGeneralConfig.ToList();
            ret.ConfigKeyValuePair = _mapper.Map<List<ConfigKeyValuePair>>(configs);

            _logger.LogInformation("Client requested config.");

            return ret;
        }

    }
}