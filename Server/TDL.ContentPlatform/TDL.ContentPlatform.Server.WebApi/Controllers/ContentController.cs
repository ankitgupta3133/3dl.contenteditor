﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using AutoMapper;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using TDL.ContentPlatform.Server.WebApi.Models;
using TDL.ContentPlatform.Server.Common.Models.Communication;
using TDL.ContentPlatform.Server.Common.Models.Content;
using TDL.ContentPlatform.Server.Common.Models.v2;
using TDL.ContentPlatform.Server.Database;
using TDL.ContentPlatform.Server.Database.Tables;
using TDL.ContentPlatform.Server.WebApi.ConfigModels;
using TDL.ContentPlatform.Server.Common.Utils;
using Asset = TDL.ContentPlatform.Server.Common.Models.v2.Asset;
using AssetContent = TDL.ContentPlatform.Server.Common.Models.v2.AssetContent;
using TDL.ContentPlatform.Server.WebApi.Utils;

namespace TDL.ContentPlatform.Server.WebApi.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]/[action]")]
    public class ContentController : Controller
    {
        private readonly ILogger<ConfigController> _logger;

        private readonly ContentDbContext _contentDbContext;

        private readonly IMapper _mapper;
        private readonly ManagedConfig _managedConfig;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly UserManager<TdlUser> _userManager;
        private readonly IMemoryCache _memoryCache;

        public ContentController(ILogger<ConfigController> logger, ContentDbContext contentDbContext,
            IMapper mapper, IOptions<ManagedConfig> managedConfig, IHttpContextAccessor httpContextAccessor,
            UserManager<TdlUser> userManager, IMemoryCache memoryCache)
        {
            _logger = logger;
            _contentDbContext = contentDbContext;
            _mapper = mapper;
            _managedConfig = managedConfig.Value;
            _httpContextAccessor = httpContextAccessor;
            _userManager = userManager;
            _memoryCache = memoryCache;
        }

        //private static Regex _platformRewriteRegex = new Regex("(Player|Editor)", RegexOptions.Compiled | RegexOptions.IgnoreCase);
        private static Regex _platformArchitectureRewriteRegex = new Regex("(X(86|64)|ARM)$", RegexOptions.Compiled | RegexOptions.IgnoreCase);

        [HttpGet]
        [Authorize(Roles = "Student,Teacher,Administrator")]
        public async Task<ContentAssetListResponse> AssetList(ContentAssetListRequest request)
        {

            // Client output
            ContentAssetListResponse objResponse = new ContentAssetListResponse();

            objResponse.Success = true;
            objResponse.ErrorMessage = string.Empty;

            // Data connection for stored proc
            SqlConnection objConnection = null;

            try
            {

                // Get user id
                string strUserName = User.FindFirst(ClaimTypes.Name).Value;

                // Get user object
                TdlUser objUser = await _userManager.FindByNameAsync(strUserName);

                if (string.IsNullOrWhiteSpace(request.Platform))
                    request.Platform = "All";

                if (request.Platform.Length > 100)
                    throw new Exception("Platform name too long.");

                // Remove "Player" and "Editor" from string. Ref https://docs.unity3d.com/ScriptReference/RuntimePlatform.html
                request.Platform = request.Platform = RewriteRequestPlatform(request.Platform);

                // All platforms
                List<CsNewPlatform> lstPlatforms = _contentDbContext.CsNewPlatform.ToList();

                // Fill dictionary. We do this conservatively in case someone names two platforms the same. Then we don't want API to crash. Therefore we don't use .ToDictionary().
                Dictionary<string, CsNewPlatform> lstAllPlatforms = new Dictionary<string, CsNewPlatform>(StringComparer.OrdinalIgnoreCase); // Also we make dictionary case insensitive

                Dictionary<Guid, CsNewPlatform> lstAllPlatformIDs = new Dictionary<Guid, CsNewPlatform>(); // Also we make dictionary case insensitive

                foreach (CsNewPlatform objPlatform in lstPlatforms)
                {

                    if (!lstAllPlatforms.ContainsKey(objPlatform.NewName))
                        lstAllPlatforms.Add(objPlatform.NewName, objPlatform);

                    if (!lstAllPlatformIDs.ContainsKey(objPlatform.Id))
                        lstAllPlatformIDs.Add(objPlatform.Id, objPlatform);

                } // foreach (CsNewPlatform objPlatform in lstPlatforms)

                // Get special "All"-platform
                lstAllPlatforms.TryGetValue("All", out CsNewPlatform objAllPlatform);

                if (objAllPlatform == null)
                    throw new Exception("Platform \"All\" does not exist in database.");

                // Get platform
                lstAllPlatforms.TryGetValue(request.Platform, out var objOutputPlatform);

                if (objOutputPlatform == null)
                {

                    objResponse.Success = false;
                    objResponse.ErrorMessage = "Invalid Platform";
                    objResponse.LocalizedError = CommonMethods.GetErrorLocal(ErrorCode.InvalidPlatform, _contentDbContext);

                    return objResponse;

                }

                // Open connection for stored procedure
                objConnection = new SqlConnection(_contentDbContext.Database.GetDbConnection().ConnectionString);

                // Used to avoid wrong execution plan being used by mssql when calling from C# clients
                SqlCommand objCommand = new SqlCommand("set arithabort on;", objConnection);

                objCommand.Connection.Open();
                objCommand.ExecuteNonQuery();

                // Indefinite timeout
                objCommand.CommandTimeout = 0;

                // Prepare a call to the stored procedure
                objCommand = new SqlCommand("dbo.spAssetList", objConnection);
                objCommand.CommandType = CommandType.StoredProcedure;

                // Create parameters                
                objCommand.Parameters.Add(new SqlParameter("userId", objUser.Id));
                objCommand.Parameters.Add(new SqlParameter("platform", objOutputPlatform.Id));
                objCommand.Parameters.Add(new SqlParameter("isDetail", request.isDetail));

                objCommand.CommandTimeout = 0;

                // Execute the stored procedure
                SqlDataReader objDataReader = objCommand.ExecuteReader();

                // There will be only one row, since the
                // result is rendered as JSON
                if (objDataReader.Read() == false)
                    return objResponse;

                // If there are no results, the data will be DBNull
                object objData = objDataReader.GetValue(0);

                if (objData == DBNull.Value)
                    return objResponse;

                // Parse database response
                objResponse.ContentRoot = JsonConvert.DeserializeObject<ContentRoot[]>(objData.ToString()).First();

                // All asset names
                Dictionary<int, AssetNames[]> lstAssetNames = _contentDbContext.AssetNames.GroupBy(a => a.AssetId).ToDictionary(k => k.Key, v => v.ToArray());

                // All languages
                List<CsNewLanguage> lstDbLanguages = _contentDbContext.CsNewLanguage.ToList();

                // Assign language
                lstAssetNames.SelectMany(a => a.Value).ToList().ForEach(delegate (AssetNames objAssetName)
                {
                    objAssetName.Language = lstDbLanguages.SingleOrDefault(l => l.Id == objAssetName.LanguageId);
                });

                // All asset tags
                //Dictionary<int, AssetTags[]> lstAssetTags = _contentDbContext.AssetTags.GroupBy(a => a.AssetId).ToDictionary(k => k.Key, v => v.ToArray());

                // All asset properties
                //Dictionary<int, AssetProperties[]> lstAssetProperties = _contentDbContext.AssetProperties.GroupBy(a => a.AssetId).ToDictionary(k => k.Key, v => v.ToArray());

                List<Asset> lstAsset = new List<Asset>();

                // Used languages
                List<CsNewLanguage> lstLanguages = new List<CsNewLanguage>();

                // Load grades, subjects and topics in order to print type count
                foreach (Grade objGrade in objResponse.ContentRoot.Grades)
                {
                    foreach (Subject objSubject in objGrade.Subjects)
                    {
                        foreach (Topic objTopic in objSubject.Topics)
                        {
                            foreach (Subtopic objSubtopic in objTopic.Subtopics)
                            {
                                List<Asset> lstSubtopicAssetRemove = new List<Asset>();

                                foreach (Asset objAsset in objSubtopic.Assets)
                                {

                                    if (request.isDetail && objAsset.AssetContents.Count == 0)
                                    {

                                        lstSubtopicAssetRemove.Add(objAsset);
                                        continue;

                                    } // if (objAsset.AssetContents.Count == 0)

                                    if (string.IsNullOrWhiteSpace(objSubtopic.ThumbnailHash) == true && objAsset.AssetContents.Count() > 0)
                                        objSubtopic.ThumbnailHash = objAsset.AssetContents.First()?.ThumbnailHash;

                                    if (string.IsNullOrWhiteSpace(objSubtopic.ThumbnailHash) == false && (objSubtopic.ThumbnailCdnUrls == null || objSubtopic.ThumbnailCdnUrls?.Count == 0))
                                    {

                                        if (objSubtopic.ThumbnailCdnUrls == null)
                                            objSubtopic.ThumbnailCdnUrls = new List<CdnUrl>();

                                        objSubtopic.ThumbnailCdnUrls.Add(new CdnUrl
                                        {
                                            Url = _managedConfig.Assets.ThumbnailBaseUrl.Replace("{filename}", Uri.EscapeUriString(objSubtopic.ThumbnailHash))
                                        });

                                    } // if (string.IsNullOrWhiteSpace(objSubtopic.ThumbnailHash) == false && (objSubtopic.ThumbnailCdnUrls == null || objSubtopic.ThumbnailCdnUrls?.Count == 0))

                                    objAsset.AssetContents.ForEach(delegate (AssetContent objAssetContent)
                                    {

                                        if (string.IsNullOrWhiteSpace(objAssetContent.ThumbnailHash) == false)
                                            objAssetContent.ThumbnailCdnUrls.Add(new CdnUrl
                                            {
                                                Url = _managedConfig.Assets.ThumbnailBaseUrl.Replace("{filename}", Uri.EscapeUriString(objAssetContent.ThumbnailHash))
                                            });

                                        if (string.IsNullOrWhiteSpace(objAssetContent.BackgroundHash) == false)
                                            objAssetContent.BackgroundCdnUrls.Add(new CdnUrl
                                            {
                                                Url = _managedConfig.Assets.BackgroundBaseUrl.Replace("{filename}", Uri.EscapeUriString(objAssetContent.BackgroundHash))
                                            });

                                        objAssetContent.FileCdnUrls.Add(new CdnUrl
                                        {
                                            Url = _managedConfig.Assets.FileBaseUrl.Replace("{filename}", Uri.EscapeUriString(objAssetContent.FileHash))
                                        });

                                        if (objAssetContent.Metadata == null)
                                            objAssetContent.Metadata = new Common.Models.v2.AssetContentMetadata();

                                    });

                                    foreach (AssociatedContent objAssociatedAsset in objAsset.AssociatedContents)
                                    {

                                        if (string.IsNullOrWhiteSpace(objAssociatedAsset.ThumbnailHash) == false)
                                        {

                                            objAssociatedAsset.ThumbnailCdnUrls = new List<CdnUrl>();

                                            objAssociatedAsset.ThumbnailCdnUrls.Add(new CdnUrl
                                            {
                                                Url = _managedConfig.Assets.ThumbnailBaseUrl.Replace("{filename}", Uri.EscapeUriString(objAssociatedAsset.ThumbnailHash))
                                            });

                                        } // if (string.IsNullOrWhiteSpace(objAssociatedAsset.ThumbnailHash) == false)

                                        if (string.IsNullOrWhiteSpace(objAssociatedAsset.BackgroundHash) == false)
                                        {

                                            objAssociatedAsset.BackgroundCdnUrls = new List<CdnUrl>();

                                            objAssociatedAsset.BackgroundCdnUrls.Add(new CdnUrl
                                            {
                                                Url = _managedConfig.Assets.BackgroundBaseUrl.Replace("{filename}", Uri.EscapeUriString(objAssociatedAsset.BackgroundHash))
                                            });

                                        }

                                        if (string.IsNullOrWhiteSpace(objAssociatedAsset.FileHash) == false)
                                            objAssociatedAsset.FileCdnUrls.Add(new CdnUrl
                                            {
                                                Url = _managedConfig.Assets.FileBaseUrl.Replace("{filename}", Uri.EscapeUriString(objAssociatedAsset.FileHash))
                                            });

                                    } // foreach (AssociatedContent objAssociatedAsset in objAsset.AssociatedContents)

                                    objAsset.LocalizedName = new Dictionary<string, string>();

                                    if (lstAssetNames.ContainsKey(objAsset.Id))
                                    {
                                        foreach (AssetNames objAssetName in lstAssetNames[objAsset.Id].Where(a => a.Language != null && a.IsDeleted == false).OrderBy(a => a.Language.NewLanguageshortname))
                                        {
                                            try
                                            {
                                                //todo fix it as this is just a quick fix for Nibiru_VR & Oculus
                                                if ((objOutputPlatform.Id == new Guid("BD023CC5-7637-E911-A961-000D3AB6D5D9") || objOutputPlatform.Id == new Guid("8C353671-0FAC-E811-A974-000D3AB6E3A0"))
                                                    && objAssetName.Language.Id == new Guid("03CAB042-5975-E911-A969-000D3AB6D5D9"))
                                                    continue;

                                                objAsset.LocalizedName.Add(objAssetName.Language.NewLanguageshortname, objAssetName.Name);

                                                if (lstLanguages.Any(l => l.Id == objAssetName.LanguageId) == false)
                                                    lstLanguages.Add(objAssetName.Language);

                                            }
                                            catch { }
                                        } // foreach (AssetNames objAssetName in lstAssetNames[objAsset.Id].Where(a => a.Language != null && a.IsDeleted == false).OrderBy(a => a.Language.NewLanguageshortname))

                                    } // if (lstAssetNames.ContainsKey(objAsset.Id))

                                    /*if (lstAssetTags.ContainsKey(objAsset.Id))
                                    {
                                        foreach (AssetTags objAssetTag in lstAssetTags[objAsset.Id])
                                        {
                                            var listTags = (from m in _contentDbContext.MstTag
                                                        join n in _contentDbContext.MstTagNames on m.TagId equals n.TagId
                                                        join l in lstDbLanguages on n.LanguageId equals l.Id
                                                        where m.TagId == objAssetTag.MstTagId
                                                        select new Language
                                                        {
                                                            CultureCode = l.NewLanguageshortname,
                                                            Name = n.Name
                                                        }).ToList();

                                            foreach (var tag in listTags)
                                            {
                                                objAsset.Tags.Add(tag.CultureCode, tag.Name);
                                            }
                                        }
                                    } // if (lstAssetTags.ContainsKey(objAsset.Id))

                                    if (lstAssetProperties.ContainsKey(objAsset.Id))
                                    {
                                        foreach (AssetProperties objAssetProperty in lstAssetProperties[objAsset.Id])
                                        {
                                            var listProperties = (from m in _contentDbContext.MstProperty
                                                            join n in _contentDbContext.MstPropertyNames on m.PropertyId equals n.PropertyId
                                                            join l in lstDbLanguages on n.LanguageId equals l.Id
                                                            where m.PropertyId == objAssetProperty.MstPropertyId
                                                            select new Language
                                                            {
                                                                CultureCode = l.NewLanguageshortname,
                                                                Name = n.Name
                                                            }).ToList();

                                            foreach (var property in listProperties)
                                            {
                                                objAsset.Properties.Add(property.CultureCode, property.Name);
                                            }
                                        }
                                    } // if (lstAssetProperties.ContainsKey(objAsset.Id))*/

                                } // foreach (Asset objAsset in objSubtopic.Assets)

                                if (lstSubtopicAssetRemove.Any())
                                    objSubtopic.Assets.RemoveAll(a => lstSubtopicAssetRemove.Select(r => r.Id).Contains(a.Id));

                                if (request.isDetail)
                                    objSubtopic.RecursiveTypeCount = objSubtopic.Assets.GroupBy(a => a.Type).ToDictionary(k => k.Key, v => v.Count());
                                else
                                    objSubtopic.RecursiveTypeCount = objSubtopic.AssetCountByType.GroupBy(a => a.Type).ToDictionary(k => k.Key, v => v.Sum(s => s.Count));

                                lstAsset.AddRange(objSubtopic.Assets);
                            } // foreach (Subtopic objSubtopic in objTopic.Subtopics)

                            List<Asset> lstTopicAssetRemove = new List<Asset>();

                            foreach (Asset objAsset in objTopic.Assets)
                            {

                                if (request.isDetail && objAsset.AssetContents.Count == 0)
                                {

                                    lstTopicAssetRemove.Add(objAsset);
                                    continue;

                                } // if (objAsset.AssetContents.Count == 0)

                                objAsset.AssetContents.ForEach(delegate (AssetContent objAssetContent)
                                {

                                    if (string.IsNullOrWhiteSpace(objAssetContent.ThumbnailHash) == false)
                                        objAssetContent.ThumbnailCdnUrls.Add(new CdnUrl
                                        {
                                            Url = _managedConfig.Assets.ThumbnailBaseUrl.Replace("{filename}", Uri.EscapeUriString(objAssetContent.ThumbnailHash))
                                        });

                                    if (string.IsNullOrWhiteSpace(objAssetContent.BackgroundHash) == false)
                                        objAssetContent.BackgroundCdnUrls.Add(new CdnUrl
                                        {
                                            Url = _managedConfig.Assets.BackgroundBaseUrl.Replace("{filename}", Uri.EscapeUriString(objAssetContent.BackgroundHash))
                                        });

                                    objAssetContent.FileCdnUrls.Add(new CdnUrl
                                    {
                                        Url = _managedConfig.Assets.FileBaseUrl.Replace("{filename}", Uri.EscapeUriString(objAssetContent.FileHash))
                                    });

                                    if (objAssetContent.Metadata == null)
                                        objAssetContent.Metadata = new Common.Models.v2.AssetContentMetadata();

                                });

                                foreach (AssociatedContent objAssociatedAsset in objAsset.AssociatedContents)
                                {

                                    if (string.IsNullOrWhiteSpace(objAssociatedAsset.ThumbnailHash) == false)
                                    {

                                        objAssociatedAsset.ThumbnailCdnUrls = new List<CdnUrl>();

                                        objAssociatedAsset.ThumbnailCdnUrls.Add(new CdnUrl
                                        {
                                            Url = _managedConfig.Assets.ThumbnailBaseUrl.Replace("{filename}", Uri.EscapeUriString(objAssociatedAsset.ThumbnailHash))
                                        });

                                    } // if (string.IsNullOrWhiteSpace(objAssociatedAsset.ThumbnailHash) == false)

                                    if (string.IsNullOrWhiteSpace(objAssociatedAsset.BackgroundHash) == false)
                                    {

                                        objAssociatedAsset.BackgroundCdnUrls = new List<CdnUrl>();

                                        objAssociatedAsset.BackgroundCdnUrls.Add(new CdnUrl
                                        {
                                            Url = _managedConfig.Assets.BackgroundBaseUrl.Replace("{filename}", Uri.EscapeUriString(objAssociatedAsset.BackgroundHash))
                                        });

                                    }

                                    if (string.IsNullOrWhiteSpace(objAssociatedAsset.FileHash) == false)
                                        objAssociatedAsset.FileCdnUrls.Add(new CdnUrl
                                        {
                                            Url = _managedConfig.Assets.FileBaseUrl.Replace("{filename}", Uri.EscapeUriString(objAssociatedAsset.FileHash))
                                        });

                                } // foreach (AssociatedContent objAssociatedAsset in objAsset.AssociatedContents)

                                objAsset.LocalizedName = new Dictionary<string, string>();

                                if (lstAssetNames.ContainsKey(objAsset.Id))
                                {
                                    foreach (AssetNames objAssetName in lstAssetNames[objAsset.Id].Where(a => a.Language != null && a.IsDeleted == false).OrderBy(a => a.Language.NewLanguageshortname))
                                    {
                                        try
                                        {
                                            //todo fix it as this is just a quick fix for Nibiru_VR & Oculus
                                            if ((objOutputPlatform.Id == new Guid("BD023CC5-7637-E911-A961-000D3AB6D5D9") || objOutputPlatform.Id == new Guid("8C353671-0FAC-E811-A974-000D3AB6E3A0"))
                                                && objAssetName.Language.Id == new Guid("03CAB042-5975-E911-A969-000D3AB6D5D9"))
                                                continue;

                                            objAsset.LocalizedName.Add(objAssetName.Language.NewLanguageshortname, objAssetName.Name);

                                            if (lstLanguages.Any(l => l.Id == objAssetName.LanguageId) == false)
                                                lstLanguages.Add(objAssetName.Language);

                                        }
                                        catch { }
                                    } // foreach (AssetNames objAssetName in lstAssetNames[objAsset.Id].Where(a => a.Language != null && a.IsDeleted == false).OrderBy(a => a.Language.NewLanguageshortname))

                                } // if (lstAssetNames.ContainsKey(objAsset.Id))

                                /*if (lstAssetTags.ContainsKey(objAsset.Id))
                                {
                                    foreach (AssetTags objAssetTag in lstAssetTags[objAsset.Id])
                                    {
                                        var listTags = (from m in _contentDbContext.MstTag
                                                        join n in _contentDbContext.MstTagNames on m.TagId equals n.TagId
                                                        join l in lstDbLanguages on n.LanguageId equals l.Id
                                                        where m.TagId == objAssetTag.MstTagId
                                                        select new Language
                                                        {
                                                            CultureCode = l.NewLanguageshortname,
                                                            Name = n.Name
                                                        }).ToList();

                                        foreach (var tag in listTags)
                                        {
                                            objAsset.Tags.Add(tag.CultureCode, tag.Name);
                                        }
                                    }
                                } // if (lstAssetTags.ContainsKey(objAsset.Id))

                                if (lstAssetProperties.ContainsKey(objAsset.Id))
                                {
                                    foreach (AssetProperties objAssetProperty in lstAssetProperties[objAsset.Id])
                                    {
                                        var listProperties = (from m in _contentDbContext.MstProperty
                                                              join n in _contentDbContext.MstPropertyNames on m.PropertyId equals n.PropertyId
                                                              join l in lstDbLanguages on n.LanguageId equals l.Id
                                                              where m.PropertyId == objAssetProperty.MstPropertyId
                                                              select new Language
                                                              {
                                                                  CultureCode = l.NewLanguageshortname,
                                                                  Name = n.Name
                                                              }).ToList();

                                        foreach (var property in listProperties)
                                        {
                                            objAsset.Properties.Add(property.CultureCode, property.Name);
                                        }
                                    }
                                } // if (lstAssetProperties.ContainsKey(objAsset.Id))*/

                            } // foreach (Asset objAsset in objTopic.Assets)

                            if (lstTopicAssetRemove.Any())
                                objTopic.Assets.RemoveAll(a => lstTopicAssetRemove.Select(r => r.Id).Contains(a.Id));

                            if (request.isDetail)
                                objTopic.RecursiveTypeCount = objTopic.Subtopics.SelectMany(st => st.Assets).Union(objTopic.Assets).GroupBy(a => a.Type).ToDictionary(k => k.Key, v => v.Count());
                            else
                            {
                                objTopic.RecursiveTypeCount = objTopic.Subtopics.SelectMany(st => st.AssetCountByType).Union(objTopic.AssetCountByType).GroupBy(a => a.Type).ToDictionary(k => k.Key, v => v.Sum(s => s.Count));
                            }

                            if (string.IsNullOrWhiteSpace(objTopic.ThumbnailHash) == true)
                            {
                                // Try subtopic
                                objTopic.ThumbnailHash = objTopic.Assets.SelectMany(a => a.AssetContents).Where(ac => string.IsNullOrWhiteSpace(ac.ThumbnailHash) == false).Select(st => st.ThumbnailHash).FirstOrDefault();

                                if (string.IsNullOrWhiteSpace(objTopic.ThumbnailHash) == false)
                                    objTopic.ThumbnailCdnUrls.Add(new CdnUrl
                                    {
                                        Url = _managedConfig.Assets.ThumbnailBaseUrl.Replace("{filename}", Uri.EscapeUriString(objTopic.ThumbnailHash))
                                    });

                                if (string.IsNullOrWhiteSpace(objTopic.ThumbnailHash) == true)
                                {

                                    // Try subtopic
                                    objTopic.ThumbnailHash = objTopic.Subtopics.Where(st => string.IsNullOrWhiteSpace(st.ThumbnailHash) == false).Select(st => st.ThumbnailHash).FirstOrDefault();

                                    if (string.IsNullOrWhiteSpace(objTopic.ThumbnailHash) == false)
                                        objTopic.ThumbnailCdnUrls.Add(new CdnUrl
                                        {
                                            Url = _managedConfig.Assets.ThumbnailBaseUrl.Replace("{filename}", Uri.EscapeUriString(objTopic.ThumbnailHash))
                                        });

                                } // if (string.IsNullOrWhiteSpace(objTopic.ThumbnailHash) == true)

                            }
                            else
                            {
                                objTopic.ThumbnailCdnUrls.Add(new CdnUrl
                                {
                                    Url = _managedConfig.Assets.ThumbnailBaseUrl.Replace("{filename}", Uri.EscapeUriString(objTopic.ThumbnailHash))
                                });
                            }// if (string.IsNullOrWhiteSpace(objTopic.ThumbnailHash) == true)

                            lstAsset.AddRange(objTopic.Assets);
                        } // foreach (Topic objTopic in objSubject.Topics)

                        List<Asset> lstSubjectAssetRemove = new List<Asset>();

                        foreach (Asset objAsset in objSubject.Assets)
                        {

                            if (request.isDetail && objAsset.AssetContents.Count == 0)
                            {

                                lstSubjectAssetRemove.Add(objAsset);
                                continue;

                            } // if (objAsset.AssetContents.Count == 0)

                            objAsset.AssetContents.ForEach(delegate (AssetContent objAssetContent)
                            {
                                if (string.IsNullOrWhiteSpace(objAssetContent.ThumbnailHash) == false)
                                    objAssetContent.ThumbnailCdnUrls.Add(new CdnUrl
                                    {
                                        Url = _managedConfig.Assets.ThumbnailBaseUrl.Replace("{filename}", Uri.EscapeUriString(objAssetContent.ThumbnailHash))
                                    });

                                if (string.IsNullOrWhiteSpace(objAssetContent.BackgroundHash) == false)
                                    objAssetContent.BackgroundCdnUrls.Add(new CdnUrl
                                    {
                                        Url = _managedConfig.Assets.BackgroundBaseUrl.Replace("{filename}", Uri.EscapeUriString(objAssetContent.BackgroundHash))
                                    });

                                objAssetContent.FileCdnUrls.Add(new CdnUrl
                                {
                                    Url = _managedConfig.Assets.FileBaseUrl.Replace("{filename}", Uri.EscapeUriString(objAssetContent.FileHash))
                                });

                                if (objAssetContent.Metadata == null)
                                    objAssetContent.Metadata = new Common.Models.v2.AssetContentMetadata();
                            });

                            foreach (AssociatedContent objAssociatedAsset in objAsset.AssociatedContents)
                            {
                                if (string.IsNullOrWhiteSpace(objAssociatedAsset.ThumbnailHash) == false)
                                {

                                    objAssociatedAsset.ThumbnailCdnUrls = new List<CdnUrl>();

                                    objAssociatedAsset.ThumbnailCdnUrls.Add(new CdnUrl
                                    {
                                        Url = _managedConfig.Assets.ThumbnailBaseUrl.Replace("{filename}", Uri.EscapeUriString(objAssociatedAsset.ThumbnailHash))
                                    });

                                } // if (string.IsNullOrWhiteSpace(objAssociatedAsset.ThumbnailHash) == false)

                                if (string.IsNullOrWhiteSpace(objAssociatedAsset.BackgroundHash) == false)
                                {

                                    objAssociatedAsset.BackgroundCdnUrls = new List<CdnUrl>();

                                    objAssociatedAsset.BackgroundCdnUrls.Add(new CdnUrl
                                    {
                                        Url = _managedConfig.Assets.BackgroundBaseUrl.Replace("{filename}", Uri.EscapeUriString(objAssociatedAsset.BackgroundHash))
                                    });

                                }

                                if (string.IsNullOrWhiteSpace(objAssociatedAsset.FileHash) == false)
                                    objAssociatedAsset.FileCdnUrls.Add(new CdnUrl
                                    {
                                        Url = _managedConfig.Assets.FileBaseUrl.Replace("{filename}", Uri.EscapeUriString(objAssociatedAsset.FileHash))
                                    });
                            } // foreach (AssociatedContent objAssociatedAsset in objAsset.AssociatedContents)

                            objAsset.LocalizedName = new Dictionary<string, string>();

                            if (lstAssetNames.ContainsKey(objAsset.Id))
                            {
                                foreach (AssetNames objAssetName in lstAssetNames[objAsset.Id].Where(a => a.Language != null && a.IsDeleted == false).OrderBy(a => a.Language.NewLanguageshortname))
                                {
                                    try
                                    {
                                        //todo fix it as this is just a quick fix for Nibiru_VR & Oculus
                                        if ((objOutputPlatform.Id == new Guid("BD023CC5-7637-E911-A961-000D3AB6D5D9") || objOutputPlatform.Id == new Guid("8C353671-0FAC-E811-A974-000D3AB6E3A0"))
                                            && objAssetName.Language.Id == new Guid("03CAB042-5975-E911-A969-000D3AB6D5D9"))
                                            continue;

                                        objAsset.LocalizedName.Add(objAssetName.Language.NewLanguageshortname, objAssetName.Name);

                                        if (lstLanguages.Any(l => l.Id == objAssetName.LanguageId) == false)
                                            lstLanguages.Add(objAssetName.Language);

                                    }
                                    catch { }
                                } // foreach (AssetNames objAssetName in lstAssetNames[objAsset.Id].Where(a => a.Language != null && a.IsDeleted == false).OrderBy(a => a.Language.NewLanguageshortname))

                            } // if (lstAssetNames.ContainsKey(objAsset.Id))

                        } // foreach (Asset objAsset in objSubject.Assets)

                        if (lstSubjectAssetRemove.Any())
                            objSubject.Assets.RemoveAll(a => lstSubjectAssetRemove.Select(r => r.Id).Contains(a.Id));

                        if (string.IsNullOrWhiteSpace(objSubject.ThumbnailHash) == true)
                        {
                            // Try topic
                            objSubject.ThumbnailHash = objSubject.Topics.Where(t => string.IsNullOrWhiteSpace(t.ThumbnailHash) == false).Select(t => t.ThumbnailHash).FirstOrDefault();

                            if (string.IsNullOrWhiteSpace(objSubject.ThumbnailHash) == true)
                                // Try subtopic
                                objSubject.ThumbnailHash = objSubject.Topics.SelectMany(t => t.Subtopics).Where(st => string.IsNullOrWhiteSpace(st.ThumbnailHash) == false).Select(st => st.ThumbnailHash).FirstOrDefault();

                            if (string.IsNullOrWhiteSpace(objSubject.ThumbnailHash) == false)
                                objSubject.ThumbnailCdnUrls.Add(new CdnUrl
                                {
                                    Url = _managedConfig.Assets.ThumbnailBaseUrl.Replace("{filename}", Uri.EscapeUriString(objSubject.ThumbnailHash))
                                });
                        }
                        else
                        {
                            objSubject.ThumbnailCdnUrls.Add(new CdnUrl
                            {
                                Url = _managedConfig.Assets.ThumbnailBaseUrl.Replace("{filename}", Uri.EscapeUriString(objSubject.ThumbnailHash))
                            });
                        }// if (string.IsNullOrWhiteSpace(objSubject.ThumbnailHash) == true)

                        if (request.isDetail)
                            objSubject.RecursiveTypeCount = objSubject.Topics.SelectMany(t => t.Assets).Union(objSubject.Topics.SelectMany(t => t.Subtopics.SelectMany(st => st.Assets))).Union(objSubject.Assets).GroupBy(a => a.Type).ToDictionary(k => k.Key, v => v.Count());
                        else
                        {
                            objSubject.RecursiveTypeCount = objSubject.Topics.SelectMany(t => t.AssetCountByType).Union(objSubject.Topics.SelectMany(t => t.Subtopics.SelectMany(st => st.AssetCountByType))).Union(objSubject.AssetCountByType).GroupBy(a => a.Type).ToDictionary(k => k.Key, v => v.Sum(s => s.Count));
                        }

                        lstAsset.AddRange(objSubject.Assets);
                    } // foreach (Subject objSubject in objGrade.Subjects)

                    if (string.IsNullOrWhiteSpace(objGrade.ThumbnailHash) == true)
                    {
                        // Try subject
                        objGrade.ThumbnailHash = objGrade.Subjects.Where(s => string.IsNullOrWhiteSpace(s.ThumbnailHash) == false).Select(s => s.ThumbnailHash).FirstOrDefault();

                        if (string.IsNullOrWhiteSpace(objGrade.ThumbnailHash) == true)
                        {

                            // Try topic
                            objGrade.ThumbnailHash = objGrade.Subjects.SelectMany(s => s.Topics).Where(t => string.IsNullOrWhiteSpace(t.ThumbnailHash) == false).Select(t => t.ThumbnailHash).FirstOrDefault();

                            if (string.IsNullOrWhiteSpace(objGrade.ThumbnailHash) == true)
                                // Try subtopic
                                objGrade.ThumbnailHash = objGrade.Subjects.SelectMany(s => s.Topics.SelectMany(t => t.Subtopics)).Where(st => string.IsNullOrWhiteSpace(st.ThumbnailHash) == false).Select(st => st.ThumbnailHash).FirstOrDefault();

                        } // if (string.IsNullOrWhiteSpace(objGrade.ThumbnailHash) == true)

                    } // if (string.IsNullOrWhiteSpace(objGrade.ThumbnailHash) == true)

                    if (string.IsNullOrWhiteSpace(objGrade.ThumbnailHash) == false)
                        objGrade.ThumbnailCdnUrls.Add(new CdnUrl
                        {
                            Url = _managedConfig.Assets.ThumbnailBaseUrl.Replace("{filename}", Uri.EscapeUriString(objGrade.ThumbnailHash))
                        });

                    if (request.isDetail)
                        objGrade.RecursiveTypeCount = objGrade.Subjects.SelectMany(s => s.Assets).Union(objGrade.Subjects.SelectMany(s => s.Topics.SelectMany(t => t.Assets))).Union(objGrade.Subjects.SelectMany(s => s.Topics.SelectMany(t => t.Subtopics.SelectMany(st => st.Assets)))).Union(objGrade.Assets).GroupBy(a => a.Type).ToDictionary(k => k.Key, v => v.Count());
                    else
                    {
                        objGrade.RecursiveTypeCount = objGrade.Subjects.SelectMany(s => s.AssetCountByType).Union(objGrade.Subjects.SelectMany(s => s.Topics.SelectMany(t => t.AssetCountByType))).Union(objGrade.Subjects.SelectMany(s => s.Topics.SelectMany(t => t.Subtopics.SelectMany(st => st.AssetCountByType)))).GroupBy(a => a.Type).ToDictionary(k => k.Key, v => v.Sum(s => s.Count));
                    }

                } // foreach (Grade objGrade in objResponse.ContentRoot.Grades)

                // Do some cleanup: We only return levels that have actual content
                if (!request.IncludeEmptyLevels)
                {
                    foreach (var grade in objResponse.ContentRoot.Grades.ToArray())
                    {
                        foreach (var subject in grade.Subjects.ToArray())
                        {
                            foreach (var topic in subject.Topics.ToArray())
                            {
                                foreach (var subtopic in topic.Subtopics.ToArray())
                                {
                                    if (request.isDetail && subtopic.Assets.Count == 0)
                                        topic.Subtopics.Remove(subtopic);
                                    else if (request.isDetail == false && subtopic.RecursiveTypeCount.Count == 0)
                                        topic.Subtopics.Remove(subtopic);
                                } // foreach (var subtopic in topic.Subtopics.ToArray())

                                if (request.isDetail && topic.Assets.Count == 0 && topic.Subtopics.Count == 0)
                                    subject.Topics.Remove(topic);
                                else if (request.isDetail == false && topic.RecursiveTypeCount.Count == 0 && topic.Subtopics.Count(X => X.RecursiveTypeCount.Count > 0) == 0)
                                    subject.Topics.Remove(topic);

                            } // foreach (var topic in subject.Topics.ToArray())

                            if (request.isDetail && subject.Assets.Count == 0 && subject.Topics.Count == 0)
                                grade.Subjects.Remove(subject);
                            else if (request.isDetail == false && subject.RecursiveTypeCount.Count == 0 && subject.Topics.Count(X => X.RecursiveTypeCount.Count > 0) == 0)
                                grade.Subjects.Remove(subject);

                        } // foreach (var subject in grade.Subjects.ToArray())

                        if (request.isDetail && grade.Assets.Count == 0 && grade.Subjects.Count == 0)
                            objResponse.ContentRoot.Grades.Remove(grade);
                        else if (request.isDetail == false && grade.Subjects.Count(X => X.RecursiveTypeCount.Count > 0) == 0)
                            objResponse.ContentRoot.Grades.Remove(grade);

                    } // foreach (var grade in objResponse.ContentRoot.Grades.ToArray())

                } // if (!request.IncludeEmptyLevels)

                if (lstLanguages.Any() == true)
                    objResponse.ContentRoot.Languages = (from l in lstLanguages
                                                         select new Language
                                                         {
                                                             CultureCode = l.NewLanguageshortname,
                                                             Id = l.Id,
                                                             Name = l.NewName
                                                         }).ToList();

                // Quiz item
                Common.Models.v2.Activities objQuiz = objResponse.ContentRoot.Activities.FirstOrDefault(a => a.ActivityName == "Quiz");

                if (objQuiz != null)
                {
                    List<ActivityContent> lstAssetContentRemove = new List<ActivityContent>();
                    foreach (ActivityItem objItem in objQuiz.ActivityItem)
                    {
                        lstAssetContentRemove = objItem.AssetContent.Where(ac => !lstAsset.Any(a => a.Id == ac.AssetId)).ToList();

                        if (lstAssetContentRemove.Any())
                            objItem.AssetContent.RemoveAll(a => lstAssetContentRemove.Select(r => r.AssetId).Contains(a.AssetId));
                    }

                    objQuiz.ActivityItem.RemoveAll(i => i.AssetContent.Count == 0);

                    foreach (ActivityItem objItem in objQuiz.ActivityItem.Where(a => a.IsVisible == true))
                    {
                        // Remove items with no attempts or labels
                        objItem.AssetContent.RemoveAll(ac => ac.NumberOfLabels <= 0 || ac.NumberOfAttempts <= 0);
                        objItem.IsVisible = objItem.AssetContent.Any();

                    } // foreach (ActivityItem objItem in objQuiz.ActivityItem.Where(a => a.IsVisible == true))

                    ActivityItem[] lstMultipleQuizItems = objQuiz.ActivityItem.Where(i => i.IsModelSpecific == false).ToArray();

                    if (lstMultipleQuizItems.Any() == true)
                    {
                        objQuiz.ActivityItem.RemoveAll(i => i.IsModelSpecific == false);
                        objResponse.ContentRoot.Activities.Add(new Common.Models.v2.Activities
                        {
                            ActivityId = objQuiz.ActivityId,
                            ActivityName = "Multiple Quiz",
                            ActivityItem = lstMultipleQuizItems.ToList()
                        });
                    } // if (lstMultipleQuizItems.Any() == true)

                } // if (objQuiz != null)

                // Puzzle item
                Common.Models.v2.Activities objPuzzle = objResponse.ContentRoot.Activities.FirstOrDefault(a => a.ActivityName == "Puzzle");

                if (objPuzzle != null)
                {
                    List<ActivityContent> lstAssetContentRemove = new List<ActivityContent>();
                    foreach (ActivityItem objItem in objPuzzle.ActivityItem)
                    {
                        lstAssetContentRemove = objItem.AssetContent.Where(ac => !lstAsset.Any(a => a.Id == ac.AssetId)).ToList();

                        if (lstAssetContentRemove.Any())
                            objItem.AssetContent.RemoveAll(a => lstAssetContentRemove.Select(r => r.AssetId).Contains(a.AssetId));
                    }

                    objPuzzle.ActivityItem.RemoveAll(i => i.AssetContent.Count == 0);

                    ActivityItem[] lstMultiplePuzzleItems = objPuzzle.ActivityItem.Where(i => i.IsModelSpecific == false).ToArray();

                    if (lstMultiplePuzzleItems.Any() == true)
                    {
                        objPuzzle.ActivityItem.RemoveAll(i => i.IsModelSpecific == false);
                        objResponse.ContentRoot.Activities.Add(new Common.Models.v2.Activities
                        {
                            ActivityId = objPuzzle.ActivityId,
                            ActivityName = "Multiple Puzzle",
                            ActivityItem = lstMultiplePuzzleItems.ToList()
                        });

                    } // if (lstMultiplePuzzleItems.Any() == true)

                } // if (objPuzzle != null)

                // Classification item
                Common.Models.v2.Activities objClassification = objResponse.ContentRoot.Activities.FirstOrDefault(a => a.ActivityName == "Classification");

                if (objClassification != null)
                {
                    ActivityItem[] lstClassificationItems = objClassification.ActivityItem.ToArray();
                    List<ActivityItem> lstActivityItemRemove = new List<ActivityItem>();

                    //Todo: this need to be implemented in stored procedure
                    foreach (var item in lstClassificationItems)
                    {
                        int modelCount = 0;
                        //Get all properties
                        var properties = _contentDbContext.ClassificationDetails.Include(c => c.MstProperty).Where(x => x.ClassificationId == item.ItemId && x.IsDeleted == false && x.IsEnabled == true).ToList();

                        foreach (var property in properties)
                        {
                            //Get all assets
                            var models = _contentDbContext.ClassificationAssets.Include(c => c.Asset).Where(x => x.ClassificationDetailId == property.Id && x.IsDeleted == false && x.IsEnabled == true).ToList();

                            foreach (var m in models)
                            {
                                //if (item.AssetContent.Where(x => x.AssetId == m.AssetId).ToList().Count == 0)
                                //{
                                modelCount++;
                                if (lstAsset.Any(a => a.Id == m.AssetId))
                                {
                                    item.AssetContent.Add(new ActivityContent
                                    {
                                        AssetId = m.AssetId,
                                        AssetContentId = m.AssetContentId
                                    });
                                }
                                //}
                            }
                        }

                        if (modelCount != item.AssetContent.Count || item.AssetContent.Count == 0)
                        {
                            lstActivityItemRemove.Add(item);
                        }
                    }

                    // Remove classification items with empty asset content
                    //objClassification.ActivityItem.RemoveAll(a => a.AssetContent == null || a.AssetContent.Count == 0);
                    objClassification.ActivityItem.RemoveAll(a => lstActivityItemRemove.Select(r => r.ItemId).Contains(a.ItemId));

                    //if (lstClassificationItems.Any() == true)
                    //{

                    //    //objClassification.ActivityItem.RemoveAll(i => i.IsModelSpecific == false);
                    //    objResponse.ContentRoot.Activities.Add(new Common.Models.v2.Activities
                    //    {

                    //        ActivityId = objClassification.ActivityId,
                    //        ActivityName = "Classification Engine",
                    //        ActivityItem = lstClassificationItems.ToList()

                    //    });

                    //} // if (lstClassificationItems.Any() == true)

                } // if (objClassification != null)

            }
            catch (Exception ex)
            {
                objResponse.Success = false;
                objResponse.ErrorMessage = ex.Message;

            }
            finally
            {
                if (objConnection != null)
                    objConnection.Close();

            }

            return objResponse;
        }

        [HttpGet]
        [Authorize(Roles = "Student,Teacher,Administrator")]
        public async Task<ContentAssetDetailResponse> AssetDetail(ContentAssetDetailRequest request)
        {
            // Client output
            ContentAssetDetailResponse objResponse = new ContentAssetDetailResponse();

            objResponse.Success = true;
            objResponse.ErrorMessage = string.Empty;
            SqlConnection objConnection = null;

            try
            {
                // Get user id
                string strUserName = User.FindFirst(ClaimTypes.Name).Value;

                // Get user object
                TdlUser objUser = await _userManager.FindByNameAsync(strUserName);

                if (string.IsNullOrWhiteSpace(request.Platform))
                    request.Platform = "All";

                if (request.Platform.Length > 100)
                    throw new Exception("Platform name too long.");

                // Remove "Player" and "Editor" from string. Ref https://docs.unity3d.com/ScriptReference/RuntimePlatform.html
                request.Platform = request.Platform = RewriteRequestPlatform(request.Platform);

                // All platforms
                List<CsNewPlatform> lstPlatforms = _contentDbContext.CsNewPlatform.ToList();

                // Fill dictionary. We do this conservatively in case someone names two platforms the same. Then we don't want API to crash. Therefore we don't use .ToDictionary().
                Dictionary<string, CsNewPlatform> lstAllPlatforms = new Dictionary<string, CsNewPlatform>(StringComparer.OrdinalIgnoreCase); // Also we make dictionary case insensitive

                Dictionary<Guid, CsNewPlatform> lstAllPlatformIDs = new Dictionary<Guid, CsNewPlatform>(); // Also we make dictionary case insensitive

                foreach (CsNewPlatform objPlatform in lstPlatforms)
                {

                    if (!lstAllPlatforms.ContainsKey(objPlatform.NewName))
                        lstAllPlatforms.Add(objPlatform.NewName, objPlatform);

                    if (!lstAllPlatformIDs.ContainsKey(objPlatform.Id))
                        lstAllPlatformIDs.Add(objPlatform.Id, objPlatform);

                } // foreach (CsNewPlatform objPlatform in lstPlatforms)

                // Get special "All"-platform
                lstAllPlatforms.TryGetValue("All", out CsNewPlatform objAllPlatform);

                if (objAllPlatform == null)
                    throw new Exception("Platform \"All\" does not exist in database.");

                // Get platform
                lstAllPlatforms.TryGetValue(request.Platform, out var objOutputPlatform);

                if (objOutputPlatform == null)
                {

                    objResponse.Success = false;
                    objResponse.ErrorMessage = "Invalid Platform";
                    objResponse.LocalizedError = CommonMethods.GetErrorLocal(ErrorCode.InvalidPlatform, _contentDbContext);

                    return objResponse;

                }

                // Open connection for stored procedure
                objConnection = new SqlConnection(_contentDbContext.Database.GetDbConnection().ConnectionString);

                // Used to avoid wrong execution plan being used by mssql when calling from C# clients
                SqlCommand objCommand = new SqlCommand("set arithabort on;", objConnection);

                objCommand.Connection.Open();
                objCommand.ExecuteNonQuery();

                // Indefinite timeout
                objCommand.CommandTimeout = 0;

                // Prepare a call to the stored procedure
                objCommand = new SqlCommand("dbo.spAssetDetail", objConnection);
                objCommand.CommandType = CommandType.StoredProcedure;

                // Create parameters
                objCommand.Parameters.Add(new SqlParameter("entityId", request.EntityId));
                objCommand.Parameters.Add(new SqlParameter("platform", objOutputPlatform.Id));
                objCommand.Parameters.Add(new SqlParameter("entityType", request.EntityType.ToString()));

                objCommand.CommandTimeout = 0;

                // Execute the stored procedure
                SqlDataReader objDataReader = objCommand.ExecuteReader();

                // There will be only one row, since the
                // result is rendered as JSON
                if (objDataReader.Read() == false)
                    return objResponse;

                // If there are no results, the data will be DBNull
                object objData = objDataReader.GetValue(0);

                if (objData == DBNull.Value)
                    return objResponse;

                // Parse database response
                objResponse.Assets = JsonConvert.DeserializeObject<List<Asset>>(objData.ToString());

                foreach (Asset objAsset in objResponse.Assets)
                {
                    objAsset.AssetContents.ForEach(delegate (AssetContent objAssetContent)
                    {
                        if (string.IsNullOrWhiteSpace(objAssetContent.ThumbnailHash) == false)
                            objAssetContent.ThumbnailCdnUrls.Add(new CdnUrl
                            {
                                Url = _managedConfig.Assets.ThumbnailBaseUrl.Replace("{filename}", Uri.EscapeUriString(objAssetContent.ThumbnailHash))
                            });

                        if (string.IsNullOrWhiteSpace(objAssetContent.BackgroundHash) == false)
                            objAssetContent.BackgroundCdnUrls.Add(new CdnUrl
                            {
                                Url = _managedConfig.Assets.BackgroundBaseUrl.Replace("{filename}", Uri.EscapeUriString(objAssetContent.BackgroundHash))
                            });

                        objAssetContent.FileCdnUrls.Add(new CdnUrl
                        {
                            Url = _managedConfig.Assets.FileBaseUrl.Replace("{filename}", Uri.EscapeUriString(objAssetContent.FileHash))
                        });

                        if (objAssetContent.Metadata == null)
                            objAssetContent.Metadata = new Common.Models.v2.AssetContentMetadata();
                    });

                    foreach (AssociatedContent objAssociatedAsset in objAsset.AssociatedContents)
                    {
                        if (string.IsNullOrWhiteSpace(objAssociatedAsset.ThumbnailHash) == false)
                        {
                            objAssociatedAsset.ThumbnailCdnUrls = new List<CdnUrl>();

                            objAssociatedAsset.ThumbnailCdnUrls.Add(new CdnUrl
                            {
                                Url = _managedConfig.Assets.ThumbnailBaseUrl.Replace("{filename}", Uri.EscapeUriString(objAssociatedAsset.ThumbnailHash))
                            });
                        }

                        if (string.IsNullOrWhiteSpace(objAssociatedAsset.BackgroundHash) == false)
                        {
                            objAssociatedAsset.BackgroundCdnUrls = new List<CdnUrl>();

                            objAssociatedAsset.BackgroundCdnUrls.Add(new CdnUrl
                            {
                                Url = _managedConfig.Assets.BackgroundBaseUrl.Replace("{filename}", Uri.EscapeUriString(objAssociatedAsset.BackgroundHash))
                            });
                        }

                        if (string.IsNullOrWhiteSpace(objAssociatedAsset.FileHash) == false)
                            objAssociatedAsset.FileCdnUrls.Add(new CdnUrl
                            {
                                Url = _managedConfig.Assets.FileBaseUrl.Replace("{filename}", Uri.EscapeUriString(objAssociatedAsset.FileHash))
                            });
                    }
                }
            }
            catch (Exception ex)
            {
                objResponse.Success = false;
                objResponse.ErrorMessage = ex.Message;

            }
            finally
            {
                if (objConnection != null)
                    objConnection.Close();

            }

            return objResponse;
        }

        [HttpGet]
        [Authorize(Roles = "Student,Teacher,Administrator")]
        public async Task<ContentAssetListResponse> AssetListNew(ContentAssetListRequest request)
        {
            var ret = new ContentAssetListResponse();

            try
            {
                // Get user id
                var username = User.FindFirst(ClaimTypes.Name).Value;
                // Get user object
                var user = await _userManager.FindByNameAsync(username);
                if (user == null)
                {
                    user = await _userManager.FindByEmailAsync(username);
                }
                // Get account
                var accountId = user.Account;

                if (string.IsNullOrWhiteSpace(request.Platform))
                    request.Platform = "All";

                if (request.Platform.Length > 100)
                    throw new Exception("Platform name too long.");

                request.Platform = RewriteRequestPlatform(request.Platform);

                var allPlatforms = _memoryCache.GetOrAddCache(CacheKeys.Platforms, null, () => _contentDbContext.CsNewPlatform.ToList());
                // Fill dictionary. We do this conservatively in case someone names two platforms the same. Then we don't want API to crash. Therefore we don't use .ToDictionary().
                var allPlatformsDic = new Dictionary<string, CsNewPlatform>(StringComparer.OrdinalIgnoreCase); // Also we make dictionary case insensitive
                var allPlatformsGuidDic = new Dictionary<Guid, CsNewPlatform>(); // Also we make dictionary case insensitive
                foreach (var ap in allPlatforms)
                {
                    if (!allPlatformsDic.ContainsKey(ap.NewName))
                        allPlatformsDic.Add(ap.NewName, ap);
                    if (!allPlatformsGuidDic.ContainsKey(ap.Id))
                        allPlatformsGuidDic.Add(ap.Id, ap);
                }

                // Get special "All"-platform
                allPlatformsDic.TryGetValue("All", out var allPlatform);
                if (allPlatform == null)
                    throw new Exception("Platform \"All\" does not exist in database.");

                // Get platform
                allPlatformsDic.TryGetValue(request.Platform, out var platform);

                if (platform == null)
                {
                    ret.Success = false;
                    ret.ErrorMessage = "Invalid Platform";
                    ret.LocalizedError = CommonMethods.GetErrorLocal(ErrorCode.InvalidPlatform, _contentDbContext);
                    return ret;
                }

                if (request.ModulePlatform != null)
                {
                    request.ModulePlatform = RewriteRequestPlatform(request.ModulePlatform);
                }
                else
                {
                    request.ModulePlatform = request.Platform;
                }
                allPlatformsDic.TryGetValue(request.ModulePlatform, out var modulePlatform);

                //var cacheKey = "Account:" + accountId.ToString();
                //if (!_memoryCache.TryGetValue(cacheKey, out ret.ContentRoot))
                //{

                //    // Set cache options.
                //    var cacheEntryOptions = new MemoryCacheEntryOptions()
                //        // Keep in cache for this time, reset time if accessed.
                //        .SetSlidingExpiration(TimeSpan.FromHours(3));

                var adminAccountId = _managedConfig?.Account?.AccountId;
                var isAdmin = adminAccountId == accountId;
                var opportunities = _contentDbContext.CsOpportunity
                     .Where(o => isAdmin || o.Parentaccountid == accountId)
                     // Include main hierarchy
                     .Include(o => o.CsNewOpportunitySubscriptionMappings).ThenInclude(os => os.NewSubscription)
                     .ThenInclude(s => s.CsNewSubscriptionPackages).ThenInclude(sp => sp.NewPackage)
                     .ThenInclude(p => p.CsNewPackageGrades).ThenInclude(pg => pg.NewGrade)
                     .ThenInclude(g => g.CsNewGradeSubjects).ThenInclude(gs => gs.NewSubject)
                     .ThenInclude(s => s.CsNewSubjectTopics).ThenInclude(st => st.NewTopic)
                     .ThenInclude(t => t.CsNewTopicSubtopics).ThenInclude(ts => ts.NewSubtopic)

                     // Include Streams under Grade
                     .Include(o => o.CsNewOpportunitySubscriptionMappings).ThenInclude(os => os.NewSubscription)
                     .ThenInclude(s => s.CsNewSubscriptionPackages).ThenInclude(sp => sp.NewPackage)
                     .ThenInclude(p => p.CsNewPackageGrades).ThenInclude(pg => pg.NewGrade)
                     .ThenInclude(g => g.CsNewGradeStreams).ThenInclude(gs => gs.NewStream)


                    //// Include Assets and AssetContents for
                    //// Subtopic
                    //.Include(o => o.CsNewOpportunitySubscriptionMappings).ThenInclude(os => os.NewSubscription)
                    //.ThenInclude(s => s.CsNewSubscriptionPackages).ThenInclude(sp => sp.NewPackage)
                    //.ThenInclude(p => p.CsNewPackageGrades).ThenInclude(pg => pg.NewGrade)
                    //.ThenInclude(g => g.CsNewGradeSubjects).ThenInclude(gs => gs.NewSubject)
                    //.ThenInclude(s => s.CsNewSubjectTopics).ThenInclude(st => st.NewTopic)
                    //.ThenInclude(t => t.CsNewTopicSubtopics).ThenInclude(ts => ts.NewSubtopic)
                    //.ThenInclude(st => st.Assets).ThenInclude(st => st.AssetContents)

                    //// Topic
                    //.Include(o => o.CsNewOpportunitySubscriptionMappings).ThenInclude(os => os.NewSubscription)
                    //.ThenInclude(s => s.CsNewSubscriptionPackages).ThenInclude(sp => sp.NewPackage)
                    //.ThenInclude(p => p.CsNewPackageGrades).ThenInclude(pg => pg.NewGrade)
                    //.ThenInclude(g => g.CsNewGradeSubjects).ThenInclude(gs => gs.NewSubject)
                    //.ThenInclude(s => s.CsNewSubjectTopics).ThenInclude(st => st.NewTopic)
                    //.ThenInclude(st => st.Assets).ThenInclude(st => st.AssetContents)

                    //// Subject
                    //.Include(o => o.CsNewOpportunitySubscriptionMappings).ThenInclude(os => os.NewSubscription)
                    //.ThenInclude(s => s.CsNewSubscriptionPackages).ThenInclude(sp => sp.NewPackage)
                    //.ThenInclude(p => p.CsNewPackageGrades).ThenInclude(pg => pg.NewGrade)
                    //.ThenInclude(g => g.CsNewGradeSubjects).ThenInclude(gs => gs.NewSubject)
                    //.ThenInclude(st => st.Assets).ThenInclude(st => st.AssetContents)

                    .ToList();

                // Remove all that user does not belong to me
                // Ankit: if no subscription/package mapped to user explicitly
                var userDetails = _contentDbContext.UserDetails.Where(u => u.UserId == user.Id && u.IsDeleted == false).ToArray();

                if (!isAdmin && userDetails.Length > 0)
                {
                    //var userDetails = _contentDbContext.UserDetails.Where(u => u.UserId == user.Id).ToArray();
                    foreach (var opportunity in opportunities.ToArray())
                    {

                        foreach (var subscriptionMapping in opportunity.CsNewOpportunitySubscriptionMappings.ToArray())
                        {
                            var subscription = subscriptionMapping.NewSubscription;
                            var userSubscription = userDetails.Where(u => u.SubscriptionId == subscription.Id && u.IsDeleted == false).ToArray();
                            if (userDetails.Length > 0 && userSubscription.Length == 0) // We have userDetails specified, but this subscription is not one of them, so remove it
                            {
                                opportunity.CsNewOpportunitySubscriptionMappings.Remove(subscriptionMapping);
                                continue;
                            }

                            foreach (var subscriptionPackage in subscription.CsNewSubscriptionPackages.ToArray())
                            {
                                var package = subscriptionPackage.NewPackage;
                                foreach (var packageGrade in package.CsNewPackageGrades.ToArray())
                                {
                                    var grade = packageGrade.NewGrade;
                                    var userGrades = userSubscription.Where(u => u.GradeId == grade.Id || u.GradeId == Guid.Empty).ToArray();
                                    if (userGrades.Length == 0)
                                    {
                                        package.CsNewPackageGrades.Remove(packageGrade);
                                        continue;
                                    }

                                    foreach (var gradeSubject in grade.CsNewGradeSubjects.ToArray())
                                    {
                                        var subject = gradeSubject.NewSubject;
                                        var userSubjects = userGrades.Where(u => u.SubjectId == grade.Id || u.SubjectId == Guid.Empty).ToArray();
                                        if (userSubjects.Length == 0)
                                        {
                                            grade.CsNewGradeSubjects.Remove(gradeSubject);
                                            continue;
                                        }


                                        foreach (var subjectTopic in subject.CsNewSubjectTopics.ToArray())
                                        {
                                            var topic = subjectTopic.NewTopic;
                                            var userTopics = userSubjects.Where(u => u.TopicId == topic.Id || u.TopicId == Guid.Empty).ToArray();
                                            if (userTopics.Length == 0)
                                            {
                                                subject.CsNewSubjectTopics.Remove(subjectTopic);
                                                continue;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    // TODO: We also need to implement for streams etc...
                }

                // Concept of "Package" is not required, so we will flatten that structure
                var packages = opportunities.SelectMany(s => s.CsNewOpportunitySubscriptionMappings)
                    .SelectMany(s => s.NewSubscription.CsNewSubscriptionPackages)
                    .GroupBy(p => p.NewPackageid).ToList();

                var packagesList = new List<Guid>();
                foreach (var package in packages)
                {
                    packagesList.Add(package.Key ?? Guid.Empty);
                }

                // Note: We are making an assumption that grade instances are reused between packages
                var grades = opportunities
                    .SelectMany(s => s.CsNewOpportunitySubscriptionMappings)
                    .SelectMany(s => s.NewSubscription.CsNewSubscriptionPackages)
                    .SelectMany(p => p.NewPackage.CsNewPackageGrades)
                    .GroupBy(g => g.NewGradeid)
                    .SelectMany(g => g.ToList())
                    .ToList();

                // Grade ID's
                //var gradeIds = grades.Select(g => g.NewGradeid).ToList();

                ret.ContentRoot = new ContentRoot();
                ret.ContentRoot.Grades = _mapper.Map<List<Grade>>(grades);

                //Get all compounds
                var compounds = _contentDbContext.MstCompound
                    .Include(c => c.MstMaterial)
                    .Include(c => c.BondedAssets)
                    .Include(c => c.SphericalAssets)
                    .Include(c => c.CompoundElement)
                    .Where(c => c.IsDeleted == false)
                    .ToList();

                ret.ContentRoot.Compounds = _mapper.Map<List<Common.Models.v2.Compound>>(compounds);
                //foreach (var c in ret.ContentRoot.Compounds)
                //{
                //    var elements = _contentDbContext.CompoundElement.Where(x => x.CompoundId == c.CompoundId && x.IsDeleted == false).Include(x => x.MstElement).ToList();
                //    c.Elements = _mapper.Map<List<Common.Models.v2.Element>>(elements);
                //}

                var elements = _contentDbContext.MstElement.Where(x => x.IsDeleted == false).ToList();
                ret.ContentRoot.Elements = _mapper.Map<List<Common.Models.v2.Element>>(elements);

                //Get all activities
                var activities = _contentDbContext.MstActivity.Where(x => x.IsActive == true).ToList();
                activities.Add(new MstActivity { ActivityId = 1, ActivityName = "Multiple Quiz" });
                activities.Add(new MstActivity { ActivityId = 2, ActivityName = "Multiple Puzzle" });
                ret.ContentRoot.Activities = _mapper.Map<List<Common.Models.v2.Activities>>(activities);
                var strPlatformId = Convert.ToString(platform.Id);

                foreach (var act in ret.ContentRoot.Activities)
                {
                    var items = new List<Database.Tables.Activities>();

                    if (act.ActivityName == "Quiz" || act.ActivityName == "Puzzle")
                    {
                        items = _contentDbContext.Activities
                                    .Include(x => x.Names).ThenInclude(x => x.Language)
                                    .Include(x => x.Platforms)
                                    //.Include(x => x.Details)
                                    .Where(x => x.ActivityType == act.ActivityId && x.IsDeleted == false && x.IsModelSpecific == true && x.Platforms.Count > 0)
                                    .ToList();
                    }
                    else if (act.ActivityName == "Multiple Quiz" || act.ActivityName == "Multiple Puzzle")
                    {
                        items = _contentDbContext.Activities
                                    .Include(x => x.Names).ThenInclude(x => x.Language)
                                    .Include(x => x.Platforms)
                                    .Where(x => x.ActivityType == act.ActivityId && x.IsDeleted == false && x.IsModelSpecific == false && x.Platforms.Count > 0)
                                    .ToList();
                    }
                    else if (act.ActivityName == "Classification")
                    {
                        items = _contentDbContext.Activities
                                .Include(x => x.Names).ThenInclude(x => x.Language)
                                .Include(x => x.Platforms)
                                .Where(x => x.ActivityType == act.ActivityId && x.IsDeleted == false && x.Platforms.Count > 0)
                                .ToList();
                    }

                    // Filter all inactive activities
                    foreach (var activity in items)
                    {
                        // Remove inactive AssetContent
                        foreach (var ap in activity.Platforms.ToArray())
                        {
                            allPlatformsGuidDic.TryGetValue(ap.PlatformId, out var contentPlatform);
                            if (contentPlatform != null &&
                                platform.Id != allPlatform.Id && // We are not targeting "All"-platform
                                contentPlatform.Id != allPlatform.Id && // Content is not "All"-platform
                                contentPlatform.Id != platform.Id) // Content platform is not requested platform
                            {
                                activity.Platforms.Remove(ap);
                                continue;
                            }
                        }
                    }

                    var newItems = items.Where(x => x.Platforms.Count > 0).ToList();

                    act.ActivityItem = _mapper.Map<List<Common.Models.v2.ActivityItem>>(newItems);

                    foreach (var item in act.ActivityItem)
                    {
                        var activityDetails = _contentDbContext.ActivityDetails.Where(x => x.ActivityId == item.ItemId && x.IsDeleted == false).ToList();
                        item.ActivityDetails = _mapper.Map<List<Common.Models.v2.ActivityDetail>>(activityDetails);

                        if (act.ActivityName == "Quiz" || act.ActivityName == "Multiple Quiz")
                        {
                            item.AssetContent = _memoryCache.GetOrAddCache(CacheKeys.QuizDetails, $"{item.ItemId}", () =>
                                 (from qd in _contentDbContext.QuizDetails
                                  where qd.QuizId == item.ItemId && qd.IsEnabled == true
                                  select new ActivityContent()
                                  {
                                      ItemDetailId = qd.Id,
                                      AssetId = qd.AssetId,
                                      AssetContentId = qd.AssetContentId,
                                      NumberOfAttempts = qd.NumberOfAttempts,
                                      NumberOfLabels = qd.NumberOfLabels
                                  }).ToList());

                            foreach (var label in item.AssetContent)
                            {
                                item.IsVisible = (item.IsVisible == true && label.NumberOfAttempts > 0) ? true : false;

                                var model = _memoryCache.GetOrAddCache(CacheKeys.ContentModel, $"{label.AssetId}:{strPlatformId}", () =>
                                    (from ac in _contentDbContext.AssetContent
                                     join m in _contentDbContext.XmlcontentModel on ac.Id equals m.AssetContentId
                                     where ac.AssetId == label.AssetId && ac.Platform == strPlatformId && ac.IsEnabled == true
                                     select m).OrderByDescending(x => x.ModelId).FirstOrDefault());

                                if (model != null)
                                {
                                    label.AssetContentId = model.AssetContentId ?? label.AssetContentId;

                                    label.QuizLabels = _memoryCache.GetOrAddCache(CacheKeys.QuizLabelDetailsContentLabel, $"{model.ModelId}:{label.ItemDetailId}", () =>
                                                       (from l in _contentDbContext.QuizLabelDetails
                                                        join xl in _contentDbContext.XmlcontentLabel on l.LabelName equals xl.En
                                                        where xl.ModelId == model.ModelId && l.QuizDetailId == label.ItemDetailId && xl.IsEnabled == true && l.IsEnabled == true
                                                        select new QuizLabels()
                                                        {
                                                            LabelId = xl.LabelId,
                                                            LabelName = l.LabelName
                                                        }).ToList());
                                }
                                else
                                {
                                    label.QuizLabels = _memoryCache.GetOrAddCache(CacheKeys.QuizLabelDetails, $"{label.ItemDetailId}", () =>
                                                       (from l in _contentDbContext.QuizLabelDetails
                                                        where l.QuizDetailId == label.ItemDetailId && l.IsEnabled == true
                                                        select new QuizLabels()
                                                        {
                                                            LabelId = l.LabelId,
                                                            LabelName = l.LabelName
                                                        }).ToList());
                                }
                            }
                        }
                        else if (act.ActivityName == "Puzzle" || act.ActivityName == "Multiple Puzzle")
                        {
                            item.AssetContent = _memoryCache.GetOrAddCache(CacheKeys.PuzzleDetails, $"{item.ItemId}", () =>
                                                (from pd in _contentDbContext.PuzzleDetails
                                                 where pd.PuzzleId == item.ItemId && pd.IsEnabled == true
                                                 select new ActivityContent()
                                                 {
                                                     ItemDetailId = pd.Id,
                                                     AssetId = pd.AssetId,
                                                     AssetContentId = pd.AssetContentId,
                                                     NumberOfAttempts = 0,
                                                     NumberOfLabels = pd.NumberOfParts
                                                 }).ToList());
                        }
                    }
                }

                // Get all relevant assets
                var assets = _contentDbContext.AssetDetails
                    .Include(a => a.Language)

                    .Include(a => a.Asset.AssetContent)
                    .ThenInclude(ac => ac.AssetContentDetail)
                    .ThenInclude(acd => acd.Language)

                    .Include(a => a.Asset.AssetContent)
                    .ThenInclude(ac => ac.XmlcontentModel)
                    .ThenInclude(ac => ac.XmlcontentLabel)

                    .Include(a => a.Asset.AssetContent)
                    .ThenInclude(a => a.Metadata)


                    .Include(a => a.Asset.AssetNames)
                    .ThenInclude(a => a.Language)

                    //.Include(a => a.Asset.AssetContent)
                    //.ThenInclude(ac => ac.QuizDetails)

                    //.Include(a => a.Asset.AssetContent)
                    //.ThenInclude(ac => ac.PuzzleDetails)
                    //var assets = _contentDbContext.Asset
                    //    .Include(a => a.AssetContent)
                    //    .ThenInclude(ac => ac.XmlcontentModel)
                    //    .ThenInclude(ac => ac.XmlcontentLabel)

                    //    .Include(a => a.AssetDetails)

                    //// Filter only on grades first
                    //.Join(_contentDbContext.AssetDetails, a => a.AssetId, ad => ad.AssetId, (a, ad) => new { a, ad })
                    //.Join(_contentDbContext.AssetContent, a => a.a.AssetId, ac => ac.AssetId, (aad, ac) => new { aad.a, aad.ad, ac })
                    //.Where(aadac => gradeIds.Contains(aadac.ad.GradeId)
                    //                && aadac.a.IsDeleted == false && aadac.ac.IsEnabled == true && aadac.a.Status == "Production"
                    //)
                    .Where(a => a.IsDeleted == false && a.Asset.IsDeleted == false && a.Asset.IsActive == true && a.Asset.Status == "Production" && packagesList.Contains(a.PackageId))
                    .ToList();


                // Filter all inactive AssetContent
                var assetsDic = assets.ToDictionary(a => a);
                foreach (var asset in assets)
                {
                    // Remove inactive AssetContent
                    foreach (var content in asset.Asset.AssetContent.ToArray())
                    {
                        var contentPlatformGuid = Guid.Parse(content.Platform);
                        allPlatformsGuidDic.TryGetValue(contentPlatformGuid, out var contentPlatform);
                        if (contentPlatform != null &&
                           modulePlatform.Id != allPlatform.Id && // We are not targeting "All"-platform
                           platform.Id != modulePlatform.Id && asset.Asset.Type == "Module" && //If value for module platform is different from content platform
                           contentPlatform.Id != allPlatform.Id && // Content is not "All"-platform
                           contentPlatform.Id != modulePlatform.Id) // Module platform is not requested platform
                        {
                            asset.Asset.AssetContent.Remove(content);
                            continue;
                        }
                        else if (contentPlatform != null &&
                           platform.Id != allPlatform.Id && // We are not targeting "All"-platform
                           platform.Id != modulePlatform.Id && asset.Asset.Type != "Module" && //If value for module platform is different from content platform
                           contentPlatform.Id != allPlatform.Id && // Content is not "All"-platform
                           contentPlatform.Id != platform.Id) // Module platform is not requested platform
                        {
                            asset.Asset.AssetContent.Remove(content);
                            continue;
                        }
                        else if (contentPlatform != null &&
                            platform.Id != allPlatform.Id && // We are not targeting "All"-platform
                            platform.Id == modulePlatform.Id && //If value for module platform is null
                            contentPlatform.Id != allPlatform.Id && // Content is not "All"-platform
                            contentPlatform.Id != platform.Id) // Content platform is not requested platform
                        {
                            asset.Asset.AssetContent.Remove(content);
                            continue;
                        }

                        if (content.IsDeleted == true || content.IsEnabled == false)
                        {
                            asset.Asset.AssetContent.Remove(content);
                            continue;
                        }

                        // Remove inactive AssetContentDetails
                        foreach (var acd in content.AssetContentDetail.ToArray())
                        {
                            if (acd.IsDeleted == true && acd.IsEnabled == true)
                                content.AssetContentDetail.Remove(acd);
                        }
                    }
                    // If Asset doesn't contain any AssetContent then remove it from dictionary
                    if (asset.Asset.AssetContent.Count == 0 || asset.Asset.AssetDetails.Count == 0)
                        assetsDic.Remove(asset);
                }
                // Back from Dictionary
                assets = assetsDic.Keys.ToList();

                // Assign assets to each level
                // TODO: This is some heavy iteration. It works for now, and with caching it should be fine...
                foreach (var grade in ret.ContentRoot.Grades)
                {
                    grade.Assets.AddRange(_mapper.Map<Asset[]>(assets
                        .Where(a => a.GradeId == grade.Id && a.SubjectId == Guid.Empty)
                        .Select(a => a.Asset)));

                    grade.ThumbnailHash = _memoryCache.GetOrAddCache(CacheKeys.EntityThumbnail, grade.Id.ToString(), () =>
                                          (from et in _contentDbContext.EntityThumbnail // TODO: We can't be visiting database for every sub-level -- 20181104 Tedd: Added caching
                                           where et.EntityId == grade.Id
                                           select et.Thumbnail).FirstOrDefault());

                    if (grade.ThumbnailHash != null)
                        grade.ThumbnailCdnUrls.Add(new CdnUrl
                        {
                            Url = _managedConfig.Assets.ThumbnailBaseUrl.Replace("{filename}", Uri.EscapeUriString(grade.ThumbnailHash))
                        });

                    foreach (var subject in grade.Subjects)
                    {
                        subject.Assets.AddRange(_mapper.Map<Asset[]>(assets
                            .Where(a => a.GradeId == grade.Id && a.SubjectId == subject.Id && a.TopicId == Guid.Empty)
                            .Select(a => a.Asset)));

                        subject.ThumbnailHash = _memoryCache.GetOrAddCache(CacheKeys.EntityThumbnail, subject.Id.ToString(), () =>
                                                   (from et in _contentDbContext.EntityThumbnail // TODO: We can't be visiting database for every sub-level -- 20181104 Tedd: Added caching
                                                    where et.EntityId == subject.Id
                                                    select et.Thumbnail).FirstOrDefault());

                        if (subject.ThumbnailHash != null)
                            subject.ThumbnailCdnUrls.Add(new CdnUrl
                            {
                                Url = _managedConfig.Assets.ThumbnailBaseUrl.Replace("{filename}", Uri.EscapeUriString(subject.ThumbnailHash))
                            });

                        foreach (var topic in subject.Topics)
                        {
                            topic.Assets.AddRange(_mapper.Map<Asset[]>(assets
                                .Where(a => a.GradeId == grade.Id && a.SubjectId == subject.Id && a.TopicId == topic.Id && a.SubtopicId == Guid.Empty)
                                .Select(a => a.Asset)));

                            topic.ThumbnailHash = _memoryCache.GetOrAddCache(CacheKeys.EntityThumbnail, topic.Id.ToString(), () =>
                                                    (from et in _contentDbContext.EntityThumbnail // TODO: We can't be visiting database for every sub-level -- 20181104 Tedd: Added caching
                                                     where et.EntityId == topic.Id
                                                     select et.Thumbnail).FirstOrDefault());
                            if (topic.ThumbnailHash != null)
                                topic.ThumbnailCdnUrls.Add(new CdnUrl
                                {
                                    Url = _managedConfig.Assets.ThumbnailBaseUrl.Replace("{filename}", Uri.EscapeUriString(topic.ThumbnailHash))
                                });

                            foreach (var subtopic in topic.Subtopics)
                            {
                                subtopic.Assets.AddRange(_mapper.Map<Asset[]>(assets
                                    .Where(a => a.GradeId == grade.Id && a.SubjectId == subject.Id && a.TopicId == topic.Id && a.SubtopicId == subtopic.Id)
                                    .Select(a => a.Asset)));

                                subtopic.ThumbnailHash = _memoryCache.GetOrAddCache(CacheKeys.EntityThumbnail, subtopic.Id.ToString(), () =>
                                                           (from et in _contentDbContext.EntityThumbnail // TODO: We can't be visiting database for every sub-level -- 20181104 Tedd: Added caching
                                                            where et.EntityId == subtopic.Id
                                                            select et.Thumbnail).FirstOrDefault());

                                if (subtopic.ThumbnailHash != null)
                                    subtopic.ThumbnailCdnUrls.Add(new CdnUrl
                                    {
                                        Url = _managedConfig.Assets.ThumbnailBaseUrl.Replace("{filename}", Uri.EscapeUriString(subtopic.ThumbnailHash))
                                    });
                            }
                        }
                    }
                }

                // Get languages we are using                
                //var languagesDb = assets.SelectMany(a => a.a.AssetLanguages).GroupBy(al => al.LanguageId).Select(g => g.Key).ToList();
                var allAssets = new List<Common.Models.v2.Asset>(512);
                allAssets.AddRange(ret.ContentRoot.Grades.SelectMany(a => a.Assets));
                allAssets.AddRange(ret.ContentRoot.Grades.SelectMany(g => g.Subjects).SelectMany(a => a.Assets));
                allAssets.AddRange(ret.ContentRoot.Grades.SelectMany(g => g.Subjects).SelectMany(s => s.Topics).SelectMany(a => a.Assets));
                allAssets.AddRange(ret.ContentRoot.Grades.SelectMany(g => g.Subjects).SelectMany(s => s.Topics).SelectMany(t => t.Subtopics).SelectMany(a => a.Assets));

                //To fix the issue: getting previous version asset content details for associated content
                var lstAsset = (from a in _contentDbContext.Asset
                                join ac in _contentDbContext.AssetContent on a.AssetId equals ac.AssetId
                                where a.IsDeleted == false && ac.IsEnabled == true
                                select new { a.AssetId, ac.Id }).ToList();

                lstAsset = (from l in lstAsset
                            group l by l.AssetId into g
                            select new
                            {
                                AssetId = g.Max(s => s.AssetId),
                                Id = g.Max(s => s.Id)
                            }).ToList();

                //Get platform value
                foreach (var asset in allAssets)
                {
                    foreach (var content in asset.AssetContents)
                    {
                        if (allPlatformsGuidDic.TryGetValue(Guid.Parse(content.PlatformId), out var p))
                            content.Platform = p.NewName;
                    }

                    //Get all the associated models
                    asset.AssociatedContents = await (from r in _contentDbContext.AssetRelations
                                                      join a in _contentDbContext.Asset on r.LinkedAssetId equals a.AssetId
                                                      join al in lstAsset on a.AssetId equals al.AssetId
                                                      join ac in _contentDbContext.AssetContent on al.Id equals ac.Id
                                                      where r.AssetId == asset.Id && r.IsSelected == true
                                                      select new AssociatedContent()
                                                      {
                                                          Id = r.LinkedContentId,
                                                          ModelId = a.AssetId,
                                                          Name = a.AssetName,
                                                          Type = a.Type,
                                                          PlatformId = ac.Platform,
                                                          Version = ac.VersionNumber,
                                                          FileHash = ac.FileName,
                                                          FileSize = ac.FileSize,
                                                          FileCdnUrls = new List<CdnUrl>() { new CdnUrl() { Url = ac.FileName } },
                                                          ThumbnailHash = ac.Thumbnail,
                                                          ThumbnailCdnUrls = new List<CdnUrl>() { new CdnUrl() { Url = ac.Thumbnail } },
                                                          BackgroundHash = ac.Background,
                                                          BackgroundCdnUrls = new List<CdnUrl>() { new CdnUrl() { Url = ac.Background } },
                                                          Languages = _mapper.Map<List<Common.Models.v2.Language>>(_contentDbContext.AssetDetails.Where(ad => ad.AssetId == r.LinkedAssetId && ad.IsDeleted == false).Select(ad => ad.Language).Distinct().ToList())
                                                      }).ToListAsync();

                    asset.Languages.AddRange(_mapper.Map<List<Common.Models.v2.Language>>(_contentDbContext.AssetDetails.Where(ad => ad.AssetId == asset.Id && ad.IsDeleted == false).Select(ad => ad.Language).Distinct().ToList()));
                }

                FixAssetUrls(allAssets);

                var allLanguages = new List<string>(1024);
                allLanguages.AddRange(allAssets.SelectMany(a => a.LocalizedName.Keys));
                allLanguages.AddRange(allAssets.SelectMany(a => a.AssetContents)
                    .Where(a => a.Metadata?.Model3DLabels?.Items != null).SelectMany(a => a.Metadata?.Model3DLabels?.Items)
                    .Where(i => i.LocalizedText != null).SelectMany(i => i.LocalizedText?.Keys));

                // TODO: This is heavy, easier to just pull all languages into a dictionary, lookup and add?
                allLanguages = allLanguages.GroupBy(g => g).SelectMany(g => g.ToList()).ToList();


                ret.ContentRoot.Languages = _mapper.Map<List<Common.Models.v2.Language>>(_contentDbContext.CsNewLanguage.Where(l => allLanguages.Contains(l.NewLanguageshortname) && (l.NewLanguageshortname.Equals("en-US") || l.NewLanguageshortname.Equals("nb-NO") || l.NewLanguageshortname.Equals("nn-NO"))));
                //ret.ContentRoot.Languages = _mapper.Map<List<Common.Models.v2.Language>>(_contentDbContext.CsNewLanguage.Where(l => allLanguages.Contains(l.NewLanguageshortname)));

                // Do some cleanup: We only return levels that have actual content
                if (!request.IncludeEmptyLevels)
                {
                    foreach (var grade in ret.ContentRoot.Grades.ToArray())
                    {
                        foreach (var subject in grade.Subjects.ToArray())
                        {
                            foreach (var topic in subject.Topics.ToArray())
                            {
                                foreach (var subtopic in topic.Subtopics.ToArray())
                                {
                                    if (subtopic.Assets.Count == 0)
                                        topic.Subtopics.Remove(subtopic);
                                }
                                if (topic.Assets.Count == 0 && topic.Subtopics.Count == 0)
                                    subject.Topics.Remove(topic);
                            }
                            if (subject.Assets.Count == 0 && subject.Topics.Count == 0)
                                grade.Subjects.Remove(subject);
                        }
                        if (grade.Assets.Count == 0 && grade.Subjects.Count == 0)
                            ret.ContentRoot.Grades.Remove(grade);
                    }
                }

                // Count number of assetcontent types in each sublevel
                // We do this as a conveniece since apps will be presenting content like folders with numbers on content

                foreach (var grade in ret.ContentRoot.Grades)
                {
                    foreach (var subject in grade.Subjects)
                    {
                        foreach (var topic in subject.Topics)
                        {
                            foreach (var subtopic in topic.Subtopics)
                            {
                                subtopic.RecursiveTypeCount = TypeCount(subtopic.Assets);
                                AddTypeCount(topic.RecursiveTypeCount, subtopic.RecursiveTypeCount);
                            }
                            if (topic.Assets.Count > 0)
                            {
                                topic.RecursiveTypeCount = TypeCount(topic.Assets);
                            }
                            AddTypeCount(subject.RecursiveTypeCount, topic.RecursiveTypeCount);
                        }
                        if (subject.Assets.Count > 0)
                        {
                            subject.RecursiveTypeCount = TypeCount(subject.Assets);
                        }
                        AddTypeCount(grade.RecursiveTypeCount, subject.RecursiveTypeCount);
                    }
                    if (grade.Assets.Count > 0)
                    {
                        grade.RecursiveTypeCount = TypeCount(grade.Assets);
                    }
                }

                // TODO: Currently a hack, should be fixed by allowing user to specify image for each level
                // We will select first thumbnail for each level
                foreach (var grade in ret.ContentRoot.Grades)
                {
                    foreach (var subject in grade.Subjects)
                    {
                        foreach (var topic in subject.Topics)
                        {
                            foreach (var subtopic in topic.Subtopics)
                            {
                                if (subtopic.ThumbnailCdnUrls == null || subtopic.ThumbnailCdnUrls.Count == 0)
                                {
                                    subtopic.ThumbnailHash = subtopic?.Assets?.FirstOrDefault()?.AssetContents?.FirstOrDefault()?.ThumbnailHash;
                                    //subtopic.ThumbnailUrl = subtopic?.Assets?.FirstOrDefault()?.AssetContents?.FirstOrDefault()?.ThumbnailUrl;
                                    subtopic.ThumbnailCdnUrls = subtopic?.Assets?.FirstOrDefault()?.AssetContents?.FirstOrDefault()?.ThumbnailCdnUrls;
                                }
                            }
                            if (topic.ThumbnailCdnUrls == null || topic.ThumbnailCdnUrls.Count == 0)
                            {
                                topic.ThumbnailHash = topic?.Assets?.FirstOrDefault()?.AssetContents?.FirstOrDefault()?.ThumbnailHash;
                                //topic.ThumbnailUrl = topic?.Assets?.FirstOrDefault()?.AssetContents?.FirstOrDefault()?.ThumbnailUrl;
                                topic.ThumbnailCdnUrls = topic?.Assets?.FirstOrDefault()?.AssetContents?.FirstOrDefault()?.ThumbnailCdnUrls;

                                if (topic.ThumbnailCdnUrls == null || topic.ThumbnailCdnUrls.Count == 0)
                                {
                                    topic.ThumbnailHash = topic?.Subtopics?.FirstOrDefault()?.ThumbnailHash;
                                    //topic.ThumbnailUrl = topic?.Subtopics?.FirstOrDefault()?.ThumbnailUrl;
                                    topic.ThumbnailCdnUrls = topic.Subtopics?.FirstOrDefault()?.ThumbnailCdnUrls;
                                }
                            }
                        }
                        if (subject.ThumbnailCdnUrls == null || subject.ThumbnailCdnUrls.Count == 0)
                        {
                            subject.ThumbnailHash = subject?.Assets?.FirstOrDefault()?.AssetContents?.FirstOrDefault()?.ThumbnailHash;
                            //subject.ThumbnailUrl = subject?.Assets?.FirstOrDefault()?.AssetContents?.FirstOrDefault()?.ThumbnailUrl;
                            subject.ThumbnailCdnUrls = subject?.Assets?.FirstOrDefault()?.AssetContents?.FirstOrDefault()?.ThumbnailCdnUrls;

                            if (subject.ThumbnailCdnUrls == null || subject.ThumbnailCdnUrls.Count == 0)
                            {
                                subject.ThumbnailHash = subject?.Topics?.FirstOrDefault()?.ThumbnailHash;
                                //subject.ThumbnailUrl = subject?.Topics?.FirstOrDefault()?.ThumbnailUrl;
                                subject.ThumbnailCdnUrls = subject.Topics?.FirstOrDefault()?.ThumbnailCdnUrls;
                            }
                        }
                    }
                    if (grade.ThumbnailCdnUrls == null || grade.ThumbnailCdnUrls.Count == 0)
                    {
                        grade.ThumbnailHash = grade?.Assets?.FirstOrDefault()?.AssetContents?.FirstOrDefault()?.ThumbnailHash;
                        //grade.ThumbnailUrl = grade?.Assets?.FirstOrDefault()?.AssetContents?.FirstOrDefault()?.ThumbnailUrl;
                        grade.ThumbnailCdnUrls = grade?.Assets?.FirstOrDefault()?.AssetContents?.FirstOrDefault()?.ThumbnailCdnUrls;

                        if (grade.ThumbnailCdnUrls == null || grade.ThumbnailCdnUrls.Count == 0)
                        {
                            grade.ThumbnailHash = grade?.Subjects?.FirstOrDefault()?.ThumbnailHash;
                            //grade.ThumbnailUrl = grade?.Subjects?.FirstOrDefault()?.ThumbnailUrl;
                            grade.ThumbnailCdnUrls = grade.Subjects?.FirstOrDefault()?.ThumbnailCdnUrls;
                        }
                    }
                }

                //    // Save data in cache.
                //    _memoryCache.Set(cacheKey, ret.ContentRoot, cacheEntryOptions);
                //}
            }
            catch (Exception exception)
            {
                _logger.LogError(exception, "Processing AssetList request");
                ret.Success = false;
                ret.ErrorMessage = exception.Message;
                return ret;
            }

            ret.Success = true;
            return ret;
        }

        private string RewriteRequestPlatform(string requestPlatform)
        {
            // Remove "Player" and "Editor" from string. Ref https://docs.unity3d.com/ScriptReference/RuntimePlatform.html
            //requestPlatform = _platformRewriteRegex.Replace(requestPlatform, "");
            requestPlatform = _platformArchitectureRewriteRegex.Replace(requestPlatform, "");
            requestPlatform = requestPlatform.Replace("Player", "");
            requestPlatform = requestPlatform.Replace("Editor", "");
            requestPlatform = requestPlatform.Replace("Metro", "WSA");
            return requestPlatform;
        }

        private void AddTypeCount(Dictionary<string, int> dst, Dictionary<string, int> src)
        {
            foreach (var kvp in src)
            {
                //if (dst==null)
                //    dst=new Dictionary<string, int>();
                if (dst.TryGetValue(kvp.Key, out var c))
                    dst[kvp.Key] = c + kvp.Value;
                else
                    dst.Add(kvp.Key, kvp.Value);
            }
        }

        private Dictionary<string, int> TypeCount(List<Asset> assets)
        {
            var ret = new Dictionary<string, int>();
            foreach (var type in assets.Select(a => a.Type))
            {
                var c = 0;
                if (!ret.TryGetValue(type, out c))
                    ret.Add(type, 1);
                else
                    ret[type] = ++c;
            }

            return ret;
        }

        private void FixAssetUrls(List<Asset> allAssets)
        {
            // We calculate URL's at this point. For now we do this in source table data, crappy way, but works... :P
            // Should have been done after automapper.
            foreach (var asset in allAssets)
            {
                foreach (var content in asset.AssetContents)
                {
                    foreach (var urlhash in content.ThumbnailCdnUrls)
                    {
                        if (urlhash.Url != null)
                            urlhash.Url = _managedConfig.Assets.ThumbnailBaseUrl.Replace("{filename}", Uri.EscapeUriString(urlhash.Url));
                    }

                    foreach (var urlhash in content.BackgroundCdnUrls)
                    {
                        if (urlhash.Url != null)
                            urlhash.Url = _managedConfig.Assets.BackgroundBaseUrl.Replace("{filename}", Uri.EscapeUriString(urlhash.Url));
                    }

                    foreach (var urlhash in content.FileCdnUrls)
                    {
                        if (urlhash.Url != null)
                            urlhash.Url = _managedConfig.Assets.FileBaseUrl.Replace("{filename}", Uri.EscapeUriString(urlhash.Url));
                    }

                    //content.FileUrl = content.FileHash;
                    //if (content.FileUrl != null)
                    //{
                    //    content.FileUrl = _managedConfig.Assets.FileBaseUrl
                    //        .Replace("{filename}", Uri.EscapeUriString(content.FileUrl));
                    //}                    
                }

                foreach (var link in asset.AssociatedContents)
                {
                    foreach (var urlhash in link.ThumbnailCdnUrls)
                    {
                        if (urlhash.Url != null)
                            urlhash.Url = _managedConfig.Assets.ThumbnailBaseUrl.Replace("{filename}", Uri.EscapeUriString(urlhash.Url));
                    }

                    foreach (var urlhash in link.BackgroundCdnUrls)
                    {
                        if (urlhash.Url != null)
                            urlhash.Url = _managedConfig.Assets.BackgroundBaseUrl.Replace("{filename}", Uri.EscapeUriString(urlhash.Url));
                    }

                    foreach (var urlhash in link.FileCdnUrls)
                    {
                        if (urlhash.Url != null)
                            urlhash.Url = _managedConfig.Assets.FileBaseUrl.Replace("{filename}", Uri.EscapeUriString(urlhash.Url));
                    }
                }
            }
        }
    }
}