﻿using System;
using System.IO;
using System.Linq;
using System.Security;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using TDL.ContentPlatform.Server.Common.Models.Communication;
using TDL.ContentPlatform.Server.Database;
using TDL.ContentPlatform.Server.Database.Tables;
using TDL.ContentPlatform.Server.WebApi.ConfigModels;

namespace TDL.ContentPlatform.Server.WebApi.Controllers
{
    [ApiVersion("1.0")]
    //[Authorize(Roles = "Administrator")]
    [Authorize(Roles = "Student,Administrator")]
    [Route("api/v{version:apiVersion}/[controller]/[action]")]
    public class ContentEditController : Controller
    {
        private readonly ILogger<ConfigController> _logger;

        private readonly ContentDbContext _contentDbContext;

        private readonly IMapper _mapper;
        private readonly ManagedConfig _managedConfig;
        private readonly UserManager<TdlUser> _userManager;
        private readonly RoleManager<TdlRole> _roleManager;
        private readonly SignInManager<TdlUser> _signInManager;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public ContentEditController(ILogger<ConfigController> logger, ContentDbContext contentDbContext, IMapper mapper, IOptions<ManagedConfig> managedConfig, UserManager<TdlUser> userManager, RoleManager<TdlRole> roleManager, SignInManager<TdlUser> signInManager, IHttpContextAccessor httpContextAccessor)
        {
            _logger = logger;
            _contentDbContext = contentDbContext;
            _mapper = mapper;
            _managedConfig = managedConfig.Value;
            _userManager = userManager;
            _roleManager = roleManager;
            _signInManager = signInManager;
            _httpContextAccessor = httpContextAccessor;
        }

        #region SetupDb

        [HttpPost]

        public async Task<ContentEditMetadataResponse> Metadata()
        {
            // Read body into string
            string data;
            using (var sr = new StreamReader(Request.Body))
            {
                data = sr.ReadToEnd();
            }
            // Convert to request object
            var request = JsonConvert.DeserializeObject<ContentEditMetadataRequest>(data);

            var ret = new ContentEditMetadataResponse();

            var userId = User?.FindFirst(ClaimTypes.NameIdentifier)?.Value ?? "";
            //var userId = _userManager.GetUserId(User);
            Guid.TryParse(userId, out var userGuid);

            try
            {
                var assetContent = await _contentDbContext.AssetContent.Include(ac => ac.Metadata).FirstOrDefaultAsync(ac => ac.Id == request.AssetContentId);
                if (assetContent == null)
                    throw new SecurityException($"Attempt at publishing to AssetContent ID {request.AssetContentId} that does not exist.");

                // Delete previous versions
                AssetContentMetadata latest = null;
                var delete = assetContent.Metadata.Where(m => m.IsDeleted != true && m.Type == request.Type).ToList();
                if (delete.Count > 0)
                {
                    foreach (var d in delete)
                    {
                        d.IsDeleted = true;
                        d.ModifiedBy = userGuid;
                        d.ModifiedOn = DateTime.UtcNow;
                        latest = d;
                    }

                    _contentDbContext.UpdateRange(delete);
                    await _contentDbContext.SaveChangesAsync();
                }

                // Not have any previous? Make new
                if (latest == null)
                {
                    latest = new AssetContentMetadata()
                    {
                        CreatedBy = userGuid,
                        CreatedOn = DateTime.UtcNow,
                        IsDeleted = false,
                        AssetContentId = request.AssetContentId,
                        Type = request.Type
                    };

                }
                else
                    latest = _mapper.Map<AssetContentMetadata>(latest);

                // Modify
                latest.Id = 0;
                latest.IsEnabled = true;
                latest.ModifiedBy = userGuid;
                latest.ModifiedOn = DateTime.UtcNow;
                latest.Json = request.Json;


                // Add
                _contentDbContext.AssetContentMetadata.Add(latest);

                await _contentDbContext.SaveChangesAsync();

            }
            catch (Exception exception)
            {
                _logger.LogError(exception, "");
                ret.Success = false;
                ret.ErrorMessage = exception.Message;
                return ret;
            }

            ret.Success = true;
            return ret;
        }
        #endregion

    }
}