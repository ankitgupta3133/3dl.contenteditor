﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using TDL.ContentPlatform.Server.Common.Models.Communication;
using TDL.ContentPlatform.Server.Database;
using TDL.ContentPlatform.Server.Database.Tables;
using TDL.ContentPlatform.Server.WebApi.ConfigModels;

namespace TDL.ContentPlatform.Server.WebApi.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]/[action]")]
    public class ContentPublishingController : Controller
    {
        private readonly ILogger<ConfigController> _logger;
        private readonly ContentDbContext _contentDbContext;
        private readonly IMapper _mapper;
        private readonly ManagedConfig _managedConfig;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly UserManager<TdlUser> _userManager;

        public ContentPublishingController(ILogger<ConfigController> logger, ContentDbContext contentDbContext,
            IMapper mapper, IOptions<ManagedConfig> managedConfig, IHttpContextAccessor httpContextAccessor,
            UserManager<TdlUser> userManager)
        {
            _logger = logger;
            _contentDbContext = contentDbContext;
            _mapper = mapper;
            _managedConfig = managedConfig.Value;
            _httpContextAccessor = httpContextAccessor;
            _userManager = userManager;
        }

        #region Publish Content
        [HttpGet]
        public async Task<ContentPublishingPublishResponse> Publish(ContentPublishingPublishRequest request)
        {
            if(request.Password != _managedConfig.ContentPublishing.Password)
            {
                throw new SecurityException("Incorrect Password");
            }

            var ret = new ContentPublishingPublishResponse();

            var content = _contentDbContext.AssetContent.Where(x => x.Id == request.Id && x.IsDeleted == false).FirstOrDefault();
            if (content != null)
            {
                string sourcePath = System.IO.Path.Combine(_managedConfig.ContentPublishing.SourceDir, content.FilePath);
                string targetPath = _managedConfig.ContentPublishing.DestinationDir;

                content.PublishFile = Tedd.StreamHasher.File.CopyFileAndRename(sourcePath, targetPath).Result.Hash;

                //This "If" block is for existing assets, to copy the thumbnail image
                if (content.Thumbnail != null)
                {
                    var asset = _contentDbContext.Asset.Where(x => x.AssetId == content.AssetId).FirstOrDefault();
                    string sourceThumbnailPath = System.IO.Path.Combine(_managedConfig.ContentPublishing.DestinationDir, asset.PackageId.ToString(), content.Thumbnail);
                    string targetThumbnailPath = _managedConfig.ContentPublishing.DestinationThumbnailDir;

                    content.PublishThumbnail = Tedd.StreamHasher.File.CopyFileAndRename(sourceThumbnailPath, targetThumbnailPath).Result.Hash;
                }

                content.IsEnabled = true;
                content.ModifiedOn = DateTime.Now;
                content.ModifiedBy = request.UserId;
                await _contentDbContext.SaveChangesAsync();

                ret.Success = true;
            }

            return ret;
        }
        #endregion

        #region Unpublish Content
        [HttpGet]
        public async Task<ContentPublishingUnpublishResponse> Unpublish(ContentPublishingUnpublishRequest request)
        {
            if (request.Password != _managedConfig.ContentPublishing.Password)
            {
                throw new SecurityException("Incorrect Password");
            }

            var ret = new ContentPublishingUnpublishResponse();

            var content = _contentDbContext.AssetContent.Where(x => x.Id == request.Id && x.IsDeleted == false).FirstOrDefault();
            if (content != null)
            {
                int isInUse = _contentDbContext.AssetContent.Where(ac => ac.IsEnabled == true  && ac.FileName == content.PublishFile).Count();
                if (isInUse == 1)
                {
                    var file = System.IO.Path.Combine(_managedConfig.ContentPublishing.DestinationDir, content.PublishFile);
                    if (System.IO.File.Exists(file))
                        System.IO.File.Delete(file);
                }

                content.IsEnabled = false;
                content.ModifiedOn = DateTime.Now;
                content.ModifiedBy = request.UserId;
                await _contentDbContext.SaveChangesAsync();

                ret.Success = true;
            }

            return ret;
        }
        #endregion
    }
}