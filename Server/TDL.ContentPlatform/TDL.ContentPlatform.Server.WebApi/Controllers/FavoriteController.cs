﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using TDL.ContentPlatform.Server.Common.Models.Communication;
using TDL.ContentPlatform.Server.Common.Models.Content;
using TDL.ContentPlatform.Server.Common.Models.v2;
using TDL.ContentPlatform.Server.Database;
using TDL.ContentPlatform.Server.Database.Tables;
using TDL.ContentPlatform.Server.WebApi.ConfigModels;
using Asset = TDL.ContentPlatform.Server.Common.Models.v2.Asset;
using AssetContent = TDL.ContentPlatform.Server.Common.Models.v2.AssetContent;

namespace TDL.ContentPlatform.Server.WebApi.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]/[action]")]
    public class FavoriteController : Controller
    {
        private readonly ILogger<ConfigController> _logger;

        private readonly ContentDbContext _contentDbContext;

        private readonly IMapper _mapper;
        private readonly ManagedConfig _managedConfig;
        private readonly UserManager<TdlUser> _userManager;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public FavoriteController(ILogger<ConfigController> logger, ContentDbContext contentDbContext, IMapper mapper, IOptions<ManagedConfig> managedConfig, 
            IHttpContextAccessor httpContextAccessor, UserManager<TdlUser> userManager)
        {
            _logger = logger;
            _contentDbContext = contentDbContext;
            _mapper = mapper;
            _managedConfig = managedConfig.Value;
            _httpContextAccessor = httpContextAccessor;
            _userManager = userManager;
        }

        //private static Regex _platformRewriteRegex = new Regex("(Player|Editor)", RegexOptions.Compiled | RegexOptions.IgnoreCase);
        private static Regex _platformArchitectureRewriteRegex = new Regex("(X(86|64)|ARM)$", RegexOptions.Compiled | RegexOptions.IgnoreCase);

        [HttpGet]
        [Authorize(Roles = "Student,Teacher,Administrator")]
        public async Task<FavoriteGetAssetsResponse> GetAssets(FavoriteGetAssetsRequest request)
        {
            // Client output
            FavoriteGetAssetsResponse objResponse = new FavoriteGetAssetsResponse();

            objResponse.Success = true;
            objResponse.ErrorMessage = string.Empty;

            // Data connection for stored proc
            SqlConnection objConnection = null;

            try
            {
                // Get user id
                string strUserName = User.FindFirst(ClaimTypes.Name).Value;

                // Get user object
                TdlUser objUser = await _userManager.FindByNameAsync(strUserName);

                if (string.IsNullOrWhiteSpace(request.Platform))
                    request.Platform = "All";

                if (request.Platform.Length > 100)
                    throw new Exception("Platform name too long.");

                // Remove "Player" and "Editor" from string. Ref https://docs.unity3d.com/ScriptReference/RuntimePlatform.html
                request.Platform = request.Platform = RewriteRequestPlatform(request.Platform);

                // All platforms
                List<CsNewPlatform> lstPlatforms = _contentDbContext.CsNewPlatform.ToList();

                // Fill dictionary. We do this conservatively in case someone names two platforms the same. Then we don't want API to crash. Therefore we don't use .ToDictionary().
                Dictionary<string, CsNewPlatform> lstAllPlatforms = new Dictionary<string, CsNewPlatform>(StringComparer.OrdinalIgnoreCase); // Also we make dictionary case insensitive

                Dictionary<Guid, CsNewPlatform> lstAllPlatformIDs = new Dictionary<Guid, CsNewPlatform>(); // Also we make dictionary case insensitive

                foreach (CsNewPlatform objPlatform in lstPlatforms)
                {

                    if (!lstAllPlatforms.ContainsKey(objPlatform.NewName))
                        lstAllPlatforms.Add(objPlatform.NewName, objPlatform);

                    if (!lstAllPlatformIDs.ContainsKey(objPlatform.Id))
                        lstAllPlatformIDs.Add(objPlatform.Id, objPlatform);

                } // foreach (CsNewPlatform objPlatform in lstPlatforms)

                // Get special "All"-platform
                lstAllPlatforms.TryGetValue("All", out CsNewPlatform objAllPlatform);

                if (objAllPlatform == null)
                    throw new Exception("Platform \"All\" does not exist in database.");

                // Get platform
                lstAllPlatforms.TryGetValue(request.Platform, out var objOutputPlatform);

                if (objOutputPlatform == null)
                {
                    objResponse.Success = false;
                    objResponse.ErrorMessage = "Invalid Platform";

                    return objResponse;
                }

                // Get all recent assets
                var favorite = _contentDbContext.FavouriteAssets.Where(x => x.UserId.Equals(objUser.Id) && x.IsDeleted == false).ToList();

                //if (favorite == null || favorite.Count == 0)
                //{
                //    objResponse.Success = true;
                //    objResponse.ErrorMessage = "No favorite asset available";

                //    return objResponse;
                //}

                // Open connection for stored procedure
                objConnection = new SqlConnection(_contentDbContext.Database.GetDbConnection().ConnectionString);

                // Used to avoid wrong execution plan being used by mssql when calling from C# clients
                SqlCommand objCommand = new SqlCommand("set arithabort on;", objConnection);

                objCommand.Connection.Open();
                objCommand.ExecuteNonQuery();

                // Indefinite timeout
                objCommand.CommandTimeout = 0;

                // Prepare a call to the stored procedure
                objCommand = new SqlCommand("dbo.spAssetList", objConnection);
                objCommand.CommandType = CommandType.StoredProcedure;

                // Create parameters
                objCommand.Parameters.Add(new SqlParameter("userId", objUser.Id));
                objCommand.Parameters.Add(new SqlParameter("platform", objOutputPlatform.Id));

                objCommand.CommandTimeout = 0;

                // Execute the stored procedure
                SqlDataReader objDataReader = objCommand.ExecuteReader();

                // There will be only one row, since the
                // result is rendered as JSON
                if (objDataReader.Read() == false)
                    return objResponse;

                // If there are no results, the data will be DBNull
                object objData = objDataReader.GetValue(0);

                if (objData == DBNull.Value)
                    return objResponse;

                // Parse database response
                objResponse.ContentRoot = JsonConvert.DeserializeObject<ContentRoot[]>(objData.ToString()).First();

                // All asset names
                Dictionary<int, AssetNames[]> lstAssetNames = _contentDbContext.AssetNames.GroupBy(a => a.AssetId).ToDictionary(k => k.Key, v => v.ToArray());

                // All languages
                List<CsNewLanguage> lstDbLanguages = _contentDbContext.CsNewLanguage.ToList();

                // Assign language
                lstAssetNames.SelectMany(a => a.Value).ToList().ForEach(delegate (AssetNames objAssetName)
                {
                    objAssetName.Language = lstDbLanguages.SingleOrDefault(l => l.Id == objAssetName.LanguageId);
                });

                // Used languages
                List<CsNewLanguage> lstLanguages = new List<CsNewLanguage>();

                // Load grades, subjects and topics in order to print type count
                foreach (Grade objGrade in objResponse.ContentRoot.Grades)
                {

                    foreach (Subject objSubject in objGrade.Subjects)
                    {

                        foreach (Topic objTopic in objSubject.Topics)
                        {

                            foreach (Subtopic objSubtopic in objTopic.Subtopics)
                            {

                                foreach (Asset objAsset in objSubtopic.Assets.ToList())
                                {
                                    if (favorite.Where(r => r.AssetId == objAsset.Id).ToList().Count == 0)
                                    {
                                        objSubtopic.Assets.Remove(objAsset);
                                        continue;
                                    }
                                    else
                                    {
                                        if (string.IsNullOrWhiteSpace(objSubtopic.ThumbnailHash) == true && objAsset.AssetContents.Count() > 0)
                                            objSubtopic.ThumbnailHash = objAsset.AssetContents.First()?.ThumbnailHash;

                                        if (string.IsNullOrWhiteSpace(objSubtopic.ThumbnailHash) == false && (objSubtopic.ThumbnailCdnUrls == null || objSubtopic.ThumbnailCdnUrls?.Count == 0))
                                        {

                                            if (objSubtopic.ThumbnailCdnUrls == null)
                                                objSubtopic.ThumbnailCdnUrls = new List<CdnUrl>();

                                            objSubtopic.ThumbnailCdnUrls.Add(new CdnUrl
                                            {
                                                Url = _managedConfig.Assets.ThumbnailBaseUrl.Replace("{filename}", Uri.EscapeUriString(objSubtopic.ThumbnailHash))
                                            });

                                        } // if (string.IsNullOrWhiteSpace(objSubtopic.ThumbnailHash) == false && (objSubtopic.ThumbnailCdnUrls == null || objSubtopic.ThumbnailCdnUrls?.Count == 0))

                                        if (objSubtopic.RecursiveTypeCount.ContainsKey(objAsset.Type))
                                            objSubtopic.RecursiveTypeCount[objAsset.Type]++;
                                        else
                                            objSubtopic.RecursiveTypeCount.Add(objAsset.Type, 1);

                                        objAsset.AssetContents.ForEach(delegate (AssetContent objAssetContent)
                                        {

                                            if (string.IsNullOrWhiteSpace(objAssetContent.ThumbnailHash) == false)
                                                objAssetContent.ThumbnailCdnUrls.Add(new CdnUrl
                                                {
                                                    Url = _managedConfig.Assets.ThumbnailBaseUrl.Replace("{filename}", Uri.EscapeUriString(objAssetContent.ThumbnailHash))
                                                });

                                            if (string.IsNullOrWhiteSpace(objAssetContent.BackgroundHash) == false)
                                                objAssetContent.BackgroundCdnUrls.Add(new CdnUrl
                                                {
                                                    Url = _managedConfig.Assets.BackgroundBaseUrl.Replace("{filename}", Uri.EscapeUriString(objAssetContent.BackgroundHash))
                                                });

                                            objAssetContent.FileCdnUrls.Add(new CdnUrl
                                            {
                                                Url = _managedConfig.Assets.FileBaseUrl.Replace("{filename}", Uri.EscapeUriString(objAssetContent.FileHash))
                                            });

                                            if (objAssetContent.Metadata == null)
                                                objAssetContent.Metadata = new Common.Models.v2.AssetContentMetadata();

                                        });

                                        foreach (AssociatedContent objAssociatedAsset in objAsset.AssociatedContents)
                                        {

                                            if (string.IsNullOrWhiteSpace(objAssociatedAsset.ThumbnailHash) == false)
                                            {

                                                objAssociatedAsset.ThumbnailCdnUrls = new List<CdnUrl>();

                                                objAssociatedAsset.ThumbnailCdnUrls.Add(new CdnUrl
                                                {
                                                    Url = _managedConfig.Assets.ThumbnailBaseUrl.Replace("{filename}", Uri.EscapeUriString(objAssociatedAsset.ThumbnailHash))
                                                });

                                            } // if (string.IsNullOrWhiteSpace(objAssociatedAsset.ThumbnailHash) == false)

                                            if (string.IsNullOrWhiteSpace(objAssociatedAsset.BackgroundHash) == false)
                                            {

                                                objAssociatedAsset.BackgroundCdnUrls = new List<CdnUrl>();

                                                objAssociatedAsset.BackgroundCdnUrls.Add(new CdnUrl
                                                {
                                                    Url = _managedConfig.Assets.BackgroundBaseUrl.Replace("{filename}", Uri.EscapeUriString(objAssociatedAsset.BackgroundHash))
                                                });

                                            }

                                            if (string.IsNullOrWhiteSpace(objAssociatedAsset.FileHash) == false)
                                                objAssociatedAsset.FileCdnUrls.Add(new CdnUrl
                                                {
                                                    Url = _managedConfig.Assets.FileBaseUrl.Replace("{filename}", Uri.EscapeUriString(objAssociatedAsset.FileHash))
                                                });

                                        } // foreach (AssociatedContent objAssociatedAsset in objAsset.AssociatedContents)

                                        objAsset.LocalizedName = new Dictionary<string, string>();

                                        if (lstAssetNames.ContainsKey(objAsset.Id))
                                        {

                                            foreach (AssetNames objAssetName in lstAssetNames[objAsset.Id].Where(a => a.Language != null && a.IsDeleted == false).OrderBy(a => a.Language.NewLanguageshortname))
                                            {

                                                try
                                                {

                                                    objAsset.LocalizedName.Add(objAssetName.Language.NewLanguageshortname, objAssetName.Name);

                                                    if (lstLanguages.Any(l => l.Id == objAssetName.LanguageId) == false)
                                                        lstLanguages.Add(objAssetName.Language);

                                                }
                                                catch { }

                                            } // foreach (AssetNames objAssetName in lstAssetNames[objAsset.Id].Where(a => a.Language != null && a.IsDeleted == false).OrderBy(a => a.Language.NewLanguageshortname))

                                        } // if (lstAssetNames.ContainsKey(objAsset.Id))
                                    }

                                } // foreach (Asset objAsset in objSubtopic.Assets)

                            } // foreach (Subtopic objSubtopic in objTopic.Subtopics)

                            foreach (Asset objAsset in objTopic.Assets.ToList())
                            {
                                if (favorite.Where(r => r.AssetId == objAsset.Id).ToList().Count == 0)
                                {
                                    objTopic.Assets.Remove(objAsset);
                                    continue;
                                }
                                else
                                {
                                    if (objTopic.RecursiveTypeCount.ContainsKey(objAsset.Type))
                                        objTopic.RecursiveTypeCount[objAsset.Type]++;
                                    else
                                        objTopic.RecursiveTypeCount.Add(objAsset.Type, 1);

                                    objAsset.AssetContents.ForEach(delegate (AssetContent objAssetContent)
                                    {

                                        if (string.IsNullOrWhiteSpace(objAssetContent.ThumbnailHash) == false)
                                            objAssetContent.ThumbnailCdnUrls.Add(new CdnUrl
                                            {
                                                Url = _managedConfig.Assets.ThumbnailBaseUrl.Replace("{filename}", Uri.EscapeUriString(objAssetContent.ThumbnailHash))
                                            });

                                        if (string.IsNullOrWhiteSpace(objAssetContent.BackgroundHash) == false)
                                            objAssetContent.BackgroundCdnUrls.Add(new CdnUrl
                                            {
                                                Url = _managedConfig.Assets.BackgroundBaseUrl.Replace("{filename}", Uri.EscapeUriString(objAssetContent.BackgroundHash))
                                            });

                                        objAssetContent.FileCdnUrls.Add(new CdnUrl
                                        {
                                            Url = _managedConfig.Assets.FileBaseUrl.Replace("{filename}", Uri.EscapeUriString(objAssetContent.FileHash))
                                        });

                                        if (objAssetContent.Metadata == null)
                                            objAssetContent.Metadata = new Common.Models.v2.AssetContentMetadata();

                                    });

                                    foreach (AssociatedContent objAssociatedAsset in objAsset.AssociatedContents)
                                    {

                                        if (string.IsNullOrWhiteSpace(objAssociatedAsset.ThumbnailHash) == false)
                                        {

                                            objAssociatedAsset.ThumbnailCdnUrls = new List<CdnUrl>();

                                            objAssociatedAsset.ThumbnailCdnUrls.Add(new CdnUrl
                                            {
                                                Url = _managedConfig.Assets.ThumbnailBaseUrl.Replace("{filename}", Uri.EscapeUriString(objAssociatedAsset.ThumbnailHash))
                                            });

                                        } // if (string.IsNullOrWhiteSpace(objAssociatedAsset.ThumbnailHash) == false)

                                        if (string.IsNullOrWhiteSpace(objAssociatedAsset.BackgroundHash) == false)
                                        {

                                            objAssociatedAsset.BackgroundCdnUrls = new List<CdnUrl>();

                                            objAssociatedAsset.BackgroundCdnUrls.Add(new CdnUrl
                                            {
                                                Url = _managedConfig.Assets.BackgroundBaseUrl.Replace("{filename}", Uri.EscapeUriString(objAssociatedAsset.BackgroundHash))
                                            });

                                        }

                                        if (string.IsNullOrWhiteSpace(objAssociatedAsset.FileHash) == false)
                                            objAssociatedAsset.FileCdnUrls.Add(new CdnUrl
                                            {
                                                Url = _managedConfig.Assets.FileBaseUrl.Replace("{filename}", Uri.EscapeUriString(objAssociatedAsset.FileHash))
                                            });

                                    } // foreach (AssociatedContent objAssociatedAsset in objAsset.AssociatedContents)

                                    objAsset.LocalizedName = new Dictionary<string, string>();

                                    if (lstAssetNames.ContainsKey(objAsset.Id))
                                    {

                                        foreach (AssetNames objAssetName in lstAssetNames[objAsset.Id].Where(a => a.Language != null && a.IsDeleted == false).OrderBy(a => a.Language.NewLanguageshortname))
                                        {

                                            try
                                            {

                                                objAsset.LocalizedName.Add(objAssetName.Language.NewLanguageshortname, objAssetName.Name);

                                                if (lstLanguages.Any(l => l.Id == objAssetName.LanguageId) == false)
                                                    lstLanguages.Add(objAssetName.Language);

                                            }
                                            catch { }

                                        } // foreach (AssetNames objAssetName in lstAssetNames[objAsset.Id].Where(a => a.Language != null && a.IsDeleted == false).OrderBy(a => a.Language.NewLanguageshortname))

                                    } // if (lstAssetNames.ContainsKey(objAsset.Id))
                                }

                            } // foreach (Asset objAsset in objTopic.Assets)

                            if (string.IsNullOrWhiteSpace(objTopic.ThumbnailHash) == true)
                            {

                                // Try subtopic
                                objTopic.ThumbnailHash = objTopic.Assets.SelectMany(a => a.AssetContents).Where(ac => string.IsNullOrWhiteSpace(ac.ThumbnailHash) == false).Select(st => st.ThumbnailHash).FirstOrDefault();

                                if (string.IsNullOrWhiteSpace(objTopic.ThumbnailHash) == false)
                                    objTopic.ThumbnailCdnUrls.Add(new CdnUrl
                                    {
                                        Url = _managedConfig.Assets.ThumbnailBaseUrl.Replace("{filename}", Uri.EscapeUriString(objTopic.ThumbnailHash))
                                    });

                                if (string.IsNullOrWhiteSpace(objTopic.ThumbnailHash) == true)
                                {

                                    // Try subtopic
                                    objTopic.ThumbnailHash = objTopic.Subtopics.Where(st => string.IsNullOrWhiteSpace(st.ThumbnailHash) == false).Select(st => st.ThumbnailHash).FirstOrDefault();

                                    if (string.IsNullOrWhiteSpace(objTopic.ThumbnailHash) == false)
                                        objTopic.ThumbnailCdnUrls.Add(new CdnUrl
                                        {
                                            Url = _managedConfig.Assets.ThumbnailBaseUrl.Replace("{filename}", Uri.EscapeUriString(objTopic.ThumbnailHash))
                                        });

                                } // if (string.IsNullOrWhiteSpace(objTopic.ThumbnailHash) == true)

                            } // if (string.IsNullOrWhiteSpace(objTopic.ThumbnailHash) == true)

                        } // foreach (Topic objTopic in objSubject.Topics)

                        List<Asset> lstSubjectAssetRemove = new List<Asset>();

                        foreach (Asset objAsset in objSubject.Assets)
                        {

                            if (objAsset.AssetContents.Count == 0 || favorite.Where(F => F.AssetId == objAsset.Id).ToList().Count == 0)
                            {
                                lstSubjectAssetRemove.Add(objAsset);
                                continue;

                            } // if (objAsset.AssetContents.Count == 0)

                            objAsset.AssetContents.ForEach(delegate (AssetContent objAssetContent)
                            {

                                if (string.IsNullOrWhiteSpace(objAssetContent.ThumbnailHash) == false)
                                    objAssetContent.ThumbnailCdnUrls.Add(new CdnUrl
                                    {
                                        Url = _managedConfig.Assets.ThumbnailBaseUrl.Replace("{filename}", Uri.EscapeUriString(objAssetContent.ThumbnailHash))
                                    });

                                if (string.IsNullOrWhiteSpace(objAssetContent.BackgroundHash) == false)
                                    objAssetContent.BackgroundCdnUrls.Add(new CdnUrl
                                    {
                                        Url = _managedConfig.Assets.BackgroundBaseUrl.Replace("{filename}", Uri.EscapeUriString(objAssetContent.BackgroundHash))
                                    });

                                objAssetContent.FileCdnUrls.Add(new CdnUrl
                                {
                                    Url = _managedConfig.Assets.FileBaseUrl.Replace("{filename}", Uri.EscapeUriString(objAssetContent.FileHash))
                                });

                                if (objAssetContent.Metadata == null)
                                    objAssetContent.Metadata = new Common.Models.v2.AssetContentMetadata();

                            });

                            foreach (AssociatedContent objAssociatedAsset in objAsset.AssociatedContents)
                            {

                                if (string.IsNullOrWhiteSpace(objAssociatedAsset.ThumbnailHash) == false)
                                {

                                    objAssociatedAsset.ThumbnailCdnUrls = new List<CdnUrl>();

                                    objAssociatedAsset.ThumbnailCdnUrls.Add(new CdnUrl
                                    {
                                        Url = _managedConfig.Assets.ThumbnailBaseUrl.Replace("{filename}", Uri.EscapeUriString(objAssociatedAsset.ThumbnailHash))
                                    });

                                } // if (string.IsNullOrWhiteSpace(objAssociatedAsset.ThumbnailHash) == false)

                                if (string.IsNullOrWhiteSpace(objAssociatedAsset.BackgroundHash) == false)
                                {

                                    objAssociatedAsset.BackgroundCdnUrls = new List<CdnUrl>();

                                    objAssociatedAsset.BackgroundCdnUrls.Add(new CdnUrl
                                    {
                                        Url = _managedConfig.Assets.BackgroundBaseUrl.Replace("{filename}", Uri.EscapeUriString(objAssociatedAsset.BackgroundHash))
                                    });

                                }

                                if (string.IsNullOrWhiteSpace(objAssociatedAsset.FileHash) == false)
                                    objAssociatedAsset.FileCdnUrls.Add(new CdnUrl
                                    {
                                        Url = _managedConfig.Assets.FileBaseUrl.Replace("{filename}", Uri.EscapeUriString(objAssociatedAsset.FileHash))
                                    });

                            } // foreach (AssociatedContent objAssociatedAsset in objAsset.AssociatedContents)

                            objAsset.LocalizedName = new Dictionary<string, string>();

                            if (lstAssetNames.ContainsKey(objAsset.Id))
                            {

                                foreach (AssetNames objAssetName in lstAssetNames[objAsset.Id].Where(a => a.Language != null && a.IsDeleted == false).OrderBy(a => a.Language.NewLanguageshortname))
                                {

                                    try
                                    {

                                        objAsset.LocalizedName.Add(objAssetName.Language.NewLanguageshortname, objAssetName.Name);

                                        if (lstLanguages.Any(l => l.Id == objAssetName.LanguageId) == false)
                                            lstLanguages.Add(objAssetName.Language);

                                    }
                                    catch { }

                                } // foreach (AssetNames objAssetName in lstAssetNames[objAsset.Id].Where(a => a.Language != null && a.IsDeleted == false).OrderBy(a => a.Language.NewLanguageshortname))

                            } // if (lstAssetNames.ContainsKey(objAsset.Id))

                        } // foreach (Asset objAsset in objSubject.Assets)

                        if (lstSubjectAssetRemove.Any())
                            objSubject.Assets.RemoveAll(a => lstSubjectAssetRemove.Select(r => r.Id).Contains(a.Id));

                        if (string.IsNullOrWhiteSpace(objSubject.ThumbnailHash) == true)
                        {

                            // Try topic
                            objSubject.ThumbnailHash = objSubject.Topics.Where(t => string.IsNullOrWhiteSpace(t.ThumbnailHash) == false).Select(t => t.ThumbnailHash).FirstOrDefault();

                            if (string.IsNullOrWhiteSpace(objSubject.ThumbnailHash) == true)
                                // Try subtopic
                                objSubject.ThumbnailHash = objSubject.Topics.SelectMany(t => t.Subtopics).Where(st => string.IsNullOrWhiteSpace(st.ThumbnailHash) == false).Select(st => st.ThumbnailHash).FirstOrDefault();

                            if (string.IsNullOrWhiteSpace(objSubject.ThumbnailHash) == false)
                                objSubject.ThumbnailCdnUrls.Add(new CdnUrl
                                {
                                    Url = _managedConfig.Assets.ThumbnailBaseUrl.Replace("{filename}", Uri.EscapeUriString(objSubject.ThumbnailHash))
                                });

                        } // if (string.IsNullOrWhiteSpace(objSubject.ThumbnailHash) == true)

                    } // foreach (Subject objSubject in objGrade.Subjects)

                    if (string.IsNullOrWhiteSpace(objGrade.ThumbnailHash) == true)
                    {

                        // Try subject
                        objGrade.ThumbnailHash = objGrade.Subjects.Where(s => string.IsNullOrWhiteSpace(s.ThumbnailHash) == false).Select(s => s.ThumbnailHash).FirstOrDefault();

                        if (string.IsNullOrWhiteSpace(objGrade.ThumbnailHash) == true)
                        {

                            // Try topic
                            objGrade.ThumbnailHash = objGrade.Subjects.SelectMany(s => s.Topics).Where(t => string.IsNullOrWhiteSpace(t.ThumbnailHash) == false).Select(t => t.ThumbnailHash).FirstOrDefault();

                            if (string.IsNullOrWhiteSpace(objGrade.ThumbnailHash) == true)
                                // Try subtopic
                                objGrade.ThumbnailHash = objGrade.Subjects.SelectMany(s => s.Topics.SelectMany(t => t.Subtopics)).Where(st => string.IsNullOrWhiteSpace(st.ThumbnailHash) == false).Select(st => st.ThumbnailHash).FirstOrDefault();

                        } // if (string.IsNullOrWhiteSpace(objGrade.ThumbnailHash) == true)

                    } // if (string.IsNullOrWhiteSpace(objGrade.ThumbnailHash) == true)

                    if (string.IsNullOrWhiteSpace(objGrade.ThumbnailHash) == false)
                        objGrade.ThumbnailCdnUrls.Add(new CdnUrl
                        {
                            Url = _managedConfig.Assets.ThumbnailBaseUrl.Replace("{filename}", Uri.EscapeUriString(objGrade.ThumbnailHash))
                        });

                } // foreach (Grade objGrade in objResponse.ContentRoot.Grades)

                // Do some cleanup: We only return levels that have actual content
                if (!request.IncludeEmptyLevels)
                {

                    foreach (var grade in objResponse.ContentRoot.Grades.ToArray())
                    {

                        foreach (var subject in grade.Subjects.ToArray())
                        {

                            foreach (var topic in subject.Topics.ToArray())
                            {

                                foreach (var subtopic in topic.Subtopics.ToArray())
                                {

                                    if (subtopic.Assets.Count == 0)
                                        topic.Subtopics.Remove(subtopic);

                                } // foreach (var subtopic in topic.Subtopics.ToArray())

                                if (topic.Assets.Count == 0 && topic.Subtopics.Count == 0)
                                    subject.Topics.Remove(topic);

                            } // foreach (var topic in subject.Topics.ToArray())

                            if (subject.Assets.Count == 0 && subject.Topics.Count == 0)
                                grade.Subjects.Remove(subject);

                        } // foreach (var subject in grade.Subjects.ToArray())

                        if (grade.Assets.Count == 0 && grade.Subjects.Count == 0)
                            objResponse.ContentRoot.Grades.Remove(grade);

                    } // foreach (var grade in objResponse.ContentRoot.Grades.ToArray())

                } // if (!request.IncludeEmptyLevels)
            }
            catch (Exception ex)
            {

                objResponse.Success = false;
                objResponse.ErrorMessage = ex.Message;

            }
            finally
            {
                if (objConnection != null)
                    objConnection.Close();
            }

            return objResponse;
        }

        [HttpPost]
        [Authorize(Roles = "Student,Teacher,Administrator")]
        public async Task<FavoriteAddAssetResponse> AddAsset(FavoriteAddAssetRequest request)
        {
            var ret = new FavoriteAddAssetResponse();

            try
            {
                // Get user id
                var username = User.FindFirst(ClaimTypes.Name).Value;
                var user = await _userManager.FindByNameAsync(username);
                if (user == null)
                {
                    user = await _userManager.FindByEmailAsync(username);
                }
                var UserID = user.Id;

                var db = _contentDbContext;

                //To get asset
                var content = db.AssetContent.Where(w => w.IsEnabled == true && w.AssetId == request.AssetID).OrderByDescending(x => x.Id).FirstOrDefault();

                var query = await db.FavouriteAssets.Where(p => p.UserId == UserID && p.AssetContentId == content.Id).FirstOrDefaultAsync();

                if (query == null)
                {
                    //To get older version asset
                    var assets = await db.FavouriteAssets.Where(p => p.UserId == UserID && p.AssetId == request.AssetID).ToListAsync();
                    assets.ForEach(i => i.IsDeleted = true);

                    FavouriteAssets newAsset = new FavouriteAssets()
                    {
                        UserId = UserID,
                        AssetId = content.AssetId.Value,
                        AssetContentId = content.Id,
                        CreatedOn = DateTime.Now,
                        IsDeleted = false
                    };
                    db.FavouriteAssets.Add(newAsset);
                    db.SaveChanges();
                }
                else
                {
                    query.AssetId = content.AssetId.Value;
                    query.AssetContentId = content.Id;
                    query.CreatedOn = DateTime.Now;
                    query.IsDeleted = false;
                    db.SaveChanges();
                }
            }
            catch (Exception exception)
            {
                _logger.LogError(exception, "Processing FavoriteAddAsset request.");
                ret.Success = false;
                ret.ErrorMessage = exception.Message;
                return ret;
            }
            ret.Success = true;
            return ret;
        }

        [HttpPost]
        [Authorize(Roles = "Student,Teacher,Administrator")]
        public async Task<FavoriteRemoveAssetResponse> RemoveAsset(FavoriteRemoveAssetRequest request)
        {
            var ret = new FavoriteRemoveAssetResponse();

            try
            {
                // Get user id
                var username = User.FindFirst(ClaimTypes.Name).Value;
                var user = await _userManager.FindByNameAsync(username);
                var UserID = user.Id;

                var db = _contentDbContext;

                //To get asset
                var query = db.FavouriteAssets.Where(p => p.UserId == UserID && p.AssetId == request.AssetID).ToList();
                query.ForEach(i => i.IsDeleted = true);
                db.SaveChanges();
            }
            catch (Exception exception)
            {
                _logger.LogError(exception, "Processing FavoriteRemoveAsset request.");
                ret.Success = false;
                ret.ErrorMessage = exception.Message;
                return ret;
            }
            ret.Success = true;
            return ret;
        }

        private string RewriteRequestPlatform(string requestPlatform)
        {
            // Remove "Player" and "Editor" from string. Ref https://docs.unity3d.com/ScriptReference/RuntimePlatform.html
            //requestPlatform = _platformRewriteRegex.Replace(requestPlatform, "");
            requestPlatform = _platformArchitectureRewriteRegex.Replace(requestPlatform, "");
            requestPlatform = requestPlatform.Replace("Player", "");
            requestPlatform = requestPlatform.Replace("Editor", "");
            requestPlatform = requestPlatform.Replace("Metro", "WSA");
            return requestPlatform;
        }

        private void AddTypeCount(Dictionary<string, int> dst, Dictionary<string, int> src)
        {
            foreach (var kvp in src)
            {
                //if (dst==null)
                //    dst=new Dictionary<string, int>();
                if (dst.TryGetValue(kvp.Key, out var c))
                    dst[kvp.Key] = c + kvp.Value;
                else
                    dst.Add(kvp.Key, kvp.Value);
            }
        }

        private Dictionary<string, int> TypeCount(List<Asset> assets)
        {
            var ret = new Dictionary<string, int>();
            foreach (var type in assets.Select(a => a.Type))
            {
                var c = 0;
                if (!ret.TryGetValue(type, out c))
                    ret.Add(type, 1);
                else
                    ret[type] = ++c;
            }

            return ret;
        }

        private void FixAssetUrls(List<Asset> allAssets)
        {
            // We calculate URL's at this point. For now we do this in source table data, crappy way, but works... :P
            // Should have been done after automapper.
            foreach (var asset in allAssets)
            {
                foreach (var content in asset.AssetContents)
                {
                    foreach (var urlhash in content.ThumbnailCdnUrls)
                    {
                        if (urlhash.Url != null)
                            urlhash.Url = _managedConfig.Assets.ThumbnailBaseUrl.Replace("{filename}", Uri.EscapeUriString(urlhash.Url));
                    }

                    foreach (var urlhash in content.FileCdnUrls)
                    {
                        if (urlhash.Url != null)
                            urlhash.Url = _managedConfig.Assets.FileBaseUrl.Replace("{filename}", Uri.EscapeUriString(urlhash.Url));
                    }                   
                }

                foreach (var link in asset.AssociatedContents)
                {
                    foreach (var urlhash in link.ThumbnailCdnUrls)
                    {
                        if (urlhash.Url != null)
                            urlhash.Url = _managedConfig.Assets.ThumbnailBaseUrl.Replace("{filename}", Uri.EscapeUriString(urlhash.Url));
                    }

                    foreach (var urlhash in link.FileCdnUrls)
                    {
                        if (urlhash.Url != null)
                            urlhash.Url = _managedConfig.Assets.FileBaseUrl.Replace("{filename}", Uri.EscapeUriString(urlhash.Url));
                    }
                }
            }
        }
    }
}