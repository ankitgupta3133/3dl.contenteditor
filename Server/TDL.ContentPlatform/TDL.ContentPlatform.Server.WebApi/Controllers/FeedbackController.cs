﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using TDL.ContentPlatform.Server.Common.Models.Communication;
using TDL.ContentPlatform.Server.Common.Models.Content;
using TDL.ContentPlatform.Server.Database;
using TDL.ContentPlatform.Server.Database.Tables;
using TDL.ContentPlatform.Server.WebApi.ConfigModels;

namespace TDL.ContentPlatform.Server.WebApi.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]/[action]")]
    public class FeedbackController : Controller
    {
        private readonly ILogger<ConfigController> _logger;
        private readonly ContentDbContext _contentDbContext;
        private readonly IMapper _mapper;
        private readonly ManagedConfig _managedConfig;
        private readonly UserManager<TdlUser> _userManager;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private IHostingEnvironment _hostingEnvironment;

        public FeedbackController(ILogger<ConfigController> logger, ContentDbContext contentDbContext, IMapper mapper, IOptions<ManagedConfig> managedConfig,
            IHttpContextAccessor httpContextAccessor, UserManager<TdlUser> userManager, IHostingEnvironment environment)
        {
            _logger = logger;
            _contentDbContext = contentDbContext;
            _mapper = mapper;
            _managedConfig = managedConfig.Value;
            _httpContextAccessor = httpContextAccessor;
            _userManager = userManager;
            _hostingEnvironment = environment;
        }

        [HttpPost]
        [Authorize(Roles = "Student,Teacher,Administrator")]
        public async Task<FeedbackAddAssetResponse> AddAsset(FeedbackAddAssetRequest request, [FromForm]IFormFile Attachment)
        {
            var ret = new FeedbackAddAssetResponse();

            try
            {
                //Replace line break with html br tag
                if(!string.IsNullOrWhiteSpace(request.Feedback))
                {
                    request.Feedback = request.Feedback.Replace("\r\n", "<br>").Replace("\n", "<br>").Replace("\r", "<br>");
                }

                // Get user id
                var username = User.FindFirst(ClaimTypes.Name).Value;
                var user = await _userManager.FindByNameAsync(username);
                if (user == null)
                {
                    user = await _userManager.FindByEmailAsync(username);
                }
                var UserID = user.Id;

                var db = _contentDbContext;

                AssetContentFeedback feedback = new AssetContentFeedback()
                {
                    UserId = UserID,
                    AssetContentId = request.AssetContentID,
                    Feedback = request.Feedback,
                    //GradeId = request.GradeId,
                    //SubjectId = request.SubjectId,
                    //TopicId = request.TopicId,
                    //SubtopicId = request.SubtopicId,
                    //SubscriptionId = Guid.Empty,
                    //LanguageId = Guid.Empty,
                    //PackageId = Guid.Empty,
                    //SyllabusId = Guid.Empty,
                    //StreamId = Guid.Empty,
                    CreatedOn = DateTime.Now,
                    CreatedBy = UserID,
                    CreatedByName = user.Firstname + (string.IsNullOrEmpty(user.Lastname) ? "" : " ") + user.Lastname,
                    IsEnabled = true,
                    IsDeleted = false
                };

                if (Attachment != null && Attachment.Length > 0)
                {
                    int MaxContentLength = 1024 * 1024 * 10; //Size = 1 MB

                    IList<string> AllowedFileExtensions = new List<string> { ".jpg", ".jpeg", ".gif", ".bmp", ".png", ".doc", ".docx", ".mp4", ".txt" };
                    var ext = Attachment.FileName.Substring(Attachment.FileName.LastIndexOf('.'));
                    var extension = ext.ToLower();
                    if (!AllowedFileExtensions.Contains(extension))
                    {
                        ret.ErrorMessage = string.Format("Please Upload file of type .jpg, .jpeg, .gif, .png, .bmp, .doc, .docx, .txt, .mp4.");
                        return ret;
                    }
                    else if (Attachment.Length > MaxContentLength)
                    {
                        ret.ErrorMessage = string.Format("Please Upload a file upto 1 mb.");
                        return ret;
                    }
                    else
                    {
                        //var filePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", Attachment.FileName);
                        //var filePath = Path.Combine(_hostingEnvironment.WebRootPath, Attachment.FileName);
                        var targetPath = Path.Combine(_managedConfig.ContentPublishing.DestinationDir, "feedback", Attachment.FileName);
                        
                        using (var fileStream = new FileStream(targetPath, FileMode.Create))
                        {
                            await Attachment.CopyToAsync(fileStream);
                        }

                        //var result = Tedd.StreamHasher.File.CopyFileAndRename(filePath, _managedConfig.ContentPublishing.DestinationDir).Result.Hash;
                        //System.IO.File.Copy(filePath, targetPath, true);
                        //if (System.IO.File.Exists(filePath))
                        //    System.IO.File.Delete(filePath);

                        feedback.PublishAttachment = Attachment.FileName;
                    }
                }
                        
                if (request.GradeId != Guid.Empty)
                {
                    feedback.GradeId = request.GradeId;
                }
                if (request.SubjectId != Guid.Empty)
                {
                    feedback.SubjectId = request.SubjectId;
                }
                if (request.TopicId != Guid.Empty)
                {
                    feedback.TopicId = request.TopicId;
                }
                if (request.SubtopicId != Guid.Empty)
                {
                    feedback.SubtopicId = request.SubtopicId;
                }

                if (string.IsNullOrEmpty(request.Status))
                {
                    request.Status = "New";
                }

                feedback.StatusId = db.MstStatus.Where(s => s.StatusName.Equals(request.Status)).Select(s => s.StatusId).FirstOrDefault();

                db.AssetContentFeedback.Add(feedback);
                db.SaveChanges();                
            }
            catch (Exception exception)
            {
                _logger.LogError(exception, "Processing FeedbackAddAsset request.");
                ret.Success = false;
                ret.ErrorMessage = exception.Message;
                return ret;
            }
            ret.Success = true;
            return ret;
        }

        [HttpGet]
        [Authorize(Roles = "Student,Teacher,Administrator")]
        public FeedbackGetAssetResponse GetAsset(FeedbackGetAssetRequest request)
        {
            var ret = new FeedbackGetAssetResponse();

            try
            {
                //Get user id
                //var username = User.FindFirst(ClaimTypes.Name).Value;
                //var user = await _userManager.FindByNameAsync(username);
                //if (user == null)
                //{
                //    user = await _userManager.FindByEmailAsync(username);
                //}
                //var UserID = user.Id;

                var db = _contentDbContext;

                ret.Feedback = (from f in db.AssetContentFeedback
                                where f.AssetContentId == request.AssetContentID && f.IsDeleted == false
                                select new FeedbackAsset()
                                {
                                    FeedbackId = f.Id,
                                    Feedback = f.Feedback,
                                    AssetContentId = f.AssetContentId,
                                    UserId = f.UserId,
                                    Username = f.CreatedByName,
                                    GradeId = f.GradeId,
                                    Grade = f.Grade.NewName,
                                    SubjectId = f.SubjectId,
                                    Subject = f.Subject.NewName,
                                    TopicId = f.TopicId,
                                    Topic = f.Topic.NewName,
                                    SubtopicId = f.SubtopicId,
                                    Subtopic = f.Subtopic.NewName,
                                    Status = f.Status.StatusName,
                                    StatusId = f.StatusId,
                                    CreatedOn = f.CreatedOn
                                }).ToList();
                //db.SaveChanges();
            }
            catch (Exception exception)
            {
                _logger.LogError(exception, "Processing FeedbackGetAsset request.");
                ret.Success = false;
                ret.ErrorMessage = exception.Message;
                return ret;
            }
            ret.Success = true;
            return ret;
        }
    }
}