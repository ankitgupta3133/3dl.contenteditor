﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace TDL.ContentPlatform.Server.WebApi.Controllers
{
    [Route("api/v1/[controller]/[action]")]
    public class FeideController : Controller
    {
        private readonly ILogger<ConfigController> _logger;

        public FeideController(ILogger<ConfigController> logger)
        {
            _logger = logger;
        }


        [HttpPost]
        public IActionResult Login()
        {
            try
            {
                string data = string.Empty;
                using (StreamReader reader = new StreamReader(Request.Body, Encoding.UTF8))
                {
                    data = reader.ReadToEnd();
                }

                _logger.LogError(data, "Feide/Login");
                return Ok();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message, "Feide/Login");
                return StatusCode(500);
            }
            
        }

        [HttpPost]
        public IActionResult Logout()
        {
            try
            {
                string data = string.Empty;
                using (StreamReader reader = new StreamReader(Request.Body, Encoding.UTF8))
                {
                    data = reader.ReadToEnd();
                }

                _logger.LogError(data, "Feide/Logout");
                return Ok();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message, "Feide/Logout");
                return StatusCode(500);
            }
           
        }
    }
}