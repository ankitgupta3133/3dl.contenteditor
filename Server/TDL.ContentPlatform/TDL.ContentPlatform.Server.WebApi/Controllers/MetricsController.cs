﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using TDL.ContentPlatform.Common.Models;
using TDL.ContentPlatform.Server.Common.Models.Communication;
using TDL.ContentPlatform.Server.Database;
using TDL.ContentPlatform.Server.Database.Tables;
using TDL.ContentPlatform.Server.WebApi.ConfigModels;
using TDL.ContentPlatform.Server.WebApi.Serviceproviders;

namespace TDL.ContentPlatform.Server.WebApi.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]/[action]")]
    public class MetricsController : Controller
    {
        private readonly ILogger<ConfigController> _logger;

        private readonly ContentDbContext _contentDbContext;

        private readonly IMapper _mapper;
        private readonly ManagedConfig _managedConfig;
        private readonly UserManager<TdlUser> _userManager;
        private readonly RoleManager<TdlRole> _roleManager;
        private readonly SignInManager<TdlUser> _signInManager;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly EmailServiceprovider _emailServiceprovider;

        public MetricsController(ILogger<ConfigController> logger, ContentDbContext contentDbContext, IMapper mapper, IOptions<ManagedConfig> managedConfig, UserManager<TdlUser> userManager, RoleManager<TdlRole> roleManager, SignInManager<TdlUser> signInManager, IHttpContextAccessor httpContextAccessor, EmailServiceprovider emailServiceprovider)
        {
            _logger = logger;
            _contentDbContext = contentDbContext;
            _mapper = mapper;
            _managedConfig = managedConfig.Value;
            _userManager = userManager;
            _roleManager = roleManager;
            _signInManager = signInManager;
            _httpContextAccessor = httpContextAccessor;
            _emailServiceprovider = emailServiceprovider;
        }

        [HttpPost]
        public async Task<MetricsClientFileUploadResponse> ClientFileUpload(List<IFormFile> files)
        {
            var ret = new MetricsClientFileUploadResponse();

            

            try
            {
                var transferredDate = DateTime.UtcNow;
                var isAuthenticated = User.Identity.IsAuthenticated;
                string username = null;
                Guid userid = Guid.Empty;
                if (isAuthenticated)
                {
                    username = User.Identity.Name;
                    Guid.TryParse(User.FindFirstValue(ClaimTypes.NameIdentifier), out userid);
                }

                var fileCount = 0;
                var totalUncompressedSize = 0;
                var totalCompressedSize = 0;
                //System.IO.Compression
                foreach (var file in files)
                {
                    if (file.Length > 0)
                    {
                        string json = null;
                        using (var fs = file.OpenReadStream())
                        {
                            using (var sr = new StreamReader(fs))
                            {
                                totalCompressedSize += (int)fs.Position;
                                json = await sr.ReadToEndAsync();
                                if (json != null)
                                    totalUncompressedSize += json.Length;
                            }
                        }

                        

                        if (!string.IsNullOrWhiteSpace(json))
                        {
                            var metrics = JsonConvert.DeserializeObject<List<MetricsItem>>(json);
                            var dbMetrics = _mapper.Map<List<ClientMetricLog>>(metrics);
                            foreach (var dbm in dbMetrics)
                            {
                                dbm.IsAuthenticated = isAuthenticated;
                                    dbm.UserId = userid;
                                    dbm.Username = username;
                                dbm.TransferredDttmUtc = transferredDate;
                            }
                            // Insert into database
                            _contentDbContext.ClientMetricLog.AddRange(dbMetrics);
                            await _contentDbContext.SaveChangesAsync();
                        }

                        fileCount++;
                    }
                }

                ret.FileCount = fileCount;
                ret.TotalCompressedSize = totalCompressedSize;
                ret.TotalUncompressedSize = totalUncompressedSize;
                ret.Success = true;
            }
            catch (Exception exception)
            {
                _logger.LogError(exception, "MetricsController.ClientFileUpload");
                ret.Success = false;
                ret.ErrorMessage = exception.Message;
                return ret;
            }

            return ret;
        }
    }
}