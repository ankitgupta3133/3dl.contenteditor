﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;

namespace TDL.ContentPlatform.Server.WebApi.Mappers
{
    public class ActivityLocalizedNameToDictionaryResolver : IValueResolver<Database.Tables.Activities, Common.Models.v2.ActivityItem, Dictionary<string, string>>
    {
        public Dictionary<string, string> Resolve(Database.Tables.Activities source, Common.Models.v2.ActivityItem destination, Dictionary<string, string> destMember, ResolutionContext context)
        {
            var ret = new Dictionary<string, string>();
            var names = source.Names.Where(an => an.IsDeleted == false).ToList();
            foreach (var an in names)
            {
                var lcc = an.Language.NewLanguageshortname;
                if (!ret.ContainsKey(lcc))
                {
                    ret.Add(lcc, an.Name);
                }
            }

            // TODO: This is a hack since AssetNames table doesn't contain data
            if (ret.Count == 0)
                ret.Add("en-US", source.ActivityName);
            return ret;
        }
    }

    public class ClassificationLocalizedNameToDictionaryResolver : IValueResolver<Database.Tables.Activities, Common.Models.v2.ClassificationItem, Dictionary<string, string>>
    {
        public Dictionary<string, string> Resolve(Database.Tables.Activities source, Common.Models.v2.ClassificationItem destination, Dictionary<string, string> destMember, ResolutionContext context)
        {
            var ret = new Dictionary<string, string>();
            var names = source.Names.Where(an => an.IsDeleted == false).ToList();
            foreach (var an in names)
            {
                var lcc = an.Language.NewLanguageshortname;
                if (!ret.ContainsKey(lcc))
                {
                    ret.Add(lcc, an.Name);
                }
            }

            // TODO: This is a hack since AssetNames table doesn't contain data
            if (ret.Count == 0)
                ret.Add("en-US", source.ActivityName);
            return ret;
        }
    }
}
