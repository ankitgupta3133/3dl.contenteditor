﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using TDL.ContentPlatform.Server.Database.Tables;
using Asset = TDL.ContentPlatform.Server.Database.Tables.Asset;
using AssetContent = TDL.ContentPlatform.Server.Database.Tables.AssetContent;

namespace TDL.ContentPlatform.Server.WebApi.Mappers
{
    public class AssetLocalizedDescriptionToDictionaryResolver : IValueResolver<AssetContent, Common.Models.v2.AssetContent, Dictionary<string, string>>
    {
        public Dictionary<string, string> Resolve(AssetContent source, Common.Models.v2.AssetContent destination, Dictionary<string, string> destMember, ResolutionContext context)
        {
            var ret = new Dictionary<string, string>();
            var acd = source.AssetContentDetail.Where(an => an.IsDeleted == false && an.IsEnabled == true).ToList();
            foreach (var an in acd)
            {
                var lcc = an.Language.NewLanguageshortname;
                if (!ret.ContainsKey(lcc))
                {
                    ret.Add(lcc, an.Description);
                }
            }

            return ret;
        }
    }
}
