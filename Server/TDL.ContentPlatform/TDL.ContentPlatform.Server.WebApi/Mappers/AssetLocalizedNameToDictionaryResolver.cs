﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using TDL.ContentPlatform.Server.Common.Models.v2;
using TDL.ContentPlatform.Server.Database.Tables;
using Asset = TDL.ContentPlatform.Server.Database.Tables.Asset;
using AssetContent = TDL.ContentPlatform.Server.Database.Tables.AssetContent;

namespace TDL.ContentPlatform.Server.WebApi.Mappers
{
    public class AssetLocalizedNameToDictionaryResolver : IValueResolver<Asset, Common.Models.v2.Asset, Dictionary<string, string>>
    {
        public Dictionary<string, string> Resolve(Asset source, Common.Models.v2.Asset destination, Dictionary<string, string> destMember, ResolutionContext context)
        {
            var ret = new Dictionary<string, string>();
            var assetNames = source.AssetNames.Where(an => an.IsDeleted == false).ToList();
            foreach (var an in assetNames)
            {
                var lcc = an.Language.NewLanguageshortname;
                if (!ret.ContainsKey(lcc))
                {
                    ret.Add(lcc, an.Name);
                }
            }

            // TODO: This is a hack since AssetNames table doesn't contain data
            if (ret.Count == 0)
                ret.Add("en-US", source.AssetName);
            return ret;
        }
    }
}
