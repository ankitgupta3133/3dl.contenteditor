﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using TDL.ContentPlatform.Common.Models;
using TDL.ContentPlatform.Server.Common.Models.Communication;
using TDL.ContentPlatform.Server.Common.Models.Content;
using TDL.ContentPlatform.Server.Common.Models.v2;
using TDL.ContentPlatform.Server.Database.Tables;
using Asset = TDL.ContentPlatform.Server.Database.Tables.Asset;
using AssetContent = TDL.ContentPlatform.Server.Database.Tables.AssetContent;
using AssetContentMetadata = TDL.ContentPlatform.Server.Database.Tables.AssetContentMetadata;

namespace TDL.ContentPlatform.Server.WebApi.Mappers
{
    public static class AutomapperBootstrapper
    {
        private static bool _isFirstRun = true;
        public static Mapper SetupMappings()
        {
            var config = new MapperConfiguration(cfg =>
            {
                // Note: We want it to throw exception any time we have unmapped entries, and we want to add them to ignore.
                //       With this feature we are forced to make an active decision on field mappings, hence less bugs.

                cfg.AddGlobalIgnore("IsDeleted");

                cfg.CreateMap<ClientGeneralConfig, ConfigKeyValuePair>(MemberList.Source)
                    .ForSourceMember(s => s.Id, a => a.Ignore())
                    .ForSourceMember(s => s.ClientType, a => a.Ignore());

                //cfg.CreateMap<ContentPackageFileTable, ContentPackageFile>(MemberList.Source)
                //    .ForSourceMember(s => s.ContentPackage, a => a.Ignore());

                cfg.CreateMap<CsNewPackage, ClientPackage>()
                    .ForMember(d => d.Id, opt => opt.MapFrom(s => s.Id))
                    .ForMember(d => d.Name, opt => opt.MapFrom(s => s.NewName))
                    .ForMember(d => d.Description, opt => opt.MapFrom(s => s.NewDescription))
                    .ForAllOtherMembers(opt => opt.Ignore());

                cfg.CreateMap<CsNewLanguage, ClientLanguage>()
                    .ForMember(d => d.Id, opt => opt.MapFrom(s => s.Id))
                    .ForMember(d => d.Name, opt => opt.MapFrom(s => s.NewName))
                    .ForAllOtherMembers(opt => opt.Ignore());

                cfg.CreateMap<CsNewGrade, ClientGrade>()
                    .ForMember(d => d.Id, opt => opt.MapFrom(s => s.Id))
                    .ForMember(d => d.Name, opt => opt.MapFrom(s => s.NewName))
                    .ForMember(d => d.Description, opt => opt.MapFrom(s => s.NewDescription))
                    .ForAllOtherMembers(opt => opt.Ignore());

                cfg.CreateMap<CsNewSyllabus, ClientSyllabus>()
                    .ForMember(d => d.Id, opt => opt.MapFrom(s => s.Id))
                    .ForMember(d => d.Name, opt => opt.MapFrom(s => s.NewName))
                    .ForMember(d => d.Description, opt => opt.MapFrom(s => s.NewDescription))
                    .ForAllOtherMembers(opt => opt.Ignore());

                cfg.CreateMap<CsNewStream, ClientStream>()
                    .ForMember(d => d.Id, opt => opt.MapFrom(s => s.Id))
                    .ForMember(d => d.Name, opt => opt.MapFrom(s => s.NewName))
                    .ForMember(d => d.Description, opt => opt.MapFrom(s => s.NewDescription))
                    .ForAllOtherMembers(opt => opt.Ignore());

                cfg.CreateMap<CsNewSubject, ClientSubject>()
                    .ForMember(d => d.Id, opt => opt.MapFrom(s => s.Id))
                    .ForMember(d => d.Name, opt => opt.MapFrom(s => s.NewName))
                    .ForMember(d => d.Description, opt => opt.MapFrom(s => s.NewDescription))
                    .ForAllOtherMembers(opt => opt.Ignore());

                cfg.CreateMap<CsNewTopic, ClientTopic>()
                    .ForMember(d => d.Id, opt => opt.MapFrom(s => s.Id))
                    .ForMember(d => d.Name, opt => opt.MapFrom(s => s.NewName))
                    .ForAllOtherMembers(opt => opt.Ignore());

                cfg.CreateMap<CsNewSubtopic, ClientSubtopic>()
                    .ForMember(d => d.Id, opt => opt.MapFrom(s => s.Id))
                    .ForMember(d => d.Name, opt => opt.MapFrom(s => s.NewName))
                    .ForAllOtherMembers(opt => opt.Ignore());

                cfg.CreateMap<Asset, ClientAsset>()
                    .ForMember(d => d.Id, opt => opt.MapFrom(s => s.AssetId))
                    .ForMember(d => d.Name, opt => opt.MapFrom(s => s.AssetName))
                    .ForMember(d => d.AssetType, opt => opt.MapFrom(s => s.Type))
                    .ForMember(d => d.Status, opt => opt.MapFrom(s => s.Status))
                    .ForMember(d => d.Language, opt => opt.MapFrom(s => s.Language))
                    .ForMember(d => d.Grade, opt => opt.MapFrom(s => s.Level))
                    .ForMember(d => d.Syllabus, opt => opt.MapFrom(s => s.Syllabus))
                    .ForMember(d => d.Stream, opt => opt.MapFrom(s => s.Stream))
                    .ForMember(d => d.Subject, opt => opt.MapFrom(s => s.Subject))
                    .ForMember(d => d.Topic, opt => opt.MapFrom(s => s.Topic))
                    .ForMember(d => d.Subtopic, opt => opt.MapFrom(s => s.Subtopic))
                    .ForMember(d => d.Tags, opt => opt.MapFrom(s => s.Tags))
                    .ForMember(d => d.IsQuizEnabled, opt => opt.MapFrom(s => s.IsQuizEnabled.Value))
                    .ForMember(d => d.PercentageOfLabels, opt => opt.MapFrom(s => s.PercentageOfLabels))
                    .ForMember(d => d.NumberOfAttempts, opt => opt.MapFrom(s => s.NumberOfAttempts))

                    .ForMember(d => d.GradeIds, opt => opt.MapFrom(s => s.AssetGrades.Select(sa => sa.GradeId).ToList()))
                    .ForMember(d => d.SyllabusIds, opt => opt.MapFrom(s => s.AssetSyllabus.Select(sa => sa.SyllabusId).ToList()))
                    .ForMember(d => d.StreamsIds, opt => opt.MapFrom(s => s.AssetStreams.Select(sa => sa.StreamId).ToList()))
                    .ForMember(d => d.SubjectIds, opt => opt.MapFrom(s => s.AssetSubjects.Select(sa => sa.SubjectId).ToList()))
                    .ForMember(d => d.TopicIds, opt => opt.MapFrom(s => s.AssetTopics.Select(sa => sa.TopicId).ToList()))
                    .ForMember(d => d.SubTopicIds, opt => opt.MapFrom(s => s.AssetSubTopics.Select(sa => sa.SubTopicId).ToList()))
                    .ForMember(d => d.LanguageIds, opt => opt.MapFrom(s => s.AssetLanguages.Select(sa => sa.LanguageId).ToList()))

                    .ForAllOtherMembers(opt => opt.Ignore());

                cfg.CreateMap<AssetNames, ClientAssetName>()
                    .ForMember(d => d.LanguageId, opt => opt.MapFrom(s => s.LanguageId))
                    .ForMember(d => d.Name, opt => opt.MapFrom(s => s.Name))
                    .ForAllOtherMembers(opt => opt.Ignore());

                cfg.CreateMap<AssetDetails, ClientAssetDetail>()
                    .ForMember(d => d.PackageId, opt => opt.MapFrom(s => s.PackageId))
                    .ForMember(d => d.LanguageId, opt => opt.MapFrom(s => s.LanguageId))
                    .ForMember(d => d.GradeId, opt => opt.MapFrom(s => s.GradeId))
                    .ForMember(d => d.SyllabusId, opt => opt.MapFrom(s => s.SyllabusId))
                    .ForMember(d => d.StreamId, opt => opt.MapFrom(s => s.StreamId))
                    .ForMember(d => d.SubjectId, opt => opt.MapFrom(s => s.SubjectId))
                    .ForMember(d => d.TopicId, opt => opt.MapFrom(s => s.TopicId))
                    .ForMember(d => d.SubtopicId, opt => opt.MapFrom(s => s.SubtopicId))
                    .ForAllOtherMembers(opt => opt.Ignore());

                cfg.CreateMap<AssetContent, ClientAssetContent>()
                    .ForMember(d => d.Id, opt => opt.MapFrom(s => s.Id))
                    .ForMember(d => d.FileName, opt => opt.MapFrom(s => s.FileName))
                    .ForMember(d => d.FileSize, opt => opt.MapFrom(s => s.FileSize))
                    .ForMember(d => d.Version, opt => opt.MapFrom(s => s.VersionNumber))
                    .ForMember(d => d.Thumbnail, opt => opt.MapFrom(s => s.Thumbnail))
                    .ForMember(d => d.PackageId, opt => opt.MapFrom(s => s.Asset.PackageId))
                    //.ForMember(d => d.LabelXml, opt => opt.MapFrom(s => s.LabelXML))
                    .ForMember(d => d.PlatformId, opt => opt.MapFrom(s => s.Platform))
                    //.ForMember(d => d.Url, opt => opt.Ignore())
                    .ForAllOtherMembers(opt => opt.Ignore());

                cfg.CreateMap<AssetContentDetail, ClientContentDetail>()
                    .ForMember(d => d.LanguageId, opt => opt.MapFrom(s => s.LanguageId))
                    .ForMember(d => d.LanguageShortName, opt => opt.MapFrom(s => s.LanguageShortName))
                    .ForMember(d => d.WhatsNew, opt => opt.MapFrom(s => s.WhatIsNew))
                    .ForMember(d => d.Description, opt => opt.MapFrom(s => s.Description))
                    .ForAllOtherMembers(opt => opt.Ignore());

                cfg.CreateMap<AssetContentDetail, ClientContentDescription>()
                    .ForMember(d => d.LanguageId, opt => opt.MapFrom(s => s.LanguageId))
                    .ForMember(d => d.Description, opt => opt.MapFrom(s => s.Description))
                    .ForAllOtherMembers(opt => opt.Ignore());

                cfg.CreateMap<AssetContentDetail, ClientContentWhatsNew>()
                    .ForMember(d => d.LanguageId, opt => opt.MapFrom(s => s.LanguageId))
                    .ForMember(d => d.WhatsNew, opt => opt.MapFrom(s => s.WhatIsNew))
                    .ForAllOtherMembers(opt => opt.Ignore());

                cfg.CreateMap<TdlUser, ClientUser>()
                    .ForMember(d => d.Firstname, opt => opt.MapFrom(s => s.Firstname))
                    .ForMember(d => d.Lastname, opt => opt.MapFrom(s => s.Lastname))
                    .ForAllOtherMembers(opt => opt.Ignore());

                cfg.CreateMap<MetricsItem, ClientMetricLog>()
                    .ForMember(d => d.MetricsType, opt => opt.MapFrom(s => s.MetricsType.ToString()))
                    .ForAllOtherMembers(opt => opt.Ignore());

                //Activities
                cfg.CreateMap<MstActivity, Common.Models.v2.Activities>()
                    .ForMember(d => d.ActivityId, opt => opt.MapFrom(s => s.ActivityId))
                    .ForMember(d => d.ActivityName, opt => opt.MapFrom(s => s.ActivityName))
                    .ForAllOtherMembers(opt => opt.Ignore());

                cfg.CreateMap<Database.Tables.Activities, Common.Models.v2.ActivityItem>()
                    .ForMember(d => d.ItemId, opt => opt.MapFrom(s => s.ActivityId))
                    .ForMember(d => d.ItemName, opt => opt.MapFrom(s => s.ActivityName))
                    .ForMember(d => d.Duration, opt => opt.MapFrom(s => s.Duration))
                    .ForMember(d => d.IsModelSpecific, opt => opt.MapFrom(s => s.IsModelSpecific))
                    .ForMember(d => d.IsVisible, opt => opt.MapFrom(s => s.IsEnabled))
                    .ForMember(d => d.LocalizedName, opt => opt.ResolveUsing<ActivityLocalizedNameToDictionaryResolver>())
                    .ForAllOtherMembers(opt => opt.Ignore());

                cfg.CreateMap<ActivityDetails, ActivityDetail>()
                    .ForMember(d => d.PackageId, opt => opt.MapFrom(s => s.PackageId))
                    .ForMember(d => d.LanguageId, opt => opt.MapFrom(s => s.LanguageId))
                    .ForMember(d => d.GradeId, opt => opt.MapFrom(s => s.GradeId))
                    .ForMember(d => d.SyllabusId, opt => opt.MapFrom(s => s.SyllabusId))
                    .ForMember(d => d.StreamId, opt => opt.MapFrom(s => s.StreamId))
                    .ForMember(d => d.SubjectId, opt => opt.MapFrom(s => s.SubjectId))
                    .ForMember(d => d.TopicId, opt => opt.MapFrom(s => s.TopicId))
                    .ForMember(d => d.SubtopicId, opt => opt.MapFrom(s => s.SubtopicId))
                    .ForAllOtherMembers(opt => opt.Ignore());

                cfg.CreateMap<Database.Tables.Activities, Common.Models.v2.ClassificationItem>()
                    .ForMember(d => d.ClassificationId, opt => opt.MapFrom(s => s.ActivityId))
                    .ForMember(d => d.ClassificationName, opt => opt.MapFrom(s => s.ActivityName))
                    .ForMember(d => d.LocalizedName, opt => opt.ResolveUsing<ClassificationLocalizedNameToDictionaryResolver>())
                    .ForAllOtherMembers(opt => opt.Ignore());

                cfg.CreateMap<Database.Tables.ClassificationDetails, Common.Models.v2.PropertyItem>()
                    .ForMember(d => d.ItemId, opt => opt.MapFrom(s => s.Id))
                    .ForMember(d => d.PropertyId, opt => opt.MapFrom(s => s.PropertyId))
                    .ForMember(d => d.PropertyName, opt => opt.MapFrom(s => s.MstProperty.PropertyName))
                    .ForAllOtherMembers(opt => opt.Ignore());

                cfg.CreateMap<Database.Tables.ClassificationAssets, Common.Models.v2.ModelItem>()
                    .ForMember(d => d.ItemDetailId, opt => opt.MapFrom(s => s.Id))
                    .ForMember(d => d.AssetId, opt => opt.MapFrom(s => s.AssetId))
                    .ForMember(d => d.AssetName, opt => opt.MapFrom(s => s.Asset.AssetName))
                    .ForMember(d => d.AssetContentId, opt => opt.MapFrom(s => s.AssetContentId))
                    .ForAllOtherMembers(opt => opt.Ignore());

                // API v2

                cfg.CreateMap<CsNewPackageGrade, Grade>()
                    .ForMember(d => d.Id, opt => opt.MapFrom(s => s.NewGrade.Id))
                    .ForMember(d => d.Name, opt => opt.MapFrom(s => s.NewGrade.NewName))
                    .ForMember(d => d.Streams, opt => opt.MapFrom(s => s.NewGrade.CsNewGradeStreams))
                    .ForMember(d => d.Subjects, opt => opt.MapFrom(s => s.NewGrade.CsNewGradeSubjects))
                    .ForAllOtherMembers(opt => opt.Ignore());

                cfg.CreateMap<CsNewGradeStream, Stream>()
                    .ForMember(d => d.Id, opt => opt.MapFrom(s => s.NewStream.Id))
                    .ForMember(d => d.Name, opt => opt.MapFrom(s => s.NewStream.NewName))
                    .ForAllOtherMembers(opt => opt.Ignore());
                cfg.CreateMap<CsNewGradeSubject, Subject>()
                    .ForMember(d => d.Id, opt => opt.MapFrom(s => s.NewSubject.Id))
                    .ForMember(d => d.Name, opt => opt.MapFrom(s => s.NewSubject.NewName))
                    .ForMember(d => d.Topics, opt => opt.MapFrom(s => s.NewSubject.CsNewSubjectTopics))
                    .ForMember(d => d.ThumbnailHash, opt => opt.Ignore())
                    .ForMember(d => d.ThumbnailCdnUrls, opt => opt.Ignore())
                    .ForAllOtherMembers(opt => opt.Ignore());
                cfg.CreateMap<CsNewSubjectTopic, Topic>()
                    .ForMember(d => d.Id, opt => opt.MapFrom(s => s.NewTopic.Id))
                    .ForMember(d => d.Name, opt => opt.MapFrom(s => s.NewTopic.NewName))
                    .ForMember(d => d.Subtopics, opt => opt.MapFrom(s => s.NewTopic.CsNewTopicSubtopics))
                    .ForMember(d => d.ThumbnailHash, opt => opt.Ignore())
                    .ForMember(d => d.ThumbnailCdnUrls, opt => opt.Ignore())
                    .ForAllOtherMembers(opt => opt.Ignore());
                cfg.CreateMap<CsNewTopicSubtopic, Subtopic>()
                    .ForMember(d => d.Id, opt => opt.MapFrom(s => s.NewSubtopic.Id))
                    .ForMember(d => d.Name, opt => opt.MapFrom(s => s.NewSubtopic.NewName))
                    .ForMember(d => d.ThumbnailHash, opt => opt.Ignore())
                    .ForMember(d => d.ThumbnailCdnUrls, opt => opt.Ignore())
                    .ForAllOtherMembers(opt => opt.Ignore());
                cfg.CreateMap<CsNewLanguage, Language>()
                    .ForMember(d => d.Id, opt => opt.MapFrom(s => s.Id))
                    .ForMember(d => d.Name, opt => opt.MapFrom(s => s.NewName))
                    .ForMember(d => d.CultureCode, opt => opt.MapFrom(s => s.NewLanguageshortname))
                    .ForAllOtherMembers(opt => opt.Ignore());
                cfg.CreateMap<ApkDetail, Apk>()
                    .ForMember(d => d.ApkName, opt => opt.MapFrom(s => s.ApkName))
                    .ForMember(d => d.ApkVersion, opt => opt.MapFrom(s => s.ApkVersion))
                    .ForMember(d => d.IsMandatory, opt => opt.MapFrom(s => s.IsMandatory))
                    .ForMember(d => d.ApkCdnUrls, opt => opt.Ignore())
                    .ForAllOtherMembers(opt => opt.Ignore());
                cfg.CreateMap<MstCompound, Common.Models.v2.Compound>()
                    .ForMember(d => d.CompoundId, opt => opt.MapFrom(s => s.CompoundId))
                    .ForMember(d => d.CompoundName, opt => opt.MapFrom(s => s.CompundName))
                    .ForMember(d => d.CommonName, opt => opt.MapFrom(s => s.CommonName))
                    .ForMember(d => d.Color, opt => opt.MapFrom(s => s.Color))
                    .ForMember(d => d.Material, opt => opt.MapFrom(s => s.MstMaterial.MaterialName))
                    .ForMember(d => d.Discription, opt => opt.MapFrom(s => s.Discription))
                    .ForMember(d => d.WikiLink, opt => opt.MapFrom(s => s.WikiLink))
                    .ForMember(d => d.BondedAssetId, opt => opt.MapFrom(s => s.BondedAssetId))
                    .ForMember(d => d.BondedAsset, opt => opt.MapFrom(s => s.BondedAssets.AssetName))
                    .ForMember(d => d.SphericalAssetId, opt => opt.MapFrom(s => s.SphericalAssetId))
                    .ForMember(d => d.SphericalAsset, opt => opt.MapFrom(s => s.SphericalAssets.AssetName))
                    .ForMember(d => d.Elements, opt => opt.MapFrom(s => s.CompoundElement))
                    .ForAllOtherMembers(opt => opt.Ignore());

                cfg.CreateMap<CompoundElement, Common.Models.v2.Element>()
                    .ForMember(d => d.ElementId, opt => opt.MapFrom(s => s.ElementId))
                    .ForMember(d => d.ElementName, opt => opt.MapFrom(s => s.MstElement.ElementName))
                    .ForAllOtherMembers(opt => opt.Ignore());

                cfg.CreateMap<MstElement, Common.Models.v2.Element>()
                    .ForMember(d => d.ElementId, opt => opt.MapFrom(s => s.ElementId))
                    .ForMember(d => d.ElementName, opt => opt.MapFrom(s => s.ElementName))
                    .ForAllOtherMembers(opt => opt.Ignore());

                cfg.CreateMap<Asset, Common.Models.v2.Asset>()
                    .ForMember(d => d.Id, opt => opt.MapFrom(s => s.AssetId))
                    .ForMember(d => d.Name, opt => opt.MapFrom(s => s.AssetName))
                    .ForMember(d => d.Type, opt => opt.MapFrom(s => s.Type))
                    .ForMember(d => d.AssetContents, opt => opt.MapFrom(s => s.AssetContent))
                    .ForMember(d => d.LocalizedName, opt => opt.ResolveUsing<AssetLocalizedNameToDictionaryResolver>())
                    .ForAllOtherMembers(opt => opt.Ignore());

                cfg.CreateMap<AssetContent, Common.Models.v2.AssetContent>()
                    .ForMember(d => d.Id, opt => opt.MapFrom(s => s.Id))
                    .ForMember(d => d.Version, opt => opt.MapFrom(s => s.VersionNumber))
                    .ForMember(d => d.FileHash, opt => opt.MapFrom(s => s.FileName ))
                    .ForMember(d => d.FileSize, opt => opt.MapFrom(s => s.FileSize ))
                    .ForMember(d => d.FileCdnUrls, opt => opt.MapFrom(s => new List<CdnUrl>() { new CdnUrl() {Url = s.FileName} }))
                    //.ForMember(d => d.FileHash, opt => opt.MapFrom(s => s.FileName))
                    //.ForMember(d => d.FileUrl, opt => opt.MapFrom(s => s.FileName))
                    //.ForMember(d => d.FileSize, opt => opt.MapFrom(s => s.FileSize))
                    .ForMember(d => d.ThumbnailHash, opt => opt.MapFrom(s => s.Thumbnail))
                    .ForMember(d => d.ThumbnailCdnUrls, opt => opt.MapFrom(s => new List<CdnUrl>() { new CdnUrl() { Url = s.Thumbnail } }))
                    .ForMember(d => d.BackgroundHash, opt => opt.MapFrom(s => s.Background))
                    .ForMember(d => d.BackgroundCdnUrls, opt => opt.MapFrom(s => new List<CdnUrl>() { new CdnUrl() { Url = s.Background } }))
                    .ForMember(d => d.PlatformId, opt => opt.MapFrom(s => s.Platform))
                    .ForMember(d => d.IsVisible, opt => opt.MapFrom(s => s.IsEnabled))
                    .ForMember(d => d.LocalizedDescription, opt => opt.ResolveUsing<AssetLocalizedDescriptionToDictionaryResolver>())
                    //.ForMember(d => d.Metadata, opt => opt.MapFrom(s => s.XmlcontentModel.FirstOrDefault(m => m.IsDeleted == false && m.IsEnabled == true)))
                    .ForMember(d => d.Metadata, opt => opt.ResolveUsing<XmlcontentModelToMetadataResolver>())
                    .ForAllOtherMembers(opt => opt.Ignore());

                cfg.CreateMap<AssetContentMetadata, AssetContentMetadata>();

                //cfg.CreateMap<XmlcontentModel, Common.Models.v2.AssetContentMetadata>()
                //    .ForPath(d => d.Model3DLabels.Items, opt => opt.MapFrom(s => s.XmlcontentLabel))
                //    .ForAllOtherMembers(opt => opt.Ignore());
                //cfg.CreateMap<XmlcontentLabel, Common.Models.v2.Model3DLabelItem>()
                //    .ForMember(d => d.HighlightColor, opt => opt.MapFrom(s => s.Highlight))
                //    .ForMember(d => d.OriginalColor, opt => opt.MapFrom(s => s.Original))
                //    .ForMember(d => d.Position, opt => opt.MapFrom(s => new Model3DVector3(s.XPosition, s.YPosition, s.ZPosition)))
                //    .ForMember(d => d.Rotation, opt => opt.MapFrom(s => new Model3DVector3(s.XRotation, s.YRotation, s.ZRotation)))
                //    .ForMember(d => d.LocalizedText, opt => opt.MapFrom(s => new Dictionary<string, string>()
                //        {
                //            { "nb-NO", s.No },
                //            { "en-US", s.En }
                //        }))
                //    .ForAllOtherMembers(opt => opt.Ignore());
                //cfg.CreateMap<ContentPackageTable, ContentPackage>(MemberList.Source);

            });


            // Verify mappings on first run
            if (_isFirstRun)
            {
                config.AssertConfigurationIsValid();
                _isFirstRun = false;
            }

            return new Mapper(config);
        }

    }
}
