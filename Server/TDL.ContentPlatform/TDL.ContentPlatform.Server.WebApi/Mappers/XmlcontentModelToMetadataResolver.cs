﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Newtonsoft.Json;
using TDL.ContentPlatform.Server.Common.Models.v2;
using TDL.ContentPlatform.Server.Database;
using AssetContent = TDL.ContentPlatform.Server.Database.Tables.AssetContent;

namespace TDL.ContentPlatform.Server.WebApi.Mappers
{
    public class XmlcontentModelToMetadataResolver : IValueResolver<AssetContent, Common.Models.v2.AssetContent, Common.Models.v2.AssetContentMetadata>
    {
        public Common.Models.v2.AssetContentMetadata Resolve(AssetContent source, Common.Models.v2.AssetContent destination, Common.Models.v2.AssetContentMetadata destMember, ResolutionContext context)
        {
            var ret = new Common.Models.v2.AssetContentMetadata();

            // We have metadata in Metadata-table, so we will only use that
            if (source.Metadata?.Count > 0)
            {
                foreach (var m in source.Metadata)
                {
                    // NOTE: For every known type we need to recognize it here. That's the drawback of splitting Metadata up into subsections.
                    if (m.Type == nameof(ret.Model3DLabels))
                        ret.Model3DLabels = JsonConvert.DeserializeObject<Model3DLabels>(m.Json);
                }

                return ret;
            }

            // Legacy, if no Metadata in Metadata-table we assume AssetContent is still using old LabelXml system
            var cm = source.XmlcontentModel.FirstOrDefault(); // m => m.IsDeleted == false && m.IsEnabled==true
            if (cm == null)
                return ret;

            ret.Model3DLabels.Items = cm.XmlcontentLabel
                .Where(l =>  l.IsEnabled == true) // l.IsDeleted == false &&
                .Select(label => new Model3DLabelItem()
            {
                LabelId = label.LabelId,
                HighlightColor = label.Highlight,
                OriginalColor = label.Original,
                Position = new Model3DVector3(label.XPosition, label.YPosition, label.ZPosition),
                Rotation = new Model3DVector3(label.XRotation, label.YRotation, label.ZRotation),
                Scale = new Model3DVector3(label.XScale, label.YScale, label.ZScale),
                LocalizedText = new Dictionary<string, string>()
                {
                    {"nb-NO", label.No},
                    {"en-US", label.En},
                    {"nn-NO", label.Nn},
                    //{"da-DK", label.Dk},
                    //{"de-DE", label.De},
                    //{"es-ES", label.Es},
                    //{"fr-FR", label.Fr},
                    //{"sv-SE", label.Se}
                }
            }).ToList();

            return ret;
        }
    }
}