﻿using Microsoft.Extensions.Logging;

namespace TDL.ContentPlatform.Server.WebApi.Models
{
    public static class LogMessageType
    {
        public static readonly EventId Login = new EventId(100, "Login");
        public static readonly EventId LoginError = new EventId(101, "Login ErrorMessage");
        public static readonly EventId LoginDenied = new EventId(102, "Login Denied");
        public static readonly EventId LoginSuccess = new EventId(103, "Login Success");

        public static readonly EventId Register = new EventId(110, "Register User");
        public static readonly EventId ChangePassword = new EventId(130, "Change Password");
        public static readonly EventId ResetPassword = new EventId(120, "Reset Password");
        public static readonly EventId EmailError = new EventId(140, "Error while sending email");
    }
}
