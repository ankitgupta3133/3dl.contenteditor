﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using TDL.ContentPlatform.Server.WebApi.ConfigModels;
using TDL.ContentPlatform.Server.WebApi.Controllers;
using TDL.ContentPlatform.Server.WebApi.Models;

namespace TDL.ContentPlatform.Server.WebApi.Serviceproviders
{
    public class EmailServiceprovider
    {
        private readonly ILogger<ConfigController> _logger;
        private readonly ManagedConfig _managedConfig;

        public EmailServiceprovider(ILogger<ConfigController> logger, IOptions<ManagedConfig> managedConfig)
        {
            _logger = logger;
            _managedConfig = managedConfig.Value;
        }

        //public void SendEmail(string destinationAddress, string subject, string body)
        //{
            //_managedConfig.Email.ServerName
        //}

        public bool SendMail(string destinationAddress, string Subject, string MessageBody, bool IsHTML = true, string BCC = "", string CC = "")
        {
            bool _return = false;
            try
            {
                var message = new MailMessage();
                message.To.Add(new MailAddress(destinationAddress));

                if (!string.IsNullOrEmpty(BCC))
                    message.Bcc.Add(new MailAddress(BCC));

                if (!string.IsNullOrEmpty(CC))
                    message.CC.Add(new MailAddress(CC));

                message.From = new MailAddress("shyam@3dl.no", "3DL");
                message.Subject = Subject;
                message.Body = MessageBody;
                message.IsBodyHtml = IsHTML;
                message.BodyEncoding = System.Text.Encoding.GetEncoding("utf-8");
                message.Headers.Add("Message-Id", String.Concat("<", DateTime.Now.ToString("yyMMdd"), ".", DateTime.Now.ToString("HHmmss"), _managedConfig.Email.ServerName.ToString()));

                using (var smtp = new SmtpClient())
                {
                    smtp.Credentials = new NetworkCredential
                    {
                        UserName = _managedConfig.Email.Username.ToString(),
                        Password = _managedConfig.Email.Password.ToString()
                    };
                    smtp.Host = _managedConfig.Email.Address.ToString();
                    smtp.Port = Convert.ToInt16(_managedConfig.Email.PortNumber.ToString());
                    smtp.EnableSsl = Convert.ToBoolean(_managedConfig.Email.EnableSsl.ToString());

                    //Add this line to bypass the certificate validation
                    System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate (object s,
                        System.Security.Cryptography.X509Certificates.X509Certificate certificate,
                        System.Security.Cryptography.X509Certificates.X509Chain chain,
                        System.Net.Security.SslPolicyErrors sslPolicyErrors)
                    {
                        return true;
                    };

                    smtp.Send(message);
                    _return = true;
                }
            }
            catch (Exception exception)
            {
                _logger.LogInformation(LogMessageType.EmailError, exception, $"{Subject}\t{destinationAddress}");
                _return = false;
            }

            return _return;
        }
    }
}
