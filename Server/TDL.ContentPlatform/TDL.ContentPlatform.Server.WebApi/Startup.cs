﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Swashbuckle.AspNetCore.Swagger;
using TDL.ContentPlatform.Server.Common.Enums;
using TDL.ContentPlatform.Server.Database;
using TDL.ContentPlatform.Server.Database.Tables;
using TDL.ContentPlatform.Server.WebApi.ConfigModels;
using TDL.ContentPlatform.Server.WebApi.Mappers;
using TDL.ContentPlatform.Server.WebApi.Serviceproviders;
using TDL.ContentPlatform.Server.WebApi.Swagger;
using IConfigurationProvider = Microsoft.Extensions.Configuration.IConfigurationProvider;

namespace TDL.ContentPlatform.Server.WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<ManagedConfig>(Configuration);
            services.AddSingleton(Configuration);

            services.AddMemoryCache();

            //var jwtConfiguration = Configuration.GetSection(nameof(JwtConfiguration)).Get<JwtConfiguration>();

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            var managedConfig = Configuration.Get<ManagedConfig>();
            services.AddSingleton(managedConfig);

            // Get options from app settings
            services.AddScoped<EmailServiceprovider, EmailServiceprovider>();

            services.AddMvc(options =>
            {
                options.Filters.Add(new RequireHttpsAttribute());
            }).SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            // API versioning
            services.AddApiVersioning(
                o =>
                {
                    // Read more about API versioning at https://www.hanselman.com/blog/ASPNETCoreRESTfulWebAPIVersioningMadeEasy.aspx
                    o.AssumeDefaultVersionWhenUnspecified = true;
                    o.DefaultApiVersion = new ApiVersion(1, 0);
                });

            // Database
            var contentPlatformConnection = Configuration.GetConnectionString("ContentPlatformConnection");
            services.AddDbContext<ContentDbContext>(options => options
                    // PS! Remember to also update ContentDbConnectionMockContextFactory in TDL.ContentPlatform.Server.Database
                    .UseSqlServer(contentPlatformConnection, x =>
                    {
                        x.MigrationsHistoryTable(tableName: "__EFMigrationsHistory_ContentPlatform");
                        x.EnableRetryOnFailure(maxRetryCount: 5, maxRetryDelay: TimeSpan.FromSeconds(10), errorNumbersToAdd: new int[0]);
                    })
                    .UseQueryTrackingBehavior(QueryTrackingBehavior.TrackAll),
                    ServiceLifetime.Scoped // If we plan to use parallel context we may need it to be transient
            );


            // Authentication
            services.AddIdentity<TdlUser, TdlRole>(options =>
                {
                    // User settings
                    options.User.RequireUniqueEmail = true;
                    options.Password = managedConfig.Security.PasswordPolicy ?? options.Password;
                })
                    .AddEntityFrameworkStores<ContentDbContext>()
                    .AddDefaultTokenProviders();

            services.AddAuthentication(o =>
                {
                    // https://github.com/openiddict/openiddict-core/issues/436
                    o.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(options =>
                {
                    options.IncludeErrorDetails = true;
                    options.RequireHttpsMetadata = true;
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ClockSkew = TimeSpan.FromMinutes(5),
                        ValidateIssuer = true,
                        SaveSigninToken = true,
                        ValidateAudience = true,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,
                        ValidIssuer = managedConfig.JwtConfiguration.Issuer,
                        ValidAudience = managedConfig.JwtConfiguration.DefaultAudience,
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(managedConfig.JwtConfiguration.Secret))
                    };
                });

            services.AddAuthorization(options =>
            {
                options.AddPolicy("Student",
                    policy =>
                    {
                        policy.AuthenticationSchemes.Add(JwtBearerDefaults.AuthenticationScheme);
                        policy.RequireAuthenticatedUser();
                        policy.RequireClaim(ClaimTypes.Role, new string[] { KnownUserRolesEnum.Student.ToString() });

                    });

                options.AddPolicy("Administrator",
                    policy =>
                    {
                        policy.AuthenticationSchemes.Add(JwtBearerDefaults.AuthenticationScheme);
                        policy.RequireAuthenticatedUser();
                        policy.RequireClaim(ClaimTypes.Role, new string[] { KnownUserRolesEnum.Administrator.ToString() });
                    });
            });

            // AutoMapper
            //services.AddAutoMapper(); // Using profiles with IoC doesn't allow us to validate
            var mapper = AutomapperBootstrapper.SetupMappings();
            services.AddSingleton<IMapper>(mapper);

            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = "3DL API",
                    Description = "API for 3DL client software",
                    TermsOfService = "None",
                    Contact = new Contact
                    {
                        Name = "Shyam Venkatraman",
                        Email = "shyam@3dl.no",
                        Url = string.Empty
                    },
                    License = new License
                    {
                        Name = "No rights of use without explicit contract",
                        Url = "https://3dl.com/"
                    }
                });

                c.AddSecurityDefinition("Bearer", new ApiKeyScheme()
                {
                    Description = "Authorization header using the Bearer scheme",
                    Name = "Authorization",
                    In = "header"
                });

                c.DocumentFilter<SwaggerSecurityRequirementsDocumentFilter>();
                c.OperationFilter<FormFileSwaggerFilter>();
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/ErrorMessage");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseAuthentication();

            //app.UseStaticFiles();
            app.UseMvc();

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), 
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "3DL API V1");
                c.RoutePrefix = string.Empty;
            });
        }
    }
}
