﻿using System;

namespace TDL.ContentPlatform.Server.WebApi.Utils
{
    public class CacheKeys
    {
        public static readonly CacheUtil.CacheKeyData Platforms = new CacheUtil.CacheKeyData(nameof(Platforms), TimeSpan.FromDays(1));
        public static readonly CacheUtil.CacheKeyData EntityThumbnail = new CacheUtil.CacheKeyData(nameof(EntityThumbnail), TimeSpan.FromHours(1));
        public static readonly CacheUtil.CacheKeyData QuizDetails = new CacheUtil.CacheKeyData(nameof(QuizDetails), TimeSpan.FromHours(1));
        public static readonly CacheUtil.CacheKeyData ContentModel = new CacheUtil.CacheKeyData(nameof(ContentModel), TimeSpan.FromHours(1));
        public static readonly CacheUtil.CacheKeyData QuizLabelDetailsContentLabel = new CacheUtil.CacheKeyData(nameof(QuizLabelDetailsContentLabel), TimeSpan.FromHours(1));
        public static readonly CacheUtil.CacheKeyData QuizLabelDetails = new CacheUtil.CacheKeyData(nameof(QuizLabelDetails), TimeSpan.FromHours(1));
        public static readonly CacheUtil.CacheKeyData PuzzleDetails = new CacheUtil.CacheKeyData(nameof(PuzzleDetails), TimeSpan.FromHours(1));
    }
}
