﻿using System;
using Microsoft.Extensions.Caching.Memory;

namespace TDL.ContentPlatform.Server.WebApi.Utils
{


    public static class CacheUtil
    {
        public class CacheKeyData
        {
            public readonly string Key;
            public readonly TimeSpan Timespan;

            public CacheKeyData(string key, TimeSpan timespan)
            {
                Key = key;
                Timespan = timespan;
            }
        }
        public static T GetOrAddCache<T>(this IMemoryCache memoryCache, string key, Func<T> action, TimeSpan? expireTime = null)
        {
            T cacheEntry;

            // Look for cache key.
            if (!memoryCache.TryGetValue(key, out cacheEntry))
            {
                // We don't have it, so execute action to retrieve it
                cacheEntry = action.Invoke();

                var cacheEntryOptions = new MemoryCacheEntryOptions()
                    // Keep in cache for this time, reset time if accessed.
                    .SetSlidingExpiration(expireTime.HasValue ? expireTime.Value : TimeSpan.FromMinutes(5));

                // Save data in cache.
                memoryCache.Set(key, cacheEntry, cacheEntryOptions);
            }

            return cacheEntry;
        }
        public static T GetOrAddCache<T>(this IMemoryCache memoryCache, CacheKeyData key, string param, Func<T> action)
        {
            if (param != null)
                return memoryCache.GetOrAddCache(key.Key + ":" + param, action, key.Timespan);
            return memoryCache.GetOrAddCache(key.Key, action, key.Timespan);
        }
    }
}