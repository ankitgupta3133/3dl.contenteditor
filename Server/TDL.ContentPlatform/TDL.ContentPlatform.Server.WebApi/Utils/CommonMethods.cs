﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TDL.ContentPlatform.Server.Common.Utils;
using TDL.ContentPlatform.Server.Database;

namespace TDL.ContentPlatform.Server.WebApi.Utils
{
    public class CommonMethods
    {
        public static Dictionary<string, string> GetErrorLocal(ErrorCode errorCode, ContentDbContext context)
        {
            Dictionary<string, string> localizedError = new Dictionary<string, string>();

            var errorMessages = (from E in context.ApplicationMessage
                                 join L in context.CsNewLanguage on E.LanguageId equals L.NewLanguageid
                                 where E.IsDeleted == false && E.MessageCode == errorCode.ToString() && E.MessageType == (byte)MessageType.Error
                                 select new { LangCode = L.NewLanguageshortname, E.Message });

            if (errorMessages.Count() > 0)
            {
                foreach (var error in errorMessages)
                {
                    localizedError.Add(error.LangCode, error.Message);
                }
            }

            return localizedError;
        }
    }
}
