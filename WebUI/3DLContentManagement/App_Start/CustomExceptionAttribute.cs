﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace _3DLContentManagement.App_Start
{
    public class CustomExceptionAttribute : HandleErrorAttribute
    {
        public override void OnException(ExceptionContext filterContext)
        {
            try
            {
                string requestBody = "", Action = "", Controller = "";

                try
                {
                    requestBody = filterContext.HttpContext.Request.Form.ToString();
                    Action = filterContext.RouteData.Values["action"].ToString();
                    Controller = filterContext.RouteData.Values["controller"].ToString();
                }
                catch (Exception)
                {
                }

                StringBuilder sbHeader = new StringBuilder();
                sbHeader.AppendLine(filterContext.RequestContext.HttpContext.Request.Headers.ToString());
                //StaticMethods.LogException(SessionHelper.LoginCode.ToString(), Action, Controller, filterContext.Exception.Message, filterContext.RequestContext.HttpContext.Request.RawUrl.ToString(), requestBody, sbHeader.ToString());
                string filePath = AppDomain.CurrentDomain.BaseDirectory + @"Logfile\Error.txt";

                using (StreamWriter writer = new StreamWriter(filePath, true))
                {
                    writer.WriteLine("Action :" + Action  + Environment.NewLine +
                        "Controller :" + Controller + Environment.NewLine +
                        "Message :" + filterContext.Exception.Message.ToString() + Environment.NewLine + 
                        "Exception :" + filterContext.Exception.ToString() + "<br/>" + Environment.NewLine +
                        "URL :" + filterContext.RequestContext.HttpContext.Request.RawUrl.ToString() + Environment.NewLine +
                        "Request :" + requestBody + Environment.NewLine +
                        "Header :" + sbHeader.ToString() + Environment.NewLine +
                        "Date :" + DateTime.Now.ToString());
                    writer.WriteLine(Environment.NewLine + "------------------------------------------------------------------------------------------------" + Environment.NewLine);
                }

                filterContext.RouteData.Values.Add("Error", filterContext.Exception.Message);
                filterContext.Result = new RedirectToRouteResult(
                                           new RouteValueDictionary(new { controller = "Error", action = "Error", message = filterContext.Exception.Message }));

                filterContext.ExceptionHandled = true;
                //erase any output already generated
                filterContext.HttpContext.Response.Clear();
            }
            catch (Exception)
            {
            }
        }
    }
}