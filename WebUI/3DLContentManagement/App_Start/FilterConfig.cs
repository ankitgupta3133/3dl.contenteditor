﻿using _3DLContentManagement.App_Start;
using System.Web;
using System.Web.Mvc;

namespace _3DLContentManagement
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            //filters.Add(new HandleErrorAttribute());
            filters.Add(new CustomExceptionAttribute());
            filters.Add(new RequireHttpsAttribute());
        }
    }
}
