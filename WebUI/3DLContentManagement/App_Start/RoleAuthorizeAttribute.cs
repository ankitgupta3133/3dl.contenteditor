﻿using System;
using System.Web.Configuration;
using System.Web.Mvc;

namespace _3DLContentManagement
{
    public class RoleAuthorizeAttribute : AuthorizeAttribute
    {
        private string redirectUrl = "";
        public RoleAuthorizeAttribute() : base()
        {

        }

        public RoleAuthorizeAttribute(string redirectUrl) : base()
        {
            this.redirectUrl = redirectUrl;
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (filterContext.HttpContext.Request.IsAuthenticated)
            {
                string authUrl = this.redirectUrl; //passed from attribute

                //if null, get it from config
                if (String.IsNullOrEmpty(authUrl))
                    authUrl = WebConfigurationManager.AppSettings["RolesAuthRedirectUrl"].ToString();

                if (!String.IsNullOrEmpty(authUrl))
                    filterContext.HttpContext.Response.Redirect(authUrl);
            }

            //else do normal process
            base.HandleUnauthorizedRequest(filterContext);
        }
    }
}