﻿using _3DLContentManagement.Database;
using _3DLContentManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace _3DLContentManagement.App_Start
{
    public class SessionExpireAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpContext ctx = HttpContext.Current;

            // check  IsAuthenticated here
            if (!HttpContext.Current.Request.IsAuthenticated)
            {
                HttpContext.Current.Session.Abandon();
                FormsAuthentication.SignOut();
                filterContext.Result = new RedirectResult("/Account/Login");
                return;
            }

            #region "This code will execute, only when user IsAuthenticated and session is null. Reassign session "
            if (HttpContext.Current.Request.IsAuthenticated)
            {
                if (HttpContext.Current.Session["UserID"] == null)
                {                    
                    //Store user id
                    Guid userID = Guid.Parse(HttpContext.Current.User.Identity.Name);

                    using (DB_Entities db = new DB_Entities())
                    {
                        var obj = db.UserProfile.Where(a => a.UserId== userID).FirstOrDefault();
                        if(obj!=null)
                        {
                            HttpContext.Current.Session["UserID"] = obj.UserId.ToString();
                            HttpContext.Current.Session["UserName"] = obj.UserName.ToString();
                            HttpContext.Current.Session["Name"] = obj.FirstName.ToString() + " " + obj.LastName;
                        }
                    }
                     
                }//End session check
            }//End Authenticated check

            #endregion
            base.OnActionExecuting(filterContext);
        }
    }
}