﻿using _3DLContentManagement.Database;
using System;
using System.Threading.Tasks;
using System.Web;
using TDL.ContentPlatform.Server.Database.Tables;

namespace _3DLContentManagement.Common
{
    public static class ActivityLogger
    {
        public static async Task AddActivityLog(int Id, string strAction, string strTitle)
        {
            string UserId = HttpContext.Current.Session["UserID"].ToString();

            try
            {
                using (DB_Entities db = new DB_Entities())
                {
                    //if (UserId != null)
                    //    strTitle = strTitle + ". This action has been performed by " + HttpContext.Current.Session["Name"].ToString();

                    db.UserLogs.Add(new UserLogs()
                    {
                        UserId = Guid.Parse(UserId),
                        UserName = HttpContext.Current.Session["Name"].ToString(),
                        AssetId = Id,
                        Action = strAction,
                        Description = strTitle,
                        AddedOn = DateTime.UtcNow,
                        IsDeleted = false
                    });

                    await db.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void AddActivityLogs(int Id, string strAction, string strTitle)
        {
            string UserId = HttpContext.Current.Session["UserID"].ToString();

            try
            {
                using (DB_Entities db = new DB_Entities())
                {
                    //if (UserId != null)
                    //    strTitle = strTitle + ". This action has been performed by " + HttpContext.Current.Session["Name"].ToString();

                    db.UserLogs.Add(new UserLogs()
                    {
                        UserId = Guid.Parse(UserId),
                        UserName = HttpContext.Current.Session["Name"].ToString(),
                        AssetId = Id,
                        Action = strAction,
                        Description = strTitle,
                        AddedOn = DateTime.UtcNow,
                        IsDeleted = false
                    });

                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}