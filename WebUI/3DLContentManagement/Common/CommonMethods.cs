﻿using _3DLContentManagement.Database;
using _3DLContentManagement.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using RT.Comb;
using TDL.ContentPlatform.Server.Database.Tables;
using System.Web.Mvc;

namespace _3DLContentManagement.Common
{
    public static class CommonMethods
    {
        /// <summary>
        /// Generates a Random Password
        /// respecting the given strength requirements.
        /// </summary>
        /// <param name="opts">A valid PasswordOptions object
        /// containing the password strength requirements.</param>
        /// <returns>A random password</returns>
        public static string GenerateRandomPassword(PasswordOptions opts = null)
        {
            if (opts == null) opts = new PasswordOptions()
            {
                RequiredLength = 8,
                RequiredUniqueChars = 4,
                RequireDigit = true,
                RequireLowercase = true,
                RequireNonAlphanumeric = true,
                RequireUppercase = true
            };

            string[] randomChars = new[] {
                "ABCDEFGHJKLMNOPQRSTUVWXYZ",    // uppercase 
                "abcdefghijkmnopqrstuvwxyz",    // lowercase
                "0123456789",                   // digits
                "!@$?_-"                        // non-alphanumeric
            };
            Random rand = new Random(Environment.TickCount);
            List<char> chars = new List<char>();

            if (opts.RequireUppercase)
                chars.Insert(rand.Next(0, chars.Count),
                    randomChars[0][rand.Next(0, randomChars[0].Length)]);

            if (opts.RequireLowercase)
                chars.Insert(rand.Next(0, chars.Count),
                    randomChars[1][rand.Next(0, randomChars[1].Length)]);

            if (opts.RequireDigit)
                chars.Insert(rand.Next(0, chars.Count),
                    randomChars[2][rand.Next(0, randomChars[2].Length)]);

            if (opts.RequireNonAlphanumeric)
                chars.Insert(rand.Next(0, chars.Count),
                    randomChars[3][rand.Next(0, randomChars[3].Length)]);

            for (int i = chars.Count; i < opts.RequiredLength || chars.Distinct().Count() < opts.RequiredUniqueChars; i++)
            {
                string rcs = randomChars[rand.Next(0, randomChars.Length)];
                chars.Insert(rand.Next(0, chars.Count),
                    rcs[rand.Next(0, rcs.Length)]);
            }

            return new string(chars.ToArray());
        }

        public static string MD5Hash(string text)
        {
            MD5 md5 = new MD5CryptoServiceProvider();

            //compute hash from the bytes of text
            md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(text));

            //get hash result after compute it
            byte[] result = md5.Hash;

            StringBuilder strBuilder = new StringBuilder();
            for (int i = 0; i < result.Length; i++)
            {
                //change it into 2 hexadecimal digits
                //for each byte
                strBuilder.Append(result[i].ToString("x2"));
            }

            return strBuilder.ToString();
        }

        public static string Encrypt(string clearText)
        {
            string EncryptionKey = WebConfigurationManager.AppSettings["AESKey"].ToString();
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText;
        }

        public static long GetDirectorySize(string fullDirectoryPath)
        {
            long startDirectorySize = 0;
            if (!Directory.Exists(fullDirectoryPath))
                return startDirectorySize; //Return 0 while Directory does not exist.

            var currentDirectory = new DirectoryInfo(fullDirectoryPath);

            //Add size of files in the Current Directory to main size.
            currentDirectory.GetFiles().ToList().ForEach(f => startDirectorySize += f.Length);

            //Loop on Sub Direcotries in the Current Directory and Calculate it's files size.
            currentDirectory.GetDirectories().ToList()
                .ForEach(d => startDirectorySize += GetDirectorySize(d.FullName));

            //Return full Size of this Directory.
            return startDirectorySize;
        }

        public static string GetBytesReadable(long i)
        {
            // Get absolute value
            long absolute_i = (i < 0 ? -i : i);
            // Determine the suffix and readable value
            string suffix;
            double readable;
            if (absolute_i >= 0x1000000000000000) // Exabyte
            {
                suffix = "EB";
                readable = (i >> 50);
            }
            else if (absolute_i >= 0x4000000000000) // Petabyte
            {
                suffix = "PB";
                readable = (i >> 40);
            }
            else if (absolute_i >= 0x10000000000) // Terabyte
            {
                suffix = "TB";
                readable = (i >> 30);
            }
            else if (absolute_i >= 0x40000000) // Gigabyte
            {
                suffix = "GB";
                readable = (i >> 20);
            }
            else if (absolute_i >= 0x100000) // Megabyte
            {
                suffix = "MB";
                readable = (i >> 10);
            }
            else if (absolute_i >= 0x400) // Kilobyte
            {
                suffix = "KB";
                readable = i;
            }
            else
            {
                return i.ToString("0 B"); // Byte
            }
            // Divide by 1024 to get fractional value
            readable = (readable / 1024);
            // Return formatted number with suffix
            return readable.ToString("0.### ") + suffix;
        }

        public static string GenerateXml(int? Id)
        {
            string destFile = string.Empty;
            string fileName = string.Empty;

            using (DB_Entities db = new DB_Entities())
            {
                var modelObj = db.XmlcontentModel.Where(x => x.ModelId == Id).Include(x => x.AssetContent).ThenInclude(x => x.Asset).FirstOrDefault();

                //save the file to server folder
                //string fullPath = WebConfigurationManager.AppSettings["PublishPath"].ToString() + modelObj.AssetContent.Asset.PackageId;
                string fullPath = WebConfigurationManager.AppSettings["PublishPath"].ToString();

                //Check for the target path if it is exist
                if (!Directory.Exists(fullPath))
                {
                    Directory.CreateDirectory(fullPath);
                }

                if (modelObj.AssetContent.IsDirectory == true)
                {
                    fileName = modelObj.AssetContent.FileName + ".xml";
                    fullPath = fullPath + "\\" + modelObj.AssetContent.FileName;
                }
                else
                {
                    //int lastIndxDot = modelObj.AssetContent.FileName.LastIndexOf('.');
                    fileName = Path.GetFileNameWithoutExtension(modelObj.AssetContent.FileName) + ".xml";
                    //fileName = Path.GetFileNameWithoutExtension(modelObj.AssetContent.FileName) + "_" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".xml";
                }
                //var doc = db.sp_GetXMLDetail(Id).ToList(); //need to include              
                var doc = DataAccess.GetXML(Id);
                string fullText = string.Empty;
                foreach (GetLabel text in doc)
                {
                    fullText += text.Label;
                }

                System.IO.DirectoryInfo info = new DirectoryInfo(fullPath);
                if (info.Exists)
                {
                    System.IO.FileInfo file = new FileInfo(Path.Combine(fullPath, fileName));
                    if (System.IO.File.Exists(file.FullName))
                    {
                        //System.IO.File.SetAttributes(file.FullName, FileAttributes.Normal);
                        file.Delete();
                    }

                    // Create a file to write to.
                    using (StreamWriter sw = System.IO.File.CreateText(file.FullName))
                    {
                        sw.WriteLine(fullText);
                    }
                }

                //throw new Exception("Needs fixing. This must be named something else and it must be json. Talk to Tedd.");
                modelObj.AssetContent.LabelXml = fileName;
                db.SaveChanges();

                return fileName;
            }
        }

        public static async Task<string> CheckAssetType(string types)
        {
            try
            {
                string strAsset = string.Empty;
                using (DB_Entities db = new DB_Entities())
                {
                    List<string> listTypes = types.Split(',').ToList();
                    foreach (string type in listTypes)
                    {
                        if (type != string.Empty && type.Trim() != "")
                        {
                            strAsset = strAsset + type.Trim() + ", ";
                            var obj = db.MstFileType.Where(a => a.TypeName.Equals(type.Trim()) && a.IsDeleted == false).FirstOrDefault();
                            if (obj == null)
                            {
                                MstFileType newTypeObj = new MstFileType()
                                {
                                    TypeName = type.Trim(),
                                    IsActive = true,
                                    CreatedBy = new Guid(HttpContext.Current.Session["UserID"].ToString()),
                                    CreatedOn = DateTime.Now,
                                    IsDeleted = false
                                };
                                db.MstFileType.Add(newTypeObj);
                                await db.SaveChangesAsync();

                                string strType = string.Format("Asset Type titled '{0}' added from add update asset", type);
                                await ActivityLogger.AddActivityLog(newTypeObj.FileTypeId, "Asset Type added", strType);
                            }
                        }
                    }
                }
                return strAsset.Substring(0, strAsset.Length - 2);
            }
            catch (Exception)
            {
                // throw;
                return types;
            }
        }

        public static async Task<string> CheckLanguage(string languages)
        {
            try
            {
                string language = string.Empty;
                using (DB_Entities db = new DB_Entities())
                {
                    if (languages == string.Empty || languages == null || languages == "" || languages.Equals("all"))
                    {
                        string allLanguage = string.Empty;
                        var languageList = db.CsNewLanguage.Select(x => x.NewName).ToList();
                        foreach (string lang in languageList)
                        {
                            allLanguage += lang + ", ";
                        }
                        languages = allLanguage.Substring(0, allLanguage.Length - 2);
                    }

                    List<string> listLanguages = languages.Split(',').ToList();
                    foreach (string lang in listLanguages)
                    {
                        if (lang != string.Empty && lang.Trim() != "")
                        {
                            language = language + lang.Trim() + ", ";
                            var obj = db.CsNewLanguage.Where(a => a.NewName.Equals(lang.Trim())).FirstOrDefault();
                            if (obj == null)
                            {
                                CsNewLanguage newLanguageObj = new CsNewLanguage()
                                {
                                    NewName = lang.Trim(),
                                    Id = Provider.Sql.Create(),
                                    Createdby = new Guid(HttpContext.Current.Session["UserID"].ToString()),
                                    Createdon = DateTime.Now
                                };
                                db.CsNewLanguage.Add(newLanguageObj);
                                await db.SaveChangesAsync();

                                string strLanguage = string.Format("Language titled '{0}' added from add update asset", lang);
                                //await ActivityLogger.AddActivityLog(newLanguageObj.Id, "Language added", strLanguage);
                            }
                        }
                    }
                }
                return language.Substring(0, language.Length - 2);
            }
            catch (Exception)
            {
                // throw;
                return languages;
            }
        }

        public static async Task<string> CheckLevel(string levels)
        {
            try
            {
                string strLevels = string.Empty;
                using (DB_Entities db = new DB_Entities())
                {
                    List<string> listLevels = levels.Split(',').ToList();
                    foreach (string level in listLevels)
                    {
                        if (level != string.Empty && level.Trim() != "")
                        {
                            strLevels = strLevels + level.Trim() + ", ";
                            var obj = db.CsNewGrade.Where(a => a.NewName.Equals(level.Trim())).FirstOrDefault();
                            if (obj == null)
                            {
                                CsNewGrade newLevelObj = new CsNewGrade()
                                {
                                    NewName = level.Trim(),
                                    Id = Provider.Sql.Create(),
                                    Createdby = new Guid(HttpContext.Current.Session["UserID"].ToString()),
                                    Createdon = DateTime.Now
                                };
                                db.CsNewGrade.Add(newLevelObj);
                                await db.SaveChangesAsync();

                                string strLevel = string.Format("Level titled '{0}' added from add update asset", level);
                                //await ActivityLogger.AddActivityLog(newLevelObj.Id, "Level added", strLevel);
                            }
                        }
                    }
                }
                return strLevels.Substring(0, strLevels.Length - 2);
            }
            catch (Exception)
            {
                // throw;
                return levels;
            }
        }

        public static async Task<string> CheckSyllabus(string syllabus)
        {
            try
            {
                string strSyllabus = string.Empty;
                using (DB_Entities db = new DB_Entities())
                {
                    List<string> listSyllabus = syllabus.Split(',').ToList();
                    foreach (string objSyllabus in listSyllabus)
                    {
                        if (objSyllabus != string.Empty && objSyllabus.Trim() != "")
                        {
                            strSyllabus = strSyllabus + objSyllabus.Trim() + ", ";
                            var obj = db.CsNewSyllabus.Where(a => a.NewName.Equals(objSyllabus.Trim())).FirstOrDefault();
                            if (obj == null)
                            {
                                CsNewSyllabus newSyllabusObj = new CsNewSyllabus()
                                {
                                    NewName = objSyllabus.Trim(),
                                    Id = Provider.Sql.Create(),
                                    Createdby = new Guid(HttpContext.Current.Session["UserID"].ToString()),
                                    Createdon = DateTime.Now
                                };
                                db.CsNewSyllabus.Add(newSyllabusObj);
                                await db.SaveChangesAsync();

                                string strActivity = string.Format("Syllabus titled '{0}' added from add update asset", objSyllabus);
                                //await ActivityLogger.AddActivityLog(newSyllabusObj.SyllabusId, "Syllabus added", strActivity);
                            }
                        }
                    }
                }
                return strSyllabus.Substring(0, strSyllabus.Length - 2);
            }
            catch (Exception)
            {
                // throw;
                return syllabus;
            }
        }

        public static async Task<string> CheckSubject(string subjects)
        {
            try
            {
                string strSubs = string.Empty;
                using (DB_Entities db = new DB_Entities())
                {
                    List<string> listSubjects = subjects.Split(',').ToList();
                    foreach (string subject in listSubjects)
                    {
                        if (subject != string.Empty && subject.Trim() != "")
                        {
                            strSubs = strSubs + subject.Trim() + ", ";
                            var obj = db.CsNewSubject.Where(a => a.NewName.Equals(subject.Trim())).FirstOrDefault();
                            if (obj == null)
                            {
                                CsNewSubject newSubjectObj = new CsNewSubject()
                                {
                                    NewName = subject.Trim(),
                                    Id = Provider.Sql.Create(),
                                    Createdby = new Guid(HttpContext.Current.Session["UserID"].ToString()),
                                    Createdon = DateTime.Now
                                };
                                db.CsNewSubject.Add(newSubjectObj);
                                await db.SaveChangesAsync();

                                string strSubject = string.Format("Subject titled '{0}' added from add update asset", subject);
                                //await ActivityLogger.AddActivityLog(newSubjectObj.SubjectId, "Subject added", strSubject);
                            }
                        }
                    }
                }
                return strSubs.Substring(0, strSubs.Length - 2);
            }
            catch (Exception)
            {
                // throw;
                return subjects;
            }
        }

        public static async Task<string> CheckTopic(string topics)
        {
            try
            {
                string strTopics = string.Empty;
                using (DB_Entities db = new DB_Entities())
                {
                    List<string> listTopics = topics.Split(',').ToList();
                    foreach (string topic in listTopics)
                    {
                        if (topic != string.Empty && topic.Trim() != "")
                        {
                            strTopics = strTopics + topic.Trim() + ", ";
                            var obj = db.CsNewTopic.Where(a => a.NewName.Equals(topic.Trim())).FirstOrDefault();
                            if (obj == null)
                            {
                                CsNewTopic newTopicObj = new CsNewTopic()
                                {
                                    NewName = topic.Trim(),
                                    Id = Provider.Sql.Create(),
                                    Createdby = new Guid(HttpContext.Current.Session["UserID"].ToString()),
                                    Createdon = DateTime.Now
                                };
                                db.CsNewTopic.Add(newTopicObj);
                                await db.SaveChangesAsync();

                                string strTopic = string.Format("Topic titled '{0}' added from add update asset", topic);
                                //await ActivityLogger.AddActivityLog(newTopicObj.TopicId, "Topic added", strTopic);
                            }
                        }
                    }
                }
                return strTopics.Substring(0, strTopics.Length - 2);
            }
            catch (Exception)
            {
                // throw;
                return topics;
            }
        }

        public static async Task<string> CheckSubtopic(string subtopics)
        {
            try
            {
                string strSubtopics = string.Empty;
                using (DB_Entities db = new DB_Entities())
                {
                    List<string> listSubtopics = subtopics.Split(',').ToList();
                    foreach (string subtopic in listSubtopics)
                    {
                        if (subtopic != string.Empty && subtopic.Trim() != "")
                        {
                            strSubtopics = strSubtopics + subtopic.Trim() + ", ";
                            var obj = db.CsNewSubtopic.Where(a => a.NewName.Equals(subtopic.Trim())).FirstOrDefault();
                            if (obj == null)
                            {
                                CsNewSubtopic newSubtopicObj = new CsNewSubtopic()
                                {
                                    NewName = subtopic.Trim(),
                                    Id = Provider.Sql.Create(),
                                    Createdby = new Guid(HttpContext.Current.Session["UserID"].ToString()),
                                    Createdon = DateTime.Now
                                };
                                db.CsNewSubtopic.Add(newSubtopicObj);
                                await db.SaveChangesAsync();

                                string strSubtopic = string.Format("Subtopic titled '{0}' added from add update asset", subtopic);
                                //await ActivityLogger.AddActivityLog(newSubtopicObj.SubtopicId, "Subtopic added", strSubtopic);
                            }
                        }
                    }
                }
                return strSubtopics.Substring(0, strSubtopics.Length - 2);
            }
            catch (Exception)
            {
                // throw;
                return subtopics;
            }
        }

        public static async Task<string> CheckTag(string tags)
        {
            try
            {
                string strTags = string.Empty;
                using (DB_Entities db = new DB_Entities())
                {
                    List<string> listTags = tags.Split(',').ToList();
                    foreach (string tag in listTags)
                    {
                        if (tag != string.Empty && tag.Trim() != "")
                        {
                            strTags = strTags + tag.Trim() + ", ";
                            var obj = db.MstTag.Where(a => a.TagName.Equals(tag.Trim()) && a.IsDeleted == false).FirstOrDefault();
                            if (obj == null)
                            {
                                MstTag newTagObj = new MstTag()
                                {
                                    TagName = tag.Trim(),
                                    IsActive = true,
                                    CreatedBy = new Guid(HttpContext.Current.Session["UserID"].ToString()),
                                    CreatedOn = DateTime.Now,
                                    IsDeleted = false
                                };
                                db.MstTag.Add(newTagObj);
                                await db.SaveChangesAsync();

                                string strTag = string.Format("Tag titled '{0}' added from add update asset", tag);
                                await ActivityLogger.AddActivityLog(newTagObj.TagId, "Tag added", strTag);
                            }
                        }
                    }
                }
                return strTags.Substring(0, strTags.Length - 2);
            }
            catch (Exception)
            {
                // throw;
                return tags;
            }
        }

        public static async Task<string> CheckProperty(string properties)
        {
            try
            {
                string strProperties = string.Empty;
                using (DB_Entities db = new DB_Entities())
                {
                    List<string> listProperties = properties.Split(',').ToList();
                    foreach (string property in listProperties)
                    {
                        if (property != string.Empty && property.Trim() != "")
                        {
                            strProperties = strProperties + property.Trim() + ", ";
                            var obj = db.MstProperty.Where(a => a.PropertyName.Equals(property.Trim()) && a.IsDeleted == false).FirstOrDefault();
                            if (obj == null)
                            {
                                MstProperty newPropertyObj = new MstProperty()
                                {
                                    PropertyName = property.Trim(),
                                    IsActive = true,
                                    CreatedBy = new Guid(HttpContext.Current.Session["UserID"].ToString()),
                                    CreatedOn = DateTime.Now,
                                    IsDeleted = false
                                };
                                db.MstProperty.Add(newPropertyObj);
                                await db.SaveChangesAsync();

                                string strProperty = string.Format("Property titled '{0}' added from add update asset", property);
                                await ActivityLogger.AddActivityLog(newPropertyObj.PropertyId, "Property added", strProperty);
                            }
                        }
                    }
                }
                return strProperties.Substring(0, strProperties.Length - 2);
            }
            catch (Exception)
            {
                // throw;
                return properties;
            }
        }

        public static async Task<string> CheckStream(string streams)
        {
            try
            {
                string txtStream = string.Empty;
                using (DB_Entities db = new DB_Entities())
                {
                    List<string> listStream = streams.Split(',').ToList();
                    foreach (string stream in listStream)
                    {
                        if (stream != string.Empty && stream.Trim() != "")
                        {
                            txtStream = txtStream + stream.Trim() + ", ";
                            var obj = db.CsNewStream.Where(a => a.NewName.Equals(stream.Trim())).FirstOrDefault();
                            if (obj == null)
                            {
                                CsNewStream objStream = new CsNewStream()
                                {
                                    NewName = stream.Trim(),
                                    Id = Provider.Sql.Create(),
                                    Createdby = new Guid(HttpContext.Current.Session["UserID"].ToString()),
                                    Createdon = DateTime.Now
                                };
                                db.CsNewStream.Add(objStream);
                                await db.SaveChangesAsync();

                                string activity = string.Format("Stream titled '{0}' added from add update asset", stream);
                                //await ActivityLogger.AddActivityLog(objStream.StreamId, "Stream added", activity);
                            }
                        }
                    }
                }
                return txtStream.Substring(0, txtStream.Length - 2);
            }
            catch (Exception)
            {
                // throw;
                return streams;
            }
        }

        public static async Task<int> GetSubtopicID(string subtopics)
        {
            try
            {
                int SubtopicID = 0;
                string strSubtopics = string.Empty;
                using (DB_Entities db = new DB_Entities())
                {
                    List<string> listSubtopics = subtopics.Split(',').ToList();
                    foreach (string subtopic in listSubtopics)
                    {
                        if (subtopic != string.Empty && subtopic.Trim() != "")
                        {
                            strSubtopics = strSubtopics + subtopic.Trim() + ", ";
                            var obj = db.MstSubtopic.Where(a => a.SubtopicName.Equals(subtopic.Trim()) && a.IsDeleted == false).FirstOrDefault();
                            if (obj == null)
                            {
                                MstSubtopic newSubtopicObj = new MstSubtopic()
                                {
                                    SubtopicName = subtopic.Trim(),
                                    IsActive = true,
                                    CreatedBy = new Guid(HttpContext.Current.Session["UserID"].ToString()),
                                    CreatedOn = DateTime.Now,
                                    IsDeleted = false

                                };
                                db.MstSubtopic.Add(newSubtopicObj);
                                await db.SaveChangesAsync();

                                SubtopicID = newSubtopicObj.SubtopicId;
                                string strSubtopic = string.Format("Subtopic titled '{0}' added", subtopic);
                                await ActivityLogger.AddActivityLog(newSubtopicObj.SubtopicId, "Subtopic added", strSubtopic);
                            }
                            else
                            {
                                SubtopicID = obj.SubtopicId;
                            }
                        }
                    }
                }
                return SubtopicID;
            }
            catch (Exception)
            {
                // throw;
                return 0;
            }
        }

        public static async Task<string> GetSubtopicDetails(string subtopics)
        {
            try
            {
                string strSubtopics = string.Empty;
                using (DB_Entities db = new DB_Entities())
                {
                    if (!string.IsNullOrEmpty(subtopics))
                    {
                        List<string> listSubtopics = subtopics.Split(',').ToList();
                        foreach (string subtopic in listSubtopics)
                        {
                            //var subtopicDesc = await Task.Run(() => db.sp_GetSubtopicDetails(subtopic.Trim()).ToList()); //need to include
                            //strSubtopics = strSubtopics + subtopicDesc.First() + ", ";
                            var subtopicDesc = await Task.Run(() => DataAccess.GetSubtopicDetails(subtopic.Trim()));
                            strSubtopics = strSubtopics + subtopicDesc + ", ";
                        }
                    }
                }
                return strSubtopics.Substring(0, strSubtopics.Length - 2);
            }
            catch (Exception)
            {
                //throw;
                return subtopics;
            }
        }

        public static async Task<Guid> GetPackageID(string packages)
        {
            Guid packageID = Guid.Empty;

            try
            {
                string txtPackage = string.Empty;
                using (DB_Entities db = new DB_Entities())
                {
                    List<string> listPackage = packages.Split(',').ToList();
                    foreach (string package in listPackage)
                    {
                        if (package != string.Empty && package.Trim() != "")
                        {
                            txtPackage = txtPackage + package.Trim() + ", ";
                            var obj = db.CsNewPackage.Where(a => a.NewName.Equals(package.Trim())).FirstOrDefault();
                            if (obj == null)
                            {
                                CsNewPackage objPackage = new CsNewPackage()
                                {
                                    NewName = package.Trim(),
                                    Id = Provider.Sql.Create(),
                                    Createdby = new Guid(HttpContext.Current.Session["UserID"].ToString()),
                                    Createdon = DateTime.Now
                                };
                                db.CsNewPackage.Add(objPackage);
                                await db.SaveChangesAsync();

                                packageID = objPackage.Id;
                                string activity = string.Format("Package titled '{0}' added", package);
                                //await ActivityLogger.AddActivityLog(objPackage.Id, "Package added", activity);
                            }
                            else
                            {
                                packageID = obj.Id;
                            }
                        }
                    }
                }
                return packageID;
            }
            catch (Exception)
            {
                // throw;
                return packageID;
            }
        }

        public static async Task AddUpdateAssetMasterData(int assetId)
        {
            DB_Entities db = new DB_Entities();
            var asset = db.Asset.Where(x => x.AssetId == assetId && x.IsDeleted == false).FirstOrDefault();

            await AddUpdateAssetLanguages(assetId, asset.Language);

            await AddUpdateAssetGrades(assetId, asset.Level);

            if (!string.IsNullOrEmpty(asset.Syllabus))
                await AddUpdateAssetSyllabus(assetId, asset.Syllabus);

            if (!string.IsNullOrEmpty(asset.Stream))
                await AddUpdateAssetStreams(assetId, asset.Stream);

            await AddUpdateAssetSubjects(assetId, asset.Subject);

            await AddUpdateAssetTopics(assetId, asset.Topic);

            if (!string.IsNullOrEmpty(asset.Subtopic))
                await AddUpdateAssetSubtopics(assetId, asset.Subtopic);
        }

        public static async Task AddUpdateAssetLanguages(int assetId, string languages)
        {
            DB_Entities db = new DB_Entities();
            var al = db.AssetLanguages.Where(x => x.AssetId == assetId).ToList();
            db.RemoveRange(al);
            db.SaveChanges();

            List<string> listLanguages = languages.Split(',').ToList();

            foreach (string lang in listLanguages)
            {
                if (lang != string.Empty && lang.Trim() != "")
                {
                    var obj = db.CsNewLanguage.Where(a => a.NewName.Equals(lang.Trim())).FirstOrDefault();
                    if (obj != null)
                    {
                        AssetLanguages newLanguageObj = new AssetLanguages()
                        {
                            AssetId = assetId,
                            LanguageId = obj.Id
                        };
                        db.AssetLanguages.Add(newLanguageObj);
                        await db.SaveChangesAsync();
                    }
                }
            }
        }

        public static async Task AddUpdateAssetGrades(int assetId, string grades)
        {
            DB_Entities db = new DB_Entities();
            var ag = db.AssetGrades.Where(x => x.AssetId == assetId).ToList();
            db.RemoveRange(ag);
            db.SaveChanges();

            List<string> listGradeds = grades.Split(',').ToList();

            foreach (string gr in listGradeds)
            {
                if (gr != string.Empty && gr.Trim() != "")
                {
                    var obj = db.CsNewGrade.Where(a => a.NewName.Equals(gr.Trim())).FirstOrDefault();
                    if (obj != null)
                    {
                        AssetGrades newGradeObj = new AssetGrades()
                        {
                            AssetId = assetId,
                            GradeId = obj.Id
                        };
                        db.AssetGrades.Add(newGradeObj);
                        await db.SaveChangesAsync();
                    }
                }
            }
        }

        public static async Task AddUpdateAssetSyllabus(int assetId, string syllabus)
        {
            DB_Entities db = new DB_Entities();
            var sa = db.AssetSyllabus.Where(x => x.AssetId == assetId).ToList();
            db.RemoveRange(sa);
            db.SaveChanges();

            List<string> listSyllabus = syllabus.Split(',').ToList();

            foreach (string sy in listSyllabus)
            {
                if (sy != string.Empty && sy.Trim() != "")
                {
                    var obj = db.CsNewSyllabus.Where(a => a.NewName.Equals(sy.Trim())).FirstOrDefault();
                    if (obj != null)
                    {
                        AssetSyllabus newSyllabusObj = new AssetSyllabus()
                        {
                            AssetId = assetId,
                            SyllabusId = obj.Id
                        };
                        db.AssetSyllabus.Add(newSyllabusObj);
                        await db.SaveChangesAsync();
                    }
                }
            }
        }

        public static async Task AddUpdateAssetStreams(int assetId, string streams)
        {
            DB_Entities db = new DB_Entities();
            var sa = db.AssetStreams.Where(x => x.AssetId == assetId).ToList();
            db.RemoveRange(sa);
            db.SaveChanges();

            List<string> listStreams = streams.Split(',').ToList();

            foreach (string st in listStreams)
            {
                if (st != string.Empty && st.Trim() != "")
                {
                    var obj = db.CsNewStream.Where(a => a.NewName.Equals(st.Trim())).FirstOrDefault();
                    if (obj != null)
                    {
                        AssetStreams newStreamObj = new AssetStreams()
                        {
                            AssetId = assetId,
                            StreamId = obj.Id
                        };
                        db.AssetStreams.Add(newStreamObj);
                        await db.SaveChangesAsync();
                    }
                }
            }
        }

        public static async Task AddUpdateAssetSubjects(int assetId, string subjects)
        {
            DB_Entities db = new DB_Entities();
            var sa = db.AssetSubjects.Where(x => x.AssetId == assetId).ToList();
            db.RemoveRange(sa);
            db.SaveChanges();

            List<string> listSubjects = subjects.Split(',').ToList();

            foreach (string sub in listSubjects)
            {
                if (sub != string.Empty && sub.Trim() != "")
                {
                    var obj = db.CsNewSubject.Where(a => a.NewName.Equals(sub.Trim())).FirstOrDefault();
                    if (obj != null)
                    {
                        AssetSubjects newSubjectObj = new AssetSubjects()
                        {
                            AssetId = assetId,
                            SubjectId = obj.Id
                        };
                        db.AssetSubjects.Add(newSubjectObj);
                        await db.SaveChangesAsync();
                    }
                }
            }
        }

        public static async Task AddUpdateAssetTopics(int assetId, string topics)
        {
            DB_Entities db = new DB_Entities();
            var at = db.AssetTopics.Where(x => x.AssetId == assetId).ToList();
            db.RemoveRange(at);
            db.SaveChanges();

            List<string> listTopics = topics.Split(',').ToList();

            foreach (string top in listTopics)
            {
                if (top != string.Empty && top.Trim() != "")
                {
                    var obj = db.CsNewTopic.Where(a => a.NewName.Equals(top.Trim())).FirstOrDefault();
                    if (obj != null)
                    {
                        AssetTopics newTopicObj = new AssetTopics()
                        {
                            AssetId = assetId,
                            TopicId = obj.Id
                        };
                        db.AssetTopics.Add(newTopicObj);
                        await db.SaveChangesAsync();
                    }
                }
            }
        }

        public static async Task AddUpdateAssetSubtopics(int assetId, string subtopics)
        {
            DB_Entities db = new DB_Entities();
            var at = db.AssetSubTopics.Where(x => x.AssetId == assetId).ToList();
            db.RemoveRange(at);
            db.SaveChanges();

            List<string> listSubtopics = subtopics.Split(',').ToList();

            foreach (string st in listSubtopics)
            {
                if (st != string.Empty && st.Trim() != "")
                {
                    var obj = db.CsNewSubtopic.Where(a => a.NewName.Equals(st.Trim())).FirstOrDefault();
                    if (obj != null)
                    {
                        AssetSubTopics newSubtopicObj = new AssetSubTopics()
                        {
                            AssetId = assetId,
                            SubTopicId = obj.Id
                        };
                        db.AssetSubTopics.Add(newSubtopicObj);
                        await db.SaveChangesAsync();
                    }
                }
            }
        }

        public static string GetAssetNames(int assetId)
        {
            string ret = string.Empty;
            try
            {
                using (DB_Entities db = new DB_Entities())
                {
                    var names = db.AssetNames.Where(x => x.AssetId == assetId && x.IsDeleted == false).OrderBy(x => x.Id).ToList();
                    foreach (var obj in names)
                    {
                        ret = ret + obj.Name.Trim() + ", ";
                    }
                }
                return ret.Substring(0, ret.Length - 2);
            }
            catch (Exception)
            {
                return ret;
            }
        }

        public static AssetDetailModel GetAssetDetails(int assetId)
        {
            AssetDetailModel ret = new AssetDetailModel();
            try
            {
                using (DB_Entities db = new DB_Entities())
                {
                    var details = db.AssetDetails.Where(x => x.AssetId == assetId && x.IsDeleted == false).OrderBy(x => x.Id).ToList();
                    foreach (var obj in details)
                    {
                        ret.Package = ret.Package + db.CsNewPackage.Where(x => x.NewPackageid == obj.PackageId).Select(x => x.NewName).FirstOrDefault() + ", ";
                        ret.Grade = ret.Grade + db.CsNewGrade.Where(x => x.NewGradeid == obj.GradeId).Select(x => x.NewName).FirstOrDefault() + ", ";
                        ret.Language = ret.Language + db.CsNewLanguage.Where(x => x.NewLanguageid == obj.LanguageId).Select(x => x.NewName).FirstOrDefault() + ", ";
                        ret.Syllabus = ret.Syllabus + db.CsNewSyllabus.Where(x => x.NewSyllabusid == obj.SyllabusId).Select(x => x.NewName).FirstOrDefault() + ", ";
                        ret.Stream = ret.Stream + db.CsNewStream.Where(x => x.NewStreamid == obj.StreamId).Select(x => x.NewName).FirstOrDefault() + ", ";
                        ret.Subject = ret.Subject + db.CsNewSubject.Where(x => x.NewSubjectid == obj.SubjectId).Select(x => x.NewName).FirstOrDefault() + ", ";
                        ret.Topic = ret.Topic + db.CsNewTopic.Where(x => x.NewTopicid == obj.TopicId).Select(x => x.NewName).FirstOrDefault() + ", ";
                        ret.Subtopic = ret.Subtopic + db.CsNewSubtopic.Where(x => x.NewSubtopicid == obj.SubtopicId).Select(x => x.NewName).FirstOrDefault() + ", ";
                    }
                }
                return ret;
            }
            catch (Exception)
            {
                return ret;
            }
        }

        public static string GetAssetLanguages(int assetId)
        {
            string ret = string.Empty;
            try
            {
                using (DB_Entities db = new DB_Entities())
                {
                    var details = db.AssetDetails.Where(x => x.AssetId == assetId && x.IsDeleted == false).OrderBy(x => x.Id).ToList();
                    foreach (var obj in details)
                    {
                        ret = ret + db.CsNewLanguage.Where(x => x.NewLanguageid == obj.LanguageId).Select(x => x.NewName).FirstOrDefault() + ", ";
                    }
                }
                return ret.Substring(0, ret.Length - 2);
            }
            catch (Exception)
            {
                return ret;
            }
        }

        public static string GetAssetGrades(int assetId)
        {
            string ret = string.Empty;
            try
            {
                using (DB_Entities db = new DB_Entities())
                {
                    var details = db.AssetDetails.Where(x => x.AssetId == assetId && x.IsDeleted == false).OrderBy(x => x.Id).ToList();
                    foreach (var obj in details)
                    {
                        ret = ret + db.CsNewGrade.Where(x => x.NewGradeid == obj.GradeId).Select(x => x.NewName).FirstOrDefault() + ", ";
                    }
                }
                return ret.Substring(0, ret.Length - 2);
            }
            catch (Exception)
            {
                return ret;
            }
        }

        public static string GetAssetSyllabus(int assetId)
        {
            string ret = string.Empty;
            try
            {
                using (DB_Entities db = new DB_Entities())
                {
                    var details = db.AssetDetails.Where(x => x.AssetId == assetId && x.IsDeleted == false).OrderBy(x => x.Id).ToList();
                    foreach (var obj in details)
                    {
                        ret = ret + db.CsNewSyllabus.Where(x => x.NewSyllabusid == obj.SyllabusId).Select(x => x.NewName).FirstOrDefault() + ", ";
                    }
                }
                return ret.Substring(0, ret.Length - 2);
            }
            catch (Exception)
            {
                return ret;
            }
        }

        public static string GetAssetStreams(int assetId)
        {
            string ret = string.Empty;
            try
            {
                using (DB_Entities db = new DB_Entities())
                {
                    var details = db.AssetDetails.Where(x => x.AssetId == assetId && x.IsDeleted == false).OrderBy(x => x.Id).ToList();
                    foreach (var obj in details)
                    {
                        ret = ret + db.CsNewStream.Where(x => x.NewStreamid == obj.StreamId).Select(x => x.NewName).FirstOrDefault() + ", ";
                    }
                }
                return ret.Substring(0, ret.Length - 2);
            }
            catch (Exception)
            {
                return ret;
            }
        }

        public static string GetAssetSubjects(int assetId)
        {
            string ret = string.Empty;
            try
            {
                using (DB_Entities db = new DB_Entities())
                {
                    var details = db.AssetDetails.Where(x => x.AssetId == assetId && x.IsDeleted == false).OrderBy(x => x.Id).ToList();
                    foreach (var obj in details)
                    {
                        ret = ret + db.CsNewSubject.Where(x => x.NewSubjectid == obj.SubjectId).Select(x => x.NewName).FirstOrDefault() + ", ";
                    }
                }
                return ret.Substring(0, ret.Length - 2);
            }
            catch (Exception)
            {
                return ret;
            }
        }

        public static string GetAssetTopics(int assetId)
        {
            string ret = string.Empty;
            try
            {
                using (DB_Entities db = new DB_Entities())
                {
                    var details = db.AssetDetails.Where(x => x.AssetId == assetId && x.IsDeleted == false).OrderBy(x => x.Id).ToList();
                    foreach (var obj in details)
                    {
                        ret = ret + db.CsNewTopic.Where(x => x.NewTopicid == obj.TopicId).Select(x => x.NewName).FirstOrDefault() + ", ";
                    }
                }
                return ret.Substring(0, ret.Length - 2);
            }
            catch (Exception)
            {
                return ret;
            }
        }

        public static string GetAssetSubtopics(int assetId)
        {
            string ret = string.Empty;
            try
            {
                using (DB_Entities db = new DB_Entities())
                {
                    var details = db.AssetDetails.Where(x => x.AssetId == assetId && x.IsDeleted == false).OrderBy(x => x.Id).ToList();
                    foreach (var obj in details)
                    {
                        ret = ret + db.CsNewSubtopic.Where(x => x.NewSubtopicid == obj.SubtopicId).Select(x => x.NewName).FirstOrDefault() + ", ";
                    }
                }
                return ret.Substring(0, ret.Length - 2);
            }
            catch (Exception)
            {
                return ret;
            }
        }

        public static IEnumerable<SelectListItem> GetAssetTags(int assetId)
        {
            using (DB_Entities db = new DB_Entities())
            {
                return (from tag in db.MstTag
                        join assetTag in db.AssetTags.Where(x => x.AssetId == assetId) on tag.TagId equals assetTag.MstTagId into assetTags
                        from assetTag in assetTags.DefaultIfEmpty()
                        where tag.IsDeleted == false
                        select new SelectListItem()
                        {
                            Text = tag.TagName,
                            Selected = assetTag != null,
                            Value = tag.TagId.ToString()
                        }).ToList().OrderByDescending(x => x.Selected).ThenBy(X => X.Text);
            }
        }

        public static bool SaveAssetTags(int assetId, string[] tags)
        {
            try
            {
                using (DB_Entities db = new DB_Entities())
                {

                    foreach (var tag in tags)
                    {
                        int tagId = Convert.ToInt32(tag);

                        AssetTags assetTag = new AssetTags()
                        {
                            AssetId = assetId,
                            MstTagId = tagId
                        };
                        db.AssetTags.Add(assetTag);
                    }
                    db.SaveChanges();
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }

        public static IEnumerable<SelectListItem> GetAssetProperties(int assetId)
        {
            using (DB_Entities db = new DB_Entities())
            {
                return (from property in db.MstProperty
                        join assetProperty in db.AssetProperties.Where(x => x.AssetId == assetId) on property.PropertyId equals assetProperty.MstPropertyId into assetProperties
                        from assetProperty in assetProperties.DefaultIfEmpty()
                        where property.IsDeleted == false
                        select new SelectListItem()
                        {
                            Text = property.PropertyName,
                            Selected = assetProperty != null,
                            Value = property.PropertyId.ToString()
                        }).ToList().OrderByDescending(x => x.Selected).ThenBy(X => X.Text);
            }
        }

        public static bool SaveAssetProperties(int assetId, string[] properties)
        {
            try
            {
                using (DB_Entities db = new DB_Entities())
                {

                    foreach (var property in properties)
                    {
                        int propertyId = Convert.ToInt32(property);

                        AssetProperties assetProperty = new AssetProperties()
                        {
                            AssetId = assetId,
                            MstPropertyId = propertyId
                        };
                        db.AssetProperties.Add(assetProperty);
                    }
                    db.SaveChanges();
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }
    }
}