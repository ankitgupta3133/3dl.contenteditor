﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _3DLContentManagement.Common
{
    public class Constants
    {
        public const string LanguageCodeEnglish = "3FC88BDA-9D70-E811-A964-000D3AB6EB93";
        public const string LanguageCodeNorwegian = "2528C1E1-9D70-E811-A964-000D3AB6EB93";
        public const string LanguageCodeNynorsk = "03CAB042-5975-E911-A969-000D3AB6D5D9";        
    }
}