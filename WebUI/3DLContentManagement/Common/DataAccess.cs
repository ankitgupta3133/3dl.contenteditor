﻿using _3DLContentManagement.Database;
using _3DLContentManagement.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Xml.Serialization;
using TDL.ContentPlatform.Server.Database.Tables;

namespace _3DLContentManagement.Common
{
    public static class DataAccess
    {
        private static string connectionString = ConfigurationManager.ConnectionStrings["DB_Entities"].ConnectionString; //Your connection string
        public static string GetSubtopicDetails(string subtopic)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                // 1.  create a command object identifying the stored procedure
                SqlCommand cmd = new SqlCommand("dbo.sp_GetSubtopicDetails", conn);

                // 2. set the command object so it knows to execute a stored procedure
                cmd.CommandType = CommandType.StoredProcedure;

                // 3. add parameter to command, which will be passed to the stored procedure
                cmd.Parameters.Add(new SqlParameter("@SubtopicName", subtopic));

                // execute the command
                using (var rdr = cmd.ExecuteReader())
                {
                    if (rdr.Read())
                    {
                        return rdr[0].ToString();
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        public static IList<GetLabel> GetXML(int? modelID)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                // 1.  create a command object identifying the stored procedure
                SqlCommand cmd = new SqlCommand("dbo.sp_GetXMLDetail", conn);

                // 2. set the command object so it knows to execute a stored procedure
                cmd.CommandType = CommandType.StoredProcedure;

                // 3. add parameter to command, which will be passed to the stored procedure
                cmd.Parameters.Add(new SqlParameter("@ModelID", modelID));

                // execute the command
                using (var rdr = cmd.ExecuteReader())
                {
                    IList<GetLabel> result = new List<GetLabel>();
                    //3. Loop through rows
                    while (rdr.Read())
                    {
                        //Get each column
                        result.Add(new GetLabel() { Label = (string)rdr.GetString(0) });
                    }
                    return result;
                }
            }

        }

        public static List<AssetViewModel> GetPackageDetails()
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand("dbo.sp_GetPackageDetail", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                using (var rdr = cmd.ExecuteReader())
                {
                    List<AssetViewModel> result = new List<AssetViewModel>();

                    while (rdr.Read())
                    {
                        //Get each column
                        result.Add(new AssetViewModel()
                        {
                            Package = rdr.GetString(0),
                            Language = rdr.GetString(1),
                            Level = rdr.GetString(2),
                            Syllabus = rdr.GetString(3),
                            Stream = rdr.GetString(4),
                            Subject = rdr.GetString(5),
                            Topic = rdr.GetString(6),
                            Subtopic = rdr.GetString(7)
                        });
                    }
                    return result;
                }
            }

        }

        public static List<AssetViewModel> GetAssetDetails()
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand("dbo.sp_GetAssetDetails", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                using (var rdr = cmd.ExecuteReader())
                {
                    List<AssetViewModel> result = new List<AssetViewModel>();

                    while (rdr.Read())
                    {
                        //Get each column
                        result.Add(new AssetViewModel()
                        {
                            AssetName = rdr.GetString(1),
                            Package = rdr.GetString(2),
                            Language = rdr.GetString(3),
                            Level = rdr.GetString(4),
                            Syllabus = rdr.GetString(5),
                            Stream = rdr.GetString(6),
                            Subject = rdr.GetString(7),
                            Topic = rdr.GetString(8),
                            Subtopic = rdr.GetString(9),
                            Status = rdr.GetString(10),
                            AssetType = rdr.GetString(11),
                            FileName = rdr.GetString(12),
                            FilePath = rdr.GetString(13)
                        });
                    }
                    return result;
                }
            }
        }

        public static List<AssetPublishStatusModel> GetAssetPublishStatusByPlatform()
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand("dbo.GetAssetPublishStatusByPlatform", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                using (var rdr = cmd.ExecuteReader())
                {
                    List<AssetPublishStatusModel> result = new List<AssetPublishStatusModel>();

                    while (rdr.Read())
                    {
                        //Get each column
                        result.Add(new AssetPublishStatusModel()
                        {
                            AssetID = rdr.GetInt32(0),
                            AssetName = rdr.IsDBNull(1) == false ? rdr.GetString(1) : string.Empty,
                            Status = rdr.IsDBNull(2) == false ? rdr.GetString(2) : string.Empty,
                            Type = rdr.IsDBNull(3) == false ? rdr.GetString(3) : string.Empty,
                            All = rdr.IsDBNull(4) == false ? rdr.GetString(4) : string.Empty,
                            Android = rdr.IsDBNull(5) == false ? rdr.GetString(5) : string.Empty,
                            Daydream = rdr.IsDBNull(6) == false ? rdr.GetString(6) : string.Empty,
                            IPhone = rdr.IsDBNull(7) == false ? rdr.GetString(7) : string.Empty,
                            MetroX64 = rdr.IsDBNull(8) == false ? rdr.GetString(8) : string.Empty,
                            Nibiru_VR = rdr.IsDBNull(9) == false ? rdr.GetString(9) : string.Empty,
                            Oculus = rdr.IsDBNull(10) == false ? rdr.GetString(10) : string.Empty,
                            OSX = rdr.IsDBNull(11) == false ? rdr.GetString(11) : string.Empty,
                            WebGL = rdr.IsDBNull(12) == false ? rdr.GetString(12) : string.Empty,
                            Windows = rdr.IsDBNull(13) == false ? rdr.GetString(13) : string.Empty,
                            WSA = rdr.IsDBNull(14) == false ? rdr.GetString(14) : string.Empty

                        });
                    }
                    return result;
                }
            }
        }

        public static void GetElements()
        {
            string path = @"E:\ContentEditor\3DL.ContentEditor\WebUI\3DLContentManagement\Content\xml\PeriodicPropertiesFile.xml";
            DataSet ds = new DataSet();
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(DataSet));
            FileStream readStream = new FileStream(path, FileMode.Open);
            ds = (DataSet)xmlSerializer.Deserialize(readStream);
            readStream.Close();

            DB_Entities db = new DB_Entities();

            for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
            {
                MstElement newObj = new MstElement()
                {
                    ElementName = ds.Tables[0].Rows[i].ItemArray[0].ToString(),
                    ElementShortName = ds.Tables[0].Rows[i].ItemArray[1].ToString(),
                    AtomicNumber = Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[2]),
                    IsDeleted = false
                };

                db.MstElement.Add(newObj);
                db.SaveChanges();
            }
        }

        public static List<RegisterViewModel> GetUserList()
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand("dbo.sp_GetUserList", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                using (var rdr = cmd.ExecuteReader())
                {
                    List<RegisterViewModel> result = new List<RegisterViewModel>();

                    while (rdr.Read())
                    {
                        //Get each column
                        result.Add(new RegisterViewModel()
                        {
                            UserID = rdr.GetGuid(0),
                            FirstName = rdr.GetString(1),
                            LastName = Convert.ToString(rdr.GetValue(2)),
                            Username = rdr.GetString(3),
                            Email = rdr.GetString(4),
                            Role = rdr.GetString(5),
                            //RoleId = rdr.GetGuid(6),
                            Account = rdr.GetString(6),
                            Opportunity = rdr.GetString(7),
                            Subscription = rdr.GetString(8),
                            IsActive = Convert.ToBoolean(rdr.GetValue(9)),
                            ExpiryDate = Convert.ToDateTime(rdr.GetValue(10))
                        });
                    }
                    return result;
                }
            }
        }

        public static RegisterViewModel GetUserDetails(Guid UserId)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand("dbo.sp_GetUserDetails", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@UserID", UserId));

                using (var rdr = cmd.ExecuteReader())
                {
                    RegisterViewModel result = new RegisterViewModel();

                    while (rdr.Read())
                    {
                        //Get each column
                        result = new RegisterViewModel()
                        {
                            UserID = rdr.GetGuid(0),
                            FirstName = rdr.GetString(1),
                            LastName = Convert.ToString(rdr.GetValue(2)),
                            Username = rdr.GetString(3),
                            Email = rdr.GetString(4),
                            Role = rdr.GetString(5),
                            RoleId = rdr.GetGuid(6),
                            Account = rdr.GetString(7),
                            AccountId = rdr.GetGuid(8),
                            Opportunity = rdr.GetString(9),
                            Subscription = rdr.GetString(10),
                            SubscriptionId = rdr.GetGuid(11),
                            IsActive = Convert.ToBoolean(rdr.GetValue(12)),
                            IsDeleted = Convert.ToBoolean(rdr.GetValue(13))
                        };
                    }
                    return result;
                }
            }
        }

        public static List<RoleModel> GetRoles()
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                //Provide the query string with a parameter placeholder.
                string queryString =
                    "SELECT Id, Name from dbo.AspNetRoles "
                        //+ "WHERE Id = @Id "
                        + "ORDER BY Name;";

                //Specify the parameter value.
                //int paramId = 5;

                SqlCommand cmd = new SqlCommand(queryString, conn);
                //cmd.Parameters.AddWithValue("@Id", paramId);

                using (var rdr = cmd.ExecuteReader())
                {
                    List<RoleModel> result = new List<RoleModel>();

                    while (rdr.Read())
                    {
                        //Get each column
                        result.Add(new RoleModel()
                        {
                            Id = rdr.GetGuid(0),
                            Name = rdr.GetString(1),
                        });
                    }
                    return result;
                }
            }
        }

        public static Boolean UpdateUserDetails(RegisterViewModel user)
        {
            Boolean status = false;

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                //Provide the query string with a parameter placeholder.
                string queryString = "UPDATE dbo.AspNetUsers SET Firstname = @Firstname, Lastname = @Lastname, Account = @Account, Username = @Username, Email = @Email, NormalizedEmail=@NEmail, NormalizedUserName=@NUsername WHERE Id = @Id ";

                //Specify the parameter value.
                Guid paramId = user.UserID;
                //String paramFirstname = user.FirstName;
                //String paramLastname = user.LastName;
                //Guid paramAccount = user.AccountId;
                if (string.IsNullOrEmpty(user.LastName))
                {
                    user.LastName = string.Empty;
                }

                SqlCommand cmd = new SqlCommand(queryString, conn);
                cmd.Parameters.AddWithValue("@Id", paramId);
                cmd.Parameters.AddWithValue("@Firstname", user.FirstName);
                cmd.Parameters.AddWithValue("@Lastname", user.LastName);
                cmd.Parameters.AddWithValue("@Account", user.AccountId);
                cmd.Parameters.AddWithValue("@Username", user.Username);
                cmd.Parameters.AddWithValue("@Email", user.Email);
                cmd.Parameters.AddWithValue("@NUsername", user.Username.ToUpper());
                cmd.Parameters.AddWithValue("@NEmail", user.Email.ToUpper());

                cmd.ExecuteNonQuery();

                //Update user role
                queryString = "UPDATE dbo.AspNetUserRoles SET RoleId = @RoleId WHERE UserId = @Id ";
                SqlCommand newCommand = new SqlCommand(queryString, conn);
                newCommand.Parameters.AddWithValue("@Id", paramId);
                newCommand.Parameters.AddWithValue("@RoleId", user.RoleId);
                newCommand.ExecuteNonQuery();

                status = true;
            }

            return status;
        }

        public static Boolean ActiveUser(Guid paramId)
        {
            Boolean status = false;

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                //Provide the query string with a parameter placeholder.
                string queryString = "UPDATE dbo.AspNetUsers SET IsActive = 1 WHERE Id = @Id ";

                SqlCommand cmd = new SqlCommand(queryString, conn);
                cmd.Parameters.AddWithValue("@Id", paramId);
                cmd.ExecuteNonQuery();

                status = true;
            }

            return status;
        }

        public static Boolean InactiveUser(Guid paramId)
        {
            Boolean status = false;

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                //Provide the query string with a parameter placeholder.
                string queryString = "UPDATE dbo.AspNetUsers SET IsActive = 0 WHERE Id = @Id ";

                SqlCommand cmd = new SqlCommand(queryString, conn);
                cmd.Parameters.AddWithValue("@Id", paramId);
                cmd.ExecuteNonQuery();

                status = true;
            }

            return status;
        }

        public static int ValidateEmail(string email, string userId)
        {
            int count;
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                //Provide the query string with a parameter placeholder.
                string queryString = "SELECT COUNT(*) FROM dbo.AspNetUsers WHERE (Email = @Email OR UserName = @Email) AND ID != @Id";

                SqlCommand cmd = new SqlCommand(queryString, conn);
                cmd.Parameters.AddWithValue("@Email", email);
                cmd.Parameters.AddWithValue("@Id", userId);
                count = (Int32)cmd.ExecuteScalar();
            }

            return count;
        }

        public static List<AppViewModel> GetAppList()
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand("dbo.sp_GetAppList", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                using (var rdr = cmd.ExecuteReader())
                {
                    List<AppViewModel> result = new List<AppViewModel>();

                    while (rdr.Read())
                    {
                        //Get each column
                        result.Add(new AppViewModel()
                        {
                            ID = rdr.GetGuid(0),
                            AppID = rdr.GetString(1),
                            AppName = Convert.ToString(rdr.GetValue(2)),
                            User = rdr.GetString(3),
                            UserID = rdr.GetGuid(4),
                            IsActive = Convert.ToBoolean(rdr.GetValue(5)),
                            IsDeleted = Convert.ToBoolean(rdr.GetValue(6))
                        });
                    }
                    return result;
                }
            }
        }

        public static List<RoleModel> GetUsers()
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                //Provide the query string with a parameter placeholder.
                string queryString = "SELECT Id, IIF(LastName is null, Firstname, FirstName + ' ' + LastName) AS [Name] from AspNetUsers where IsActive = 1 AND FirstName IS NOT NULL ORDER BY Name";

                SqlCommand cmd = new SqlCommand(queryString, conn);

                using (var rdr = cmd.ExecuteReader())
                {
                    List<RoleModel> result = new List<RoleModel>();

                    while (rdr.Read())
                    {
                        //Get each column
                        result.Add(new RoleModel()
                        {
                            Id = rdr.GetGuid(0),
                            Name = rdr.GetString(1),
                        });
                    }
                    return result;
                }
            }
        }
    }

    //public class DataSP
    //{
    //    private static IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["DB_Entities"].ConnectionString);
    //    var parameter = new DynamicParameters();
    //}
}