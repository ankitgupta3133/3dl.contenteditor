﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace _3DLContentManagement.Common
{
    public static class HttpRestClient
    {
        public enum HttpMethods
        {
            GET,
            POST
        }

        public static TRes DoHttp<TReq, TRes>(string url, HttpMethods method, TReq request, string authToken = null)
        {
            using (var client = new WebClient())
            {
                if (!string.IsNullOrWhiteSpace(authToken))
                    client.Headers[HttpRequestHeader.Authorization] = authToken;

                client.Headers[HttpRequestHeader.UserAgent] = ".Net WebClient";
                client.Headers[HttpRequestHeader.Accept] = "application/json";
                client.Headers[HttpRequestHeader.AcceptEncoding] = "gzip";

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                string resJson = null;
                if (method == HttpMethods.POST)
                {
                    client.Headers[HttpRequestHeader.ContentType] = "application/json";
                    client.Encoding = System.Text.Encoding.UTF8;
                    var reqJson = JsonConvert.SerializeObject(request);
                    resJson = client.UploadString(url, method.ToString(), reqJson);
                }
                if (method == HttpMethods.GET)
                {
                    var properties = from p in typeof(TReq).GetProperties()
                                     where p.GetValue(request, null) != null
                                     select p.Name + "=" + WebUtility.UrlEncode(p.GetValue(request, null).ToString());

                    url = url + "?" + String.Join("&", properties.ToArray());
                    resJson = client.DownloadString(url);
                }
                var response = JsonConvert.DeserializeObject<TRes>(resJson);
                return response;
            }

        }
    }
}