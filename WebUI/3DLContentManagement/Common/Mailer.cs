﻿using System;
using System.Configuration;
using System.Net;
using System.Net.Mail;

namespace _3DLContentManagement.Common
{
    public static class Mailer
    {
        public static bool SendMail(string From, string To, string Subject, string MessageBody, bool IsHTML = true, string BCC = "", string CC = "")
        {
            bool _return = false;
            try
            {
                var message = new MailMessage();
                message.To.Add(new MailAddress(To));

                if (!string.IsNullOrEmpty(BCC))
                    message.Bcc.Add(new MailAddress(BCC));

                if (!string.IsNullOrEmpty(CC))
                    message.CC.Add(new MailAddress(CC));

                message.From = new MailAddress(From, "3DL");
                message.Subject = Subject;
                message.Body = MessageBody;
                message.IsBodyHtml = IsHTML;
                message.BodyEncoding = System.Text.Encoding.GetEncoding("utf-8");
                message.Headers.Add("Message-Id", String.Concat("<", DateTime.Now.ToString("yyMMdd"), ".", DateTime.Now.ToString("HHmmss"), ConfigurationManager.AppSettings["WebMailDomainName"].ToString()));

                using (var smtp = new SmtpClient())
                {
                    smtp.Credentials = new NetworkCredential
                    {
                        UserName = ConfigurationManager.AppSettings["AdminEmail"].ToString(),
                        Password = ConfigurationManager.AppSettings["Password"].ToString()
                    };
                    smtp.Host = ConfigurationManager.AppSettings["SMTP"].ToString();
                    smtp.Port = Convert.ToInt16(ConfigurationManager.AppSettings["Port"]);
                    smtp.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]);
                    //Add this line to bypass the certificate validation
                    System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate (object s,
                        System.Security.Cryptography.X509Certificates.X509Certificate certificate,
                        System.Security.Cryptography.X509Certificates.X509Chain chain,
                        System.Net.Security.SslPolicyErrors sslPolicyErrors)
                    {
                        return true;
                    };

                    smtp.Send(message);
                    _return = true;
                }
            }
            catch (Exception)
            {
                _return = false;
                //throw;
            }

            return _return;
        }
    }
}