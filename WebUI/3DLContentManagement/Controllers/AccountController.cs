﻿using _3DLContentManagement.Common;
using _3DLContentManagement.Database;
using _3DLContentManagement.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.Security;

namespace _3DLContentManagement.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login()
        {
            LoginViewModel vm = new LoginViewModel();

            if (Request.Cookies[FormsAuthentication.FormsCookieName] != null)
            {
                HttpCookie authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];
                if (authCookie != null)
                {
                    FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(authCookie.Value);
                    Guid UserId = Guid.Parse(ticket.Name);

                    //LoggedInUser userDetailObj = JsonConvert.DeserializeObject<LoggedInUser>(userData);
                    using (DB_Entities db = new DB_Entities())
                    {
                        var obj = db.UserProfile.Where(a => a.UserId == UserId).FirstOrDefault();
                        if (obj != null)
                        {
                            Session["UserID"] = obj.UserId.ToString();
                            Session["UserName"] = obj.UserName.ToString();
                            Session["Name"] = obj.FirstName.ToString() + " " + obj.LastName;
                            return RedirectToAction("Index", "Content");
                        }
                    }
                }
            }

            return View(vm);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel objUser)
        {
            using (DB_Entities db = new DB_Entities())
            {
                var obj = db.UserProfile.Where(a => a.UserName.Equals(objUser.UserName)).FirstOrDefault();
                if (obj != null)
                {
                    //for existing users
                    string password = CommonMethods.MD5Hash(objUser.Password);
                    if (obj.Password == password)
                    {
                        password = CommonMethods.Encrypt(objUser.Password);
                        obj.Password = password;
                        db.SaveChanges();
                    }

                    password = CommonMethods.Encrypt(objUser.Password);
                    if (obj.Password == password)
                    {
                        if (obj.IsDeleted == false)
                        {
                            if (obj.IsActive == true)
                            {
                                Session["UserID"] = obj.UserId.ToString();
                                Session["UserName"] = obj.UserName.ToString();
                                Session["Name"] = obj.FirstName.ToString() + " " + obj.LastName;
                                await Task.Run(() => SetAuthentication(obj.UserId, "User", obj.UserName));
                                TempData["Message"] = "Login";
                                return RedirectToAction("Index", "Content");
                            }
                            else
                            {
                                ModelState.AddModelError("", "Account inactivated, contact to 3DL administration.");
                                return View(objUser);
                            }
                        }
                        else
                        {
                            ModelState.AddModelError("", "Account expired, contact to 3DL administration.");
                            return View(objUser);
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("Password", "Invalid Password.");
                        return View(objUser);
                    }
                }
                else
                {
                    ModelState.AddModelError("UserName", "Invalid UserName.");
                    return View(objUser);
                }
            }
        }
        private void SetAuthentication(Guid UserID, string Role, string UserName)
        {
            //FormAuth
            string value = Convert.ToString(WebConfigurationManager.AppSettings["AdminAddress"]);
            var admin = value.Split(';').ToList();
            if (admin.Contains(UserName))
            {
                Role = "Admin";
            }           
           
            FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(
              1,
              UserID.ToString(),  //user id
              DateTime.Now,
              DateTime.Now.AddMinutes(30),  // expiry
              true,  //do not remember
              Role,
              "/");
            HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, FormsAuthentication.Encrypt(authTicket));
            Response.Cookies.Add(cookie);
        }
        public ActionResult Logout()
        {
            Session.Abandon();
            System.Web.Security.FormsAuthentication.SignOut();
            return RedirectToAction("Login", "Account");
        }

        //
        // GET: /Account/ChangePassword
        public ActionResult ChangePassword()
        {
            return View();
        }

        //
        // POST: /Account/ChangePassword
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            using (DB_Entities db = new DB_Entities())
            {
                string UserName = Convert.ToString(Session["UserName"]);
                var userObj = db.UserProfile.Where(a => a.UserName == UserName &&  a.IsDeleted == false).FirstOrDefault();

                if (userObj != null)
                {
                    //for existing users
                    string password = CommonMethods.MD5Hash(model.OldPassword.Trim());
                    if (userObj.Password == password)
                    {
                        password = CommonMethods.Encrypt(model.OldPassword.Trim());
                        userObj.Password = password;
                        db.SaveChanges();
                    }

                    password = CommonMethods.Encrypt(model.OldPassword.Trim());
                    if (userObj.Password == password)
                    {
                        userObj.Password = await Task.Run(() => CommonMethods.Encrypt(model.NewPassword.Trim()));
                        userObj.ModifiedBy = new Guid(Session["UserID"].ToString());
                        userObj.ModifiedOn = DateTime.Now;
                        db.Entry(userObj).State = EntityState.Modified;
                        db.SaveChanges();

                        string strAction = "User details updated";
                        string strActivity = string.Format("User details updated for named '{0}'. Password changed.", userObj.FirstName);
                        await ActivityLogger.AddActivityLog(0, strAction, strActivity);
                    }
                    else
                    {
                        ModelState.AddModelError("", "Invalid Old Password.");
                        return View(model);
                    }
                }
            }

            TempData["Message"] = "Password updated successfully.";
            return RedirectToAction("Index", "Home");
        }

        //
        // GET: /Account/MyProfile
        public ActionResult MyProfile()
        {
            ChangeProfileViewModel profile = new ChangeProfileViewModel();
            using (DB_Entities db = new DB_Entities())
            {
                string UserName = Convert.ToString(Session["UserName"]);
                var userObj = db.UserProfile.Where(a => a.UserName == UserName && a.IsDeleted == false).FirstOrDefault();

                if (userObj != null)
                {
                    profile.UserName = userObj.UserName;
                    profile.FirstName = userObj.FirstName;
                    profile.LastName = userObj.LastName;
                }
            }
            return View(profile);
        }

        //
        // POST: /Account/MyProfile
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> MyProfile(ChangeProfileViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            using (DB_Entities db = new DB_Entities())
            {
                var userObj = db.UserProfile.Where(a => a.UserName == model.UserName && a.IsDeleted == false).FirstOrDefault();

                if (userObj != null)
                {
                    string strAction = "User details updated";
                    string strActivity = string.Format("User details updated for named '{0}'. Password changed.", userObj.FirstName);

                    if (userObj.UserName != model.UserName)
                    {
                        strActivity += string.Format("UserName changed from '{0}' to '{1}'; ", userObj.UserName, model.UserName);
                    }

                    if (!userObj.FirstName.Equals(model.FirstName.Trim()))
                    {
                        strActivity += string.Format("First Name changed from '{0}' to '{1}'; ", userObj.FirstName, model.FirstName);
                    }

                    model.LastName = model.LastName != string.Empty && model.LastName != null ? model.LastName.Trim() : model.LastName;
                    if (userObj.LastName != model.LastName)
                    {
                        strActivity += string.Format("Last Name changed from '{0}' to '{1}'; ", userObj.LastName, model.LastName);
                    }

                    //for existing users
                    string password = CommonMethods.MD5Hash(model.OldPassword.Trim());
                    if (userObj.Password == password)
                    {
                        password = CommonMethods.Encrypt(model.OldPassword.Trim());
                        userObj.Password = password;
                        db.SaveChanges();
                    }

                    password = CommonMethods.Encrypt(model.OldPassword.Trim());
                    if (userObj.Password == password)
                    {
                        if(model.NewPassword != string.Empty && model.NewPassword != null && model.NewPassword.Trim() != "")
                        {
                            userObj.Password = await Task.Run(() => CommonMethods.Encrypt(model.NewPassword.Trim()));
                            strActivity += string.Format("Password changed; ");
                        }

                        userObj.UserName = model.UserName.Trim();
                        userObj.FirstName = model.FirstName.Trim();
                        userObj.LastName = model.LastName;
                        userObj.ModifiedBy = new Guid(Session["UserID"].ToString());
                        userObj.ModifiedOn = DateTime.Now;
                        db.Entry(userObj).State = EntityState.Modified;
                        db.SaveChanges();

                        await ActivityLogger.AddActivityLog(0, strAction, strActivity);
                        Session["Name"] = userObj.FirstName.ToString() + " " + userObj.LastName;
                    }
                    else
                    {
                        ModelState.AddModelError("", "Invalid Old Password.");
                        return View(model);
                    }
                }
            }

            TempData["Message"] = "User details updated successfully.";
            return RedirectToAction("Index", "Content");
        }

        //
        // GET: /Account/ForgotPassword
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        //
        // POST: /Account/ChangePassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            using (DB_Entities db = new DB_Entities())
            {
                string UserName = model.Email;
                var userObj = db.UserProfile.Where(a => a.UserName == UserName && a.IsDeleted == false).FirstOrDefault();

                if (userObj != null)
                {
                    string ResetURL = WebConfigurationManager.AppSettings["domain"].ToString() + "Account/ResetPassword?code=" + userObj.UserId;
                    var mailcontent = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/EmailTemplates/ForgotPasswordTemplate.html"));
                    mailcontent = mailcontent.Replace("{ResetURL}", ResetURL);

                    //Sharing account details with the user
                    await Task.Run(() => Mailer.SendMail(WebConfigurationManager.AppSettings["replyAddress"].ToString(), userObj.UserName, "Forgot Password Assistance", mailcontent));
                }
                else
                {
                    ModelState.AddModelError("Email", "This email address is not registered yet.");
                    return View(model);
                }
            }

            return RedirectToAction("ForgotPasswordConfirmation");
        }

        //
        // GET: /Account/ForgotPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        //
        // GET: /Account/ResetPassword
        [AllowAnonymous]
        public ActionResult ResetPassword(string code)
        {
            return code == null ? View("Error") : View();
        }

        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            using (DB_Entities db = new DB_Entities())
            {
                Guid UserID = new Guid(model.Code);
                var userObj = db.UserProfile.Where(a => a.UserId == UserID && a.IsDeleted == false).FirstOrDefault();

                if (userObj != null)
                {
                    if (userObj.UserName == model.Email.Trim())
                    {
                        userObj.Password = await Task.Run(() => CommonMethods.Encrypt(model.ConfirmPassword.Trim()));
                        userObj.ModifiedBy = UserID;
                        userObj.ModifiedOn = DateTime.Now;
                        db.Entry(userObj).State = EntityState.Modified;
                        await db.SaveChangesAsync();

                        //string strAction = "Password reset";
                        //string strActivity = string.Format("User details updated for named '{0}'. Password reset.", userObj.FirstName);
                        //ActivityLogger.AddActivityLog(0, strAction, strActivity);
                    }
                    else
                    {
                        ModelState.AddModelError("Email", "Invalid Email.");
                        return View(model);
                    }
                }
                else
                {
                    TempData["Message"] = "Invalid reset password link.";
                    return View(model);
                }
            }

            return RedirectToAction("ResetPasswordConfirmation");
        }

        //
        // GET: /Account/ResetPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            Session.Abandon();
            return View();
        }
    }
}