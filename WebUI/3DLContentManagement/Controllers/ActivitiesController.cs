﻿using _3DLContentManagement.App_Start;
using _3DLContentManagement.Common;
using _3DLContentManagement.Database;
using _3DLContentManagement.Models;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using TDL.ContentPlatform.Server.Database.Tables;

namespace _3DLContentManagement.Controllers
{
    [RoleAuthorize]
    [SessionExpire]
    public class ActivitiesController : Controller
    {
        // GET: Activities
        public ActionResult Index()
        {
            using (DB_Entities db = new DB_Entities())
            {
                var activities = db.MstActivity.Where(x => x.IsDeleted == false && x.IsActive == true).ToList();
                if (activities != null && activities.Count > 0)
                {
                    ViewBag.ActivityId = activities[0].ActivityId;
                    ViewBag.Activity = activities[0].ActivityName;
                }
                ViewBag.Activities = activities;
            }
            return View();
        }

        #region Select
        public ActionResult GetAll([DataSourceRequest] DataSourceRequest request, int Id, string Type)
        {
            List<ActivityModel> listActivities = new List<ActivityModel>();
            using (DB_Entities db = new DB_Entities())
            {
                var activities = db.Activities.Where(x => x.IsDeleted == false && x.ActivityType == Id).OrderBy(x => x.ActivityName).ToList();

                listActivities = activities.Select(activity => new ActivityModel()
                {
                    ActivityId = activity.ActivityId,
                    ActivityName = activity.ActivityName,
                    ActivityLanguageId = activity.LanguageId,
                    IsModelSpecific = activity.IsModelSpecific,
                    ActivityType = activity.ActivityType,
                    ActivityTypeName = Type,
                    IsActivityEnabled = activity.IsEnabled,
                    IsActivityDeleted = activity.IsDeleted
                }).Distinct().ToList();
            }

            ViewBag.ActivityId = Id;
            ViewBag.Activity = Type;

            return Json(listActivities.ToDataSourceResult(request));
        }

        #endregion

        public ActionResult GetActivity(int activityId, string activityType)
        {
            DB_Entities db = new DB_Entities();
            string selected = string.Empty;
            var assets = GetAssets();

            if (activityType == "Quiz")
            {
                if (activityId == 0)
                {
                    var newAct = new QuizModel() { QuizId = 0, QuizName = "", IsQuizModelSpecific = false };
                    newAct.QuizDetails = (from a in assets
                                          select new QuizDetailModel()
                                          {
                                              Id = 0,
                                              QuizId = 0,
                                              AssetId = a.AssetId,
                                              AssetName = a.AssetName,
                                              AssetContentId = a.AssetContentId,
                                              ModelId = a.ModelId,
                                              IsSelected = false
                                          }).OrderBy(x => x.AssetName).ToList();

                    //foreach (var qd in newAct.QuizDetails)
                    //{
                    //    var labels = GetLabels(qd.ModelId);

                    //    qd.QuizLabels = labels.Select(label => new QuizLabelDetailModel()
                    //    {
                    //        Id = 0,
                    //        QuizDetailId = 0,
                    //        LabelId = label.LabelID,
                    //        LabelName = label.EN,
                    //        IsSelected = false
                    //    }).ToList();
                    //}

                    if (string.IsNullOrEmpty(selected))
                    {
                        ViewBag.SelectedAsset = "No model selected";
                    }

                    return PartialView("_AddUpdateQuiz", newAct);
                }
                else
                {
                    var act = (from a in db.Activities
                               where a.ActivityId == activityId && a.IsDeleted == false
                               select new QuizModel()
                               {
                                   QuizId = a.ActivityId,
                                   QuizName = a.ActivityName,
                                   IsQuizEnabled = a.IsEnabled,
                                   IsQuizDeleted = a.IsDeleted,
                                   IsQuizModelSpecific = a.IsModelSpecific
                               }).FirstOrDefault();

                    var quizzes = (from qd in db.QuizDetails where qd.QuizId == activityId select qd).ToList();
                    act.QuizDetails = (from a in assets
                                       join q in quizzes on a.AssetId equals q.AssetId into aq
                                       from q in aq.DefaultIfEmpty()
                                       select new QuizDetailModel()
                                       {
                                           Id = q == null ? 0 : q.Id,
                                           QuizId = activityId,
                                           AssetId = a.AssetId,
                                           AssetName = a.AssetName,
                                           AssetContentId = a.AssetContentId,
                                           IsSelected = q == null ? false : (q.IsEnabled ?? false),
                                           ModelId = a.ModelId,
                                           NumberOfAttempts = q == null ? 0 : q.NumberOfAttempts,
                                           NumberOfLabels = q == null ? 0 : q.NumberOfLabels,
                                           Labels = q?.Labels
                                       }).OrderByDescending(x => x.IsSelected).ThenBy(x => x.AssetName).ToList();


                    foreach (var qd in act.QuizDetails)
                    {
                        var labels = GetLabels(qd.ModelId);
                        if (qd.IsSelected == true)
                        {
                            //var labels = GetLabels(qd.ModelId);

                            qd.QuizLabels = (from l in labels
                                             join q in db.QuizLabelDetails on l.EN equals q.LabelName into lq
                                             from q in lq.DefaultIfEmpty()
                                             where q.QuizDetailId == qd.Id
                                             select new QuizLabelDetailModel()
                                             {
                                                 Id = q == null ? 0 : q.Id,
                                                 QuizDetailId = q == null ? 0 : q.QuizDetailId,
                                                 LabelId = l.LabelID,
                                                 LabelName = l.EN,
                                                 IsSelected = q == null ? false : (q.IsEnabled ?? false)
                                             }).OrderByDescending(X => X.IsSelected).ThenBy(X => X.LabelName).ToList();

                            selected = selected + qd.AssetName + ", ";
                        }
                        else
                        {
                            break;
                        }
                    }
                    if (!string.IsNullOrEmpty(selected))
                    {
                        ViewBag.SelectedAsset = selected.Substring(0, selected.Length - 2);
                    }

                    return PartialView("_AddUpdateQuiz", act);
                }
            }
            else if (activityType == "Puzzle")
            {
                if (activityId == 0)
                {
                    var newAct = new PuzzleModel() { PuzzleId = 0, PuzzleName = "", IsPuzzleModelSpecific = false };

                    newAct.PuzzleDetails = (from a in assets
                                            select new PuzzleDetailModel()
                                            {
                                                Id = 0,
                                                PuzzleId = 0,
                                                AssetId = a.AssetId,
                                                AssetName = a.AssetName,
                                                AssetContentId = a.AssetContentId,
                                                IsSelected = false,
                                                ModelId = 0,
                                                NumberOfParts = 0
                                            }).OrderBy(x => x.AssetName).ToList();

                    if (string.IsNullOrEmpty(selected))
                    {
                        ViewBag.SelectedAsset = "No model selected";
                    }

                    return PartialView("_AddUpdatePuzzle", newAct);
                }
                else
                {
                    var act = (from a in db.Activities
                               where a.ActivityId == activityId && a.IsDeleted == false
                               select new PuzzleModel()
                               {
                                   PuzzleId = a.ActivityId,
                                   PuzzleName = a.ActivityName,
                                   IsPuzzleModelSpecific = a.IsModelSpecific,
                                   IsPuzzleEnabled = a.IsEnabled,
                                   IsPuzzleDeleted = a.IsDeleted
                               }).FirstOrDefault();

                    var puzzles = (from pd in db.PuzzleDetails where pd.PuzzleId == activityId select pd).ToList();
                    act.PuzzleDetails = (from a in assets
                                         join p in puzzles on a.AssetId equals p.AssetId into ap
                                         from p in ap.DefaultIfEmpty()
                                         select new PuzzleDetailModel()
                                         {
                                             Id = p == null ? 0 : p.Id,
                                             PuzzleId = activityId,
                                             AssetId = a.AssetId,
                                             AssetName = a.AssetName,
                                             AssetContentId = a.AssetContentId,
                                             IsSelected = p == null ? false : (p.IsEnabled ?? false),
                                             ModelId = a.ModelId,
                                             NumberOfParts = p == null ? 0 : p.NumberOfParts
                                         }).OrderByDescending(x => x.IsSelected).ThenBy(x => x.AssetName).ToList();

                    foreach (var pd in act.PuzzleDetails)
                    {
                        if (pd.IsSelected == true)
                        {
                            selected = selected + pd.AssetName + ", ";
                        }
                        else
                        {
                            break;
                        }
                    }

                    if (!string.IsNullOrEmpty(selected))
                    {
                        ViewBag.SelectedAsset = selected.Substring(0, selected.Length - 2);
                    }

                    return PartialView("_AddUpdatePuzzle", act);
                }
            }

            return Json("Not a valid request!", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AddUpdateQuiz(int activityId, string activityType)
        {
            DB_Entities db = new DB_Entities();
            List<AssetNameModel> listNames = new List<AssetNameModel>();
            string selected = string.Empty;
            var assets = GetAssets();
            var platforms = GetPlatforms();
            ViewBag.Language = db.CsNewLanguage.Select(x => new SelectListItem { Text = x.NewName, Value = x.NewLanguageid.ToString() }).OrderBy(x => x.Text).ToList();
            ViewBag.Package = db.CsNewPackage.Select(t => new SelectListItem { Text = t.NewName, Value = t.NewPackageid.ToString() }).OrderBy(x => x.Text).ToList();

            if (activityId == 0)
            {
                var newAct = new QuizModel() { QuizId = 0, QuizName = "", IsQuizModelSpecific = false };
                newAct.QuizDetails = (from a in assets
                                      select new QuizDetailModel()
                                      {
                                          Id = 0,
                                          QuizId = 0,
                                          AssetId = a.AssetId,
                                          AssetName = a.AssetName,
                                          AssetContentId = a.AssetContentId,
                                          ModelId = a.ModelId,
                                          IsSelected = false
                                      }).OrderBy(x => x.AssetName).ToList();

                if (string.IsNullOrEmpty(selected))
                {
                    ViewBag.SelectedAsset = "No model selected";
                }

                AssetNameModel name = new AssetNameModel();
                listNames.Add(name);
                newAct.Names = listNames;

                newAct.Platforms = platforms;

                return View(newAct);
            }
            else
            {
                var act = (from a in db.Activities
                           where a.ActivityId == activityId && a.IsDeleted == false
                           select new QuizModel()
                           {
                               QuizId = a.ActivityId,
                               QuizName = a.ActivityName,
                               Duration = a.Duration,
                               IsQuizEnabled = a.IsEnabled,
                               IsQuizDeleted = a.IsDeleted,
                               IsQuizModelSpecific = a.IsModelSpecific
                           }).FirstOrDefault();

                listNames = GetActivityNames(activityId);
                act.Names = listNames;

                act.Details = GetActivityDetails(activityId);

                var listPlatform = (from ap in db.ActivityPlatforms where ap.ActivityId == activityId select ap).ToList();
                act.Platforms = (from p in platforms
                                 join l in listPlatform on p.PlatformId equals l.PlatformId into ap
                                 from l in ap.DefaultIfEmpty()
                                 select new PlatformModel()
                                 {
                                     Id = l == null ? 0 : l.Id,
                                     PlatformId = p.PlatformId,
                                     PlatformName = p.PlatformName,
                                     IsSelected = l == null ? false : l.IsSelected
                                 }).OrderByDescending(x => x.IsSelected).ThenBy(x => x.PlatformName).ToList();

                var quizzes = (from qd in db.QuizDetails where qd.QuizId == activityId select qd).ToList();
                act.QuizDetails = (from a in assets
                                   join q in quizzes on a.AssetId equals q.AssetId into aq
                                   from q in aq.DefaultIfEmpty()
                                   select new QuizDetailModel()
                                   {
                                       Id = q == null ? 0 : q.Id,
                                       QuizId = activityId,
                                       AssetId = a.AssetId,
                                       AssetName = a.AssetName,
                                       AssetContentId = a.AssetContentId,
                                       IsSelected = q == null ? false : (q.IsEnabled ?? false),
                                       ModelId = a.ModelId,
                                       NumberOfAttempts = q == null ? 0 : q.NumberOfAttempts,
                                       NumberOfLabels = q == null ? 0 : q.NumberOfLabels,
                                       Labels = q?.Labels
                                   }).OrderByDescending(x => x.IsSelected).ThenBy(x => x.AssetName).ToList();


                foreach (var qd in act.QuizDetails)
                {
                    var labels = GetLabels(qd.ModelId);
                    if (qd.IsSelected == true)
                    {
                        //var labels = GetLabels(qd.ModelId);

                        qd.QuizLabels = (from l in labels
                                         join q in db.QuizLabelDetails.Where(o => o.QuizDetailId == qd.Id) on l.EN equals q.LabelName into lq
                                         from q in lq.DefaultIfEmpty()
                                         select new QuizLabelDetailModel()
                                         {
                                             Id = q == null ? 0 : q.Id,
                                             QuizDetailId = q == null ? 0 : q.QuizDetailId,
                                             LabelId = l.LabelID,
                                             LabelName = l.EN,
                                             IsSelected = q == null ? false : (q.IsEnabled ?? false)
                                         }).OrderByDescending(X => X.IsSelected).ThenBy(X => X.LabelName).ToList();

                        selected = selected + qd.AssetName + ", ";
                    }
                    else
                    {
                        break;
                    }
                }
                if (!string.IsNullOrEmpty(selected))
                {
                    ViewBag.SelectedAsset = selected.Substring(0, selected.Length - 2);
                }

                return View(act);
            }
        }

        [HttpPost]
        public ActionResult AddUpdatePuzzle(int activityId, string activityType)
        {
            DB_Entities db = new DB_Entities();
            List<AssetNameModel> listNames = new List<AssetNameModel>();
            string selected = string.Empty;
            var assets = GetAssets();
            var platforms = GetPlatforms();
            ViewBag.Language = db.CsNewLanguage.Select(x => new SelectListItem { Text = x.NewName, Value = x.NewLanguageid.ToString() }).OrderBy(x => x.Text).ToList();
            ViewBag.Package = db.CsNewPackage.Select(t => new SelectListItem { Text = t.NewName, Value = t.NewPackageid.ToString() }).OrderBy(x => x.Text).ToList();

            if (activityId == 0)
            {
                var newAct = new PuzzleModel() { PuzzleId = 0, PuzzleName = "", IsPuzzleModelSpecific = false };

                newAct.PuzzleDetails = (from a in assets
                                        select new PuzzleDetailModel()
                                        {
                                            Id = 0,
                                            PuzzleId = 0,
                                            AssetId = a.AssetId,
                                            AssetName = a.AssetName,
                                            AssetContentId = a.AssetContentId,
                                            IsSelected = false,
                                            ModelId = a.ModelId,
                                            NumberOfParts = 0
                                        }).OrderBy(x => x.AssetName).ToList();

                if (string.IsNullOrEmpty(selected))
                {
                    ViewBag.SelectedAsset = "No model selected";
                }

                AssetNameModel name = new AssetNameModel();
                listNames.Add(name);
                newAct.Names = listNames;

                newAct.PuzzlePlatforms = platforms;

                return View(newAct);
            }
            else
            {
                var act = (from a in db.Activities
                           where a.ActivityId == activityId && a.IsDeleted == false
                           select new PuzzleModel()
                           {
                               PuzzleId = a.ActivityId,
                               PuzzleName = a.ActivityName,
                               Duration = a.Duration,
                               IsPuzzleModelSpecific = a.IsModelSpecific,
                               IsPuzzleEnabled = a.IsEnabled,
                               IsPuzzleDeleted = a.IsDeleted
                           }).FirstOrDefault();

                listNames = GetActivityNames(activityId);
                act.Names = listNames;

                act.Details = GetActivityDetails(activityId);

                var listPlatform = (from ap in db.ActivityPlatforms where ap.ActivityId == activityId select ap).ToList();
                act.PuzzlePlatforms = (from p in platforms
                                       join l in listPlatform on p.PlatformId equals l.PlatformId into ap
                                       from l in ap.DefaultIfEmpty()
                                       select new PlatformModel()
                                       {
                                           Id = l == null ? 0 : l.Id,
                                           PlatformId = p.PlatformId,
                                           PlatformName = p.PlatformName,
                                           IsSelected = l == null ? false : l.IsSelected
                                       }).OrderByDescending(x => x.IsSelected).ThenBy(x => x.PlatformName).ToList();

                var puzzles = (from pd in db.PuzzleDetails where pd.PuzzleId == activityId select pd).ToList();
                act.PuzzleDetails = (from a in assets
                                     join p in puzzles on a.AssetId equals p.AssetId into ap
                                     from p in ap.DefaultIfEmpty()
                                     select new PuzzleDetailModel()
                                     {
                                         Id = p == null ? 0 : p.Id,
                                         PuzzleId = activityId,
                                         AssetId = a.AssetId,
                                         AssetName = a.AssetName,
                                         AssetContentId = a.AssetContentId,
                                         IsSelected = p == null ? false : (p.IsEnabled ?? false),
                                         ModelId = a.ModelId,
                                         NumberOfParts = p == null ? 0 : p.NumberOfParts
                                     }).OrderByDescending(x => x.IsSelected).ThenBy(x => x.AssetName).ToList();

                foreach (var pd in act.PuzzleDetails)
                {
                    if (pd.IsSelected == true)
                    {
                        selected = selected + pd.AssetName + ", ";
                    }
                    else
                    {
                        break;
                    }
                }

                if (!string.IsNullOrEmpty(selected))
                {
                    ViewBag.SelectedAsset = selected.Substring(0, selected.Length - 2);
                }

                return View(act);
            }
        }

        [HttpPost]
        public ActionResult AddUpdateClassification(int activityId, string activityType)
        {
            DB_Entities db = new DB_Entities();
            List<AssetNameModel> listNames = new List<AssetNameModel>();
            string selected = string.Empty;
            var properties = GetProperties();
            var platforms = GetPlatforms();
            ViewBag.Language = db.CsNewLanguage.Select(x => new SelectListItem { Text = x.NewName, Value = x.NewLanguageid.ToString() }).OrderBy(x => x.Text).ToList();
            ViewBag.Package = db.CsNewPackage.Select(t => new SelectListItem { Text = t.NewName, Value = t.NewPackageid.ToString() }).OrderBy(x => x.Text).ToList();

            if (activityId == 0)
            {
                var newAct = new ClassificationModel() { ClassificationId = 0, ClassificationName = "", IsClassificationModelSpecific = false };

                newAct.Properties = (from a in properties
                                     select new ClassificationPropertyModel()
                                     {
                                         Id = 0,
                                         ClassificationId = 0,
                                         PropertyId = a.PropertyId,
                                         PropertyName = a.PropertyName,
                                         IsSelected = false
                                     }).OrderBy(x => x.PropertyName).ToList();

                if (string.IsNullOrEmpty(selected))
                {
                    ViewBag.SelectedAsset = "No property selected";
                }

                AssetNameModel name = new AssetNameModel();
                listNames.Add(name);
                newAct.Names = listNames;

                newAct.ClassificationPlatforms = platforms;

                return View(newAct);
            }
            else
            {
                var act = (from a in db.Activities
                           where a.ActivityId == activityId && a.IsDeleted == false
                           select new ClassificationModel()
                           {
                               ClassificationId = a.ActivityId,
                               ClassificationName = a.ActivityName,
                               IsClassificationModelSpecific = a.IsModelSpecific,
                               IsClassificationEnabled = a.IsEnabled,
                               IsClassificationDeleted = a.IsDeleted
                           }).FirstOrDefault();

                listNames = GetActivityNames(activityId);
                act.Names = listNames;

                act.Details = GetActivityDetails(activityId);

                var listPlatform = (from ap in db.ActivityPlatforms where ap.ActivityId == activityId select ap).ToList();
                act.ClassificationPlatforms = (from p in platforms
                                               join l in listPlatform on p.PlatformId equals l.PlatformId into ap
                                               from l in ap.DefaultIfEmpty()
                                               select new PlatformModel()
                                               {
                                                   Id = l == null ? 0 : l.Id,
                                                   PlatformId = p.PlatformId,
                                                   PlatformName = p.PlatformName,
                                                   IsSelected = l == null ? false : l.IsSelected
                                               }).OrderByDescending(x => x.IsSelected).ThenBy(x => x.PlatformName).ToList();

                var items = (from cd in db.ClassificationDetails where cd.ClassificationId == activityId && cd.IsEnabled == true select cd).ToList();
                act.Properties = (from p in properties
                                  join i in items on p.PropertyId equals i.PropertyId into pi
                                  from i in pi.DefaultIfEmpty()
                                  select new ClassificationPropertyModel()
                                  {
                                      Id = i == null ? 0 : i.Id,
                                      ClassificationId = activityId,
                                      PropertyId = p.PropertyId,
                                      PropertyName = p.PropertyName,
                                      IsSelected = i == null ? false : (i.IsEnabled ?? false)
                                  }).OrderByDescending(x => x.IsSelected).ThenBy(x => x.PropertyName).ToList();

                foreach (var p in act.Properties)
                {
                    if (p.IsSelected == true)
                    {
                        var assets = (from a in db.Asset
                                      join ac in db.AssetContent on a.AssetId equals ac.AssetId
                                      join m in db.XmlcontentModel on ac.Id equals m.AssetContentId
                                      join ap in db.AssetProperties on a.AssetId equals ap.AssetId
                                      where a.IsDeleted == false && ac.IsEnabled == true && ap.MstPropertyId == p.PropertyId
                                      select new ActivityAsset()
                                      {
                                          AssetId = a.AssetId,
                                          AssetName = a.AssetName,
                                          AssetContentId = ac.Id,
                                      }).ToList();

                        var listAssets = (from l in assets
                                          group l by l.AssetId into g
                                          select new LinkedAsset()
                                          {
                                              LinkedAssetId = g.Max(s => s.AssetId),
                                              LinkedAssetName = g.Max(s => s.AssetName),
                                              LinkedContentId = g.Max(s => s.AssetContentId),
                                          }).ToList();

                        p.ClassificationAssets = (from l in listAssets
                                                  join q in db.ClassificationAssets.Where(o => o.ClassificationDetailId == p.Id) on l.LinkedAssetId equals q.AssetId into lq
                                                  from q in lq.DefaultIfEmpty()
                                                  select new LinkedAsset()
                                                  {
                                                      RelationId = q == null ? 0 : q.Id,
                                                      LinkedAssetId = l.LinkedAssetId,
                                                      LinkedAssetName = l.LinkedAssetName,
                                                      LinkedContentId = l.LinkedContentId,
                                                      IsSelected = q == null ? false : (q.IsEnabled ?? false)
                                                  }).OrderByDescending(X => X.IsSelected).ThenBy(X => X.LinkedAssetName).ToList();

                        selected = selected + p.PropertyName + ", ";
                    }
                    else
                    {
                        break;
                    }
                }

                if (!string.IsNullOrEmpty(selected))
                {
                    ViewBag.SelectedProperties = selected.Substring(0, selected.Length - 2);
                }

                return View(act);
            }
        }

        public List<AssetNameModel> GetActivityNames(int activityID)
        {
            DB_Entities db = new DB_Entities();
            List<AssetNameModel> listNames = new List<AssetNameModel>();
            var names = db.ActivityNames.Where(x => x.ActivityId == activityID && x.IsDeleted == false).OrderBy(x => x.Id).ToList();

            listNames = names.Select(name => new AssetNameModel()
            {
                ID = name.Id,
                Name = name.Name,
                LanguageId = name.LanguageId,
                Language = db.CsNewLanguage.Where(x => x.NewLanguageid == name.LanguageId).Select(x => x.NewName).FirstOrDefault(),
                IsDeleted = name.IsDeleted
            }).ToList();

            return listNames;
        }

        public List<AssetDetailModel> GetActivityDetails(int activityID)
        {
            DB_Entities db = new DB_Entities();
            List<AssetDetailModel> listDetails = new List<AssetDetailModel>();
            var details = db.ActivityDetails.Where(x => x.ActivityId == activityID && x.IsDeleted == false).OrderBy(x => x.Id).ToList();

            listDetails = details.Select(detail => new AssetDetailModel()
            {
                ID = detail.Id,
                PackageId = detail.PackageId,
                LanguageId = detail.LanguageId,
                GradeId = detail.GradeId,
                SyllabusId = detail.SyllabusId,
                StreamId = detail.StreamId,
                SubjectId = detail.SubjectId,
                TopicId = detail.TopicId,
                SubtopicId = detail.SubtopicId,
                Package = db.CsNewPackage.Where(x => x.NewPackageid == detail.PackageId).Select(x => x.NewName).FirstOrDefault(),
                Grade = db.CsNewGrade.Where(x => x.NewGradeid == detail.GradeId).Select(x => x.NewName).FirstOrDefault(),
                Language = db.CsNewLanguage.Where(x => x.NewLanguageid == detail.LanguageId).Select(x => x.NewName).FirstOrDefault(),
                Syllabus = db.CsNewSyllabus.Where(x => x.NewSyllabusid == detail.SyllabusId).Select(x => x.NewName).FirstOrDefault(),
                Stream = db.CsNewStream.Where(x => x.NewStreamid == detail.StreamId).Select(x => x.NewName).FirstOrDefault(),
                Subject = db.CsNewSubject.Where(x => x.NewSubjectid == detail.SubjectId).Select(x => x.NewName).FirstOrDefault(),
                Topic = db.CsNewTopic.Where(x => x.NewTopicid == detail.TopicId).Select(x => x.NewName).FirstOrDefault(),
                Subtopic = db.CsNewSubtopic.Where(x => x.NewSubtopicid == detail.SubtopicId).Select(x => x.NewName).FirstOrDefault(),
                IsDeleted = detail.IsDeleted
            }).ToList();

            return listDetails;
        }

        public ActionResult GetAssetLabels(string contentId, string number)
        {
            int Id = Convert.ToInt32(contentId);
            ViewBag.AssetNum = Convert.ToInt32(number);
            DB_Entities db = new DB_Entities();
            List<Label> listLabels = new List<Label>();
            var model = (from ac in db.AssetContent
                         join x in db.XmlcontentModel
                         on ac.Id equals x.AssetContentId
                         where ac.Id == Id && ac.IsEnabled == true
                         select new QuizDetailModel()
                         {
                             Id = 0,
                             QuizId = 0,
                             AssetId = ac.AssetId ?? 0,
                             AssetName = ac.Asset.AssetName,
                             AssetContentId = ac.Id,
                             IsSelected = false,
                             ModelId = x.ModelId,
                             NumberOfAttempts = 0,
                             NumberOfLabels = 0
                         }).FirstOrDefault();

            var labels = (from a in db.XmlcontentLabel
                          where a.ModelId == model.ModelId && a.IsEnabled == true
                          select a).Distinct().ToList();

            model.QuizLabels = labels.Select(label => new QuizLabelDetailModel()
            {
                Id = 0,
                QuizDetailId = 0,
                LabelId = label.LabelId,
                LabelName = label.En,
                IsSelected = false
            }).OrderBy(X => X.LabelName).ToList();

            return PartialView("_Labels", model);
        }

        public ActionResult GetClassificationAssets(string propertyId, string property, string number)
        {
            int Id = Convert.ToInt32(propertyId);
            ViewBag.AssetNum = Convert.ToInt32(number);
            DB_Entities db = new DB_Entities();

            var model = new ClassificationPropertyModel()
            {
                Id = 0,
                ClassificationId = 0,
                PropertyId = Id,
                PropertyName = property,
                IsSelected = true
            };

            var assets = (from a in db.Asset
                          join ac in db.AssetContent on a.AssetId equals ac.AssetId
                          join m in db.XmlcontentModel on ac.Id equals m.AssetContentId
                          join ap in db.AssetProperties on a.AssetId equals ap.AssetId
                          where a.IsDeleted == false && ac.IsEnabled == true && ap.MstPropertyId == Id
                          select new ActivityAsset()
                          {
                              AssetId = a.AssetId,
                              AssetName = a.AssetName,
                              AssetContentId = ac.Id,
                              ModelId = m.ModelId
                          }).ToList();

            model.ClassificationAssets = (from l in assets
                                          group l by l.AssetId into g
                                          select new LinkedAsset()
                                          {
                                              RelationId = 0,
                                              LinkedAssetId = g.Max(s => s.AssetId),
                                              LinkedAssetName = g.Max(s => s.AssetName),
                                              LinkedContentId = g.Max(s => s.AssetContentId),
                                              IsSelected = false //to show all the assets unselected by default
                                          }).OrderBy(X => X.LinkedAssetName).ToList();

            return PartialView("_Classification", model);
        }

        public List<ActivityAsset> GetAssets()
        {
            DB_Entities db = new DB_Entities();
            List<ActivityAsset> listAssets = new List<ActivityAsset>();
            var assets = (from a in db.Asset
                          join ac in db.AssetContent on a.AssetId equals ac.AssetId
                          join m in db.XmlcontentModel on ac.Id equals m.AssetContentId
                          where a.IsDeleted == false && ac.IsEnabled == true
                          select new ActivityAsset()
                          {
                              AssetId = a.AssetId,
                              AssetName = a.AssetName,
                              AssetContentId = ac.Id,
                              ModelId = m.ModelId
                          }).ToList();

            listAssets = (from l in assets
                          group l by l.AssetId into g
                          select new ActivityAsset()
                          {
                              AssetId = g.Max(s => s.AssetId),
                              AssetName = g.Max(s => s.AssetName),
                              AssetContentId = g.Max(s => s.AssetContentId),
                              ModelId = g.Max(s => s.ModelId)
                          }).ToList();

            return listAssets;
        }

        public List<PlatformModel> GetPlatforms()
        {
            DB_Entities db = new DB_Entities();

            var platforms = (from p in db.CsNewPlatform
                             select new PlatformModel()
                             {
                                 Id = 0,
                                 PlatformId = p.Id,
                                 PlatformName = p.NewName,
                                 IsSelected = false
                             }).OrderBy(x => x.PlatformName).ToList();

            return platforms;
        }

        public List<MstProperty> GetProperties()
        {
            DB_Entities db = new DB_Entities();

            var properties = db.MstProperty.Where(x => x.IsDeleted == false).OrderBy(x => x.PropertyName).ToList();
            return properties;
        }

        public List<Label> GetLabels(int modelId)
        {
            using (DB_Entities db = new DB_Entities())
            {
                List<Label> listLabels = new List<Label>();

                var labels = (from a in db.XmlcontentLabel
                              where a.ModelId == modelId && a.IsEnabled == true
                              select a).Distinct().ToList();

                listLabels = labels.Select(label => new Label()
                {
                    LabelID = label.LabelId,
                    ModelID = modelId,
                    number = label.Number ?? 0,
                    EN = label.En,
                    NO = label.No,
                    AR = label.Ar,
                    IsEnabled = label.IsEnabled,
                    IsDeleted = label.IsDeleted
                }).ToList();

                return listLabels;
            }
        }

        public string GetLabelCount(string contentId)
        {
            string ret = "0";
            int Id = Convert.ToInt32(contentId);
            DB_Entities db = new DB_Entities();
            var labels = (from m in db.XmlcontentModel
                          join l in db.XmlcontentLabel
                          on m.ModelId equals l.ModelId
                          where m.AssetContentId == Id && l.IsEnabled == true
                          select l).ToList();

            ret = labels.Count.ToString();
            return ret;
        }

        [HttpPost]
        public ActionResult SavePuzzle(PuzzleModel puzzle)
        {
            List<AssetNameModel> newNames = puzzle.Names.Where(d => d.IsDeleted == false && (d.LanguageId == Guid.Empty || string.IsNullOrWhiteSpace(d.Name))).ToList();
            using (DB_Entities db = new DB_Entities())
            {
                if (newNames.Count > 0)
                {
                    ViewBag.Language = db.CsNewLanguage.Select(x => new SelectListItem { Text = x.NewName, Value = x.NewLanguageid.ToString() }).OrderBy(x => x.Text).ToList();
                    ViewBag.Package = db.CsNewPackage.Select(t => new SelectListItem { Text = t.NewName, Value = t.NewPackageid.ToString() }).OrderBy(x => x.Text).ToList();
                    TempData["Error"] = String.Format("Select language and provide valid name for puzzle.");
                    return View("AddUpdatePuzzle", puzzle);
                }
                else
                {
                    if (puzzle.PuzzleId == 0)
                    {
                        var selected = puzzle.PuzzleDetails.Where(x => x.IsSelected == true).ToList();

                        Activities newActivityObj = new Activities()
                        {
                            ActivityName = puzzle.Names[0].Name.Trim(),
                            Duration = puzzle.Duration,
                            LanguageId = puzzle.Names[0].LanguageId,
                            ActivityType = 2,
                            IsModelSpecific = puzzle.IsPuzzleModelSpecific,
                            IsEnabled = puzzle.IsPuzzleEnabled,
                            CreatedBy = new Guid(Session["UserID"].ToString()),
                            CreatedOn = DateTime.Now,
                            IsDeleted = puzzle.IsPuzzleDeleted
                        };
                        db.Activities.Add(newActivityObj);
                        db.SaveChanges();
                        puzzle.PuzzleId = newActivityObj.ActivityId;

                        foreach (var pd in selected)
                        {
                            PuzzleDetails newObj = new PuzzleDetails()
                            {
                                PuzzleId = newActivityObj.ActivityId,
                                AssetId = pd.AssetId,
                                AssetContentId = pd.AssetContentId,
                                ModelId = pd.ModelId,
                                NumberOfParts = pd.NumberOfParts,
                                IsEnabled = true,
                                CreatedBy = new Guid(Session["UserID"].ToString()),
                                CreatedOn = DateTime.Now,
                                IsDeleted = false

                            };
                            db.PuzzleDetails.Add(newObj);
                            db.SaveChanges();
                        }

                        puzzle.Names = SaveActivityNames(puzzle.PuzzleId, puzzle.Names);
                        puzzle.Details = SaveActivityDetails(puzzle.PuzzleId, puzzle.Details);
                        puzzle.PuzzlePlatforms = SaveActivityPlatforms(puzzle.PuzzleId, puzzle.PuzzlePlatforms);
                        TempData["Message"] = "Puzzle '" + puzzle.Names[0].Name.Trim() + "' added successfully!";
                    }
                    else
                    {
                        var activityObj = db.Activities.Where(x => x.ActivityId == puzzle.PuzzleId && x.IsDeleted == false).FirstOrDefault();
                        if (activityObj != null)
                        {
                            activityObj.ActivityName = puzzle.Names[0].Name.Trim();
                            activityObj.Duration = puzzle.Duration;
                            activityObj.LanguageId = puzzle.Names[0].LanguageId;
                            activityObj.IsModelSpecific = puzzle.IsPuzzleModelSpecific;
                            activityObj.IsEnabled = puzzle.IsPuzzleEnabled;
                            activityObj.ModifiedBy = new Guid(Session["UserID"].ToString());
                            activityObj.ModifiedOn = DateTime.Now;
                            activityObj.IsDeleted = puzzle.IsPuzzleDeleted;
                            db.Entry(activityObj).State = EntityState.Modified;
                            db.SaveChanges();
                        }

                        var selectedAssets = puzzle.PuzzleDetails.Where(x => x.IsSelected == true).ToList();
                        db.PuzzleDetails.Where(w => w.PuzzleId == puzzle.PuzzleId && w.IsEnabled == true).ToList().ForEach(i => i.IsEnabled = false);
                        db.SaveChanges();

                        foreach (var pd in selectedAssets)
                        {
                            var pObj = db.PuzzleDetails.Where(x => x.PuzzleId == puzzle.PuzzleId && x.AssetId == pd.AssetId).FirstOrDefault();
                            if (pObj != null)
                            {
                                pObj.IsEnabled = true;
                                pObj.AssetContentId = pd.AssetContentId;
                                pObj.NumberOfParts = pd.NumberOfParts;
                                pObj.ModifiedBy = new Guid(Session["UserID"].ToString());
                                pObj.ModifiedOn = DateTime.Now;
                                db.Entry(pObj).State = EntityState.Modified;
                                db.SaveChanges();
                            }
                            else
                            {
                                PuzzleDetails newObj = new PuzzleDetails()
                                {
                                    PuzzleId = puzzle.PuzzleId,
                                    AssetId = pd.AssetId,
                                    AssetContentId = pd.AssetContentId,
                                    ModelId = pd.ModelId,
                                    NumberOfParts = pd.NumberOfParts,
                                    IsEnabled = true,
                                    CreatedBy = new Guid(Session["UserID"].ToString()),
                                    CreatedOn = DateTime.Now,
                                    IsDeleted = false

                                };
                                db.PuzzleDetails.Add(newObj);
                                db.SaveChanges();
                            }
                        }

                        puzzle.Names = SaveActivityNames(puzzle.PuzzleId, puzzle.Names);
                        puzzle.Details = SaveActivityDetails(puzzle.PuzzleId, puzzle.Details);
                        puzzle.PuzzlePlatforms = SaveActivityPlatforms(puzzle.PuzzleId, puzzle.PuzzlePlatforms);
                        TempData["Message"] = "Puzzle '" + puzzle.Names[0].Name.Trim() + "' updated successfully!";
                    }
                }
            }
            return RedirectToAction("Index", "Activities");
        }

        [HttpPost]
        public ActionResult SaveQuiz(QuizModel quiz)
        {
            //if (!ModelState.IsValid)
            //{
            //    List<ModelError> errors = new List<ModelError>();
            //    foreach (ModelState modelState in ViewData.ModelState.Values)
            //    {
            //        foreach (ModelError error in modelState.Errors)
            //        {
            //            errors.Add(error);
            //        }
            //    }
            //    TempData["Message"] = "Some error occured!";
            //}
            List<AssetNameModel> newNames = quiz.Names.Where(d => d.IsDeleted == false && (d.LanguageId == Guid.Empty || string.IsNullOrWhiteSpace(d.Name))).ToList();
            using (DB_Entities db = new DB_Entities())
            {
                if (newNames.Count > 0)
                {
                    ViewBag.Language = db.CsNewLanguage.Select(x => new SelectListItem { Text = x.NewName, Value = x.NewLanguageid.ToString() }).OrderBy(x => x.Text).ToList();
                    ViewBag.Package = db.CsNewPackage.Select(t => new SelectListItem { Text = t.NewName, Value = t.NewPackageid.ToString() }).OrderBy(x => x.Text).ToList();
                    TempData["Error"] = String.Format("Select language and provide valid name for quiz.");
                    return View("AddUpdateQuiz", quiz);
                }
                else
                {
                    //This will check if user select same labels from multiple models
                    //var InvalidLabels = ValidateSelectedLabels(quiz);
                    var InvalidLabels = new List<string>();
                    if (InvalidLabels.Count == 0)
                    {
                        if (quiz.QuizId == 0)
                        {
                            Activities newActivityObj = new Activities()
                            {
                                ActivityName = quiz.Names[0].Name.Trim(),
                                Duration = quiz.Duration,
                                LanguageId = quiz.Names[0].LanguageId,
                                ActivityType = 1,
                                IsModelSpecific = quiz.IsQuizModelSpecific,
                                IsEnabled = quiz.IsQuizEnabled,
                                CreatedBy = new Guid(Session["UserID"].ToString()),
                                CreatedOn = DateTime.Now,
                                IsDeleted = quiz.IsQuizDeleted
                            };
                            db.Activities.Add(newActivityObj);
                            db.SaveChanges();
                            quiz.QuizId = newActivityObj.ActivityId;

                            var selected = quiz.QuizDetails.Where(x => x.IsSelected == true).ToList();
                            foreach (var qd in selected)
                            {
                                QuizDetails newObj = new QuizDetails()
                                {
                                    QuizId = newActivityObj.ActivityId,
                                    AssetId = qd.AssetId,
                                    AssetContentId = qd.AssetContentId,
                                    ModelId = qd.ModelId,
                                    NumberOfAttempts = qd.NumberOfAttempts,
                                    NumberOfLabels = qd.NumberOfLabels,
                                    Labels = qd.Labels,
                                    IsEnabled = true,
                                    CreatedBy = new Guid(Session["UserID"].ToString()),
                                    CreatedOn = DateTime.Now,
                                    IsDeleted = false
                                };

                                db.QuizDetails.Add(newObj);
                                db.SaveChanges();
                                qd.Id = newObj.Id;

                                foreach (var label in qd.QuizLabels)
                                {
                                    QuizLabelDetails newLabelObj = new QuizLabelDetails()
                                    {
                                        QuizDetailId = newObj.Id,
                                        LabelId = label.LabelId,
                                        LabelName = label.LabelName,
                                        IsEnabled = label.IsSelected,
                                        CreatedBy = new Guid(Session["UserID"].ToString()),
                                        CreatedOn = DateTime.Now,
                                        IsDeleted = false
                                    };

                                    db.QuizLabelDetails.Add(newLabelObj);
                                    db.SaveChanges();
                                    label.Id = newLabelObj.Id;
                                }
                            }

                            quiz.Names = SaveActivityNames(quiz.QuizId, quiz.Names);
                            quiz.Details = SaveActivityDetails(quiz.QuizId, quiz.Details);
                            quiz.Platforms = SaveActivityPlatforms(quiz.QuizId, quiz.Platforms);
                            TempData["Message"] = "Quiz '" + quiz.Names[0].Name.Trim() + "' added successfully!";
                        }
                        else
                        {
                            var activityObj = db.Activities.Where(x => x.ActivityId == quiz.QuizId && x.IsDeleted == false).FirstOrDefault();
                            if (activityObj != null)
                            {
                                activityObj.ActivityName = quiz.Names[0].Name.Trim();
                                activityObj.Duration = quiz.Duration;
                                activityObj.LanguageId = quiz.Names[0].LanguageId;
                                activityObj.IsEnabled = quiz.IsQuizEnabled;
                                activityObj.IsModelSpecific = quiz.IsQuizModelSpecific;
                                activityObj.ModifiedBy = new Guid(Session["UserID"].ToString());
                                activityObj.ModifiedOn = DateTime.Now;
                                activityObj.IsDeleted = quiz.IsQuizDeleted;
                                db.Entry(activityObj).State = EntityState.Modified;
                                db.SaveChanges();
                            }

                            quiz.Names = SaveActivityNames(quiz.QuizId, quiz.Names);
                            quiz.Details = SaveActivityDetails(quiz.QuizId, quiz.Details);
                            quiz.Platforms = SaveActivityPlatforms(quiz.QuizId, quiz.Platforms);

                            var selectedAssets = quiz.QuizDetails.Where(x => x.IsSelected == true).ToList();
                            db.QuizDetails.Where(w => w.QuizId == quiz.QuizId && w.IsEnabled == true).ToList().ForEach(i => i.IsEnabled = false);
                            db.SaveChanges();

                            foreach (var qd in selectedAssets)
                            {
                                var qObj = db.QuizDetails.Where(x => x.QuizId == quiz.QuizId && x.AssetId == qd.AssetId).FirstOrDefault();
                                if (qObj != null)
                                {
                                    qObj.AssetContentId = qd.AssetContentId;
                                    qObj.NumberOfAttempts = qd.NumberOfAttempts;
                                    qObj.NumberOfLabels = qd.NumberOfLabels;
                                    qObj.Labels = qd.Labels;
                                    qObj.IsEnabled = true;
                                    qObj.ModifiedBy = new Guid(Session["UserID"].ToString());
                                    qObj.ModifiedOn = DateTime.Now;
                                    db.Entry(qObj).State = EntityState.Modified;
                                    db.SaveChanges();

                                    db.QuizLabelDetails.Where(w => w.QuizDetailId == qObj.Id && w.IsEnabled == true).ToList().ForEach(i => i.IsEnabled = false);
                                    db.SaveChanges();
                                }
                                else
                                {
                                    QuizDetails newObj = new QuizDetails()
                                    {
                                        QuizId = quiz.QuizId,
                                        AssetId = qd.AssetId,
                                        AssetContentId = qd.AssetContentId,
                                        ModelId = qd.ModelId,
                                        NumberOfAttempts = qd.NumberOfAttempts,
                                        NumberOfLabels = qd.NumberOfLabels,
                                        Labels = qd.Labels,
                                        IsEnabled = true,
                                        CreatedBy = new Guid(Session["UserID"].ToString()),
                                        CreatedOn = DateTime.Now,
                                        IsDeleted = false
                                    };

                                    db.QuizDetails.Add(newObj);
                                    db.SaveChanges();
                                    qd.Id = newObj.Id;
                                }

                                foreach (var label in qd.QuizLabels)
                                {
                                    var qlObj = db.QuizLabelDetails.Where(x => x.QuizDetailId == qd.Id && x.LabelName.Equals(label.LabelName)).FirstOrDefault();
                                    if (qObj != null)
                                    {
                                        if (qlObj != null)
                                        {
                                            qlObj.LabelId = label.LabelId;
                                            qlObj.LabelName = label.LabelName;
                                            qlObj.IsEnabled = label.IsSelected;
                                            qlObj.ModifiedBy = new Guid(Session["UserID"].ToString());
                                            qlObj.ModifiedOn = DateTime.Now;
                                            db.Entry(qlObj).State = EntityState.Modified;
                                            db.SaveChanges();
                                        }
                                    }
                                    else
                                    {
                                        QuizLabelDetails newLabelObj = new QuizLabelDetails()
                                        {
                                            QuizDetailId = qd.Id,
                                            LabelId = label.LabelId,
                                            LabelName = label.LabelName,
                                            IsEnabled = label.IsSelected,
                                            CreatedBy = new Guid(Session["UserID"].ToString()),
                                            CreatedOn = DateTime.Now,
                                            IsDeleted = false
                                        };

                                        db.QuizLabelDetails.Add(newLabelObj);
                                        db.SaveChanges();
                                        label.Id = newLabelObj.Id;
                                    }
                                }
                            }

                            TempData["Message"] = "Quiz '" + quiz.Names[0].Name.Trim() + "' updated successfully!";
                        }
                    }
                    else
                    {
                        var str = String.Join(",", InvalidLabels);
                        TempData["Error"] = String.Format("Duplicate labels: {0}. Please select label from any one of the selected Models.", str);
                        return View("AddUpdateQuiz", quiz);
                    }
                }
            }

            return RedirectToAction("Index", "Activities");
        }

        public List<string> ValidateSelectedLabels(QuizModel quiz)
        {
            List<string> selectedLabels = new List<string>();
            var selectedAssets = quiz.QuizDetails.Where(x => x.IsSelected == true).ToList();

            foreach (var qd in selectedAssets)
            {
                selectedLabels.AddRange(qd.QuizLabels.Where(x => x.IsSelected == true).Select(x => x.LabelName).ToList());
            }

            var duplicateLabels = selectedLabels.GroupBy(x => x).Where(group => group.Count() > 1).Select(group => group.Key).ToList();
            return duplicateLabels;
        }

        [HttpPost]
        public ActionResult SaveClassification(ClassificationModel classification)
        {
            List<AssetNameModel> newNames = classification.Names.Where(d => d.IsDeleted == false && (d.LanguageId == Guid.Empty || string.IsNullOrWhiteSpace(d.Name))).ToList();
            using (DB_Entities db = new DB_Entities())
            {
                if (newNames.Count > 0)
                {
                    ViewBag.Language = db.CsNewLanguage.Select(x => new SelectListItem { Text = x.NewName, Value = x.NewLanguageid.ToString() }).OrderBy(x => x.Text).ToList();
                    ViewBag.Package = db.CsNewPackage.Select(t => new SelectListItem { Text = t.NewName, Value = t.NewPackageid.ToString() }).OrderBy(x => x.Text).ToList();
                    TempData["Error"] = String.Format("Select language and provide valid name for classification.");
                    return View("AddUpdateClassification", classification);
                }
                else
                {
                    //This will check if user select same model from multiple properties
                    var InvalidModels = ValidateSelectedModels(classification);

                    if (InvalidModels.Count == 0)
                    {
                        if (classification.ClassificationId == 0)
                        {
                            Activities newActivityObj = new Activities()
                            {
                                ActivityName = classification.Names[0].Name.Trim(),
                                Duration = 0,
                                LanguageId = classification.Names[0].LanguageId,
                                ActivityType = 3,
                                IsModelSpecific = classification.IsClassificationModelSpecific,
                                IsEnabled = classification.IsClassificationEnabled,
                                CreatedBy = new Guid(Session["UserID"].ToString()),
                                CreatedOn = DateTime.Now,
                                IsDeleted = classification.IsClassificationDeleted
                            };
                            db.Activities.Add(newActivityObj);
                            db.SaveChanges();
                            classification.ClassificationId = newActivityObj.ActivityId;

                            var selected = classification.Properties.Where(x => x.IsSelected == true).ToList();
                            foreach (var p in selected)
                            {
                                ClassificationDetails newObj = new ClassificationDetails()
                                {
                                    ClassificationId = newActivityObj.ActivityId,
                                    PropertyId = p.PropertyId,
                                    IsEnabled = true,
                                    CreatedBy = new Guid(Session["UserID"].ToString()),
                                    CreatedOn = DateTime.Now,
                                    IsDeleted = false
                                };

                                db.ClassificationDetails.Add(newObj);
                                db.SaveChanges();
                                p.Id = newObj.Id;

                                foreach (var model in p.ClassificationAssets)
                                {
                                    if (model.IsSelected)
                                    {
                                        ClassificationAssets newModelObj = new ClassificationAssets()
                                        {
                                            ClassificationDetailId = newObj.Id,
                                            AssetId = model.LinkedAssetId,
                                            AssetContentId = model.LinkedContentId,
                                            IsEnabled = model.IsSelected,
                                            CreatedBy = new Guid(Session["UserID"].ToString()),
                                            CreatedOn = DateTime.Now,
                                            IsDeleted = false
                                        };

                                        db.ClassificationAssets.Add(newModelObj);
                                        db.SaveChanges();
                                        model.RelationId = newModelObj.Id;
                                    }
                                }
                            }

                            classification.Names = SaveActivityNames(classification.ClassificationId, classification.Names);
                            classification.Details = SaveActivityDetails(classification.ClassificationId, classification.Details);
                            classification.ClassificationPlatforms = SaveActivityPlatforms(classification.ClassificationId, classification.ClassificationPlatforms);
                            TempData["Message"] = "Classification '" + classification.Names[0].Name.Trim() + "' added successfully!";
                        }
                        else
                        {
                            var activityObj = db.Activities.Where(x => x.ActivityId == classification.ClassificationId && x.IsDeleted == false).FirstOrDefault();
                            if (activityObj != null)
                            {
                                activityObj.ActivityName = classification.Names[0].Name.Trim();
                                //activityObj.Duration = classification.Duration;
                                activityObj.LanguageId = classification.Names[0].LanguageId;
                                activityObj.IsEnabled = classification.IsClassificationEnabled;
                                activityObj.IsModelSpecific = classification.IsClassificationModelSpecific;
                                activityObj.ModifiedBy = new Guid(Session["UserID"].ToString());
                                activityObj.ModifiedOn = DateTime.Now;
                                activityObj.IsDeleted = classification.IsClassificationDeleted;
                                db.Entry(activityObj).State = EntityState.Modified;
                                db.SaveChanges();
                            }

                            classification.Names = SaveActivityNames(classification.ClassificationId, classification.Names);
                            classification.Details = SaveActivityDetails(classification.ClassificationId, classification.Details);
                            classification.ClassificationPlatforms = SaveActivityPlatforms(classification.ClassificationId, classification.ClassificationPlatforms);

                            var selectedProperties = classification.Properties.Where(x => x.IsSelected == true).ToList();
                            db.ClassificationDetails.Where(w => w.ClassificationId == classification.ClassificationId && w.IsEnabled == true).ToList().ForEach(i => i.IsEnabled = false);
                            db.SaveChanges();

                            foreach (var cd in selectedProperties)
                            {
                                var qObj = db.ClassificationDetails.Where(x => x.ClassificationId == classification.ClassificationId && x.PropertyId == cd.PropertyId).FirstOrDefault();
                                if (qObj != null)
                                {
                                    qObj.IsEnabled = true;
                                    qObj.ModifiedBy = new Guid(Session["UserID"].ToString());
                                    qObj.ModifiedOn = DateTime.Now;
                                    db.Entry(qObj).State = EntityState.Modified;
                                    db.SaveChanges();

                                    db.ClassificationAssets.Where(w => w.ClassificationDetailId == qObj.Id && w.IsEnabled == true).ToList().ForEach(i => i.IsEnabled = false);
                                    db.SaveChanges();
                                }
                                else
                                {
                                    ClassificationDetails newObj = new ClassificationDetails()
                                    {
                                        ClassificationId = classification.ClassificationId,
                                        PropertyId = cd.PropertyId,
                                        IsEnabled = true,
                                        CreatedBy = new Guid(Session["UserID"].ToString()),
                                        CreatedOn = DateTime.Now,
                                        IsDeleted = false
                                    };

                                    db.ClassificationDetails.Add(newObj);
                                    db.SaveChanges();
                                    cd.Id = newObj.Id;
                                }

                                foreach (var model in cd.ClassificationAssets)
                                {
                                    if (model.IsSelected)
                                    {
                                        var qlObj = db.ClassificationAssets.Where(x => x.ClassificationDetailId == cd.Id && x.AssetId == model.LinkedAssetId).FirstOrDefault();
                                        if (qlObj != null)
                                        {
                                            qlObj.AssetContentId = model.LinkedContentId;
                                            qlObj.IsEnabled = model.IsSelected;
                                            qlObj.ModifiedBy = new Guid(Session["UserID"].ToString());
                                            qlObj.ModifiedOn = DateTime.Now;
                                            db.Entry(qlObj).State = EntityState.Modified;
                                            db.SaveChanges();
                                        }
                                        else
                                        {
                                            ClassificationAssets newModelObj = new ClassificationAssets()
                                            {
                                                ClassificationDetailId = cd.Id,
                                                AssetId = model.LinkedAssetId,
                                                AssetContentId = model.LinkedAssetId,
                                                IsEnabled = model.IsSelected,
                                                CreatedBy = new Guid(Session["UserID"].ToString()),
                                                CreatedOn = DateTime.Now,
                                                IsDeleted = false
                                            };

                                            db.ClassificationAssets.Add(newModelObj);
                                            db.SaveChanges();
                                            model.RelationId = newModelObj.Id;
                                        }
                                    }
                                }
                            }

                            TempData["Message"] = "Classification '" + classification.Names[0].Name.Trim() + "' updated successfully!";
                        }
                    }
                    else
                    {
                        if(classification.Details == null)
                        {
                            classification.Details = new List<AssetDetailModel>();
                        }
                        var str = String.Join(",", InvalidModels);
                        TempData["Error"] = String.Format("Duplicate Models: {0}. Please select model from any one of the selected properties.", str);
                        return View("AddUpdateClassification", classification);
                    }
                }
            }

            return RedirectToAction("Index", "Activities");
        }

        public List<string> ValidateSelectedModels(ClassificationModel classification)
        {
            List<string> selectedModels = new List<string>();
            var selectedProperties = classification.Properties.Where(x => x.IsSelected == true).ToList();

            foreach (var p in selectedProperties)
            {
                selectedModels.AddRange(p.ClassificationAssets.Where(x => x.IsSelected == true).Select(x => x.LinkedAssetName).ToList());
            }

            var duplicateModels = selectedModels.GroupBy(x => x).Where(group => group.Count() > 1).Select(group => group.Key).ToList();
            return duplicateModels;
        }

        public List<AssetNameModel> SaveActivityNames(int activityID, List<AssetNameModel> names)
        {
            DB_Entities db = new DB_Entities();

            db.ActivityNames.Where(w => w.ActivityId == activityID && w.IsDeleted == false).ToList().ForEach(i => i.IsDeleted = true);
            db.SaveChanges();

            foreach (AssetNameModel name in names)
            {
                if (name.ID == 0 && name.IsDeleted == false)
                {
                    ActivityNames newName = new ActivityNames()
                    {
                        ActivityId = activityID,
                        Name = name.Name,
                        LanguageId = name.LanguageId,
                        IsDeleted = name.IsDeleted
                    };
                    db.ActivityNames.Add(newName);
                    db.SaveChanges();

                    name.ID = newName.Id;
                }
                else if (name.IsDeleted == false)
                {
                    var nameObj = db.ActivityNames.Where(x => x.Id == name.ID).FirstOrDefault();
                    if (nameObj != null)
                    {
                        nameObj.Name = name.Name;
                        nameObj.LanguageId = name.LanguageId;
                        nameObj.IsDeleted = name.IsDeleted;
                        db.SaveChanges();
                    }
                }
            }

            List<AssetNameModel> newNames = names.Where(d => d.IsDeleted == false).ToList();
            return newNames;
        }

        public List<AssetDetailModel> SaveActivityDetails(int activityID, List<AssetDetailModel> details)
        {
            if (details != null)
            {
                DB_Entities db = new DB_Entities();

                db.ActivityDetails.Where(w => w.ActivityId == activityID && w.IsDeleted == false).ToList().ForEach(i => i.IsDeleted = true);
                db.SaveChanges();

                foreach (AssetDetailModel detail in details)
                {
                    if (detail.ID == 0 && detail.IsDeleted == false)
                    {
                        ActivityDetails newDetail = new ActivityDetails()
                        {
                            ActivityId = activityID,
                            PackageId = detail.PackageId,
                            LanguageId = detail.LanguageId,
                            GradeId = detail.GradeId,
                            SyllabusId = detail.SyllabusId,
                            StreamId = detail.StreamId,
                            SubjectId = detail.SubjectId,
                            TopicId = detail.TopicId,
                            SubtopicId = detail.SubtopicId,
                            IsDeleted = detail.IsDeleted
                        };
                        db.ActivityDetails.Add(newDetail);
                        db.SaveChanges();

                        detail.ID = newDetail.Id;
                    }
                    else
                    {
                        var detailObj = db.ActivityDetails.Where(x => x.Id == detail.ID).FirstOrDefault();
                        if (detailObj != null)
                        {
                            detailObj.PackageId = detail.PackageId;
                            detailObj.LanguageId = detail.LanguageId;
                            detailObj.GradeId = detail.GradeId;
                            detailObj.SyllabusId = detail.SyllabusId;
                            detailObj.StreamId = detail.StreamId;
                            detailObj.SubjectId = detail.SubjectId;
                            detailObj.TopicId = detail.TopicId;
                            detailObj.SubtopicId = detail.SubtopicId;
                            detailObj.IsDeleted = detail.IsDeleted;
                            db.SaveChanges();
                        }
                    }
                }
            }
            //List<AssetDetailModel> newDetails = details.Where(d => d.IsDeleted == false).ToList();
            return details;
        }

        public List<PlatformModel> SaveActivityPlatforms(int activityID, List<PlatformModel> platforms)
        {
            DB_Entities db = new DB_Entities();

            foreach (PlatformModel platform in platforms)
            {
                if (platform.Id == 0 && platform.IsSelected == true)
                {
                    var op = db.ActivityPlatforms.Where(x => x.ActivityId == activityID && x.PlatformId == platform.PlatformId).FirstOrDefault();
                    if (op != null)
                    {
                        op.IsSelected = platform.IsSelected;
                        db.SaveChanges();
                    }
                    else
                    {
                        ActivityPlatforms newPlatform = new ActivityPlatforms()
                        {
                            ActivityId = activityID,
                            PlatformId = platform.PlatformId,
                            IsSelected = platform.IsSelected
                        };
                        db.ActivityPlatforms.Add(newPlatform);
                        db.SaveChanges();

                        platform.Id = newPlatform.Id;
                    }
                }
                else
                {
                    var objPlatform = db.ActivityPlatforms.Where(x => x.Id == platform.Id).FirstOrDefault();
                    if (objPlatform != null)
                    {
                        objPlatform.IsSelected = platform.IsSelected;
                        db.SaveChanges();
                    }
                }
            }

            return platforms;
        }

        #region Delete
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteActivity([DataSourceRequest] DataSourceRequest request, ActivityModel activity)
        {
            ActivityModel at = new ActivityModel();
            if (activity != null)
            {
                using (DB_Entities db = new DB_Entities())
                {
                    var activityObj = db.Activities.Where(x => x.ActivityId == activity.ActivityId && x.IsDeleted == false).FirstOrDefault();
                    if (activityObj != null)
                    {
                        activityObj.IsModelSpecific = activity.IsModelSpecific;
                        activityObj.IsEnabled = activity.IsActivityEnabled;
                        activityObj.ModifiedBy = new Guid(Session["UserID"].ToString());
                        activityObj.ModifiedOn = DateTime.Now;
                        activityObj.IsDeleted = true;
                        db.Entry(activityObj).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                }
            }

            return Json(new[] { activity }.ToDataSourceResult(request, ModelState));
        }

        #endregion
    }
}