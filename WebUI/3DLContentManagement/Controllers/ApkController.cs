﻿using _3DLContentManagement.Common;
using _3DLContentManagement.Database;
using _3DLContentManagement.Models;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using TDL.ContentPlatform.Server.Database.Tables;

namespace _3DLContentManagement.Controllers
{
    public class ApkController : Controller
    {
        // GET: Apk
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetAllApks([DataSourceRequest] DataSourceRequest request)
        {
            List<ApkDetail> listApks = new List<ApkDetail>();
            using (DB_Entities db = new DB_Entities())
            {
                var apks = db.ApkDetail.Where(x => x.IsDeleted == false).OrderBy(x => x.ApkVersion).ToList();

                listApks = apks.Select(apk => new ApkDetail()
                {
                    ApkId = apk.ApkId,
                    ApkName = apk.ApkName,
                    ApkVersion = apk.ApkVersion,
                    ApkDescription = apk.ApkDescription,
                    PublishApk = apk.PublishApk,
                    IsMandatory = apk.IsMandatory,
                    IsActive = Convert.ToBoolean(apk.IsActive != null ? apk.IsActive : true),
                    IsDeleted = Convert.ToBoolean(apk.IsDeleted != null ? apk.IsDeleted : false),
                    CreatedBy = apk.CreatedBy != null ? apk.CreatedBy.Value : new Guid(),
                    CreatedOn = apk.CreatedOn != null ? apk.CreatedOn.Value : DateTime.UtcNow
                }).ToList();
            }

            var jsonResult = Json(listApks.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

        public bool ActiveApk(int Id)
        {
            using (DB_Entities db = new DB_Entities())
            {
                var apk = db.ApkDetail.Where(x => x.ApkId == Id).FirstOrDefault();
                if (apk != null)
                {
                    apk.IsActive = true;
                    db.SaveChanges();
                }
            }

            return true;
        }

        public bool InactiveApk(int Id)
        {
            using (DB_Entities db = new DB_Entities())
            {
                var apk = db.ApkDetail.Where(x => x.ApkId == Id).FirstOrDefault();
                if (apk != null)
                {
                    apk.IsActive = false;
                    db.SaveChanges();
                }
            }

            return true;
        }

        public bool DeleteApk(int Id)
        {
            using (DB_Entities db = new DB_Entities())
            {
                var apk = db.ApkDetail.Where(x => x.ApkId == Id).FirstOrDefault();
                if (apk != null)
                {
                    apk.IsDeleted = true;
                    db.SaveChanges();
                }
            }

            return true;
        }

        public ActionResult AddUpdateApk(int Id)
        {
            ApkDetail apk = new ApkDetail();
            using (DB_Entities db = new DB_Entities())
            {
                if (Id == 0)
                {
                    apk.ApkId = Id;
                    apk.IsMandatory = false;
                    apk.IsActive = true;
                    apk.IsDeleted = false;
                    ViewBag.AddedType = "Add";
                }
                else
                {
                    var record = db.ApkDetail.Where(x => x.ApkId == Id).FirstOrDefault();
                    if (record != null)
                    {
                        apk.ApkId = record.ApkId;
                        apk.ApkName = record.ApkName;
                        apk.ApkDescription = record.ApkDescription;
                        apk.ApkVersion = record.ApkVersion;
                        apk.PublishApk = record.PublishApk;
                        apk.IsMandatory = record.IsMandatory;
                        apk.IsActive = record.IsActive ?? true;
                        apk.IsDeleted = record.IsDeleted ?? false;                       
                    }

                    ViewBag.AddedType = "Edit";
                }
            }

            return View(apk);
        }

        [HttpPost]
        public async Task<ActionResult> AddUpdateApk(ApkDetail apk)
        {
            if (apk.ApkId != 0 || apk.ApkName != null)
            {
                try
                {
                    //HttpPostedFileBase UploadFile (UploadFile != null && UploadFile.ContentLength > 0)
                    //if (UploadFile != null && UploadFile.ContentLength > 0)
                    //{
                    //    string path = string.Empty;
                    //    path = System.IO.Path.Combine(WebConfigurationManager.AppSettings["PublishPath"].ToString(), "apks");

                    //    // get path including filename
                    //    path = Path.Combine(path, UploadFile.FileName);
                    //    //UploadFile.SaveAs(path);
                    //    apk.ApkName = string.IsNullOrWhiteSpace(apk.ApkName) ? UploadFile.FileName : apk.ApkName.Trim();
                    //    apk.PublishApk = path;
                    //}
                    string sourcePath = System.IO.Path.Combine(WebConfigurationManager.AppSettings["SourcePath"].ToString(), "apk");
                    string targetPath = System.IO.Path.Combine(WebConfigurationManager.AppSettings["PublishPath"].ToString(), "apks");

                    //Get the files having same name
                    string[] files = Directory.GetFiles(sourcePath, apk.ApkName);

                    if (files.Length > 0)
                    {
                        foreach (string filename in files)
                        {
                            string file = Path.Combine(targetPath, apk.ApkName);
                            using (FileStream SourceStream = System.IO.File.Open(filename, FileMode.Open, FileAccess.Read))
                            {
                                using (FileStream DestinationStream = System.IO.File.Open(file, FileMode.OpenOrCreate, FileAccess.ReadWrite))
                                {
                                    await SourceStream.CopyToAsync(DestinationStream);
                                }
                            }

                            apk.PublishApk = file;
                        }

                        using (DB_Entities db = new DB_Entities())
                        {
                            if (apk.ApkId == 0)
                            {
                                ApkDetail newApkObj = new ApkDetail()
                                {
                                    //Id = Provider.Sql.Create(),
                                    ApkName = apk.ApkName,
                                    ApkVersion = apk.ApkVersion,
                                    IsMandatory = apk.IsMandatory,
                                    PublishApk = apk.PublishApk,
                                    IsActive = apk.IsActive,
                                    CreatedBy = new Guid(Session["UserID"].ToString()),
                                    CreatedOn = DateTime.UtcNow,
                                    IsDeleted = false
                                };
                                db.ApkDetail.Add(newApkObj);
                                db.SaveChanges();

                                apk.ApkId = newApkObj.ApkId;
                                TempData["Message"] = "Apk added successfully!";
                            }
                            else
                            {
                                var apkObj = db.ApkDetail.Where(x => x.ApkId == apk.ApkId && x.IsDeleted == false).FirstOrDefault();
                                if (apkObj != null)
                                {
                                    apkObj.ApkName = apk.ApkName;
                                    apkObj.ApkVersion = apk.ApkId;
                                    apkObj.ApkDescription = apk.ApkDescription;
                                    apkObj.IsMandatory = apk.IsMandatory;
                                    apkObj.IsActive = apk.IsActive;
                                    apkObj.ModifiedBy = new Guid(Session["UserID"].ToString());
                                    apkObj.ModifiedOn = DateTime.UtcNow;
                                    apkObj.IsDeleted = apk.IsDeleted;
                                    db.Entry(apkObj).State = EntityState.Modified;
                                    db.SaveChanges();
                                }

                                TempData["Message"] = "Apk details updated successfully!";
                            }
                        }
                    }
                    else
                    {
                        TempData["Message"] = "Apk file is not available at FTP.";
                    }
                }
                catch (Exception)
                {
                    TempData["Message"] = "Exception occured.";
                }
            }
            else
            {
                TempData["Message"] = "Enter valid apk filename.";
            }            

            return View(apk);
        }
    }
}