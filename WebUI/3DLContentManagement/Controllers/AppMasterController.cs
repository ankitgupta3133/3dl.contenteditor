﻿using _3DLContentManagement.App_Start;
using _3DLContentManagement.Common;
using _3DLContentManagement.Database;
using _3DLContentManagement.Models;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.EntityFrameworkCore;
using RT.Comb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TDL.ContentPlatform.Server.Database.Tables;

namespace _3DLContentManagement.Controllers
{
    [Authorize]
    [SessionExpire]
    public class AppMasterController : Controller
    {
        // GET: AppMaster
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetAllApps([DataSourceRequest] DataSourceRequest request)
        {
            List<AppViewModel> listUsers = DataAccess.GetAppList();
            var jsonResult = Json(listUsers.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

        public bool ActiveApp(Guid Id)
        {
            using(DB_Entities db = new DB_Entities())
            {
                var record = db.AppMaster.Where(x => x.Id == Id).FirstOrDefault();
                if(record != null)
                {
                    record.IsActive = true;
                    db.SaveChanges();
                }
            }

            return true;
        }

        public bool InactiveApp(Guid Id)
        {
            using (DB_Entities db = new DB_Entities())
            {
                var record = db.AppMaster.Where(x => x.Id == Id).FirstOrDefault();
                if (record != null)
                {
                    record.IsActive = false;
                    db.SaveChanges();
                }
            }

            return true;
        }

        public bool DeleteApp(Guid Id)
        {
            using (DB_Entities db = new DB_Entities())
            {
                var record = db.AppMaster.Where(x => x.Id == Id).FirstOrDefault();
                if (record != null)
                {
                    record.IsDeleted = true;
                    db.SaveChanges();
                }
            }

            return true;
        }

        public ActionResult AddUpdateApp(Guid Id)
        {
            AppViewModel app = new AppViewModel();
            using (DB_Entities db = new DB_Entities())
            {
                var users = DataAccess.GetUsers();
                ViewBag.Users = users.Select(x => new SelectListItem { Text = x.Name, Value = x.Id.ToString() }).ToList();

                if (Id == Guid.Empty)
                {
                    app.ID = Guid.Empty;
                    app.IsActive = true;
                    app.IsDeleted = false;
                    ViewBag.AddedType = "Add";
                }
                else
                {
                    var record = db.AppMaster.Where(x => x.Id == Id).FirstOrDefault();
                    if(record != null)
                    {
                        app.ID = record.Id;
                        app.AppName = record.AppName;
                        app.AppID = record.AppId;
                        app.UserID = record.UserId;
                        app.User = users.Where(x => x.Id == record.UserId).FirstOrDefault().Name;
                        app.IsActive = record.IsActive ?? true;
                        app.IsDeleted = record.IsDeleted ?? false;
                    }
                    ViewBag.AddedType = "Edit";
                }
            }

            return View(app);
        }

        [HttpPost]
        public ActionResult AddUpdateApp(AppViewModel app)
        {
            using (DB_Entities db = new DB_Entities())
            {
                if (app.ID == Guid.Empty)
                {
                    AppMaster newAppObj = new AppMaster()
                    {
                        //Id = Provider.Sql.Create(),
                        AppId = app.AppID.Trim(),
                        AppName = app.AppName,
                        UserId = app.UserID,
                        IsActive = app.IsActive,
                        CreatedBy = new Guid(Session["UserID"].ToString()),
                        CreatedOn = DateTime.Now,
                        IsDeleted = false
                    };
                    db.AppMaster.Add(newAppObj);
                    db.SaveChanges();

                    app.ID = newAppObj.Id;
                    TempData["Message"] = "App added successfully!";
                }
                else
                {
                    var appObj = db.AppMaster.Where(x => x.Id == app.ID && x.IsDeleted == false).FirstOrDefault();
                    if (appObj != null)
                    {
                        appObj.AppId = app.AppID;
                        appObj.AppName = app.AppName;
                        appObj.UserId = app.UserID;
                        appObj.IsActive = app.IsActive;
                        appObj.ModifiedBy = new Guid(Session["UserID"].ToString());
                        appObj.ModifiedOn = DateTime.Now;
                        appObj.IsDeleted = app.IsDeleted;
                        db.Entry(appObj).State = EntityState.Modified;
                        db.SaveChanges();
                    }

                    TempData["Message"] = "App details updated successfully!";
                }

                var users = DataAccess.GetUsers();
                ViewBag.Users = users.Select(x => new SelectListItem { Text = x.Name, Value = x.Id.ToString() }).ToList();
            }

            return View(app);
        }
    }
}