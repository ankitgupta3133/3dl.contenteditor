﻿using _3DLContentManagement.App_Start;
using _3DLContentManagement.Common;
using _3DLContentManagement.Database;
using _3DLContentManagement.Models;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _3DLContentManagement.Controllers
{
    [RoleAuthorize]
    [SessionExpire]
    public class AssetController : Controller
    {
        // GET: Asset
        public ActionResult Asset()
        {
            return View();
        }

        public ActionResult Package()
        {
            return View();
        }

        public ActionResult GetAllAssets([DataSourceRequest] DataSourceRequest request)
        {
            List<AssetViewModel> listAssets = DataAccess.GetAssetDetails();
            var jsonResult = Json(listAssets.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
            //return Json(listAssets.ToDataSourceResult(request));
        }

        public ActionResult AssetPublishStatus()
        {
            return View();
        }

        public ActionResult GetAssetPublishStatus([DataSourceRequest] DataSourceRequest request)
        {
            List<AssetPublishStatusModel> listAssetPlatforms = DataAccess.GetAssetPublishStatusByPlatform();
            var jsonResult = Json(listAssetPlatforms.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;            
        }

        public ActionResult GetAllPackages([DataSourceRequest] DataSourceRequest request)
        {
            List<AssetViewModel> listPackages = DataAccess.GetPackageDetails();
            var jsonResult = Json(listPackages.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

        #region Download Excel
        [HttpPost]
        public ActionResult Excel_Export_Asset(string contentType, string base64, string fileName)
        {
            var fileContents = Convert.FromBase64String(base64);

            return File(fileContents, contentType, fileName);
        }
        #endregion

        public ActionResult About()
        {
            return View();
        }
    }
}