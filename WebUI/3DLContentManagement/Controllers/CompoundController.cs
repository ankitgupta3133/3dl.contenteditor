﻿using _3DLContentManagement.App_Start;
using _3DLContentManagement.Database;
using _3DLContentManagement.Models;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TDL.ContentPlatform.Server.Database.Tables;

namespace _3DLContentManagement.Controllers
{
    [RoleAuthorize]
    [SessionExpire]
    public class CompoundController : Controller
    {
        // GET: Compound
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetAll([DataSourceRequest] DataSourceRequest request)
        {
            List<CompoundModel> listCompounds = new List<CompoundModel>();
            using (DB_Entities db = new DB_Entities())
            {
                var compounds = db.MstCompound.Where(x => x.IsDeleted == false).Include(x => x.MstMaterial).ToList();

                listCompounds = compounds.Select(compound => new CompoundModel()
                {
                    CompoundId = compound.CompoundId,
                    CompoundName = compound.CompundName,
                    CommonName = compound.CommonName,
                    CompoundFormulae = compound.CompoundFormulae,
                    Material = compound.MstMaterial.MaterialName,
                    Color = compound.Color,
                    IsActive = compound.IsActive,
                    IsDeleted = compound.IsDeleted
                }).OrderBy(x => x.CompoundName).ToList();
            }

            return Json(listCompounds.ToDataSourceResult(request));
        }

        [HttpPost]
        public ActionResult AddUpdateCompound(int compoundId)
        {
            DB_Entities db = new DB_Entities();
            string selected = string.Empty;
            var listElements = GetElements();
            ViewBag.Materials = db.MstMaterial.Where(x => x.IsDeleted == false).Select(x => new SelectListItem { Text = x.MaterialName, Value = x.MaterialId.ToString() }).ToList();
            ViewBag.Assets = db.Asset.Where(x => x.IsDeleted == false).Select(x => new SelectListItem { Text = x.AssetName, Value = x.AssetId.ToString() }).ToList();

            if (compoundId == 0)
            {
                var newCompound = new CompoundModel() { CompoundId = 0, CompoundName = "", IsActive = true, Elements = listElements };
                return View(newCompound);
            }
            else
            {
                var objCompound = db.MstCompound.Where(a => a.CompoundId == compoundId && a.IsDeleted == false).Include(x => x.MstMaterial).Include(x => x.SphericalAssets).Include(x => x.BondedAssets).Select(a => 
                                   new CompoundModel()
                                   {
                                       CompoundId = a.CompoundId,
                                       CompoundName = a.CompundName,
                                       CommonName = a.CommonName,
                                       CompoundFormulae = a.CompoundFormulae,
                                       Color = a.Color,
                                       MaterialId = a.MaterialId,
                                       Material = a.MstMaterial.MaterialName,
                                       BondedAssetId = a.BondedAssetId,
                                       BondedAsset = a.BondedAssets.AssetName,
                                       SphericalAssetId = a.SphericalAssetId,
                                       SphericalAsset = a.SphericalAssets.AssetName,
                                       Discription = a.Discription,
                                       WikiLink = a.WikiLink,
                                       IsActive = a.IsActive,
                                       IsDeleted = a.IsDeleted
                                   }).FirstOrDefault();

                var elements = (from ce in db.CompoundElement where ce.CompoundId == compoundId select ce).ToList();
                objCompound.Elements = (from e in listElements
                                        join c in elements on e.ElementId equals c.ElementId into ce
                                     from c in ce.DefaultIfEmpty()
                                     select new CompoundElementModel()
                                     {
                                         Id = c == null ? 0 : c.Id,
                                         CompoundId = compoundId,
                                         ElementId = e.ElementId,
                                         ElementName = e.ElementName,
                                         IsSelected = c == null ? false : (!c.IsDeleted ?? false)
                                     }).OrderByDescending(x => x.IsSelected).ToList();

                return View(objCompound);
            }
        }

        public List<CompoundElementModel> GetElements()
        {
            DB_Entities db = new DB_Entities();
            var elements = (from e in db.MstElement
                          select new CompoundElementModel()
                          {
                              ElementId = e.ElementId,
                              ElementName = e.ElementName
                          }).OrderBy(x => x.ElementId).ToList();

            return elements;
        }

        [HttpPost]
        public ActionResult SaveCompound(CompoundModel compound)
        {
            if (!ModelState.IsValid)
            {
                TempData["Message"] = "Some error occured!";
            }
            else
            {
                using (DB_Entities db = new DB_Entities())
                {
                    if (compound.CompoundId == 0)
                    {
                        var selected = compound.Elements.Where(x => x.IsSelected == true).ToList();

                        MstCompound newCompoundObj = new MstCompound()
                        {
                            CompundName = compound.CompoundName.Trim(),
                            CommonName = compound.CommonName.Trim(),
                            CompoundFormulae = compound.CompoundFormulae,
                            Color = compound.Color,
                            MaterialId = compound.MaterialId,
                            BondedAssetId = compound.BondedAssetId,
                            SphericalAssetId = compound.SphericalAssetId,
                            Discription = compound.Discription,
                            WikiLink = compound.WikiLink,
                            IsActive = compound.IsActive,
                            CreatedBy = new Guid(Session["UserID"].ToString()),
                            CreatedOn = DateTime.Now,
                            IsDeleted = compound.IsDeleted
                        };
                        db.MstCompound.Add(newCompoundObj);
                        db.SaveChanges();
                        compound.CompoundId = newCompoundObj.CompoundId;

                        foreach (var el in selected)
                        {
                            CompoundElement newObj = new CompoundElement()
                            {
                                CompoundId = newCompoundObj.CompoundId,
                                ElementId = el.ElementId,
                                IsDeleted = false

                            };
                            db.CompoundElement.Add(newObj);
                            db.SaveChanges();
                        }

                        TempData["Message"] = "Compound '" + compound.CompoundName + "' added successfully!";
                    }
                    else
                    {
                        var compoundObj = db.MstCompound.Where(x => x.CompoundId == compound.CompoundId && x.IsDeleted == false).FirstOrDefault();
                        if (compoundObj != null)
                        {
                            compoundObj.CompundName = compound.CompoundName.Trim();
                            compoundObj.CommonName = compound.CommonName.Trim();
                            compoundObj.CompoundFormulae = compound.CompoundFormulae;
                            compoundObj.Color = compound.Color;
                            compoundObj.MaterialId = compound.MaterialId;
                            compoundObj.BondedAssetId = compound.BondedAssetId;
                            compoundObj.SphericalAssetId = compound.SphericalAssetId;
                            compoundObj.Discription = compound.Discription;
                            compoundObj.WikiLink = compound.WikiLink;
                            compoundObj.IsActive = compound.IsActive;
                            compoundObj.ModifiedBy = new Guid(Session["UserID"].ToString());
                            compoundObj.ModifiedOn = DateTime.Now;
                            compoundObj.IsDeleted = compound.IsDeleted;
                            db.Entry(compoundObj).State = EntityState.Modified;
                            db.SaveChanges();
                        }

                        var selectedElements = compound.Elements.Where(x => x.IsSelected == true).ToList();
                        db.CompoundElement.Where(w => w.CompoundId == compound.CompoundId && w.IsDeleted == false).ToList().ForEach(i => i.IsDeleted = true);
                        db.SaveChanges();

                        foreach (var el in selectedElements)
                        {
                            var pObj = db.CompoundElement.Where(x => x.CompoundId == compound.CompoundId && x.ElementId == el.ElementId).FirstOrDefault();
                            if (pObj != null)
                            {
                                pObj.IsDeleted = false;
                                db.Entry(pObj).State = EntityState.Modified;
                                db.SaveChanges();
                            }
                            else
                            {
                                CompoundElement newObj = new CompoundElement()
                                {
                                    CompoundId = compound.CompoundId,
                                    ElementId = el.ElementId,
                                    IsDeleted = false

                                };
                                db.CompoundElement.Add(newObj);
                                db.SaveChanges();
                            }
                        }

                        TempData["Message"] = "Compound '" + compound.CompoundName + "' updated successfully!";
                    }
                }
            }
            return RedirectToAction("Index", "Compound");
        }

        #region Delete
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteCompound([DataSourceRequest] DataSourceRequest request, CompoundModel compound)
        {
            if (compound != null)
            {
                using (DB_Entities db = new DB_Entities())
                {
                    var objCompound = db.MstCompound.Where(x => x.CompoundId == compound.CompoundId && x.IsDeleted == false).FirstOrDefault();
                    if (objCompound != null)
                    {
                        objCompound.ModifiedBy = new Guid(Session["UserID"].ToString());
                        objCompound.ModifiedOn = DateTime.Now;
                        objCompound.IsDeleted = true;
                        db.Entry(objCompound).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                }
            }

            return Json(new[] { compound }.ToDataSourceResult(request, ModelState));
        }

        #endregion
    }
}