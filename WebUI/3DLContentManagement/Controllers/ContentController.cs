﻿using _3DLContentManagement.App_Start;
using _3DLContentManagement.Common;
using _3DLContentManagement.Database;
using _3DLContentManagement.Models;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;

namespace _3DLContentManagement.Controllers
{
    [RoleAuthorize]
    [SessionExpire]
    public class ContentController : Controller
    {
        // GET: Content
        public ActionResult Index()
        {
            ViewBag.FilePath = WebConfigurationManager.AppSettings["domain"].ToString() + WebConfigurationManager.AppSettings["domain_subfolder"].ToString();

            if (!string.IsNullOrWhiteSpace(Convert.ToString(TempData["Path"])))
            {
                string path = Convert.ToString(TempData["Path"]);
                if (path.Length > 0)
                {
                    string file = path.Substring(0, path.Length - 1).Split('/').Last();
                    ViewBag.Path = path.Substring(0, path.Length - (file.Length + 1));
                    ViewBag.Folder = file;
                }
            }
            return View();
        }

        public ActionResult GetContent([DataSourceRequest] DataSourceRequest request, string path, string file, string search)
        {
            List<ContentModel> explorerModel = new List<ContentModel>();
            if (search == null || search == string.Empty || search == "")
            {
                if (TempData["Message"] != null && Convert.ToString(TempData["Message"]) != string.Empty)
                {
                    path = Convert.ToString(TempData["Path"]);
                    file = Convert.ToString(TempData["File"]);

                    if (Convert.ToString(TempData["Search"]) == "Added")
                    {
                        if (path.Length > 0)
                        {
                            file = path.Substring(0, path.Length - 1).Split('/').Last();
                        }
                        else
                        {
                            file = string.Empty;
                        }

                    }
                }
                //else if(Convert.ToString(TempData["Search"]) == "Search")
                //{

                //}
                else
                {
                    if (file != null && file != "")
                    {
                        if (path != null && path != "")
                        {
                            path = path + file + "/";
                        }
                        else
                        {
                            path = file + "/";
                        }
                    }
                    else
                    {
                        if (path.Length > 0)
                        {
                            file = path.Substring(0, path.Length - 1).Split('/').Last();
                        }
                    }
                }

                explorerModel = GetFTPDetails(path, file);
            }
            else
            {
                explorerModel = GetContentOnSearch(path, file, search);
                //if(explorerModel.Count == 0)
                //{
                //    TempData["Search"] = "Search";
                //}
            }

            ViewBag.Folder = file;
            ViewBag.Path = path;
            ViewBag.FilePath = WebConfigurationManager.AppSettings["domain"].ToString() + WebConfigurationManager.AppSettings["domain_subfolder"].ToString();
            return Json(explorerModel.ToDataSourceResult(request));
        }

        public List<ContentModel> GetFTPDetails(string path, string file)
        {
            string realPath;
            //realPath = Server.MapPath("~/3DLContentFiles/" + path);
            realPath = WebConfigurationManager.AppSettings["SourcePath"].ToString() + path;
            int pathLength = WebConfigurationManager.AppSettings["SourcePath"].ToString().Length;

            List<ContentModel> contentListModel = new List<ContentModel>();

            if (System.IO.Directory.Exists(realPath))
            {
                DB_Entities db = new DB_Entities();
                IEnumerable<string> dirList = Directory.EnumerateDirectories(realPath);
                foreach (string dir in dirList)
                {
                    DirectoryInfo d = new DirectoryInfo(dir);
                    ContentModel dirModel = new ContentModel();

                    dirModel.Name = Path.GetFileName(dir);
                    long dirSize = CommonMethods.GetDirectorySize(dir);
                    dirModel.Size = dirSize.ToString();
                    //dirModel.DirSizeText = (dirSize < 1024) ? dirSize.ToString() + " B" : ((dirSize / 1024) < 1024 ? dirSize / 1024 + " KB" : (dirSize / 1024) / 1024 + " MB");
                    dirModel.SizeText = CommonMethods.GetBytesReadable(dirSize);
                    dirModel.AccessedOn = d.LastAccessTime;
                    dirModel.Path = path;
                    dirModel.Folder = file;

                    string dirPath = dir.Substring(pathLength, dir.Length - pathLength);
                    int count = db.AssetContent.Where(x => x.IsDeleted == false && x.FilePath.Equals(dirPath)).ToList().Count;
                    if (count == 0)
                    {
                        dirPath = dirPath + "/";
                        count = db.AssetContent.Where(x => x.IsDeleted == false && x.FilePath.Substring(0, dirPath.Length).Contains(dirPath)).ToList().Count;
                    }
                    dirModel.Status = count == 0 ? "Not Linked" : "Linked";
                    dirModel.IsDirectory = true;

                    contentListModel.Add(dirModel);
                }

                IEnumerable<string> fileList = Directory.EnumerateFiles(realPath);
                foreach (string files in fileList)
                {
                    FileInfo f = new FileInfo(files);

                    ContentModel fileModel = new ContentModel();

                    fileModel.Name = Path.GetFileName(files);
                    fileModel.AccessedOn = f.LastAccessTime;
                    fileModel.Path = path;
                    fileModel.Size = f.Length.ToString();
                    //fileModel.FileSizeText = (f.Length < 1024) ? f.Length.ToString() + " B" : ((f.Length / 1024) < 1024 ? f.Length / 1024 + " KB" : (f.Length / 1024) / 1024 + " MB");
                    fileModel.SizeText = CommonMethods.GetBytesReadable(f.Length);
                    string filePath = files.Substring(pathLength, files.Length - pathLength);
                    int count = db.AssetContent.Where(x => x.IsDeleted == false && x.FilePath.Substring(0, filePath.Length).Contains(filePath)).ToList().Count;
                    fileModel.Status = count == 0 ? "Not Linked" : "Linked";
                    fileModel.IsDirectory = false;
                    fileModel.Folder = file;

                    contentListModel.Add(fileModel);
                }
            }

            return contentListModel;
        }

        public List<ContentModel> GetContentOnSearch(string path, string file, string search)
        {
            string realPath;
            List<ContentModel> explorerModel = new List<ContentModel>();
            int pLength = WebConfigurationManager.AppSettings["SourcePath"].ToString().Length;
            realPath = WebConfigurationManager.AppSettings["SourcePath"].ToString() + path;
            int pathLength = realPath.Replace('/', '\\').Length;

            if (System.IO.File.Exists(realPath))
            {
                return explorerModel;
            }
            else if (System.IO.Directory.Exists(realPath))
            {
                DB_Entities db = new DB_Entities();
                IEnumerable<string> dirList = Directory.GetDirectories(realPath, "**", SearchOption.AllDirectories);
                foreach (string dir in dirList)
                {
                    DirectoryInfo d = new DirectoryInfo(dir);
                    ContentModel dirModel = new ContentModel();

                    dirModel.Name = Path.GetFileName(dir);
                    if (dirModel.Name.ToLower().Contains(search.ToLower()))
                    {
                        long dirSize = CommonMethods.GetDirectorySize(dir);
                        string prevPath = path;
                        dirModel.Path = prevPath + d.FullName.Substring(pathLength, d.FullName.Length - (pathLength + dirModel.Name.Length)).Replace('\\', '/');
                        dirModel.Size = dirSize.ToString();
                        dirModel.SizeText = CommonMethods.GetBytesReadable(dirSize);
                        dirModel.AccessedOn = d.LastAccessTime;
                        dirModel.IsDirectory = true;
                        dirModel.Folder = file;

                        string dirPath = dir.Substring(pLength, dir.Length - pLength).Replace('\\', '/');
                        int count = db.AssetContent.Where(x => x.IsDeleted == false && x.FilePath.Equals(dirPath)).ToList().Count;
                        if (count == 0)
                        {
                            dirPath = dirPath + "/";
                            count = db.AssetContent.Where(x => x.IsDeleted == false && x.FilePath.Substring(0, dirPath.Length).Contains(dirPath)).ToList().Count;
                        }
                        dirModel.Status = count == 0 ? "Not Linked" : "Linked";

                        explorerModel.Add(dirModel);
                    }
                }

                IEnumerable<string> fileList = Directory.GetFiles(realPath, "*.*", SearchOption.AllDirectories);
                foreach (string files in fileList)
                {
                    FileInfo f = new FileInfo(files);
                    ContentModel fileModel = new ContentModel();

                    fileModel.Name = Path.GetFileName(files);
                    if (fileModel.Name.ToLower().Contains(search.ToLower()))
                    {
                        fileModel.Path = f.Directory.FullName.Substring(pLength, f.Directory.FullName.Length - (pLength)).Replace('\\', '/');
                        if (fileModel.Path.Length > 0)
                        {
                            fileModel.Path = fileModel.Path + '/';
                        }
                        fileModel.AccessedOn = f.LastAccessTime;
                        fileModel.Size = f.Length.ToString();
                        fileModel.SizeText = CommonMethods.GetBytesReadable(f.Length);
                        string filePath = files.Substring(pLength, files.Length - pLength).Replace('\\', '/');
                        int count = db.AssetContent.Where(x => x.IsDeleted == false && x.FilePath.Substring(0, filePath.Length).Contains(filePath)).ToList().Count;
                        fileModel.Status = count == 0 ? "Not Linked" : "Linked";
                        fileModel.IsDirectory = false;
                        fileModel.Folder = file;

                        explorerModel.Add(fileModel);
                    }
                }
            }

            return explorerModel;
        }
    }
}