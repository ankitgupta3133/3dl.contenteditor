﻿using _3DLContentManagement.App_Start;
using _3DLContentManagement.Common;
using _3DLContentManagement.Database;
using _3DLContentManagement.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Configuration;
using System.Web.Mvc;
using TDL.ContentPlatform.Server.Database.Tables;

namespace _3DLContentManagement.Controllers
{
    [RoleAuthorize]
    [SessionExpire]
    public class GenerateXMLController : Controller
    {
        // GET: GenerateXML
        public ActionResult Index(int? AssetContentID)
        {
            DB_Entities db = new DB_Entities();
            XmlFormModel model = new XmlFormModel();



            List<Label> listLabels = new List<Label>();
            ViewBag.Language = db.CsNewLanguage.Select(x => new SelectListItem { Text = x.NewName, Value = x.NewLanguageid.ToString() }).OrderBy(x => x.Text).ToList();
            XmlcontentModel objModel = db.XmlcontentModel.Where(x => x.AssetContentId == AssetContentID).FirstOrDefault();

            if (objModel != null)
            {
                List<XmlFormModelNamesModel> xmlcontentModelNames = (from a in db.XmlcontentModelNames
                                                                     join b in db.CsNewLanguage on a.LanguageId equals b.NewLanguageid into TL
                                                                     from b in TL.DefaultIfEmpty()
                                                                     where a.ModelId == objModel.ModelId && a.IsDeleted == false
                                                                     select new XmlFormModelNamesModel()
                                                                     {
                                                                         Id = a.Id,
                                                                         IsDeleted = a.IsDeleted,
                                                                         LanguageId = a.LanguageId,
                                                                         Language = b.NewName,
                                                                         Name = a.ModelName
                                                                     }).ToList();

                if (xmlcontentModelNames.Count > 0)
                {
                    model.ModelNames = xmlcontentModelNames;
                }
                else
                {
                    XmlFormModelNamesModel formNamesModel = new XmlFormModelNamesModel() { Id = 0, Name = string.Empty };
                    model.ModelNames = new List<XmlFormModelNamesModel>();
                    model.ModelNames.Add(formNamesModel);
                }

                model.ModelID = objModel.ModelId;
                model.AssetContentID = objModel.AssetContentId.Value;
                model.ModelName = objModel.ModelName;
                model.hidingposX = objModel.HidingposX.Value;//.ToString("0.#########");
                model.hidingposY = objModel.HidingposY.Value;
                model.hidingposZ = objModel.HidingposZ.Value;
                model.IsEnabled = objModel.IsEnabled;
                model.IsDeleted = objModel.IsDeleted;
                model.CreatedBy = objModel.CreatedBy;
                model.CreatedOn = objModel.CreatedOn;

                listLabels = GetXmlLabelList(model.ModelID);
            }
            else
            {
                model.ModelID = 0;
                model.AssetContentID = AssetContentID.Value;

                XmlFormModelNamesModel formNamesModel = new XmlFormModelNamesModel() { Id = 0, Name = string.Empty };
                model.ModelNames = new List<XmlFormModelNamesModel>();
                model.ModelNames.Add(formNamesModel);

                LabelNamesModel labelNamesModel = new LabelNamesModel() { ID = 0, Name = string.Empty };
                Label label = new Label();
                label.LabelNames = new List<LabelNamesModel>();
                label.LabelNames.Add(labelNamesModel);
                label.LabelID = 0;
                label.number = 1;
                label.IsEnabled = true;
                listLabels.Add(label);
            }

            var assetObj = db.AssetContent.Where(x => x.Id == AssetContentID && x.IsDeleted == false).Include(x => x.Asset).FirstOrDefault();
            ViewBag.AssetName = assetObj.Asset.AssetName;
            ViewBag.AssetID = assetObj.AssetId;

            model.Labels = listLabels;
            return View(model);
        }

        [HttpPost]
        public ActionResult Index(XmlFormModel xmlModel)
        {
            if (ModelState.IsValid)
            {
                using (DB_Entities db = new DB_Entities())
                {
                    var obj = db.XmlcontentModel.Where(x => x.AssetContentId == xmlModel.AssetContentID).FirstOrDefault();
                    if (xmlModel.ModelID == 0 && obj == null)
                    {
                        List<XmlFormModelNamesModel> modelNamesToBeSaved = xmlModel.ModelNames.Where(a => a.IsDeleted == false).ToList();

                        for (int i = 0; i < modelNamesToBeSaved.Count; i++)
                        {
                            if (i == 0)
                            {
                                XmlcontentModel newModel = new XmlcontentModel()
                                {
                                    ModelName = modelNamesToBeSaved.ElementAt(0).Name,//xmlModel.ModelName,
                                    AssetContentId = xmlModel.AssetContentID,
                                    HidingposX = 0.000000001M,
                                    HidingposY = 0.000000001M,
                                    HidingposZ = 0.000000001M,
                                    IsEnabled = true,
                                    CreatedBy = new Guid(Session["UserID"].ToString()),
                                    CreatedOn = DateTime.Now,
                                    IsDeleted = false
                                };

                                if (xmlModel.ModelNames.Where(a => a.LanguageId == new Guid(Constants.LanguageCodeEnglish)).Count() > 0)
                                {
                                    newModel.En = xmlModel.ModelNames.Where(a => a.LanguageId == new Guid(Constants.LanguageCodeEnglish)).Select(a => a.Name).FirstOrDefault();
                                }

                                if (xmlModel.ModelNames.Where(a => a.LanguageId == new Guid(Constants.LanguageCodeNorwegian)).Count() > 0)
                                {
                                    newModel.No = xmlModel.ModelNames.Where(a => a.LanguageId == new Guid(Constants.LanguageCodeNorwegian)).Select(a => a.Name).FirstOrDefault();
                                }

                                if (xmlModel.ModelNames.Where(a => a.LanguageId == new Guid(Constants.LanguageCodeNynorsk)).Count() > 0)
                                {
                                    newModel.Nn = xmlModel.ModelNames.Where(a => a.LanguageId == new Guid(Constants.LanguageCodeNynorsk)).Select(a => a.Name).FirstOrDefault();
                                }

                                db.XmlcontentModel.Add(newModel);
                                db.SaveChanges();
                                xmlModel.ModelID = newModel.ModelId;
                            }

                            //Model Name
                            XmlcontentModelNames newXmlcontentModelName = new XmlcontentModelNames()
                            {
                                IsDeleted = false,
                                ModelName = modelNamesToBeSaved.ElementAt(i).Name,
                                LanguageId = modelNamesToBeSaved.ElementAt(i).LanguageId,
                                ModelId = xmlModel.ModelID
                            };

                            db.XmlcontentModelNames.Add(newXmlcontentModelName);
                        }

                        db.SaveChanges();
                        xmlModel.Labels = SaveXmlLabels(xmlModel.ModelID, xmlModel.Labels);
                    }
                    else if (xmlModel.ModelID == 0 && obj != null)
                    {
                        List<XmlFormModelNamesModel> modelNamesToBeSaved = xmlModel.ModelNames.Where(a => a.IsDeleted == false).ToList();

                        for (int i = 0; i < modelNamesToBeSaved.Count; i++)
                        {
                            if (i == 0)
                            {
                                obj.ModelName = modelNamesToBeSaved.ElementAt(0).Name;//xmlModel.ModelName;
                                //obj.En = xmlModel.EN;
                                //obj.No = xmlModel.NO;
                                //obj.Ar = xmlModel.AR;
                                obj.HidingposX = xmlModel.hidingposX;
                                obj.HidingposY = xmlModel.hidingposY;
                                obj.HidingposZ = xmlModel.hidingposZ;
                                obj.IsEnabled = xmlModel.IsEnabled;
                                obj.ModifiedBy = new Guid(Session["UserID"].ToString());
                                obj.ModifiedOn = DateTime.Now;
                                obj.IsDeleted = xmlModel.IsDeleted;

                                if (xmlModel.ModelNames.Where(a => a.LanguageId == new Guid(Constants.LanguageCodeEnglish)).Count() > 0)
                                {
                                    obj.En = xmlModel.ModelNames.Where(a => a.LanguageId == new Guid(Constants.LanguageCodeEnglish)).Select(a => a.Name).FirstOrDefault();
                                }

                                if (xmlModel.ModelNames.Where(a => a.LanguageId == new Guid(Constants.LanguageCodeNorwegian)).Count() > 0)
                                {
                                    obj.No = xmlModel.ModelNames.Where(a => a.LanguageId == new Guid(Constants.LanguageCodeNorwegian)).Select(a => a.Name).FirstOrDefault();
                                }
                                if (xmlModel.ModelNames.Where(a => a.LanguageId == new Guid(Constants.LanguageCodeNynorsk)).Count() > 0)
                                {
                                    obj.Nn = xmlModel.ModelNames.Where(a => a.LanguageId == new Guid(Constants.LanguageCodeNynorsk)).Select(a => a.Name).FirstOrDefault();
                                }


                                db.Entry(obj).State = EntityState.Modified;
                                db.SaveChanges();

                                xmlModel.ModelID = obj.ModelId;
                            }

                            //Model Name
                            XmlcontentModelNames newXmlcontentModelName = new XmlcontentModelNames()
                            {
                                IsDeleted = modelNamesToBeSaved.ElementAt(i).IsDeleted,
                                ModelName = modelNamesToBeSaved.ElementAt(i).Name,
                                LanguageId = modelNamesToBeSaved.ElementAt(i).LanguageId,
                                ModelId = xmlModel.ModelID
                            };

                            db.XmlcontentModelNames.Add(newXmlcontentModelName);
                        }

                        db.SaveChanges();
                        xmlModel.Labels = SaveXmlLabels(obj.ModelId, xmlModel.Labels);
                    }
                    else
                    {
                        var modelObj = db.XmlcontentModel.Where(x => x.ModelId == xmlModel.ModelID).FirstOrDefault();
                        if (modelObj != null)
                        {
                            modelObj.ModelName = xmlModel.ModelNames.Where(a => a.IsDeleted == false).ElementAt(0).Name;// xmlModel.ModelName;
                            //modelObj.En = xmlModel.EN;
                            //modelObj.No = xmlModel.NO;
                            //modelObj.Ar = xmlModel.AR;
                            modelObj.HidingposX = xmlModel.hidingposX;
                            modelObj.HidingposY = xmlModel.hidingposY;
                            modelObj.HidingposZ = xmlModel.hidingposZ;
                            modelObj.IsEnabled = xmlModel.IsEnabled;
                            modelObj.ModifiedBy = new Guid(Session["UserID"].ToString());
                            modelObj.ModifiedOn = DateTime.Now;
                            modelObj.IsDeleted = xmlModel.IsDeleted;

                            if (xmlModel.ModelNames.Where(a => a.LanguageId == new Guid(Constants.LanguageCodeEnglish)).Count() > 0)
                            {
                                modelObj.En = xmlModel.ModelNames.Where(a => a.LanguageId == new Guid(Constants.LanguageCodeEnglish)).Select(a => a.Name).FirstOrDefault();
                            }

                            if (xmlModel.ModelNames.Where(a => a.LanguageId == new Guid(Constants.LanguageCodeNorwegian)).Count() > 0)
                            {
                                modelObj.No = xmlModel.ModelNames.Where(a => a.LanguageId == new Guid(Constants.LanguageCodeNorwegian)).Select(a => a.Name).FirstOrDefault();
                            }
                            if (xmlModel.ModelNames.Where(a => a.LanguageId == new Guid(Constants.LanguageCodeNynorsk)).Count() > 0)
                            {
                                modelObj.Nn = xmlModel.ModelNames.Where(a => a.LanguageId == new Guid(Constants.LanguageCodeNynorsk)).Select(a => a.Name).FirstOrDefault();
                            }


                            db.Entry(modelObj).State = EntityState.Modified;
                            db.SaveChanges();

                            for (int i = 0; i < xmlModel.ModelNames.Count; i++)
                            {
                                var modelNameObj = db.XmlcontentModelNames.Where(a => a.Id == xmlModel.ModelNames.ElementAt(i).Id).FirstOrDefault();
                                if (modelNameObj != null)
                                {
                                    modelNameObj.IsDeleted = xmlModel.ModelNames.ElementAt(i).IsDeleted;
                                    modelNameObj.ModelName = xmlModel.ModelNames.ElementAt(i).Name;
                                    modelNameObj.LanguageId = xmlModel.ModelNames.ElementAt(i).LanguageId;
                                }
                                else
                                {
                                    XmlcontentModelNames newXmlcontentModelName = new XmlcontentModelNames()
                                    {
                                        IsDeleted = xmlModel.ModelNames.ElementAt(i).IsDeleted,
                                        ModelName = xmlModel.ModelNames.ElementAt(i).Name,
                                        LanguageId = xmlModel.ModelNames.ElementAt(i).LanguageId,
                                        ModelId = xmlModel.ModelID
                                    };

                                    db.XmlcontentModelNames.Add(newXmlcontentModelName);
                                }
                            }
                            db.SaveChanges();

                            xmlModel.Labels = SaveXmlLabels(modelObj.ModelId, xmlModel.Labels);
                        }
                    }

                    var assetObj = db.AssetContent.Where(x => x.Id == xmlModel.AssetContentID && x.IsDeleted == false).Include(x => x.Asset).FirstOrDefault();
                    ViewBag.AssetName = assetObj.Asset.AssetName;
                    ViewBag.AssetID = assetObj.AssetId;
                }
            }

            return RedirectToAction("Index", new { AssetContentID = xmlModel.AssetContentID });
        }

        public List<Label> GetXmlLabelList(int ModelID)
        {
            DB_Entities db = new DB_Entities();
            List<Label> listLabels = new List<Label>();
            var labels = db.XmlcontentLabel.Where(x => x.ModelId == ModelID).Include(a => a.XmlContentLabelNames).OrderBy(x => x.LabelId).ToList();

            //LabelNamesModel labelNamesModel = new LabelNamesModel() { ID = 0, Name = string.Empty };
            List<LabelNamesModel> labelNamesModel = new List<LabelNamesModel>();
            labelNamesModel.Add(new LabelNamesModel()
            {
                ID = 0,
                Name = string.Empty
            });

            listLabels = labels.Select(label => new Label()
            {
                LabelID = label.LabelId,
                ModelID = label.ModelId != null ? label.ModelId.Value : 0,
                LabelName = label.LabelName,
                face = label.Face != null ? label.Face.Value : 0,
                number = label.Number != null ? label.Number.Value : 0,
                EN = label.En,
                NO = label.No,
                AR = label.Ar,
                NN = label.Nn,
                highlight = label.Highlight,
                original = label.Original,
                xPosition = Convert.ToDecimal(label.XPosition),
                yPosition = Convert.ToDecimal(label.YPosition),
                zPosition = Convert.ToDecimal(label.ZPosition),
                xRotation = Convert.ToDecimal(label.XRotation),
                yRotation = Convert.ToDecimal(label.YRotation),
                zRotation = Convert.ToDecimal(label.ZRotation),
                xScale = Convert.ToDecimal(label.XScale),
                yScale = Convert.ToDecimal(label.YScale),
                zScale = Convert.ToDecimal(label.ZScale),//label.zScale != null ? label.zScale.Value.ToString("0.####") : null,
                IsEnabled = Convert.ToBoolean(label.IsEnabled != null ? label.IsEnabled : true),
                IsDeleted = Convert.ToBoolean(label.IsDeleted != null ? label.IsDeleted : false),
                CreatedBy = label.CreatedBy != null ? label.CreatedBy.Value : new Guid(),
                CreatedOn = label.CreatedOn != null ? label.CreatedOn.Value : DateTime.UtcNow,
                ModifiedBy = label.ModifiedBy != null ? label.ModifiedBy.Value : new Guid(),
                ModifiedOn = label.ModifiedOn != null ? label.ModifiedOn.Value : DateTime.UtcNow,
                LabelNames = label.XmlContentLabelNames != null && label.XmlContentLabelNames.Count > 0 ? label.XmlContentLabelNames.Where(x => x.IsDeleted == false).Select(a => new LabelNamesModel()
                {
                    Description = a.Description,
                    ID = a.LabelNameId,
                    LanguageId = a.LanguageId,
                    Name = a.LabelName,
                    Language = db.CsNewLanguage.Where(x => x.NewLanguageid == a.LanguageId).Select(x => x.NewName).FirstOrDefault()
                }).ToList() : labelNamesModel
            }).ToList();

            return listLabels;
        }

        public List<Label> SaveXmlLabels(int modelID, List<Label> labels)
        {
            DB_Entities db = new DB_Entities();
            foreach (Label label in labels)
            {
                if (label.LabelID == 0)
                {
                    XmlcontentLabel newLabel = new XmlcontentLabel()
                    {
                        ModelId = modelID,
                        LabelName = label.LabelName,
                        Face = label.face,
                        Number = label.number,
                        //En = label.EN,
                        //No = label.NO,
                        //Ar = label.AR,
                        Highlight = label.highlight,
                        Original = label.original,
                        XPosition = Convert.ToString(label.xPosition),
                        YPosition = Convert.ToString(label.yPosition),
                        ZPosition = Convert.ToString(label.zPosition),
                        XRotation = Convert.ToString(label.xRotation),
                        YRotation = Convert.ToString(label.yRotation),
                        ZRotation = Convert.ToString(label.zRotation),
                        XScale = Convert.ToString(label.xScale),
                        YScale = Convert.ToString(label.yScale),
                        ZScale = Convert.ToString(label.zScale),
                        IsEnabled = label.IsEnabled,
                        CreatedBy = new Guid(Session["UserID"].ToString()),
                        CreatedOn = DateTime.Now,
                        IsDeleted = false

                    };

                    if (label.LabelNames.Where(a => a.LanguageId == new Guid(Constants.LanguageCodeEnglish)).Count() > 0)
                    {
                        newLabel.En = label.LabelNames.Where(a => a.LanguageId == new Guid(Constants.LanguageCodeEnglish)).Select(a => a.Name).FirstOrDefault();
                    }

                    if (label.LabelNames.Where(a => a.LanguageId == new Guid(Constants.LanguageCodeNorwegian)).Count() > 0)
                    {
                        newLabel.No = label.LabelNames.Where(a => a.LanguageId == new Guid(Constants.LanguageCodeNorwegian)).Select(a => a.Name).FirstOrDefault();
                    }
                    if (label.LabelNames.Where(a => a.LanguageId == new Guid(Constants.LanguageCodeNynorsk)).Count() > 0)
                    {
                        newLabel.Nn = label.LabelNames.Where(a => a.LanguageId == new Guid(Constants.LanguageCodeNynorsk)).Select(a => a.Name).FirstOrDefault();
                    }


                    db.XmlcontentLabel.Add(newLabel);
                    db.SaveChanges();

                    foreach (var labelName in label.LabelNames.Where(a => a.IsDeleted == false))
                    {
                        XmlContentLabelNames xmlLabelName = new XmlContentLabelNames()
                        {
                            IsDeleted = false,
                            LabelId = newLabel.LabelId,
                            LabelName = labelName.Name,
                            LanguageId = labelName.LanguageId,
                            Description = labelName.Description
                        };

                        db.XmlContentLabelNames.Add(xmlLabelName);
                    }

                    db.SaveChanges();

                    label.LabelID = newLabel.LabelId;
                }
                else
                {
                    var labelObj = db.XmlcontentLabel.Where(x => x.LabelId == label.LabelID).Include(a => a.XmlContentLabelNames).FirstOrDefault();
                    if (labelObj != null)
                    {
                        labelObj.Face = label.face;
                        labelObj.Number = label.number;
                        //labelObj.En = label.EN;
                        //labelObj.No = label.NO;
                        //labelObj.Ar = label.AR;
                        labelObj.Highlight = label.highlight;
                        labelObj.Original = label.original;
                        labelObj.XPosition = Convert.ToString(label.xPosition);
                        labelObj.YPosition = Convert.ToString(label.yPosition);
                        labelObj.ZPosition = Convert.ToString(label.zPosition);
                        labelObj.XRotation = Convert.ToString(label.xRotation);
                        labelObj.YRotation = Convert.ToString(label.yRotation);
                        labelObj.ZRotation = Convert.ToString(label.zRotation);
                        labelObj.XScale = Convert.ToString(label.xScale);
                        labelObj.YScale = Convert.ToString(label.yScale);
                        labelObj.ZScale = Convert.ToString(label.zScale);
                        labelObj.IsEnabled = label.IsEnabled;
                        labelObj.ModifiedBy = new Guid(Session["UserID"].ToString());
                        labelObj.ModifiedOn = DateTime.Now;
                        labelObj.IsDeleted = label.IsDeleted;

                        if (label.LabelNames.Where(a => a.LanguageId == new Guid(Constants.LanguageCodeEnglish)).Count() > 0)
                        {
                            labelObj.En = label.LabelNames.Where(a => a.LanguageId == new Guid(Constants.LanguageCodeEnglish)).Select(a => a.Name).FirstOrDefault();
                        }

                        if (label.LabelNames.Where(a => a.LanguageId == new Guid(Constants.LanguageCodeNorwegian)).Count() > 0)
                        {
                            labelObj.No = label.LabelNames.Where(a => a.LanguageId == new Guid(Constants.LanguageCodeNorwegian)).Select(a => a.Name).FirstOrDefault();
                        }

                        if (label.LabelNames.Where(a => a.LanguageId == new Guid(Constants.LanguageCodeNynorsk)).Count() > 0)
                        {
                            labelObj.Nn = label.LabelNames.Where(a => a.LanguageId == new Guid(Constants.LanguageCodeNynorsk)).Select(a => a.Name).FirstOrDefault();
                        }

                        db.Entry(labelObj).State = EntityState.Modified;
                        db.SaveChanges();

                        List<XmlContentLabelNames> labelNames = labelObj.XmlContentLabelNames.ToList();
                        if (labelNames.Count > 0)
                        {
                            labelNames.ForEach((a) =>
                            {
                                a.IsDeleted = true;
                            });
                        }
                        db.SaveChanges();

                        if (label.LabelNames != null)
                        {
                            foreach (var labelName in label.LabelNames.Where(a => a.IsDeleted == false))
                            {
                                XmlContentLabelNames xmlLabelName = new XmlContentLabelNames()
                                {
                                    IsDeleted = false,
                                    LabelId = labelObj.LabelId,
                                    LabelName = labelName.Name,
                                    LanguageId = labelName.LanguageId,
                                    Description = labelName.Description
                                };

                                db.XmlContentLabelNames.Add(xmlLabelName);
                            }
                            db.SaveChanges();
                        }
                    }
                }
            }

            return labels;
        }

        public ActionResult GetNewLabel(int Id)
        {
            XmlFormModel model = new XmlFormModel();

            List<Label> labels = new List<Label>();

            using (DB_Entities db = new DB_Entities())
            {
                LabelNamesModel labelNamesModel = new LabelNamesModel() { ID = 0, Name = string.Empty };
                Label label = new Label();
                label.LabelNames = new List<LabelNamesModel>();
                label.LabelNames.Add(labelNamesModel);

                label.LabelID = 0;
                label.ModelID = Id;
                label.number = db.XmlcontentLabel.Where(x => x.ModelId == Id).ToList().Count + 1;

                labels.Add(label);
                model.Labels = labels;

                return PartialView("_AddLabel", model);
            }
        }

        [HttpPost]
        public ActionResult SaveNewLabel(XmlFormModel model)
        {
            DB_Entities db = new DB_Entities();
            if (ModelState.IsValid)
            {
                if (model.Labels[0].LabelID == 0)
                {
                    XmlcontentLabel newLabel = new XmlcontentLabel()
                    {
                        ModelId = model.Labels[0].ModelID,
                        LabelName = model.Labels[0].LabelNames.ElementAt(0).Name,
                        Face = model.Labels[0].face,
                        Number = model.Labels[0].number,
                        //En = model.Labels[0].EN,
                        //No = model.Labels[0].NO,
                        //Ar = model.Labels[0].AR,
                        Highlight = model.Labels[0].highlight,
                        Original = model.Labels[0].original,
                        XPosition = Convert.ToString(model.Labels[0].xPosition),
                        YPosition = Convert.ToString(model.Labels[0].yPosition),
                        ZPosition = Convert.ToString(model.Labels[0].zPosition),
                        XRotation = Convert.ToString(model.Labels[0].xRotation),
                        YRotation = Convert.ToString(model.Labels[0].yRotation),
                        ZRotation = Convert.ToString(model.Labels[0].zRotation),
                        XScale = Convert.ToString(model.Labels[0].xScale),
                        YScale = Convert.ToString(model.Labels[0].yScale),
                        ZScale = Convert.ToString(model.Labels[0].zScale),
                        IsEnabled = model.Labels[0].IsEnabled,
                        CreatedBy = new Guid(Session["UserID"].ToString()),
                        CreatedOn = DateTime.Now,
                        IsDeleted = false

                    };

                    if (model.Labels[0].LabelNames.Where(a => a.LanguageId == new Guid(Constants.LanguageCodeEnglish)).Count() > 0)
                    {
                        newLabel.En = model.Labels[0].LabelNames.Where(a => a.LanguageId == new Guid(Constants.LanguageCodeEnglish)).Select(a => a.Name).FirstOrDefault();
                    }

                    if (model.Labels[0].LabelNames.Where(a => a.LanguageId == new Guid(Constants.LanguageCodeNorwegian)).Count() > 0)
                    {
                        newLabel.No = model.Labels[0].LabelNames.Where(a => a.LanguageId == new Guid(Constants.LanguageCodeNorwegian)).Select(a => a.Name).FirstOrDefault();
                    }

                    db.XmlcontentLabel.Add(newLabel);
                    db.SaveChanges();

                    foreach (var labelName in model.Labels[0].LabelNames.Where(a => a.IsDeleted == false))
                    {
                        XmlContentLabelNames xmlLabelName = new XmlContentLabelNames()
                        {
                            IsDeleted = false,
                            LabelId = newLabel.LabelId,
                            LabelName = labelName.Name,
                            LanguageId = labelName.LanguageId,
                            Description = labelName.Description
                        };

                        db.XmlContentLabelNames.Add(xmlLabelName);
                    }

                    db.SaveChanges();
                    model.Labels[0].LabelID = newLabel.LabelId;
                }
                else
                {
                    var labelObj = db.XmlcontentLabel.Where(x => x.LabelId == model.Labels[0].LabelID).Include(a => a.XmlContentLabelNames).FirstOrDefault();
                    if (labelObj != null)
                    {
                        labelObj.Face = model.Labels[0].face;
                        labelObj.Number = model.Labels[0].number;
                        //labelObj.En = model.Labels[0].EN;
                        //labelObj.No = model.Labels[0].NO;
                        //labelObj.Ar = model.Labels[0].AR;
                        labelObj.Highlight = model.Labels[0].highlight;
                        labelObj.Original = model.Labels[0].original;
                        labelObj.XPosition = Convert.ToString(model.Labels[0].xPosition);
                        labelObj.YPosition = Convert.ToString(model.Labels[0].yPosition);
                        labelObj.ZPosition = Convert.ToString(model.Labels[0].zPosition);
                        labelObj.XRotation = Convert.ToString(model.Labels[0].xRotation);
                        labelObj.YRotation = Convert.ToString(model.Labels[0].yRotation);
                        labelObj.ZRotation = Convert.ToString(model.Labels[0].zRotation);
                        labelObj.XScale = Convert.ToString(model.Labels[0].xScale);
                        labelObj.YScale = Convert.ToString(model.Labels[0].yScale);
                        labelObj.ZScale = Convert.ToString(model.Labels[0].zScale);
                        labelObj.IsEnabled = model.Labels[0].IsEnabled;
                        labelObj.ModifiedBy = new Guid(Session["UserID"].ToString());
                        labelObj.ModifiedOn = DateTime.Now;
                        labelObj.IsDeleted = model.Labels[0].IsDeleted;

                        if (model.Labels[0].LabelNames.Where(a => a.LanguageId == new Guid(Constants.LanguageCodeEnglish)).Count() > 0)
                        {
                            labelObj.En = model.Labels[0].LabelNames.Where(a => a.LanguageId == new Guid(Constants.LanguageCodeEnglish)).Select(a => a.Name).FirstOrDefault();
                        }

                        if (model.Labels[0].LabelNames.Where(a => a.LanguageId == new Guid(Constants.LanguageCodeNorwegian)).Count() > 0)
                        {
                            labelObj.No = model.Labels[0].LabelNames.Where(a => a.LanguageId == new Guid(Constants.LanguageCodeNorwegian)).Select(a => a.Name).FirstOrDefault();
                        }

                        db.Entry(labelObj).State = EntityState.Modified;
                        db.SaveChanges();

                        List<XmlContentLabelNames> labelNames = labelObj.XmlContentLabelNames.ToList();
                        if (labelNames.Count > 0)
                        {
                            labelNames.ForEach((a) =>
                            {
                                a.IsDeleted = true;
                            });
                        }

                        foreach (var labelName in model.Labels[0].LabelNames.Where(a => a.IsDeleted == false))
                        {
                            XmlContentLabelNames xmlLabelName = new XmlContentLabelNames()
                            {
                                IsDeleted = false,
                                LabelId = labelObj.LabelId,
                                LabelName = labelName.Name,
                                LanguageId = labelName.LanguageId,
                                Description = labelName.Description
                            };

                            db.XmlContentLabelNames.Add(xmlLabelName);
                        }

                        db.SaveChanges();
                    }
                }
            }
            else
            {
                return PartialView("_AddLabel", model);
            }

            var modelObj = db.XmlcontentModel.Where(x => x.ModelId == model.Labels[0].ModelID).FirstOrDefault();
            int Id = modelObj.AssetContentId.Value;
            return RedirectToAction("Index", new { @AssetContentID = Id });
        }

        [HttpPost]
        public ActionResult CreateData(int? Id)
        {
            string fileName = CommonMethods.GenerateXml(Id);

            var doc = DataAccess.GetXML(Id);
            string fullText = string.Empty;
            foreach (GetLabel text in doc)
            {
                fullText += text.Label;
            }

            return File(Encoding.UTF8.GetBytes(fullText), "application/xml", fileName);
        }

        [HttpPost]
        public string SaveLabelDescription(int Id, string Desc)
        {
            string message = string.Empty;
            using (DB_Entities db = new DB_Entities())
            {
                var labelNameObj = db.XmlContentLabelNames.Where(x => x.LabelNameId == Id).FirstOrDefault();
                if (labelNameObj != null)
                {
                    labelNameObj.Description = Desc;
                    db.SaveChanges();
                    message = "Description saved successfully";
                }
                else
                {
                    message = "Error|Label Name not found!";
                }

                return message;
            }
        }
    }
}