﻿using _3DLContentManagement.App_Start;
using _3DLContentManagement.Common;
using _3DLContentManagement.Database;
using _3DLContentManagement.Models;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using TDL.ContentPlatform.Server.Common.Models.Communication;
using TDL.ContentPlatform.Server.Database.Tables;
using static _3DLContentManagement.Common.HttpRestClient;

namespace _3DLContentManagement.Controllers
{
    [RoleAuthorize]
    [SessionExpire]
    public class HomeController : Controller
    {
        public async Task<ActionResult> Index(string path, string file)
        {
            if ((TempData["Message"] != null && Convert.ToString(TempData["Message"]) != string.Empty) ||
                    (TempData["Search"] != null && Convert.ToString(TempData["Search"]) != string.Empty))
            {
                path = Convert.ToString(TempData["Path"]);
                file = Convert.ToString(TempData["File"]);

                if (Convert.ToString(TempData["Search"]) == "Added")
                {
                    if (path.Length > 0)
                    {
                        file = path.Substring(0, path.Length - 1).Split('/').Last();
                    }
                    else
                    {
                        file = string.Empty;
                    }

                }
            }
            else
            {
                if (file != null && file != "")
                {
                    if (path != null && path != "")
                    {
                        path = path + file + "/";
                    }
                    else
                    {
                        path = file + "/";
                    }
                }
            }

            ViewBag.Folder = file == "" ? null : file;
            ViewBag.Path = path;
            ViewBag.FilePath = WebConfigurationManager.AppSettings["domain"].ToString() + WebConfigurationManager.AppSettings["domain_subfolder"].ToString() + path;

            ExplorerModel explorerModel = await Task.Run(() => GetFTPDetails(path));
            return View(explorerModel);
        }

        [HttpPost]
        public ActionResult Index(string path, string file, string search)
        {
            ViewBag.Folder = file;
            ViewBag.Path = path;
            ViewBag.FilePath = WebConfigurationManager.AppSettings["domain"].ToString() + WebConfigurationManager.AppSettings["domain_subfolder"].ToString();// + path;

            string realPath;
            int pLength = WebConfigurationManager.AppSettings["SourcePath"].ToString().Length;
            realPath = WebConfigurationManager.AppSettings["SourcePath"].ToString() + path;
            //if(realPath.Contains('/'))
            //{
            //    realPath = realPath.Substring(0, realPath.Length - 1);
            //}
            //realPath.Replace('/', '\\');
            int pathLength = realPath.Replace('/', '\\').Length;

            List<FileModel> fileListModel = new List<FileModel>();
            List<DirModel> dirListModel = new List<DirModel>();

            if (System.IO.File.Exists(realPath))
            {
                return base.File(realPath, "application/octet-stream");
            }
            else if (System.IO.Directory.Exists(realPath))
            {
                if (search == null || search == string.Empty || search == "")
                {
                    TempData["Search"] = "Search";
                    TempData["Path"] = path;
                    TempData["File"] = file;
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    int iMatchCount = 0;
                    DB_Entities db = new DB_Entities();
                    IEnumerable<string> dirList = Directory.GetDirectories(realPath, "**", SearchOption.AllDirectories);
                    foreach (string dir in dirList)
                    {
                        DirectoryInfo d = new DirectoryInfo(dir);

                        DirModel dirModel = new DirModel();

                        dirModel.DirName = Path.GetFileName(dir);
                        if (dirModel.DirName.ToLower().Contains(search.ToLower()))
                        {
                            iMatchCount++;
                            long dirSize = CommonMethods.GetDirectorySize(dir);
                            string prevPath = path; //path == null || path == "" ? path : path.Substring(0, path.Length-1);
                            dirModel.DirPath = prevPath + d.FullName.Substring(pathLength, d.FullName.Length - (pathLength + dirModel.DirName.Length)).Replace('\\', '/');
                            dirModel.DirSize = dirSize.ToString();
                            //dirModel.DirSizeText = (dirSize < 1024) ? dirSize.ToString() + " B" : ((dirSize / 1024) < 1024 ? dirSize / 1024 + " KB" : (dirSize / 1024) / 1024 + " MB");
                            dirModel.DirSizeText = CommonMethods.GetBytesReadable(dirSize);
                            dirModel.DirAccessed = d.LastAccessTime;

                            string dirPath = dir.Substring(pLength, dir.Length - pLength).Replace('\\', '/');
                            int count = db.AssetContent.Where(x => x.IsDeleted == false && x.FilePath.Equals(dirPath)).ToList().Count;
                            if (count == 0)
                            {
                                dirPath = dirPath + "/";
                                count = db.AssetContent.Where(x => x.IsDeleted == false && x.FilePath.Substring(0, dirPath.Length).Contains(dirPath)).ToList().Count;
                            }
                            dirModel.Status = count == 0 ? "Not Linked" : "Linked";

                            dirListModel.Add(dirModel);
                        }
                    }

                    IEnumerable<string> fileList = Directory.GetFiles(realPath, "*.*", SearchOption.AllDirectories);
                    foreach (string files in fileList)
                    {
                        FileInfo f = new FileInfo(files);

                        FileModel fileModel = new FileModel();

                        fileModel.FileName = Path.GetFileName(files);
                        if (fileModel.FileName.ToLower().Contains(search.ToLower()))
                        {
                            iMatchCount++;
                            //string fullpath = f.Directory.FullName + "\\";
                            fileModel.FilePath = f.Directory.FullName.Substring(pLength, f.Directory.FullName.Length - (pLength)).Replace('\\', '/');
                            if (fileModel.FilePath.Length > 0)
                            {
                                fileModel.FilePath = fileModel.FilePath + '/';
                            }
                            fileModel.FileAccessed = f.LastAccessTime;
                            fileModel.FileSize = f.Length.ToString();
                            //fileModel.FileSizeText = (f.Length < 1024) ? f.Length.ToString() + " B" : ((f.Length / 1024) < 1024 ? f.Length / 1024 + " KB" : (f.Length / 1024) / 1024 + " MB");
                            fileModel.FileSizeText = CommonMethods.GetBytesReadable(f.Length);
                            string filePath = files.Substring(pLength, files.Length - pLength).Replace('\\', '/');
                            int count = db.AssetContent.Where(x => x.IsDeleted == false && x.FilePath.Substring(0, filePath.Length).Contains(filePath)).ToList().Count;
                            fileModel.Status = count == 0 ? "Not Linked" : "Linked";

                            fileListModel.Add(fileModel);
                        }
                    }

                    if (iMatchCount == 0)
                    {
                        TempData["Message"] = "Oops! No items match your Search text.";
                        TempData["Path"] = path;
                        TempData["File"] = file;
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        ViewBag.Search = "Search";
                        ViewBag.Folder = file == "" ? null : file;
                        TempData["Message"] = iMatchCount + " item(s) match your Search text \"" + search + "\"";
                    }

                    ExplorerModel explorerModel = new ExplorerModel(dirListModel, fileListModel);
                    return View(explorerModel);
                }
            }
            else
            {
                ExplorerModel explorerModel = new ExplorerModel(dirListModel, fileListModel);
                return View(explorerModel);
            }
        }

        public ExplorerModel GetFTPDetails(string path)
        {
            string realPath;
            //realPath = Server.MapPath("~/3DLContentFiles/" + path);
            realPath = WebConfigurationManager.AppSettings["SourcePath"].ToString() + path;
            int pathLength = WebConfigurationManager.AppSettings["SourcePath"].ToString().Length;

            List<FileModel> fileListModel = new List<FileModel>();
            List<DirModel> dirListModel = new List<DirModel>();

            if (System.IO.Directory.Exists(realPath))
            {
                DB_Entities db = new DB_Entities();
                IEnumerable<string> dirList = Directory.EnumerateDirectories(realPath);
                foreach (string dir in dirList)
                {
                    DirectoryInfo d = new DirectoryInfo(dir);
                    DirModel dirModel = new DirModel();

                    dirModel.DirName = Path.GetFileName(dir);
                    long dirSize = CommonMethods.GetDirectorySize(dir);
                    dirModel.DirSize = dirSize.ToString();
                    //dirModel.DirSizeText = (dirSize < 1024) ? dirSize.ToString() + " B" : ((dirSize / 1024) < 1024 ? dirSize / 1024 + " KB" : (dirSize / 1024) / 1024 + " MB");
                    dirModel.DirSizeText = CommonMethods.GetBytesReadable(dirSize);
                    dirModel.DirAccessed = d.LastAccessTime;
                    dirModel.DirPath = path;

                    string dirPath = dir.Substring(pathLength, dir.Length - pathLength);
                    int count = db.AssetContent.Where(x => x.IsDeleted == false && x.FilePath.Equals(dirPath)).ToList().Count;
                    if (count == 0)
                    {
                        dirPath = dirPath + "/";
                        count = db.AssetContent.Where(x => x.IsDeleted == false && x.FilePath.Substring(0, dirPath.Length).Contains(dirPath)).ToList().Count;
                    }
                    dirModel.Status = count == 0 ? "Not Linked" : "Linked";

                    dirListModel.Add(dirModel);
                }

                IEnumerable<string> fileList = Directory.EnumerateFiles(realPath);
                foreach (string files in fileList)
                {
                    FileInfo f = new FileInfo(files);

                    FileModel fileModel = new FileModel
                    {
                        FileName = Path.GetFileName(files),
                        FileAccessed = f.LastAccessTime,
                        FilePath = path,
                        FileSize = f.Length.ToString(),
                        //fileModel.FileSizeText = (f.Length < 1024) ? f.Length.ToString() + " B" : ((f.Length / 1024) < 1024 ? f.Length / 1024 + " KB" : (f.Length / 1024) / 1024 + " MB");
                        FileSizeText = CommonMethods.GetBytesReadable(f.Length)
                    };
                    string filePath = files.Substring(pathLength, files.Length - pathLength);
                    int count = db.AssetContent.Where(x => x.IsDeleted == false && x.FilePath.Substring(0, filePath.Length).Contains(filePath)).ToList().Count;
                    fileModel.Status = count == 0 ? "Not Linked" : "Linked";

                    fileListModel.Add(fileModel);
                }
            }

            ExplorerModel explorerModel = new ExplorerModel(dirListModel, fileListModel);
            return explorerModel;
        }

        public ExplorerModel GetDirectoryDetails(string path)
        {
            string realPath, filePath;
            realPath = WebConfigurationManager.AppSettings["SourcePath"].ToString() + path;
            int pathLength = realPath.Replace('/', '\\').Length;

            List<DirModel> dirListModel = new List<DirModel>();
            List<FileModel> fileListModel = new List<FileModel>();

            if (System.IO.Directory.Exists(realPath))
            {
                IEnumerable<string> dirList = Directory.EnumerateDirectories(realPath);
                foreach (string dir in dirList)
                {
                    DirModel dirModel = new DirModel();
                    DirectoryInfo d = new DirectoryInfo(dir);
                    dirModel.DirName = Path.GetFileName(dir);
                    filePath = path + d.FullName.Substring(pathLength, d.FullName.Length - (pathLength + dirModel.DirName.Length)).Replace('\\', '/');
                    dirModel.DirPath = filePath == "/" ? "" : filePath;
                    long dirSize = CommonMethods.GetDirectorySize(dir);
                    dirModel.DirSize = dirSize.ToString();
                    //dirModel.DirSizeText = (dirSize < 1024) ? dirSize.ToString() + " B" : ((dirSize / 1024) < 1024 ? dirSize / 1024 + " KB" : (dirSize / 1024) / 1024 + " MB");
                    dirModel.DirSizeText = CommonMethods.GetBytesReadable(dirSize);
                    dirListModel.Add(dirModel);
                }

                IEnumerable<string> fileList = Directory.EnumerateFiles(realPath);
                foreach (string file in fileList)
                {
                    FileModel fileModel = new FileModel();
                    FileInfo f = new FileInfo(file);
                    fileModel.FileName = Path.GetFileName(file);
                    fileModel.FilePath = path + f.Directory.FullName.Substring(pathLength, f.Directory.FullName.Length - (pathLength)).Replace('\\', '/');
                    if (fileModel.FilePath.Length > 0)
                    {
                        fileModel.FilePath = fileModel.FilePath + '/';
                    }
                    fileModel.FileSize = f.Length.ToString();
                    //fileModel.FileSizeText = (f.Length < 1024) ? f.Length.ToString() + " B" : ((f.Length / 1024) < 1024 ? f.Length / 1024 + " KB" : (f.Length / 1024) / 1024 + " MB");
                    fileModel.FileSizeText = CommonMethods.GetBytesReadable(f.Length);
                    fileListModel.Add(fileModel);
                }
            }
            ExplorerModel explorerModel = new ExplorerModel(dirListModel, fileListModel);
            return explorerModel;
        }
        //FTP connection(explicit)
        private List<string> FTPDetail(string FileName)
        {
            string uri = "";
            string Host = WebConfigurationManager.AppSettings["host"].ToString();
            string Username = WebConfigurationManager.AppSettings["username"].ToString();
            string Password = WebConfigurationManager.AppSettings["password"].ToString();
            //int Port = Convert.ToInt32(WebConfigurationManager.AppSettings["port"].ToString());
            uri = "ftp://" + Host + "//" + FileName;

            FtpWebRequest objFTP;
            objFTP = (FtpWebRequest)WebRequest.Create(new Uri(uri));
            objFTP.EnableSsl = true;
            objFTP.Credentials = new NetworkCredential(Username, Password);
            objFTP.UsePassive = true;
            objFTP.KeepAlive = false;
            objFTP.Proxy = null;
            objFTP.UseBinary = false;
            objFTP.Timeout = 20000;

            objFTP.Method = WebRequestMethods.Ftp.ListDirectory;
            FtpWebResponse response = (FtpWebResponse)objFTP.GetResponse();
            Stream responseStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(responseStream);
            string names = reader.ReadToEnd();

            reader.Close();
            response.Close();

            return names.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries).ToList();
        }

        public ActionResult Assets(string path, string file)
        {
            TempData["Path"] = path;
            TempData["File"] = file;
            ViewBag.FilePath = WebConfigurationManager.AppSettings["domain"].ToString() + WebConfigurationManager.AppSettings["domain_subfolder"].ToString();
            return View();
        }

        public ActionResult MasterData()
        {
            return View();
        }
        #region Upload thumbnail image
        [HttpPost]
        public ActionResult UploadFiles(HttpPostedFileBase UploadFile, string txtPath, string txtFile, int txtFileType)
        {
            if (UploadFile != null && UploadFile.ContentLength > 0)
                try
                {
                    string path = string.Empty;
                    if (txtFileType == 1)
                    {
                        path = WebConfigurationManager.AppSettings["SourcePath"].ToString() + txtPath + txtFile;
                    }
                    else
                    {
                        path = WebConfigurationManager.AppSettings["SourcePath"].ToString() + txtPath;
                    }

                    path = Path.Combine(path, Path.GetFileName(UploadFile.FileName));

                    UploadFile.SaveAs(path);
                    TempData["Message"] = "File uploaded successfully";
                }
                catch (Exception ex)
                {
                    TempData["Message"] = "ERROR:" + ex.Message.ToString();
                }
            else
            {
                TempData["Message"] = "You have not specified a file.";
            }

            TempData["Path"] = txtPath;
            TempData["File"] = txtFile;
            return RedirectToAction("Index", "Home");
        }
        #endregion

        #region Select
        public ActionResult GetAll([DataSourceRequest] DataSourceRequest request)
        {
            string FileName = Convert.ToString(TempData.Peek("File"));
            string FilePath = Convert.ToString(TempData.Peek("Path")) + FileName;

            List<AssetViewModel> listAssets = new List<AssetViewModel>();
            using (DB_Entities db = new DB_Entities())
            {
                //This is for if user clicked on Linked button against file
                var assets = db.AssetContent.Where(x => x.IsDeleted == false && (x.FilePath.Equals(FilePath) || x.FilePath.Contains(FilePath + "/"))).Select(x => x.Asset).Distinct().OrderBy(x => x.AssetName).ToList();

                //This is for if user clicked on Asset List
                if (string.IsNullOrEmpty(FilePath))
                {
                    assets = db.Asset.Where(x => x.IsDeleted == false && x.FilePath.Contains(FilePath)).OrderBy(x => x.AssetName).ToList();
                }

                listAssets = assets.Select(asset => new AssetViewModel()
                {
                    AssetID = asset.AssetId,
                    AssetName = asset.AssetName,
                    FileName = asset.FileName,//.Substring(asset.FileName.LastIndexOf('/') + 1, asset.FileName.Length - (asset.FileName.LastIndexOf('/') + 1)),
                    FilePath = asset.FilePath,
                    FileSize = Convert.ToInt64(asset.FileSize),
                    FileSizeText = CommonMethods.GetBytesReadable(Convert.ToInt64(asset.FileSize)),
                    IsDirectory = asset.IsDirectory,
                    Status = asset.Status,
                    AssetType = asset.Type,
                    Language = asset.Language,
                    Syllabus = asset.Syllabus,
                    Subject = asset.Subject,
                    Level = asset.Level,
                    Topic = asset.Topic,
                    Subtopic = asset.Subtopic,
                    SubtopicDesc = asset.SubtopicDesc,
                    //Tags = asset.Tags,
                    //Properties = asset.Properties,
                    ContentPackageID = asset.PackageId,
                    IsQuizEnabled = asset.IsQuizEnabled ?? false,
                    PercentageOfLabels = asset.PercentageOfLabels,
                    NumberOfAttempts = asset.NumberOfAttempts,
                    IsActive = Convert.ToBoolean(asset.IsActive ?? true),
                    IsDeleted = Convert.ToBoolean(asset.IsDeleted ?? false),
                    CreatedBy = asset.CreatedBy != null ? asset.CreatedBy.Value : new Guid(),
                    CreatedOn = asset.CreatedOn != null ? asset.CreatedOn.Value : DateTime.UtcNow,
                    ModifiedOn = asset.ModifiedOn != null ? asset.ModifiedOn.Value : DateTime.UtcNow,
                    ModifiedByName = asset.ModifiedByName //!= null ? asset.ModifiedByName : db.UserProfiles.FirstOrDefault(x => x.UserId == asset.CreatedBy).FirstName
                    //MultipleAssetName = CommonMethods.GetAssetNames(asset.AssetId)
                    //MultipleLanguage = CommonMethods.GetAssetLanguages(asset.AssetId),
                    //MultipleLevel = CommonMethods.GetAssetGrades(asset.AssetId),
                    //MultipleSyllabus = CommonMethods.GetAssetSyllabus(asset.AssetId),
                    //MultipleStream = CommonMethods.GetAssetStreams(asset.AssetId),
                    //MultipleSubject = CommonMethods.GetAssetSubjects(asset.AssetId),
                    //MultipleTopic = CommonMethods.GetAssetTopics(asset.AssetId),
                    //MultipleSubtopic = CommonMethods.GetAssetSubtopics(asset.AssetId)
                }).ToList();
            }

            return Json(listAssets.ToDataSourceResult(request));
        }

        #endregion

        #region Delete
        public async Task<bool> DeleteAssets(int Id)
        {
            bool status = false;
            using (DB_Entities db = new DB_Entities())
            {
                var assetObj = db.Asset.Where(x => x.AssetId == Id && x.IsDeleted == false).FirstOrDefault();
                if (assetObj != null)
                {
                    assetObj.IsDeleted = true;
                    assetObj.ModifiedOn = DateTime.Now;
                    assetObj.ModifiedBy = new Guid(Session["UserID"].ToString());
                    assetObj.ModifiedByName = Session["Name"].ToString();
                    db.Entry(assetObj).State = EntityState.Modified;
                    await db.SaveChangesAsync();

                    status = true;
                    string strAction = "Asset deleted";
                    string strActivity = string.Format("Asset titled '{0}' deleted.", assetObj.AssetName);
                    await ActivityLogger.AddActivityLog(Id, strAction, strActivity);
                }
            }

            return status;
        }

        #endregion

        public bool CheckAssets(string AssetName, int Id)
        {
            using (DB_Entities db = new DB_Entities())
            {
                int count = 0;

                if (Id != 0)
                    count = db.Asset.Where(p => p.AssetName == AssetName.Trim() && p.IsDeleted == false && p.AssetId != Id).Count();
                else
                    count = db.Asset.Where(p => p.AssetName == AssetName.Trim() && p.IsDeleted == false).Count();

                return count > 0;
            }
        }

        [HttpPost]
        public ActionResult AddAsset(string path, string file, string size, int type)
        {
            TempData["Type"] = type;
            TempData["Path"] = path;
            TempData["File"] = file;
            TempData["Size"] = size;
            return RedirectToAction("AddUpdateAsset");
        }

        public async Task<ActionResult> AddUpdateAsset(int? assetID)
        {
            DB_Entities db = new DB_Entities();
            Asset objAsset = new Asset();
            if (assetID != null && assetID != 0)
            {
                objAsset = db.Asset.FirstOrDefault(x => x.AssetId == assetID);
                if (objAsset.Type == "Module")
                {
                    return RedirectToAction("Index", "Module", new { @assetID = objAsset.AssetId });
                }
            }

            AssetViewModel asset = new AssetViewModel();
            List<AssetContentModel> listContents = new List<AssetContentModel>();
            List<AssetNameModel> listNames = new List<AssetNameModel>();
            List<AssetDetailModel> listDetails = new List<AssetDetailModel>();
            List<Label> listLabels = new List<Label>();
            await GetDataListFromSessions();
            asset.FileName = Convert.ToString(TempData.Peek("File"));
            asset.FilePath = Convert.ToString(TempData.Peek("Path")) + asset.FileName;
            asset.FileSize = Convert.ToInt64(TempData.Peek("Size"));
            asset.IsDirectory = Convert.ToBoolean(TempData.Peek("Type"));
            ViewBag.AddedType = "Add";

            if (assetID != null && assetID != 0)
            {
                CsNewPackage objPackage = db.CsNewPackage.FirstOrDefault(x => x.Id == objAsset.PackageId);

                listNames = await GetAssetNames(objAsset.AssetId);
                listDetails = await GetAssetDetails(objAsset.AssetId);
                listContents = await GetAssetContentList(objAsset.AssetId);

                asset.AssetID = objAsset.AssetId;
                asset.AssetName = objAsset.AssetName;
                asset.FileName = objAsset.FileName;
                asset.FilePath = objAsset.FilePath;
                asset.FileSize = Convert.ToInt64(objAsset.FileSize);
                asset.FileSizeText = CommonMethods.GetBytesReadable(Convert.ToInt64(objAsset.FileSize));
                asset.IsDirectory = objAsset.IsDirectory;
                asset.AssetType = objAsset.Type;
                asset.Language = objAsset.Language;
                asset.Syllabus = objAsset.Syllabus;
                asset.Status = objAsset.Status;
                asset.Stream = objAsset.Stream;
                asset.Subject = objAsset.Subject;
                asset.Level = objAsset.Level;
                asset.Topic = objAsset.Topic;
                asset.Subtopic = objAsset.Subtopic;
                asset.SubtopicDesc = objAsset.SubtopicDesc;
                asset.VimeoUrl = objAsset.VimeoUrl;
                asset.IsQuizEnabled = objAsset.IsQuizEnabled ?? false;
                asset.PercentageOfLabels = objAsset.PercentageOfLabels;
                asset.NumberOfAttempts = objAsset.NumberOfAttempts;
                asset.ContentPackageID = objAsset.PackageId;
                asset.Package = objPackage?.NewName;
                asset.IsPublished = db.AssetContent.Where(x => x.AssetId == assetID && x.IsEnabled == true).ToList().Count > 0 ? true : false;
                asset.IsActive = objAsset.IsActive;
                asset.IsDeleted = objAsset.IsDeleted;
                asset.CreatedBy = objAsset.CreatedBy;
                asset.CreatedOn = objAsset.CreatedOn;

                ViewBag.TagsSelectList = new SelectList(CommonMethods.GetAssetTags(asset.AssetID), "Value", "Text");
                ViewBag.PropertiesSelectList = new SelectList(CommonMethods.GetAssetProperties(asset.AssetID), "Value", "Text");

                ViewBag.AddedType = "Edit";
            }
            else
            {
                AssetContentModel content = new AssetContentModel();
                content.ID = 0;
                content.FileName = Convert.ToString(TempData.Peek("File"));
                content.FilePath = Convert.ToString(TempData.Peek("Path")) + asset.FileName;
                content.FileSize = Convert.ToString(TempData.Peek("Size"));
                content.FileSizeText = CommonMethods.GetBytesReadable(Convert.ToInt64(content.FileSize));
                content.IsDirectory = Convert.ToBoolean(TempData.Peek("Type"));
                content.PlatformId = db.CsNewPlatform.Where(x => x.NewName.Equals("All")).Select(x => x.NewPlatformid.ToString()).FirstOrDefault();
                content.VersionNumber = 1;
                listContents.Add(content);

                AssetNameModel name = new AssetNameModel();
                listNames.Add(name);

                AssetDetailModel detail = new AssetDetailModel();
                listDetails.Add(detail);


                ViewBag.TagsSelectList = new SelectList(CommonMethods.GetAssetTags(asset.AssetID), "Value", "Text");
                ViewBag.PropertiesSelectList = new SelectList(CommonMethods.GetAssetProperties(asset.AssetID), "Value", "Text");
            }

            asset.Names = listNames;
            asset.Details = listDetails;
            asset.Contents = listContents;

            //if (asset.IsDirectory == true)
            //{
            //    ViewBag.FileList = GetDirectoryDetails(asset.FilePath);
            //}

            //ViewBag.FilePath = WebConfigurationManager.AppSettings["domain"].ToString() + WebConfigurationManager.AppSettings["domain_subfolder"].ToString();
            return View(asset);
        }

        public async Task<List<AssetNameModel>> GetAssetNames(int assetID)
        {
            DB_Entities db = new DB_Entities();
            List<AssetNameModel> listNames = new List<AssetNameModel>();
            var names = await db.AssetNames.Where(x => x.AssetId == assetID && x.IsDeleted == false).OrderBy(x => x.Id).ToListAsync();
            var languages = (from l in db.CsNewLanguage
                             select new DataList { Text = l.NewName, Value = l.Id }).ToList();
            listNames = names.Select(name => new AssetNameModel()
            {
                ID = name.Id,
                Name = name.Name,
                LanguageId = name.LanguageId,
                Language = languages.Where(x => x.Value == name.LanguageId).Select(x => x.Text).FirstOrDefault(),
                IsDeleted = name.IsDeleted
            }).ToList();

            return listNames;
        }

        public async Task<List<AssetDetailModel>> GetAssetDetails(int assetID)
        {
            DB_Entities db = new DB_Entities();
            List<AssetDetailModel> listDetails = new List<AssetDetailModel>();
            var details = await db.AssetDetails.Where(x => x.AssetId == assetID && x.IsDeleted == false).OrderBy(x => x.Id).ToListAsync();

            listDetails = details.Select(detail => new AssetDetailModel()
            {
                ID = detail.Id,
                PackageId = detail.PackageId,
                LanguageId = detail.LanguageId,
                GradeId = detail.GradeId,
                SyllabusId = detail.SyllabusId,
                StreamId = detail.StreamId,
                SubjectId = detail.SubjectId,
                TopicId = detail.TopicId,
                SubtopicId = detail.SubtopicId,
                Package = db.CsNewPackage.Where(x => x.NewPackageid == detail.PackageId).Select(x => x.NewName).FirstOrDefault(),
                Grade = db.CsNewGrade.Where(x => x.NewGradeid == detail.GradeId).Select(x => x.NewName).FirstOrDefault(),
                Language = db.CsNewLanguage.Where(x => x.NewLanguageid == detail.LanguageId).Select(x => x.NewName).FirstOrDefault(),
                Syllabus = db.CsNewSyllabus.Where(x => x.NewSyllabusid == detail.SyllabusId).Select(x => x.NewName).FirstOrDefault(),
                Stream = db.CsNewStream.Where(x => x.NewStreamid == detail.StreamId).Select(x => x.NewName).FirstOrDefault(),
                Subject = db.CsNewSubject.Where(x => x.NewSubjectid == detail.SubjectId).Select(x => x.NewName).FirstOrDefault(),
                Topic = db.CsNewTopic.Where(x => x.NewTopicid == detail.TopicId).Select(x => x.NewName).FirstOrDefault(),
                Subtopic = db.CsNewSubtopic.Where(x => x.NewSubtopicid == detail.SubtopicId).Select(x => x.NewName).FirstOrDefault(),
                IsDeleted = detail.IsDeleted
            }).ToList();

            return listDetails;
        }

        public async Task GetDataListFromSessions()
        {
            using (DB_Entities db = new DB_Entities())
            {
                ViewBag.Package = await db.CsNewPackage.Select(t => new SelectListItem { Text = t.NewName, Value = t.NewPackageid.ToString() }).OrderBy(x => x.Text).ToListAsync();
                ViewBag.AssetType = await db.MstFileType.Where(x => x.IsDeleted == false && x.IsActive == true).Select(x => x.TypeName).OrderBy(x => x).ToListAsync();
                ViewBag.Status = await db.MstStatus.Where(x => x.IsDeleted == false && x.IsActive == true).Select(x => x.StatusName).OrderBy(x => x).ToListAsync();
                ViewBag.Tags = await db.MstTag.Where(x => x.IsDeleted == false && x.IsActive == true).Select(x => x.TagName).OrderBy(x => x).ToListAsync();
                ViewBag.Properties = await db.MstProperty.Where(x => x.IsDeleted == false && x.IsActive == true).Select(x => x.PropertyName).OrderBy(x => x).ToListAsync();
                ViewBag.Language = await db.CsNewLanguage.Select(x => new SelectListItem { Text = x.NewName, Value = x.NewLanguageid.ToString() }).OrderBy(x => x.Text).ToListAsync();

                //var values = from Platform e in Enum.GetValues(typeof(Platform))
                //             select new { Id = e, Name = e.ToString() };
                //ViewBag.Platform = new SelectList(values, "Id", "Name");
                ViewBag.Platform = await db.CsNewPlatform.Select(x => new SelectListItem { Text = x.NewName, Value = x.NewPlatformid.ToString() }).OrderBy(x => x.Text).ToListAsync();
                ViewBag.Activities = await db.MstActivity.Where(x => x.IsDeleted == false && x.IsActive == true).ToListAsync();
            }
        }

        public JsonResult GetLanguagesAndGrades(string Id)
        {
            LanguageAndGradeList lst = new LanguageAndGradeList();
            using (DB_Entities db = new DB_Entities())
            {
                lst.Languages = (from pl in db.CsNewPackageLanguage
                                 join l in db.CsNewLanguage on pl.NewLanguageid equals l.NewLanguageid
                                 where pl.NewPackageid == Guid.Parse(Id)
                                 select new DataList { Text = l.NewName, Value = l.NewLanguageid }).Distinct().OrderBy(x => x.Text).ToList();

                lst.Grades = (from pg in db.CsNewPackageGrade
                              join g in db.CsNewGrade on pg.NewGradeid equals g.NewGradeid
                              where pg.NewPackageid == Guid.Parse(Id)
                              select new DataList { Text = g.NewName, Value = g.NewGradeid }).Distinct().OrderBy(x => x.Text).ToList();
            }

            return Json(lst, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetSyllabus(Guid Id)
        {
            SyllabusList lst = new SyllabusList();
            using (DB_Entities db = new DB_Entities())
            {
                lst.Syllabus = (from gs in db.CsNewGradeSyllabus
                                join cs in db.CsNewSyllabus on gs.NewSyllabusid equals cs.NewSyllabusid
                                where gs.NewGradeid == Id
                                select new DataList { Text = cs.NewName, Value = cs.NewSyllabusid }).Distinct().OrderBy(x => x.Text).ToList();

                lst.Streams = (from gs in db.CsNewGradeStream
                               join cs in db.CsNewStream on gs.NewStreamid equals cs.NewStreamid
                               where gs.NewGradeid == Id
                               select new DataList { Text = cs.NewName, Value = cs.NewStreamid }).Distinct().OrderBy(x => x.Text).ToList();

                lst.Subjects = (from gs in db.CsNewGradeSubject
                                join cs in db.CsNewSubject on gs.NewSubjectid equals cs.NewSubjectid
                                where gs.NewGradeid == Id
                                select new DataList { Text = cs.NewName, Value = cs.NewSubjectid }).Distinct().OrderBy(x => x.Text).ToList();
            }

            return Json(lst, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetTopics(Guid Id)
        {
            TopicList lst = new TopicList();
            using (DB_Entities db = new DB_Entities())
            {
                lst.Topics = (from st in db.CsNewSubjectTopic
                              join t in db.CsNewTopic on st.NewTopicid equals t.NewTopicid
                              where st.NewSubjectid == Id
                              select new DataList { Text = t.NewName, Value = t.NewTopicid }).Distinct().OrderBy(x => x.Text).ToList();

            }

            return Json(lst, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetSubtopics(Guid Id)
        {
            SubtopicList lst = new SubtopicList();
            using (DB_Entities db = new DB_Entities())
            {
                lst.Subtopics = (from ts in db.CsNewTopicSubtopic
                                 join st in db.CsNewSubtopic on ts.NewSubtopicid equals st.NewSubtopicid
                                 where ts.NewTopicid == Id
                                 select new DataList { Text = st.NewName, Value = st.NewSubtopicid }).Distinct().OrderBy(x => x.Text).ToList();

            }

            return Json(lst, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetDescription(int Id)
        {
            DB_Entities db = new DB_Entities();
            List<ContentDescriptionModel> desc = new List<ContentDescriptionModel>();
            var details = db.AssetContentDetail.Where(x => x.AssetContentId == Id && x.IsDeleted == false).OrderBy(x => x.Id).ToList();

            if (details.Count != 0)
            {
                desc = details.Select(detail => new ContentDescriptionModel()
                {
                    ID = detail.Id,
                    AssetContentID = detail.AssetContentId ?? 0,
                    Description = detail.Description,
                    WhatIsNew = detail.WhatIsNew,
                    LanguageId = detail.LanguageId,
                    Language = db.CsNewLanguage.Where(x => x.NewLanguageid == detail.LanguageId).Select(x => x.NewName).FirstOrDefault(),
                    IsDeleted = detail.IsDeleted ?? false
                }).ToList();
            }
            else
            {
                ContentDescriptionModel detail = new ContentDescriptionModel();
                detail.AssetContentID = Id;
                desc.Add(detail);
            }

            return PartialView("_Description", desc);
        }

        public ActionResult GetWhatsNew(int Id)
        {
            DB_Entities db = new DB_Entities();
            List<ContentDescriptionModel> desc = new List<ContentDescriptionModel>();
            var details = db.AssetContentDetail.Where(x => x.AssetContentId == Id && x.IsEnabled == false).OrderBy(x => x.Id).ToList();

            if (details.Count != 0)
            {
                desc = details.Select(detail => new ContentDescriptionModel()
                {
                    ID = detail.Id,
                    AssetContentID = detail.AssetContentId ?? 0,
                    Description = detail.Description,
                    WhatIsNew = detail.WhatIsNew,
                    LanguageId = detail.LanguageId,
                    Language = db.CsNewLanguage.Where(x => x.NewLanguageid == detail.LanguageId).Select(x => x.NewName).FirstOrDefault(),
                    IsDeleted = detail.IsEnabled ?? false,
                }).ToList();
            }
            else
            {
                ContentDescriptionModel detail = new ContentDescriptionModel();
                detail.AssetContentID = Id;
                desc.Add(detail);
            }
            return PartialView("_WhatsNew", desc);
        }

        [HttpPost]
        public string SaveDescription(List<ContentDescriptionModel> details)
        {
            string message = string.Empty;
            int num = details.Where(d => d.IsDeleted == false && (d.LanguageId == Guid.Empty || string.IsNullOrWhiteSpace(d.Description))).ToList().Count();
            if (num > 0)
            {
                message = "Error";
            }
            else
            {
                DB_Entities db = new DB_Entities();
                int contentId = details[0].AssetContentID;
                db.AssetContentDetail.Where(w => w.AssetContentId == contentId && w.IsDeleted == false).ToList().ForEach(i => i.IsDeleted = true);
                db.SaveChanges();

                foreach (ContentDescriptionModel detail in details)
                {
                    var descObj = db.AssetContentDetail.Where(x => x.AssetContentId == contentId && x.LanguageId == detail.LanguageId).FirstOrDefault();

                    if (descObj == null && detail.ID == 0)
                    {
                        if (detail.LanguageId != Guid.Empty && !string.IsNullOrWhiteSpace(detail.Description))
                        {
                            AssetContentDetail newDesc = new AssetContentDetail()
                            {
                                AssetContentId = contentId,
                                Description = detail.Description,
                                LanguageId = detail.LanguageId,
                                LanguageShortName = db.CsNewLanguage.Where(x => x.NewLanguageid == detail.LanguageId).Select(x => x.NewLanguageshortname).FirstOrDefault(),
                                IsDeleted = detail.IsDeleted,
                                IsEnabled = true,
                                CreatedBy = new Guid(Session["UserID"].ToString()),
                                CreatedOn = DateTime.Now
                            };
                            db.AssetContentDetail.Add(newDesc);
                            db.SaveChanges();

                            detail.ID = newDesc.Id;
                        }
                    }
                    else
                    {
                        descObj.Description = detail.Description;
                        descObj.LanguageId = detail.LanguageId;
                        descObj.LanguageShortName = db.CsNewLanguage.Where(x => x.NewLanguageid == detail.LanguageId).Select(x => x.NewLanguageshortname).FirstOrDefault();
                        descObj.IsDeleted = detail.IsDeleted;
                        descObj.ModifiedBy = new Guid(Session["UserID"].ToString());
                        descObj.ModifiedOn = DateTime.Now;
                        db.SaveChanges();
                    }
                }

                message = "Description saved successfully";
            }
            return message;
        }

        [HttpPost]
        public string SaveWhatIsNew(List<ContentDescriptionModel> details)
        {
            string message = string.Empty;
            int num = details.Where(d => d.IsDeleted == false && (d.LanguageId == Guid.Empty || string.IsNullOrWhiteSpace(d.WhatIsNew))).ToList().Count();
            if (num > 0)
            {
                message = "Error";
            }
            else
            {
                DB_Entities db = new DB_Entities();
                int contentId = details[0].AssetContentID;
                db.AssetContentDetail.Where(w => w.AssetContentId == contentId && w.IsEnabled == false).ToList().ForEach(i => i.IsEnabled = true);
                db.SaveChanges();

                foreach (ContentDescriptionModel detail in details)
                {
                    var descObj = db.AssetContentDetail.Where(x => x.AssetContentId == contentId && x.LanguageId == detail.LanguageId).FirstOrDefault();

                    if (descObj == null && detail.ID == 0)
                    {
                        if (detail.LanguageId != Guid.Empty && !string.IsNullOrWhiteSpace(detail.WhatIsNew))
                        {
                            AssetContentDetail newDesc = new AssetContentDetail()
                            {
                                AssetContentId = contentId,
                                WhatIsNew = detail.WhatIsNew,
                                LanguageId = detail.LanguageId,
                                //LanguageShortName = db.CsNewLanguage.Where(x => x.NewLanguageid == detail.LanguageId).Select(x => x.NewLanguageshortname).FirstOrDefault(),
                                IsDeleted = true,
                                IsEnabled = detail.IsDeleted,
                                CreatedBy = new Guid(Session["UserID"].ToString()),
                                CreatedOn = DateTime.Now
                            };
                            db.AssetContentDetail.Add(newDesc);
                            db.SaveChanges();

                            detail.ID = newDesc.Id;
                        }
                    }
                    else
                    {
                        descObj.WhatIsNew = detail.WhatIsNew;
                        descObj.LanguageId = detail.LanguageId;
                        //descObj.LanguageShortName = db.CsNewLanguage.Where(x => x.NewLanguageid == detail.LanguageId).Select(x => x.NewLanguageshortname).FirstOrDefault();
                        descObj.IsEnabled = detail.IsDeleted;
                        descObj.ModifiedBy = new Guid(Session["UserID"].ToString());
                        descObj.ModifiedOn = DateTime.Now;
                        db.SaveChanges();
                    }
                }

                message = "Comments saved successfully";
            }
            return message;
        }

        public async Task<AssetViewModel> ValidateMasterData(AssetViewModel asset)
        {
            using (DB_Entities db = new DB_Entities())
            {
                if (asset.AssetID == 0)
                {
                    //Check Language value if exists in master
                    asset.Language = await CommonMethods.CheckLanguage(asset.Language);

                    //Check Syllabus value if exists in master
                    asset.Syllabus = await CommonMethods.CheckSyllabus(asset.Syllabus);

                    //Check level value if exists in master
                    asset.Level = await CommonMethods.CheckLevel(asset.Level);

                    //Check subject value if exists in master
                    asset.Subject = await CommonMethods.CheckSubject(asset.Subject);

                    //Check topic value if exists in master
                    asset.Topic = await Task.Run(() => CommonMethods.CheckTopic(asset.Topic));

                    //Check subtopic value if exists in master
                    asset.Subtopic = await Task.Run(() => CommonMethods.CheckSubtopic(asset.Subtopic));
                    asset.SubtopicDesc = await CommonMethods.GetSubtopicDetails(asset.Subtopic);

                    //Check asset type value if exists in master
                    asset.AssetType = await CommonMethods.CheckAssetType(asset.AssetType);

                    //Check tag value if exists in master
                    //asset.Tags = await CommonMethods.CheckTag(asset.Tags);

                    //Check tag value if exists in master
                    //asset.Properties = await CommonMethods.CheckProperty(asset.Properties);

                    //Check stream value if exists in master
                    asset.Stream = await CommonMethods.CheckStream(asset.Stream);

                    //Check package if exists in DB
                    asset.ContentPackageID = await CommonMethods.GetPackageID(asset.Package);
                }
                else
                {
                    bool IsChanged = false;
                    string strActivity = string.Empty;

                    var assetObj = db.Asset.Where(x => x.AssetId == asset.AssetID && x.IsDeleted == false).FirstOrDefault();
                    if (assetObj != null)
                    {
                        if (assetObj.AssetName != asset.AssetName)
                        {
                            IsChanged = true;
                            strActivity += string.Format("Asset name changed from '{0}' to '{1}'; ", assetObj.AssetName, asset.AssetName);
                        }

                        if (assetObj.Type != asset.AssetType)
                        {
                            IsChanged = true;
                            asset.AssetType = await CommonMethods.CheckAssetType(asset.AssetType);

                            strActivity += string.Format("Type changed from '{0}' to '{1}'; ", assetObj.Type, asset.AssetType);
                        }

                        if (assetObj.Status != asset.Status)
                        {
                            IsChanged = true;
                            strActivity += string.Format("Status changed from '{0}' to '{1}'; ", assetObj.Status, asset.Status);
                        }

                        if (assetObj.Language != asset.Language)
                        {
                            IsChanged = true;
                            asset.Language = await CommonMethods.CheckLanguage(asset.Language);
                            await CommonMethods.AddUpdateAssetLanguages(asset.AssetID, asset.Language);
                            strActivity += string.Format("Language changed from '{0}' to '{1}'; ", assetObj.Language, asset.Language);
                        }

                        if (assetObj.Syllabus != asset.Syllabus)
                        {
                            IsChanged = true;
                            if (!string.IsNullOrEmpty(asset.Syllabus))
                            {
                                asset.Syllabus = await CommonMethods.CheckSyllabus(asset.Syllabus);
                                await CommonMethods.AddUpdateAssetSyllabus(asset.AssetID, asset.Syllabus);
                            }
                            strActivity += string.Format("Syllabus changed from '{0}' to '{1}'; ", assetObj.Syllabus, asset.Syllabus);
                        }

                        if (assetObj.Level != asset.Level)
                        {
                            IsChanged = true;
                            asset.Level = await CommonMethods.CheckLevel(asset.Level);
                            await CommonMethods.AddUpdateAssetGrades(asset.AssetID, asset.Level);
                            strActivity += string.Format("Level changed from '{0}' to '{1}'; ", assetObj.Level, asset.Level);
                        }

                        if (assetObj.Subject != asset.Subject)
                        {
                            IsChanged = true;
                            asset.Subject = await CommonMethods.CheckSubject(asset.Subject);
                            await CommonMethods.AddUpdateAssetSubjects(asset.AssetID, asset.Subject);
                            strActivity += string.Format("Subject changed from '{0}' to '{1}'; ", assetObj.Subject, asset.Subject);
                        }

                        if (assetObj.Topic != asset.Topic)
                        {
                            IsChanged = true;
                            asset.Topic = await CommonMethods.CheckTopic(asset.Topic);
                            await CommonMethods.AddUpdateAssetTopics(asset.AssetID, asset.Topic);
                            strActivity += string.Format("Topic changed from '{0}' to '{1}'; ", assetObj.Topic, asset.Topic);
                        }

                        if (assetObj.Subtopic != asset.Subtopic)
                        {
                            IsChanged = true;
                            if (!string.IsNullOrEmpty(asset.Subtopic))
                            {
                                asset.Subtopic = await Task.Run(() => CommonMethods.CheckSubtopic(asset.Subtopic));
                                await CommonMethods.AddUpdateAssetSubtopics(asset.AssetID, asset.Subtopic);
                            }

                            strActivity += string.Format("Subtopic changed from '{0}' to '{1}'; ", assetObj.Subtopic, asset.Subtopic);

                            asset.SubtopicDesc = await CommonMethods.GetSubtopicDetails(asset.Subtopic);
                        }
                        else if (assetObj.Subtopic != null && assetObj.SubtopicDesc == null)
                        {
                            IsChanged = true;
                            asset.SubtopicDesc = await CommonMethods.GetSubtopicDetails(asset.Subtopic);
                        }

                        //if (assetObj.Tags != asset.Tags)
                        //{
                        //    IsChanged = true;
                        //    asset.Tags = await CommonMethods.CheckTag(asset.Tags);

                        //    strActivity += string.Format("Tags changed from '{0}' to '{1}'; ", assetObj.Tags, asset.Tags);
                        //}

                        //if (assetObj.Properties != asset.Properties)
                        //{
                        //    IsChanged = true;
                        //    asset.Properties = await CommonMethods.CheckProperty(asset.Properties);

                        //    strActivity += string.Format("Properties changed from '{0}' to '{1}'; ", assetObj.Properties, asset.Properties);
                        //}

                        if (assetObj.Stream != asset.Stream)
                        {
                            IsChanged = true;
                            if (!string.IsNullOrEmpty(asset.Stream))
                            {
                                asset.Stream = await CommonMethods.CheckStream(asset.Stream);
                                await CommonMethods.AddUpdateAssetStreams(asset.AssetID, asset.Stream);
                            }

                            strActivity += string.Format("Stream changed from '{0}' to '{1}'; ", assetObj.Stream, asset.Stream);
                        }

                        //Check package if exists in DB
                        asset.ContentPackageID = await CommonMethods.GetPackageID(asset.Package);
                        if (assetObj.PackageId != asset.ContentPackageID)
                        {
                            IsChanged = true;
                            //if (assetObj.PackageID != null && assetObj.PackageID != Guid.Empty)
                            //{
                            //    strActivity += string.Format("Package changed from '{0}' to '{1}'; ", assetObj.ContentPackage.Name, asset.Package);
                            //}
                        }

                        if (assetObj.IsActive != asset.IsActive)
                        {
                            IsChanged = true;
                            strActivity += string.Format("Activation status changed from '{0}' to '{1}'; ", assetObj.IsActive, asset.IsActive);
                        }

                        TempData["IsChanged"] = IsChanged;
                        TempData["strActivity"] = strActivity;
                    }
                }
            }

            return asset;
        }

        [HttpPost]
        public async Task<ActionResult> AddUpdateAsset(AssetViewModel asset, string addedType)
        {
            await GetDataListFromSessions();
            if (ModelState.IsValid)
            {
                bool IsChanged = false;
                string strAction;
                string strActivity;

                //Validate Master Data - No need
                //asset = await ValidateMasterData(asset);

                using (DB_Entities db = new DB_Entities())
                {
                    if (asset.AssetID == 0)
                    {
                        Asset newAssetObj = new Asset()
                        {
                            AssetName = asset.Names[0].Name.Trim(),
                            FileName = asset.FileName,
                            FilePath = asset.FilePath,
                            FileSize = Convert.ToString(asset.FileSize),
                            IsDirectory = asset.IsDirectory,
                            Type = asset.AssetType,
                            Status = asset.Status,
                            Language = asset.Details[0].Language,
                            Syllabus = asset.Details[0].Syllabus,
                            Subject = asset.Details[0].Subject,
                            Level = asset.Details[0].Grade,
                            Topic = asset.Details[0].Topic,
                            Subtopic = asset.Details[0].Subtopic,
                            SubtopicDesc = asset.SubtopicDesc,
                            //Tags = asset.Tags,
                            VimeoUrl = asset.VimeoUrl,
                            Stream = asset.Details[0].Stream,
                            PackageId = asset.Details[0].PackageId,
                            IsQuizEnabled = asset.IsQuizEnabled,
                            PercentageOfLabels = asset.PercentageOfLabels,
                            NumberOfAttempts = asset.NumberOfAttempts,
                            IsActive = asset.IsActive == null ? true : asset.IsActive,
                            CreatedBy = new Guid(Session["UserID"].ToString()),
                            CreatedOn = DateTime.Now,
                            ModifiedByName = Session["Name"].ToString(),
                            ModifiedOn = DateTime.Now,
                            IsDeleted = false
                        };
                        db.Asset.Add(newAssetObj);
                        db.SaveChanges();

                        if (asset.NewTags != null && asset.NewTags.Length > 0)
                        {
                            CommonMethods.SaveAssetTags(newAssetObj.AssetId, asset.NewTags);
                        }

                        if (asset.NewProperties != null && asset.NewProperties.Length > 0)
                        {
                            CommonMethods.SaveAssetProperties(newAssetObj.AssetId, asset.NewProperties);
                        }

                        asset.AssetID = newAssetObj.AssetId;
                        asset.AssetName = asset.Names[0].Name.Trim();
                        strAction = "Asset added";
                        strActivity = string.Format("Asset titled '{0}' added.", asset.AssetName);

                        //await CommonMethods.AddUpdateAssetMasterData(asset.AssetID);
                        asset.Names = await SaveAssetNames(asset.AssetID, asset.Names);
                        asset.Details = await SaveAssetDetails(asset.AssetID, asset.Details);
                        asset.Contents = SaveAssetContents(asset.AssetID, asset.Contents);
                        await ActivityLogger.AddActivityLog(asset.AssetID, strAction, strActivity);
                    }
                    else
                    {
                        strAction = "Asset updated";
                        strActivity = string.Format("Asset titled '{0}' updated. ", asset.AssetName);

                        var assetObj = db.Asset.Where(x => x.AssetId == asset.AssetID && x.IsDeleted == false).FirstOrDefault();
                        if (assetObj != null)
                        {
                            strActivity += Convert.ToString(TempData["strActivity"]);
                            IsChanged = Convert.ToBoolean(TempData["IsChanged"]);

                            if (asset.IsDeleted == true)
                            {
                                IsChanged = true;
                                strAction = "Asset deleted";
                                strActivity = string.Format("Asset titled '{0}' deleted.", asset.AssetName);
                            }

                            //await CommonMethods.AddUpdateAssetMasterData(asset.AssetID);
                            asset.Names = await SaveAssetNames(asset.AssetID, asset.Names);
                            asset.Details = await SaveAssetDetails(asset.AssetID, asset.Details);
                            asset.Contents = SaveAssetContents(asset.AssetID, asset.Contents);

                            asset.AssetName = asset.Names[0].Name.Trim();
                            assetObj.AssetName = asset.Names[0].Name.Trim();
                            assetObj.Type = asset.AssetType;
                            assetObj.Status = asset.Status;
                            assetObj.Language = asset.Details[0].Language;
                            assetObj.Syllabus = asset.Details[0].Syllabus;
                            assetObj.Subject = asset.Details[0].Subject;
                            assetObj.Level = asset.Details[0].Grade;
                            assetObj.Topic = asset.Details[0].Topic;
                            assetObj.Subtopic = asset.Details[0].Subtopic;
                            assetObj.SubtopicDesc = asset.SubtopicDesc;
                            //assetObj.Tags = asset.Tags;
                            assetObj.VimeoUrl = asset.VimeoUrl;
                            assetObj.Stream = asset.Details[0].Stream;
                            assetObj.PackageId = asset.Details[0].PackageId;
                            assetObj.IsQuizEnabled = asset.IsQuizEnabled;
                            assetObj.PercentageOfLabels = asset.PercentageOfLabels;
                            assetObj.NumberOfAttempts = asset.NumberOfAttempts;
                            assetObj.IsActive = asset.IsActive;
                            assetObj.ModifiedBy = new Guid(Session["UserID"].ToString());
                            assetObj.ModifiedByName = Session["Name"].ToString();
                            assetObj.ModifiedOn = DateTime.Now;
                            assetObj.IsDeleted = asset.IsDeleted;
                            db.Entry(assetObj).State = EntityState.Modified;
                            db.SaveChanges();

                            List<AssetTags> assetTags = db.AssetTags.Where(a => a.AssetId == asset.AssetID).ToList();
                            assetTags.ForEach((a) =>
                            {
                                db.AssetTags.Remove(a);
                            });
                            db.SaveChanges();

                            if (asset.NewTags != null && asset.NewTags.Length > 0)
                            {
                                CommonMethods.SaveAssetTags(assetObj.AssetId, asset.NewTags);
                            }

                            List<AssetProperties> assetProperties = db.AssetProperties.Where(a => a.AssetId == asset.AssetID).ToList();
                            assetProperties.ForEach((a) =>
                            {
                                db.AssetProperties.Remove(a);
                            });
                            db.SaveChanges();

                            if (asset.NewProperties != null && asset.NewProperties.Length > 0)
                            {
                                CommonMethods.SaveAssetProperties(assetObj.AssetId, asset.NewProperties);
                            }

                            await ActivityLogger.AddActivityLog(asset.AssetID, strAction, strActivity);

                        }
                    }

                    ViewBag.TagsSelectList = new SelectList(CommonMethods.GetAssetTags(asset.AssetID), "Value", "Text");
                    ViewBag.PropertiesSelectList = new SelectList(CommonMethods.GetAssetProperties(asset.AssetID), "Value", "Text");
                }
            }
            else
            {
                //if (string.IsNullOrEmpty(asset.Contents[0].Description))
                //{
                //    ModelState.AddModelError("", "Description is required.");
                //}

                ViewBag.AddedType = addedType;
                //await GetDataListFromSessions();
                return RedirectToAction("AddUpdateAsset", new { @assetID = asset.AssetID });
            }

            if (addedType == "Edit")
            {
                TempData["Message"] = "Asset '" + asset.AssetName + "' updated successfully.";
                //return RedirectToAction("Assets");
            }
            else
            {
                TempData["Search"] = "Added";
                TempData["Message"] = "Asset '" + asset.AssetName + "' added successfully.";
                //return RedirectToAction("Index");
            }
            TempData.Keep("Path");
            ViewBag.AddedType = addedType;

            return RedirectToAction("AddUpdateAsset", new { @assetID = asset.AssetID });
        }

        public async Task<List<AssetNameModel>> SaveAssetNames(int assetID, List<AssetNameModel> names)
        {
            DB_Entities db = new DB_Entities();

            db.AssetNames.Where(w => w.AssetId == assetID && w.IsDeleted == false).ToList().ForEach(i => i.IsDeleted = true);
            db.SaveChanges();

            foreach (AssetNameModel name in names)
            {
                if (name.ID == 0 && name.IsDeleted == false)
                {
                    AssetNames newName = new AssetNames()
                    {
                        AssetId = assetID,
                        Name = name.Name,
                        LanguageId = name.LanguageId,
                        IsDeleted = name.IsDeleted
                    };
                    db.AssetNames.Add(newName);
                    db.SaveChanges();

                    name.ID = newName.Id;
                }
                else
                {
                    var nameObj = await db.AssetNames.Where(x => x.Id == name.ID).FirstOrDefaultAsync();
                    if (nameObj != null)
                    {
                        nameObj.Name = name.Name;
                        nameObj.LanguageId = name.LanguageId;
                        nameObj.IsDeleted = name.IsDeleted;
                        db.SaveChanges();
                    }
                }
            }

            List<AssetNameModel> newNames = names.Where(d => d.IsDeleted == false).ToList();
            return newNames;
        }

        public async Task<List<AssetDetailModel>> SaveAssetDetails(int assetID, List<AssetDetailModel> details)
        {
            DB_Entities db = new DB_Entities();

            db.AssetDetails.Where(w => w.AssetId == assetID && w.IsDeleted == false).ToList().ForEach(i => i.IsDeleted = true);
            db.SaveChanges();

            foreach (AssetDetailModel detail in details)
            {
                if (detail.ID == 0 && detail.IsDeleted == false)
                {
                    AssetDetails newDetail = new AssetDetails()
                    {
                        AssetId = assetID,
                        PackageId = detail.PackageId,
                        LanguageId = detail.LanguageId,
                        GradeId = detail.GradeId,
                        SyllabusId = detail.SyllabusId,
                        StreamId = detail.StreamId,
                        SubjectId = detail.SubjectId,
                        TopicId = detail.TopicId,
                        SubtopicId = detail.SubtopicId,
                        IsDeleted = detail.IsDeleted
                    };
                    db.AssetDetails.Add(newDetail);
                    db.SaveChanges();

                    detail.ID = newDetail.Id;
                }
                else
                {
                    var detailObj = await db.AssetDetails.Where(x => x.Id == detail.ID).FirstOrDefaultAsync();
                    if (detailObj != null)
                    {
                        detailObj.PackageId = detail.PackageId;
                        detailObj.LanguageId = detail.LanguageId;
                        detailObj.GradeId = detail.GradeId;
                        detailObj.SyllabusId = detail.SyllabusId;
                        detailObj.StreamId = detail.StreamId;
                        detailObj.SubjectId = detail.SubjectId;
                        detailObj.TopicId = detail.TopicId;
                        detailObj.SubtopicId = detail.SubtopicId;
                        detailObj.IsDeleted = detail.IsDeleted;
                        db.SaveChanges();
                    }
                }
            }

            //List<AssetDetailModel> newDetails = details.Where(d => d.IsDeleted == false).ToList();
            return details;
        }

        #region Publish Assets
        public async Task<bool> PublishAssets(int Id)
        {
            bool status = false;
            using (DB_Entities db = new DB_Entities())
            {
                var assetObj = db.AssetContent.Where(x => x.Id == Id && x.IsDeleted == false).Include(x => x.Asset).FirstOrDefault();
                if (assetObj != null)
                {
                    string sourcePath = WebConfigurationManager.AppSettings["SourcePath"].ToString() + assetObj.FilePath;
                    //string targetPath = WebConfigurationManager.AppSettings["PublishPath"].ToString() + assetObj.Asset.PackageId;
                    string targetPath = WebConfigurationManager.AppSettings["PublishPath"].ToString();

                    //Check for the target path if it is exist
                    if (!Directory.Exists(targetPath))
                    {
                        Directory.CreateDirectory(targetPath);
                    }

                    if (assetObj.IsDirectory == true)
                    {
                        targetPath = Path.Combine(targetPath, assetObj.FileName);
                        if (!Directory.Exists(targetPath))
                        {
                            Directory.CreateDirectory(targetPath);
                        }

                        if (Directory.Exists(sourcePath))
                        {
                            //Now Create all of the directories
                            //Copy all the files & Replaces any files with the same name
                            foreach (string dirPath in Directory.GetDirectories(sourcePath, "*", SearchOption.AllDirectories))
                            {
                                Directory.CreateDirectory(dirPath.Replace(sourcePath, targetPath));
                                foreach (string filename in Directory.EnumerateFiles(dirPath))
                                {
                                    using (FileStream SourceStream = System.IO.File.Open(filename, FileMode.Open, FileAccess.Read))
                                    {
                                        using (FileStream DestinationStream = System.IO.File.Open(filename.Replace(sourcePath, targetPath), FileMode.OpenOrCreate, FileAccess.ReadWrite))
                                        {
                                            await SourceStream.CopyToAsync(DestinationStream);
                                        }
                                    }
                                }
                            }

                            foreach (string filename in Directory.EnumerateFiles(sourcePath))
                            {
                                using (FileStream SourceStream = System.IO.File.Open(filename, FileMode.Open, FileAccess.Read))
                                {
                                    using (FileStream DestinationStream = System.IO.File.Open(targetPath + filename.Substring(filename.LastIndexOf('\\')), FileMode.OpenOrCreate, FileAccess.ReadWrite))
                                    {
                                        await SourceStream.CopyToAsync(DestinationStream);
                                    }
                                }
                            }
                        }
                        else
                        {
                            Console.WriteLine("Source path does not exist!");
                        }
                    }
                    else
                    {
                        sourcePath = sourcePath.Substring(0, sourcePath.Length - assetObj.FileName.Length);

                        if (Directory.Exists(sourcePath))
                        {
                            //Get the files having same name
                            string[] files = Directory.GetFiles(sourcePath, assetObj.FileName);

                            foreach (string filename in files)
                            {
                                string file = Path.Combine(targetPath, assetObj.FileName);
                                using (FileStream SourceStream = System.IO.File.Open(filename, FileMode.Open, FileAccess.Read))
                                {
                                    using (FileStream DestinationStream = System.IO.File.Open(file, FileMode.OpenOrCreate, FileAccess.ReadWrite))
                                    {
                                        await SourceStream.CopyToAsync(DestinationStream);
                                    }
                                }
                            }
                            //var hashResult = File.CopyFileAndRename(sourcePath, targetPath).Result;
                        }
                        else
                        {
                            Console.WriteLine("Source path does not exist!");
                        }
                    }

                    //assetObj.Asset.ContentPackage.IsEnabled = true;
                    assetObj.IsEnabled = true;
                    assetObj.ModifiedOn = DateTime.Now;
                    assetObj.ModifiedBy = new Guid(Session["UserID"].ToString());
                    db.Entry(assetObj).State = EntityState.Modified;
                    await db.SaveChangesAsync();


                    string strAction = "Content published";
                    string strActivity = string.Format("Content titled '{0}' has been published for Asset titled '{0}'.", assetObj.FileName, assetObj.Asset.AssetName);
                    await ActivityLogger.AddActivityLog(Id, strAction, strActivity);

                    status = true;
                }
            }


            return status;
        }
        #endregion
        //public bool PublishAssets(int Id)
        //{
        //    var host = WebConfigurationManager.AppSettings["HostApi"].ToString();
        //    var pass = WebConfigurationManager.AppSettings["ContentPublishingPassword"].ToString();
        //    var user = new Guid(Session["UserID"].ToString());
        //    var response = DoHttp<ContentPublishingPublishRequest, ContentPublishingPublishResponse>($"{host}/api/v1/ContentPublishing/Publish", HttpMethods.GET, new ContentPublishingPublishRequest() { Id = Id, Password = pass, UserId = user });

        //    return response.Success;
        //}

        #region Unpublish Assets
        public async Task<bool> UnpublishAssets(int Id)
        {
            bool status = false;
            using (DB_Entities db = new DB_Entities())
            {
                var assetObj = db.AssetContent.Where(x => x.Id == Id && x.IsDeleted == false).Include(x => x.Asset).FirstOrDefault();
                if (assetObj != null)
                {
                    //string targetPath = WebConfigurationManager.AppSettings["PublishPath"].ToString() + assetObj.Asset.PackageId;
                    string targetPath = WebConfigurationManager.AppSettings["PublishPath"].ToString();
                    string fileName = assetObj.FileName;
                    string destFile = string.Empty;

                    if (assetObj.IsDirectory == true)
                    {
                        System.IO.DirectoryInfo myDirInfo = new DirectoryInfo(Path.Combine(targetPath, fileName));

                        if (Directory.Exists(myDirInfo.FullName))
                        {
                            foreach (FileInfo file in myDirInfo.GetFiles())
                            {
                                System.IO.File.SetAttributes(file.FullName, FileAttributes.Normal);
                                file.Delete();
                            }
                            foreach (DirectoryInfo dir in myDirInfo.GetDirectories())
                            {
                                dir.Delete(true);
                            }

                            myDirInfo.Delete(true);
                        }
                    }
                    else
                    {
                        //string file = Path.Combine(targetPath, fileName);
                        System.IO.FileInfo file = new FileInfo(Path.Combine(targetPath, fileName));
                        if (System.IO.File.Exists(file.FullName))
                        {
                            System.IO.File.SetAttributes(file.FullName, FileAttributes.Normal);
                            file.Delete();
                            //System.IO.File.Delete(file);
                        }
                    }

                    //int count = db.AssetContent.Where(x => x.AssetId == assetObj.AssetId && x.IsDeleted == false && x.IsEnabled == true).ToList().Count;
                    //assetObj.Asset.ContentPackage.IsEnabled = count == 1 ? false : true;

                    assetObj.IsEnabled = false;
                    assetObj.ModifiedOn = DateTime.Now;
                    assetObj.ModifiedBy = new Guid(Session["UserID"].ToString());
                    db.Entry(assetObj).State = EntityState.Modified;
                    await db.SaveChangesAsync();


                    string strAction = "Content unpublished";
                    string strActivity = string.Format("Content titled '{0}' has been unpublished for Asset titled '{0}'.", assetObj.FileName, assetObj.Asset.AssetName);
                    await ActivityLogger.AddActivityLog(Id, strAction, strActivity);

                    status = true;
                }
            }

            return status;
        }
        #endregion

        #region Select Asset

        public ActionResult SelectAsset(string path, string file, string size, int type)
        {
            ViewBag.Type = type;
            ViewBag.Path = path;
            ViewBag.File = file;
            ViewBag.Size = size;
            return View();
        }

        #endregion

        public ActionResult GetAllAsset([DataSourceRequest] DataSourceRequest request)
        {
            List<AssetViewModel> listAssets = new List<AssetViewModel>();
            using (DB_Entities db = new DB_Entities())
            {
                var assets = db.Asset.Where(x => x.IsDeleted == false).OrderBy(x => x.AssetName).ToList();

                listAssets = assets.Select(asset => new AssetViewModel()
                {
                    AssetID = asset.AssetId,
                    AssetName = asset.AssetName,
                    FileName = asset.FileName,
                    FilePath = asset.FilePath,
                    FileSize = Convert.ToInt64(asset.FileSize),
                    FileSizeText = CommonMethods.GetBytesReadable(Convert.ToInt64(asset.FileSize)),
                    IsDirectory = asset.IsDirectory,
                    Status = asset.Status,
                    AssetType = asset.Type,
                    Language = asset.Language,
                    Syllabus = asset.Syllabus,
                    Stream = asset.Stream,
                    Subject = asset.Subject,
                    Level = asset.Level,
                    Topic = asset.Topic,
                    Subtopic = asset.Subtopic,
                    SubtopicDesc = asset.SubtopicDesc,
                    //TagsNew = asset.AssetTags == null ? "" : string.Join(",", (from a in asset.AssetTags join b in db.MstTag on a.MstTagId equals b.TagId select b.TagName).ToList()),
                    //Properties = asset.Properties,
                    ContentPackageID = asset.PackageId,
                    IsQuizEnabled = asset.IsQuizEnabled ?? false,
                    PercentageOfLabels = asset.PercentageOfLabels,
                    NumberOfAttempts = asset.NumberOfAttempts,
                    IsActive = Convert.ToBoolean(asset.IsActive ?? true),
                    IsDeleted = Convert.ToBoolean(asset.IsDeleted ?? false),
                    CreatedBy = asset.CreatedBy != null ? asset.CreatedBy.Value : new Guid(),
                    CreatedOn = asset.CreatedOn != null ? asset.CreatedOn.Value : DateTime.UtcNow,
                    ModifiedOn = asset.ModifiedOn != null ? asset.ModifiedOn.Value : DateTime.UtcNow,
                    ModifiedByName = asset.ModifiedByName
                }).ToList();

                return Json(listAssets.ToDataSourceResult(request));
            }

        }

        [HttpPost]
        public async Task<ActionResult> AddtoExistingAsset(string path, string fileName, string size, int type, int assetId)
        {
            DB_Entities db = new DB_Entities();
            AssetViewModel asset = new AssetViewModel();
            List<AssetNameModel> listNames = new List<AssetNameModel>();
            List<AssetDetailModel> listDetails = new List<AssetDetailModel>();
            List<AssetContentModel> listContents = new List<AssetContentModel>();
            await GetDataListFromSessions();
            if (assetId != 0)
            {
                Asset objAsset = db.Asset.FirstOrDefault(x => x.AssetId == assetId);
                CsNewPackage objPackage = db.CsNewPackage.FirstOrDefault(x => x.Id == objAsset.PackageId);

                listNames = await GetAssetNames(objAsset.AssetId);
                listDetails = await GetAssetDetails(objAsset.AssetId);
                listContents = await GetAssetContentList(objAsset.AssetId);

                asset.AssetID = objAsset.AssetId;
                asset.AssetName = objAsset.AssetName;
                asset.FileName = objAsset.FileName;
                asset.FilePath = objAsset.FilePath;
                asset.FileSize = Convert.ToInt64(objAsset.FileSize);
                asset.FileSizeText = CommonMethods.GetBytesReadable(Convert.ToInt64(objAsset.FileSize));
                asset.IsDirectory = objAsset.IsDirectory;
                asset.AssetType = objAsset.Type;
                asset.Language = objAsset.Language;
                asset.Syllabus = objAsset.Syllabus;
                asset.Status = objAsset.Status;
                asset.Subject = objAsset.Subject;
                asset.Stream = objAsset.Stream;
                asset.Level = objAsset.Level;
                asset.Topic = objAsset.Topic;
                asset.Subtopic = objAsset.Subtopic;
                asset.SubtopicDesc = objAsset.SubtopicDesc;
                //asset.Tags = objAsset.Tags;
                asset.VimeoUrl = objAsset.VimeoUrl;
                asset.IsQuizEnabled = objAsset.IsQuizEnabled ?? false;
                asset.PercentageOfLabels = objAsset.PercentageOfLabels;
                asset.NumberOfAttempts = objAsset.NumberOfAttempts;
                asset.ContentPackageID = objAsset.PackageId;
                asset.Package = objPackage?.NewName;
                asset.IsPublished = db.AssetContent.Where(x => x.AssetId == assetId && x.IsEnabled == true).ToList().Count > 0 ? true : false;
                asset.IsActive = objAsset.IsActive;
                asset.IsDeleted = objAsset.IsDeleted;
                asset.CreatedBy = objAsset.CreatedBy;
                asset.CreatedOn = objAsset.CreatedOn;

                ViewBag.TagsSelectList = new SelectList(CommonMethods.GetAssetTags(objAsset.AssetId), "Value", "Text");
                ViewBag.PropertiesSelectList = new SelectList(CommonMethods.GetAssetProperties(objAsset.AssetId), "Value", "Text");

                ViewBag.AddedType = "Edit";
            }
            else
            {
                AssetNameModel name = new AssetNameModel();
                listNames.Add(name);

                AssetDetailModel detail = new AssetDetailModel();
                listDetails.Add(detail);
            }

            AssetContentModel content = new AssetContentModel();
            content.ID = 0;
            content.FileName = fileName;
            content.FilePath = path + fileName;
            content.FileSize = size;
            content.FileSizeText = CommonMethods.GetBytesReadable(Convert.ToInt64(content.FileSize));
            content.IsDirectory = Convert.ToBoolean(type);
            content.VersionNumber = db.AssetContent.Where(x => x.AssetId == asset.AssetID).ToList().Count + 1;
            content.PlatformId = db.CsNewPlatform.Where(x => x.NewName.Equals("All")).Select(x => x.NewPlatformid.ToString()).FirstOrDefault();
            listContents.Add(content);

            asset.Names = listNames;
            asset.Details = listDetails;
            asset.Contents = listContents;

            if (asset.IsDirectory == true)
            {
                ViewBag.FileList = GetDirectoryDetails(asset.FilePath);
            }
            ViewBag.TagsSelectList = new SelectList(CommonMethods.GetAssetTags(asset.AssetID), "Value", "Text");
            ViewBag.PropertiesSelectList = new SelectList(CommonMethods.GetAssetProperties(asset.AssetID), "Value", "Text");

            TempData["Path"] = path;
            ViewBag.FilePath = WebConfigurationManager.AppSettings["domain"].ToString() + WebConfigurationManager.AppSettings["domain_subfolder"].ToString();
            return View(asset);
        }

        [HttpPost]
        public async Task<ActionResult> AddtoExistingAssets(AssetViewModel asset, string addedType)
        {
            //bool IsValid = true;
            //bool IsChanged = false;
            string newContent = string.Empty;

            DB_Entities db = new DB_Entities();
            foreach (AssetContentModel content in asset.Contents)
            {
                if (content.ID == 0)
                {
                    if (string.IsNullOrEmpty(content.Description))
                    {
                        //ModelState.AddModelError("", "Description is required.");
                        //await GetDataListFromSessions();
                        //return View("AddtoExistingAsset", asset);
                    }
                    else
                    {
                        newContent = content.FileName;
                    }
                }
            }

            //if (IsValid)
            //{
            string strActivity = string.Format("Asset titled '{0}' updated. New content titled '{1}' added. ", asset.AssetName, newContent);

            var assetObj = db.Asset.Where(x => x.AssetId == asset.AssetID && x.IsDeleted == false).FirstOrDefault();
            if (assetObj != null)
            {
                //Validate Master Data
                //asset = await ValidateMasterData(asset);

                //strActivity += Convert.ToString(TempData["strActivity"]);
                //IsChanged = Convert.ToBoolean(TempData["IsChanged"]);
                asset.Names = await SaveAssetNames(asset.AssetID, asset.Names);
                asset.Details = await SaveAssetDetails(asset.AssetID, asset.Details);
                //asset.Contents = SaveAssetContents(asset.AssetID, asset.Contents);
                asset.Contents = await SaveAssetContentDetails(asset.AssetID, asset.Contents);

                asset.AssetName = asset.Names[0].Name.Trim();
                assetObj.AssetName = asset.Names[0].Name.Trim();
                assetObj.Type = asset.AssetType;
                assetObj.Status = asset.Status;
                assetObj.Language = asset.Details[0].Language;
                assetObj.Syllabus = asset.Details[0].Syllabus;
                assetObj.Subject = asset.Details[0].Subject;
                assetObj.Level = asset.Details[0].Grade;
                assetObj.Topic = asset.Details[0].Topic;
                assetObj.Subtopic = asset.Details[0].Subtopic;
                assetObj.SubtopicDesc = asset.SubtopicDesc;
                //assetObj.Tags = asset.Tags;
                assetObj.VimeoUrl = asset.VimeoUrl;
                assetObj.Stream = asset.Details[0].Stream;
                assetObj.PackageId = asset.Details[0].PackageId;
                assetObj.IsQuizEnabled = asset.IsQuizEnabled;
                assetObj.PercentageOfLabels = asset.PercentageOfLabels;
                assetObj.NumberOfAttempts = asset.NumberOfAttempts;
                assetObj.IsActive = asset.IsActive;
                assetObj.ModifiedBy = new Guid(Session["UserID"].ToString());
                assetObj.ModifiedByName = Session["Name"].ToString();
                assetObj.ModifiedOn = DateTime.Now;
                assetObj.IsDeleted = asset.IsDeleted;
                db.Entry(assetObj).State = EntityState.Modified;
                db.SaveChanges();

                List<AssetTags> assetTags = db.AssetTags.Where(a => a.AssetId == asset.AssetID).ToList();
                assetTags.ForEach((a) =>
                {
                    db.AssetTags.Remove(a);
                });
                db.SaveChanges();

                if (asset.NewTags != null && asset.NewTags.Length > 0)
                {
                    CommonMethods.SaveAssetTags(assetObj.AssetId, asset.NewTags);
                }

                List<AssetProperties> assetProperties = db.AssetProperties.Where(a => a.AssetId == asset.AssetID).ToList();
                assetProperties.ForEach((a) =>
                {
                    db.AssetProperties.Remove(a);
                });
                db.SaveChanges();

                if (asset.NewProperties != null && asset.NewProperties.Length > 0)
                {
                    CommonMethods.SaveAssetProperties(assetObj.AssetId, asset.NewProperties);
                }

                await ActivityLogger.AddActivityLog(asset.AssetID, "Asset updated", strActivity);
            }
            //}
            //else
            //{
            //    await GetDataListFromSessions();
            //    return View("AddtoExistingAsset", asset);
            //}
            TempData.Keep("Path");
            TempData["Message"] = "Content added to '" + asset.AssetName + "' successfully.";
            return RedirectToAction("AddUpdateAsset", new { @assetID = asset.AssetID });
        }

        public async Task<List<AssetContentModel>> GetAssetContentList(int assetID)
        {
            DB_Entities db = new DB_Entities();
            List<AssetContentModel> listContents = new List<AssetContentModel>();
            var contents = db.AssetContent.Where(x => x.IsDeleted == false && x.AssetId == assetID).OrderBy(x => x.Id).ToList();

            //This is for already added assets, it will make entry into AssetContent table - Starts
            Asset asset = db.Asset.FirstOrDefault(x => x.AssetId == assetID);
            if (contents == null || contents.Count == 0)
            {
                AssetContent newContent = new AssetContent()
                {
                    AssetId = asset.AssetId,
                    FileName = asset.FileName,
                    FilePath = asset.FilePath,
                    FileSize = asset.FileSize,
                    IsDirectory = asset.IsDirectory,
                    VersionNumber = 1,
                    IsEnabled = false,
                    CreatedBy = asset.CreatedBy,
                    CreatedOn = DateTime.Now,
                    Platform = db.CsNewPlatform.Where(x => x.NewName.Equals("All")).Select(x => x.NewPlatformid.ToString()).FirstOrDefault(),
                    IsDeleted = false

                };
                db.AssetContent.Add(newContent);
                await db.SaveChangesAsync();

                contents = await db.AssetContent.Where(x => x.IsDeleted == false && x.AssetId == assetID).OrderBy(x => x.Id).ToListAsync();
            }
            //This is for already added assets, it will make entry into AssetContent table - Ends

            listContents = contents.Select(content => new AssetContentModel()
            {
                ID = content.Id,
                AssetID = content.AssetId,
                FileName = content.FileName,
                FilePath = content.FilePath,
                FileSize = content.FileSize,
                FileSizeText = CommonMethods.GetBytesReadable(Convert.ToInt64(content.FileSize)),
                Thumbnail = content.Thumbnail,
                Background = content.Background,
                IsDirectory = content.IsDirectory,
                VersionNumber = content.VersionNumber,
                PublishFolder = asset.PackageId,
                PlatformId = content.Platform,
                Platform = db.CsNewPlatform.Where(x => x.NewPlatformid == Guid.Parse(content.Platform)).Select(x => x.NewName).FirstOrDefault(),
                IsEnabled = Convert.ToBoolean(content.IsEnabled != null ? content.IsEnabled : false),
                IsDeleted = Convert.ToBoolean(content.IsDeleted != null ? content.IsDeleted : false),
                CreatedBy = content.CreatedBy != null ? content.CreatedBy.Value : new Guid(),
                CreatedOn = content.CreatedOn != null ? content.CreatedOn.Value : DateTime.UtcNow,
                ModifiedBy = content.ModifiedBy != null ? content.ModifiedBy.Value : new Guid(),
                ModifiedOn = content.ModifiedOn != null ? content.ModifiedOn.Value : DateTime.UtcNow
            }).ToList();

            return listContents;
        }

        public List<AssetContentModel> SaveAssetContents(int assetID, List<AssetContentModel> contents)
        {
            DB_Entities db = new DB_Entities();
            foreach (AssetContentModel content in contents)
            {
                if (content.ID == 0)
                {
                    var prevContent = db.AssetContent.Where(x => x.AssetId == assetID).Include(x => x.XmlcontentModel).OrderByDescending(x => x.Id).FirstOrDefault();

                    AssetContent newContent = new AssetContent()
                    {
                        AssetId = assetID,
                        FileName = content.FileName,
                        FilePath = HttpUtility.HtmlDecode(content.FilePath),
                        FileSize = content.FileSize,
                        IsDirectory = content.IsDirectory,
                        VersionNumber = content.VersionNumber,
                        Platform = content.PlatformId,
                        Thumbnail = prevContent?.Thumbnail,
                        Background = prevContent?.Background,
                        IsEnabled = false,
                        CreatedBy = new Guid(Session["UserID"].ToString()),
                        CreatedOn = DateTime.Now,
                        IsDeleted = false
                    };
                    db.AssetContent.Add(newContent);
                    db.SaveChanges();

                    content.ID = newContent.Id;

                    if (prevContent != null && prevContent.XmlcontentModel.FirstOrDefault() != null)
                    {
                        //db.sp_CreateLabels(assetID, newContent.Id);
                        db.Database.ExecuteSqlCommand($"sp_CreateLabels {assetID}, {newContent.Id}");

                        //To generate xml file in publish folder
                        var XmlModel = db.XmlcontentModel.Where(x => x.AssetContentId == newContent.Id).FirstOrDefault();
                        if (XmlModel != null)
                        {
                            string fileName = CommonMethods.GenerateXml(XmlModel.ModelId);
                        }
                    }
                }
                else
                {
                    var contentObj = db.AssetContent.Where(x => x.Id == content.ID && x.IsDeleted == false).FirstOrDefault();
                    if (contentObj != null)
                    {
                        contentObj.VersionNumber = content.VersionNumber;
                        contentObj.IsEnabled = content.IsEnabled;
                        contentObj.ModifiedBy = new Guid(Session["UserID"].ToString());
                        contentObj.ModifiedOn = DateTime.Now;
                        contentObj.IsDeleted = content.IsDeleted;
                        contentObj.Platform = content.PlatformId;
                        db.Entry(contentObj).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                }

                //await Task.Run(() => SaveAssetContentDetail(content));
            }

            return contents.Where(x => x.IsDeleted == false).ToList();
        }

        public async Task<List<AssetContentModel>> SaveAssetContentDetails(int assetID, List<AssetContentModel> contents)
        {
            DB_Entities db = new DB_Entities();
            foreach (AssetContentModel content in contents)
            {
                if (content.ID == 0 && content.IsDeleted == false && !string.IsNullOrWhiteSpace(content.FileName))
                {
                    var prevContent = db.AssetContent.Where(x => x.AssetId == assetID && x.IsDeleted == false).Include(x => x.XmlcontentModel).OrderByDescending(x => x.Id).FirstOrDefault();
                    content.VersionNumber = db.AssetContent.Where(x => x.AssetId == assetID).Count() + 1;
                    AssetContent newContent = new AssetContent()
                    {
                        AssetId = assetID,
                        FileName = content.FileName,
                        FilePath = HttpUtility.HtmlDecode(content.FilePath),
                        FileSize = content.FileSize,
                        IsDirectory = content.IsDirectory,
                        VersionNumber = content.VersionNumber,
                        Platform = content.PlatformId,
                        Thumbnail = prevContent?.Thumbnail,
                        Background = prevContent?.Background,
                        IsEnabled = false,
                        CreatedBy = new Guid(Session["UserID"].ToString()),
                        CreatedOn = DateTime.Now,
                        IsDeleted = false
                    };
                    db.AssetContent.Add(newContent);
                    db.SaveChanges();

                    content.ID = newContent.Id;
                    if (prevContent != null)
                    {
                        await SaveDescriptionWhatsNew(assetID, prevContent.Id, newContent.Id);
                    }

                    if (prevContent != null && prevContent.XmlcontentModel.FirstOrDefault() != null)
                    {
                        //db.sp_CreateLabels(assetID, newContent.Id);
                        db.Database.ExecuteSqlCommand($"sp_CreateLabels {assetID}, {newContent.Id}");

                        //To generate xml file in publish folder
                        var XmlModel = db.XmlcontentModel.Where(x => x.AssetContentId == newContent.Id).FirstOrDefault();
                        if (XmlModel != null)
                        {
                            string fileName = CommonMethods.GenerateXml(XmlModel.ModelId);
                        }
                    }
                }
                else
                {
                    var contentObj = db.AssetContent.Where(x => x.Id == content.ID && x.IsDeleted == false).FirstOrDefault();
                    if (contentObj != null)
                    {
                        contentObj.VersionNumber = content.VersionNumber;
                        contentObj.IsEnabled = content.IsEnabled;
                        contentObj.ModifiedBy = new Guid(Session["UserID"].ToString());
                        contentObj.ModifiedOn = DateTime.Now;
                        contentObj.IsDeleted = content.IsDeleted;
                        contentObj.Platform = content.PlatformId;
                        db.SaveChanges();
                    }
                }
            }

            return contents;
        }

        public async Task SaveDescriptionWhatsNew(int AssetId, int prevContentId, int newContentId)
        {
            using (DB_Entities db = new DB_Entities())
            {
                var details = await db.AssetContentDetail.Where(x => x.AssetContentId == prevContentId && (x.IsDeleted == false || x.IsEnabled == false)).OrderBy(x => x.Id).ToListAsync();

                foreach (var detail in details)
                {
                    AssetContentDetail record = new AssetContentDetail()
                    {
                        AssetContentId = newContentId,
                        Description = detail.Description,
                        WhatIsNew = detail.WhatIsNew,
                        LanguageId = detail.LanguageId,
                        LanguageShortName = detail.LanguageShortName,
                        IsDeleted = detail.IsDeleted,
                        IsEnabled = detail.IsEnabled,
                        CreatedBy = new Guid(Session["UserID"].ToString()),
                        CreatedOn = DateTime.Now
                    };

                    db.AssetContentDetail.Add(record);
                    db.SaveChanges();
                }
            }
        }

        public ActionResult GetContentDetails(int Id)
        {
            using (DB_Entities db = new DB_Entities())
            {
                string path, realPath, filePath;
                List<DirModel> dirListModel = new List<DirModel>();
                List<FileModel> fileListModel = new List<FileModel>();
                var assetObj = db.AssetContent.Where(x => x.Id == Id && x.IsDeleted == false).FirstOrDefault();
                ViewBag.VersionNumber = assetObj.VersionNumber;
                path = assetObj.FilePath;

                realPath = WebConfigurationManager.AppSettings["SourcePath"].ToString() + path;
                int pathLength = realPath.Replace('/', '\\').Length;

                if (assetObj.IsDirectory == true)
                {
                    if (System.IO.Directory.Exists(realPath))
                    {
                        IEnumerable<string> dirList = Directory.EnumerateDirectories(realPath);
                        foreach (string dir in dirList)
                        {
                            DirModel dirModel = new DirModel();
                            DirectoryInfo d = new DirectoryInfo(dir);
                            dirModel.DirName = Path.GetFileName(dir);
                            filePath = path + d.FullName.Substring(pathLength, d.FullName.Length - (pathLength + dirModel.DirName.Length)).Replace('\\', '/');
                            dirModel.DirPath = filePath == "/" ? "" : filePath;
                            long dirSize = CommonMethods.GetDirectorySize(dir);
                            dirModel.DirSize = dirSize.ToString();
                            //dirModel.DirSizeText = (dirSize < 1024) ? dirSize.ToString() + " B" : ((dirSize / 1024) < 1024 ? dirSize / 1024 + " KB" : (dirSize / 1024) / 1024 + " MB");
                            dirModel.DirSizeText = CommonMethods.GetBytesReadable(dirSize);
                            dirListModel.Add(dirModel);
                        }

                        IEnumerable<string> fileList = Directory.EnumerateFiles(realPath);
                        foreach (string file in fileList)
                        {
                            FileModel fileModel = new FileModel();
                            FileInfo f = new FileInfo(file);
                            fileModel.FileName = Path.GetFileName(file);
                            fileModel.FilePath = path + f.Directory.FullName.Substring(pathLength, f.Directory.FullName.Length - (pathLength)).Replace('\\', '/');
                            if (fileModel.FilePath.Length > 0)
                            {
                                fileModel.FilePath = fileModel.FilePath + '/';
                            }
                            fileModel.FileSize = f.Length.ToString();
                            //fileModel.FileSizeText = (f.Length < 1024) ? f.Length.ToString() + " B" : ((f.Length / 1024) < 1024 ? f.Length / 1024 + " KB" : (f.Length / 1024) / 1024 + " MB");
                            fileModel.FileSizeText = CommonMethods.GetBytesReadable(f.Length);
                            fileListModel.Add(fileModel);
                        }
                    }
                }
                else
                {
                    FileModel fileModel = new FileModel();
                    FileInfo f = new FileInfo(realPath);
                    fileModel.FileName = Path.GetFileName(realPath);
                    fileModel.FilePath = path.Substring(0, path.Length - assetObj.FileName.Length);
                    fileModel.FileSize = f.Length.ToString();
                    //fileModel.FileSizeText = (f.Length < 1024) ? f.Length.ToString() + " B" : ((f.Length / 1024) < 1024 ? f.Length / 1024 + " KB" : (f.Length / 1024) / 1024 + " MB");
                    fileModel.FileSizeText = CommonMethods.GetBytesReadable(f.Length);
                    fileListModel.Add(fileModel);
                }

                ExplorerModel explorerModel = new ExplorerModel(dirListModel, fileListModel);
                return PartialView("_AddAsset", explorerModel);
            }
        }

        public void SaveAssetContentDetail(AssetContentModel content)
        {
            using (DB_Entities db = new DB_Entities())
            {
                List<string> languages = new List<string> { "EN", "AR", "NO" };
                foreach (string name in languages)
                {
                    var contentObj = db.AssetContentDetail.Where(x => x.AssetContentId == content.ID && x.LanguageShortName == name).FirstOrDefault();
                    if (contentObj != null)
                    {
                        if (name == "NO")
                        {
                            //contentObj.Description = content.DescriptionInNO;
                            //contentObj.WhatIsNew = content.WhatIsNewInNO;
                        }
                        else if (name == "AR")
                        {
                            //contentObj.Description = content.DescriptionInAR;
                            //contentObj.WhatIsNew = content.WhatIsNewInAR;
                        }
                        else
                        {
                            contentObj.Description = content.Description;
                            contentObj.WhatIsNew = content.WhatIsNew;
                        }

                        contentObj.ModifiedBy = new Guid(Session["UserID"].ToString());
                        contentObj.ModifiedOn = DateTime.Now;
                        db.Entry(contentObj).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                    else
                    {
                        AssetContentDetail newContent = new AssetContentDetail()
                        {
                            AssetContentId = content.ID,
                            LanguageShortName = name,
                            IsEnabled = false,
                            CreatedBy = new Guid(Session["UserID"].ToString()),
                            CreatedOn = DateTime.Now,
                            IsDeleted = false
                        };

                        if (name == "NO")
                        {
                            //newContent.Description = content.DescriptionInNO;
                            //newContent.WhatIsNew = content.WhatIsNewInNO;
                        }
                        else if (name == "AR")
                        {
                            //newContent.Description = content.DescriptionInAR;
                            //newContent.WhatIsNew = content.WhatIsNewInAR;
                        }
                        else
                        {
                            newContent.Description = content.Description;
                            newContent.WhatIsNew = content.WhatIsNew;
                        }

                        db.AssetContentDetail.Add(newContent);
                        db.SaveChanges();
                    }
                }
            }
        }

        #region Upload thumbnail image
        [HttpPost]
        public string UploadImage(HttpPostedFileBase UploadFile, string txtPath, string txtContentID)
        {
            string message = string.Empty;
            int Id = Convert.ToInt16(txtContentID);
            if (UploadFile != null && UploadFile.ContentLength > 0)
            {
                try
                {
                    string path = string.Empty;
                    string filePath = AppDomain.CurrentDomain.BaseDirectory + @"Logfile\Error.txt";

                    //using (StreamWriter writer = new StreamWriter(filePath, true))
                    //{
                    //    writer.WriteLine(Environment.NewLine + "------------------------------------------------------------------------------------------------" + Environment.NewLine);
                    //    writer.WriteLine("UploadImage :" + "Action start" + Environment.NewLine);
                    //    writer.WriteLine(txtPath + Environment.NewLine);
                    //}
                    path = WebConfigurationManager.AppSettings["PublishPath"].ToString() + txtPath;
                    //using (StreamWriter writer = new StreamWriter(filePath, true))
                    //{
                    //    writer.WriteLine(path);
                    //    writer.WriteLine(Environment.NewLine + "------------------------------------------------------------------------------------------------" + Environment.NewLine);
                    //}

                    //Check for the target path if it is exist
                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }

                    path = Path.Combine(path, Path.GetFileName(UploadFile.FileName));

                    UploadFile.SaveAs(path);
                    message = UploadFile.FileName;

                    using (DB_Entities db = new DB_Entities())
                    {
                        var asset = db.AssetContent.Where(x => x.Id == Id).FirstOrDefault();
                        asset.Thumbnail = Path.GetFileName(UploadFile.FileName);
                        db.SaveChanges();
                        Id = asset?.AssetId ?? 0;
                    }
                }
                catch (Exception ex)
                {
                    message = "Exception";//ex.Message.ToString();
                }
            }
            else
            {
                message = "Error";
            }

            //return Content("<script>alert('" + message + "');</script>");
            //return RedirectToAction("AddUpdateAsset", new { @assetID = Id });
            return message;
        }
        #endregion

        #region Upload Background image
        [HttpPost]
        public string BackgroundImage(HttpPostedFileBase UploadBGFile, string txtBGPath, string txtBGContentID)
        {
            string message = string.Empty;
            int Id = Convert.ToInt16(txtBGContentID);
            if (UploadBGFile != null && UploadBGFile.ContentLength > 0)
            {
                try
                {
                    string path = string.Empty;
                    path = WebConfigurationManager.AppSettings["PublishPath"].ToString() + txtBGPath;

                    //Check for the target path if it is exist
                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }

                    path = Path.Combine(path, Path.GetFileName(UploadBGFile.FileName));

                    UploadBGFile.SaveAs(path);
                    message = UploadBGFile.FileName;

                    using (DB_Entities db = new DB_Entities())
                    {
                        var asset = db.AssetContent.Where(x => x.Id == Id).FirstOrDefault();
                        asset.Background = Path.GetFileName(UploadBGFile.FileName);
                        db.SaveChanges();
                        Id = asset?.AssetId ?? 0;
                    }
                }
                catch (Exception ex)
                {
                    message = "Exception";
                }
            }
            else
            {
                message = "Error";
            }

            return message;
        }
        #endregion

        #region Activity
        public ActionResult GetActivity(int assetID)
        {
            ViewBag.AssetId = assetID;
            return PartialView("_Activities");
        }

        public ActionResult GetAssetActivities([DataSourceRequest] DataSourceRequest request, int Id)
        {
            DB_Entities db = new DB_Entities();
            List<ActivityModel> listActivities = new List<ActivityModel>();
            var activities = (from a in db.Activities
                              join q in db.QuizDetails on a.ActivityId equals q.QuizId
                              where q.AssetId == Id && a.IsDeleted == false
                              select a).ToList();

            var puzzles = (from a in db.Activities
                           join p in db.PuzzleDetails on a.ActivityId equals p.PuzzleId
                           where p.AssetId == Id && a.IsDeleted == false
                           select a).ToList();

            activities.AddRange(puzzles);

            listActivities = activities.Select(activity => new ActivityModel()
            {
                ActivityId = activity.ActivityId,
                ActivityName = activity.ActivityName,
                ActivityLanguageId = activity.LanguageId,
                IsModelSpecific = activity.IsModelSpecific,
                ActivityType = activity.ActivityType,
                ActivityTypeName = db.MstActivity.Where(x => x.ActivityId == activity.ActivityType).Select(x => x.ActivityName).FirstOrDefault(),
                IsActivityEnabled = activity.IsEnabled,
                IsActivityDeleted = activity.IsDeleted
            }).Distinct().ToList();

            return Json(listActivities.ToDataSourceResult(request));
        }

        public List<Label> GetAssetLabels(int assetID)
        {
            DB_Entities db = new DB_Entities();
            List<Label> listLabels = new List<Label>();
            var model = (from ac in db.AssetContent
                         join x in db.XmlcontentModel
                         on ac.Id equals x.AssetContentId
                         where ac.AssetId == assetID && ac.IsDeleted == false //&& x.IsDeleted == false
                         select x).OrderByDescending(x => x.ModelId).FirstOrDefault();

            if (model != null)
            {
                var labels = (from a in db.XmlcontentLabel.Include(x => x.XmlContentLabelNames)
                              where a.ModelId == model.ModelId && a.IsEnabled == true
                              select a).Distinct().ToList();

                listLabels = labels.Select(label => new Label()
                {
                    LabelID = label.LabelId,
                    ModelID = model.ModelId,
                    AssetContentId = model.AssetContentId,
                    number = label.Number ?? 0,
                    EN = label.En,
                    NO = label.No,
                    AR = label.Ar,
                    NN = label.Nn,
                    IsEnabled = label.IsEnabled,
                    IsDeleted = label.IsDeleted
                }).OrderBy(X => X.EN).ToList();
            }

            return listLabels;
        }

        public ActionResult GetQuiz(int assetID)
        {
            DB_Entities db = new DB_Entities();
            List<AssetNameModel> listNames = new List<AssetNameModel>();
            var platforms = GetPlatforms();
            ViewBag.Language = db.CsNewLanguage.Select(x => new SelectListItem { Text = x.NewName, Value = x.NewLanguageid.ToString() }).OrderBy(x => x.Text).ToList();

            var act = (from a in db.Activities
                       join q in db.QuizDetails on a.ActivityId equals q.QuizId
                       where q.AssetId == assetID && a.IsDeleted == false && a.IsModelSpecific == true
                       select new QuizModel()
                       {
                           QuizId = a.ActivityId,
                           QuizName = a.ActivityName,
                           //ActivityLanguageId = a.LanguageId,
                           //ActivityType = a.ActivityType,
                           //ActivityTypeName = db.MstActivity.Where(x => x.ActivityId == a.ActivityType).Select(x => x.ActivityName).FirstOrDefault(),
                           IsQuizEnabled = a.IsEnabled,
                           IsQuizDeleted = a.IsDeleted,
                           IsQuizModelSpecific = a.IsModelSpecific
                       }).FirstOrDefault();

            if (act == null)
            {
                act = new QuizModel() { QuizId = 0, QuizName = "", IsQuizModelSpecific = true };
            }

            //ViewBag.Labels = GetAssetLabels(assetID);
            var labels = GetAssetLabels(assetID);

            if (act.QuizId == 0)
            {
                AssetNameModel name = new AssetNameModel();
                listNames.Add(name);
                act.Names = listNames;
                act.Platforms = platforms;

                if (labels.Count != 0)
                {
                    act.QuizDetails.Add(new QuizDetailModel() { Id = 0, QuizId = 0, AssetId = assetID, AssetContentId = labels[0].AssetContentId ?? 0, ModelId = labels[0].ModelID });
                }
                else
                {
                    act.QuizDetails.Add(new QuizDetailModel() { Id = 0, QuizId = 0, AssetId = assetID, AssetContentId = 0, ModelId = 0 });
                }

                //act.QuizLabels.Add(new QuizLabelDetailModel() { Id = 0, QuizId = 0, LabelId = 0 });
                act.QuizDetails[0].QuizLabels = labels.Select(label => new QuizLabelDetailModel()
                {
                    Id = 0,
                    QuizDetailId = 0,
                    LabelId = label.LabelID,
                    LabelName = label.EN,
                    IsSelected = false
                }).ToList();
            }
            else
            {
                listNames = GetActivityNames(act.QuizId);
                act.Names = listNames;

                var listPlatform = (from ap in db.ActivityPlatforms where ap.ActivityId == act.QuizId select ap).ToList();
                act.Platforms = (from p in platforms
                                 join l in listPlatform on p.PlatformId equals l.PlatformId into ap
                                 from l in ap.DefaultIfEmpty()
                                 select new PlatformModel()
                                 {
                                     Id = l == null ? 0 : l.Id,
                                     PlatformId = p.PlatformId,
                                     PlatformName = p.PlatformName,
                                     IsSelected = l == null ? false : l.IsSelected
                                 }).OrderByDescending(x => x.IsSelected).ThenBy(x => x.PlatformName).ToList();

                act.QuizDetails = (from qd in db.QuizDetails
                                   where qd.QuizId == act.QuizId
                                   select new QuizDetailModel()
                                   {
                                       Id = qd.Id,
                                       QuizId = qd.QuizId,
                                       AssetId = assetID,
                                       AssetContentId = qd.AssetContentId,
                                       ModelId = qd.ModelId,
                                       NumberOfLabels = qd.NumberOfLabels,
                                       NumberOfAttempts = qd.NumberOfAttempts,
                                       Labels = qd.Labels
                                   }).ToList();

                if (act.QuizDetails.Count != 0)
                {
                    act.QuizDetails[0].QuizLabels = (from l in labels
                                                     join q in db.QuizLabelDetails.Where(o => o.QuizDetailId == act.QuizDetails[0].Id) on l.EN equals q.LabelName into lq
                                                     from q in lq.DefaultIfEmpty()
                                                     select new QuizLabelDetailModel()
                                                     {
                                                         Id = q == null ? 0 : q.Id,
                                                         QuizDetailId = q == null ? 0 : q.QuizDetailId,
                                                         LabelId = l.LabelID,
                                                         LabelName = l.EN,
                                                         IsSelected = q == null ? false : (q.IsEnabled ?? false)
                                                     }).ToList();
                }
                else
                {
                    act.QuizDetails.Add(new QuizDetailModel() { Id = 0, QuizId = 0, AssetId = assetID, AssetContentId = 0, ModelId = 0 });

                    act.QuizDetails[0].QuizLabels = labels.Select(label => new QuizLabelDetailModel()
                    {
                        Id = 0,
                        QuizDetailId = 0,
                        LabelId = label.LabelID,
                        LabelName = label.EN,
                        IsSelected = false
                    }).ToList();
                }
            }

            return PartialView("_Quiz", act);
        }

        public ActionResult GetPuzzle(int assetID)
        {
            DB_Entities db = new DB_Entities();
            List<AssetNameModel> listNames = new List<AssetNameModel>();
            var platforms = GetPlatforms();
            ViewBag.Language = db.CsNewLanguage.Select(x => new SelectListItem { Text = x.NewName, Value = x.NewLanguageid.ToString() }).OrderBy(x => x.Text).ToList();

            var puzzle = (from a in db.Activities
                          join p in db.PuzzleDetails on a.ActivityId equals p.PuzzleId
                          where p.AssetId == assetID && a.IsDeleted == false && a.IsModelSpecific == true
                          select new PuzzleModel()
                          {
                              PuzzleId = a.ActivityId,
                              PuzzleName = a.ActivityName,
                              //ActivityLanguageId = a.LanguageId,
                              //ActivityType = a.ActivityType,
                              //ActivityTypeName = db.MstActivity.Where(x => x.ActivityId == a.ActivityType).Select(x => x.ActivityName).FirstOrDefault(),
                              IsPuzzleEnabled = a.IsEnabled,
                              IsPuzzleDeleted = a.IsDeleted,
                              IsPuzzleModelSpecific = a.IsModelSpecific
                          }).FirstOrDefault();

            if (puzzle == null)
            {
                puzzle = new PuzzleModel() { PuzzleId = 0, PuzzleName = "", IsPuzzleModelSpecific = true };
            }

            var num = GetAssetLabels(assetID);

            if (puzzle.PuzzleId == 0)
            {
                AssetNameModel name = new AssetNameModel();
                listNames.Add(name);
                puzzle.Names = listNames;
                puzzle.PuzzlePlatforms = platforms;

                if (num.Count != 0)
                {
                    puzzle.PuzzleDetails.Add(new PuzzleDetailModel() { Id = 0, PuzzleId = 0, AssetId = assetID, NumberOfParts = num.Count, AssetContentId = num[0].AssetContentId ?? 0, ModelId = num[0].ModelID });
                }
                else
                {
                    puzzle.PuzzleDetails.Add(new PuzzleDetailModel() { Id = 0, PuzzleId = 0, AssetId = assetID, NumberOfParts = 0, ModelId = 0 });
                }
            }
            else
            {
                listNames = GetActivityNames(puzzle.PuzzleId);
                puzzle.Names = listNames;

                var listPlatform = (from ap in db.ActivityPlatforms where ap.ActivityId == puzzle.PuzzleId select ap).ToList();
                puzzle.PuzzlePlatforms = (from p in platforms
                                          join l in listPlatform on p.PlatformId equals l.PlatformId into ap
                                          from l in ap.DefaultIfEmpty()
                                          select new PlatformModel()
                                          {
                                              Id = l == null ? 0 : l.Id,
                                              PlatformId = p.PlatformId,
                                              PlatformName = p.PlatformName,
                                              IsSelected = l == null ? false : l.IsSelected
                                          }).OrderByDescending(x => x.IsSelected).ThenBy(x => x.PlatformName).ToList();

                puzzle.PuzzleDetails = (from pd in db.PuzzleDetails
                                        where pd.PuzzleId == puzzle.PuzzleId
                                        select new PuzzleDetailModel()
                                        {
                                            Id = pd.Id,
                                            PuzzleId = pd.PuzzleId,
                                            AssetId = assetID,
                                            AssetContentId = pd.AssetContentId,
                                            ModelId = pd.ModelId,
                                            NumberOfParts = pd.NumberOfParts
                                        }).ToList();
            }

            return PartialView("_Puzzle", puzzle);
        }

        public List<AssetNameModel> GetActivityNames(int activityID)
        {
            using (DB_Entities db = new DB_Entities())
            {
                List<AssetNameModel> listNames = new List<AssetNameModel>();
                var names = db.ActivityNames.Where(x => x.ActivityId == activityID && x.IsDeleted == false).OrderBy(x => x.Id).ToList();

                listNames = names.Select(name => new AssetNameModel()
                {
                    ID = name.Id,
                    Name = name.Name,
                    LanguageId = name.LanguageId,
                    Language = db.CsNewLanguage.Where(x => x.NewLanguageid == name.LanguageId).Select(x => x.NewName).FirstOrDefault(),
                    IsDeleted = name.IsDeleted
                }).ToList();

                return listNames;
            }
        }

        public List<PlatformModel> GetPlatforms()
        {
            using (DB_Entities db = new DB_Entities())
            {
                var platforms = (from p in db.CsNewPlatform
                                 select new PlatformModel()
                                 {
                                     Id = 0,
                                     PlatformId = p.Id,
                                     PlatformName = p.NewName,
                                     IsSelected = false
                                 }).OrderBy(X => X.PlatformName).ToList();

                return platforms;
            }
        }

        [HttpPost]
        public JsonResult SaveQuiz(QuizModel quiz)
        {
            //if (!ModelState.IsValid)
            //{
            //    ModelState.AddModelError("QuizName", "Name is required.");
            //    return Json("Quiz Name is required!", JsonRequestBehavior.AllowGet);
            //}
            List<AssetNameModel> newNames = quiz.Names.Where(d => d.IsDeleted == false && (d.LanguageId == Guid.Empty || string.IsNullOrWhiteSpace(d.Name))).ToList();

            using (DB_Entities db = new DB_Entities())
            {
                ViewBag.Language = db.CsNewLanguage.Select(x => new SelectListItem { Text = x.NewName, Value = x.NewLanguageid.ToString() }).OrderBy(x => x.Text).ToList();

                if (newNames.Count > 0)
                {
                    return Json("Select language and provide valid name for Quiz.", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    if (quiz.QuizId == 0)
                    {
                        Activities newActivityObj = new Activities()
                        {
                            ActivityName = quiz.Names[0].Name.Trim(),
                            LanguageId = quiz.Names[0].LanguageId,
                            ActivityType = 1,
                            IsModelSpecific = true,
                            IsEnabled = quiz.IsQuizEnabled,
                            CreatedBy = new Guid(Session["UserID"].ToString()),
                            CreatedOn = DateTime.Now,
                            IsDeleted = quiz.IsQuizDeleted
                        };
                        db.Activities.Add(newActivityObj);
                        db.SaveChanges();
                        quiz.QuizId = newActivityObj.ActivityId;

                        QuizDetails newObj = new QuizDetails()
                        {
                            QuizId = newActivityObj.ActivityId,
                            AssetId = quiz.QuizDetails[0].AssetId,
                            AssetContentId = quiz.QuizDetails[0].AssetContentId,
                            ModelId = quiz.QuizDetails[0].ModelId,
                            NumberOfAttempts = quiz.QuizDetails[0].NumberOfAttempts,
                            NumberOfLabels = quiz.QuizDetails[0].NumberOfLabels,
                            Labels = quiz.QuizDetails[0].Labels,
                            IsEnabled = quiz.IsQuizEnabled,
                            CreatedBy = new Guid(Session["UserID"].ToString()),
                            CreatedOn = DateTime.Now,
                            IsDeleted = false
                        };

                        db.QuizDetails.Add(newObj);
                        db.SaveChanges();
                        quiz.QuizDetails[0].Id = newObj.Id;

                        foreach (var label in quiz.QuizDetails[0].QuizLabels)
                        {
                            QuizLabelDetails newLabelObj = new QuizLabelDetails()
                            {
                                QuizDetailId = newObj.Id,
                                LabelId = label.LabelId,
                                LabelName = label.LabelName,
                                IsEnabled = label.IsSelected,
                                CreatedBy = new Guid(Session["UserID"].ToString()),
                                CreatedOn = DateTime.Now,
                                IsDeleted = false
                            };

                            db.QuizLabelDetails.Add(newLabelObj);
                            db.SaveChanges();
                            label.Id = newLabelObj.Id;
                        }
                    }
                    else
                    {
                        var activityObj = db.Activities.Where(x => x.ActivityId == quiz.QuizId && x.IsDeleted == false).FirstOrDefault();
                        if (activityObj != null)
                        {
                            activityObj.ActivityName = quiz.Names[0].Name.Trim();
                            activityObj.LanguageId = quiz.Names[0].LanguageId;
                            activityObj.IsEnabled = quiz.IsQuizEnabled;
                            activityObj.ModifiedBy = new Guid(Session["UserID"].ToString());
                            activityObj.ModifiedOn = DateTime.Now;
                            activityObj.IsDeleted = quiz.IsQuizDeleted;
                            db.Entry(activityObj).State = EntityState.Modified;
                            db.SaveChanges();
                        }

                        var pObj = db.QuizDetails.Where(x => x.QuizId == quiz.QuizId && x.IsDeleted == false).FirstOrDefault();
                        if (pObj != null)
                        {
                            pObj.NumberOfAttempts = quiz.QuizDetails[0].NumberOfAttempts;
                            pObj.NumberOfLabels = quiz.QuizDetails[0].NumberOfLabels;
                            pObj.Labels = quiz.QuizDetails[0].Labels;
                            pObj.IsEnabled = quiz.IsQuizEnabled;
                            pObj.ModifiedBy = new Guid(Session["UserID"].ToString());
                            pObj.ModifiedOn = DateTime.Now;
                            pObj.IsDeleted = quiz.IsQuizDeleted;
                            db.Entry(pObj).State = EntityState.Modified;
                            db.SaveChanges();
                        }

                        foreach (var label in quiz.QuizDetails[0].QuizLabels)
                        {
                            if (label.Id == 0)
                            {
                                QuizLabelDetails newLabelObj = new QuizLabelDetails()
                                {
                                    QuizDetailId = quiz.QuizDetails[0].Id,
                                    LabelId = label.LabelId,
                                    LabelName = label.LabelName,
                                    IsEnabled = label.IsSelected,
                                    CreatedBy = new Guid(Session["UserID"].ToString()),
                                    CreatedOn = DateTime.Now,
                                    IsDeleted = false
                                };

                                db.QuizLabelDetails.Add(newLabelObj);
                                db.SaveChanges();
                                label.Id = newLabelObj.Id;
                            }
                            else
                            {
                                var lObj = db.QuizLabelDetails.Where(x => x.Id == label.Id).FirstOrDefault();
                                if (lObj != null)
                                {
                                    lObj.LabelName = label.LabelName;
                                    lObj.IsEnabled = label.IsSelected;
                                    lObj.ModifiedBy = new Guid(Session["UserID"].ToString());
                                    lObj.ModifiedOn = DateTime.Now;
                                    db.Entry(lObj).State = EntityState.Modified;
                                    db.SaveChanges();
                                }
                            }
                        }
                    }
                }
            }

            quiz.Names = SaveActivityNames(quiz.QuizId, quiz.Names);
            quiz.Platforms = SaveActivityPlatforms(quiz.QuizId, quiz.Platforms);
            return Json("Quiz saved successfully!", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SavePuzzle(PuzzleModel puzzle)
        {
            List<AssetNameModel> newNames = puzzle.Names.Where(d => d.IsDeleted == false && (d.LanguageId == Guid.Empty || string.IsNullOrWhiteSpace(d.Name))).ToList();
            string strAction = "Puzzle updated";
            string strActivity = string.Format("Puzzle titled '{0}' ", puzzle.PuzzleName);

            using (DB_Entities db = new DB_Entities())
            {
                ViewBag.Language = db.CsNewLanguage.Select(x => new SelectListItem { Text = x.NewName, Value = x.NewLanguageid.ToString() }).OrderBy(x => x.Text).ToList();

                if (newNames.Count > 0)
                {
                    return Json("Select language and provide valid name for Puzzle.", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    if (puzzle.PuzzleId == 0)
                    {
                        Activities newActivityObj = new Activities()
                        {
                            ActivityName = puzzle.Names[0].Name.Trim(),
                            LanguageId = puzzle.Names[0].LanguageId,
                            ActivityType = 2,
                            IsModelSpecific = true,
                            IsEnabled = puzzle.IsPuzzleEnabled,
                            CreatedBy = new Guid(Session["UserID"].ToString()),
                            CreatedOn = DateTime.Now,
                            IsDeleted = puzzle.IsPuzzleDeleted
                        };
                        db.Activities.Add(newActivityObj);
                        db.SaveChanges();
                        puzzle.PuzzleId = newActivityObj.ActivityId;

                        strAction = "Puzzle added";
                        strActivity += string.Format("added; ");

                        PuzzleDetails newObj = new PuzzleDetails()
                        {
                            PuzzleId = newActivityObj.ActivityId,
                            AssetId = puzzle.PuzzleDetails[0].AssetId,
                            AssetContentId = puzzle.PuzzleDetails[0].AssetContentId,
                            ModelId = puzzle.PuzzleDetails[0].ModelId,
                            NumberOfParts = puzzle.PuzzleDetails[0].NumberOfParts,
                            IsEnabled = puzzle.IsPuzzleEnabled,
                            CreatedBy = new Guid(Session["UserID"].ToString()),
                            CreatedOn = DateTime.Now,
                            IsDeleted = false
                        };
                        db.PuzzleDetails.Add(newObj);
                        db.SaveChanges();
                    }
                    else
                    {
                        var activityObj = db.Activities.Where(x => x.ActivityId == puzzle.PuzzleId && x.IsDeleted == false).FirstOrDefault();
                        if (activityObj != null)
                        {
                            if (activityObj.ActivityName != puzzle.Names[0].Name.Trim())
                            {
                                strActivity += string.Format("updated from '{0}'; ", activityObj.ActivityName);
                            }

                            if (activityObj.IsEnabled != puzzle.IsPuzzleEnabled)
                            {
                                strActivity += string.Format("updated. Activation status changed from '{0}' to '{1}'; ", activityObj.IsEnabled, puzzle.IsPuzzleEnabled);
                            }

                            activityObj.ActivityName = puzzle.Names[0].Name.Trim();
                            activityObj.LanguageId = puzzle.Names[0].LanguageId;
                            activityObj.IsEnabled = puzzle.IsPuzzleEnabled;
                            activityObj.ModifiedBy = new Guid(Session["UserID"].ToString());
                            activityObj.ModifiedOn = DateTime.Now;
                            activityObj.IsDeleted = puzzle.IsPuzzleDeleted;
                            db.Entry(activityObj).State = EntityState.Modified;
                            db.SaveChanges();

                            if (puzzle.IsPuzzleDeleted == true)
                            {
                                strAction = "Puzzle deleted";
                                strActivity += string.Format("deleted; ");
                            }
                        }

                        var pObj = db.PuzzleDetails.Where(x => x.PuzzleId == puzzle.PuzzleId && x.IsDeleted == false).FirstOrDefault();
                        if (pObj != null)
                        {
                            pObj.IsEnabled = puzzle.IsPuzzleEnabled;
                            pObj.ModifiedBy = new Guid(Session["UserID"].ToString());
                            pObj.ModifiedOn = DateTime.Now;
                            pObj.IsDeleted = puzzle.IsPuzzleDeleted;
                            db.Entry(pObj).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                    }
                }
            }

            puzzle.Names = SaveActivityNames(puzzle.PuzzleId, puzzle.Names);
            puzzle.PuzzlePlatforms = SaveActivityPlatforms(puzzle.PuzzleId, puzzle.PuzzlePlatforms);
            ActivityLogger.AddActivityLogs(puzzle.PuzzleId, strAction, strActivity.Substring(0, strActivity.Length - 2));
            return Json("Puzzle saved successfully!", JsonRequestBehavior.AllowGet);
        }

        public List<AssetNameModel> SaveActivityNames(int activityID, List<AssetNameModel> names)
        {
            DB_Entities db = new DB_Entities();

            db.ActivityNames.Where(w => w.ActivityId == activityID && w.IsDeleted == false).ToList().ForEach(i => i.IsDeleted = true);
            db.SaveChanges();

            foreach (AssetNameModel name in names)
            {
                if (name.ID == 0 && name.IsDeleted == false)
                {
                    ActivityNames newName = new ActivityNames()
                    {
                        ActivityId = activityID,
                        Name = name.Name,
                        LanguageId = name.LanguageId,
                        IsDeleted = name.IsDeleted
                    };
                    db.ActivityNames.Add(newName);
                    db.SaveChanges();

                    name.ID = newName.Id;
                }
                else if (name.IsDeleted == false)
                {
                    var nameObj = db.ActivityNames.Where(x => x.Id == name.ID).FirstOrDefault();
                    if (nameObj != null)
                    {
                        nameObj.Name = name.Name;
                        nameObj.LanguageId = name.LanguageId;
                        nameObj.IsDeleted = name.IsDeleted;
                        db.SaveChanges();
                    }
                }
            }

            List<AssetNameModel> newNames = names.Where(d => d.IsDeleted == false).ToList();
            return newNames;
        }

        public List<PlatformModel> SaveActivityPlatforms(int activityID, List<PlatformModel> platforms)
        {
            DB_Entities db = new DB_Entities();

            foreach (PlatformModel platform in platforms)
            {
                if (platform.Id == 0 && platform.IsSelected == true)
                {
                    ActivityPlatforms newPlatform = new ActivityPlatforms()
                    {
                        ActivityId = activityID,
                        PlatformId = platform.PlatformId,
                        IsSelected = platform.IsSelected
                    };
                    db.ActivityPlatforms.Add(newPlatform);
                    db.SaveChanges();

                    platform.Id = newPlatform.Id;
                }
                else
                {
                    var objPlatform = db.ActivityPlatforms.Where(x => x.Id == platform.Id).FirstOrDefault();
                    if (objPlatform != null)
                    {
                        objPlatform.IsSelected = platform.IsSelected;
                        db.SaveChanges();
                    }
                }
            }

            return platforms;
        }
        #endregion

        public ActionResult GetLinkedAssets(int assetID, string asset)
        {
            DB_Entities db = new DB_Entities();
            string selected = string.Empty;
            int count = 0;
            var assets = GetAssets(assetID);

            var relation = new AssetRelationModel() { SelectedAssetId = assetID, SelectedAssetName = asset };
            relation.LinkedAssets = (from a in assets
                                     join r in db.AssetRelations.Where(o => o.AssetId == assetID) on a.AssetId equals r.LinkedAssetId into ar
                                     from r in ar.DefaultIfEmpty()
                                     select new LinkedAsset()
                                     {
                                         RelationId = r == null ? 0 : r.RelationId,
                                         LinkedAssetId = a.AssetId,
                                         LinkedAssetName = a.AssetName,
                                         LinkedContentId = a.AssetContentId,
                                         IsSelected = r == null ? false : (r.IsSelected ?? false)
                                     }).OrderByDescending(x => x.IsSelected).ThenBy(x => x.LinkedAssetName).ToList();

            foreach (var pd in relation.LinkedAssets)
            {
                if (pd.IsSelected == true)
                {
                    selected = selected + pd.LinkedAssetName + ", ";
                    count++;
                }
                else
                {
                    break;
                }
            }

            ViewBag.SelectedCount = count;
            if (!string.IsNullOrEmpty(selected))
            {
                ViewBag.SelectedAsset = selected.Substring(0, selected.Length - 2);
            }

            return PartialView("_AssociatedContent", relation);
        }

        public List<ActivityAsset> GetAssets(int assetId)
        {
            DB_Entities db = new DB_Entities();
            List<ActivityAsset> listAssets = new List<ActivityAsset>();
            var assets = (from a in db.Asset
                          join ac in db.AssetContent on a.AssetId equals ac.AssetId
                          where a.IsDeleted == false && ac.IsEnabled == true && a.AssetId != assetId
                          select new ActivityAsset()
                          {
                              AssetId = a.AssetId,
                              AssetName = a.AssetName,
                              AssetContentId = ac.Id
                          }).ToList();

            listAssets = (from l in assets
                          group l by l.AssetId into g
                          select new ActivityAsset()
                          {
                              AssetId = g.Max(s => s.AssetId),
                              AssetName = g.Max(s => s.AssetName),
                              AssetContentId = g.Max(s => s.AssetContentId)
                          }).ToList();

            return listAssets;
        }

        #region Select Asset
        public ActionResult GetAllModule([DataSourceRequest] DataSourceRequest request, int Id)
        {
            List<AssetViewModel> listAssets = new List<AssetViewModel>();
            using (DB_Entities db = new DB_Entities())
            {
                var assets = GetAssets(Id);
                //var assets = db.Asset.Where(x => x.IsDeleted == false).OrderBy(x => x.AssetName).ToList();

                listAssets = (from at in assets
                              join a in db.Asset on at.AssetId equals a.AssetId
                              join ac in db.AssetContent on at.AssetContentId equals ac.Id
                              join r in db.AssetRelations.Where(o => o.AssetId == Id) on a.AssetId equals r.LinkedAssetId into ar
                              from r in ar.DefaultIfEmpty()
                                  //where a.IsDeleted == false && a.AssetId != Id
                              select new AssetViewModel()
                              {
                                  AssetID = a.AssetId,
                                  AssetName = a.AssetName,
                                  FileName = ac.FileName,
                                  FilePath = ac.FilePath,
                                  FileSize = Convert.ToInt64(ac.FileSize),
                                  FileSizeText = CommonMethods.GetBytesReadable(Convert.ToInt64(ac.FileSize)),
                                  IsDirectory = ac.IsDirectory,
                                  Status = a.Status,
                                  AssetType = a.Type,
                                  Language = a.Language,
                                  Syllabus = a.Syllabus,
                                  Stream = a.Stream,
                                  Subject = a.Subject,
                                  Level = a.Level,
                                  Topic = a.Topic,
                                  Subtopic = a.Subtopic,
                                  SubtopicDesc = a.SubtopicDesc,
                                  //Tags = a.Tags,
                                  IsActive = a.IsActive ?? true,
                                  IsDeleted = r == null ? false : (r.IsSelected ?? false),
                                  CreatedBy = a.CreatedBy != null ? a.CreatedBy.Value : new Guid(),
                                  CreatedOn = a.CreatedOn != null ? a.CreatedOn.Value : DateTime.UtcNow,
                                  ModifiedOn = a.ModifiedOn != null ? a.ModifiedOn.Value : DateTime.UtcNow,
                                  ModifiedByName = a.ModifiedByName
                              }).OrderByDescending(x => x.IsDeleted).ThenBy(x => x.AssetName).ToList();

            }

            return Json(listAssets.ToDataSourceResult(request));
        }
        #endregion

        [HttpPost]
        public ActionResult SaveLinkedAssets(AssetRelationModel relation)
        {
            if (!ModelState.IsValid)
            {
                return Json("Some error occured!", JsonRequestBehavior.AllowGet);
            }

            using (DB_Entities db = new DB_Entities())
            {
                var selected = relation.LinkedAssets.Where(x => x.IsSelected == true).ToList();
                db.AssetRelations.Where(w => w.AssetId == relation.SelectedAssetId && w.IsSelected == true).ToList().ForEach(i => i.IsSelected = false);

                foreach (var rel in selected)
                {
                    if (rel.RelationId == 0)
                    {
                        AssetRelations newObj = new AssetRelations()
                        {
                            AssetId = relation.SelectedAssetId,
                            LinkedAssetId = rel.LinkedAssetId,
                            LinkedContentId = rel.LinkedContentId,
                            IsSelected = rel.IsSelected,
                            CreatedBy = new Guid(Session["UserID"].ToString()),
                            CreatedOn = DateTime.Now,
                            IsDeleted = false
                        };

                        db.AssetRelations.Add(newObj);
                        db.SaveChanges();
                        rel.RelationId = newObj.RelationId;
                    }
                    else
                    {
                        var rObj = db.AssetRelations.Where(x => x.RelationId == rel.RelationId).FirstOrDefault();
                        if (rObj != null)
                        {
                            rObj.LinkedAssetId = rel.LinkedAssetId;
                            rObj.LinkedContentId = rel.LinkedContentId;
                            rObj.IsSelected = rel.IsSelected;
                            rObj.ModifiedBy = new Guid(Session["UserID"].ToString());
                            rObj.ModifiedOn = DateTime.Now;
                            db.Entry(rObj).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                    }
                }
            }

            return Json("Relation for the model '" + relation.SelectedAssetName + "' saved successfully!", JsonRequestBehavior.AllowGet);
        }

        #region Assign package and other details
        public ActionResult GetNewRow()
        {
            using (DB_Entities db = new DB_Entities())
            {
                ViewBag.Package = db.CsNewPackage.Select(t => new SelectListItem { Text = t.NewName, Value = t.NewPackageid.ToString() }).OrderBy(x => x.Text).ToList();
            }
            return PartialView("_AddRow");
        }

        [HttpPost]
        public string AssignDetails(string txtAssetIds, string txtPackageId, string txtLanguageId, string txtGradeId,
            string txtSyllabusId, string txtStreamId, string txtSubjectId, string txtTopicId, string txtSubtopicId)
        {
            string message = string.Empty;
            if (Guid.Parse(txtPackageId) != Guid.Empty)
            {
                try
                {
                    List<int> AssetIds = txtAssetIds.Split(',').Select(int.Parse).ToList();
                    using (DB_Entities db = new DB_Entities())
                    {
                        foreach (var id in AssetIds)
                        {
                            AssetDetails newDetail = new AssetDetails()
                            {
                                AssetId = id,
                                PackageId = Guid.Parse(txtPackageId),
                                LanguageId = Guid.Parse(txtLanguageId),
                                GradeId = Guid.Parse(txtGradeId),
                                SyllabusId = Guid.Parse(txtSyllabusId),
                                StreamId = Guid.Parse(txtStreamId),
                                SubjectId = Guid.Parse(txtSubjectId),
                                TopicId = Guid.Parse(txtTopicId),
                                SubtopicId = Guid.Parse(txtSubtopicId),
                                IsDeleted = false
                            };
                            db.AssetDetails.Add(newDetail);
                            db.SaveChanges();
                        }
                    }
                }
                catch (Exception ex)
                {
                    message = "Exception";//ex.Message.ToString();
                }
            }
            else
            {
                message = "Error";
            }

            return message;
        }
        #endregion

        public ActionResult RemoveContent(string Id)
        {
            string message = string.Empty;
            int contentId = Convert.ToInt16(Id);
            if (contentId != 0)
            {
                try
                {
                    using (DB_Entities db = new DB_Entities())
                    {
                        var content = db.AssetContent.Where(x => x.Id == contentId).FirstOrDefault();
                        content.IsDeleted = true;
                        db.SaveChanges();

                        string path = string.Empty;
                        path = WebConfigurationManager.AppSettings["SourcePath"].ToString();
                        string fileName = content.FilePath;

                        bool IsDeleted = false;
                        try
                        {
                            if (content.IsDirectory == true)
                            {
                                System.IO.DirectoryInfo myDirInfo = new DirectoryInfo(Path.Combine(path, fileName));

                                if (Directory.Exists(myDirInfo.FullName))
                                {
                                    foreach (FileInfo file in myDirInfo.GetFiles())
                                    {
                                        System.IO.File.SetAttributes(file.FullName, FileAttributes.Normal);
                                        file.Delete();
                                    }
                                    foreach (DirectoryInfo dir in myDirInfo.GetDirectories())
                                    {
                                        dir.Delete(true);
                                    }

                                    myDirInfo.Delete(true);
                                }
                            }
                            else
                            {
                                System.IO.FileInfo file = new FileInfo(Path.Combine(path, fileName));
                                if (System.IO.File.Exists(file.FullName))
                                {
                                    System.IO.File.SetAttributes(file.FullName, FileAttributes.Normal);
                                    file.Delete();
                                    //System.IO.File.Delete(file.FullName);
                                }
                            }

                            IsDeleted = true;
                        }
                        catch (Exception ex)
                        {
                            string filePath = AppDomain.CurrentDomain.BaseDirectory + @"Logfile\Error.txt";

                            using (StreamWriter writer = new StreamWriter(filePath, true))
                            {
                                writer.WriteLine(Environment.NewLine + "------------------------------------------------------------------------------------------------" + Environment.NewLine);
                                writer.WriteLine("Remove Content :" + ex.Message.ToString() + ex.StackTrace + Environment.NewLine);
                                writer.WriteLine(Environment.NewLine + "------------------------------------------------------------------------------------------------" + Environment.NewLine);
                            }
                        }

                        if (IsDeleted)
                        {
                            //TempData["Message"] = "Content file named '" + content.FileName + "' deleted successfully.";
                            //return RedirectToAction("AddUpdateAsset", new { @assetID = content.AssetId });
                            message = Convert.ToString(content.AssetId);
                        }
                        else
                        {
                            message = "Access";
                        }
                    }
                }
                catch (Exception ex)
                {
                    message = "Exception";//ex.Message.ToString();
                }
            }
            else
            {
                message = "Error";
            }

            return Content(message);
        }

        #region Add Vimeo Url
        [HttpPost]
        public string AddVimeoUrl(string txtAssetID, string txtVimeoUrl)
        {
            string message = string.Empty;
            int Id = Convert.ToInt16(txtAssetID);
            if (txtVimeoUrl != null && Id != 0)
            {
                try
                {
                    using (DB_Entities db = new DB_Entities())
                    {
                        var asset = db.Asset.Where(x => x.AssetId == Id).FirstOrDefault();
                        asset.VimeoUrl = txtVimeoUrl;
                        db.SaveChanges();
                        message = txtVimeoUrl;
                    }
                }
                catch (Exception ex)
                {
                    message = "Exception";
                }
            }
            else
            {
                message = "Error";
            }

            return message;
        }
        #endregion
    }
}