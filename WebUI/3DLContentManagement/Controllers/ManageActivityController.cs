﻿using _3DLContentManagement.Common;
using _3DLContentManagement.Database;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using TDL.ContentPlatform.Server.Database.Tables;

namespace _3DLContentManagement.Controllers
{
    [RoleAuthorize]
    public class ManageActivityController : Controller
    {
        // GET: ManageActivity
        public ActionResult Index()
        {
            return View();
        }

        #region Select
        public ActionResult GetAll([DataSourceRequest] DataSourceRequest request)
        {
            List<MstActivity> listActivities = new List<MstActivity>();
            using (DB_Entities db = new DB_Entities())
            {
                var activities = db.MstActivity.Where(x => x.IsDeleted == false).OrderBy(x => x.ActivityName).ToList();

                listActivities = activities.Select(activity => new MstActivity()
                {
                    ActivityId = activity.ActivityId,
                    ActivityName = activity.ActivityName,
                    IsActive = Convert.ToBoolean(activity.IsActive != null ? activity.IsActive : true),
                    IsDeleted = Convert.ToBoolean(activity.IsDeleted != null ? activity.IsDeleted : false),
                    CreatedBy = activity.CreatedBy != null ? activity.CreatedBy.Value : new Guid(),
                    CreatedOn = activity.CreatedOn != null ? activity.CreatedOn.Value : DateTime.UtcNow
                }).ToList();

            }

            return Json(listActivities.ToDataSourceResult(request));
        }

        #endregion

        #region Add       
        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> AddUpdateActivity([DataSourceRequest] DataSourceRequest request, MstActivity activity)
        {
            if (CheckActivities(activity.ActivityName, activity.ActivityId))
            {
                return this.Json(new DataSourceResult
                {
                    Errors = "ALREADY_EXISTS"
                });
            }

            MstActivity at = new MstActivity();
            if (activity != null)
            {
                at = await AddUpdateActivities(activity);
            }

            return Json(new[] { at }.ToDataSourceResult(request, ModelState));
        }
        #endregion

        [HttpGet]
        public JsonResult CheckActivity(string ActivityName, int ActivityId)
        {
            return Json(CheckActivities(ActivityName, ActivityId), JsonRequestBehavior.AllowGet);

        }

        #region Delete
        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> DeleteActivity([DataSourceRequest] DataSourceRequest request, MstActivity activity)
        {
            MstActivity at = new MstActivity();
            if (activity != null)
            {
                activity.IsDeleted = true;
                at = await AddUpdateActivities(activity);
            }

            return Json(new[] { at }.ToDataSourceResult(request, ModelState));
        }

        #endregion

        public bool CheckActivities(string ActivityName, int ActivityId)
        {
            using (DB_Entities db = new DB_Entities())
            {
                int count = 0;

                if (ActivityId != 0)
                    count = db.MstActivity.Where(p => p.ActivityName == ActivityName.Trim() && p.IsDeleted == false && p.ActivityId != ActivityId).Count();
                else
                    count = db.MstActivity.Where(p => p.ActivityName == ActivityName.Trim() && p.IsDeleted == false).Count();

                return count > 0;
            }
        }

        public async Task<MstActivity> AddUpdateActivities(MstActivity activity)
        {
            string strAction = "Activity updated";
            string strActivity = string.Format("Activity titled '{0}' ", activity.ActivityName);

            using (DB_Entities db = new DB_Entities())
            {
                if (activity.ActivityId == 0)
                {
                    MstActivity newActivityObj = new MstActivity()
                    {
                        ActivityName = activity.ActivityName.Trim(),
                        IsActive = activity.IsActive,
                        CreatedBy = new Guid(Session["UserID"].ToString()),
                        CreatedOn = DateTime.Now,
                        IsDeleted = false

                    };
                    db.MstActivity.Add(newActivityObj);
                    await db.SaveChangesAsync();
                    activity.ActivityId = newActivityObj.ActivityId;
                    strAction = "Activity added";
                    strActivity += string.Format("added; ");
                }
                else
                {

                    var activityObj = db.MstActivity.Where(x => x.ActivityId == activity.ActivityId && x.IsDeleted == false).FirstOrDefault();
                    if (activityObj != null)
                    {
                        if (activityObj.ActivityName != activity.ActivityName)
                        {
                            strActivity += string.Format("updated from '{0}'; ", activityObj.ActivityName);
                        }

                        if (activityObj.IsActive != activity.IsActive)
                        {
                            strActivity += string.Format("updated. Activation status changed from '{0}' to '{1}'; ", activityObj.IsActive, activity.IsActive);
                        }

                        activityObj.ActivityName = activity.ActivityName.Trim();
                        activityObj.IsActive = activity.IsActive;
                        activityObj.ModifiedBy = new Guid(Session["UserID"].ToString());
                        activityObj.ModifiedOn = DateTime.Now;
                        activityObj.IsDeleted = activity.IsDeleted;
                        db.Entry(activityObj).State = EntityState.Modified;
                        await db.SaveChangesAsync();

                        if (activity.IsDeleted == true)
                        {
                            strAction = "Activity deleted";
                            strActivity += string.Format("deleted; ");
                        }
                    }
                }
            }

            await ActivityLogger.AddActivityLog(activity.ActivityId, strAction, strActivity.Substring(0, strActivity.Length - 2));

            return activity;
        }
    }
}