﻿using _3DLContentManagement.App_Start;
using _3DLContentManagement.Common;
using _3DLContentManagement.Database;
using _3DLContentManagement.Models;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using TDL.ContentPlatform.Server.Database.Tables;

namespace _3DLContentManagement.Controllers
{
    [RoleAuthorize]
    public class ManageFeedbackController : Controller
    {
        // GET: ManageFeedback
        public ActionResult Index()
        {
            DB_Entities db = new DB_Entities();

            List<string> st = new List<string>(new string[] { "New", "Archived", "Completed", "WIP" });
            ViewBag.Status = db.MstStatus.Where(x => x.IsDeleted == false && st.Contains(x.StatusName)).Select(x => x.StatusName).ToList();

            return View();
        }

        #region Select
        public ActionResult GetAll([DataSourceRequest] DataSourceRequest request)
        {
            List<FeedbackModel> listFeedbacks = new List<FeedbackModel>();
            using (DB_Entities db = new DB_Entities())
            {
                var accounts = DataAccess.GetUserList();

                listFeedbacks = (from f in db.AssetContentFeedback
                                 join a in accounts on f.UserId equals a.UserID
                                 where f.IsDeleted == false
                                 select new FeedbackModel()
                                 {
                                     FeedbackId = f.Id,
                                     Feedback = f.Feedback,
                                     AssetContentId = f.AssetContentId,
                                     Asset = f.AssetContent.Asset.AssetName,
                                     UserId = f.UserId,
                                     Username = f.CreatedByName,
                                     UserEmail = a.Email,
                                     Account = a.Account,
                                     GradeId = f.GradeId,
                                     Grade = f.Grade.NewName,
                                     SubjectId = f.SubjectId,
                                     Subject = f.Subject.NewName,
                                     TopicId = f.TopicId,
                                     Topic = f.Topic.NewName,
                                     SubtopicId = f.SubtopicId,
                                     Subtopic = f.Subtopic.NewName,
                                     StatusId = f.StatusId,
                                     Status = f.Status.StatusName,
                                     AttachedFile = f.PublishAttachment,
                                     AddedOn = f.CreatedOn.Value.ToString("dd MMMM yyyy hh:mm:ss tt"),
                                     CreatedBy = f.CreatedByName,
                                     CreatedOn = f.CreatedOn
                                 }).ToList();

                //listSubtopics = subtopics.Select(subtopic => new MstSubtopic()
                //{
                //    SubtopicId = subtopic.SubtopicId,
                //    SubtopicName = subtopic.SubtopicName,
                //    PsubtopicId = subtopic.PsubtopicId,
                //    PsubtopicName = subtopic.PsubtopicName,
                //    IsActive = Convert.ToBoolean(subtopic.IsActive != null ? subtopic.IsActive : true),
                //    IsDeleted = Convert.ToBoolean(subtopic.IsDeleted != null ? subtopic.IsDeleted : false),
                //    CreatedBy = subtopic.CreatedBy != null ? subtopic.CreatedBy.Value : new Guid(),
                //    CreatedOn = subtopic.CreatedOn != null ? subtopic.CreatedOn.Value : DateTime.UtcNow
                //}).ToList();

                List<string> st = new List<string>(new string[] { "New", "Archived", "Completed", "WIP" });
                ViewBag.Status = db.MstStatus.Where(x => x.IsDeleted == false && st.Contains(x.StatusName)).Select(x => x.StatusName).ToList();
            }

            return Json(listFeedbacks.ToDataSourceResult(request));
        }

        #endregion

        #region Add       
        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> AddUpdateFeedback([DataSourceRequest] DataSourceRequest request, FeedbackModel model)
        {
            if (!CheckStatus(model.Status))
            {
                return this.Json(new DataSourceResult
                {
                    Errors = "NOT_EXISTS"
                });
            }

            FeedbackModel fb = new FeedbackModel();
            if (model != null)
            {
                fb = await AddUpdateFeedbacks(model);
            }

            return Json(new[] { fb }.ToDataSourceResult(request, ModelState));
        }
        #endregion

        public bool CheckStatus(string StatusName)
        {
            using (DB_Entities db = new DB_Entities())
            {
                int count = 0;

                if (!string.IsNullOrEmpty(StatusName))
                    count = db.MstStatus.Where(p => p.StatusName == StatusName.Trim() && p.IsDeleted == false).Count();

                return count > 0;
            }
        }

        public async Task<FeedbackModel> AddUpdateFeedbacks(FeedbackModel fb)
        {
            string strAction = "Feedback status updated";
            string strActivity = string.Format("Feedback Id titled '{0}' ", fb.FeedbackId);

            using (DB_Entities db = new DB_Entities())
            {
                //Check parent subtopic value if exists in master
                if (fb.Status != null && fb.Status != string.Empty)
                {
                    fb.StatusId = db.MstStatus.Where(s => s.StatusName.Equals(fb.Status)).Select(s => s.StatusId).FirstOrDefault();
                }

                if (fb.FeedbackId == 0)
                {
                    strAction = "Feedback added";
                    strActivity += string.Format("added; ");
                }
                else
                {
                    var feedbackObj = db.AssetContentFeedback.Where(x => x.Id == fb.FeedbackId && x.IsDeleted == false).FirstOrDefault();
                    if (feedbackObj != null)
                    {
                        if (feedbackObj.StatusId != fb.StatusId)
                        {
                            strActivity += string.Format("Feedback status changed from '{0}' to '{1}'; ", feedbackObj.Status, fb.Status);
                        }

                        feedbackObj.StatusId = fb.StatusId;
                        //feedbackObj.IsActive = subtopic.IsActive;
                        feedbackObj.ModifiedBy = new Guid(Session["UserID"].ToString());
                        feedbackObj.ModifiedOn = DateTime.Now;
                        //feedbackObj.IsDeleted = subtopic.IsDeleted;
                        db.Entry(feedbackObj).State = EntityState.Modified;
                        db.SaveChanges();

                        //if (feedback.IsDeleted == true)
                        //{
                        //    strAction = "Subtopic deleted";
                        //    strActivity += string.Format("deleted; ");
                        //}
                    }
                }
            }

            await ActivityLogger.AddActivityLog(fb.FeedbackId, strAction, strActivity.Substring(0, strActivity.Length - 2));

            return fb;
        }


        public ActionResult RefreshStatus()
        {
            using (DB_Entities db = new DB_Entities())
            {
                List<string> st = new List<string>(new string[] { "New", "Archived", "Completed", "WIP" });
                var result = db.MstStatus.Where(x => x.IsDeleted == false && st.Contains(x.StatusName)).Select(x => x.StatusName).ToList();

                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        public string GetThumbnail(string Id)
        {
            var newId = Guid.Parse(Id);
            using (DB_Entities db = new DB_Entities())
            {
                var result = db.EntityThumbnail.Where(x => x.EntityId == newId && x.IsDeleted == false).FirstOrDefault();

                return result?.Thumbnail;
            }
        }

        [HttpPost]
        public ActionResult Pdf_Export_Save(string contentType, string base64, string fileName)
        {
            var fileContents = Convert.FromBase64String(base64);

            return File(fileContents, contentType, fileName);
        }
    }
}