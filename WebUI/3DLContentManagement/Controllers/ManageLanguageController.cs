﻿using _3DLContentManagement.App_Start;
using _3DLContentManagement.Common;
using _3DLContentManagement.Database;
using _3DLContentManagement.Models;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using TDL.ContentPlatform.Server.Database.Tables;

namespace _3DLContentManagement.Controllers
{
    [RoleAuthorize]
    public class ManageLanguageController : Controller
    {
        // GET: ManageLanguage
        public ActionResult Index()
        {
            return View();
        }

        #region Select
        public ActionResult GetAll([DataSourceRequest] DataSourceRequest request)
        {
            List<CsNewLanguage> listLanguages = new List<CsNewLanguage>();
            using (DB_Entities db = new DB_Entities())
            {
                var languages = db.CsNewLanguage.OrderBy(x => x.NewName).ToList();

                listLanguages = languages.Select(language => new CsNewLanguage()
                {
                    Id = language.Id,
                    NewName = language.NewName,
                    Createdbyyominame = language.Createdbyyominame,
                    Createdon = language.Createdon != null ? language.Createdon.Value : DateTime.UtcNow
                }).ToList();

            }

            return Json(listLanguages.ToDataSourceResult(request));
        }

        #endregion

        #region Add       
        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> AddUpdateLanguage([DataSourceRequest] DataSourceRequest request, MstLanguage language)
        {
            if (CheckLanguages(language.LanguageName, language.LanguageId))
            {
                return this.Json(new DataSourceResult
                {
                    Errors = "ALREADY_EXISTS"
                });
            }

            MstLanguage at = new MstLanguage();
            if (language != null)
            {
                at = await AddUpdateLanguage(language);
            }

            return Json(new[] { at }.ToDataSourceResult(request, ModelState));
        }
        #endregion

        [HttpGet]
        public JsonResult CheckLanguage(string LanguageName, int Id)
        {
            return Json(CheckLanguages(LanguageName, Id), JsonRequestBehavior.AllowGet);

        }

        #region Delete
        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> DeleteLanguage([DataSourceRequest] DataSourceRequest request, MstLanguage language)
        {
            MstLanguage at = new MstLanguage();
            if (language != null)
            {
                language.IsDeleted = true;
                at = await AddUpdateLanguage(language);
            }

            return Json(new[] { at }.ToDataSourceResult(request, ModelState));
        }

        #endregion

        public bool CheckLanguages(string LanguageName, int Id)
        {
            using (DB_Entities db = new DB_Entities())
            {
                int count = 0;

                if (Id != 0)
                    count = db.MstLanguage.Where(p => p.LanguageName == LanguageName.Trim() && p.IsDeleted == false && p.LanguageId != Id).Count();
                else
                    count = db.MstLanguage.Where(p => p.LanguageName == LanguageName.Trim() && p.IsDeleted == false).Count();

                return count > 0;
            }
        }

        public async Task<MstLanguage> AddUpdateLanguage(MstLanguage language)
        {
            string strAction = "Language updated";
            string strActivity = string.Format("Language titled '{0}' ", language.LanguageName);

            using (DB_Entities db = new DB_Entities())
            {
                if (language.LanguageId == 0)
                {
                    MstLanguage newLanguageObj = new MstLanguage()
                    {
                        LanguageName = language.LanguageName.Trim(),
                        IsActive = language.IsActive,
                        CreatedBy = new Guid(Session["UserID"].ToString()),
                        CreatedOn = DateTime.Now,
                        IsDeleted = false

                    };
                    db.MstLanguage.Add(newLanguageObj);
                    await db.SaveChangesAsync();
                    language.LanguageId = newLanguageObj.LanguageId;
                    strAction = "Language added";
                    strActivity += string.Format("added; ");
                }
                else
                {

                    var languageObj = db.MstLanguage.Where(x => x.LanguageId == language.LanguageId && x.IsDeleted == false).FirstOrDefault();
                    if (languageObj != null)
                    {
                        if (languageObj.LanguageName != language.LanguageName)
                        {
                            strActivity += string.Format("updated from '{0}'; ", languageObj.LanguageName);
                        }

                        if (languageObj.IsActive != language.IsActive)
                        {
                            strActivity += string.Format("updated. Activation status changed from '{0}' to '{1}'; ", languageObj.IsActive, language.IsActive);
                        }

                        languageObj.LanguageName = language.LanguageName.Trim();
                        languageObj.IsActive = language.IsActive;
                        languageObj.ModifiedBy = new Guid(Session["UserID"].ToString());
                        languageObj.ModifiedOn = DateTime.Now;
                        languageObj.IsDeleted = language.IsDeleted;
                        db.Entry(languageObj).State = EntityState.Modified;
                        await db.SaveChangesAsync();                        

                        if (language.IsDeleted == true)
                        {
                            strAction = "Language deleted";
                            strActivity += string.Format("deleted; ", language.LanguageName);
                        }
                    }
                }
            }

            await ActivityLogger.AddActivityLog(language.LanguageId, strAction, strActivity.Substring(0, strActivity.Length - 2));

            return language;
        }
    }
}