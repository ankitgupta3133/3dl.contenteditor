﻿using _3DLContentManagement.App_Start;
using _3DLContentManagement.Common;
using _3DLContentManagement.Database;
using _3DLContentManagement.Models;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using TDL.ContentPlatform.Server.Database.Tables;

namespace _3DLContentManagement.Controllers
{
    [RoleAuthorize]
    public class ManageLevelController : Controller
    {
        // GET: ManageUsers
        public ActionResult Index()
        {
            return View();
        }

        #region Select
        public ActionResult GetAllLevels([DataSourceRequest] DataSourceRequest request)
        {
            List<CsNewGrade> listGrades = new List<CsNewGrade>();
            using (DB_Entities db = new DB_Entities())
            {
                var grades = db.CsNewGrade.OrderBy(x => x.NewName).ToList();

                listGrades = grades.Select(grade => new CsNewGrade()
                {
                    Id = grade.Id,
                    NewName = grade.NewName,
                    Createdbyyominame = grade.Createdbyyominame,
                    Createdon = grade.Createdon != null ? grade.Createdon.Value : DateTime.UtcNow
                }).ToList();

            }

            return Json(listGrades.ToDataSourceResult(request));
        }

        #endregion

        #region Add       
        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> AddUpdateLevels([DataSourceRequest] DataSourceRequest request, MstLevel level)
        {
            if (CheckLevel(level.LevelName, level.LevelId))
            {
                return this.Json(new DataSourceResult
                {
                    Errors = "ALREADY_EXISTS"
                });
            }

            MstLevel at = new MstLevel();
            if (level != null)
            {
                at = await AddUpdateLevel(level);
            }

            return Json(new[] { at }.ToDataSourceResult(request, ModelState));
        }
        #endregion

        #region Delete
        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> DeleteLevel([DataSourceRequest] DataSourceRequest request, MstLevel level)
        {
            MstLevel at = new MstLevel();
            if (level != null)
            {
                level.IsDeleted = true;
                at = await AddUpdateLevel(level);
            }

            return Json(new[] { at }.ToDataSourceResult(request, ModelState));
        }

        #endregion

        public bool CheckLevel(string LevelName, int Id)
        {
            using (DB_Entities db = new DB_Entities())
            {
                int count = 0;

                if (Id != 0)
                    count = db.MstLevel.Where(p => p.LevelName == LevelName.Trim() && p.IsDeleted == false && p.LevelId != Id).Count();
                else
                    count = db.MstLevel.Where(p => p.LevelName == LevelName.Trim() && p.IsDeleted == false).Count();

                return count > 0;
            }
        }

        public async Task<MstLevel> AddUpdateLevel(MstLevel level)
        {
            string strAction = "Level details updated";
            string strActivity = string.Format("Level named '{0}' ", level.LevelName);
            using (DB_Entities db = new DB_Entities())
            {
                if (level.LevelId == 0)
                {
                    MstLevel newLevelObj = new MstLevel()
                    {
                        LevelName = level.LevelName.Trim(),
                        IsActive = level.IsActive,
                        CreatedBy = new Guid(Session["UserID"].ToString()),
                        CreatedOn = DateTime.Now,
                        IsDeleted = false
                    };
                    db.MstLevel.Add(newLevelObj);
                    await db.SaveChangesAsync();

                    level.LevelId = newLevelObj.LevelId;
                    strAction = "Level added";
                    strActivity += string.Format("added; ");
                }
                else
                {
                    strActivity = string.Format("Level details updated for named '{0}'. ", level.LevelName);
                    var levelObj = db.MstLevel.Where(x => x.LevelId == level.LevelId && x.IsDeleted == false).FirstOrDefault();
                    if (levelObj != null)
                    {
                        if (levelObj.LevelName != level.LevelName)
                        {
                            strActivity += string.Format("Level name changed from '{0}' to '{1}'; ", levelObj.LevelName, level.LevelName);
                        }

                        if (levelObj.IsActive != level.IsActive)
                        {
                            strActivity += string.Format("updated. Activation status changed from '{0}' to '{1}'; ", levelObj.IsActive, level.IsActive);
                        }

                        levelObj.LevelName = level.LevelName.Trim();
                        levelObj.IsActive = level.IsActive;
                        levelObj.ModifiedBy = new Guid(Session["UserID"].ToString());
                        levelObj.ModifiedOn = DateTime.Now;
                        levelObj.IsDeleted = level.IsDeleted;
                        db.Entry(levelObj).State = EntityState.Modified;
                        await db.SaveChangesAsync();

                        if (level.IsDeleted == true)
                        {
                            strAction = "Level deleted";
                            strActivity += string.Format("deleted; ");
                        }
                    }
                }
            }

            await ActivityLogger.AddActivityLog(0, strAction, strActivity.Substring(0, strActivity.Length - 2));

            return level;
        }

        public string GetThumbnail(string Id)
        {
            var newId = Guid.Parse(Id);
            using (DB_Entities db = new DB_Entities())
            {
                var result = db.EntityThumbnail.Where(x => x.EntityId == newId && x.IsDeleted == false).FirstOrDefault();

                return result?.Thumbnail;
            }
        }

        #region Upload thumbnail image
        [HttpPost]
        public string UploadImage(HttpPostedFileBase UploadFile, string txtID)
        {
            string message = string.Empty;

            if (UploadFile != null && UploadFile.ContentLength > 0)
            {
                try
                {
                    string path = string.Empty;
                    path = System.IO.Path.Combine(WebConfigurationManager.AppSettings["PublishPath"].ToString(), "thumbnails");

                    //Check for the target path if it is exist
                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }

                    path = Path.Combine(path, Path.GetFileName(UploadFile.FileName));

                    UploadFile.SaveAs(path);
                    message = UploadFile.FileName;
                    var newId = Guid.Parse(txtID);
                    using (DB_Entities db = new DB_Entities())
                    {
                        var asset = db.EntityThumbnail.Where(x => x.EntityId == newId).FirstOrDefault();
                        if (asset != null)
                        {
                            asset.Thumbnail = Path.GetFileName(UploadFile.FileName);
                            asset.ModifiedBy = new Guid(Session["UserID"].ToString());
                            asset.ModifiedOn = DateTime.Now;
                            db.SaveChanges();
                        }
                        else
                        {
                            EntityThumbnail newContent = new EntityThumbnail()
                            {
                                EntityId = newId,
                                Thumbnail = Path.GetFileName(UploadFile.FileName),
                                CreatedBy = new Guid(Session["UserID"].ToString()),
                                CreatedOn = DateTime.Now,
                                IsDeleted = false
                            };

                            db.EntityThumbnail.Add(newContent);
                            db.SaveChanges();
                        }
                    }
                }
                catch (Exception)
                {
                    message = "Exception";
                }
            }
            else
            {
                message = "Error";
            }

            return message;
        }
        #endregion
    }
}