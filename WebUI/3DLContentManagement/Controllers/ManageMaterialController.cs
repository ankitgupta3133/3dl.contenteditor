﻿using _3DLContentManagement.Common;
using _3DLContentManagement.Database;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using TDL.ContentPlatform.Server.Database.Tables;

namespace _3DLContentManagement.Controllers
{
    [RoleAuthorize]
    public class ManageMaterialController : Controller
    {
        // GET: ManageMaterial
        public ActionResult Index()
        {
            return View();
        }

        #region Select
        public ActionResult GetAll([DataSourceRequest] DataSourceRequest request)
        {
            List<MstMaterial> listMaterial = new List<MstMaterial>();
            using (DB_Entities db = new DB_Entities())
            {
                var materials = db.MstMaterial.Where(x => x.IsDeleted == false).OrderBy(x => x.MaterialName).ToList();

                listMaterial = materials.Select(material => new MstMaterial()
                {
                    MaterialId = material.MaterialId,
                    MaterialName = material.MaterialName,
                    IsActive = Convert.ToBoolean(material.IsActive != null ? material.IsActive : true),
                    IsDeleted = Convert.ToBoolean(material.IsDeleted != null ? material.IsDeleted : false),
                    CreatedBy = material.CreatedBy != null ? material.CreatedBy.Value : new Guid(),
                    CreatedOn = material.CreatedOn != null ? material.CreatedOn.Value : DateTime.UtcNow
                }).ToList();

            }

            return Json(listMaterial.ToDataSourceResult(request));
        }

        #endregion

        #region Add       
        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> AddUpdateMaterial([DataSourceRequest] DataSourceRequest request, MstMaterial material)
        {
            if (CheckMaterial(material.MaterialName, material.MaterialId))
            {
                return this.Json(new DataSourceResult
                {
                    Errors = "ALREADY_EXISTS"
                });
            }

            MstMaterial objMaterial = new MstMaterial();
            if (material != null)
            {
                objMaterial = await AddUpdateMaterialDetails(material);
            }

            return Json(new[] { objMaterial }.ToDataSourceResult(request, ModelState));
        }
        #endregion

        #region Delete
        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> DeleteMaterial([DataSourceRequest] DataSourceRequest request, MstMaterial material)
        {
            MstMaterial objMaterial = new MstMaterial();
            if (material != null)
            {
                material.IsDeleted = true;
                objMaterial = await AddUpdateMaterialDetails(material);
            }

            return Json(new[] { objMaterial }.ToDataSourceResult(request, ModelState));
        }

        #endregion

        public bool CheckMaterial(string MaterialName, int Id)
        {
            using (DB_Entities db = new DB_Entities())
            {
                int count = 0;

                if (Id != 0)
                    count = db.MstMaterial.Where(p => p.MaterialName == MaterialName.Trim() && p.IsDeleted == false && p.MaterialId != Id).Count();
                else
                    count = db.MstMaterial.Where(p => p.MaterialName == MaterialName.Trim() && p.IsDeleted == false).Count();

                return count > 0;
            }
        }

        public async Task<MstMaterial> AddUpdateMaterialDetails(MstMaterial material)
        {
            string strAction = "Material updated";
            string strActivity = string.Format("Material titled '{0}' ", material.MaterialName);

            using (DB_Entities db = new DB_Entities())
            {
                if (material.MaterialId == 0)
                {
                    MstMaterial newMaterialObj = new MstMaterial()
                    {
                        MaterialName = material.MaterialName.Trim(),
                        IsActive = material.IsActive,
                        CreatedBy = new Guid(Session["UserID"].ToString()),
                        CreatedOn = DateTime.Now,
                        IsDeleted = false

                    };
                    db.MstMaterial.Add(newMaterialObj);
                    await db.SaveChangesAsync();
                    material.MaterialId = newMaterialObj.MaterialId;
                    strAction = "Material added";
                    strActivity += string.Format("added; ");
                }
                else
                {
                    var materialObj = db.MstMaterial.Where(x => x.MaterialId == material.MaterialId && x.IsDeleted == false).FirstOrDefault();
                    if (materialObj != null)
                    {
                        if (materialObj.MaterialName != material.MaterialName)
                        {
                            strActivity += string.Format("updated from '{0}'; ", materialObj.MaterialName);
                        }

                        if (materialObj.IsActive != material.IsActive)
                        {
                            strActivity += string.Format("updated. Activation status changed from '{0}' to '{1}'; ", materialObj.IsActive, material.IsActive);
                        }

                        materialObj.MaterialName = material.MaterialName.Trim();
                        materialObj.IsActive = material.IsActive;
                        materialObj.ModifiedBy = new Guid(Session["UserID"].ToString());
                        materialObj.ModifiedOn = DateTime.Now;
                        materialObj.IsDeleted = material.IsDeleted;
                        db.Entry(materialObj).State = EntityState.Modified;
                        await db.SaveChangesAsync();

                        if (material.IsDeleted == true)
                        {
                            strAction = "Material deleted";
                            strActivity += string.Format("deleted; ");
                        }
                    }
                }
            }

            await ActivityLogger.AddActivityLog(material.MaterialId, strAction, strActivity.Substring(0, strActivity.Length - 2));

            return material;
        }
    }
}