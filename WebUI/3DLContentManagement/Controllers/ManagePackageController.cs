﻿using _3DLContentManagement.Common;
using _3DLContentManagement.Database;
using _3DLContentManagement.Models;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using RT.Comb;
using TDL.ContentPlatform.Server.Database;
using TDL.ContentPlatform.Server.Database.Tables;

namespace _3DLContentManagement.Controllers
{
    [RoleAuthorize]
    public class ManagePackageController : Controller
    {
        // GET: ManagePackage
        public ActionResult Index()
        {
            return View();
        }
        #region Select
        public ActionResult GetAll([DataSourceRequest] DataSourceRequest request)
        {
            List<CsNewPackage> listPackage = new List<CsNewPackage>();
            using (DB_Entities db = new DB_Entities())
            {
                var packages = db.CsNewPackage.OrderBy(x => x.NewName).ToList();

                listPackage = packages.Select(package => new CsNewPackage()
                {
                    Id = package.Id,
                    NewName = package.NewName,
                    Createdbyyominame = package.Createdbyyominame,
                    Createdon = package.Createdon != null ? package.Createdon.Value : DateTime.UtcNow
                }).ToList();

            }

            return Json(listPackage.ToDataSourceResult(request));
        }

        #endregion

        #region Add       
        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> AddUpdatePackage([DataSourceRequest] DataSourceRequest request, CsNewPackage package)
        {
            if (CheckPackage(package.NewName, package.Id))
            {
                return this.Json(new DataSourceResult
                {
                    Errors = "ALREADY_EXISTS"
                });
            }

            CsNewPackage objPackage = new CsNewPackage();
            if (package != null)
            {
                objPackage = await AddUpdatePackageDetails(package);
            }

            return Json(new[] { objPackage }.ToDataSourceResult(request, ModelState));
        }
        #endregion

        #region Delete
        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> DeletePackage([DataSourceRequest] DataSourceRequest request, CsNewPackage package)
        {
            CsNewPackage objPackage = new CsNewPackage();
            if (package != null)
            {
                //package.IsDeleted = true;
                objPackage = await AddUpdatePackageDetails(package);
            }

            return Json(new[] { objPackage }.ToDataSourceResult(request, ModelState));
        }

        #endregion

        public bool CheckPackage(string Name, Guid Id)
        {
            using (DB_Entities db = new DB_Entities())
            {
                int count = 0;

                if (Id != Guid.Empty)
                    count = db.CsNewPackage.Where(p => p.NewName == Name.Trim() && p.Id != Id).Count();
                else
                    count = db.CsNewPackage.Where(p => p.NewName == Name.Trim()).Count();

                return count > 0;
            };
        }

        public async Task<CsNewPackage> AddUpdatePackageDetails(CsNewPackage package)
        {
            string strAction = "Package updated";
            string strActivity = string.Format("Package titled '{0}' ", package.NewName);

            using (ContentDbContext db = new ContentDbContext())
            {
                if (package.Id == Guid.Empty)
                {
                    CsNewPackage newPackageObj = new CsNewPackage()
                    {
                        NewName = package.NewName.Trim(),
                        Id = Provider.Sql.Create(),
                        Createdby = new Guid(Session["UserID"].ToString()),
                        Createdon = DateTime.Now
                    };
                    db.CsNewPackage.Add(newPackageObj);
                    await db.SaveChangesAsync();
                    package.Id = newPackageObj.Id;
                    strAction = "Package added";
                    strActivity += string.Format("added; ");
                }
                else
                {
                    var packageObj = db.CsNewPackage.Where(x => x.Id == package.Id).FirstOrDefault();
                    if (packageObj != null)
                    {
                        if (packageObj.NewName != package.NewName)
                        {
                            strActivity += string.Format("updated from '{0}'; ", packageObj.NewName);
                        }

                        //if (packageObj.IsEnabled != package.IsEnabled)
                        //{
                        //    strActivity += string.Format("updated. Activation status changed from '{0}' to '{1}'; ", packageObj.IsEnabled, package.IsEnabled);
                        //}

                        packageObj.NewName = package.NewName.Trim();
                        packageObj.Modifiedby = new Guid(Session["UserID"].ToString());
                        packageObj.Modifiedon = DateTime.Now;
                        db.Entry(packageObj).State = EntityState.Modified;
                        await db.SaveChangesAsync();

                        //if (package.IsDeleted == true)
                        //{
                        //    strAction = "package deleted";
                        //    strActivity += string.Format("deleted; ");
                        //}
                    }
                }
            }

            //await ActivityLogger.AddActivityLog(package.ID, strAction, strActivity.Substring(0, strActivity.Length - 2));

            return package;
        }
    }
}