﻿using _3DLContentManagement.App_Start;
using _3DLContentManagement.Common;
using _3DLContentManagement.Database;
using _3DLContentManagement.Models;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using TDL.ContentPlatform.Server.Database.Tables;

namespace _3DLContentManagement.Controllers
{
    [RoleAuthorize]
    public class ManagePropertyController : Controller
    {
        // GET: ManageProperty
        public ActionResult Index()
        {
            return View();
        }

        #region Select
        public ActionResult GetAll([DataSourceRequest] DataSourceRequest request)
        {
            List<MstProperty> listProperties = new List<MstProperty>();
            using (DB_Entities db = new DB_Entities())
            {
                var propertys = db.MstProperty.Where(x => x.IsDeleted == false).OrderBy(x => x.PropertyName).ToList();

                listProperties = propertys.Select(property => new MstProperty()
                {
                    PropertyId = property.PropertyId,
                    PropertyName = property.PropertyName,
                    IsActive = Convert.ToBoolean(property.IsActive != null ? property.IsActive : true),
                    IsDeleted = Convert.ToBoolean(property.IsDeleted != null ? property.IsDeleted : false),
                    CreatedBy = property.CreatedBy != null ? property.CreatedBy.Value : new Guid(),
                    CreatedOn = property.CreatedOn != null ? property.CreatedOn.Value : DateTime.UtcNow
                }).ToList();

            }

            return Json(listProperties.ToDataSourceResult(request));
        }

        [HttpGet]
        public JsonResult GetProperties(string q, string selRec)
        {
            q = q.Replace("'", "''");
            List<int> exclude = new List<int>();
            if (!string.IsNullOrWhiteSpace(selRec) && selRec != "null")
            {
                if (selRec.IndexOf(",") > 0)
                {
                    foreach (string s in selRec.Split(','))
                    {
                        exclude.Add(Convert.ToInt32(s));
                    }
                }
                else
                {
                    exclude.Add(Convert.ToInt32(selRec));
                }
            }


            using (DB_Entities db = new DB_Entities())
            {
                var propertys = (from T in db.MstProperty
                            where T.IsDeleted == false && !exclude.Contains(T.PropertyId) && (q == "" || T.PropertyName.Contains(q))
                            select new PropertyNamesModel
                            {
                                PropertyId = T.PropertyId,
                                Name = T.PropertyName
                            }).OrderBy(X => X.Name).ToList();
                return Json(new
                {
                    results = propertys.Select(x => new
                    {
                        id = x.PropertyId,
                        text = x.Name
                    }),
                    more = false
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult AddUpdateProperty(int propertyId)
        {
            PropertyViewModel propertyViewModel = new PropertyViewModel
            {
                PropertyId = propertyId,
                IsActive = true,
                IsDeleted = false
            };
            if (propertyId == 0)
            {
                PropertyNamesModel newPropertyName = new PropertyNamesModel() { PropertyId = 0, Name = string.Empty };
                propertyViewModel.PropertyNames = new List<PropertyNamesModel>();
                propertyViewModel.PropertyNames.Add(newPropertyName);

                using (DB_Entities db = new DB_Entities())
                {
                    ViewBag.Language = db.CsNewLanguage.Select(x => new SelectListItem { Text = x.NewName, Value = x.NewLanguageid.ToString() }).OrderBy(x => x.Text).ToList();
                }
            }
            else
            {
                using (DB_Entities db = new DB_Entities())
                {
                    var property = db.MstProperty.Where(a => a.PropertyId == propertyId).FirstOrDefault();
                    propertyViewModel.IsActive = property.IsActive;

                    propertyViewModel.PropertyNames = (from T in db.MstPropertyNames
                                             join L in db.CsNewLanguage on T.LanguageId equals L.NewLanguageid into TL
                                             from D in TL.DefaultIfEmpty()
                                             where T.PropertyId == propertyId && T.IsDeleted == false
                                             select new PropertyNamesModel
                                             {
                                                 PropertyId = T.PropertyId,
                                                 IsDeleted = T.IsDeleted,
                                                 LanguageId = T.LanguageId,
                                                 Language = D.NewName,
                                                 Id = T.Id,
                                                 Name = T.Name
                                             }).ToList();

                    ViewBag.Language = db.CsNewLanguage.Select(x => new SelectListItem { Text = x.NewName, Value = x.NewLanguageid.ToString() }).OrderBy(x => x.Text).ToList();
                }
            }

            return PartialView("_AddUpdateProperty", propertyViewModel);
        }

        #endregion

        #region Add       
        [HttpPost]
        public async Task<JsonResult> AddUpdateProperty(MstProperty property)
        {
            if (property.PropertyNames != null && property.PropertyNames.Count > 0)
            {
                if (CheckProperties(property.PropertyNames.ElementAt(0).Name, property.PropertyId))
                {
                    return this.Json(new DataSourceResult
                    {
                        Errors = "ALREADY_EXISTS"
                    });
                }

                MstProperty at = new MstProperty();
                if (property != null)
                {
                    at = await AddUpdatePropertyDB(property);
                }

                if (at.PropertyId == 0)
                {
                    return Json(new
                    {
                        isSuccess = false,
                        message = "Error adding property"
                    });
                }
                else
                {
                    return Json(new
                    {
                        isSuccess = true,
                        message = property.PropertyId == 0 ? "Property added successfully." : "Property updated successfully."
                    });
                }
            }
            else
            {
                return Json(new
                {
                    isSuccess = false,
                    message = "Please add atleast one property"
                });
            }
        }
        #endregion

        [HttpGet]
        public JsonResult CheckProperty(string PropertyName, int Id)
        {
            return Json(CheckProperties(PropertyName, Id), JsonRequestBehavior.AllowGet);

        }

        #region Delete
        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> DeleteProperty([DataSourceRequest] DataSourceRequest request, MstProperty property)
        {
            MstProperty at = new MstProperty();
            if (property != null)
            {
                property.IsDeleted = true;
                at = await AddUpdatePropertyDB(property);
            }

            return Json(new[] { at }.ToDataSourceResult(request, ModelState));
        }

        #endregion

        public bool CheckProperties(string PropertyName, int Id)
        {
            using (DB_Entities db = new DB_Entities())
            {
                int count = 0;

                if (Id != 0)
                    count = db.MstProperty.Where(p => p.PropertyName == PropertyName.Trim() && p.IsDeleted == false && p.PropertyId != Id).Count();
                else
                    count = db.MstProperty.Where(p => p.PropertyName == PropertyName.Trim() && p.IsDeleted == false).Count();

                return count > 0;
            }
        }

        public async Task<MstProperty> AddUpdatePropertyDB(MstProperty property)
        {
            string strAction = "Property updated";
            string strActivity = string.Format("Property titled '{0}' ", property.PropertyName);

            using (DB_Entities db = new DB_Entities())
            {
                if (property.PropertyId == 0)
                {
                    for (int i = 0; i < property.PropertyNames.Count; i++)
                    {
                        if (i == 0)
                        {
                            MstProperty newPropertyObj = new MstProperty()
                            {
                                PropertyName = property.PropertyNames.ElementAt(i).Name.Trim(),
                                IsActive = property.IsActive,
                                CreatedBy = new Guid(Session["UserID"].ToString()),
                                CreatedOn = DateTime.Now,
                                IsDeleted = false

                            };
                            db.MstProperty.Add(newPropertyObj);
                            await db.SaveChangesAsync();
                            property.PropertyId = newPropertyObj.PropertyId;
                        }

                        //Property Name
                        MstPropertyNames newMstPropertyName = new MstPropertyNames()
                        {
                            IsDeleted = false,
                            Name = property.PropertyNames.ElementAt(i).Name,
                            LanguageId = property.PropertyNames.ElementAt(i).LanguageId,
                            PropertyId = property.PropertyId
                        };
                        db.MstPropertyNames.Add(newMstPropertyName);
                    }

                    await db.SaveChangesAsync();
                    strAction = "Property added";
                    strActivity += string.Format("added; ");
                }
                else
                {

                    var propertyObj = db.MstProperty.Where(x => x.PropertyId == property.PropertyId && x.IsDeleted == false).FirstOrDefault();
                    if (propertyObj != null)
                    {
                        if (property.IsDeleted == true || property.PropertyNames.Where(x => x.IsDeleted == false).Count() == 0)
                        {
                            propertyObj.IsDeleted = true;
                            strAction = "Property deleted";
                            strActivity += string.Format("deleted; ");
                        }
                        else
                        {

                            for (int i = 0; i < property.PropertyNames.Count; i++)
                            {
                                var propertyNameObj = await db.MstPropertyNames.Where(a => a.Id == property.PropertyNames.ElementAt(i).Id).FirstOrDefaultAsync();
                                if (propertyNameObj != null)
                                {
                                    propertyNameObj.IsDeleted = property.PropertyNames.ElementAt(i).IsDeleted;
                                    propertyNameObj.Name = property.PropertyNames.ElementAt(i).Name;
                                    propertyNameObj.LanguageId = property.PropertyNames.ElementAt(i).LanguageId;
                                }
                                else
                                {
                                    MstPropertyNames newMstPropertyName = new MstPropertyNames()
                                    {
                                        IsDeleted = false,
                                        Name = property.PropertyNames.ElementAt(i).Name,
                                        LanguageId = property.PropertyNames.ElementAt(i).LanguageId,
                                        PropertyId = property.PropertyId
                                    };
                                    db.MstPropertyNames.Add(newMstPropertyName);
                                }
                            }

                            propertyObj.PropertyName = property.PropertyNames.Where(x => x.IsDeleted == false).FirstOrDefault().Name.Trim();
                            propertyObj.IsActive = property.IsActive;
                            propertyObj.ModifiedBy = new Guid(Session["UserID"].ToString());
                            propertyObj.ModifiedOn = DateTime.Now;                            
                            db.Entry(propertyObj).State = EntityState.Modified;

                        }
                        await db.SaveChangesAsync();
                        //if (property.IsDeleted == true)
                        //{
                        //    strAction = "Property deleted";
                        //    strActivity += string.Format("deleted; ");
                        //}
                    }
                }
            }

            await ActivityLogger.AddActivityLog(property.PropertyId, strAction, strActivity.Substring(0, strActivity.Length - 2));

            return property;
        }
    }
}