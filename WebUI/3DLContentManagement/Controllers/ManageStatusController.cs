﻿using _3DLContentManagement.App_Start;
using _3DLContentManagement.Common;
using _3DLContentManagement.Database;
using _3DLContentManagement.Models;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using TDL.ContentPlatform.Server.Database.Tables;

namespace _3DLContentManagement.Controllers
{
    [RoleAuthorize]
    public class ManageStatusController : Controller
    {
        // GET: ManageStatus
        public ActionResult Index()
        {
            return View();
        }

        #region Select
        public ActionResult GetAll([DataSourceRequest] DataSourceRequest request)
        {
            List<MstStatus> listStatus = new List<MstStatus>();
            using (DB_Entities db = new DB_Entities())
            {
                var status = db.MstStatus.Where(x => x.IsDeleted == false).OrderBy(x => x.StatusName).ToList();

                listStatus = status.Select(statu => new MstStatus()
                {
                    StatusId = statu.StatusId,
                    StatusName = statu.StatusName,
                    IsActive = Convert.ToBoolean(statu.IsActive != null ? statu.IsActive : true),
                    IsDeleted = Convert.ToBoolean(statu.IsDeleted != null ? statu.IsDeleted : false),
                    CreatedBy = statu.CreatedBy != null ? statu.CreatedBy.Value : new Guid(),
                    CreatedOn = statu.CreatedOn != null ? statu.CreatedOn.Value : DateTime.UtcNow
                }).ToList();

            }

            return Json(listStatus.ToDataSourceResult(request));
        }

        #endregion

        #region Add       
        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> AddUpdateStatus([DataSourceRequest] DataSourceRequest request, MstStatus statu)
        {
            if (CheckStatus(statu.StatusName, statu.StatusId))
            {
                return this.Json(new DataSourceResult
                {
                    Errors = "ALREADY_EXISTS"
                });
            }

            MstStatus at = new MstStatus();
            if (statu != null)
            {
                at = await AddUpdateStatusType(statu);
            }

            return Json(new[] { at }.ToDataSourceResult(request, ModelState));
        }
        #endregion

        [HttpGet]
        public JsonResult CheckStatu(string StatusName, int Id)
        {
            return Json(CheckStatus(StatusName, Id), JsonRequestBehavior.AllowGet);

        }

        #region Delete
        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> DeleteStatus([DataSourceRequest] DataSourceRequest request, MstStatus statu)
        {
            MstStatus at = new MstStatus();
            if (statu != null)
            {
                statu.IsDeleted = true;
                at = await AddUpdateStatusType(statu);
            }

            return Json(new[] { at }.ToDataSourceResult(request, ModelState));
        }

        #endregion

        public bool CheckStatus(string StatusName, int Id)
        {
            using (DB_Entities db = new DB_Entities())
            {
                int count = 0;

                if (Id != 0)
                    count = db.MstStatus.Where(p => p.StatusName == StatusName.Trim() && p.IsDeleted == false && p.StatusId != Id).Count();
                else
                    count = db.MstStatus.Where(p => p.StatusName == StatusName.Trim() && p.IsDeleted == false).Count();

                return count > 0;
            }
        }

        public async Task<MstStatus> AddUpdateStatusType(MstStatus statu)
        {
            string strAction = "Status updated";
            string strActivity = string.Format("Status titled '{0}' ", statu.StatusName);

            using (DB_Entities db = new DB_Entities())
            {
                if (statu.StatusId == 0)
                {
                    MstStatus newstatusObj = new MstStatus()
                    {
                        StatusName = statu.StatusName.Trim(),
                        IsActive = statu.IsActive,
                        CreatedBy = new Guid(Session["UserID"].ToString()),
                        CreatedOn = DateTime.Now,
                        IsDeleted = false

                    };
                    db.MstStatus.Add(newstatusObj);
                    await db.SaveChangesAsync();
                    statu.StatusId = newstatusObj.StatusId;
                    strAction = "Status added";
                    strActivity += string.Format("added; ");
                }
                else
                {

                    var statuObj = db.MstStatus.Where(x => x.StatusId == statu.StatusId && x.IsDeleted == false).FirstOrDefault();
                    if (statuObj != null)
                    {
                        if (statuObj.StatusName != statu.StatusName)
                        {
                            strActivity += string.Format("updated from '{0}'; ", statuObj.StatusName);
                        }

                        if (statuObj.IsActive != statu.IsActive)
                        {
                            strActivity += string.Format("updated. Activation status changed from '{0}' to '{1}'; ", statuObj.IsActive, statu.IsActive);
                        }

                        statuObj.StatusName = statu.StatusName.Trim();
                        statuObj.IsActive = statu.IsActive;
                        statuObj.ModifiedBy = new Guid(Session["UserID"].ToString());
                        statuObj.ModifiedOn = DateTime.Now;
                        statuObj.IsDeleted = statu.IsDeleted;
                        db.Entry(statuObj).State = EntityState.Modified;
                        await db.SaveChangesAsync();                        

                        if (statu.IsDeleted == true)
                        {
                            strAction = "Status deleted";
                            strActivity += string.Format("deleted; ");
                        }
                    }
                }
            }

            await ActivityLogger.AddActivityLog(statu.StatusId, strAction, strActivity.Substring(0, strActivity.Length - 2));

            return statu;
        }
    }
}