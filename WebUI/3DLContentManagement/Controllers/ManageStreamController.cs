﻿using _3DLContentManagement.Common;
using _3DLContentManagement.Database;
using _3DLContentManagement.Models;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using TDL.ContentPlatform.Server.Database.Tables;

namespace _3DLContentManagement.Controllers
{
    [RoleAuthorize]
    public class ManageStreamController : Controller
    {
        // GET: ManageStream
        public ActionResult Index()
        {
            return View();
        }

        #region Select
        public ActionResult GetAll([DataSourceRequest] DataSourceRequest request)
        {
            List<MstStream> listStream = new List<MstStream>();
            using (DB_Entities db = new DB_Entities())
            {
                var streams = db.MstStream.Where(x => x.IsDeleted == false).OrderBy(x => x.StreamName).ToList();

                listStream = streams.Select(stream => new MstStream()
                {
                    StreamId = stream.StreamId,
                    StreamName = stream.StreamName,
                    IsActive = Convert.ToBoolean(stream.IsActive != null ? stream.IsActive : true),
                    IsDeleted = Convert.ToBoolean(stream.IsDeleted != null ? stream.IsDeleted : false),
                    CreatedBy = stream.CreatedBy != null ? stream.CreatedBy.Value : new Guid(),
                    CreatedOn = stream.CreatedOn != null ? stream.CreatedOn.Value : DateTime.UtcNow
                }).ToList();

            }

            return Json(listStream.ToDataSourceResult(request));
        }

        #endregion

        #region Add       
        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> AddUpdateStream([DataSourceRequest] DataSourceRequest request, MstStream stream)
        {
            if (CheckStream(stream.StreamName, stream.StreamId))
            {
                return this.Json(new DataSourceResult
                {
                    Errors = "ALREADY_EXISTS"
                });
            }

            MstStream objStream = new MstStream();
            if (stream != null)
            {
                objStream = await AddUpdateStreamDetails(stream);
            }

            return Json(new[] { objStream }.ToDataSourceResult(request, ModelState));
        }
        #endregion

        #region Delete
        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> DeleteStream([DataSourceRequest] DataSourceRequest request, MstStream stream)
        {
            MstStream objStream = new MstStream();
            if (stream != null)
            {
                stream.IsDeleted = true;
                objStream = await AddUpdateStreamDetails(stream);
            }

            return Json(new[] { objStream }.ToDataSourceResult(request, ModelState));
        }

        #endregion

        public bool CheckStream(string StreamName, int Id)
        {
            using (DB_Entities db = new DB_Entities())
            {
                int count = 0;

                if (Id != 0)
                    count = db.MstStream.Where(p => p.StreamName == StreamName.Trim() && p.IsDeleted == false && p.StreamId != Id).Count();
                else
                    count = db.MstStream.Where(p => p.StreamName == StreamName.Trim() && p.IsDeleted == false).Count();

                return count > 0;
            }
        }

        public async Task<MstStream> AddUpdateStreamDetails(MstStream stream)
        {
            string strAction = "Stream updated";
            string strActivity = string.Format("Stream titled '{0}' ", stream.StreamName);

            using (DB_Entities db = new DB_Entities())
            {
                if (stream.StreamId == 0)
                {
                    MstStream newStreamObj = new MstStream()
                    {
                        StreamName = stream.StreamName.Trim(),
                        IsActive = stream.IsActive,
                        CreatedBy = new Guid(Session["UserID"].ToString()),
                        CreatedOn = DateTime.Now,
                        IsDeleted = false

                    };
                    db.MstStream.Add(newStreamObj);
                    await db.SaveChangesAsync();
                    stream.StreamId = newStreamObj.StreamId;
                    strAction = "Stream added";
                    strActivity += string.Format("added; ");
                }
                else
                {
                    var streamObj = db.MstStream.Where(x => x.StreamId == stream.StreamId && x.IsDeleted == false).FirstOrDefault();
                    if (streamObj != null)
                    {
                        if (streamObj.StreamName != stream.StreamName)
                        {
                            strActivity += string.Format("updated from '{0}'; ", streamObj.StreamName);
                        }

                        if (streamObj.IsActive != stream.IsActive)
                        {
                            strActivity += string.Format("updated. Activation status changed from '{0}' to '{1}'; ", streamObj.IsActive, stream.IsActive);
                        }

                        streamObj.StreamName = stream.StreamName.Trim();
                        streamObj.IsActive = stream.IsActive;
                        streamObj.ModifiedBy = new Guid(Session["UserID"].ToString());
                        streamObj.ModifiedOn = DateTime.Now;
                        streamObj.IsDeleted = stream.IsDeleted;
                        db.Entry(streamObj).State = EntityState.Modified;
                        await db.SaveChangesAsync();

                        if (stream.IsDeleted == true)
                        {
                            strAction = "Stream deleted";
                            strActivity += string.Format("deleted; ");
                        }
                    }
                }
            }

            await ActivityLogger.AddActivityLog(stream.StreamId, strAction, strActivity.Substring(0, strActivity.Length - 2));

            return stream;
        }
    }
}