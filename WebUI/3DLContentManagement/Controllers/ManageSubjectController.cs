﻿using _3DLContentManagement.App_Start;
using _3DLContentManagement.Common;
using _3DLContentManagement.Database;
using _3DLContentManagement.Models;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using TDL.ContentPlatform.Server.Database.Tables;

namespace _3DLContentManagement.Controllers
{
    [RoleAuthorize]
    public class ManageSubjectController : Controller
    {
        // GET: ManageSubject
        public ActionResult Index()
        {
            return View();
        }

        #region Select
        public ActionResult GetAll([DataSourceRequest] DataSourceRequest request)
        {
            List<CsNewSubject> listsubjects = new List<CsNewSubject>();
            using (DB_Entities db = new DB_Entities())
            {
                //var subjects = db.MstSubject.Where(x => x.IsDeleted == false).OrderBy(x => x.SubjectName).ToList();

                //listsubjects = subjects.Select(subject => new MstSubject()
                //{
                //    SubjectId = subject.SubjectId,
                //    SubjectName = subject.SubjectName,
                //    IsActive = Convert.ToBoolean(subject.IsActive != null ? subject.IsActive : true),
                //    IsDeleted = Convert.ToBoolean(subject.IsDeleted != null ? subject.IsDeleted : false),
                //    CreatedBy = subject.CreatedBy != null ? subject.CreatedBy.Value : new Guid(),
                //    CreatedOn = subject.CreatedOn != null ? subject.CreatedOn.Value : DateTime.UtcNow
                //}).ToList();
                var subjects = db.CsNewSubject.OrderBy(x => x.NewName).ToList();

                listsubjects = subjects.Select(subject => new CsNewSubject()
                {
                    Id = subject.Id,
                    NewName = subject.NewName,
                    Createdbyyominame = subject.Createdbyyominame,
                    Createdon = subject.Createdon != null ? subject.Createdon.Value : DateTime.UtcNow
                }).ToList();
            }

            return Json(listsubjects.ToDataSourceResult(request));
        }

        #endregion

        #region Add       
        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> AddUpdateSubject([DataSourceRequest] DataSourceRequest request, MstSubject subject)
        {
            if (CheckSubjects(subject.SubjectName, subject.SubjectId))
            {
                return this.Json(new DataSourceResult
                {
                    Errors = "ALREADY_EXISTS"
                });
            }

            MstSubject at = new MstSubject();
            if (subject != null)
            {
                at = await AddUpdateSubject(subject);
            }

            return Json(new[] { at }.ToDataSourceResult(request, ModelState));
        }
        #endregion

        [HttpGet]
        public JsonResult CheckSubject(string SubjectName, int Id)
        {
            return Json(CheckSubjects(SubjectName, Id), JsonRequestBehavior.AllowGet);

        }

        #region Delete
        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> DeleteSubject([DataSourceRequest] DataSourceRequest request, MstSubject subject)
        {
            MstSubject at = new MstSubject();
            if (subject != null)
            {
                subject.IsDeleted = true;
                at = await AddUpdateSubject(subject);
            }

            return Json(new[] { at }.ToDataSourceResult(request, ModelState));
        }

        #endregion

        public bool CheckSubjects(string SubjectName, int Id)
        {
            using (DB_Entities db = new DB_Entities())
            {
                int count = 0;

                if (Id != 0)
                    count = db.MstSubject.Where(p => p.SubjectName == SubjectName.Trim() && p.IsDeleted == false && p.SubjectId != Id).Count();
                else
                    count = db.MstSubject.Where(p => p.SubjectName == SubjectName.Trim() && p.IsDeleted == false).Count();

                return count > 0;
            }
        }

        public async Task<MstSubject> AddUpdateSubject(MstSubject subject)
        {
            string strAction = "Subject updated";
            string strActivity = string.Format("Subject titled '{0}' ", subject.SubjectName);

            using (DB_Entities db = new DB_Entities())
            {
                if (subject.SubjectId == 0)
                {
                    MstSubject newSubjectObj = new MstSubject()
                    {
                        SubjectName = subject.SubjectName.Trim(),
                        IsActive = subject.IsActive,
                        CreatedBy = new Guid(Session["UserID"].ToString()),
                        CreatedOn = DateTime.Now,
                        IsDeleted = false

                    };
                    db.MstSubject.Add(newSubjectObj);
                    await db.SaveChangesAsync();
                    subject.SubjectId = newSubjectObj.SubjectId;
                    strAction = "Subject added";
                    strActivity += string.Format("added; ");
                }
                else
                {

                    var subjectObj = db.MstSubject.Where(x => x.SubjectId == subject.SubjectId && x.IsDeleted == false).FirstOrDefault();
                    if (subjectObj != null)
                    {
                        if (subjectObj.SubjectName != subject.SubjectName)
                        {
                            strActivity += string.Format("updated from '{0}'; ", subjectObj.SubjectName);
                        }

                        if (subjectObj.IsActive != subject.IsActive)
                        {
                            strActivity += string.Format("updated. Activation status changed from '{0}' to '{1}'; ", subjectObj.IsActive, subject.IsActive);
                        }

                        subjectObj.SubjectName = subject.SubjectName.Trim();
                        subjectObj.IsActive = subject.IsActive;
                        subjectObj.ModifiedBy = new Guid(Session["UserID"].ToString());
                        subjectObj.ModifiedOn = DateTime.Now;
                        subjectObj.IsDeleted = subject.IsDeleted;
                        db.Entry(subjectObj).State = EntityState.Modified;
                        await db.SaveChangesAsync();                        

                        if (subject.IsDeleted == true)
                        {
                            strAction = "Subject deleted";
                            strActivity += string.Format("deleted; ");
                        }
                    }
                }
            }

            await ActivityLogger.AddActivityLog(subject.SubjectId, strAction, strActivity.Substring(0, strActivity.Length - 2));

            return subject;
        }

        public string GetThumbnail(string Id)
        {
            var newId = Guid.Parse(Id);
            using (DB_Entities db = new DB_Entities())
            {
                var result = db.EntityThumbnail.Where(x => x.EntityId == newId && x.IsDeleted == false).FirstOrDefault();

                return result?.Thumbnail;
            }
        }

        #region Upload thumbnail image
        [HttpPost]
        public string UploadImage(HttpPostedFileBase UploadFile, string txtID)
        {
            string message = string.Empty;

            if (UploadFile != null && UploadFile.ContentLength > 0)
            {
                try
                {
                    string path = string.Empty;
                    path = System.IO.Path.Combine(WebConfigurationManager.AppSettings["PublishPath"].ToString(), "thumbnails");

                    //Check for the target path if it is exist
                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }

                    path = Path.Combine(path, Path.GetFileName(UploadFile.FileName));

                    UploadFile.SaveAs(path);
                    message = UploadFile.FileName;
                    var newId = Guid.Parse(txtID);
                    using (DB_Entities db = new DB_Entities())
                    {
                        var asset = db.EntityThumbnail.Where(x => x.EntityId == newId).FirstOrDefault();
                        if (asset != null)
                        {
                            asset.Thumbnail = Path.GetFileName(UploadFile.FileName);
                            asset.ModifiedBy = new Guid(Session["UserID"].ToString());
                            asset.ModifiedOn = DateTime.Now;
                            db.SaveChanges();
                        }
                        else
                        {
                            EntityThumbnail newContent = new EntityThumbnail()
                            {
                                EntityId = newId,
                                Thumbnail = Path.GetFileName(UploadFile.FileName),
                                CreatedBy = new Guid(Session["UserID"].ToString()),
                                CreatedOn = DateTime.Now,
                                IsDeleted = false
                            };

                            db.EntityThumbnail.Add(newContent);
                            db.SaveChanges();
                        }
                    }
                }
                catch (Exception)
                {
                    message = "Exception";
                }
            }
            else
            {
                message = "Error";
            }

            return message;
        }
        #endregion
    }
}