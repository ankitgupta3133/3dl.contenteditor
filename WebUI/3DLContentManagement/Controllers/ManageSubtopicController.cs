﻿using _3DLContentManagement.App_Start;
using _3DLContentManagement.Common;
using _3DLContentManagement.Database;
using _3DLContentManagement.Models;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using TDL.ContentPlatform.Server.Database.Tables;

namespace _3DLContentManagement.Controllers
{
    [RoleAuthorize]
    public class ManageSubtopicController : Controller
    {
        // GET: ManageSubtopic
        public ActionResult Index()
        {
            return View();
        }

        #region Select
        public ActionResult GetAll([DataSourceRequest] DataSourceRequest request)
        {
            List<CsNewSubtopic> listSubtopics = new List<CsNewSubtopic>();
            using (DB_Entities db = new DB_Entities())
            {
                //var subtopics = db.MstSubtopic.Where(x => x.IsDeleted == false).OrderBy(x => x.SubtopicName).ToList();

                //listSubtopics = subtopics.Select(subtopic => new MstSubtopic()
                //{
                //    SubtopicId = subtopic.SubtopicId,
                //    SubtopicName = subtopic.SubtopicName,
                //    PsubtopicId = subtopic.PsubtopicId,
                //    PsubtopicName = subtopic.PsubtopicName,
                //    IsActive = Convert.ToBoolean(subtopic.IsActive != null ? subtopic.IsActive : true),
                //    IsDeleted = Convert.ToBoolean(subtopic.IsDeleted != null ? subtopic.IsDeleted : false),
                //    CreatedBy = subtopic.CreatedBy != null ? subtopic.CreatedBy.Value : new Guid(),
                //    CreatedOn = subtopic.CreatedOn != null ? subtopic.CreatedOn.Value : DateTime.UtcNow
                //}).ToList();

                var subtopics = db.CsNewSubtopic.OrderBy(x => x.NewName).ToList();

                listSubtopics = subtopics.Select(subtopic => new CsNewSubtopic()
                {
                    Id = subtopic.Id,
                    NewName = subtopic.NewName,
                    Createdbyyominame = subtopic.Createdbyyominame,
                    Createdon = subtopic.Createdon != null ? subtopic.Createdon.Value : DateTime.UtcNow
                }).ToList();
            }

            return Json(listSubtopics.ToDataSourceResult(request));
        }

        #endregion

        #region Add       
        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> AddUpdateSubtopic([DataSourceRequest] DataSourceRequest request, MstSubtopic subtopic)
        {
            //string status = CheckSubtopic(subtopic.SubtopicName, subtopic.SubtopicId);
            if (CheckSubtopics(subtopic.SubtopicName, subtopic.SubtopicId))
            {
                return this.Json(new DataSourceResult
                {
                    Errors = "ALREADY_EXISTS"
                });
            }

            MstSubtopic at = new MstSubtopic();
            if (subtopic != null)
            {
                at = await AddUpdateSubtopics(subtopic);
            }

            return Json(new[] { at }.ToDataSourceResult(request, ModelState));
        }
        #endregion

        #region Delete
        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> DeleteSubtopic([DataSourceRequest] DataSourceRequest request, MstSubtopic subtopic)
        {
            MstSubtopic at = new MstSubtopic();
            if (subtopic != null)
            {
                if (CheckSubtopic(subtopic.SubtopicName, subtopic.SubtopicId))
                {
                    return this.Json(new DataSourceResult
                    {
                        Errors = "ALREADY_MAPPED"
                    });
                }
                else
                {
                    subtopic.IsDeleted = true;
                    at = await AddUpdateSubtopics(subtopic);
                }                
            }

            return Json(new[] { at }.ToDataSourceResult(request, ModelState));
        }

        #endregion

        public bool CheckSubtopics(string SubtopicName, int Id)
        {
            using (DB_Entities db = new DB_Entities())
            {
                int count = 0;

                if (Id != 0)
                    count = db.MstSubtopic.Where(p => p.SubtopicName == SubtopicName.Trim() && p.IsDeleted == false && p.SubtopicId != Id).Count();
                else
                    count = db.MstSubtopic.Where(p => p.SubtopicName == SubtopicName.Trim() && p.IsDeleted == false).Count();

                return count > 0;
            }
        }

        public bool CheckSubtopic(string SubtopicName, int Id)
        {
            using (DB_Entities db = new DB_Entities())
            {
                int count = 0;

                if (Id != 0)
                    count = db.MstSubtopic.Where(p => p.IsDeleted == false && p.PsubtopicId == Id).Count();

                return count > 0;
            }
        }

        #region Delete
        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> AddUpdateSubtopicDetails([DataSourceRequest] DataSourceRequest request, MstSubtopic subtopic)
        {
            MstSubtopic at = new MstSubtopic();
            if (subtopic != null)
            {
                using (DB_Entities db = new DB_Entities())
                {
                    if (subtopic.IsActive == false)
                    {
                        int count = 0;
                        count = db.MstSubtopic.Where(p => p.IsDeleted == false && p.IsActive == true && p.PsubtopicId == subtopic.SubtopicId).Count();

                        if (count > 0)
                        {
                            return this.Json(new DataSourceResult
                            {
                                Errors = "ALREADY_AVAILABLE"
                            });
                        }
                        else
                        {
                            at = await AddUpdateSubtopics(subtopic);
                        }
                    }
                    else
                    {
                        at = await AddUpdateSubtopics(subtopic);
                    }
                }
            }

            return Json(new[] { at }.ToDataSourceResult(request, ModelState));
        }

        #endregion

        public async Task<MstSubtopic> AddUpdateSubtopics(MstSubtopic subtopic)
        {
            string strAction = "Subtopic updated";
            string strActivity = string.Format("Subtopic titled '{0}' ", subtopic.SubtopicName);

            using (DB_Entities db = new DB_Entities())
            {
                if (subtopic.SubtopicId == 0)
                {
                    //Check parent subtopic value if exists in master
                    if (subtopic.PsubtopicName != null && subtopic.PsubtopicName != string.Empty)
                    {
                        subtopic.PsubtopicId = await CommonMethods.GetSubtopicID(subtopic.PsubtopicName);
                    }

                    MstSubtopic newsubtopicObj = new MstSubtopic()
                    {
                        SubtopicName = subtopic.SubtopicName.Trim(),
                        PsubtopicId = subtopic.PsubtopicId,
                        PsubtopicName = subtopic.PsubtopicName == null ? null : subtopic.PsubtopicName.Trim(),
                        IsActive = subtopic.IsActive,
                        CreatedBy = new Guid(Session["UserID"].ToString()),
                        CreatedOn = DateTime.Now,
                        IsDeleted = false

                    };
                    db.MstSubtopic.Add(newsubtopicObj);
                    db.SaveChanges();
                    subtopic.SubtopicId = newsubtopicObj.SubtopicId;
                    strAction = "Subtopic added";
                    strActivity += string.Format("added; ");
                }
                else
                {
                    var subtopicObj = db.MstSubtopic.Where(x => x.SubtopicId == subtopic.SubtopicId && x.IsDeleted == false).FirstOrDefault();
                    if (subtopicObj != null)
                    {
                        strActivity += string.Format("updated; ");
                        if (subtopicObj.SubtopicName != subtopic.SubtopicName)
                        {
                            db.MstSubtopic.Where(w => w.PsubtopicId == subtopic.SubtopicId).ToList().ForEach(i => i.PsubtopicName = subtopic.SubtopicName);
                            strActivity += string.Format("Name from '{0}'; ", subtopicObj.SubtopicName);
                        }

                        if (subtopicObj.PsubtopicName != subtopic.PsubtopicName)
                        {
                            //Check parent subtopic value if exists in master
                            if (subtopic.PsubtopicName != null && subtopic.PsubtopicName != string.Empty)
                            {
                                subtopic.PsubtopicId = await CommonMethods.GetSubtopicID(subtopic.PsubtopicName);
                            }
                            else
                            {
                                subtopic.PsubtopicId = 0;
                            }
                            strActivity += string.Format("Parent Subtopic changed from '{0}' to '{1}'; ", subtopicObj.PsubtopicName, subtopic.PsubtopicName);
                        }

                        if (subtopicObj.IsActive != subtopic.IsActive)
                        {
                            strActivity += string.Format("Activation status changed from '{0}' to '{1}'; ", subtopicObj.IsActive, subtopic.IsActive);
                        }

                        subtopicObj.SubtopicName = subtopic.SubtopicName.Trim();
                        subtopicObj.PsubtopicId = subtopic.PsubtopicId;
                        subtopicObj.PsubtopicName = subtopic.PsubtopicName?.Trim();
                        subtopicObj.IsActive = subtopic.IsActive;
                        subtopicObj.ModifiedBy = new Guid(Session["UserID"].ToString());
                        subtopicObj.ModifiedOn = DateTime.Now;
                        subtopicObj.IsDeleted = subtopic.IsDeleted;
                        db.Entry(subtopicObj).State = EntityState.Modified;
                        db.SaveChanges();                        

                        if (subtopic.IsDeleted == true)
                        {
                            strAction = "Subtopic deleted";
                            strActivity += string.Format("deleted; ");
                        }
                    }
                }
            }

            await ActivityLogger.AddActivityLog(subtopic.SubtopicId, strAction, strActivity.Substring(0, strActivity.Length - 2));

            return subtopic;
        }


        public ActionResult RefreshSubTopic()
        {
            using (DB_Entities db = new DB_Entities())
            {
                var result = db.MstSubtopic.Where(x => x.IsDeleted == false).Select(x => x.SubtopicName).ToList();

                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        public string GetThumbnail(string Id)
        {
            var newId = Guid.Parse(Id);
            using (DB_Entities db = new DB_Entities())
            {
                var result = db.EntityThumbnail.Where(x => x.EntityId == newId && x.IsDeleted == false).FirstOrDefault();

                return result?.Thumbnail;
            }
        }

        #region Upload thumbnail image
        [HttpPost]
        public string UploadImage(HttpPostedFileBase UploadFile, string txtID)
        {
            string message = string.Empty;

            if (UploadFile != null && UploadFile.ContentLength > 0)
            {
                try
                {
                    string path = string.Empty;
                    path = System.IO.Path.Combine(WebConfigurationManager.AppSettings["PublishPath"].ToString(), "thumbnails");

                    //Check for the target path if it is exist
                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }

                    path = Path.Combine(path, Path.GetFileName(UploadFile.FileName));

                    UploadFile.SaveAs(path);
                    message = UploadFile.FileName;
                    var newId = Guid.Parse(txtID);
                    using (DB_Entities db = new DB_Entities())
                    {
                        var asset = db.EntityThumbnail.Where(x => x.EntityId == newId).FirstOrDefault();
                        if (asset != null)
                        {
                            asset.Thumbnail = Path.GetFileName(UploadFile.FileName);
                            asset.ModifiedBy = new Guid(Session["UserID"].ToString());
                            asset.ModifiedOn = DateTime.Now;
                            db.SaveChanges();
                        }
                        else
                        {
                            EntityThumbnail newContent = new EntityThumbnail()
                            {
                                EntityId = newId,
                                Thumbnail = Path.GetFileName(UploadFile.FileName),
                                CreatedBy = new Guid(Session["UserID"].ToString()),
                                CreatedOn = DateTime.Now,
                                IsDeleted = false
                            };

                            db.EntityThumbnail.Add(newContent);
                            db.SaveChanges();
                        }
                    }
                }
                catch (Exception)
                {
                    message = "Exception";
                }
            }
            else
            {
                message = "Error";
            }

            return message;
        }
        #endregion
    }
}