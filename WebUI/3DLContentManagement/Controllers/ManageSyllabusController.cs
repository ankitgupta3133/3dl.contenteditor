﻿using _3DLContentManagement.Common;
using _3DLContentManagement.Database;
using _3DLContentManagement.Models;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using TDL.ContentPlatform.Server.Database.Tables;

namespace _3DLContentManagement.Controllers
{
    [RoleAuthorize]
    public class ManageSyllabusController : Controller
    {
        // GET: ManageSyllabus
        public ActionResult Index()
        {
            return View();
        }

        #region Select
        public ActionResult GetAll([DataSourceRequest] DataSourceRequest request)
        {
            List<MstSyllabus> listSyllabus = new List<MstSyllabus>();
            using (DB_Entities db = new DB_Entities())
            {
                var syllabus = db.MstSyllabus.Where(x => x.IsDeleted == false).OrderBy(x => x.SyllabusName).ToList();

                listSyllabus = syllabus.Select(syllabu => new MstSyllabus()
                {
                    SyllabusId = syllabu.SyllabusId,
                    SyllabusName = syllabu.SyllabusName,
                    IsActive = Convert.ToBoolean(syllabu.IsActive != null ? syllabu.IsActive : true),
                    IsDeleted = Convert.ToBoolean(syllabu.IsDeleted != null ? syllabu.IsDeleted : false),
                    CreatedBy = syllabu.CreatedBy != null ? syllabu.CreatedBy.Value : new Guid(),
                    CreatedOn = syllabu.CreatedOn != null ? syllabu.CreatedOn.Value : DateTime.UtcNow
                }).ToList();

            }

            return Json(listSyllabus.ToDataSourceResult(request));
        }

        #endregion

        #region Add       
        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> AddUpdateSyllabus([DataSourceRequest] DataSourceRequest request, MstSyllabus syllabus)
        {
            if (CheckSyllabus(syllabus.SyllabusName, syllabus.SyllabusId))
            {
                return this.Json(new DataSourceResult
                {
                    Errors = "ALREADY_EXISTS"
                });
            }

            MstSyllabus objSyllabus = new MstSyllabus();
            if (syllabus != null)
            {
                objSyllabus = await AddUpdateSyllabusDetails(syllabus);
            }

            return Json(new[] { objSyllabus }.ToDataSourceResult(request, ModelState));
        }
        #endregion

        #region Delete
        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> DeleteSyllabus([DataSourceRequest] DataSourceRequest request, MstSyllabus syllabus)
        {
            MstSyllabus objSyllabus = new MstSyllabus();
            if (syllabus != null)
            {
                syllabus.IsDeleted = true;
                objSyllabus = await AddUpdateSyllabusDetails(syllabus);
            }

            return Json(new[] { objSyllabus }.ToDataSourceResult(request, ModelState));
        }

        #endregion

        public bool CheckSyllabus(string SyllabusName, int Id)
        {
            using (DB_Entities db = new DB_Entities())
            {
                int count = 0;

                if (Id != 0)
                    count = db.MstSyllabus.Where(p => p.SyllabusName == SyllabusName.Trim() && p.IsDeleted == false && p.SyllabusId != Id).Count();
                else
                    count = db.MstSyllabus.Where(p => p.SyllabusName == SyllabusName.Trim() && p.IsDeleted == false).Count();

                return count > 0;
            }
        }

        public async Task<MstSyllabus> AddUpdateSyllabusDetails(MstSyllabus syllabus)
        {
            string strAction = "Syllabus updated";
            string strActivity = string.Format("Syllabus titled '{0}' ", syllabus.SyllabusName);

            using (DB_Entities db = new DB_Entities())
            {
                if (syllabus.SyllabusId == 0)
                {
                    MstSyllabus newSyllabusObj = new MstSyllabus()
                    {
                        SyllabusName = syllabus.SyllabusName.Trim(),
                        IsActive = syllabus.IsActive,
                        CreatedBy = new Guid(Session["UserID"].ToString()),
                        CreatedOn = DateTime.Now,
                        IsDeleted = false

                    };
                    db.MstSyllabus.Add(newSyllabusObj);
                    await db.SaveChangesAsync();
                    syllabus.SyllabusId = newSyllabusObj.SyllabusId;
                    strAction = "Syllabus added";
                    strActivity += string.Format("added; ");
                }
                else
                {
                    var syllabusObj = db.MstSyllabus.Where(x => x.SyllabusId == syllabus.SyllabusId && x.IsDeleted == false).FirstOrDefault();
                    if (syllabusObj != null)
                    {
                        if (syllabusObj.SyllabusName != syllabus.SyllabusName)
                        {
                            strActivity += string.Format("updated from '{0}'; ", syllabusObj.SyllabusName);
                        }

                        if (syllabusObj.IsActive != syllabus.IsActive)
                        {
                            strActivity += string.Format("updated. Activation status changed from '{0}' to '{1}'; ", syllabusObj.IsActive, syllabus.IsActive);
                        }

                        syllabusObj.SyllabusName = syllabus.SyllabusName.Trim();
                        syllabusObj.IsActive = syllabus.IsActive;
                        syllabusObj.ModifiedBy = new Guid(Session["UserID"].ToString());
                        syllabusObj.ModifiedOn = DateTime.Now;
                        syllabusObj.IsDeleted = syllabus.IsDeleted;
                        db.Entry(syllabusObj).State = EntityState.Modified;
                        await db.SaveChangesAsync();

                        if (syllabus.IsDeleted == true)
                        {
                            strAction = "Syllabus deleted";
                            strActivity += string.Format("deleted; ");
                        }
                    }
                }
            }

            await ActivityLogger.AddActivityLog(syllabus.SyllabusId, strAction, strActivity.Substring(0, strActivity.Length - 2));

            return syllabus;
        }
    }
}