﻿using _3DLContentManagement.App_Start;
using _3DLContentManagement.Common;
using _3DLContentManagement.Database;
using _3DLContentManagement.Models;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using TDL.ContentPlatform.Server.Database.Tables;

namespace _3DLContentManagement.Controllers
{
    [RoleAuthorize]
    public class ManageTagsController : Controller
    {
        // GET: ManageTags
        public ActionResult Index()
        {
            return View();
        }

        #region Select
        public ActionResult GetAll([DataSourceRequest] DataSourceRequest request)
        {
            List<MstTag> listTags = new List<MstTag>();
            using (DB_Entities db = new DB_Entities())
            {
                var tags = db.MstTag.Where(x => x.IsDeleted == false).OrderBy(x => x.TagName).ToList();

                listTags = tags.Select(tag => new MstTag()
                {
                    TagId = tag.TagId,
                    TagName = tag.TagName,
                    IsActive = Convert.ToBoolean(tag.IsActive != null ? tag.IsActive : true),
                    IsDeleted = Convert.ToBoolean(tag.IsDeleted != null ? tag.IsDeleted : false),
                    CreatedBy = tag.CreatedBy != null ? tag.CreatedBy.Value : new Guid(),
                    CreatedOn = tag.CreatedOn != null ? tag.CreatedOn.Value : DateTime.UtcNow
                }).ToList();

            }

            return Json(listTags.ToDataSourceResult(request));
        }

        [HttpGet]
        public ActionResult AddUpdateTags(int tagId)
        {
            TagViewModel tagViewModel = new TagViewModel();
            tagViewModel.TagId = tagId;
            tagViewModel.IsActive = true;
            tagViewModel.IsDeleted = false;
            if (tagId == 0)
            {
                TagNamesModel newTagName = new TagNamesModel() { TagId = 0, Name = string.Empty };
                tagViewModel.TagNames = new List<TagNamesModel>();
                tagViewModel.TagNames.Add(newTagName);

                using (DB_Entities db = new DB_Entities())
                {
                    ViewBag.Language = db.CsNewLanguage.Select(x => new SelectListItem { Text = x.NewName, Value = x.NewLanguageid.ToString() }).OrderBy(x => x.Text).ToList();
                }
            }
            else
            {
                using (DB_Entities db = new DB_Entities())
                {
                    var tag = db.MstTag.Where(a => a.TagId == tagId).FirstOrDefault();
                    tagViewModel.IsActive = tag.IsActive;

                    tagViewModel.TagNames = (from T in db.MstTagNames
                                             join L in db.CsNewLanguage on T.LanguageId equals L.NewLanguageid into TL
                                             from D in TL.DefaultIfEmpty()
                                             where T.TagId == tagId && T.IsDeleted == false
                                             select new TagNamesModel
                                             {
                                                 TagId = T.TagId,
                                                 IsDeleted = T.IsDeleted,
                                                 LanguageId = T.LanguageId,
                                                 Language = D.NewName,
                                                 Id = T.Id,
                                                 Name = T.Name
                                             }).ToList();

                    ViewBag.Language = db.CsNewLanguage.Select(x => new SelectListItem { Text = x.NewName, Value = x.NewLanguageid.ToString() }).OrderBy(x => x.Text).ToList();
                }
            }

            return PartialView("_AddUpdateTags", tagViewModel);
        }

        #endregion

        #region Add       
        [HttpPost]
        public async Task<JsonResult> AddUpdateTags(MstTag tag)
        {
            if (tag.TagNames != null && tag.TagNames.Count > 0)
            {
                if (CheckTags(tag.TagNames.ElementAt(0).Name, tag.TagId))
                {
                    return this.Json(new DataSourceResult
                    {
                        Errors = "ALREADY_EXISTS"
                    });
                }

                MstTag at = new MstTag();
                if (tag != null)
                {
                    at = await AddUpdateTagsDB(tag);
                }

                if (at.TagId == 0)
                {
                    return Json(new
                    {
                        isSuccess = false,
                        message = "Error adding tag"
                    });
                }
                else
                {
                    return Json(new
                    {
                        isSuccess = true,
                        message = tag.TagId == 0 ? "Tag added successfully." : "Tag updated successfully."
                    });
                }
            }
            else
            {
                return Json(new
                {
                    isSuccess = false,
                    message = "Please add atleast one tag"
                });
            }
        }
        #endregion

        [HttpGet]
        public JsonResult CheckTag(string TagName, int Id)
        {
            return Json(CheckTags(TagName, Id), JsonRequestBehavior.AllowGet);

        }

        #region Delete
        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> DeleteTag([DataSourceRequest] DataSourceRequest request, MstTag tag)
        {
            MstTag at = new MstTag();
            if (tag != null)
            {
                tag.IsDeleted = true;
                at = await AddUpdateTagsDB(tag);
            }

            return Json(new[] { at }.ToDataSourceResult(request, ModelState));
        }

        #endregion

        public bool CheckTags(string TagName, int Id)
        {
            using (DB_Entities db = new DB_Entities())
            {
                int count = 0;

                if (Id != 0)
                    count = db.MstTag.Where(p => p.TagName == TagName.Trim() && p.IsDeleted == false && p.TagId != Id).Count();
                else
                    count = db.MstTag.Where(p => p.TagName == TagName.Trim() && p.IsDeleted == false).Count();

                return count > 0;
            }
        }

        public async Task<MstTag> AddUpdateTagsDB(MstTag tag)
        {
            string strAction = "Tag updated";
            string strActivity = string.Format("Tag titled '{0}' ", tag.TagName);

            using (DB_Entities db = new DB_Entities())
            {
                if (tag.TagId == 0)
                {
                    for (int i = 0; i < tag.TagNames.Count; i++)
                    {
                        if (i == 0)
                        {
                            MstTag newTagObj = new MstTag()
                            {
                                TagName = tag.TagNames.ElementAt(i).Name.Trim(),
                                IsActive = tag.IsActive,
                                CreatedBy = new Guid(Session["UserID"].ToString()),
                                CreatedOn = DateTime.Now,
                                IsDeleted = false

                            };
                            db.MstTag.Add(newTagObj);
                            await db.SaveChangesAsync();
                            tag.TagId = newTagObj.TagId;
                        }

                        //Tag Name
                        MstTagNames newMstTagName = new MstTagNames()
                        {
                            IsDeleted = false,
                            Name = tag.TagNames.ElementAt(i).Name,
                            LanguageId = tag.TagNames.ElementAt(i).LanguageId,
                            TagId = tag.TagId
                        };
                        db.MstTagNames.Add(newMstTagName);
                    }

                    await db.SaveChangesAsync();
                    strAction = "Tag added";
                    strActivity += string.Format("added; ");
                }
                else
                {

                    var tagObj = db.MstTag.Where(x => x.TagId == tag.TagId && x.IsDeleted == false).FirstOrDefault();
                    if (tagObj != null)
                    {
                        if (tag.IsDeleted == true || tag.TagNames.Where(x => x.IsDeleted == false).Count() == 0)
                        {
                            tagObj.IsDeleted = true;
                            strAction = "Tag deleted";
                            strActivity += string.Format("deleted; ");
                        }
                        else
                        {

                            for (int i = 0; i < tag.TagNames.Count; i++)
                            {
                                var tagNameObj = await db.MstTagNames.Where(a => a.Id == tag.TagNames.ElementAt(i).Id).FirstOrDefaultAsync();
                                if (tagNameObj != null)
                                {
                                    tagNameObj.IsDeleted = tag.TagNames.ElementAt(i).IsDeleted;
                                    tagNameObj.Name = tag.TagNames.ElementAt(i).Name;
                                    tagNameObj.LanguageId = tag.TagNames.ElementAt(i).LanguageId;
                                }
                                else
                                {
                                    MstTagNames newMstTagName = new MstTagNames()
                                    {
                                        IsDeleted = false,
                                        Name = tag.TagNames.ElementAt(i).Name,
                                        LanguageId = tag.TagNames.ElementAt(i).LanguageId,
                                        TagId = tag.TagId
                                    };
                                    db.MstTagNames.Add(newMstTagName);
                                }
                            }

                            tagObj.TagName = tag.TagNames.Where(x => x.IsDeleted == false).FirstOrDefault().Name.Trim();
                            tagObj.IsActive = tag.IsActive;
                            tagObj.ModifiedBy = new Guid(Session["UserID"].ToString());
                            tagObj.ModifiedOn = DateTime.Now;                           
                            db.Entry(tagObj).State = EntityState.Modified;

                        }
                        await db.SaveChangesAsync();                        
                    }
                }
            }

            await ActivityLogger.AddActivityLog(tag.TagId, strAction, strActivity.Substring(0, strActivity.Length - 2));

            return tag;
        }
    }
}