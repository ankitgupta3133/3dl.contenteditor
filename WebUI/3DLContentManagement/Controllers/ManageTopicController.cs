﻿using _3DLContentManagement.App_Start;
using _3DLContentManagement.Common;
using _3DLContentManagement.Database;
using _3DLContentManagement.Models;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using TDL.ContentPlatform.Server.Database.Tables;

namespace _3DLContentManagement.Controllers
{
    [RoleAuthorize]
    public class ManageTopicController : Controller
    {
        // GET: ManageTopic
        public ActionResult Index()
        {
            return View();
        }

        #region Select
        public ActionResult GetAll([DataSourceRequest] DataSourceRequest request)
        {
            //PopulateSubjects();

            List<CsNewTopic> listTopics = new List<CsNewTopic>();
            using (DB_Entities db = new DB_Entities())
            {
                //var topics = db.MstTopic.Where(x => x.IsDeleted == false).OrderBy(x => x.TopicName).ToList();

                //listTopics = topics.Select(topic => new MstTopic()
                //{
                //    TopicId = topic.TopicId,
                //    TopicName = topic.TopicName,
                //    SubjectId = topic.SubjectId,
                //    //Subject = topic.MstSubject.SubjectName,
                //    IsActive = Convert.ToBoolean(topic.IsActive != null ? topic.IsActive : true),
                //    IsDeleted = Convert.ToBoolean(topic.IsDeleted != null ? topic.IsDeleted : false),
                //    CreatedBy = topic.CreatedBy != null ? topic.CreatedBy.Value : new Guid(),
                //    CreatedOn = topic.CreatedOn != null ? topic.CreatedOn.Value : DateTime.UtcNow
                //}).ToList();
                var topics = db.CsNewTopic.OrderBy(x => x.NewName).ToList();

                listTopics = topics.Select(topic => new CsNewTopic()
                {
                    Id = topic.Id,
                    NewName = topic.NewName,
                    Createdbyyominame = topic.Createdbyyominame,
                    Createdon = topic.Createdon != null ? topic.Createdon.Value : DateTime.UtcNow
                }).ToList();

            }

            return Json(listTopics.ToDataSourceResult(request));
        }

        #endregion
        private void PopulateSubjects()
        {
            var dataContext = new DB_Entities();
            var subjects = dataContext.MstSubject
                        .Select(c => new MstSubject
                        {
                            SubjectId = c.SubjectId,
                            SubjectName = c.SubjectName
                        })
                        .OrderBy(e => e.SubjectName);

            ViewData["subjects"] = subjects;
            ViewData["defaultSubject"] = subjects.FirstOrDefault();
        }

        #region Add       
        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> AddUpdateTopic([DataSourceRequest] DataSourceRequest request, MstTopic topic)
        {
            if (CheckTopics(topic.TopicName, topic.TopicId))
            {
                return this.Json(new DataSourceResult
                {
                    Errors = "ALREADY_EXISTS"
                });
            }

            MstTopic at = new MstTopic();
            if (topic != null)
            {
                at = await AddUpdateTopics(topic);
            }

            return Json(new[] { at }.ToDataSourceResult(request, ModelState));
        }
        #endregion

        [HttpGet]
        public JsonResult CheckTopic(string TopicName, int Id)
        {
            return Json(CheckTopics(TopicName, Id), JsonRequestBehavior.AllowGet);

        }

        #region Delete
        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> DeleteTopic([DataSourceRequest] DataSourceRequest request, MstTopic topic)
        {
            MstTopic at = new MstTopic();
            if (topic != null)
            {
                topic.IsDeleted = true;
                at = await AddUpdateTopics(topic);
            }

            return Json(new[] { at }.ToDataSourceResult(request, ModelState));
        }

        #endregion

        public bool CheckTopics(string TopicName, int Id)
        {
            using (DB_Entities db = new DB_Entities())
            {
                int count = 0;

                if (Id != 0)
                    count = db.MstTopic.Where(p => p.TopicName == TopicName.Trim() && p.IsDeleted == false && p.TopicId != Id).Count();
                else
                    count = db.MstTopic.Where(p => p.TopicName == TopicName.Trim() && p.IsDeleted == false).Count();

                return count > 0;
            }
        }

        public async Task<MstTopic> AddUpdateTopics(MstTopic topic)
        {
            string strAction = "Topic updated";
            string strActivity = string.Format("Topic titled '{0}' ", topic.TopicName);

            using (DB_Entities db = new DB_Entities())
            {
                if (topic.TopicId == 0)
                {
                    MstTopic newTopicObj = new MstTopic()
                    {
                        TopicName = topic.TopicName.Trim(),
                        IsActive = topic.IsActive,
                        CreatedBy = new Guid(Session["UserID"].ToString()),
                        CreatedOn = DateTime.Now,
                        IsDeleted = false

                    };
                    db.MstTopic.Add(newTopicObj);
                    await db.SaveChangesAsync();
                    topic.TopicId = newTopicObj.TopicId;
                    strAction = "Topic added";
                    strActivity += string.Format("added; ");
                }
                else
                {
                    var topicObj = db.MstTopic.Where(x => x.TopicId == topic.TopicId && x.IsDeleted == false).FirstOrDefault();
                    if (topicObj != null)
                    {
                        if (topicObj.TopicName != topic.TopicName)
                        {
                            strActivity += string.Format("updated from '{0}'; ", topicObj.TopicName);
                        }

                        if (topicObj.IsActive != topic.IsActive)
                        {
                            strActivity += string.Format("updated. Activation status changed from '{0}' to '{1}'; ", topicObj.IsActive, topic.IsActive);
                        }

                        topicObj.TopicName = topic.TopicName.Trim();
                        topicObj.IsActive = topic.IsActive;
                        topicObj.ModifiedBy = new Guid(Session["UserID"].ToString());
                        topicObj.ModifiedOn = DateTime.Now;
                        topicObj.IsDeleted = topic.IsDeleted;
                        db.Entry(topicObj).State = EntityState.Modified;
                        await db.SaveChangesAsync();                        

                        if (topic.IsDeleted == true)
                        {
                            strAction = "Topic deleted";
                            strActivity += string.Format("deleted; ", topic.TopicName);
                        }
                    }
                }
            }

            await ActivityLogger.AddActivityLog(topic.TopicId, strAction, strActivity.Substring(0, strActivity.Length - 2));

            return topic;
        }

        public string GetThumbnail(string Id)
        {
            var newId = Guid.Parse(Id);
            using (DB_Entities db = new DB_Entities())
            {
                var result = db.EntityThumbnail.Where(x => x.EntityId == newId && x.IsDeleted == false).FirstOrDefault();

                return result?.Thumbnail;
            }
        }

        #region Upload thumbnail image
        [HttpPost]
        public string UploadImage(HttpPostedFileBase UploadFile, string txtID)
        {
            string message = string.Empty;

            if (UploadFile != null && UploadFile.ContentLength > 0)
            {
                try
                {
                    string path = string.Empty;
                    path = System.IO.Path.Combine(WebConfigurationManager.AppSettings["PublishPath"].ToString(), "thumbnails");
                    //path = Path.Combine(path, Path.GetFileName(UploadFile.FileName));

                    //var source = Path.Combine(path, "temp");
                    ////Check for the target path if it is exist
                    //if (!Directory.Exists(source))
                    //{
                    //    Directory.CreateDirectory(source);
                    //}

                    // get path including filename
                    path = Path.Combine(path, UploadFile.FileName);
                    UploadFile.SaveAs(path);

                    //string file = Tedd.StreamHasher.File.CopyFileAndRename(source, path).Result.Hash;

                    message = UploadFile.FileName;
                    var newId = Guid.Parse(txtID);
                    using (DB_Entities db = new DB_Entities())
                    {
                        var asset = db.EntityThumbnail.Where(x => x.EntityId == newId).FirstOrDefault();
                        if (asset != null)
                        {
                            asset.Thumbnail = Path.GetFileName(UploadFile.FileName);
                            //asset.ThumbnailUrl = file;
                            asset.ModifiedBy = new Guid(Session["UserID"].ToString());
                            asset.ModifiedOn = DateTime.Now;
                            db.SaveChanges();
                        }
                        else
                        {
                            EntityThumbnail newContent = new EntityThumbnail()
                            {
                                EntityId = newId,
                                Thumbnail = Path.GetFileName(UploadFile.FileName),
                                //ThumbnailUrl = file,
                                CreatedBy = new Guid(Session["UserID"].ToString()),
                                CreatedOn = DateTime.Now,
                                IsDeleted = false
                            };

                            db.EntityThumbnail.Add(newContent);
                            db.SaveChanges();
                        }
                    }

                    //if (System.IO.File.Exists(source))
                    //    System.IO.File.Delete(source);
                }
                catch (Exception)
                {
                    message = "Exception";
                }
            }
            else
            {
                message = "Error";
            }

            return message;
        }
        #endregion
    }
}