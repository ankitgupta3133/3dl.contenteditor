﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using _3DLContentManagement.Models;
using _3DLContentManagement.Common;
using _3DLContentManagement.App_Start;
using System.Threading.Tasks;
using TDL.ContentPlatform.Server.Database.Tables;
using _3DLContentManagement.Database;
using Microsoft.EntityFrameworkCore;

namespace _3DLContentManagement.Controllers
{
    [RoleAuthorize]
    public class ManageTypeController : Controller
    {
        // GET: ManageType
        public ActionResult Index()
        {
            return View();
        }

        #region Select
        public ActionResult GetAll([DataSourceRequest] DataSourceRequest request)
        {
            List<MstFileType> listAssets = new List<MstFileType>();
            using (DB_Entities db = new DB_Entities())
            {               
                var assets = db.MstFileType.Where(x => x.IsDeleted == false).OrderBy(x => x.TypeName).ToList();

                listAssets = assets.Select(asset => new MstFileType()
                {
                    FileTypeId = asset.FileTypeId,
                    TypeName = asset.TypeName,
                    IsActive = Convert.ToBoolean(asset.IsActive != null ? asset.IsActive : true),
                    IsDeleted = Convert.ToBoolean(asset.IsDeleted != null ? asset.IsDeleted : false),
                    CreatedBy = asset.CreatedBy != null ? asset.CreatedBy.Value : new Guid(),
                    CreatedOn = asset.CreatedOn != null ? asset.CreatedOn.Value : DateTime.UtcNow
                }).ToList();
                
            }

            return Json(listAssets.ToDataSourceResult(request));
        }

        #endregion

        #region Add       
        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> AddUpdateAssetType([DataSourceRequest] DataSourceRequest request, MstFileType asset)
        {
            if (CheckAssetsType(asset.TypeName, asset.FileTypeId))
            {
                return this.Json(new DataSourceResult
                {
                    Errors = "ALREADY_EXISTS"
                });
            }

            MstFileType at = new MstFileType();
            if (asset != null)
            {
                at = await AddUpdateType(asset);
            }

            return Json(new[] { at }.ToDataSourceResult(request, ModelState));
        }
        #endregion

        [HttpGet]
        public JsonResult CheckAssetType(string TypeName, int Id)
        {
            return Json(CheckAssetsType(TypeName, Id), JsonRequestBehavior.AllowGet);

        }

        #region Delete
        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> DeleteAssetType([DataSourceRequest] DataSourceRequest request, MstFileType asset)
        {
            MstFileType at = new MstFileType();
            if (asset != null)
            {
                asset.IsDeleted = true;
                at = await AddUpdateType(asset);
            }

            return Json(new[] { at }.ToDataSourceResult(request, ModelState));
        }

        #endregion

        public bool CheckAssetsType(string TypeName, int Id)
        {
            using (DB_Entities db = new DB_Entities())
            {
                int count = 0;
                
                if (Id != 0)
                    count = db.MstFileType.Where(p => p.TypeName == TypeName.Trim() && p.IsDeleted == false && p.FileTypeId != Id).Count();
                else
                    count = db.MstFileType.Where(p => p.TypeName == TypeName.Trim() && p.IsDeleted == false).Count();
                
                return count > 0;
            }
        }

        public async Task<MstFileType> AddUpdateType(MstFileType asset)
        {
            string strAction = "Asset updated";
            string strActivity = string.Format("Asset type titled '{0}' ", asset.TypeName);

            using (DB_Entities db = new DB_Entities())
            {
                if (asset.FileTypeId == 0)
                {
                    MstFileType newAssetTypeObj = new MstFileType()
                    {
                        TypeName = asset.TypeName.Trim(),
                        IsActive = asset.IsActive,
                        CreatedBy = new Guid(Session["UserID"].ToString()),
                        CreatedOn = DateTime.Now,
                        IsDeleted = false

                    };
                    db.MstFileType.Add(newAssetTypeObj);
                    await db.SaveChangesAsync();
                    asset.FileTypeId = newAssetTypeObj.FileTypeId;
                    strAction = "Asset added";
                    strActivity += string.Format("added; ");                    
                }
                else
                {

                    var assetObj = db.MstFileType.Where(x => x.FileTypeId == asset.FileTypeId && x.IsDeleted == false).FirstOrDefault();
                    if (assetObj != null)
                    {
                        if (assetObj.TypeName != asset.TypeName)
                        {
                            strActivity += string.Format("updated from '{0}'; ", assetObj.TypeName);
                        }

                        if (assetObj.IsActive != asset.IsActive)
                        {
                            strActivity += string.Format("updated. Activation status changed from '{0}' to '{1}'; ", assetObj.IsActive, asset.IsActive);
                        }

                        assetObj.TypeName = asset.TypeName.Trim();
                        assetObj.IsActive = asset.IsActive;
                        assetObj.ModifiedBy = new Guid(Session["UserID"].ToString());
                        assetObj.ModifiedOn = DateTime.Now;
                        assetObj.IsDeleted = asset.IsDeleted;
                        db.Entry(assetObj).State = EntityState.Modified;
                        await db.SaveChangesAsync();                        

                        if (asset.IsDeleted == true)
                        {
                            strAction = "Asset deleted";
                            strActivity += string.Format("deleted; ", asset.TypeName);
                        }                        
                    }
                }
            }

            await ActivityLogger.AddActivityLog(asset.FileTypeId, strAction, strActivity.Substring(0, strActivity.Length - 2));

            return asset;
        }
    }
}