﻿using _3DLContentManagement.App_Start;
using _3DLContentManagement.Common;
using _3DLContentManagement.Database;
using _3DLContentManagement.Models;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using RT.Comb;
using TDL.ContentPlatform.Server.Database.Tables;

namespace _3DLContentManagement.Controllers
{
    [RoleAuthorize(Roles="Admin")]
    public class ManageUsersController : Controller
    {
        // GET: ManageUsers
        public ActionResult Index()
        {
            //ViewBag.Password = CommonMethods.GenerateRandomPassword();
            return View();
        }

        #region Select
        public ActionResult GetAll([DataSourceRequest] DataSourceRequest request)
        {
            List<UserProfile> listUsers = new List<UserProfile>();
            using (DB_Entities db = new DB_Entities())
            {
                var users = db.UserProfile.Where(x => x.IsDeleted == false).OrderBy(x => x.FirstName).ToList();

                listUsers = users.Select(user => new UserProfile()
                {
                    UserId = user.UserId,
                    UserName = user.UserName,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    Password = user.Password,
                    IsActive = Convert.ToBoolean(user.IsActive != null ? user.IsActive : true),
                    IsDeleted = Convert.ToBoolean(user.IsDeleted != null ? user.IsDeleted : false),
                    CreatedBy = user.CreatedBy != null ? user.CreatedBy.Value : new Guid(),
                    CreatedOn = user.CreatedOn != null ? user.CreatedOn.Value : DateTime.UtcNow
                }).ToList();

            }

            return Json(listUsers.ToDataSourceResult(request));
        }

        #endregion

        #region Add       
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddUpdateUser([DataSourceRequest] DataSourceRequest request, UserProfile user, string AutoPassword)
        {
            if (CheckUsers(user.UserName, user.UserId))
            {
                return this.Json(new DataSourceResult
                {
                    Errors = "ALREADY_EXISTS"
                });
            }

            UserProfile at = new UserProfile();
            if (user != null)
            {
                if (user.Password == string.Empty || user.Password == null)
                {
                    user.Password = AutoPassword;
                }
                at = AddUpdateUsers(user);
            }

            return Json(new[] { at }.ToDataSourceResult(request, ModelState));
        }
        #endregion

        [HttpGet]
        public JsonResult CheckUser(string UserName, Guid Id)
        {
            return Json(CheckUsers(UserName, Id), JsonRequestBehavior.AllowGet);

        }

        #region Delete
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteUser([DataSourceRequest] DataSourceRequest request, UserProfile user)
        {
            UserProfile at = new UserProfile();
            if (user != null)
            {
                user.IsDeleted = true;
                at = AddUpdateUsers(user);
            }

            return Json(new[] { at }.ToDataSourceResult(request, ModelState));
        }

        #endregion

        public bool CheckUsers(string UserName, Guid Id)
        {
            using (DB_Entities db = new DB_Entities())
            {
                int count = 0;

                if (Id != Guid.Empty)
                    count = db.UserProfile.Where(p => p.UserName == UserName.Trim() && p.IsDeleted == false && p.UserId != Id).Count();
                else
                    count = db.UserProfile.Where(p => p.UserName == UserName.Trim() && p.IsDeleted == false).Count();

                return count > 0;
            }
        }

        public UserProfile AddUpdateUsers(UserProfile user)
        {
            string strAction = "User details updated";
            string strActivity = string.Format("User named '{0}' ", user.UserName);
            using (DB_Entities db = new DB_Entities())
            {
                if (user.UserId == Guid.Empty)
                {
                    UserProfile newUserObj = new UserProfile()
                    {
                        UserId =  Provider.Sql.Create(),
                        FirstName = user.FirstName.Trim(),
                        LastName = user.LastName,
                        Password = CommonMethods.Encrypt(user.Password.Trim()),
                        UserName = user.UserName.Trim(),
                        IsActive = user.IsActive,
                        CreatedBy = new Guid(Session["UserID"].ToString()),
                        CreatedOn = DateTime.Now,
                        IsDeleted = false
                    };
                    db.UserProfile.Add(newUserObj);
                    db.SaveChanges();

                    var mailcontent = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/EmailTemplates/NewUserCreation.html"));
                    mailcontent = mailcontent.Replace("{Name}", user.FirstName.Trim()).Replace("{URL}", WebConfigurationManager.AppSettings["domain"].ToString()).Replace("{Email}", user.UserName).Replace("{Password}", user.Password);

                    //Sharing account details with the user
                    Mailer.SendMail(WebConfigurationManager.AppSettings["replyAddress"].ToString(), user.UserName, "New account creation", mailcontent);

                    strAction = "User added";
                    strActivity += string.Format("added; ");
                }
                else
                {
                    strActivity = string.Format("User details updated for named '{0}'. ", user.FirstName);
                    var userObj = db.UserProfile.Where(x => x.UserId == user.UserId && x.IsDeleted == false).FirstOrDefault();
                    if (userObj != null)
                    {
                        if (userObj.UserName != user.UserName)
                        {
                            strActivity += string.Format("UserName changed from '{0}' to '{1}'; ", userObj.UserName, user.UserName);
                        }

                        if (!userObj.FirstName.Equals(user.FirstName.Trim()))
                        {
                            strActivity += string.Format("First Name changed from '{0}' to '{1}'; ", userObj.FirstName, user.FirstName);
                        }

                        user.LastName = user.LastName != string.Empty && user.LastName != null ? user.LastName.Trim() : user.LastName;
                        if (userObj.LastName != user.LastName)
                        {
                            strActivity += string.Format("Last Name changed from '{0}' to '{1}'; ", userObj.LastName, user.LastName);
                        }

                        if (userObj.IsActive != user.IsActive)
                        {
                            strActivity += string.Format("updated. Activation status changed from '{0}' to '{1}'; ", userObj.IsActive, user.IsActive);
                        }

                        userObj.FirstName = user.FirstName.Trim();
                        userObj.LastName = user.LastName;
                        if (userObj.Password != user.Password.Trim()){
                            userObj.Password = CommonMethods.Encrypt(user.Password.Trim());
                            strActivity += string.Format("Password changed; ");

                            var mailcontent = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/EmailTemplates/ChangePassword.html"));
                            mailcontent = mailcontent.Replace("{Name}", user.FirstName.Trim()).Replace("{URL}", WebConfigurationManager.AppSettings["domain"].ToString()).Replace("{Email}", user.UserName).Replace("{Password}", user.Password);

                            //Sharing account details with the user
                            Mailer.SendMail(WebConfigurationManager.AppSettings["replyAddress"].ToString(), user.UserName, "Password Changed", mailcontent);
                        };
                        userObj.UserName = user.UserName.Trim();
                        userObj.IsActive = user.IsActive;
                        userObj.ModifiedBy = new Guid(Session["UserID"].ToString());
                        userObj.ModifiedOn = DateTime.Now;
                        userObj.IsDeleted = user.IsDeleted;
                        db.Entry(userObj).State = EntityState.Modified;
                        db.SaveChanges();                        

                        if (user.IsDeleted == true)
                        {
                            strAction = "User deleted";
                            strActivity += string.Format("deleted; ");
                        }
                    }
                }
            }

            ActivityLogger.AddActivityLogs(0, strAction, strActivity.Substring(0, strActivity.Length - 2));

            return user;
        }
    }
}