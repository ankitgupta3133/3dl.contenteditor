﻿using _3DLContentManagement.Common;
using _3DLContentManagement.Database;
using _3DLContentManagement.Models;
using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using TDL.ContentPlatform.Server.Database.Tables;

namespace _3DLContentManagement.Controllers
{
    [RoleAuthorize]
    public class ModuleController : Controller
    {
        // GET: Module
        public async Task<ActionResult> Index(int? assetID)
        {
            DB_Entities db = new DB_Entities();
            AssetViewModel asset = new AssetViewModel();
            List<AssetContentModel> listContents = new List<AssetContentModel>();
            List<AssetNameModel> listNames = new List<AssetNameModel>();
            List<AssetDetailModel> listDetails = new List<AssetDetailModel>();
            List<Label> listLabels = new List<Label>();
            await GetDataListFromSessions();
            ViewBag.AddedType = "Add";

            if (assetID != null && assetID != 0)
            {
                Asset objAsset = db.Asset.FirstOrDefault(x => x.AssetId == assetID);
                CsNewPackage objPackage = db.CsNewPackage.FirstOrDefault(x => x.Id == objAsset.PackageId);

                listNames = await GetAssetNames(objAsset.AssetId);
                listDetails = await GetAssetDetails(objAsset.AssetId);
                listContents = await GetAssetContentList(objAsset.AssetId);

                asset.AssetID = objAsset.AssetId;
                asset.AssetName = objAsset.AssetName;
                asset.IsDirectory = objAsset.IsDirectory;
                asset.AssetType = objAsset.Type;
                asset.Language = objAsset.Language;
                asset.Syllabus = objAsset.Syllabus;
                asset.Status = objAsset.Status;
                asset.Stream = objAsset.Stream;
                asset.Subject = objAsset.Subject;
                asset.Level = objAsset.Level;
                asset.Topic = objAsset.Topic;
                asset.Subtopic = objAsset.Subtopic;
                asset.SubtopicDesc = objAsset.SubtopicDesc;
                //asset.Tags = objAsset.Tags;
                asset.IsActive = objAsset.IsActive;
                asset.IsDeleted = objAsset.IsDeleted;
                asset.CreatedBy = objAsset.CreatedBy;
                asset.CreatedOn = objAsset.CreatedOn;

                ViewBag.TagsSelectList = new SelectList(CommonMethods.GetAssetTags(objAsset.AssetId), "Value", "Text");
                ViewBag.PropertiesSelectList = new SelectList(CommonMethods.GetAssetProperties(objAsset.AssetId), "Value", "Text");

                ViewBag.AddedType = "Edit";
            }
            else
            {
                AssetContentModel content = new AssetContentModel();
                content.ID = 0;
                content.PlatformId = db.CsNewPlatform.Where(x => x.NewName.Equals("All")).Select(x => x.NewPlatformid.ToString()).FirstOrDefault();
                content.VersionNumber = 1;
                listContents.Add(content);

                AssetNameModel name = new AssetNameModel();
                listNames.Add(name);

                AssetDetailModel detail = new AssetDetailModel();
                listDetails.Add(detail);

                ViewBag.TagsSelectList = new SelectList(CommonMethods.GetAssetTags(0), "Value", "Text");
                ViewBag.PropertiesSelectList = new SelectList(CommonMethods.GetAssetProperties(0), "Value", "Text");
            }

            asset.Names = listNames;
            asset.Details = listDetails;
            asset.Contents = listContents;

            return View(asset);
        }

        [HttpPost]
        public async Task<ActionResult> Index(AssetViewModel asset, string addedType)
        {
            await GetDataListFromSessions();
            if (ModelState.IsValid)
            {
                bool IsChanged = false;
                string strAction;
                string strActivity;

                using (DB_Entities db = new DB_Entities())
                {
                    if (asset.AssetID == 0)
                    {
                        Asset newAssetObj = new Asset()
                        {
                            AssetName = asset.Names[0].Name.Trim(),
                            FileName = asset.FileName,
                            FilePath = asset.FilePath,
                            FileSize = Convert.ToString(asset.FileSize),
                            IsDirectory = asset.IsDirectory,
                            Type = asset.AssetType,
                            Status = asset.Status,
                            Language = asset.Details[0].Language,
                            Syllabus = asset.Details[0].Syllabus,
                            Subject = asset.Details[0].Subject,
                            Level = asset.Details[0].Grade,
                            Topic = asset.Details[0].Topic,
                            Subtopic = asset.Details[0].Subtopic,
                            SubtopicDesc = asset.SubtopicDesc,
                            //Tags = asset.Tags,
                            Stream = asset.Details[0].Stream,
                            PackageId = asset.Details[0].PackageId,
                            IsQuizEnabled = asset.IsQuizEnabled,
                            PercentageOfLabels = asset.PercentageOfLabels,
                            NumberOfAttempts = asset.NumberOfAttempts,
                            IsActive = asset.IsActive == null ? true : asset.IsActive,
                            CreatedBy = new Guid(Session["UserID"].ToString()),
                            CreatedOn = DateTime.Now,
                            ModifiedByName = Session["Name"].ToString(),
                            ModifiedOn = DateTime.Now,
                            IsDeleted = false
                        };
                        db.Asset.Add(newAssetObj);
                        db.SaveChanges();

                        if (asset.NewTags != null && asset.NewTags.Length > 0)
                        {
                            CommonMethods.SaveAssetTags(newAssetObj.AssetId, asset.NewTags);                           
                        }

                        if (asset.NewProperties != null && asset.NewProperties.Length > 0)
                        {
                            CommonMethods.SaveAssetProperties(newAssetObj.AssetId, asset.NewProperties);
                        }

                        asset.AssetID = newAssetObj.AssetId;
                        asset.AssetName = asset.Names[0].Name.Trim();
                        strAction = "Module added";
                        strActivity = string.Format("Module titled '{0}' added.", asset.AssetName);

                        asset.Names = await SaveAssetNames(asset.AssetID, asset.Names);
                        asset.Details = await SaveAssetDetails(asset.AssetID, asset.Details);
                        asset.Contents = SaveAssetContents(asset.AssetID, asset.Contents);
                        await ActivityLogger.AddActivityLog(asset.AssetID, strAction, strActivity);
                    }
                    else
                    {
                        strAction = "Asset updated";
                        strActivity = string.Format("Asset titled '{0}' updated. ", asset.AssetName);

                        var assetObj = db.Asset.Where(x => x.AssetId == asset.AssetID && x.IsDeleted == false).FirstOrDefault();
                        if (assetObj != null)
                        {
                            strActivity += Convert.ToString(TempData["strActivity"]);
                            IsChanged = Convert.ToBoolean(TempData["IsChanged"]);

                            if (asset.IsDeleted == true)
                            {
                                IsChanged = true;
                                strAction = "Module deleted";
                                strActivity = string.Format("Module titled '{0}' deleted.", asset.AssetName);
                            }

                            asset.Names = await SaveAssetNames(asset.AssetID, asset.Names);
                            asset.Details = await SaveAssetDetails(asset.AssetID, asset.Details);
                            asset.Contents = await SaveAssetContentDetails(asset.AssetID, asset.Contents);

                            asset.AssetName = asset.Names[0].Name.Trim();
                            assetObj.AssetName = asset.Names[0].Name.Trim();
                            assetObj.Type = asset.AssetType;
                            assetObj.Status = asset.Status;
                            assetObj.Language = asset.Details[0].Language;
                            assetObj.Syllabus = asset.Details[0].Syllabus;
                            assetObj.Subject = asset.Details[0].Subject;
                            assetObj.Level = asset.Details[0].Grade;
                            assetObj.Topic = asset.Details[0].Topic;
                            assetObj.Subtopic = asset.Details[0].Subtopic;
                            assetObj.SubtopicDesc = asset.SubtopicDesc;
                            //assetObj.Tags = asset.Tags;
                            assetObj.Stream = asset.Details[0].Stream;
                            assetObj.PackageId = asset.Details[0].PackageId;
                            assetObj.IsQuizEnabled = asset.IsQuizEnabled;
                            assetObj.PercentageOfLabels = asset.PercentageOfLabels;
                            assetObj.NumberOfAttempts = asset.NumberOfAttempts;
                            assetObj.IsActive = asset.IsActive;
                            assetObj.ModifiedBy = new Guid(Session["UserID"].ToString());
                            assetObj.ModifiedByName = Session["Name"].ToString();
                            assetObj.ModifiedOn = DateTime.Now;
                            assetObj.IsDeleted = asset.IsDeleted;
                            db.Entry(assetObj).State = EntityState.Modified;
                            db.SaveChanges();

                            List<AssetTags> assetTags = db.AssetTags.Where(a => a.AssetId == asset.AssetID).ToList();
                            assetTags.ForEach((a) =>
                            {
                                db.AssetTags.Remove(a);
                            });
                            db.SaveChanges();

                            if (asset.NewTags != null && asset.NewTags.Length > 0)
                            {
                                CommonMethods.SaveAssetTags(assetObj.AssetId, asset.NewTags);
                            }

                            List<AssetProperties> assetProperties = db.AssetProperties.Where(a => a.AssetId == asset.AssetID).ToList();
                            assetProperties.ForEach((a) =>
                            {
                                db.AssetProperties.Remove(a);
                            });
                            db.SaveChanges();

                            if (asset.NewProperties != null && asset.NewProperties.Length > 0)
                            {
                                CommonMethods.SaveAssetProperties(assetObj.AssetId, asset.NewProperties);
                            }

                            await ActivityLogger.AddActivityLog(asset.AssetID, strAction, strActivity);

                        }
                    }

                    ViewBag.TagsSelectList = new SelectList(CommonMethods.GetAssetTags(asset.AssetID), "Value", "Text");
                    ViewBag.PropertiesSelectList = new SelectList(CommonMethods.GetAssetProperties(asset.AssetID), "Value", "Text");
                }
            }
            else
            {
                ViewBag.AddedType = addedType;
                return View(asset);
            }

            if (addedType == "Edit")
            {
                TempData["Message"] = "Module '" + asset.AssetName + "' updated successfully.";
            }
            else
            {
                TempData["Search"] = "Added";
                TempData["Message"] = "Module '" + asset.AssetName + "' added successfully.";
            }

            ViewBag.AddedType = addedType;
            return View(asset);
        }

        public async Task<List<AssetNameModel>> GetAssetNames(int assetID)
        {
            DB_Entities db = new DB_Entities();
            List<AssetNameModel> listNames = new List<AssetNameModel>();
            var names = await db.AssetNames.Where(x => x.AssetId == assetID && x.IsDeleted == false).OrderBy(x => x.Id).ToListAsync();

            listNames = names.Select(name => new AssetNameModel()
            {
                ID = name.Id,
                Name = name.Name,
                LanguageId = name.LanguageId,
                Language = db.CsNewLanguage.Where(x => x.NewLanguageid == name.LanguageId).Select(x => x.NewName).FirstOrDefault(),
                IsDeleted = name.IsDeleted
            }).ToList();

            return listNames;
        }

        public async Task<List<AssetDetailModel>> GetAssetDetails(int assetID)
        {
            DB_Entities db = new DB_Entities();
            List<AssetDetailModel> listDetails = new List<AssetDetailModel>();
            var details = await db.AssetDetails.Where(x => x.AssetId == assetID && x.IsDeleted == false).OrderBy(x => x.Id).ToListAsync();

            listDetails = details.Select(detail => new AssetDetailModel()
            {
                ID = detail.Id,
                PackageId = detail.PackageId,
                LanguageId = detail.LanguageId,
                GradeId = detail.GradeId,
                SyllabusId = detail.SyllabusId,
                StreamId = detail.StreamId,
                SubjectId = detail.SubjectId,
                TopicId = detail.TopicId,
                SubtopicId = detail.SubtopicId,
                Package = db.CsNewPackage.Where(x => x.NewPackageid == detail.PackageId).Select(x => x.NewName).FirstOrDefault(),
                Grade = db.CsNewGrade.Where(x => x.NewGradeid == detail.GradeId).Select(x => x.NewName).FirstOrDefault(),
                Language = db.CsNewLanguage.Where(x => x.NewLanguageid == detail.LanguageId).Select(x => x.NewName).FirstOrDefault(),
                Syllabus = db.CsNewSyllabus.Where(x => x.NewSyllabusid == detail.SyllabusId).Select(x => x.NewName).FirstOrDefault(),
                Stream = db.CsNewStream.Where(x => x.NewStreamid == detail.StreamId).Select(x => x.NewName).FirstOrDefault(),
                Subject = db.CsNewSubject.Where(x => x.NewSubjectid == detail.SubjectId).Select(x => x.NewName).FirstOrDefault(),
                Topic = db.CsNewTopic.Where(x => x.NewTopicid == detail.TopicId).Select(x => x.NewName).FirstOrDefault(),
                Subtopic = db.CsNewSubtopic.Where(x => x.NewSubtopicid == detail.SubtopicId).Select(x => x.NewName).FirstOrDefault(),
                IsDeleted = detail.IsDeleted
            }).ToList();

            return listDetails;
        }

        public async Task GetDataListFromSessions()
        {
            using (DB_Entities db = new DB_Entities())
            {
                ViewBag.Package = await db.CsNewPackage.Select(t => new SelectListItem { Text = t.NewName, Value = t.NewPackageid.ToString() }).ToListAsync();
                ViewBag.AssetType = await db.MstFileType.Where(x => x.IsDeleted == false && x.IsActive == true).Select(x => x.TypeName).ToListAsync();
                ViewBag.Status = await db.MstStatus.Where(x => x.IsDeleted == false && x.IsActive == true).Select(x => x.StatusName).ToListAsync();
                ViewBag.Tags = await db.MstTag.Where(x => x.IsDeleted == false && x.IsActive == true).Select(x => x.TagName).ToListAsync();
                ViewBag.Language = await db.CsNewLanguage.Select(x => new SelectListItem { Text = x.NewName, Value = x.NewLanguageid.ToString() }).ToListAsync();
                ViewBag.Platform = await db.CsNewPlatform.Select(x => new SelectListItem { Text = x.NewName, Value = x.NewPlatformid.ToString() }).ToListAsync();
                ViewBag.Activities = await db.MstActivity.Where(x => x.IsDeleted == false && x.IsActive == true).ToListAsync();
            }
        }

        public JsonResult GetLanguagesAndGrades(string Id)
        {
            LanguageAndGradeList lst = new LanguageAndGradeList();
            using (DB_Entities db = new DB_Entities())
            {
                lst.Languages = (from pl in db.CsNewPackageLanguage
                                 join l in db.CsNewLanguage on pl.NewLanguageid equals l.NewLanguageid
                                 where pl.NewPackageid == Guid.Parse(Id)
                                 select new DataList { Text = l.NewName, Value = l.NewLanguageid }).Distinct().ToList();

                lst.Grades = (from pg in db.CsNewPackageGrade
                              join g in db.CsNewGrade on pg.NewGradeid equals g.NewGradeid
                              where pg.NewPackageid == Guid.Parse(Id)
                              select new DataList { Text = g.NewName, Value = g.NewGradeid }).Distinct().ToList();
            }

            return Json(lst, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetSyllabus(Guid Id)
        {
            SyllabusList lst = new SyllabusList();
            using (DB_Entities db = new DB_Entities())
            {
                lst.Syllabus = (from gs in db.CsNewGradeSyllabus
                                join cs in db.CsNewSyllabus on gs.NewSyllabusid equals cs.NewSyllabusid
                                where gs.NewGradeid == Id
                                select new DataList { Text = cs.NewName, Value = cs.NewSyllabusid }).Distinct().ToList();

                lst.Streams = (from gs in db.CsNewGradeStream
                               join cs in db.CsNewStream on gs.NewStreamid equals cs.NewStreamid
                               where gs.NewGradeid == Id
                               select new DataList { Text = cs.NewName, Value = cs.NewStreamid }).Distinct().ToList();

                lst.Subjects = (from gs in db.CsNewGradeSubject
                                join cs in db.CsNewSubject on gs.NewSubjectid equals cs.NewSubjectid
                                where gs.NewGradeid == Id
                                select new DataList { Text = cs.NewName, Value = cs.NewSubjectid }).Distinct().ToList();
            }

            return Json(lst, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetTopics(Guid Id)
        {
            TopicList lst = new TopicList();
            using (DB_Entities db = new DB_Entities())
            {
                lst.Topics = (from st in db.CsNewSubjectTopic
                              join t in db.CsNewTopic on st.NewTopicid equals t.NewTopicid
                              where st.NewSubjectid == Id
                              select new DataList { Text = t.NewName, Value = t.NewTopicid }).Distinct().ToList();

            }

            return Json(lst, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetSubtopics(Guid Id)
        {
            SubtopicList lst = new SubtopicList();
            using (DB_Entities db = new DB_Entities())
            {
                lst.Subtopics = (from ts in db.CsNewTopicSubtopic
                                 join st in db.CsNewSubtopic on ts.NewSubtopicid equals st.NewSubtopicid
                                 where ts.NewTopicid == Id
                                 select new DataList { Text = st.NewName, Value = st.NewSubtopicid }).Distinct().ToList();

            }

            return Json(lst, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetDescription(int Id)
        {
            DB_Entities db = new DB_Entities();
            List<ContentDescriptionModel> desc = new List<ContentDescriptionModel>();
            var details = db.AssetContentDetail.Where(x => x.AssetContentId == Id && x.IsDeleted == false).OrderBy(x => x.Id).ToList();

            if (details.Count != 0)
            {
                desc = details.Select(detail => new ContentDescriptionModel()
                {
                    ID = detail.Id,
                    AssetContentID = detail.AssetContentId ?? 0,
                    Description = detail.Description,
                    WhatIsNew = detail.WhatIsNew,
                    LanguageId = detail.LanguageId,
                    Language = db.CsNewLanguage.Where(x => x.NewLanguageid == detail.LanguageId).Select(x => x.NewName).FirstOrDefault(),
                    IsDeleted = detail.IsDeleted ?? false
                }).ToList();
            }
            else
            {
                ContentDescriptionModel detail = new ContentDescriptionModel();
                detail.AssetContentID = Id;
                desc.Add(detail);
            }

            return PartialView("_Description", desc);
        }

        public ActionResult GetWhatsNew(int Id)
        {
            DB_Entities db = new DB_Entities();
            List<ContentDescriptionModel> desc = new List<ContentDescriptionModel>();
            var details = db.AssetContentDetail.Where(x => x.AssetContentId == Id && x.IsEnabled == false).OrderBy(x => x.Id).ToList();

            if (details.Count != 0)
            {
                desc = details.Select(detail => new ContentDescriptionModel()
                {
                    ID = detail.Id,
                    AssetContentID = detail.AssetContentId ?? 0,
                    Description = detail.Description,
                    WhatIsNew = detail.WhatIsNew,
                    LanguageId = detail.LanguageId,
                    Language = db.CsNewLanguage.Where(x => x.NewLanguageid == detail.LanguageId).Select(x => x.NewName).FirstOrDefault(),
                    IsDeleted = detail.IsEnabled ?? false,
                }).ToList();
            }
            else
            {
                ContentDescriptionModel detail = new ContentDescriptionModel();
                detail.AssetContentID = Id;
                desc.Add(detail);
            }
            return PartialView("_WhatsNew", desc);
        }

        [HttpPost]
        public string SaveDescription(List<ContentDescriptionModel> details)
        {
            string message = string.Empty;
            int num = details.Where(d => d.IsDeleted == false && (d.LanguageId == Guid.Empty || string.IsNullOrWhiteSpace(d.Description))).ToList().Count();
            if (num > 0)
            {
                message = "Error";
            }
            else
            {
                DB_Entities db = new DB_Entities();
                int contentId = details[0].AssetContentID;
                db.AssetContentDetail.Where(w => w.AssetContentId == contentId && w.IsDeleted == false).ToList().ForEach(i => i.IsDeleted = true);
                db.SaveChanges();

                foreach (ContentDescriptionModel detail in details)
                {
                    var descObj = db.AssetContentDetail.Where(x => x.AssetContentId == contentId && x.LanguageId == detail.LanguageId).FirstOrDefault();

                    if (descObj == null && detail.ID == 0)
                    {
                        if (detail.LanguageId != Guid.Empty && !string.IsNullOrWhiteSpace(detail.Description))
                        {
                            AssetContentDetail newDesc = new AssetContentDetail()
                            {
                                AssetContentId = contentId,
                                Description = detail.Description,
                                LanguageId = detail.LanguageId,
                                //LanguageShortName = db.CsNewLanguage.Where(x => x.NewLanguageid == detail.LanguageId).Select(x => x.NewLanguageshortname).FirstOrDefault(),
                                IsDeleted = detail.IsDeleted,
                                CreatedBy = new Guid(Session["UserID"].ToString()),
                                CreatedOn = DateTime.Now,
                                IsEnabled = true
                            };
                            db.AssetContentDetail.Add(newDesc);
                            db.SaveChanges();

                            detail.ID = newDesc.Id;
                        }
                    }
                    else
                    {
                        descObj.Description = detail.Description;
                        descObj.LanguageId = detail.LanguageId;
                        descObj.IsDeleted = detail.IsDeleted;
                        descObj.ModifiedBy = new Guid(Session["UserID"].ToString());
                        descObj.ModifiedOn = DateTime.Now;
                        db.SaveChanges();
                    }
                }

                message = "Description saved successfully";
            }
            return message;
        }

        [HttpPost]
        public string SaveWhatIsNew(List<ContentDescriptionModel> details)
        {
            string message = string.Empty;
            int num = details.Where(d => d.IsDeleted == false && (d.LanguageId == Guid.Empty || string.IsNullOrWhiteSpace(d.WhatIsNew))).ToList().Count();
            if (num > 0)
            {
                message = "Error";
            }
            else
            {
                DB_Entities db = new DB_Entities();
                int contentId = details[0].AssetContentID;
                db.AssetContentDetail.Where(w => w.AssetContentId == contentId && w.IsEnabled == false).ToList().ForEach(i => i.IsEnabled = true);
                db.SaveChanges();

                foreach (ContentDescriptionModel detail in details)
                {
                    var descObj = db.AssetContentDetail.Where(x => x.AssetContentId == contentId && x.LanguageId == detail.LanguageId).FirstOrDefault();

                    if (descObj == null && detail.ID == 0)
                    {
                        if (detail.LanguageId != Guid.Empty && !string.IsNullOrWhiteSpace(detail.WhatIsNew)) {
                            AssetContentDetail newDesc = new AssetContentDetail()
                            {
                                AssetContentId = contentId,
                                WhatIsNew = detail.WhatIsNew,
                                LanguageId = detail.LanguageId,
                                //LanguageShortName = db.CsNewLanguage.Where(x => x.NewLanguageid == detail.LanguageId).Select(x => x.NewLanguageshortname).FirstOrDefault(),
                                IsDeleted = true,
                                IsEnabled = detail.IsDeleted,
                                CreatedBy = new Guid(Session["UserID"].ToString()),
                                CreatedOn = DateTime.Now
                            };
                            db.AssetContentDetail.Add(newDesc);
                            db.SaveChanges();

                            detail.ID = newDesc.Id;
                        }
                    }
                    else
                    {
                        descObj.WhatIsNew = detail.WhatIsNew;
                        descObj.LanguageId = detail.LanguageId;
                        descObj.IsEnabled = detail.IsDeleted;
                        descObj.ModifiedBy = new Guid(Session["UserID"].ToString());
                        descObj.ModifiedOn = DateTime.Now;
                        db.SaveChanges();
                    }
                }

                message = "Comments saved successfully";
            }
            return message;
        }

        public async Task<List<AssetContentModel>> GetAssetContentList(int assetID)
        {
            DB_Entities db = new DB_Entities();
            List<AssetContentModel> listContents = new List<AssetContentModel>();
            var contents = await db.AssetContent.Where(x => x.IsDeleted == false && x.AssetId == assetID).OrderBy(x => x.Id).ToListAsync();

            listContents = contents.Select(content => new AssetContentModel()
            {
                ID = content.Id,
                AssetID = content.AssetId,
                FileName = content.FileName,
                FilePath = content.FilePath,
                FileSize = content.FileSize,
                FileSizeText = CommonMethods.GetBytesReadable(Convert.ToInt64(content.FileSize)),
                Thumbnail = content.Thumbnail,
                IsDirectory = content.IsDirectory,
                VersionNumber = content.VersionNumber,
                //PublishFolder = asset.PackageId,
                PlatformId = content.Platform,
                Platform = db.CsNewPlatform.Where(x => x.NewPlatformid == Guid.Parse(content.Platform)).Select(x => x.NewName).FirstOrDefault(),
                IsEnabled = Convert.ToBoolean(content.IsEnabled != null ? content.IsEnabled : false),
                IsDeleted = Convert.ToBoolean(content.IsDeleted != null ? content.IsDeleted : false),
                CreatedBy = content.CreatedBy != null ? content.CreatedBy.Value : new Guid(),
                CreatedOn = content.CreatedOn != null ? content.CreatedOn.Value : DateTime.UtcNow,
                ModifiedBy = content.ModifiedBy != null ? content.ModifiedBy.Value : new Guid(),
                ModifiedOn = content.ModifiedOn != null ? content.ModifiedOn.Value : DateTime.UtcNow
            }).ToList();

            return listContents;
        }

        public List<AssetContentModel> SaveAssetContents(int assetID, List<AssetContentModel> contents)
        {
            DB_Entities db = new DB_Entities();
            foreach (AssetContentModel content in contents)
            {
                if (content.ID == 0 && content.IsDeleted == false && !string.IsNullOrWhiteSpace(content.FileName))
                {
                    var prevContent = db.AssetContent.Where(x => x.AssetId == assetID).Include(x => x.XmlcontentModel).OrderByDescending(x => x.Id).FirstOrDefault();
                    content.VersionNumber = db.AssetContent.Where(x => x.AssetId == assetID).Count() + 1;
                    AssetContent newContent = new AssetContent()
                    {
                        AssetId = assetID,
                        FileName = content.FileName,
                        FilePath = content.FileName,
                        FileSize = content.FileSize,
                        IsDirectory = content.IsDirectory,
                        VersionNumber = content.VersionNumber,
                        Platform = content.PlatformId,
                        Thumbnail = prevContent?.Thumbnail,
                        IsEnabled = false,
                        CreatedBy = new Guid(Session["UserID"].ToString()),
                        CreatedOn = DateTime.Now,
                        IsDeleted = false
                    };
                    db.AssetContent.Add(newContent);
                    db.SaveChanges();

                    content.ID = newContent.Id;
                }
                else
                {
                    var contentObj = db.AssetContent.Where(x => x.Id == content.ID && x.IsDeleted == false).FirstOrDefault();
                    if (contentObj != null)
                    {
                        contentObj.VersionNumber = content.VersionNumber;
                        contentObj.IsEnabled = content.IsEnabled;
                        contentObj.ModifiedBy = new Guid(Session["UserID"].ToString());
                        contentObj.ModifiedOn = DateTime.Now;
                        contentObj.IsDeleted = content.IsDeleted;
                        contentObj.Platform = content.PlatformId;
                        db.SaveChanges();
                    }
                }
            }

            return contents;
        }

        public async Task<List<AssetNameModel>> SaveAssetNames(int assetID, List<AssetNameModel> names)
        {
            DB_Entities db = new DB_Entities();

            db.AssetNames.Where(w => w.AssetId == assetID && w.IsDeleted == false).ToList().ForEach(i => i.IsDeleted = true);
            db.SaveChanges();

            foreach (AssetNameModel name in names)
            {
                if (name.ID == 0 && name.IsDeleted == false)
                {
                    AssetNames newName = new AssetNames()
                    {
                        AssetId = assetID,
                        Name = name.Name,
                        LanguageId = name.LanguageId,
                        IsDeleted = name.IsDeleted
                    };
                    db.AssetNames.Add(newName);
                    db.SaveChanges();

                    name.ID = newName.Id;
                }
                else
                {
                    var nameObj = await db.AssetNames.Where(x => x.Id == name.ID).FirstOrDefaultAsync();
                    if (nameObj != null)
                    {
                        nameObj.Name = name.Name;
                        nameObj.LanguageId = name.LanguageId;
                        nameObj.IsDeleted = name.IsDeleted;
                        db.SaveChanges();
                    }
                }
            }

            List<AssetNameModel> newNames = names.Where(d => d.IsDeleted == false).ToList();
            return newNames;
        }

        public async Task<List<AssetDetailModel>> SaveAssetDetails(int assetID, List<AssetDetailModel> details)
        {
            DB_Entities db = new DB_Entities();

            db.AssetDetails.Where(w => w.AssetId == assetID && w.IsDeleted == false).ToList().ForEach(i => i.IsDeleted = true);
            db.SaveChanges();

            foreach (AssetDetailModel detail in details)
            {
                if (detail.ID == 0 && detail.IsDeleted == false)
                {
                    AssetDetails newDetail = new AssetDetails()
                    {
                        AssetId = assetID,
                        PackageId = detail.PackageId,
                        LanguageId = detail.LanguageId,
                        GradeId = detail.GradeId,
                        SyllabusId = detail.SyllabusId,
                        StreamId = detail.StreamId,
                        SubjectId = detail.SubjectId,
                        TopicId = detail.TopicId,
                        SubtopicId = detail.SubtopicId,
                        IsDeleted = detail.IsDeleted
                    };
                    db.AssetDetails.Add(newDetail);
                    db.SaveChanges();

                    detail.ID = newDetail.Id;
                }
                else
                {
                    var detailObj = await db.AssetDetails.Where(x => x.Id == detail.ID).FirstOrDefaultAsync();
                    if (detailObj != null)
                    {
                        detailObj.PackageId = detail.PackageId;
                        detailObj.LanguageId = detail.LanguageId;
                        detailObj.GradeId = detail.GradeId;
                        detailObj.SyllabusId = detail.SyllabusId;
                        detailObj.StreamId = detail.StreamId;
                        detailObj.SubjectId = detail.SubjectId;
                        detailObj.TopicId = detail.TopicId;
                        detailObj.SubtopicId = detail.SubtopicId;
                        detailObj.IsDeleted = detail.IsDeleted;
                        db.SaveChanges();
                    }
                }
            }

            List<AssetDetailModel> newDetails = details.Where(d => d.IsDeleted == false).ToList();
            return newDetails;
        }

        public async Task<List<AssetContentModel>> SaveAssetContentDetails(int assetID, List<AssetContentModel> contents)
        {
            DB_Entities db = new DB_Entities();
            foreach (AssetContentModel content in contents)
            {
                if (content.ID == 0 && content.IsDeleted == false && !string.IsNullOrWhiteSpace(content.FileName))
                {
                    var prevContent = db.AssetContent.Where(x => x.AssetId == assetID && x.IsEnabled == false).Include(x => x.XmlcontentModel).OrderByDescending(x => x.Id).FirstOrDefault();
                    content.VersionNumber = db.AssetContent.Where(x => x.AssetId == assetID).Count() + 1;
                    AssetContent newContent = new AssetContent()
                    {
                        AssetId = assetID,
                        FileName = content.FileName,
                        FilePath = content.FileName,
                        FileSize = content.FileSize,
                        IsDirectory = content.IsDirectory,
                        VersionNumber = content.VersionNumber,
                        Platform = content.PlatformId,
                        Thumbnail = prevContent?.Thumbnail,
                        IsEnabled = false,
                        CreatedBy = new Guid(Session["UserID"].ToString()),
                        CreatedOn = DateTime.Now,
                        IsDeleted = false
                    };
                    db.AssetContent.Add(newContent);
                    db.SaveChanges();

                    content.ID = newContent.Id;
                    if (prevContent != null) {
                        await SaveDescriptionWhatsNew(assetID, prevContent.Id, newContent.Id);
                    }
                }
                else
                {
                    var contentObj = db.AssetContent.Where(x => x.Id == content.ID && x.IsDeleted == false).FirstOrDefault();
                    if (contentObj != null)
                    {
                        contentObj.VersionNumber = content.VersionNumber;
                        contentObj.IsEnabled = content.IsEnabled;
                        contentObj.ModifiedBy = new Guid(Session["UserID"].ToString());
                        contentObj.ModifiedOn = DateTime.Now;
                        contentObj.IsDeleted = content.IsDeleted;
                        contentObj.Platform = content.PlatformId;
                        db.SaveChanges();
                    }
                }
            }

            return contents;
        }

        public async Task SaveDescriptionWhatsNew(int AssetId, int prevContentId, int newContentId)
        {
            using (DB_Entities db = new DB_Entities())
            {
                var details = await db.AssetContentDetail.Where(x => x.AssetContentId == prevContentId && (x.IsDeleted == false || x.IsEnabled == false)).OrderBy(x => x.Id).ToListAsync();

                foreach (var detail in details)
                {
                    AssetContentDetail record = new AssetContentDetail()
                    {
                        AssetContentId = newContentId,
                        Description = detail.Description,
                        WhatIsNew = detail.WhatIsNew,
                        LanguageId = detail.LanguageId,
                        LanguageShortName = detail.LanguageShortName,
                        IsDeleted = detail.IsDeleted,
                        IsEnabled = detail.IsEnabled,
                        CreatedBy = new Guid(Session["UserID"].ToString()),
                        CreatedOn = DateTime.Now
                    };

                    db.AssetContentDetail.Add(record);
                    db.SaveChanges();
                }
            }
        }

        #region Publish Module
        public async Task<bool> PublishModule(int Id)
        {
            bool status = false;
            using (DB_Entities db = new DB_Entities())
            {
                var assetObj = db.AssetContent.Where(x => x.Id == Id && x.IsDeleted == false).Include(x => x.Asset).FirstOrDefault();
                if (assetObj != null)
                {
                    assetObj.IsEnabled = true;
                    assetObj.ModifiedOn = DateTime.Now;
                    assetObj.ModifiedBy = new Guid(Session["UserID"].ToString());
                    db.Entry(assetObj).State = EntityState.Modified;
                    await db.SaveChangesAsync();


                    string strAction = "Module published";
                    string strActivity = string.Format("Content titled '{0}' has been published for Module titled '{0}'.", assetObj.FileName, assetObj.Asset.AssetName);
                    await ActivityLogger.AddActivityLog(Id, strAction, strActivity);

                    status = true;
                }
            }


            return status;
        }
        #endregion

        #region Unpublish Module
        public async Task<bool> UnpublishModule(int Id)
        {
            bool status = false;
            using (DB_Entities db = new DB_Entities())
            {
                var assetObj = db.AssetContent.Where(x => x.Id == Id && x.IsDeleted == false).Include(x => x.Asset).FirstOrDefault();
                if (assetObj != null)
                {
                    assetObj.IsEnabled = false;
                    assetObj.ModifiedOn = DateTime.Now;
                    assetObj.ModifiedBy = new Guid(Session["UserID"].ToString());
                    db.Entry(assetObj).State = EntityState.Modified;
                    await db.SaveChangesAsync();


                    string strAction = "Module unpublished";
                    string strActivity = string.Format("Content titled '{0}' has been unpublished for Module titled '{0}'.", assetObj.FileName, assetObj.Asset.AssetName);
                    await ActivityLogger.AddActivityLog(Id, strAction, strActivity);

                    status = true;
                }
            }

            return status;
        }
        #endregion
    }
}