﻿using _3DLContentManagement.App_Start;
using _3DLContentManagement.Common;
using _3DLContentManagement.Database;
using _3DLContentManagement.Models;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using TDL.ContentPlatform.Server.Common.Models.Communication;
using TDL.ContentPlatform.Server.Database.Tables;
using static _3DLContentManagement.Common.HttpRestClient;

namespace _3DLContentManagement.Controllers
{
    [Authorize]
    public class UserManagementController : Controller
    {
        // GET: UserManagement
        [RoleAuthorize]
        [SessionExpire]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetAllUsers([DataSourceRequest] DataSourceRequest request)
        {
            List<RegisterViewModel> listUsers = DataAccess.GetUserList();
            var jsonResult = Json(listUsers.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

        [RoleAuthorize]
        [SessionExpire]
        public ActionResult Register(Guid UserId)
        {
            RegisterViewModel user = new RegisterViewModel();
            List<UserDetailModel> listDetails = new List<UserDetailModel>();
            using (DB_Entities db = new DB_Entities())
            {
                ViewBag.Accounts = (from a in db.CsAccount
                                    join o in db.CsOpportunity on a.Accountid equals o.Parentaccountid
                                    join os in db.CsNewOpportunitySubscriptionMapping on o.Opportunityid equals os.NewOpportunityid
                                    select new SelectListItem { Text = a.Name, Value = a.Accountid.ToString() }).Distinct().OrderBy(x => x.Text).ToList();

                //ViewBag.Accounts = db.CsAccount.Select(x => new SelectListItem { Text = x.Name, Value = x.Accountid.ToString() }).OrderBy(x => x.Text).ToList();
                var roles = DataAccess.GetRoles();
                ViewBag.Roles = roles.Select(x => new SelectListItem { Text = x.Name, Value = x.Id.ToString() }).OrderBy(x => x.Text).ToList();

                if (UserId == Guid.Empty)
                {
                    user.Password = CommonMethods.GenerateRandomPassword();

                    UserDetailModel detail = new UserDetailModel();
                    detail.ExpiryDate = DateTime.Now;
                    listDetails.Add(detail);

                    ViewBag.AddedType = "Add";
                }
                else
                {
                    user = DataAccess.GetUserDetails(UserId);

                    //Get user details (Subscription, Package, Grade, Subject, etc)
                    listDetails = GetUserDetails(UserId);
                    if (listDetails.Count == 0)
                    {
                        listDetails.Add(new UserDetailModel { ExpiryDate = DateTime.Now });
                    }

                    ViewBag.AddedType = "Edit";
                }

                user.Platforms = db.CsNewPlatform.Select(x => new SelectListItem { Text = x.NewName, Value = x.Id.ToString() }).OrderBy(x => x.Text).ToList();

                user.Details = listDetails;
            }

            return View(user);
        }

        [HttpPost]
        public ActionResult Register(RegisterViewModel user)
        {
            using (DB_Entities db = new DB_Entities())
            {
                //Copying the email value to username if username value is null or empty
                if (string.IsNullOrEmpty(user.Username))
                {
                    user.Username = user.Email;
                }

                if (user.UserID == Guid.Empty)
                {
                    var host = WebConfigurationManager.AppSettings["HostApi"].ToString();
                    var admin = WebConfigurationManager.AppSettings["ApiAdmin"].ToString();
                    var password = WebConfigurationManager.AppSettings["ApiAdminPassword"].ToString();
                    var loggedInUser = new Guid(Session["UserID"].ToString());

                    var loginRequest = new AccountLoginRequest() { Username = admin, Password = password };
                    var loginResponse = DoHttp<AccountLoginRequest, AccountLoginResponse>($"{host}/api/v1/Account/Login", HttpMethods.GET, loginRequest);

                    var request = new AccountRegisterRequest() { Firstname = user.FirstName, Lastname = user.LastName, Username = user.Username, Password = user.Password, Email = user.Email, Account = user.AccountId };
                    request.Roles = new string[] { user.Role };

                    var response = DoHttp<AccountRegisterRequest, AccountRegisterResponse>($"{host}/api/v1/Account/Register", HttpMethods.POST, request, loginResponse.AuthorizationToken);

                    if (response.Success)
                    {
                        user.UserID = response.UserId;
                        SaveUserDetails(user);
                        TempData["Message"] = "User registered successfully!";
                        return RedirectToAction("Index", "UserManagement");
                    }
                    else
                    {
                        TempData["Message"] = "Error: " + response.Errors[0].Description;// + response.ErrorMessage;
                    }
                }
                else
                {
                    var status = DataAccess.UpdateUserDetails(user);

                    if (status)
                    {
                        user.Details = AddUpdateUserDetails(user.UserID, user.AccountId, user.Details);
                        TempData["Message"] = "User details for '" + user.FirstName.Trim() + "' updated successfully!";
                        return RedirectToAction("Index", "UserManagement");
                    }
                }

                //ViewBag.Accounts = db.CsAccount.Select(x => new SelectListItem { Text = x.Name, Value = x.Accountid.ToString() }).OrderBy(x => x.Text).ToList();
                ViewBag.Accounts = (from a in db.CsAccount
                                    join o in db.CsOpportunity on a.Accountid equals o.Parentaccountid
                                    join os in db.CsNewOpportunitySubscriptionMapping on o.Opportunityid equals os.NewOpportunityid
                                    select new SelectListItem { Text = a.Name, Value = a.Accountid.ToString() }).Distinct().OrderBy(x => x.Text).ToList();
                var roles = DataAccess.GetRoles();
                ViewBag.Roles = roles.Select(x => new SelectListItem { Text = x.Name, Value = x.Id.ToString() }).OrderBy(x => x.Text).ToList();
            }

            return View(user);
        }

        public int ValidateEmail(string Email, string UserId)
        {
            var count = DataAccess.ValidateEmail(Email, UserId);
            return count;
        }

        public List<UserDetailModel> GetUserDetails(Guid UserId)
        {
            DB_Entities db = new DB_Entities();
            List<UserDetailModel> listDetails = new List<UserDetailModel>();
            var details = db.UserDetails.Where(x => x.UserId == UserId && x.IsDeleted == false).OrderBy(x => x.Id).ToList();

            foreach (var detail in details)
            {
                UserDetailModel userDetail = new UserDetailModel();

                userDetail.ID = detail.Id;
                userDetail.SubscriptionId = detail.SubscriptionId;
                userDetail.PackageId = detail.PackageId;
                userDetail.LanguageId = detail.LanguageId;
                userDetail.GradeId = detail.GradeId;
                userDetail.SyllabusId = detail.SyllabusId;
                userDetail.StreamId = detail.StreamId;
                userDetail.SubjectId = detail.SubjectId;
                userDetail.TopicId = detail.TopicId;
                userDetail.Subscription = db.CsNewSubscription.Where(x => x.Id == detail.SubscriptionId).Select(x => x.NewName).FirstOrDefault();
                userDetail.Package = db.CsNewPackage.Where(x => x.NewPackageid == detail.PackageId).Select(x => x.NewName).FirstOrDefault();
                userDetail.Grade = db.CsNewGrade.Where(x => x.NewGradeid == detail.GradeId).Select(x => x.NewName).FirstOrDefault();
                userDetail.Language = db.CsNewLanguage.Where(x => x.NewLanguageid == detail.LanguageId).Select(x => x.NewName).FirstOrDefault();
                userDetail.Syllabus = db.CsNewSyllabus.Where(x => x.NewSyllabusid == detail.SyllabusId).Select(x => x.NewName).FirstOrDefault();
                userDetail.Stream = db.CsNewStream.Where(x => x.NewStreamid == detail.StreamId).Select(x => x.NewName).FirstOrDefault();
                userDetail.Subject = db.CsNewSubject.Where(x => x.NewSubjectid == detail.SubjectId).Select(x => x.NewName).FirstOrDefault();
                userDetail.Topic = db.CsNewTopic.Where(x => x.NewTopicid == detail.TopicId).Select(x => x.NewName).FirstOrDefault();
                userDetail.ExpiryDate = detail.dtSubscriptionEndDate ?? DateTime.Now;
                userDetail.IsActive = detail.IsActive;
                userDetail.IsDeleted = detail.IsDeleted;

                userDetail.UserDetailIds = db.UserDetailPlatform.Where(X => X.UserDetailId == detail.Id).Select(X => X.PlatformId.ToString()).ToArray();                

                listDetails.Add(userDetail);
            }
            return listDetails;
        }
        
        public void SaveUserDetails(RegisterViewModel user)
        {
            DB_Entities db = new DB_Entities();

            int duration = 0; bool IsDemoUser = false; string language = string.Empty;
            int count = 0;
            DateTime expiryDate = DateTime.Now;

            db.UserDetails.Where(w => w.UserId == user.UserID && w.IsDeleted == false).ToList().ForEach(i => i.IsDeleted = true);
            db.SaveChanges();

            foreach (UserDetailModel detail in user.Details)
            {
                //var subscriptionId = (from o in db.CsOpportunity
                //                     join os in db.CsNewOpportunitySubscriptionMapping on o.Id equals os.NewOpportunityid
                //                     join s in db.CsNewSubscription on os.NewSubscriptionid equals s.Id
                //                     join sp in db.CsNewSubscriptionPackage on s.Id equals sp.NewSubscriptionid
                //                     where o.Parentaccountid == AccountId && sp.NewPackageid == detail.PackageId
                //                     select s.Id).FirstOrDefault();

                if (detail.ID == 0 && detail.IsDeleted == false && detail.SubscriptionId != Guid.Empty)
                {
                    //var sub = db.CsNewSubscription.Where(x => x.Id == detail.SubscriptionId).FirstOrDefault();
                    //duration = sub.NewDuration ?? 0;

                    var endDate = Convert.ToDateTime(detail.ExpiryDate.ToString());
                    expiryDate = endDate;

                    UserDetails newDetail = new UserDetails()
                    {
                        UserId = user.UserID,
                        SubscriptionId = detail.SubscriptionId,
                        PackageId = detail.PackageId,
                        LanguageId = detail.LanguageId,
                        GradeId = detail.GradeId,
                        SyllabusId = detail.SyllabusId,
                        StreamId = detail.StreamId,
                        SubjectId = detail.SubjectId,
                        TopicId = detail.TopicId,
                        dtSubscriptionStartDate = DateTime.Today,
                        dtSubscriptionEndDate = endDate, //DateTime.Today.AddDays(duration),
                        IsSubscriptionExpired = false,
                        CreatedBy = new Guid(Session["UserID"].ToString()),
                        CreatedOn = DateTime.UtcNow,
                        IsActive = true,
                        IsDeleted = detail.IsDeleted
                    };
                    db.UserDetails.Add(newDetail);
                    db.SaveChanges();

                    //Add/Update User Detail Platform    
                    if (detail.UserDetailIds != null)
                    {
                        foreach (var platformId in detail.UserDetailIds)
                        {
                            UserDetailPlatform newPlatform = new UserDetailPlatform()
                            {
                                UserDetailId = newDetail.Id,
                                PlatformId = new Guid(platformId),
                                CreatedBy = new Guid(Session["UserID"].ToString()),
                                CreatedOn = DateTime.UtcNow
                            };
                            db.UserDetailPlatform.Add(newPlatform);
                        }

                        db.SaveChanges();
                    }

                    detail.ID = newDetail.Id;

                    if (detail.Subscription.ToLower().Contains("demo"))
                    {
                        IsDemoUser = true;
                    }

                    //To get the language
                    if (!string.IsNullOrEmpty(detail.Language))
                    {
                        language = detail.Language;
                    }
                    else if (!string.IsNullOrEmpty(detail.Package))
                    {
                        language = GetLanguageFromPackage(detail.PackageId);
                    }
                    else
                    {
                        language = GetLanguageFromSubscription(detail.SubscriptionId);
                    }

                    count++;
                }
            }

            //&& user.Account.ToLower().Contains("demo")
            if (count == 0)
            {
                var subs = (from o in db.CsOpportunity
                            join os in db.CsNewOpportunitySubscriptionMapping on o.Id equals os.NewOpportunityid
                            join s in db.CsNewSubscription on os.NewSubscriptionid equals s.Id
                            where o.Parentaccountid == user.AccountId
                            select new DataList { Text = s.NewName, Value = s.Id }).Distinct().ToList();

                if (subs != null && (subs.Count == 1 || user.Account.ToLower().Contains("demo")))
                {
                    var demoSub = subs.Where(x => x.Text.ToLower().Contains("demo")).FirstOrDefault();

                    if (demoSub == null && user.Account.ToLower().Contains("demo"))
                    {
                        demoSub = subs.FirstOrDefault();
                    }

                    if (demoSub != null)
                    {
                        IsDemoUser = true;
                        duration = db.CsNewSubscription.Where(x => x.Id == demoSub.Value).Select(x => x.NewDuration).FirstOrDefault() ?? 0;
                        expiryDate = DateTime.Today.AddDays(duration);
                        UserDetails newDetail = new UserDetails()
                        {
                            UserId = user.UserID,
                            SubscriptionId = demoSub.Value ?? Guid.Empty,
                            PackageId = Guid.Empty,
                            LanguageId = Guid.Empty,
                            GradeId = Guid.Empty,
                            SyllabusId = Guid.Empty,
                            StreamId = Guid.Empty,
                            SubjectId = Guid.Empty,
                            TopicId = Guid.Empty,
                            dtSubscriptionStartDate = DateTime.Today,
                            dtSubscriptionEndDate = expiryDate,
                            IsSubscriptionExpired = false,
                            CreatedBy = new Guid(Session["UserID"].ToString()),
                            CreatedOn = DateTime.UtcNow,
                            IsActive = true,
                            IsDeleted = false
                        };
                        db.UserDetails.Add(newDetail);
                        db.SaveChanges();

                        var subDetail = user.Details.FirstOrDefault(X => X.SubscriptionId == demoSub.Value);

                        //Add/Update User Detail Platform    
                        if (subDetail != null && subDetail.UserDetailIds != null)
                        {
                            foreach (var platformId in subDetail.UserDetailIds)
                            {
                                UserDetailPlatform newPlatform = new UserDetailPlatform()
                                {
                                    UserDetailId = newDetail.Id,
                                    PlatformId = new Guid(platformId),
                                    CreatedBy = new Guid(Session["UserID"].ToString()),
                                    CreatedOn = DateTime.UtcNow
                                };
                                db.UserDetailPlatform.Add(newPlatform);
                            }

                            db.SaveChanges();
                        }

                        if (string.IsNullOrEmpty(language))
                        {
                            language = GetLanguageFromSubscription(demoSub.Value ?? Guid.Empty);
                        }
                    }

                    //if (string.IsNullOrEmpty(language))
                    //{
                    //    language = GetLanguageFromSubscription(subs.FirstOrDefault().Value ?? Guid.Empty);
                    //}
                }
                else if (subs != null && subs.Count > 0)
                {
                    language = GetLanguageFromSubscription(subs.FirstOrDefault().Value ?? Guid.Empty);

                    foreach (var item in subs)
                    {
                        if (item.Value != Guid.Empty)
                        {
                            var sub = db.CsNewSubscription.Where(x => x.Id == item.Value).FirstOrDefault();
                            duration = sub.NewDuration ?? 0;
                            expiryDate = DateTime.Today.AddDays(duration);

                            UserDetails newDetail = new UserDetails()
                            {
                                UserId = user.UserID,
                                SubscriptionId = sub.NewSubscriptionid ?? Guid.Empty,
                                PackageId = Guid.Empty,
                                LanguageId = Guid.Empty,
                                GradeId = Guid.Empty,
                                SyllabusId = Guid.Empty,
                                StreamId = Guid.Empty,
                                SubjectId = Guid.Empty,
                                TopicId = Guid.Empty,
                                dtSubscriptionStartDate = DateTime.Today,
                                dtSubscriptionEndDate = expiryDate,
                                IsSubscriptionExpired = false,
                                CreatedBy = new Guid(Session["UserID"].ToString()),
                                CreatedOn = DateTime.UtcNow,
                                IsActive = true,
                                IsDeleted = false
                            };
                            db.UserDetails.Add(newDetail);
                            db.SaveChanges();

                            var subDetail = user.Details.FirstOrDefault(X => X.SubscriptionId == item.Value);

                            //Add/Update User Detail Platform    
                            if (subDetail != null && subDetail.UserDetailIds != null)
                            {
                                foreach (var platformId in subDetail.UserDetailIds)
                                {
                                    UserDetailPlatform newPlatform = new UserDetailPlatform()
                                    {
                                        UserDetailId = newDetail.Id,
                                        PlatformId = new Guid(platformId),
                                        CreatedBy = new Guid(Session["UserID"].ToString()),
                                        CreatedOn = DateTime.UtcNow
                                    };
                                    db.UserDetailPlatform.Add(newPlatform);
                                }

                                db.SaveChanges();
                            }
                        }
                    }
                }
            }

            if (string.IsNullOrEmpty(language))
            {
                language = GetLanguageFromAccount(user.AccountId);
            }

            //Sending mail
            if (IsDemoUser)
            {
                string demoFilename = string.Concat("NewDemoUserRegistration_", language, ".html");
                var demofilepath = System.IO.Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~/EmailTemplates"), demoFilename);
                var expiryDateString = expiryDate.ToString("dd MMMM yyyy");
                if (language != "English")
                {
                    var format = "dd MMMM yyyy";
                    var culture = db.CsNewLanguage.Where(x => x.NewName == language).Select(x => x.NewLanguageshortname).FirstOrDefault() ?? "en-US";
                    var dt = DateTime.ParseExact(expiryDateString, format, new CultureInfo("en-US"));

                    expiryDateString = dt.ToString(format, new CultureInfo(culture));
                }
                var demomailcontent = System.IO.File.ReadAllText(demofilepath);
                demomailcontent = demomailcontent.Replace("{Name}", user.FirstName.Trim()).Replace("{URL}", WebConfigurationManager.AppSettings["domain"].ToString()).Replace("{Email}", user.Email).Replace("{Password}", user.Password).Replace("{EndDate}", expiryDateString).Replace("{supportEmail}", WebConfigurationManager.AppSettings["supportAddress"].ToString());

                //Sharing account details with the user
                Mailer.SendMail(WebConfigurationManager.AppSettings["replyAddress"].ToString(), user.Email, "New demo account creation", demomailcontent);
            }
            else
            {
                string filename = string.Concat("NewUserRegistration_", language, ".html");
                var filepath = System.IO.Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~/EmailTemplates"), filename);

                var mailcontent = System.IO.File.ReadAllText(filepath);
                mailcontent = mailcontent.Replace("{Name}", user.FirstName.Trim()).Replace("{URL}", WebConfigurationManager.AppSettings["domain"].ToString()).Replace("{Email}", user.Email).Replace("{Password}", user.Password).Replace("{supportEmail}", WebConfigurationManager.AppSettings["supportAddress"].ToString());

                //Sharing account details with the user
                Mailer.SendMail(WebConfigurationManager.AppSettings["replyAddress"].ToString(), user.Email, "New account creation", mailcontent);
            }
        }

        public List<UserDetailModel> AddUpdateUserDetails(Guid UserId, Guid AccountId, List<UserDetailModel> details)
        {
            DB_Entities db = new DB_Entities();

            db.UserDetails.Where(w => w.UserId == UserId && w.IsDeleted == false).ToList().ForEach(i => i.IsDeleted = true);
            db.SaveChanges();

            //Delete existing
            var userDeial = db.UserDetails.Where(w => w.UserId == UserId).ToList();
            if (userDeial != null && userDeial.Count() > 0)
            {
                var userIds = userDeial.Select(X => X.Id).ToArray();
                var userPlatforms = db.UserDetailPlatform.Where(w => userIds.Contains(w.UserDetailId));
                if (userPlatforms.Count() > 0)
                {
                    userPlatforms.Each(platform =>
                    {
                        db.UserDetailPlatform.Remove(platform);
                    });

                    db.SaveChanges();
                }
            }

            foreach (UserDetailModel detail in details)
            {
                var endDate = Convert.ToDateTime(detail.ExpiryDate.ToString());

                if (detail.ID == 0 && detail.IsDeleted == false && detail.SubscriptionId != Guid.Empty)
                {
                    //var sub = db.CsNewSubscription.Where(x => x.Id == detail.SubscriptionId).FirstOrDefault();
                    //int duration = sub.NewDuration ?? 0;

                    UserDetails newDetail = new UserDetails()
                    {
                        UserId = UserId,
                        SubscriptionId = detail.SubscriptionId,
                        PackageId = detail.PackageId,
                        LanguageId = detail.LanguageId,
                        GradeId = detail.GradeId,
                        SyllabusId = detail.SyllabusId,
                        StreamId = detail.StreamId,
                        SubjectId = detail.SubjectId,
                        TopicId = detail.TopicId,
                        dtSubscriptionStartDate = DateTime.Today,
                        dtSubscriptionEndDate = endDate, //DateTime.Today.AddDays(duration),
                        IsSubscriptionExpired = false,
                        CreatedBy = new Guid(Session["UserID"].ToString()),
                        CreatedOn = DateTime.UtcNow,
                        IsActive = true,
                        IsDeleted = detail.IsDeleted
                    };
                    db.UserDetails.Add(newDetail);
                    db.SaveChanges();

                    if (detail.UserDetailIds != null)
                    {
                        foreach (var platformId in detail.UserDetailIds)
                        {
                            UserDetailPlatform newPlatform = new UserDetailPlatform()
                            {
                                UserDetailId = newDetail.Id,
                                PlatformId = new Guid(platformId),
                                CreatedBy = new Guid(Session["UserID"].ToString()),
                                CreatedOn = DateTime.UtcNow
                            };
                            db.UserDetailPlatform.Add(newPlatform);
                        }

                        db.SaveChanges();
                    }

                    detail.ID = newDetail.Id;
                }
                else
                {
                    var detailObj = db.UserDetails.Where(x => x.Id == detail.ID).FirstOrDefault();
                    if (detailObj != null)
                    {
                        detailObj.SubscriptionId = detail.SubscriptionId;
                        detailObj.PackageId = detail.PackageId;
                        detailObj.LanguageId = detail.LanguageId;
                        detailObj.GradeId = detail.GradeId;
                        detailObj.SyllabusId = detail.SyllabusId;
                        detailObj.StreamId = detail.StreamId;
                        detailObj.SubjectId = detail.SubjectId;
                        detailObj.TopicId = detail.TopicId;
                        detailObj.dtSubscriptionEndDate = endDate;
                        detailObj.IsActive = detail.IsActive;
                        detailObj.IsDeleted = detail.IsDeleted;
                        detailObj.ModifiedBy = new Guid(Session["UserID"].ToString());
                        detailObj.ModifiedOn = DateTime.UtcNow;
                        db.SaveChanges();
                    }                   

                    if (detail.UserDetailIds != null)
                    {
                        foreach (var platformId in detail.UserDetailIds)
                        {
                            UserDetailPlatform newPlatform = new UserDetailPlatform()
                            {
                                UserDetailId = detail.ID,
                                PlatformId = new Guid(platformId),
                                CreatedBy = new Guid(Session["UserID"].ToString()),
                                CreatedOn = DateTime.UtcNow
                            };

                            db.UserDetailPlatform.Add(newPlatform);
                            db.SaveChanges();
                        }
                    }
                }
            }

            return details;
        }

        public JsonResult GetSubscriptions(string AccountId)
        {
            PackageList lst = new PackageList();
            using (DB_Entities db = new DB_Entities())
            {
                lst.Subscriptions = (from o in db.CsOpportunity
                                     join os in db.CsNewOpportunitySubscriptionMapping on o.Id equals os.NewOpportunityid
                                     join s in db.CsNewSubscription on os.NewSubscriptionid equals s.Id
                                     where o.Parentaccountid == Guid.Parse(AccountId)
                                     select new DataList { Text = s.NewName, Value = s.Id }).Distinct().ToList();
            }

            return Json(lst, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetPackages(string SubscriptionId)
        {
            PackageList lst = new PackageList();
            using (DB_Entities db = new DB_Entities())
            {
                lst.Packages = (from sp in db.CsNewSubscriptionPackage
                                join p in db.CsNewPackage on sp.NewPackageid equals p.NewPackageid
                                where sp.NewSubscriptionid == Guid.Parse(SubscriptionId)
                                select new DataList { Text = p.NewName, Value = p.NewPackageid }).Distinct().ToList();

                var SubId = Guid.Parse(SubscriptionId);
                var sub = db.CsNewSubscription.Where(x => x.Id == SubId).FirstOrDefault();
                int duration = sub.NewDuration ?? 0;
                string ret = DateTime.Now.AddDays(duration).ToString("yyyy-MM-dd");
                lst.Subscriptions = new List<DataList>();
                lst.Subscriptions.Add(new DataList { Text = ret, Value = SubId });
            }

            return Json(lst, JsonRequestBehavior.AllowGet);
        }

        public string GetExpiryDate(string SubscriptionId)
        {
            string ret = DateTime.Now.ToString("yyyy-MM-dd");
            using (DB_Entities db = new DB_Entities())
            {
                var SubId = Guid.Parse(SubscriptionId);
                var sub = db.CsNewSubscription.Where(x => x.Id == SubId).FirstOrDefault();
                int duration = sub.NewDuration ?? 0;
                ret = DateTime.Now.AddDays(duration).ToString("yyyy-MM-dd");
            }

            return ret;
        }

        public string GetLanguageFromPackage(Guid PackageId)
        {
            using (DB_Entities db = new DB_Entities())
            {
                var pack = (from pl in db.CsNewPackageLanguage
                            join l in db.CsNewLanguage on pl.NewLanguageid equals l.NewLanguageid
                            where pl.NewPackageid == PackageId
                            select l).Distinct().FirstOrDefault();

                if (pack != null)
                {
                    return pack.NewName;
                }
            }

            return string.Empty;
        }

        public string GetLanguageFromSubscription(Guid SubscriptionId)
        {
            using (DB_Entities db = new DB_Entities())
            {
                var sub = (from sp in db.CsNewSubscriptionPackage
                           join pl in db.CsNewPackageLanguage on sp.NewPackageid equals pl.NewPackageid
                           join l in db.CsNewLanguage on pl.NewLanguageid equals l.NewLanguageid
                           where sp.NewSubscriptionid == SubscriptionId
                           select l).Distinct().FirstOrDefault();

                if (sub != null)
                {
                    return sub.NewName;
                }
            }

            return string.Empty;
        }

        public string GetLanguageFromAccount(Guid AccountId)
        {
            using (DB_Entities db = new DB_Entities())
            {
                var newSub = (from o in db.CsOpportunity
                              join os in db.CsNewOpportunitySubscriptionMapping on o.Id equals os.NewOpportunityid
                              join s in db.CsNewSubscription on os.NewSubscriptionid equals s.Id
                              where o.Parentaccountid == AccountId
                              select new DataList { Text = s.NewName, Value = s.Id }).Distinct().FirstOrDefault();

                if (newSub != null)
                {
                    var newLang = (from sp in db.CsNewSubscriptionPackage
                                   join pl in db.CsNewPackageLanguage on sp.NewPackageid equals pl.NewPackageid
                                   join l in db.CsNewLanguage on pl.NewLanguageid equals l.NewLanguageid
                                   where sp.NewSubscriptionid == newSub.Value
                                   select l).Distinct().FirstOrDefault();

                    if (newLang != null)
                    {
                        return newLang.NewName;
                    }
                }
            }

            return "English";
        }

        public bool ActiveUser(Guid Id)
        {
            bool status = false;
            status = DataAccess.ActiveUser(Id);

            return status;
        }

        public bool InactiveUser(Guid Id)
        {
            bool status = false;
            status = DataAccess.InactiveUser(Id);

            return status;
        }

        public bool DeleteUser(Guid Id)
        {
            var host = WebConfigurationManager.AppSettings["HostApi"].ToString();
            var admin = WebConfigurationManager.AppSettings["ApiAdmin"].ToString();
            var password = WebConfigurationManager.AppSettings["ApiAdminPassword"].ToString();
            var loggedInUser = new Guid(Session["UserID"].ToString());

            var loginRequest = new AccountLoginRequest() { Username = admin, Password = password };
            var loginResponse = DoHttp<AccountLoginRequest, AccountLoginResponse>($"{host}/api/v1/Account/Login", HttpMethods.GET, loginRequest);

            var request = new AccountDeleteUserRequest() { Id = Convert.ToString(Id) };

            var response = DoHttp<AccountDeleteUserRequest, AccountDeleteUserResponse>($"{host}/api/v1/Account/DeleteUser", HttpMethods.POST, request, loginResponse.AuthorizationToken);

            if (response.Success)
            {
                using (DB_Entities db = new DB_Entities())
                {
                    db.UserDetails.Where(w => w.UserId == Id && w.IsDeleted == false).ToList().ForEach(i => i.IsDeleted = true);
                    db.SaveChanges();
                }
                //TempData["Message"] = "User deleted successfully!";
            }
            //else
            //{
            //    TempData["Message"] = "Some error occured while processing your request.";
            //}

            return response.Success;
        }

        //
        // GET: /UserManagement/ForgotPassword
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        //
        // POST: /UserManagement/ChangePassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ForgotPassword(ForgotPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var host = WebConfigurationManager.AppSettings["HostApi"].ToString();
            var request = new AccountResetPasswordRequest() { Username = model.Email };

            var response = DoHttp<AccountResetPasswordRequest, AccountResetPasswordResponse>($"{host}/api/v1/Account/ResetPassword", HttpMethods.POST, request);

            if (response.Success)
            {
                TempData["Message"] = "Reset password link has been sent to your mail id!";
            }
            else
            {
                TempData["Message"] = "Error: " + response.Errors[0].Description;
            }

            return RedirectToAction("ForgotPasswordConfirmation");
        }

        //
        // GET: /UserManagement/ForgotPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        //
        // GET: /UserManagement/ResetPassword
        [AllowAnonymous]
        public ActionResult ResetPassword(string code)
        {
            return code == null ? View("Error") : View();
        }

        //
        // POST: /UserManagement/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var host = WebConfigurationManager.AppSettings["HostApi"].ToString();
            //model.Code = model.Code.Replace("%2b", "+");//Replace("%2f", "/");
            var request = new AccountResetPasswordLinkRequest() { Username = model.Email, NewPassword = model.Password, ResetPasswordToken = model.Code };
            string filePath = AppDomain.CurrentDomain.BaseDirectory + @"Logfile\Error.txt";

            using (StreamWriter writer = new StreamWriter(filePath, true))
            {
                writer.WriteLine(Environment.NewLine + "------------------------------------------------------------------------------------------------" + Environment.NewLine);
                writer.WriteLine("Action :" + "Reset Password start" + Environment.NewLine);
                writer.WriteLine(request.ResetPasswordToken + Environment.NewLine);
            }
            var response = DoHttp<AccountResetPasswordLinkRequest, AccountResetPasswordLinkResponse>($"{host}/api/v1/Account/ResetPasswordLink", HttpMethods.POST, request);
            using (StreamWriter writer = new StreamWriter(filePath, true))
            {
                writer.WriteLine("Action :" + "Reset Password end" + Environment.NewLine);
                writer.WriteLine("------------------------------------------------------------------------------------------------" + Environment.NewLine);
            }
            if (response.Success)
            {
                TempData["Message"] = "Password reset successfully!";
            }
            else
            {
                TempData["Message"] = "Error: " + response.Errors[0].Description;
            }

            return RedirectToAction("ResetPasswordConfirmation");
        }

        //
        // GET: /UserManagement/ResetPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            Session.Abandon();
            return View();
        }

        //
        // GET: /UserManagement/ChangePassword
        [AllowAnonymous]
        public ActionResult ChangePassword()
        {
            return View();
        }

        //
        // POST: /UserManagement/ChangePassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ChangePassword(ChangePasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var host = WebConfigurationManager.AppSettings["HostApi"].ToString();
            var request = new AccountChangePasswordRequest() { Username = model.Email, OldPassword = model.OldPassword, NewPassword = model.NewPassword };

            var response = DoHttp<AccountChangePasswordRequest, AccountChangePasswordResponse>($"{host}/api/v1/Account/ChangePassword", HttpMethods.POST, request);

            if (response.Success)
            {
                TempData["Message"] = "Password changed successfully!";
            }
            else
            {
                TempData["Error"] = response.Errors[0].Description;
            }

            return RedirectToAction("ChangePasswordConfirmation");
        }

        //
        // GET: /UserManagement/ChangePasswordConfirmation
        [AllowAnonymous]
        public ActionResult ChangePasswordConfirmation()
        {
            Session.Abandon();
            return View();
        }

        public string ResendPassword(string Email, string UserId)
        {
            string message = string.Empty;
            string strPassword = CommonMethods.GenerateRandomPassword();

            var host = WebConfigurationManager.AppSettings["HostApi"].ToString();
            var request = new AccountResetPasswordLinkRequest() { Username = Email, NewPassword = strPassword };

            var response = DoHttp<AccountResetPasswordLinkRequest, AccountResetPasswordLinkResponse>($"{host}/api/v1/Account/ResendPassword", HttpMethods.POST, request);

            if (response.Success)
            {
                message = "Password resent successfully!";
            }
            else
            {
                message = "Error: " + response.Errors[0].Description;
            }

            return message;
        }

        public string ResendInvitation(string Email, string UserId)
        {
            string message = string.Empty;
            string strPassword = CommonMethods.GenerateRandomPassword();

            var host = WebConfigurationManager.AppSettings["HostApi"].ToString();
            var request = new AccountResetPasswordLinkRequest() { Username = Email, NewPassword = strPassword };

            var response = DoHttp<AccountResetPasswordLinkRequest, AccountResetPasswordLinkResponse>($"{host}/api/v1/Account/ResendInvitation", HttpMethods.POST, request);

            if (response.Success)
            {
                using (DB_Entities db = new DB_Entities())
                {
                    //Getting language
                    string language = string.Empty;
                    var Id = Guid.Parse(UserId);
                    var et = DateTime.Today; bool IsDemoUser = false;

                    var user = DataAccess.GetUserDetails(Id);
                    var details = db.UserDetails.Where(x => x.UserId == Id && x.IsDeleted == false).ToList();
                    foreach (var detail in details)
                    {
                        et = (DateTime)detail.dtSubscriptionEndDate;

                        var sub = db.CsNewSubscription.Where(x => x.Id == detail.SubscriptionId).FirstOrDefault();
                        if (sub.NewName.ToLower().Contains("demo"))
                        {
                            IsDemoUser = true;
                        }

                        if (detail.LanguageId != Guid.Empty)
                        {
                            var la = db.CsNewLanguage.Where(x => x.Id == detail.LanguageId).FirstOrDefault();
                            language = la != null ? la.NewName : string.Empty;
                            break;
                        }
                        else if (detail.PackageId != Guid.Empty)
                        {
                            language = GetLanguageFromPackage(detail.PackageId);
                            break;
                        }
                        else
                        {
                            language = GetLanguageFromSubscription(detail.SubscriptionId);
                            break;
                        }
                    }

                    if (string.IsNullOrEmpty(language))
                    {
                        language = GetLanguageFromAccount(user.AccountId);

                        if (IsDemoUser == false && user.Account.ToLower().Contains("demo"))
                        {
                            IsDemoUser = true;
                        }
                    }

                    //Sending mail
                    if (IsDemoUser)
                    {
                        string demoFilename = string.Concat("NewDemoUserRegistration_", language, ".html");
                        var demofilepath = System.IO.Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~/EmailTemplates"), demoFilename);
                        var expiryDate = et.ToString("dd MMMM yyyy");
                        if (language != "English")
                        {
                            var format = "dd MMMM yyyy";
                            var culture = db.CsNewLanguage.Where(x => x.NewName == language).Select(x => x.NewLanguageshortname).FirstOrDefault() ?? "en-US";
                            var dt = DateTime.ParseExact(expiryDate, format, new CultureInfo("en-US"));

                            expiryDate = dt.ToString(format, new CultureInfo(culture));
                        }
                        var demomailcontent = System.IO.File.ReadAllText(demofilepath);
                        demomailcontent = demomailcontent.Replace("{Name}", user.FirstName.Trim()).Replace("{URL}", WebConfigurationManager.AppSettings["domain"].ToString()).Replace("{Email}", user.Email).Replace("{Password}", strPassword).Replace("{EndDate}", expiryDate).Replace("{supportEmail}", WebConfigurationManager.AppSettings["supportAddress"].ToString());

                        //Sharing account details with the user
                        Mailer.SendMail(WebConfigurationManager.AppSettings["replyAddress"].ToString(), user.Email, "New demo account creation", demomailcontent);
                    }
                    else
                    {
                        string filename = string.Concat("NewUserRegistration_", language, ".html");
                        var filepath = System.IO.Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~/EmailTemplates"), filename);

                        var mailcontent = System.IO.File.ReadAllText(filepath);
                        mailcontent = mailcontent.Replace("{Name}", user.FirstName.Trim()).Replace("{URL}", WebConfigurationManager.AppSettings["domain"].ToString()).Replace("{Email}", user.Email).Replace("{Password}", strPassword).Replace("{supportEmail}", WebConfigurationManager.AppSettings["supportAddress"].ToString());

                        //Sharing account details with the user
                        Mailer.SendMail(WebConfigurationManager.AppSettings["replyAddress"].ToString(), user.Email, "New account creation", mailcontent);
                    }
                }

                message = "Invitation resent successfully!";
            }
            else
            {
                message = "Error: " + response.Errors[0].Description;
            }

            return message;
        }
    }
}