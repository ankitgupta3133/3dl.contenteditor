﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using TDL.ContentPlatform.Server.Database;

namespace _3DLContentManagement.Database
{
    public class DB_Entities : ContentDbContext
    {
        public DB_Entities() : base()
        {

        }
        public DB_Entities(DbContextOptions<ContentDbContext> options) : base(options)
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            {
                if (optionsBuilder.IsConfigured == false)
                {
                    optionsBuilder.UseSqlServer(ConfigurationManager.ConnectionStrings["DB_Entities"].ConnectionString);
                }

                base.OnConfiguring(optionsBuilder);
            }
        }
    }
}