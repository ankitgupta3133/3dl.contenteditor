﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace _3DLContentManagement.Models
{
    public class LoginViewModel
    {
        [Display(Name = "Email or UserName")]
        [Required(ErrorMessage = "UserName is required.")]
        [EmailAddress(ErrorMessage = "Invalid UserName.")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Password is required.")]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }
    }

    public class LoggedInUser
    { 
        public LoggedInUser()
        {
            this.IsImpersonated = false;
        }
        public string UserName { get; set; }
        public Nullable<System.Guid> UserID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName
        {
            get
            {
                return this.FirstName + (!string.IsNullOrEmpty(this.LastName) ? " " + this.LastName : "");
            }
        }
        public bool? IsImpersonated { get; set; }
        public string ImpersonatedBy { get; set; }
        public string ImpersonatedUserRole { get; set; }
        public int SecurityFrameworkID { get; set; }
    }

    public class RegisterViewModel
    {
        public Guid UserID { get; set; }
        [Required(ErrorMessage = "First Name is required.")]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 8)]
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Password is required.")]
        [Display(Name = "Password")]
        public string Password { get; set; }
        //[Required(ErrorMessage = "User Name is required.")]
        public string Username { get; set; }
        [Required(ErrorMessage = "Email is required.")]
        [EmailAddress(ErrorMessage = "Invalid email.")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Account is required.")]
        [Display(Name = "Account")]
        public string Account { get; set; }
        public Guid AccountId { get; set; }
        public string Subscription { get; set; }
        public Guid SubscriptionId { get; set; }
        public string Opportunity { get; set; }
        [Required(ErrorMessage = "Role is required.")]
        public string Role { get; set; }
        public Guid RoleId { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public List<UserDetailModel> Details { get; set; }
        public IEnumerable<System.Web.Mvc.SelectListItem> Platforms { get; set; }
    }

    public class UserDetailModel
    {
        public int ID { get; set; }
        public string Subscription { get; set; }
        public string Package { get; set; }
        public string Language { get; set; }
        public string Grade { get; set; }
        public string Syllabus { get; set; }
        public string Stream { get; set; }
        public string Subject { get; set; }
        public string Topic { get; set; }
        public Guid SubscriptionId { get; set; }
        public Guid PackageId { get; set; }
        public Guid LanguageId { get; set; }
        public Guid GradeId { get; set; }
        public Guid SyllabusId { get; set; }
        public Guid StreamId { get; set; }
        public Guid SubjectId { get; set; }
        public Guid TopicId { get; set; }
        //public int Duration { get; set; }
        public DateTime ExpiryDate { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public string[] UserDetailIds { get; set; }        
        public UserDetailModel()
        {
            IsActive = true;
        }
    }

    public class RoleModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }

    public class DirModel
    {
        public string DirName { get; set; }
        public string DirPath { get; set; }
        public string Status { get; set; }
        public string DirSize { get; set; }
        public string DirSizeText { get; set; }
        public DateTime DirAccessed { get; set; }
    }

    public class FileModel
    {
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public string Status { get; set; }
        public string FileSize { get; set; }
        public string FileSizeText { get; set; }
        public DateTime FileAccessed { get; set; }
    }

    public class ExplorerModel
    {
        public List<DirModel> dirModelList;
        public List<FileModel> fileModelList;

        public ExplorerModel(List<DirModel> _dirModelList, List<FileModel> _fileModelList)
        {
            dirModelList = _dirModelList;
            fileModelList = _fileModelList;
        }
    }

    public class ContentModel
    {
        public string Name { get; set; }
        public string Path { get; set; }
        public string Folder { get; set; }
        public string Status { get; set; }
        public string Size { get; set; }
        public string SizeText { get; set; }
        public DateTime AccessedOn { get; set; }
        public bool IsDirectory { get; set; }
    }

    public class PasswordOptions
    {
        public PasswordOptions()
        {
        }

        public int RequiredLength { get; set; }
        public int RequiredUniqueChars { get; set; }
        public bool RequireDigit { get; set; }
        public bool RequireLowercase { get; set; }
        public bool RequireNonAlphanumeric { get; set; }
        public bool RequireUppercase { get; set; }
    }

    public class ChangePasswordViewModel
    {
        [Display(Name = "Email")]
        [Required(ErrorMessage = "Email is required.")]
        [EmailAddress(ErrorMessage = "Invalid Email.")]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 8)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }
    public class ChangeProfileViewModel
    {
        [Display(Name = "Email or User Name")]
        [Required(ErrorMessage = "User Name is required.")]
        [EmailAddress(ErrorMessage = "Invalid UserName.")]
        public string UserName { get; set; }
        [Required(ErrorMessage = "First Name is required.")]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Current password is required to update the details.")]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 8)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }
    public class ForgotPasswordViewModel
    {
        [Required(ErrorMessage = "Email is required.")]
        [EmailAddress(ErrorMessage = "Invalid Email.")]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class ResetPasswordViewModel
    {
        [Required(ErrorMessage = "Email is required.")]
        [EmailAddress(ErrorMessage = "Invalid Email.")]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Password is required.")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 8)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }
    public class Topic
    {
        public int TopicID { get; set; }
        public string TopicName { get; set; }
        public Nullable<int> SubjectId { get; set; }
        public string Subject { get; set; }
        public Nullable<System.Guid> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<System.Guid> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<bool> IsActive { get; set; }
    }

    public class Subtopic
    {
        public int SubtopicID { get; set; }
        public string SubtopicName { get; set; }
        public Nullable<int> TopicId { get; set; }
        public string Topic { get; set; }
        public Nullable<System.Guid> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<System.Guid> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<bool> IsActive { get; set; }
    }

    public class DataList
    {
        public Guid? Value { get; set; }
        public string Text { get; set; }
    }

    public class PackageList
    {
        public List<DataList> Subscriptions { get; set; }
        public List<DataList> Packages { get; set; }
    }

    public class LanguageAndGradeList
    {
        public List<DataList> Languages { get; set; }
        public List<DataList> Grades { get; set; }
    }

    public class SyllabusList
    {
        public List<DataList> Syllabus { get; set; }
        public List<DataList> Streams { get; set; }
        public List<DataList> Subjects { get; set; }
    }

    public class SubjectList
    {
        public List<DataList> Subjects { get; set; }
    }

    public class TopicList
    {
        public List<DataList> Topics { get; set; }
    }

    public class SubtopicList
    {
        public List<DataList> Subtopics { get; set; }
    }
}