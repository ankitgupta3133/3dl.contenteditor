﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _3DLContentManagement.Models
{

    public class AssetViewModel
    {
        public int AssetID { get; set; }
        public string AssetName { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public Int64 FileSize { get; set; }
        public string FileSizeText { get; set; }
        [Required(ErrorMessage = "Type is required.")]
        public string AssetType { get; set; }
        [Required(ErrorMessage = "Status is required.")]
        public string Status { get; set; }
        public string Package { get; set; }
        public string Language { get; set; }
        [Display(Name = "Grade")]
        public string Level { get; set; }
        public string Syllabus { get; set; }
        public string Stream { get; set; }
        public string Subject { get; set; }
        public string Topic { get; set; }
        public string Subtopic { get; set; }
        public string SubtopicDesc { get; set; }
        public string TagsNew { get; set; }
        public string[] NewTags { get; set; }
        public string PropertiesNew { get; set; }
        public string[] NewProperties { get; set; }
        public string VimeoUrl { get; set; }
        public Guid ContentPackageID { get; set; }
        public Nullable<int> ReplaceAssetID { get; set; }
        public decimal PercentageOfLabels { get; set; }
        public int NumberOfAttempts { get; set; }
        public string MultipleAssetName { get; set; }
        public string MultipleLanguage { get; set; }
        public string MultipleLevel { get; set; }
        public string MultipleSyllabus { get; set; }
        public string MultipleStream { get; set; }
        public string MultipleSubject { get; set; }
        public string MultipleTopic { get; set; }
        public string MultipleSubtopic { get; set; }
        public Nullable<System.Guid> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<System.Guid> ModifiedBy { get; set; }
        public string ModifiedByName { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsDirectory { get; set; }
        public Nullable<bool> IsPublished { get; set; }
        public bool IsQuizEnabled { get; set; }
        public List<AssetContentModel> Contents { get; set; }
        public List<AssetNameModel> Names { get; set; }
        public List<AssetDetailModel> Details { get; set; }
        public List<ActivityModel> Activities { get; set; }
        public List<Label> Labels { get; set; }        

        public AssetViewModel()
        {
            IsActive = true;
            IsQuizEnabled = false;
            NumberOfAttempts = 0;
            PercentageOfLabels = 0.0M;
        }
    }

    public class AssetContentModel
    {
        public int ID { get; set; }
        public Nullable<int> AssetID { get; set; }
        [Required(ErrorMessage = "Module Name is required.")]
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public string FileSize { get; set; }
        public string FileSizeText { get; set; }
        public Nullable<bool> IsDirectory { get; set; }
        public int VersionNumber { get; set; }
        public string Description { get; set; }
        public string WhatIsNew { get; set; }
        [Required(ErrorMessage = "Platform is required.")]
        public string Platform { get; set; }
        public string PlatformId { get; set; }
        public Guid PublishFolder { get; set; }
        public string Thumbnail { get; set; }
        public string Background { get; set; }
        public Nullable<System.Guid> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<System.Guid> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public Nullable<bool> IsEnabled { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public AssetContentModel()
        {
            IsEnabled = false;
            IsDeleted = false;
        }
    }

    public enum Platform
    {
        All,
        Android,
        iOS,
        NibiruVR,
        Windows
    }

    public class AssetNameModel
    {
        public int ID { get; set; }
        [Required(ErrorMessage = "Name is required.")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Language is required.")]
        public string Language { get; set; }
        public Guid LanguageId { get; set; }
        public bool IsDeleted { get; set; }
    }

    public class AssetDetailModel
    {
        public int ID { get; set; }
        public string Package { get; set; }
        public string Language { get; set; }
        public string Grade { get; set; }
        public string Syllabus { get; set; }
        public string Stream { get; set; }
        public string Subject { get; set; }
        public string Topic { get; set; }
        public string Subtopic { get; set; }
        public string SubtopicDesc { get; set; }
        public Guid PackageId { get; set; }
        public Guid LanguageId { get; set; }
        public Guid GradeId { get; set; }
        public Guid SyllabusId { get; set; }
        public Guid StreamId { get; set; }
        public Guid SubjectId { get; set; }
        public Guid TopicId { get; set; }
        public Guid SubtopicId { get; set; }
        public bool IsDeleted { get; set; }
    }

    public class ContentDescriptionModel
    {
        public int ID { get; set; }
        public int AssetContentID { get; set; }
        [Required(ErrorMessage = "Description is required.")]
        public string Description { get; set; }
        public string WhatIsNew { get; set; }
        [Required(ErrorMessage = "Language is required.")]
        public string Language { get; set; }
        public string LanguageShortName { get; set; }
        public Guid LanguageId { get; set; }
        public bool IsDeleted { get; set; }
    }

    public class ActivityModel
    {
        public int ActivityId { get; set; }
        [Required(ErrorMessage = "Name is required.")]
        public string ActivityName { get; set; }
        public int ActivityType { get; set; }
        public string ActivityTypeName { get; set; }
        public Guid? ActivityLanguageId { get; set; }
        public bool? IsModelSpecific { get; set; }
        public Guid? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public Guid? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public bool? IsActivityEnabled { get; set; }
        public bool? IsActivityDeleted { get; set; }
        //public List<QuizDetailModel> QuizDetails { get; set; }
        //public List<QuizLabelDetailModel> QuizLabels { get; set; }
        //public List<PuzzleDetailModel> PuzzleDetails { get; set; }
        public ActivityModel()
        {
            IsActivityEnabled = false;
            IsActivityDeleted = false;
            //QuizDetails = new List<QuizDetailModel>();
            //QuizLabels = new List<QuizLabelDetailModel>();
            //PuzzleDetails = new List<PuzzleDetailModel>();
        }
    }

    public class QuizModel
    {
        public int QuizId { get; set; }
        [Required(ErrorMessage = "Name is required.")]
        public string QuizName { get; set; }
        [Required(ErrorMessage = "Duration is required.")]
        public int Duration { get; set; }
        public bool? IsQuizModelSpecific { get; set; }
        public bool? IsQuizEnabled { get; set; }
        public bool? IsQuizDeleted { get; set; }
        [Required(ErrorMessage = "Number of Attempts is required.")]
        public int NumberOfAttempts { get; set; }
        [Required(ErrorMessage = "Number of Labels is required.")]
        public int NumberOfLabels { get; set; }
        public List<AssetNameModel> Names { get; set; }
        public List<AssetDetailModel> Details { get; set; }
        public List<QuizDetailModel> QuizDetails { get; set; }
        public List<PlatformModel> Platforms { get; set; }
        public QuizModel()
        {
            NumberOfAttempts = 0;
            NumberOfLabels = 0;
            IsQuizEnabled = false;
            IsQuizDeleted = false;
            IsQuizModelSpecific = true;
            QuizDetails = new List<QuizDetailModel>();
            Names = new List<AssetNameModel>();
            Details = new List<AssetDetailModel>();
            Platforms = new List<PlatformModel>();
        }
    }

    public class PuzzleModel
    {
        public int PuzzleId { get; set; }
        [Required(ErrorMessage = "Name is required.")]
        public string PuzzleName { get; set; }
        [Required(ErrorMessage = "Duration is required.")]
        public int Duration { get; set; }
        public bool? IsPuzzleModelSpecific { get; set; }
        public bool? IsPuzzleEnabled { get; set; }
        public bool? IsPuzzleDeleted { get; set; }
        public List<AssetNameModel> Names { get; set; }
        public List<AssetDetailModel> Details { get; set; }
        public List<PuzzleDetailModel> PuzzleDetails { get; set; }
        public List<PlatformModel> PuzzlePlatforms { get; set; }
        public PuzzleModel()
        {
            IsPuzzleEnabled = false;
            IsPuzzleDeleted = false;
            IsPuzzleModelSpecific = true;
            PuzzleDetails = new List<PuzzleDetailModel>();
            Names = new List<AssetNameModel>();
            Details = new List<AssetDetailModel>();
            PuzzlePlatforms = new List<PlatformModel>();
        }
    }

    public class QuizDetailModel
    {
        public int Id { get; set; }
        public int QuizId { get; set; }
        public int AssetId { get; set; }
        public string AssetName { get; set; }
        public int AssetContentId { get; set; }
        public int NumberOfAttempts { get; set; }
        public int NumberOfLabels { get; set; }
        public int ModelId { get; set; }
        public string Labels { get; set; }
        public bool IsSelected { get; set; }
        public List<QuizLabelDetailModel> QuizLabels { get; set; }
        public QuizDetailModel()
        {
            IsSelected = false;
            NumberOfAttempts = 0;
            NumberOfLabels = 0;
            Labels = string.Empty;
            QuizLabels = new List<QuizLabelDetailModel>();
        }
    }

    public class QuizLabelDetailModel
    {
        public int Id { get; set; }
        public int QuizDetailId { get; set; }
        public int LabelId { get; set; }
        public string LabelName { get; set; }
        public bool IsSelected { get; set; }
    }

    public class PuzzleDetailModel
    {
        public int Id { get; set; }
        public int PuzzleId { get; set; }
        public int AssetId { get; set; }
        public string AssetName { get; set; }
        public int AssetContentId { get; set; }
        public int NumberOfParts { get; set; }
        public int ModelId { get; set; }
        public bool IsSelected { get; set; }
        public PuzzleDetailModel()
        {
            IsSelected = false;
        }
    }

    public class ClassificationModel
    {
        public int ClassificationId { get; set; }
        [Required(ErrorMessage = "Name is required.")]
        public string ClassificationName { get; set; }
        public bool? IsClassificationModelSpecific { get; set; }
        public bool? IsClassificationEnabled { get; set; }
        public bool? IsClassificationDeleted { get; set; }
        public List<AssetNameModel> Names { get; set; }
        public List<AssetDetailModel> Details { get; set; }
        public List<PlatformModel> ClassificationPlatforms { get; set; }
        public List<ClassificationPropertyModel> Properties { get; set; }
        public ClassificationModel()
        {
            IsClassificationEnabled = false;
            IsClassificationDeleted = false;
            IsClassificationModelSpecific = true;
            Names = new List<AssetNameModel>();
            Details = new List<AssetDetailModel>();
            ClassificationPlatforms = new List<PlatformModel>();
            Properties = new List<ClassificationPropertyModel>();
        }
    }

    public class ClassificationPropertyModel
    {
        public int Id { get; set; }
        public int ClassificationId { get; set; }
        public int PropertyId { get; set; }
        public string PropertyName { get; set; }
        public bool IsSelected { get; set; }
        public List<LinkedAsset> ClassificationAssets { get; set; }
        public ClassificationPropertyModel()
        {
            IsSelected = false;
            ClassificationAssets = new List<LinkedAsset>();
        }
    }

    public class ActivityAsset
    {
        public int AssetId { get; set; }
        public string AssetName { get; set; }
        public int AssetContentId { get; set; }
        public int ModelId { get; set; }
        public int NumberOfLabels { get; set; }
        public List<AssetLabel> AssetLabels { get; set; }
        public ActivityAsset()
        {
            NumberOfLabels = 0;
            AssetLabels = new List<AssetLabel>();
        }
    }

    public class AssetRelationModel
    {
        public int SelectedAssetId { get; set; }
        public string SelectedAssetName { get; set; }
        public List<LinkedAsset> LinkedAssets { get; set; }
        public AssetRelationModel()
        {
            LinkedAssets = new List<LinkedAsset>();
        }
    }

    public class LinkedAsset
    {
        public int RelationId { get; set; }
        public int LinkedAssetId { get; set; }
        public string LinkedAssetName { get; set; }
        public int LinkedContentId { get; set; }
        public bool IsSelected { get; set; }
        public LinkedAsset()
        {
            RelationId = 0;
            IsSelected = false;
        }
    }

    public class AssetLabel
    {
        public int LabelId { get; set; }
        public int ModelId { get; set; }
        public string LabelName { get; set; }
    }
    public class PlatformModel
    {
        public int Id { get; set; }
        public Guid PlatformId { get; set; }
        public string PlatformName { get; set; }
        public bool IsSelected { get; set; }
    }
    public class CompoundModel
    {
        public int CompoundId { get; set; }
        [Required(ErrorMessage = "Compound Name is required.")]
        [StringLength(100)]
        public string CompoundName { get; set; }
        public string CommonName { get; set; }
        [AllowHtml]
        [StringLength(100)]
        public string CompoundFormulae { get; set; }
        public int? SphericalAssetId { get; set; }
        public int? BondedAssetId { get; set; }
        public int? MaterialId { get; set; }
        public string SphericalAsset { get; set; }
        public string BondedAsset { get; set; }
        public string Material { get; set; }
        [StringLength(50)]
        public string Color { get; set; }
        [StringLength(100)]
        public string WikiLink { get; set; }
        [AllowHtml]
        [MaxLength]
        public string Discription { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDeleted { get; set; }
        public List<CompoundElementModel> Elements { get; set; }
        public CompoundModel()
        {
            Elements = new List<CompoundElementModel>();
            IsDeleted = false;
        }
    }

    public class CompoundElementModel
    {
        public int Id { get; set; }
        public int ElementId { get; set; }
        public int CompoundId { get; set; }
        public string ElementName { get; set; }
        public bool IsSelected { get; set; }
        public CompoundElementModel()
        {
            Id = 0;
            CompoundId = 0;
            IsSelected = false;
        }
    }

    public class AppViewModel
    {
        public Guid ID { get; set; }
        [Required(ErrorMessage = "App Name is required.")]
        public string AppName { get; set; }
        [Required(ErrorMessage = "App ID is required.")]
        public string AppID { get; set; }
        [Required(ErrorMessage = "User is required.")]
        public string User { get; set; }
        public Guid UserID { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
    }

    public class AssetPublishStatusModel
    {
        public int AssetID { get; set; }        
        public string AssetName { get; set; }
        public string Status { get; set; }
        public string Type { get; set; }        
        public string All { get; set; }
        public string Android { get; set; }
        public string Daydream { get; set; }
        public string IPhone { get; set; }
        public string MetroX64 { get; set; }
        public string Nibiru_VR { get; set; }
        public string Oculus { get; set; }
        public string OSX { get; set; }
        public string WebGL { get; set; }
        public string Windows { get; set; }
        public string WSA { get; set; }
    }
}