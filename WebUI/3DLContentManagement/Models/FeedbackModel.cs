﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _3DLContentManagement.Models
{
    public class FeedbackModel
    {
        public int FeedbackId { get; set; }
        [AllowHtml]
        public string Feedback { get; set; }
        public int AssetContentId { get; set; }
        public string Asset { get; set; }
        public Guid UserId { get; set; }
        public string Username { get; set; }
        public string UserEmail { get; set; }
        public string Account { get; set; }
        public Guid? GradeId { get; set; }
        public string Grade { get; set; }
        public Guid? SubjectId { get; set; }
        public string Subject { get; set; }
        public Guid? TopicId { get; set; }
        public string Topic { get; set; }
        public Guid? SubtopicId { get; set; }
        public string Subtopic { get; set; }
        public int? StatusId { get; set; }
        public string Status { get; set; }
        public string AddedOn { get; set; }
        public string AttachedFile { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
    }
}