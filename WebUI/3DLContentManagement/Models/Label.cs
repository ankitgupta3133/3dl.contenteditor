﻿using System;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace _3DLContentManagement.Models
{
    public class Label
    {
        public int LabelID { get; set; }
        public int ModelID { get; set; }
        public string LabelName { get; set; }
        [Required(ErrorMessage = "Face is required.")]
        public int face { get; set; }
        public int number { get; set; }
        //[Required(ErrorMessage = "EN is required.")]
        public string EN { get; set; }
        //[Required(ErrorMessage = "NO is required.")]
        public string NO { get; set; }
        //[Required(ErrorMessage = "NN is required.")]
        public string NN { get; set; }
        //[Required(ErrorMessage = "AR is required.")]
        public string AR { get; set; }
        [Required(ErrorMessage = "highlight is required.")]
        public string highlight { get; set; }
        //[Required(ErrorMessage = "original is required.")]
        public string original { get; set; }
        [Required(ErrorMessage = "X-Position is required.")]
        public decimal xPosition { get; set; }
        [Required(ErrorMessage = "Y-Position is required.")]
        public decimal yPosition { get; set; }
        [Required(ErrorMessage = "Z-Position is required.")]
        public decimal zPosition { get; set; }
        [Required(ErrorMessage = "X-Rotation is required.")]
        public decimal xRotation { get; set; }
        [Required(ErrorMessage = "Y-Rotation is required.")]
        public decimal yRotation { get; set; }
        [Required(ErrorMessage = "Z-Rotation is required.")]
        public decimal zRotation { get; set; }
        [Required(ErrorMessage = "X-Scale is required.")]
        public decimal xScale { get; set; }
        [Required(ErrorMessage = "Y-Scale is required.")]
        public decimal yScale { get; set; }
        [Required(ErrorMessage = "Z-Scale is required.")]
        public decimal zScale { get; set; }
        public Nullable<System.Guid> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<System.Guid> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public Nullable<bool> IsEnabled { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public int? AssetContentId { get; set; } 
        public List<LabelNamesModel> LabelNames { get; set; }

        public Label()
        {
            IsEnabled = true;
            IsDeleted = false;
            AssetContentId = 0;
        }
    }

    public class GetLabel
    {
        public string Label { get; set; }
    }
}