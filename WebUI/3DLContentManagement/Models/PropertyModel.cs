﻿using System;

namespace _3DLContentManagement.Models
{
    public class PropertyModel
    {
        public int PropertyId { get; set; }
        public string PropertyName { get; set; }
        public Guid? LanguageId { get; set; }
        public string Language { get; set; }
        public DateTime? CreatedOn { get; set; }
        public Guid? CreatedBy { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDeleted { get; set; }
        public PropertyModel()
        {
            IsActive = true;
            IsDeleted = false;
        }
    }
}