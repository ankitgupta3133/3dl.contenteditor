﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace _3DLContentManagement.Models
{
    public class TagViewModel
    {
        public int TagId { get; set; }
        
        public string TagName { get; set; }
        public Guid? CreatedBy { get; set; }        
        public DateTime? CreatedOn { get; set; }
        public Guid? ModifiedBy { get; set; }        
        public DateTime? ModifiedOn { get; set; }
        public bool? IsDeleted { get; set; }
        public bool? IsActive { get; set; }
        public List<TagNamesModel> TagNames { get; set; }
    }

    public class TagNamesModel
    {
        public int Id { get; set; }
        public int TagId { get; set; }
        [Required(ErrorMessage = "Name is required.")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Language is required.")]
        public string Language { get; set; }
        public Guid LanguageId { get; set; }
        public bool IsDeleted { get; set; }        
    }

    public class PropertyViewModel
    {
        public int PropertyId { get; set; }

        public string PropertyName { get; set; }
        public Guid? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public Guid? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public bool? IsDeleted { get; set; }
        public bool? IsActive { get; set; }
        public List<PropertyNamesModel> PropertyNames { get; set; }
    }

    public class PropertyNamesModel
    {
        public int Id { get; set; }
        public int PropertyId { get; set; }
        [Required(ErrorMessage = "Name is required.")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Language is required.")]
        public string Language { get; set; }
        public Guid LanguageId { get; set; }
        public bool IsDeleted { get; set; }
    }
}