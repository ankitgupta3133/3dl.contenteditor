﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace _3DLContentManagement.Models
{
    public class XmlFormModel
    {
        public int ModelID { get; set; }
        public int AssetContentID { get; set; }
        public string ModelName { get; set; }
        public decimal hidingposX { get; set; }
        public decimal hidingposY { get; set; }
        public decimal hidingposZ { get; set; }
        public List<Label> Labels { get; set; }
        public Nullable<System.Guid> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<System.Guid> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public Nullable<bool> IsEnabled { get; set; }
        public Nullable<bool> IsDeleted { get; set; }

        public List<XmlFormModelNamesModel> ModelNames { get; set; }

        public XmlFormModel()
        {
        }
    }
}