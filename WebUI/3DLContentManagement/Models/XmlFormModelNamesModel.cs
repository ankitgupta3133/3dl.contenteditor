﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace _3DLContentManagement.Models
{
    public class XmlFormModelNamesModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Language is required.")]
        public string Language { get; set; }
        public Guid LanguageId { get; set; }

        [Required(ErrorMessage = "Name is required.")]
        public string Name { get; set; }
      
        public bool IsDeleted { get; set; }
    }
}