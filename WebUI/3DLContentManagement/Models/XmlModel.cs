﻿using System;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace _3DLContentManagement.Models
{
    public class XmlModel
    {
        [XmlRoot("xml")]
        public class xml
        {
            [XmlElement("model")]
            public model model { get; set; }
        }

        [SerializableAttribute()]
        public class model
        {
            [XmlAttributeAttribute()]
            public string EN { get; set; }
            [XmlAttributeAttribute()]
            public string NO { get; set; }
            [XmlAttributeAttribute()]
            public string AR { get; set; }
            [XmlAttributeAttribute()]
            public string hidingposX { get; set; }
            [XmlAttributeAttribute()]
            public string hidingposY { get; set; }
            [XmlAttributeAttribute()]
            public string hidingposZ { get; set; }
            [XmlElementAttribute("label")]
            public label[] label { get; set; }
        }

        [SerializableAttribute()]
        public class label
        {
            [XmlAttributeAttribute()]
            public string face { get; set; }
            [XmlAttributeAttribute()]
            public string number { get; set; }
            [XmlElementAttribute("name")]
            public name name { get; set; }
            [XmlElementAttribute("color")]
            public color color { get; set; }
            [XmlElement("popup")]
            public popup popup { get; set; }
        }

        [SerializableAttribute()]
        public class name
        {
            [XmlAttributeAttribute()]
            public string EN { get; set; }
            [XmlAttributeAttribute()]
            public string NO { get; set; }
            [XmlAttributeAttribute()]
            public string AR { get; set; }
        }

        [SerializableAttribute()]
        public class color
        {
            [XmlAttributeAttribute()]
            public string highlight { get; set; }
            [XmlAttributeAttribute()]
            public string original { get; set; }
        }

        [SerializableAttribute()]
        public class position
        {
            [XmlAttributeAttribute()]
            public string x { get; set; }
            [XmlAttributeAttribute()]
            public string y { get; set; }
            [XmlAttributeAttribute()]
            public string z { get; set; }
        }

        [SerializableAttribute()]
        public class rotation
        {
            [XmlAttributeAttribute()]
            public string x { get; set; }
            [XmlAttributeAttribute()]
            public string y { get; set; }
            [XmlAttributeAttribute()]
            public string z { get; set; }
        }

        [SerializableAttribute()]
        public class scale
        {
            [XmlAttributeAttribute()]
            public string x { get; set; }
            [XmlAttributeAttribute()]
            public string y { get; set; }
            [XmlAttributeAttribute()]
            public string z { get; set; }
        }

        [SerializableAttribute()]
        public class popup
        {
            [XmlElementAttribute("position")]
            public position popupPosition { get; set; }
            [XmlElementAttribute("rotation")]
            public rotation popupRotation { get; set; }
            [XmlElementAttribute("scale")]
            public scale popupScale { get; set; }
        }

        public XmlModel()
        {
        }
    }
}