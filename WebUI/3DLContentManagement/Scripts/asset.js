﻿function ValidateForm() {
    if ($("#frmAsset").valid()) {
        showLoader();
    }
}

$('#UploadFile').change(function () {
    var i = $(this).next('label').clone();
    var file = $('#UploadFile')[0].files[0].name;
    $(this).next('label').text(file);
});

$("#btnUpload").on("click", function () {
    var actionurl = '/Home/UploadImage';
    var formData = new FormData($("#frmImage")[0]);
    var id = $('#txtRowID').val();
    $.ajax({
        url: actionurl,
        type: 'post',
        data: formData,
        processData: false,
        contentType: false,
        async: false,
        success: function (data) {
            if (data == "Error") {
                $('#divError').append("<strong>Error!</strong> You do not specified a file.").show();
            }
            else if (data == "Exception") {
                $('#divError').append("<strong>Error!</strong> Some error occured while uploading a file.").show();
            }
            else {
                $('#AddImageOption').modal('hide');
                $("#UploadFile").val('');
                $('#Contents_' + id + '__Thumbnail').val(data);
                alert("Thumbnail image uploaded successfully");
            }
        }
    });
});

$('#UploadBGFile').change(function () {
    var i = $(this).next('label').clone();
    var file = $('#UploadBGFile')[0].files[0].name;
    $(this).next('label').text(file);
});

$("#btnBGUpload").on("click", function () {
    var actionurl = '/Home/BackgroundImage';
    var formData = new FormData($("#frmBackground")[0]);
    var id = $('#txtBGRowID').val();
    $.ajax({
        url: actionurl,
        type: 'post',
        data: formData,
        processData: false,
        contentType: false,
        async: false,
        success: function (data) {
            if (data == "Error") {
                $('#divBGError').append("<strong>Error!</strong> You do not specified a file.").show();
            }
            else if (data == "Exception") {
                $('#divBGError').append("<strong>Error!</strong> Some error occured while uploading a file.").show();
            }
            else {
                $('#AddBackgroundImage').modal('hide');
                $("#UploadBGFile").val('');
                $('#Contents_' + id + '__Background').val(data);
                alert("Background image uploaded successfully");
            }
        }
    });
});

function ReplaceSpecialCharacters(str) {
    return str.replace(/[^a-zA-Z0-9]/g, "");
}

function split(val) {
    return val.split(/,\s*/);
}

function extractLast(term) {
    return split(term).pop();
}

function GetMasterData() {
    $("#AssetType").autocomplete({
        minLength: 0,
        source: availableTypes,
        change: function (event, ui) {
            if (this.value == '3D Model') {
                $('.btnLabel').show();
                $('#divQuiz').hide();
                $('.btnVimeoUrl').hide();
            }
            else if (this.value == 'Activity – Quizzes') {
                $('#divQuiz').show();
                $('.btnLabel').hide();
                $('.btnVimeoUrl').hide();
            }
            else if ($('#AssetType').val() == '3D Video' || $('#AssetType').val() == '2D Video') {
                $('.btnVimeoUrl').show();
                $('.btnLabel').hide();
                $('#divQuiz').hide();
            }
            else {
                $('.btnLabel').hide();
                $('#divQuiz').hide();
                $('.btnVimeoUrl').hide();
                if (ui.item == null) {
                    var terms = split(this.value);
                    var last = terms.pop();
                    if (last != '') {
                        alert('<b>' + last + '</b> is not available in master data, please contact to Administrator to add this in master data.');
                        $(this).val(this.value.substr(0, this.value.length - last.length - 2)); // removes text from input
                        $(this).effect("highlight", {}, 5000);
                        $(this).attr("style", "border: solid 1px red;");
                    }
                    return false;
                }
            }
        }
    });

    $("#Status").autocomplete({
        minLength: 0,
        source: availableStatus,
        change: function (event, ui) {
            if (ui.item == null) {
                var terms = split(this.value);
                var last = terms.pop();
                if (last != '') {
                    alert('<b>' + last + '</b> is not available in master data, please contact to Administrator to add this in master data.');
                    $(this).val(this.value.substr(0, this.value.length - last.length - 2)); // removes text from input
                    $(this).effect("highlight", {}, 5000);
                    $(this).attr("style", "border: solid 1px red;");
                }
                return false;
            }
        }
    });
    $('#AssetTags').multiselect({
        nonSelectedText: 'Select Tags',
        selectAllValue: 'multiselect-all',
        buttonWidth: '300px',
        maxHeight: 200,
        numberDisplayed: 10,
        selectedClass: 'active',
        includeSelectAllOption: false,
        enableCaseInsensitiveFiltering: true,
        enableFiltering: true,
        enableClickableOptGroups: false,
        enableCollapsibleOptGroups: false,
        onChange: function (element, checked) {
            var acId = element.val();
            var id = element.attr('class') + '__IsSelected';
            if (checked) {
                $("#" + id).val("True");
            }
            else {
                $("#" + id).val("False");
            }
        }
    });

    $('#AssetProperties').multiselect({
        nonSelectedText: 'Select Properties',
        selectAllValue: 'multiselect-all',
        buttonWidth: '300px',
        maxHeight: 200,
        numberDisplayed: 10,
        selectedClass: 'active',
        includeSelectAllOption: false,
        enableCaseInsensitiveFiltering: true,
        enableFiltering: true,
        enableClickableOptGroups: false,
        enableCollapsibleOptGroups: false,
        onChange: function (element, checked) {
            var acId = element.val();
            var id = element.attr('class') + '__IsSelected';
            if (checked) {
                $("#" + id).val("True");
            }
            else {
                $("#" + id).val("False");
            }
        }
    });   

    $(".package").each(function () {
        packageRef = $(this);
        if ($(this).val().length > 0) {
            var id = $(this).attr('name') + 'Id';
            var packageId = $("input[name='" + id + "']").val();
            $.ajax({
                url: '/Home/GetLanguagesAndGrades',
                data: { Id: packageId },
                type: 'GET',
                async: false,
                success: function (data) {
                    if (packageRef != undefined) {
                        GetLanguages(packageRef, data.Languages);
                        GetGrades(packageRef, data.Grades);
                    }
                }
            });
        }
    });

    $(".grade").each(function () {
        gradeRef = $(this);
        if ($(this).val().length > 0) {
            var id = $(this).attr('name') + 'Id';
            var gradeId = $("input[name='" + id + "']").val();
            $.ajax({
                url: '/Home/GetSyllabus',
                data: { Id: gradeId },
                type: 'GET',
                async: false,
                success: function (data) {
                    GetSyllabus(gradeRef, data.Syllabus);
                    GetStreams(gradeRef, data.Streams);
                    GetSubjects(gradeRef, data.Subjects);
                }
            });
        }
    });

    $(".subject").each(function () {
        subjectRef = $(this);
        if ($(this).val().length > 0) {
            var id = $(this).attr('name') + 'Id';
            var subjectId = $("input[name='" + id + "']").val();
            $.ajax({
                url: '/Home/GetTopics',
                data: { Id: subjectId },
                type: 'GET',
                async: false,
                success: function (data) {
                    GetTopics(subjectRef, data.Topics);
                }
            });
        }
    });

    $(".topic").each(function () {
        topicRef = $(this);
        if ($(this).val().length > 0) {
            var id = $(this).attr('name') + 'Id';
            var topicId = $("input[name='" + id + "']").val();
            $.ajax({
                url: '/Home/GetSubtopics',
                data: { Id: topicId },
                type: 'GET',
                async: false,
                success: function (data) {
                    GetSubtopics(topicRef, data.Subtopics);
                }
            });
        }
    });
}

function GetAllPlatforms() {
    $(".platform").autocomplete({
        minLength: 0,
        //selectFirst: true,
        //delay: 0,
        //autoFocus: true,
        //placeholder: "All",
        source: function (request, response) {
            response($.map(availablePlatforms, function (item) {
                if (item.Text.toLowerCase().includes(request.term.toLowerCase())) {
                    return { label: item.Text, value: item.Text, id: item.Value };
                }
            }));
        },
        //focus: function (event, ui) {
        //$(this).val(ui.item.value);
        //return false;
        //},
        select: function (event, ui) {
            var id = $(this).attr('name') + 'Id';
            $("input[name='" + id + "']").val(ui.item.id);
            $(this).val(ui.item.value);
            return false;
        },
        change: function (event, ui) {
            if (ui.item == null) {
                var terms = split(this.value);
                var last = terms.pop();
                if (last != '') {
                    alert('<b>' + last + '</b> is not available in master data, please contact to Administrator to add this in master data.');
                    $(this).val(this.value.substr(0, this.value.length - last.length - 2)); // removes text from input
                    $(this).effect("highlight", {}, 5000);
                    $(this).attr("style", "border: solid 1px red;");
                }
                else {
                    var id = $(this).attr('name') + 'Id';
                    $("input[name='" + id + "']").val('00000000-0000-0000-0000-000000000000');
                    $(this).val('');
                }
                return false;
            }
        }
    });//.val('All').data('autocomplete')._trigger('select');
    //.autocomplete("instance")._renderItem = function (ul, item) {
    //    return $("<li>")
    //        .append("<div>" + item.label + "</div>")//+ "<br>" + item.desc 
    //        .appendTo(ul);
    //};
    $(".platform").each(function () {
        if ($(this).val() == null || $(this).val() == undefined || $(this).val() == "") {
            $(this).val('All');
        }
    });
}

function GetActivityDetails() {
    $(".package").each(function () {
        packageRef = $(this);
        if ($(this).val().length > 0) {
            var id = $(this).attr('name') + 'Id';
            var packageId = $("input[name='" + id + "']").val();
            $.ajax({
                url: '/Home/GetLanguagesAndGrades',
                data: { Id: packageId },
                type: 'GET',
                async: false,
                success: function (data) {
                    if (packageRef != undefined) {
                        GetLanguages(packageRef, data.Languages);
                        GetGrades(packageRef, data.Grades);
                    }
                }
            });
        }
    });

    $(".grade").each(function () {
        gradeRef = $(this);
        if ($(this).val().length > 0) {
            var id = $(this).attr('name') + 'Id';
            var gradeId = $("input[name='" + id + "']").val();
            $.ajax({
                url: '/Home/GetSyllabus',
                data: { Id: gradeId },
                type: 'GET',
                async: false,
                success: function (data) {
                    GetSyllabus(gradeRef, data.Syllabus);
                    GetStreams(gradeRef, data.Streams);
                    GetSubjects(gradeRef, data.Subjects);
                }
            });
        }
    });

    $(".subject").each(function () {
        subjectRef = $(this);
        if ($(this).val().length > 0) {
            var id = $(this).attr('name') + 'Id';
            var subjectId = $("input[name='" + id + "']").val();
            $.ajax({
                url: '/Home/GetTopics',
                data: { Id: subjectId },
                type: 'GET',
                async: false,
                success: function (data) {
                    GetTopics(subjectRef, data.Topics);
                }
            });
        }
    });

    $(".topic").each(function () {
        topicRef = $(this);
        if ($(this).val().length > 0) {
            var id = $(this).attr('name') + 'Id';
            var topicId = $("input[name='" + id + "']").val();
            $.ajax({
                url: '/Home/GetSubtopics',
                data: { Id: topicId },
                type: 'GET',
                async: false,
                success: function (data) {
                    GetSubtopics(topicRef, data.Subtopics);
                }
            });
        }
    });
}

function GetAllLanguages() {
    $(".language").autocomplete({
        minLength: 0,
        source: function (request, response) {
            response($.map(availableLanguages, function (item) {
                if (item.Text.toLowerCase().includes(request.term.toLowerCase())) {
                    return { label: item.Text, value: item.Text, id: item.Value };
                }
            }));
        },
        select: function (event, ui) {
            var id = $(this).attr('name') + 'Id';
            $("input[name='" + id + "']").val(ui.item.id);
            $(this).val(ui.item.value);
            return false;
        },
        change: function (event, ui) {
            if (ui.item == null) {
                var terms = split(this.value);
                var last = terms.pop();
                if (last != '') {
                    alert('<b>' + last + '</b> is not available in master data, please contact to Administrator to add this in master data.');
                    $(this).val(this.value.substr(0, this.value.length - last.length - 2)); // removes text from input
                    $(this).effect("highlight", {}, 5000);
                    $(this).attr("style", "border: solid 1px red;");
                }
                else {
                    var id = $(this).attr('name') + 'Id';
                    $("input[name='" + id + "']").val('00000000-0000-0000-0000-000000000000');
                    $(this).val('');
                }
                return false;
            }
        }
    });
}

function GetPackages() {
    var packageRef;
    //console.log("Length=>" + $(".package").length);
    $(".package").each(function () {
        packageRef = $(this);
        $(this).autocomplete({
            minLength: 0,
            source: function (request, response) {
                response($.map(availablePackages, function (item) {
                    if (item.Text.toLowerCase().includes(request.term.toLowerCase())) {
                        return { label: item.Text, value: item.Text, id: item.Value };
                    }
                    //else {
                    //    return null;
                    //}
                }));
                //filteredArray = filteredArray.slice(0, 10);
                //response(filteredArray);
            },
            select: function (event, ui) {
                var id = $(this).attr('name') + 'Id';
                $("input[name='" + id + "']").val(ui.item.id);
                $(this).val(ui.item.value);
                packageRef = $(this);
                return false;
            },
            change: function (event, ui) {
                var lang = $(this).parent().parent().next().find(".planguage");
                var langId = lang.attr('name') + 'Id';
                $("input[name='" + langId + "']").val('00000000-0000-0000-0000-000000000000');
                lang.val('');

                var gr = $(this).parent().parent().next().next().find(".grade");
                var grId = gr.attr('name') + 'Id';
                $("input[name='" + grId + "']").val('00000000-0000-0000-0000-000000000000');
                gr.val('');

                var sy = gr.parent().next().find(".syllabus");
                var syId = sy.attr('name') + 'Id';
                $("input[name='" + syId + "']").val('00000000-0000-0000-0000-000000000000');
                sy.val('');

                var str = gr.parent().parent().parent().next().find(".stream");
                var strId = str.attr('name') + 'Id';
                $("input[name='" + strId + "']").val('00000000-0000-0000-0000-000000000000');
                str.val('');

                var sub = gr.parent().parent().parent().next().find(".subject");
                var subId = sub.attr('name') + 'Id';
                $("input[name='" + subId + "']").val('00000000-0000-0000-0000-000000000000');
                sub.val('');

                var t = sub.parent().next().find(".topic");
                var tId = t.attr('name') + 'Id';
                $("input[name='" + tId + "']").val('00000000-0000-0000-0000-000000000000');
                t.val('');

                var st = sub.parent().next().next().find(".subtopic");
                var stId = st.attr('name') + 'Id';
                $("input[name='" + stId + "']").val('00000000-0000-0000-0000-000000000000');
                st.val('');

                if (ui.item == null) {
                    var terms = split(this.value);
                    var last = terms.pop();
                    if (last != '') {
                        alert('<b>' + last + '</b> is not available in master data, please contact to Administrator to add this in master data.');
                        $(this).val(this.value.substr(0, this.value.length - last.length - 2)); // removes text from input
                        $(this).effect("highlight", {}, 5000);
                        $(this).attr("style", "border: solid 1px red;");
                    }
                    else {
                        var id = $(this).attr('name') + 'Id';
                        $("input[name='" + id + "']").val('00000000-0000-0000-0000-000000000000');
                        $(this).val('');

                        var name = $(this).attr('name');
                        name = name.replace("Package", "IsDeleted");
                        $("input[name='" + name + "']").val("True");
                    }
                    return false;
                }
                else {
                    var name = $(this).attr('name');
                    name = name.replace("Package", "IsDeleted");
                    $("input[name='" + name + "']").val("False");

                    $.ajax({
                        url: '/Home/GetLanguagesAndGrades',
                        data: { Id: ui.item.id },
                        type: 'GET',
                        async: false,
                        success: function (data) {
                            if (packageRef != undefined) {
                                GetLanguages(packageRef, data.Languages);
                                GetGrades(packageRef, data.Grades);
                            }
                        }
                    });
                }
            }
        });
    });
}

function GetLanguages(ref, data) {
    //$(".planguage").each(function () {
    //var element = $("#" + ref).parent().parent().next().find(".planguage"); //here ref is DOM id
    var element = $(ref).parent().parent().next().find(".planguage");
    $(element).autocomplete({
        minLength: 0,
        source: function (request, response) {
            response($.map(data, function (item) {
                if (item.Text.toLowerCase().includes(extractLast(request.term.toLowerCase()))) {
                    return { label: item.Text, value: item.Text, id: item.Value };
                }
            }));
        },
        select: function (event, ui) {
            var id = $(this).attr('name') + 'Id';
            $("input[name='" + id + "']").val(ui.item.id);
            $(this).val(ui.item.value);
            return false;
        },
        change: function (event, ui) {
            if (ui.item == null) {
                var terms = split(this.value);
                var last = terms.pop();
                if (last != '') {
                    alert('<b>' + last + '</b> is not available in master data, please contact to Administrator to add this in master data.');
                    $(this).val(this.value.substr(0, this.value.length - last.length - 2)); // removes text from input
                    $(this).effect("highlight", {}, 5000);
                    $(this).attr("style", "border: solid 1px red;");
                }
                else {
                    var id = $(this).attr('name') + 'Id';
                    $("input[name='" + id + "']").val('00000000-0000-0000-0000-000000000000');
                    $(this).val('');
                }
                return false;
            }
        }
    });
    //});
}

function GetGrades(ref, data) {
    //$(".grade").each(function () {
    var element = $(ref).parent().parent().next().next().find(".grade");
    $(element).on("keydown", function (event) {
        if (event.keyCode === $.ui.keyCode.TAB &&
            $(this).autocomplete("instance").menu.active) {
            event.preventDefault();
        }
    })
        .autocomplete({
            minLength: 0,
            source: function (request, response) {
                response($.map(data, function (item) {
                    if (item.Text.toLowerCase().includes(extractLast(request.term.toLowerCase()))) {
                        return { label: item.Text, value: item.Text, id: item.Value };
                    }
                }));
            },
            focus: function () {
                return false;
            },
            select: function (event, ui) {
                var id = $(this).attr('name') + 'Id';
                $("input[name='" + id + "']").val(ui.item.id);
                $(this).val(ui.item.value);
                element = $(this);
                return false;
            },
            change: function (event, ui) {
                var sy = $(this).parent().next().find(".syllabus");
                var syId = sy.attr('name') + 'Id';
                $("input[name='" + syId + "']").val('00000000-0000-0000-0000-000000000000');
                sy.val('');

                var str = $(this).parent().parent().parent().next().find(".stream");
                var strId = str.attr('name') + 'Id';
                $("input[name='" + strId + "']").val('00000000-0000-0000-0000-000000000000');
                str.val('');

                var sub = $(this).parent().parent().parent().next().find(".subject");
                var subId = sub.attr('name') + 'Id';
                $("input[name='" + subId + "']").val('00000000-0000-0000-0000-000000000000');
                sub.val('');

                var t = sub.parent().next().find(".topic");
                var tId = t.attr('name') + 'Id';
                $("input[name='" + tId + "']").val('00000000-0000-0000-0000-000000000000');
                t.val('');

                var st = sub.parent().next().next().find(".subtopic");
                var stId = st.attr('name') + 'Id';
                $("input[name='" + stId + "']").val('00000000-0000-0000-0000-000000000000');
                st.val('');

                if (ui.item == null) {
                    var terms = split(this.value);
                    var last = terms.pop();
                    if (last != '') {
                        alert('<b>' + last + '</b> is not available in master data, please contact to Administrator to add this in master data.');
                        $(this).val(this.value.substr(0, this.value.length - last.length - 2)); // removes text from input
                        $(this).effect("highlight", {}, 5000);
                        $(this).attr("style", "border: solid 1px red;");
                    }
                    else {
                        var id = $(this).attr('name') + 'Id';
                        $("input[name='" + id + "']").val('00000000-0000-0000-0000-000000000000');
                        $(this).val('');
                    }
                    return false;
                }
                else {
                    $.ajax({
                        url: '/Home/GetSyllabus',
                        data: { Id: ui.item.id },
                        type: 'GET',
                        async: false,
                        success: function (data) {
                            GetSyllabus(element, data.Syllabus);
                            GetStreams(element, data.Streams);
                            GetSubjects(element, data.Subjects);
                        }
                    });
                }
            }
        });
    //});
}

function GetSyllabus(ref, data) {
    //$(".syllabus").each(function () {
    var element = $(ref).parent().next().find(".syllabus");
    $(element).on("keydown", function (event) {
        if (event.keyCode === $.ui.keyCode.TAB &&
            $(this).autocomplete("instance").menu.active) {
            event.preventDefault();
        }
    })
        .autocomplete({
            minLength: 0,
            source: function (request, response) {
                response($.map(data, function (item) {
                    if (item.Text.toLowerCase().includes(extractLast(request.term.toLowerCase()))) {
                        return { label: item.Text, value: item.Text, id: item.Value };
                    }
                }));
            },
            focus: function () {
                return false;
            },
            select: function (event, ui) {
                var id = $(this).attr('name') + 'Id';
                $("input[name='" + id + "']").val(ui.item.id);
                $(this).val(ui.item.value);
                return false;
            },
            change: function (event, ui) {
                if (ui.item == null) {
                    var terms = split(this.value);
                    var last = terms.pop();
                    if (last != '') {
                        alert('<b>' + last + '</b> is not available in master data, please contact to Administrator to add this in master data.');
                        $(this).val(this.value.substr(0, this.value.length - last.length - 2)); // removes text from input
                        $(this).effect("highlight", {}, 5000);
                        $(this).attr("style", "border: solid 1px red;");
                    }
                    else {
                        var id = $(this).attr('name') + 'Id';
                        $("input[name='" + id + "']").val('00000000-0000-0000-0000-000000000000');
                        $(this).val('');
                    }
                    return false;
                }
            }
        });
    //});
}

function GetStreams(ref, data) {
    //$(".stream").each(function () {
    var element = $(ref).parent().parent().parent().next().find(".stream");
    $(element).on("keydown", function (event) {
        if (event.keyCode === $.ui.keyCode.TAB &&
            $(this).autocomplete("instance").menu.active) {
            event.preventDefault();
        }
    })
        .autocomplete({
            minLength: 0,
            source: function (request, response) {
                response($.map(data, function (item) {
                    if (item.Text.toLowerCase().includes(extractLast(request.term.toLowerCase()))) {
                        return { label: item.Text, value: item.Text, id: item.Value };
                    }
                }));
            },
            focus: function () {
                return false;
            },
            select: function (event, ui) {
                var id = $(this).attr('name') + 'Id';
                $("input[name='" + id + "']").val(ui.item.id);
                $(this).val(ui.item.value);
                return false;
            },
            change: function (event, ui) {
                if (ui.item == null) {
                    var terms = split(this.value);
                    var last = terms.pop();
                    if (last != '') {
                        alert('<b>' + last + '</b> is not available in master data, please contact to Administrator to add this in master data.');
                        $(this).val(this.value.substr(0, this.value.length - last.length - 2)); // removes text from input
                        $(this).effect("highlight", {}, 5000);
                        $(this).attr("style", "border: solid 1px red;");
                    }
                    else {
                        var id = $(this).attr('name') + 'Id';
                        $("input[name='" + id + "']").val('00000000-0000-0000-0000-000000000000');
                        $(this).val('');
                    }
                    return false;
                }
            }
        });
    //});
}

function GetSubjects(ref, data) {
    //$(".subject").each(function () {
    var element = $(ref).parent().parent().parent().next().find(".subject");
    $(element).on("keydown", function (event) {
        if (event.keyCode === $.ui.keyCode.TAB &&
            $(this).autocomplete("instance").menu.active) {
            event.preventDefault();
        }
    })
        .autocomplete({
            minLength: 0,
            source: function (request, response) {
                response($.map(data, function (item) {
                    if (item.Text.toLowerCase().includes(extractLast(request.term.toLowerCase()))) {
                        return { label: item.Text, value: item.Text, id: item.Value };
                    }
                }));
            },
            focus: function () {
                return false;
            },
            select: function (event, ui) {
                var id = $(this).attr('name') + 'Id';
                $("input[name='" + id + "']").val(ui.item.id);
                $(this).val(ui.item.value);
                element = $(this);
                return false;
            },
            change: function (event, ui) {
                var t = $(this).parent().next().find(".topic");
                var tId = t.attr('name') + 'Id';
                $("input[name='" + tId + "']").val('00000000-0000-0000-0000-000000000000');
                t.val('');

                var st = $(this).parent().next().next().find(".subtopic");
                var stId = st.attr('name') + 'Id';
                $("input[name='" + stId + "']").val('00000000-0000-0000-0000-000000000000');
                st.val('');

                if (ui.item == null) {
                    var terms = split(this.value);
                    var last = terms.pop();
                    if (last != '') {
                        alert('<b>' + last + '</b> is not available in master data, please contact to Administrator to add this in master data.');
                        $(this).val(this.value.substr(0, this.value.length - last.length - 2)); // removes text from input
                        $(this).effect("highlight", {}, 5000);
                        $(this).attr("style", "border: solid 1px red;");
                    }
                    else {
                        var id = $(this).attr('name') + 'Id';
                        $("input[name='" + id + "']").val('00000000-0000-0000-0000-000000000000');
                        $(this).val('');
                    }
                    return false;
                }
                else {
                    $.ajax({
                        url: '/Home/GetTopics',
                        data: { Id: ui.item.id },
                        type: 'GET',
                        async: false,
                        success: function (data) {
                            GetTopics(element, data.Topics);
                        }
                    });
                }
            }
        });
    //});
}

function GetTopics(ref, data) {
    //$(".topic").each(function () {
    var element = $(ref).parent().next().find(".topic");
    $(element).on("keydown", function (event) {
        if (event.keyCode === $.ui.keyCode.TAB &&
            $(this).autocomplete("instance").menu.active) {
            event.preventDefault();
        }
    })
        .autocomplete({
            minLength: 0,
            source: function (request, response) {
                response($.map(data, function (item) {
                    if (item.Text.toLowerCase().includes(extractLast(request.term.toLowerCase()))) {
                        return { label: item.Text, value: item.Text, id: item.Value };
                    }
                }));
            },
            focus: function () {
                return false;
            },
            select: function (event, ui) {
                var id = $(this).attr('name') + 'Id';
                $("input[name='" + id + "']").val(ui.item.id);
                $(this).val(ui.item.value);
                element = $(this);
                return false;
            },
            change: function (event, ui) {
                var st = $(this).parent().next().find(".subtopic");
                var stId = st.attr('name') + 'Id';
                $("input[name='" + stId + "']").val('00000000-0000-0000-0000-000000000000');
                st.val('');

                if (ui.item == null) {
                    var terms = split(this.value);
                    var last = terms.pop();
                    if (last != '') {
                        alert('<b>' + last + '</b> is not available in master data, please contact to Administrator to add this in master data.');
                        $(this).val(this.value.substr(0, this.value.length - last.length - 2)); // removes text from input
                        $(this).effect("highlight", {}, 5000);
                        $(this).attr("style", "border: solid 1px red;");
                    }
                    else {
                        var id = $(this).attr('name') + 'Id';
                        $("input[name='" + id + "']").val('00000000-0000-0000-0000-000000000000');
                        $(this).val('');
                    }
                    return false;
                }
                else {
                    $.ajax({
                        url: '/Home/GetSubtopics',
                        data: { Id: ui.item.id },
                        type: 'GET',
                        async: false,
                        success: function (data) {
                            GetSubtopics(element, data.Subtopics);
                        }
                    });
                }
            }
        });
    //});
}

function GetSubtopics(ref, data) {
    //$(".subtopic").each(function () {
    var element = $(ref).parent().next().find(".subtopic");
    $(element).on("keydown", function (event) {
        if (event.keyCode === $.ui.keyCode.TAB &&
            $(this).autocomplete("instance").menu.active) {
            event.preventDefault();
        }
    })
        .autocomplete({
            minLength: 0,
            source: function (request, response) {
                response($.map(data, function (item) {
                    if (item.Text.toLowerCase().includes(extractLast(request.term.toLowerCase()))) {
                        return { label: item.Text, value: item.Text, id: item.Value };
                    }
                }));
            },
            focus: function () {
                return false;
            },
            select: function (event, ui) {
                var id = $(this).attr('name') + 'Id';
                $("input[name='" + id + "']").val(ui.item.id);
                $(this).val(ui.item.value);
                return false;
            },
            change: function (event, ui) {
                if (ui.item == null) {
                    var terms = split(this.value);
                    var last = terms.pop();
                    if (last != '') {
                        alert('<b>' + last + '</b> is not available in master data, please contact to Administrator to add this in master data.');
                        $(this).val(this.value.substr(0, this.value.length - last.length - 2)); // removes text from input
                        $(this).effect("highlight", {}, 5000);
                        $(this).attr("style", "border: solid 1px red;");
                    }
                    else {
                        var id = $(this).attr('name') + 'Id';
                        $("input[name='" + id + "']").val('00000000-0000-0000-0000-000000000000');
                        $(this).val('');
                    }
                    return false;
                }
            }
        });
    //});
}

/* Define function for escaping user input to be treated as a literal string within a regular expression */
function escapeRegExp(string) {
    return string.replace(/[.*+?^${}()|[\]\\]/g, "\\$&");
}

/* Define functin to find and replace specified term with replacement string */
function replaceAll(str, term, replacement) {
    return str.replace(new RegExp(escapeRegExp(term), 'g'), replacement);
}

function getGuid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

function AddNameRow(obj,count1,template) {  
    if (obj != undefined && obj != null) {
        var num = $(obj).closest(".divNameParent").find('.row').length;
        var row = null;

        if (template != undefined && template != null && template != "") {
            row = $(document).find('.'+template).html();
        }
        else {
            row = $('.AddName').html();
        }

        //row = row.replace("{count}", num);
        row = replaceAll(row, "{count}", num);
        if (count1 != undefined && count1 != null) {
            row = replaceAll(row, "{count1}", count1);
        }

        row = replaceAll(row, "{guid}", getGuid());        
        $(obj).closest(".divNameParent").append(row);
    }
    else {
        var num = $('#divName').find('.row').length;
        var row = $('.AddName').html();
        //row = row.replace("{count}", num);
        row = replaceAll(row, "{count}", num);
        $('#divName').append(row);
    }
    GetAllLanguages();
}

function AddDetailRow() {
    //$('#divDetail').append($('<div id="divRow' + i + '" class="row">').load('@Url.Action("GetNewRow", "Home")'));
    //$('#divDetail').append($('<div class="row">').load("/Home/GetNewRow"));
    var num = $('#divDetail').find('.detail').length;
    var row = $('.AddDetail').html();
    row = replaceAll(row, "{count}", num);
    $('#divDetail').append(row);
    GetPackages();
}

function RemoveRow(e) {
    //$(e).parent().parent().parent().remove();
    //$(e).closest('.row').remove();
    $(e).closest('.row').hide();
    var id = $(e).attr('data-id');
    $("input[name='" + id + "']").val("True");
}

function RemoveDetailRow(e) {
    $(e).closest('.detail').hide();
    var id = $(e).attr('data-id');
    $("input[name='" + id + "']").val("True");
}

function UploadImage(rowId, id, path) {
    if (id == 0)
        alert("Save the asset details before adding thumbnail image.")
    else {
        $('#txtRowID').val(rowId);
        var file = $('#Contents_' + rowId + '__Thumbnail').val();
        $('#txtContentID').val(id);
        //$('#txtPath').val(path);
        $('#txtPath').val("thumbnails");
        $('#divError').hide();
        $('#btnUpload').val("Upload Image");
        $('#btnCancel').val("Cancel");
        if (file != null && file != '')
            $('#txtFileName').html(file);
        else
            $('#txtFileName').html("No file available");
        $('#AddImageOption').modal('show');
    }
}

function BackgroundImage(rowId, id, path) {
    if (id == 0)
        alert("Save the asset details before adding background image.")
    else {
        $('#txtBGRowID').val(rowId);
        var file = $('#Contents_' + rowId + '__Background').val();
        $('#txtBGContentID').val(id);
        $('#txtBGPath').val("background");
        $('#divBGError').hide();
        $('#btnBGUpload').val("Upload Image");
        $('#btnBGCancel').val("Cancel");
        if (file != null && file != '')
            $('#txtBGFileName').html(file);
        else
            $('#txtBGFileName').html("No background image available");
        $('#AddBackgroundImage').modal('show');
    }
}

$(document).on("click", ".lblDescription", function () {
    var id = $(this).attr('data-id');
    if (id == 0)
        alert("Save the asset details before adding description.")
    else {
        $('#divDescription').append($('<div>').load('/Home/GetDescription?Id=' + id));
        $('#btnSaveDesc').val("Save Description");
        $('#btnCancelDesc').val("Cancel");
        $('#AddDescription').modal('show');
    }
});

function AddDescRow() {
    var num = $('#divDesc').find('.row').length;
    var row = $('.AddDesc').html();
    row = replaceAll(row, "{count}", num);
    $('#divDesc').append(row);
    GetAllLanguages();
}

$('.close-div').click(function () {
    $('#divDescription').empty();
    $('#divWhatsNew').empty();
});

$('#btnSaveDesc').click(function () {
    //e.preventDefault();
    var actionurl = '/Home/SaveDescription';

    //do your own request an handle the results
    $.ajax({
        url: actionurl,
        type: 'post',
        dataType: 'text',
        data: $("#frmDesc").serialize(),
        success: function (data) {
            if (data == "Error") {
                //alert("Add the valid language to save comments");
                $('#divDescInfo').show();
            }
            else {
                $('#AddDescription').modal('hide');
                $('#divDescription').empty();
                alert(data);
            }
        }
    });
});

$(document).on("click", "#lblWhatIsNew", function () {
    var id = $(this).attr('data-id');
    if (id == 0)
        alert("Save the asset details before adding comments for What's New.")
    else {
        $('#divWhatsNew').append($('<div>').load('/Home/GetWhatsNew?Id=' + id));
        $('#btnSaveComment').val("Save Comments");
        $('#btnCancelComment').val("Cancel");
        $('#AddWhatIsNew').modal('show');
    }
});

function AddWNRow() {
    var num = $('#divWN').find('.row').length;
    var row = $('.AddWN').html();
    row = replaceAll(row, "{count}", num);
    $('#divWN').append(row);
    GetAllLanguages();
}

$('#btnSaveComment').click(function () {
    var actionurl = '/Home/SaveWhatIsNew';

    $.ajax({
        url: actionurl,
        type: 'post',
        dataType: 'text',
        data: $("#frmWhatsNew").serialize(),
        success: function (data) {
            if (data == "Error") {
                //alert("Add the valid language to save comments");
                $('#divWNInfo').show();
            }
            else {
                $('#AddWhatIsNew').modal('hide');
                $('#divWhatsNew').empty();
                alert(data);
            }
        }
    });
});

$(document).on("click", "#btnLabel", function () {
    var contentID = $(this).attr('data-id');
    if (contentID == 0)
        alert("Save the asset details before adding labels.")
    else {
        var varForm = document.createElement("form");

        var k = document.createElement("input");
        k.setAttribute('type', "hidden");
        k.setAttribute('name', "AssetContentID");
        k.setAttribute('value', contentID);
        varForm.appendChild(k);

        varForm.setAttribute('method', "get");
        varForm.setAttribute('action', "/GenerateXML/Index");
        document.body.appendChild(varForm);
        varForm.hidden = true;
        varForm.submit();
    }
})

$(document).on("click", "#lblVersionNumber", function () {
    var contentID = $(this).attr('data-id');
    $.ajax({
        url: '/Home/GetContentDetails',
        data: { Id: contentID },
        type: 'GET',
        success: function (data) {
            $('#divContent').html(data);
            $('body, html').animate({ scrollTop: $('#divContent').offset().top }, 'slow');
        }
    });
});

function getHasChanges() {
    var hasChanges = false;

    $(":input:not(:button):not([type=hidden])").each(function () {
        if ((this.type == "text" || this.type == "textarea" || this.type == "hidden") && this.defaultValue != this.value) {
            hasChanges = true;
            return false;
        }
        //else if ((this.type == "radio" || this.type == "checkbox") && this.defaultChecked != this.checked) {
        //    hasChanges = true;
        //    return false;
        //}
        else {
            if ((this.type == "select-one" || this.type == "select-multiple")) {
                for (var x = 0; x < this.length; x++) {
                    if (this.options[x].selected != this.options[x].defaultSelected) {
                        hasChanges = true;
                        return false;
                    }
                }
            }
        }
    });

    return hasChanges;
}

function UnpublishContent(e, contentID) {

    var id = e.attr('data-id');
    var message = 'Are you sure you want to Unpublish this Asset? This activity might take some time to remove the content from publish location and this content will no longer be available for users.';

    if (confirm(message, function () {
        showLoader();
        $.ajax({
            url: '/Home/UnPublishAssets',
            data: { Id: contentID },
            type: 'GET',
            async: false,
            success: function (data) {
                publishStatus = true;
                hideLoader();
                e.bootstrapToggle('toggle');
                $('#Contents_' + id + '__IsEnabled').val('false');
                alert("Asset unpublished successfully.");
            }
        });
    }, '', function () {
        publishStatus = true;
        e.bootstrapToggle('off');
    }, function () {
        publishStatus = true;
        e.bootstrapToggle('toggle');
    }));

}

function PublishContent(e, contentID) {
    var id = e.attr('data-id');
    var message = 'Are you sure you want to publish this Asset? This activity might take some time to copy the content to the publish location and this content will be available for users.';

    if (confirm(message, function () {
        showLoader();
        $.ajax({
            url: '/Home/PublishAssets',
            data: { Id: contentID },
            type: 'GET',
            async: false,
            success: function (data) {
                hideLoader();
                publishStatus = true;
                if (data == 'True') {
                    e.bootstrapToggle('toggle');
                    $('#Contents_' + id + '__IsEnabled').val('true');
                    alert("Asset published successfully.");
                }
                else {
                    e.bootstrapToggle('off');
                    var msg = 'Something went wrong! Please try again.';
                    window.location.href = '/Error/Error?message=' + msg;
                }
            },
            error: function (xhr) {
                window.location.href = '/Error/Error';
            }
        });
    }, '', function () {
        publishStatus = true;
        e.bootstrapToggle('on');
    }, function () {
        publishStatus = true;
        e.bootstrapToggle('toggle');
    }));
}

function UnpublishModule(e, contentID) {

    var id = e.attr('data-id');
    var message = 'Are you sure you want to Unpublish this Module?';

    if (confirm(message, function () {
        showLoader();
        $.ajax({
            url: '/Module/UnPublishModule',
            data: { Id: contentID },
            type: 'GET',
            async: false,
            success: function (data) {
                publishStatus = true;
                hideLoader();
                e.bootstrapToggle('toggle');
                $('#Contents_' + id + '__IsEnabled').val('false');
                alert("Module unpublished successfully.");
            }
        });
    }, '', function () {
        publishStatus = true;
        e.bootstrapToggle('off');
    }, function () {
        publishStatus = true;
        e.bootstrapToggle('toggle');
    }));

}

function PublishModule(e, contentID) {
    var id = e.attr('data-id');
    var message = 'Are you sure you want to publish this Module?';

    if (confirm(message, function () {
        showLoader();
        $.ajax({
            url: '/Module/PublishModule',
            data: { Id: contentID },
            type: 'GET',
            async: false,
            success: function (data) {
                hideLoader();
                publishStatus = true;
                if (data == 'True') {
                    e.bootstrapToggle('toggle');
                    $('#Contents_' + id + '__IsEnabled').val('true');
                    alert("Module published successfully.");
                }
                else {
                    e.bootstrapToggle('off');
                    var msg = 'Something went wrong! Please try again.';
                    window.location.href = '/Error/Error?message=' + msg;
                }
            },
            error: function (xhr) {
                window.location.href = '/Error/Error';
            }
        });
    }, '', function () {
        publishStatus = true;
        e.bootstrapToggle('on');
    }, function () {
        publishStatus = true;
        e.bootstrapToggle('toggle');
    }));

}