﻿
showLoader();
window.onload = function () {
    hideLoader();
};

$(document).ready(function () {
    /*$('#collapseOne').collapse("hide");*/
    $('.table-responsive').on('show.bs.dropdown', function () {
        $('.table-responsive').css("overflow", "inherit");
    });

    $('.table-responsive').on('hide.bs.dropdown', function () {
        $('.table-responsive').css("overflow", "auto");
    });

    //$('.bodyScroller').mCustomScrollbar({
    //    theme: "dark-3", autoHideScrollbar: true,
    //    advanced: { updateOnContentResize: true, updateOnBrowserResize: true }
    //});

    $('[data-toggle="tooltip"]').tooltip()

    $(document).ajaxStart(function () {
        showLoader();
    });

    $(document).ajaxComplete(function (event, xhr, settings) {

        if ($(".modal-backdrop").length > 0 && (!!navigator.userAgent.match(/Trident/g) || !!navigator.userAgent.match(/MSIE/g)))
            $(".modal-backdrop").remove();

        hideLoader();
        if (xhr.status == 500) {
            alert("Your session expired. Please login again.")
            window.location.href = "/Account/Login";
        }
    });

    function reposition() {        
        var modal = $(this), dialog = modal.find('.modal-content');
        modal.css('display', 'block');

        // Dividing by two centers the modal exactly, but dividing by three
        // or four works better for larger screens.
        dialog.css("margin-top", Math.max(0, ($(window).height() - dialog.height()) / 2));
    }
    // Reposition when a modal is shown
    $('.modal').on('show.bs.modal', reposition);
    // Reposition when the window is resized
    $(window).on('resize', function () {
        $('.modal:visible').each(reposition);
    });
});

(function ($) {
    $(window).on("load", function () {
        $(".content").mCustomScrollbar();
    });
})(jQuery);

function checkEmail(e) {
    if ($(e).val() != "" && $(e).attr("readonly") != "readonly") {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;

        $.ajax({
            url: '/Base/CheckEmailAlreadyExist?email=' + $(e).val(),
            success: function (data) { if (data) { alert("This email " + "'" + $(e).val() + "'" + " already exists in our records!!"); $(e).val(''); $(e).focus(); } }
        });
    }
}

function UpdateScrollPosition(id) {
    // Remove "link" from the ID
    id = id.replace("link", "");
    // Scroll
    $('html,body').animate({
        scrollTop: $("#" + id).offset().top
    },
        'slow');
}

function showLoader() {
    if ($('.loadingoverlay').length == 0)
        $.LoadingOverlay("show", { image: '../Content/images/loading.gif' });
}
function hideLoader() {
    $.LoadingOverlay("hide");
}

(function () {
    var _alert = window.alert;
    window.alert = function (str) {
        $(document.createElement('div')).attr({
            title: 'Message',
            'class': 'dialog'
            //}).html('<pre>' + unescape(str)+ '</pre>').dialog({
        }).html(unescape(str)).dialog({
            position: {
                my: "top center",
                at: "top center"
            },
            overlay: {
                backgroundColor: "red",
                opacity: 0.5
            },
            show: {
                effect: "blind",
                duration: 200
            },
            hide: {
                effect: "blind",
                duration: 200
            },
            buttons: {
                "OK": function () {
                    $(this).dialog('close');
                }
            },
            draggable: false,
            zIndex: 99999999,
            modal: true,
            resizable: false,
            width: '550px'
        });
    };
})();

window.confirm = function (message, callback, caption, cancel_callback, close_callback) {
    caption = caption || 'Confirmation'
    $(document.createElement('div')).attr({
        title: caption,
        'class': 'dialog'
    }).html(unescape(message)).dialog({
        position: {
            my: "center center",
            at: "center center"
        },
        overlay: {
            backgroundColor: "red",
            opacity: 0.5
        },
        show: {
            effect: "blind",
            duration: 200
        },
        hide: {
            effect: "blind",
            duration: 200
        },
        buttons: {
            "OK": function () {
                $(this).dialog('close');
                callback();
                return true;
            },
            "Cancel": function () {
                $(this).dialog('close');

                if (typeof cancel_callback != 'undefined')
                    cancel_callback();

                return false;
            }
        },
        close: function () {
            if (typeof close_callback != 'undefined')
                close_callback();

            $(this).remove();
        },
        draggable: false,
        modal: true,
        zIndex: 1051,
        resizable: false,
        width: '550px'
    });
};

function onKendoGrid_ItemDelete(e) {
    e.preventDefault();
    if (confirm("Are you sure you want to delete this record?", function () {
        var grid = $("#grid").data("kendoGrid");
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        grid.dataSource.remove(dataItem)
        grid.dataSource.sync()
    }));
    return false;
}

function imgError(image) {
    image.onerror = "";
    image.src = "/Content/images/banner.jpg";
    return true;
}

function ShowNoDataMsgGrid(e) {
    var grid = e.sender;
    if (grid.dataSource.total() == 0) {
        var colCount = grid.columns.length;
        $(e.sender.wrapper)
            .find('tbody')
            .append('<tr class="kendo-data-row"><td colspan="' + colCount + '" class="no-data">No record found.</td></tr>');
    }
}

function ClearData() {
    window.localStorage.clear();//removeItem("kendo-grid-options");
}

function NavigatetoPrevious() {
    parent.history.back();
    return false;
}

function openPopUp(item) {
    //var path = item.Path.split("~").join("..");
    if (item.Type == '.jpg' || item.Type == '.JPG' || item.Type == '.jpeg' || item.Type == '.png' || item.Type == '.gif') {
        //alert("<img width='100%' height='100%' src='" + item.Path + "' />");
        $("#divContent").html("<img width='100%' height='100%' src='" + item.Path + "' />");
    }

    else if (item.Type == '.txt' || item.Type == '.doc' || item.Type == '.docx') {
        var txtFile = item.Path;
        $.ajax({
            url: txtFile,
            success: function (data) {
                $("#divContent").html("<pre width='100%' height='300px'>" + data + "</pre>");
            }
        });

    }
    else if (item.Type == '.docx333') {
        $("#divContent").html(" <iframe class='doc' height='300px' src='https://docs.google.com/gview?url=" + item.Path + "&embedded=true'></iframe>");

    }
    else if (item.Type == '.mp4') {
        $("#divContent").html("<video controls='controls' type='video/mp4'  width='100%' height='300px' src='" + item.Path + "' />");

    }
    else if (item.Type == '.pdf') {
        $("#divContent").html(" <embed src='" + item.Path + "' height='375px' width='100%' alt='pdf' pluginspage='http://www.adobe.com/products/acrobat/readstep2.html'>");
    }
    else {
        $("#divContent").html("Format not accepted ,<a href='" + item.Path + "' download>Click here to download </a>");
    }

    $("#ContentModal").modal('show');
}
