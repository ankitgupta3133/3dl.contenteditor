﻿function ValidateForm() {
    if ($("#frmUser").valid()) {
        showLoader();
    }
}

$('#UploadFile').change(function () {
    var i = $(this).next('label').clone();
    var file = $('#UploadFile')[0].files[0].name;
    $(this).next('label').text(file);
});

$("#btnUpload").on("click", function () {
    var actionurl = '/Home/UploadImage';
    var formData = new FormData($("#frmImage")[0]);
    var id = $('#txtRowID').val();
    $.ajax({
        url: actionurl,
        type: 'post',
        data: formData,
        processData: false,
        contentType: false,
        async: false,
        success: function (data) {
            if (data == "Error") {
                $('#divError').append("<strong>Error!</strong> You do not specified a file.").show();
            }
            else if (data == "Exception") {
                $('#divError').append("<strong>Error!</strong> Some error occured while uploading a file.").show();
            }
            else {
                $('#AddImageOption').modal('hide');
                $("#UploadFile").val('');
                $('#Contents_' + id + '__Thumbnail').val(data);
                alert("Thumbnail image uploaded successfully");
            }
        }
    });
});

function ReplaceSpecialCharacters(str) {
    return str.replace(/[^a-zA-Z0-9]/g, "");
}

function split(val) {
    return val.split(/,\s*/);
}

function extractLast(term) {
    return split(term).pop();
}

function GetMasterData() {
    $(".role").autocomplete({
        minLength: 0,
        source: function (request, response) {
            response($.map(availableRoles, function (item) {
                if (item.Text.toLowerCase().includes(request.term.toLowerCase())) {
                    return { label: item.Text, value: item.Text, id: item.Value };
                }
            }));
        },
        select: function (event, ui) {
            var id = $(this).attr('name') + 'Id';
            $("input[name='" + id + "']").val(ui.item.id);
            $(this).val(ui.item.value);
            return false;
        },
        change: function (event, ui) {
            if (ui.item == null) {
                var terms = split(this.value);
                var last = terms.pop();
                if (last != '') {
                    alert('<b>' + last + '</b> is not available in master data, please contact to Administrator to add this in master data.');
                    $(this).val(this.value.substr(0, this.value.length - last.length - 2)); // removes text from input
                    $(this).effect("highlight", {}, 5000);
                    $(this).attr("style", "border: solid 1px red;");
                }
                else {
                    var id = $(this).attr('name') + 'Id';
                    $("input[name='" + id + "']").val('00000000-0000-0000-0000-000000000000');
                    $(this).val('');
                }
                return false;
            }
        }
    });

    $(".account").each(function () {
        if ($(this).val().length > 0) {
            var id = $(this).attr('name') + 'Id';
            var accountId = $("input[name='" + id + "']").val();
            $.ajax({
                url: '/UserManagement/GetSubscriptions',
                data: { AccountId: accountId },
                type: 'GET',
                async: false,
                success: function (data) {
                    GetSubscriptions(data.Subscriptions);
                }
            });
        }
    });

    $(".subscription").each(function () {
        subscriptionRef = $(this);
        if ($(this).val().length > 0) {
            var id = $(this).attr('name') + 'Id';
            var subscriptionId = $("input[name='" + id + "']").val();
            $.ajax({
                url: '/UserManagement/GetPackages',
                data: { SubscriptionId: subscriptionId },
                type: 'GET',
                async: false,
                success: function (data) {
                    GetPackages(subscriptionRef, data.Packages);
                    //GetExpiryDate(subscriptionRef, data.Subscriptions);
                }
            });
        }
    });

    $(".package").each(function () {
        packageRef = $(this);
        if ($(this).val().length > 0) {
            var id = $(this).attr('name') + 'Id';
            var packageId = $("input[name='" + id + "']").val();
            $.ajax({
                url: '/Home/GetLanguagesAndGrades',
                data: { Id: packageId },
                type: 'GET',
                async: false,
                success: function (data) {
                    if (packageRef != undefined) {
                        GetLanguages(packageRef, data.Languages);
                        GetGrades(packageRef, data.Grades);
                    }
                }
            });
        }
    });

    $(".grade").each(function () {
        gradeRef = $(this);
        if ($(this).val().length > 0) {
            var id = $(this).attr('name') + 'Id';
            var gradeId = $("input[name='" + id + "']").val();
            $.ajax({
                url: '/Home/GetSyllabus',
                data: { Id: gradeId },
                type: 'GET',
                async: false,
                success: function (data) {
                    GetSyllabus(gradeRef, data.Syllabus);
                    GetStreams(gradeRef, data.Streams);
                    GetSubjects(gradeRef, data.Subjects);
                }
            });
        }
    });

    //$(".subject").each(function () {
    //    subjectRef = $(this);
    //    if ($(this).val().length > 0) {
    //        var id = $(this).attr('name') + 'Id';
    //        var subjectId = $("input[name='" + id + "']").val();
    //        $.ajax({
    //            url: '/Home/GetTopics',
    //            data: { Id: subjectId },
    //            type: 'GET',
    //            async: false,
    //            success: function (data) {
    //                GetTopics(subjectRef, data.Topics);
    //            }
    //        });
    //    }
    //});
}

function GetAllPlatforms() {
    $(".platform").autocomplete({
        minLength: 0,
        //selectFirst: true,
        //delay: 0,
        //autoFocus: true,
        //placeholder: "All",
        source: function (request, response) {
            response($.map(availablePlatforms, function (item) {
                if (item.Text.toLowerCase().includes(request.term.toLowerCase())) {
                    return { label: item.Text, value: item.Text, id: item.Value };
                }
            }));
        },
        //focus: function (event, ui) {
        //$(this).val(ui.item.value);
        //return false;
        //},
        select: function (event, ui) {
            var id = $(this).attr('name') + 'Id';
            $("input[name='" + id + "']").val(ui.item.id);
            $(this).val(ui.item.value);
            return false;
        },
        change: function (event, ui) {
            if (ui.item == null) {
                var terms = split(this.value);
                var last = terms.pop();
                if (last != '') {
                    alert('<b>' + last + '</b> is not available in master data, please contact to Administrator to add this in master data.');
                    $(this).val(this.value.substr(0, this.value.length - last.length - 2)); // removes text from input
                    $(this).effect("highlight", {}, 5000);
                    $(this).attr("style", "border: solid 1px red;");
                }
                else {
                    var id = $(this).attr('name') + 'Id';
                    $("input[name='" + id + "']").val('00000000-0000-0000-0000-000000000000');
                    $(this).val('');
                }
                return false;
            }
        }
    });//.val('All').data('autocomplete')._trigger('select');
    //.autocomplete("instance")._renderItem = function (ul, item) {
    //    return $("<li>")
    //        .append("<div>" + item.label + "</div>")//+ "<br>" + item.desc 
    //        .appendTo(ul);
    //};
    $(".platform").each(function () {
        if ($(this).val() == null || $(this).val() == undefined || $(this).val() == "") {
            $(this).val('All');
        }
    });
}

function GetAllLanguages() {
    $(".language").autocomplete({
        minLength: 0,
        source: function (request, response) {
            response($.map(availableLanguages, function (item) {
                if (item.Text.toLowerCase().includes(request.term.toLowerCase())) {
                    return { label: item.Text, value: item.Text, id: item.Value };
                }
            }));
        },
        select: function (event, ui) {
            var id = $(this).attr('name') + 'Id';
            $("input[name='" + id + "']").val(ui.item.id);
            $(this).val(ui.item.value);
            return false;
        },
        change: function (event, ui) {
            if (ui.item == null) {
                var terms = split(this.value);
                var last = terms.pop();
                if (last != '') {
                    alert('<b>' + last + '</b> is not available in master data, please contact to Administrator to add this in master data.');
                    $(this).val(this.value.substr(0, this.value.length - last.length - 2)); // removes text from input
                    $(this).effect("highlight", {}, 5000);
                    $(this).attr("style", "border: solid 1px red;");
                }
                else {
                    var id = $(this).attr('name') + 'Id';
                    $("input[name='" + id + "']").val('00000000-0000-0000-0000-000000000000');
                    $(this).val('');
                }
                return false;
            }
        }
    });
}

function GetAccounts() {
    $(".account").autocomplete({
        minLength: 0,
        source: function (request, response) {
            response($.map(availableAccounts, function (item) {
                if (item.Text.toLowerCase().includes(request.term.toLowerCase())) {
                    return { label: item.Text, value: item.Text, id: item.Value };
                }
            }));
        },
        select: function (event, ui) {
            var id = $(this).attr('name') + 'Id';
            $("input[name='" + id + "']").val(ui.item.id);
            $(this).val(ui.item.value);
            return false;
        },
        change: function (event, ui) {
            if (ui.item == null) {
                var terms = split(this.value);
                var last = terms.pop();
                if (last != '') {
                    alert('<b>' + last + '</b> is not available in master data, please contact to Administrator to add this in master data.');
                    $(this).val(this.value.substr(0, this.value.length - last.length - 2)); // removes text from input
                    $(this).effect("highlight", {}, 5000);
                    $(this).attr("style", "border: solid 1px red;");
                }
                else {
                    var id = $(this).attr('name') + 'Id';
                    $("input[name='" + id + "']").val('00000000-0000-0000-0000-000000000000');
                    $(this).val('');
                }
                return false;
            }
            else {
                $.ajax({
                    url: '/UserManagement/GetSubscriptions',
                    data: { AccountId: ui.item.id },
                    type: 'GET',
                    async: false,
                    success: function (data) {
                        GetSubscriptions(data.Subscriptions);
                    }
                });
            }
        }
    });
}

function RemoveDetails(ref) {
    var id = ref.attr('name') + 'Id';
    $("input[name='" + id + "']").val('00000000-0000-0000-0000-000000000000');
    ref.val('');

    var lang = ref.parent().parent().next().find(".planguage");
    var langId = lang.attr('name') + 'Id';
    $("input[name='" + langId + "']").val('00000000-0000-0000-0000-000000000000');
    lang.val('');

    var gr = ref.parent().parent().next().next().find(".grade");
    var grId = gr.attr('name') + 'Id';
    $("input[name='" + grId + "']").val('00000000-0000-0000-0000-000000000000');
    gr.val('');

    var sy = gr.parent().next().find(".syllabus");
    var syId = sy.attr('name') + 'Id';
    $("input[name='" + syId + "']").val('00000000-0000-0000-0000-000000000000');
    sy.val('');

    var str = gr.parent().parent().parent().next().find(".stream");
    var strId = str.attr('name') + 'Id';
    $("input[name='" + strId + "']").val('00000000-0000-0000-0000-000000000000');
    str.val('');

    var sub = gr.parent().parent().parent().next().find(".subject");
    var subId = sub.attr('name') + 'Id';
    $("input[name='" + subId + "']").val('00000000-0000-0000-0000-000000000000');
    sub.val('');

    var t = sub.parent().next().find(".topic");
    var tId = t.attr('name') + 'Id';
    $("input[name='" + tId + "']").val('00000000-0000-0000-0000-000000000000');
    t.val('');

    var subscription = sub.parent().next().next().find(".subscription");
    var subscriptionId = subscription.attr('name') + 'Id';
    $("input[name='" + subscriptionId + "']").val('00000000-0000-0000-0000-000000000000');
    subscription.val('');
}

function GetSubscriptions(data) {
    var subscriptionRef;
    $(".subscription").each(function () {
        subscriptionRef = $(this);

        //clean value from all the fields on change of account
        //RemoveDetails(subscriptionRef);

        $(this).autocomplete({
            minLength: 0,
            source: function (request, response) {
                response($.map(data, function (item) {
                    if (item.Text.toLowerCase().includes(request.term.toLowerCase())) {
                        return { label: item.Text, value: item.Text, id: item.Value };
                    }
                }));
            },
            select: function (event, ui) {
                var id = $(this).attr('name') + 'Id';
                $("input[name='" + id + "']").val(ui.item.id);
                $(this).val(ui.item.value);
                subscriptionRef = $(this);
                return false;
            },
            change: function (event, ui) {
                var pack = $(this).parent().parent().next().find(".package");
                var packId = pack.attr('name') + 'Id';
                $("input[name='" + packId + "']").val('00000000-0000-0000-0000-000000000000');
                pack.val('');

                var lang = $(this).parent().parent().next().next().find(".planguage");
                var langId = lang.attr('name') + 'Id';
                $("input[name='" + langId + "']").val('00000000-0000-0000-0000-000000000000');
                lang.val('');

                var gr = lang.parent().next().find(".grade");
                var grId = gr.attr('name') + 'Id';
                $("input[name='" + grId + "']").val('00000000-0000-0000-0000-000000000000');
                gr.val('');

                var sy = gr.parent().parent().parent().next().find(".syllabus");
                var syId = sy.attr('name') + 'Id';
                $("input[name='" + syId + "']").val('00000000-0000-0000-0000-000000000000');
                sy.val('');

                var str = gr.parent().parent().parent().next().find(".stream");
                var strId = str.attr('name') + 'Id';
                $("input[name='" + strId + "']").val('00000000-0000-0000-0000-000000000000');
                str.val('');

                var sub = str.parent().next().find(".subject");
                var subId = sub.attr('name') + 'Id';
                $("input[name='" + subId + "']").val('00000000-0000-0000-0000-000000000000');
                sub.val('');

                //var t = str.parent().next().next().find(".topic");
                //var tId = t.attr('name') + 'Id';
                //$("input[name='" + tId + "']").val('00000000-0000-0000-0000-000000000000');
                //t.val('');

                if (ui.item == null) {
                    var terms = split(this.value);
                    var last = terms.pop();
                    if (last != '') {
                        alert('<b>' + last + '</b> is not available in master data, please contact to Administrator to add this in master data.');
                        $(this).val(this.value.substr(0, this.value.length - last.length - 2)); // removes text from input
                        $(this).effect("highlight", {}, 5000);
                        $(this).attr("style", "border: solid 1px red;");
                    }
                    else {
                        var id = $(this).attr('name') + 'Id';
                        $("input[name='" + id + "']").val('00000000-0000-0000-0000-000000000000');
                        $(this).val('');

                        var name = $(this).attr('name');
                        name = name.replace("Subscription", "IsDeleted");
                        $("input[name='" + name + "']").val("True");
                    }
                    return false;
                }
                else {
                    var name = $(this).attr('name');
                    name = name.replace("Subscription", "IsDeleted");
                    $("input[name='" + name + "']").val("False");
                    $.ajax({
                        url: '/UserManagement/GetPackages',
                        data: { SubscriptionId: ui.item.id },
                        type: 'GET',
                        async: false,
                        success: function (data) {
                            if (subscriptionRef != undefined) {
                                GetPackages(subscriptionRef, data.Packages);
                                GetExpiryDate(subscriptionRef, data.Subscriptions);
                            }
                        }
                    });
                }
            }
        });
    });
}

function GetExpiryDate(ref, data) {
    debugger;
    var element = $(ref).parent().parent().parent().parent().next().find(".expirydate");
    $(element).val(data[0].Text);
    var id = $(element).attr('data-id');
    $("input[name='" + id + "']").val($(element).val());
}

function GetPackages(ref, data) {
    var element = $(ref).parent().parent().next().find(".package");
    $(element).on("keydown", function (event) {
        if (event.keyCode === $.ui.keyCode.TAB &&
            $(this).autocomplete("instance").menu.active) {
            event.preventDefault();
        }
    }).autocomplete({
        minLength: 0,
        source: function (request, response) {
            response($.map(data, function (item) {
                if (item.Text.toLowerCase().includes(request.term.toLowerCase())) {
                    return { label: item.Text, value: item.Text, id: item.Value };
                }
            }));
        },
        focus: function () {
            return false;
        },
        select: function (event, ui) {
            var id = $(this).attr('name') + 'Id';
            $("input[name='" + id + "']").val(ui.item.id);
            $(this).val(ui.item.value);
            packageRef = $(this);
            return false;
        },
        change: function (event, ui) {
            var lang = $(this).parent().next().find(".planguage");
            var langId = lang.attr('name') + 'Id';
            $("input[name='" + langId + "']").val('00000000-0000-0000-0000-000000000000');
            lang.val('');

            var gr = $(this).parent().next().next().find(".grade");
            var grId = gr.attr('name') + 'Id';
            $("input[name='" + grId + "']").val('00000000-0000-0000-0000-000000000000');
            gr.val('');

            var sy = gr.parent().parent().parent().next().find(".syllabus");
            var syId = sy.attr('name') + 'Id';
            $("input[name='" + syId + "']").val('00000000-0000-0000-0000-000000000000');
            sy.val('');

            var str = gr.parent().parent().parent().next().find(".stream");
            var strId = str.attr('name') + 'Id';
            $("input[name='" + strId + "']").val('00000000-0000-0000-0000-000000000000');
            str.val('');

            var sub = str.parent().next().find(".subject");
            var subId = sub.attr('name') + 'Id';
            $("input[name='" + subId + "']").val('00000000-0000-0000-0000-000000000000');
            sub.val('');

            //var t = str.parent().next().next().find(".topic");
            //var tId = t.attr('name') + 'Id';
            //$("input[name='" + tId + "']").val('00000000-0000-0000-0000-000000000000');
            //t.val('');

            if (ui.item == null) {
                var terms = split(this.value);
                var last = terms.pop();
                if (last != '') {
                    alert('<b>' + last + '</b> is not available in master data, please contact to Administrator to add this in master data.');
                    $(this).val(this.value.substr(0, this.value.length - last.length - 2)); // removes text from input
                    $(this).effect("highlight", {}, 5000);
                    $(this).attr("style", "border: solid 1px red;");
                }
                else {
                    var id = $(this).attr('name') + 'Id';
                    $("input[name='" + id + "']").val('00000000-0000-0000-0000-000000000000');
                    $(this).val('');
                }
                return false;
            }
            else {
                $.ajax({
                    url: '/Home/GetLanguagesAndGrades',
                    data: { Id: ui.item.id },
                    type: 'GET',
                    async: false,
                    success: function (data) {
                        if (element != undefined) {
                            GetLanguages(element, data.Languages);
                            GetGrades(element, data.Grades);
                        }
                    }
                });
            }
        }
    });
}

function GetLanguages(ref, data) {
    var element = $(ref).parent().next().find(".planguage");
    $(element).autocomplete({
        minLength: 0,
        source: function (request, response) {
            response($.map(data, function (item) {
                if (item.Text.toLowerCase().includes(extractLast(request.term.toLowerCase()))) {
                    return { label: item.Text, value: item.Text, id: item.Value };
                }
            }));
        },
        select: function (event, ui) {
            var id = $(this).attr('name') + 'Id';
            $("input[name='" + id + "']").val(ui.item.id);
            $(this).val(ui.item.value);
            return false;
        },
        change: function (event, ui) {
            if (ui.item == null) {
                var terms = split(this.value);
                var last = terms.pop();
                if (last != '') {
                    alert('<b>' + last + '</b> is not available in master data, please contact to Administrator to add this in master data.');
                    $(this).val(this.value.substr(0, this.value.length - last.length - 2)); // removes text from input
                    $(this).effect("highlight", {}, 5000);
                    $(this).attr("style", "border: solid 1px red;");
                }
                else {
                    var id = $(this).attr('name') + 'Id';
                    $("input[name='" + id + "']").val('00000000-0000-0000-0000-000000000000');
                    $(this).val('');
                }
                return false;
            }
        }
    });
}

function GetGrades(ref, data) {
    var element = $(ref).parent().next().next().find(".grade");
    $(element).on("keydown", function (event) {
        if (event.keyCode === $.ui.keyCode.TAB &&
            $(this).autocomplete("instance").menu.active) {
            event.preventDefault();
        }
    })
        .autocomplete({
            minLength: 0,
            source: function (request, response) {
                response($.map(data, function (item) {
                    if (item.Text.toLowerCase().includes(extractLast(request.term.toLowerCase()))) {
                        return { label: item.Text, value: item.Text, id: item.Value };
                    }
                }));
            },
            focus: function () {
                return false;
            },
            select: function (event, ui) {
                var id = $(this).attr('name') + 'Id';
                $("input[name='" + id + "']").val(ui.item.id);
                $(this).val(ui.item.value);
                element = $(this);
                return false;
            },
            change: function (event, ui) {
                var sy = $(this).parent().parent().parent().next().find(".syllabus");
                var syId = sy.attr('name') + 'Id';
                $("input[name='" + syId + "']").val('00000000-0000-0000-0000-000000000000');
                sy.val('');

                var str = $(this).parent().parent().parent().next().find(".stream");
                var strId = str.attr('name') + 'Id';
                $("input[name='" + strId + "']").val('00000000-0000-0000-0000-000000000000');
                str.val('');

                var sub = str.parent().next().find(".subject");
                var subId = sub.attr('name') + 'Id';
                $("input[name='" + subId + "']").val('00000000-0000-0000-0000-000000000000');
                sub.val('');

                var t = str.parent().next().next().find(".topic");
                var tId = t.attr('name') + 'Id';
                $("input[name='" + tId + "']").val('00000000-0000-0000-0000-000000000000');
                t.val('');

                if (ui.item == null) {
                    var terms = split(this.value);
                    var last = terms.pop();
                    if (last != '') {
                        alert('<b>' + last + '</b> is not available in master data, please contact to Administrator to add this in master data.');
                        $(this).val(this.value.substr(0, this.value.length - last.length - 2)); // removes text from input
                        $(this).effect("highlight", {}, 5000);
                        $(this).attr("style", "border: solid 1px red;");
                    }
                    else {
                        var id = $(this).attr('name') + 'Id';
                        $("input[name='" + id + "']").val('00000000-0000-0000-0000-000000000000');
                        $(this).val('');
                    }
                    return false;
                }
                else {
                    $.ajax({
                        url: '/Home/GetSyllabus',
                        data: { Id: ui.item.id },
                        type: 'GET',
                        async: false,
                        success: function (data) {
                            GetSyllabus(element, data.Syllabus);
                            GetStreams(element, data.Streams);
                            GetSubjects(element, data.Subjects);
                        }
                    });
                }
            }
        });
}

function GetSyllabus(ref, data) {
    var element = $(ref).parent().parent().parent().next().find(".syllabus");
    $(element).on("keydown", function (event) {
        if (event.keyCode === $.ui.keyCode.TAB &&
            $(this).autocomplete("instance").menu.active) {
            event.preventDefault();
        }
    })
        .autocomplete({
            minLength: 0,
            source: function (request, response) {
                response($.map(data, function (item) {
                    if (item.Text.toLowerCase().includes(extractLast(request.term.toLowerCase()))) {
                        return { label: item.Text, value: item.Text, id: item.Value };
                    }
                }));
            },
            focus: function () {
                return false;
            },
            select: function (event, ui) {
                var id = $(this).attr('name') + 'Id';
                $("input[name='" + id + "']").val(ui.item.id);
                $(this).val(ui.item.value);
                return false;
            },
            change: function (event, ui) {
                if (ui.item == null) {
                    var terms = split(this.value);
                    var last = terms.pop();
                    if (last != '') {
                        alert('<b>' + last + '</b> is not available in master data, please contact to Administrator to add this in master data.');
                        $(this).val(this.value.substr(0, this.value.length - last.length - 2)); // removes text from input
                        $(this).effect("highlight", {}, 5000);
                        $(this).attr("style", "border: solid 1px red;");
                    }
                    else {
                        var id = $(this).attr('name') + 'Id';
                        $("input[name='" + id + "']").val('00000000-0000-0000-0000-000000000000');
                        $(this).val('');
                    }
                    return false;
                }
            }
        });
}

function GetStreams(ref, data) {
    var element = $(ref).parent().parent().parent().next().find(".stream");
    $(element).on("keydown", function (event) {
        if (event.keyCode === $.ui.keyCode.TAB &&
            $(this).autocomplete("instance").menu.active) {
            event.preventDefault();
        }
    })
        .autocomplete({
            minLength: 0,
            source: function (request, response) {
                response($.map(data, function (item) {
                    if (item.Text.toLowerCase().includes(extractLast(request.term.toLowerCase()))) {
                        return { label: item.Text, value: item.Text, id: item.Value };
                    }
                }));
            },
            focus: function () {
                return false;
            },
            select: function (event, ui) {
                var id = $(this).attr('name') + 'Id';
                $("input[name='" + id + "']").val(ui.item.id);
                $(this).val(ui.item.value);
                return false;
            },
            change: function (event, ui) {
                if (ui.item == null) {
                    var terms = split(this.value);
                    var last = terms.pop();
                    if (last != '') {
                        alert('<b>' + last + '</b> is not available in master data, please contact to Administrator to add this in master data.');
                        $(this).val(this.value.substr(0, this.value.length - last.length - 2)); // removes text from input
                        $(this).effect("highlight", {}, 5000);
                        $(this).attr("style", "border: solid 1px red;");
                    }
                    else {
                        var id = $(this).attr('name') + 'Id';
                        $("input[name='" + id + "']").val('00000000-0000-0000-0000-000000000000');
                        $(this).val('');
                    }
                    return false;
                }
            }
        });
}

function GetSubjects(ref, data) {
    var element = $(ref).parent().parent().parent().next().find(".subject");
    $(element).on("keydown", function (event) {
        if (event.keyCode === $.ui.keyCode.TAB &&
            $(this).autocomplete("instance").menu.active) {
            event.preventDefault();
        }
    })
        .autocomplete({
            minLength: 0,
            source: function (request, response) {
                response($.map(data, function (item) {
                    if (item.Text.toLowerCase().includes(extractLast(request.term.toLowerCase()))) {
                        return { label: item.Text, value: item.Text, id: item.Value };
                    }
                }));
            },
            focus: function () {
                return false;
            },
            select: function (event, ui) {
                var id = $(this).attr('name') + 'Id';
                $("input[name='" + id + "']").val(ui.item.id);
                $(this).val(ui.item.value);
                element = $(this);
                return false;
            },
            change: function (event, ui) {
                //var t = $(this).parent().next().find(".topic");
                //var tId = t.attr('name') + 'Id';
                //$("input[name='" + tId + "']").val('00000000-0000-0000-0000-000000000000');
                //t.val('');

                if (ui.item == null) {
                    var terms = split(this.value);
                    var last = terms.pop();
                    if (last != '') {
                        alert('<b>' + last + '</b> is not available in master data, please contact to Administrator to add this in master data.');
                        $(this).val(this.value.substr(0, this.value.length - last.length - 2)); // removes text from input
                        $(this).effect("highlight", {}, 5000);
                        $(this).attr("style", "border: solid 1px red;");
                    }
                    else {
                        var id = $(this).attr('name') + 'Id';
                        $("input[name='" + id + "']").val('00000000-0000-0000-0000-000000000000');
                        $(this).val('');
                    }
                    return false;
                }
                //else {
                //    $.ajax({
                //        url: '/Home/GetTopics',
                //        data: { Id: ui.item.id },
                //        type: 'GET',
                //        async: false,
                //        success: function (data) {
                //            GetTopics(element, data.Topics);
                //        }
                //    });
                //}
            }
        });
}

function GetTopics(ref, data) {
    var element = $(ref).parent().next().find(".topic");
    $(element).on("keydown", function (event) {
        if (event.keyCode === $.ui.keyCode.TAB &&
            $(this).autocomplete("instance").menu.active) {
            event.preventDefault();
        }
    })
        .autocomplete({
            minLength: 0,
            source: function (request, response) {
                response($.map(data, function (item) {
                    if (item.Text.toLowerCase().includes(extractLast(request.term.toLowerCase()))) {
                        return { label: item.Text, value: item.Text, id: item.Value };
                    }
                }));
            },
            focus: function () {
                return false;
            },
            select: function (event, ui) {
                var id = $(this).attr('name') + 'Id';
                $("input[name='" + id + "']").val(ui.item.id);
                $(this).val(ui.item.value);
                element = $(this);
                return false;
            },
            change: function (event, ui) {
                var st = $(this).parent().next().find(".subtopic");
                var stId = st.attr('name') + 'Id';
                $("input[name='" + stId + "']").val('00000000-0000-0000-0000-000000000000');
                st.val('');

                if (ui.item == null) {
                    var terms = split(this.value);
                    var last = terms.pop();
                    if (last != '') {
                        alert('<b>' + last + '</b> is not available in master data, please contact to Administrator to add this in master data.');
                        $(this).val(this.value.substr(0, this.value.length - last.length - 2)); // removes text from input
                        $(this).effect("highlight", {}, 5000);
                        $(this).attr("style", "border: solid 1px red;");
                    }
                    else {
                        var id = $(this).attr('name') + 'Id';
                        $("input[name='" + id + "']").val('00000000-0000-0000-0000-000000000000');
                        $(this).val('');
                    }
                    return false;
                }
                else {
                    $.ajax({
                        url: '/Home/GetSubtopics',
                        data: { Id: ui.item.id },
                        type: 'GET',
                        async: false,
                        success: function (data) {
                            GetSubtopics(element, data.Subtopics);
                        }
                    });
                }
            }
        });
}

/* Define function for escaping user input to be treated as a literal string within a regular expression */
function escapeRegExp(string) {
    return string.replace(/[.*+?^${}()|[\]\\]/g, "\\$&");
}

/* Define functin to find and replace specified term with replacement string */
function replaceAll(str, term, replacement) {
    return str.replace(new RegExp(escapeRegExp(term), 'g'), replacement);
}

function AddDetailRow() {
    var num = $('#divDetail').find('.detail').length;
    var row = $('.AddDetail').html();
    row = replaceAll(row, "{count}", num);
    $('#divDetail').append(row);
    var subscriptionRef = $('#divDetail').find(".subscription").last();
    $.ajax({
        url: '/UserManagement/GetSubscriptions',
        data: { AccountId: $('#AccountId').val() },
        type: 'GET',
        async: false,
        success: function (data) {
            if (subscriptionRef != undefined) {
                GetRowSubscriptions(subscriptionRef, data.Subscriptions);
            }
        }
    });
}

function RemoveDetailRow(e) {
    $(e).closest('.detail').hide();
    var id = $(e).attr('data-id');
    $("input[name='" + id + "']").val("True");
}

function GetRowSubscriptions(subscriptionRef, data) {
    subscriptionRef.autocomplete({
        minLength: 0,
        source: function (request, response) {
            response($.map(data, function (item) {
                if (item.Text.toLowerCase().includes(request.term.toLowerCase())) {
                    return { label: item.Text, value: item.Text, id: item.Value };
                }
            }));
        },
        select: function (event, ui) {
            var id = $(this).attr('name') + 'Id';
            $("input[name='" + id + "']").val(ui.item.id);
            $(this).val(ui.item.value);
            subscriptionRef = $(this);
            return false;
        },
        change: function (event, ui) {
            if (ui.item == null) {
                var terms = split(this.value);
                var last = terms.pop();
                if (last != '') {
                    alert('<b>' + last + '</b> is not available in master data, please contact to Administrator to add this in master data.');
                    $(this).val(this.value.substr(0, this.value.length - last.length - 2)); // removes text from input
                    $(this).effect("highlight", {}, 5000);
                    $(this).attr("style", "border: solid 1px red;");
                }
                else {
                    var id = $(this).attr('name') + 'Id';
                    $("input[name='" + id + "']").val('00000000-0000-0000-0000-000000000000');
                    $(this).val('');

                    var name = $(this).attr('name');
                    name = name.replace("Subscription", "IsDeleted");
                    $("input[name='" + name + "']").val("True");
                }
                return false;
            }
            else {
                var name = $(this).attr('name');
                name = name.replace("Subscription", "IsDeleted");
                $("input[name='" + name + "']").val("False");

                $.ajax({
                    url: '/UserManagement/GetPackages',
                    data: { SubscriptionId: ui.item.id },
                    type: 'GET',
                    async: false,
                    success: function (data) {
                        if (subscriptionRef != undefined) {
                            GetPackages(subscriptionRef, data.Packages);
                            GetExpiryDate(subscriptionRef, data.Subscriptions);
                        }
                    }
                });
            }
        }
    });
}